#!/bin/bash
echo "server=" $1
server=("$@")

# tar and compress only important files

dir=${PWD##*/}
dirt=$dir"slices"
dirtz=$dirt".tar.gz"

#compacting
echo $dirt
mkdir $dirt

cp uxy01  uxy02  uxy03  uxy04  uyz01      $dirt
cp vxy01  vxy02  vxy03  vxy04  vyz01      $dirt
cp wxy01  wxy02  wxy03  wxy04  wyz01      $dirt
cp oxxy01  oxxy02  oxxy03  oxxy04  oxyz01 $dirt
cp oyxy01  oyxy02  oyxy03  oyxy04  oyyz01 $dirt
cp ozxy01  ozxy02  ozxy03  ozxy04  ozyz01 $dirt
cp pxy01  pxy02  pxy03  pxy04 pyz01       $dirt
cp thxy01  thxy02  thxy03  thxy04  thyz01 $dirt
cp define.h mylist.nml xaverages.dat param.nml $dirt

echo "compresing" 
tar czvf $dirtz $dirt

#sending files
echo "sending files to" $server
scp $dirtz $server

