import numpy as N
import matplotlib.pylab as P
import matplotlib
import matplotlib.ticker as T
import os
from matplotlib import rc

# PLOT STYLE #                                                                                                         
matplotlib.rcParams['font.serif'] = 'cmr10'
matplotlib.rcParams['font.sans-serif'] = 'cmr10'

font = { 'size' : 20}
rc('font', **font)
rc('xtick', labelsize = 20)
rc('ytick', labelsize = 20)
root_dir = '/Users/fabiodelsordo/Desktop/Apstars/paper/paper.tayler'

#drho,delta, eta, by ,lamb = N.loadtxt(root_dir+'/results_thick.dat', unpack=True)
grav5, bfield5,delta5,lamb5 = N.loadtxt(root_dir+'/results_B_dependence50.txt', unpack=True)
grav10, bfield10,delta10,lamb10 = N.loadtxt(root_dir+'/results_B_dependence100.txt', unpack=True)
grav15, bfield15,delta15,lamb15 = N.loadtxt(root_dir+'/results_B_dependence150.txt', unpack=True)

P.plot(delta5,lamb5,'-o',color='r',linewidth=6,label=r'$g=50$')
P.plot(delta10,lamb10,'-o',color='b',linewidth=6,label=r'$g=100$')
P.plot(delta15,lamb15,'-o',color='g',linewidth=6,label=r'$g=150$')
P.xlabel(r'$\delta = \omega_{BV} / \omega_A$' , fontsize=20)
P.ylabel(r'$\Gamma = \sigma\; \omega_A$', fontsize=20)
#P.ylabel(r'${\rm max} \overline{B}_{\theta} \; [{\rm T}]$',fontsize=26)
#P.ylim(0.5,2.5)
#P.xlim(4,13)
#P.title("Effect of stratification for m=1",fontsize=26)
P.yscale('log')
P.xscale('log')
analysis_dir = root_dir
os.chdir(analysis_dir)
P.tight_layout()#
P.legend(loc=3)
P.savefig("lamvsdelta_Bdep.pdf",format='pdf',bbox_inches='tight')


P.show()
