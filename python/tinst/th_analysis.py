import eulag as E
import matplotlib.pylab as P
import numpy as N
import os
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc                  


matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 24}                         
rc('font', **font)                            
rc('xtick', labelsize = 20)                   
rc('ytick', labelsize = 20)                   

 
def get_folder_name(folder):
    '''
    Returns the folder name, given a full folder path
    '''
    return folder.split(os.sep)[-1]   

def time_series(f,thg):
    # number of time_steps
    nt = (f.bx.shape)[0]    
    # computing mean values of the quantities       

    bxrms = N.sqrt(N.mean((f.bx[:,thg,:]**2),axis=(1,2)))
    byrms = N.sqrt(N.mean((f.by[:,thg,:]**2),axis=(1,2)))
    bzrms = N.sqrt(N.mean((f.bz[:,thg,:]**2),axis=(1,2)))
    
    return bxrms,byrms,bzrms

def BVfreq2(g,s):
    gravity = raw_input("Enter gravity of model: ")
    gravity = float(gravity)
    gr = gravity/(g.rf/g.rf[0])**2
    return (gr/s.the_am)*E.deriv(g.r,s.the_am)

# --------------------------------------------------
# Main program, reading the data

# getting folder name
fname = get_folder_name(os.getcwd())


s = E.read_strat()
g = E.read_grid()

#Choosing the appropriate latitude
theta = g.th*180./N.pi
thg1, = N.where((theta > 0.) & (theta < 15.))
thg2, = N.where((theta > 15.) & (theta < 30))
thg3, = N.where((theta > 30.) & (theta < 45.))
thg4, = N.where((theta > 45.) & (theta < 60.))
thg5, = N.where((theta > 60.) & (theta < 75.))
thg6, = N.where((theta > 75.) & (theta < 90.))
thg7, = N.where((theta > 90.) & (theta < 105.))
thg8, = N.where((theta > 105.) & (theta < 120.))
thg9, = N.where((theta > 120.) & (theta < 135.))
thg10, = N.where((theta > 135.) & (theta < 150.))
thg11, = N.where((theta > 150.) & (theta < 165.))
thg12, = N.where((theta > 165.) & (theta < 180.))
thgT, = N.where((theta > 1.) & (theta < 90.))
thg = thgT
print('theta is going from', theta[thg[0]], 'to', theta[thg[-1]])

# PLOT STYLE #                                                                                                         
font = { 'size' : 22}
year = 365.*24.*3600.
mu0 = 4.*N.pi*1e-7
#f = E.read_f11()

# time series data
bxrms, byrms, bzrms = time_series(f,thg)

# compute Brunt-Vaisala Frequency N2
drho = s.rho_am[0]/s.rho_am[-1]
print ("Density contrast = ", drho)
N2 = BVfreq2(g,s)

#delta = Brunt-Vaisala Frequency / Alfven Frequency
bxmr = f.bx[:,thg,:].mean(axis=1)
vA = f.bx[0,:,:].max()/N.sqrt(mu0*s.rho_am[:])
fA = vA/(g.r[-1]-g.r[0])
delta = N.sqrt(N2/fA**2)
mean_delta = N.mean(delta[0:36])
print ('delta = <N_BV/f_A> = '), mean_delta

time = f.time*N.abs(fA.mean())
# figure No. 1:  time series
P.figure(1)
P.plot(time,bxrms,linewidth=3,label=r'$\overline{B}_{\phi}$')
P.plot(time,byrms,linewidth=3,label=r'$\overline{B}_{\theta}$')
P.plot(time,bzrms,linewidth=3,label=r'$\overline{B}_{r}$')
P.xlabel(r'$t\; {\rm [yr]}$',fontsize=26 )
P.ylabel(r'$B_{\rm rms}\; {\rm [T]}$',fontsize=26 )
P.xlim(0.5,time.max())
P.yscale('log')
P.legend(loc=4)
P.title('(a)  '+fname,loc='left',fontsize=22)
P.tight_layout()
P.show()

t0 = raw_input("Enter initial time to compute growth rate: ")
t0 = float(t0)
t1 = raw_input("Enter final time to compute growth rate: ")
t1 = float(t1)
itgrow = t1
itmax, = N.where(byrms == byrms.max())[0]
itdec = -10
P.figure(1)
P.axvline(x=t0,linestyle='--',c='b',linewidth=3)
P.axvline(x=t1,linestyle='--',c='b')
P.axvline(time[itmax],linestyle='--',c='g')
P.axvline(time[itdec],linestyle='--',c='r')
P.savefig("ts_"+fname+".eps",format='eps',dpi=400,bbox_inches='tight')

# compute growth rate by computing the derivative 
it1, = N.where((time > t0) & (time < t1))  
dtime = time[2]-time[1]
gr = N.mean(E.deriv1(time[it1],N.log(byrms[it1])))
C = N.mean(N.log(byrms[it1]) - time[it1]*gr)
P.plot(time[it1],N.exp(time[it1]*gr+C), linestyle='--', linewidth=3)

print ("Growth rate = ", gr )

# Saving time series
N.save('ts',(time,bxrms, byrms, bzrms))


#eta = Rotation Frequency / Alfven Frequency
period = raw_input("Enter Rotation Period: ")
period = float(period)*24.*3600.
if (period == 0.):
   omega = 0.
else: 
   omega = 2.*N.pi/period
eta = 2.*omega/N.abs(fA)
mean_eta = N.mean(eta[3:-3])
print ('eta = <2 omega/f_A> = '),mean_eta 

P.figure(2)
P.subplot(211)
P.plot(g.rf[3:-3],delta[3:-3],linewidth=3,label=r'$\delta = N_{BV}/f_A$')
P.axhline(y=N.mean(delta[3:-3]),c='k',linestyle='--')
P.ylabel(r'$\delta = N_{BV}/f_A$',fontsize=26)
P.title(r'(b)',loc='left',fontsize=22)
P.subplot(212)
#P.plot(g.rf[3:-3],(bxmr[0,3:-3] - bxmr[int(itgrow),3:-3])/bxmr[0,3:-3],linewidth=3)
P.plot(g.rf[3:-3],(bxmr[0,3:-3] - bxmr[itmax,3:-3])/bxmr[0,3:-3],linewidth=3)
P.plot(g.rf[3:-3],(bxmr[0,3:-3] - bxmr[itdec,3:-3])/bxmr[0,3:-3],linewidth=3)
P.xlabel(r'$r \; {\rm [R_s]}$', fontsize=26)
P.ylabel(r'$\langle \frac{B_{\phi}^0-B_{\phi}}{B_{\phi}^0}\rangle $',fontsize=26)
P.title(r'(c)',loc='left',fontsize=18)
P.tight_layout()
P.savefig("delta_"+fname+".eps",format='eps',dpi=400,bbox_inches='tight')
P.show()


print fname,'%12.4f'%drho,  \
            '%12.4f'%mean_delta, \
            '%12.4f'%mean_eta, \
            '%12.4f'%byrms[itmax],\
            '%12.4f'%gr,\
            '%12.4f'%theta[thg].mean()
