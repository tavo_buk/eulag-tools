import numpy as N
import eulag as E
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib
from matplotlib.ticker import FormatStrFormatter
from matplotlib import rc
from scipy.ndimage.filters import gaussian_filter
import matplotlib.pyplot as P
#from mpl_toolkits.basemap import Basemap
from pylab import meshgrid
from numpy import pi, sin, cos
#import cmocean
#from mpl_toolkits.basemap import Basemap
# PLOT STYLE #

matplotlib.rcParams['font.family'] = 'serif'
matplotlib.rcParams['font.serif'] = 'sans-serif'

font = { 'size' : 20}
rc('font', **font)
rc('xtick', labelsize = 7)
rc('ytick', labelsize = 7)

legend = {'fontsize': 9, 'numpoints' : 3}
rc('legend',**legend)
axes = {'labelsize': 9, 'titlesize' : 9}
rc('axes', **axes)
rc('mathtext',fontset='cm')
rc('lines', linewidth = 1)
rc('patch', linewidth = .8)

#use this, but at the expense of slowdown of rendering
#rc('text', usetex = True)
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]

def merid1(f1,adjust_color=1,title=r'a)',name=r'Fig',pr=0.0):

    """
    This routine creates one meridional diagram of the quantity f1
    Variables:  adjust_color = 0 - 1, sets the level of color saturation
    	    title = 'Fig', sets the name of the figure title and saving file 
    
    """

    s = E.read_strat()
    g = E.read_grid()
    R, TH = N.meshgrid(g.r, g.th)
    X = R*N.sin(TH)
    Y = R*N.cos(TH)
    X.shape

    fig, ax = P.subplots(nrows = 1, ncols = 1, facecolor = None, edgecolor = None, linewidth = None)

    # adjusting levels

    f1min = f1.min()
    f1max = f1.max()
    if abs(f1min) > abs(f1max):
        f1max = f1max*abs(f1min)/abs(f1max)
    else:
        f1min = f1min*abs(f1max)/abs(f1min)
    levels = N.linspace(f1min, f1max, 50)
    #levels = N.linspace(-1., 1., 50, endpoint=True)*adjust_color[0]
    print 'f1min = ', f1min
    print 'f1max = ', f1max
    tick = N.linspace(levels[0], levels[-1], 2, endpoint=True)

    # Creating the contour plot

    cs = ax.contourf(X, Y, f1, levels = levels, interpolation = 'bicubic',\
         cmap='RdYlBu',extend='both')

    # Add colorbar
    divider = make_axes_locatable(ax)
    #cax = divider.append_axes("left", size = "5%", pad = 0.08)
    cax1 = fig.add_axes([0.4, 0.375, 0.015, 0.25])
    cbar1 = fig.colorbar(cs, cax = cax1, ticks = tick, ticklocation = 'left', format = '%1.1f')
    cbar1.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 0, length = 6)
    cbar1.set_label(r'$B_{\phi}(t=0) \; {\rm [T]}$', rotation=90)

    # Make sure aspect ratio preserved
    ax.set_aspect('equal')

    # Turn off rectangular frame.
    ax.set_frame_on(False)

    # Turn off axis ticks.
    ax.set_xticks([])
    ax.set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
    ax.set_title(title+name,loc='left')
    ax.plot(rmax*N.sin(g.th), rmax*N.cos(g.th),'k')
    ax.plot(rmin*N.sin(g.th), rmin*N.cos(g.th),'k')
    ax.plot(g.r*N.sin(min(g.th)), g.r*N.cos(min(g.th)), 'k')
    ax.plot(g.r*N.sin(max(g.th)), g.r*N.cos(max(g.th)), 'k')

    # Saving the figure
    P.savefig(name+"%02d.png" %pr,bbox_inches='tight')


def merid3(f1,f2,f3,adjust_color = (1., 1., 1.),label=' ',pr = 0):
    from scipy.integrate import simps
    g = E.read_grid()

    cut = 5. # Degrees to be cut off
    good = int(cut*g.m/180.)

    R, TH = N.meshgrid(g.r, g.th)
    X = R*N.sin(TH)
    Y = R*N.cos(TH)

    fig, ax = P.subplots(nrows = 1, ncols = 3, facecolor = None, edgecolor = None, linewidth = None)
    ax[0].text(0,1.2*6.96e8, label)
    f1min = f1.min()
    f1max = f1.max()
    if abs(f1min) > abs(f1max):
        f1max = f1max*abs(f1min)/abs(f1max)
    else:
        f1min = f1min*abs(f1max)/abs(f1min)

#   levels = N.linspace(-f1.max(), f1.max(), 50, endpoint=True)*adjust_color[0]
    lp =  N.logspace(-3,N.log(adjust_color[0]),32)
    ln = -lp
    levels = N.concatenate([ln[::-1],lp])

    tick = N.linspace(levels[0], levels[-1], 2, endpoint=True)

    cs = ax[0].contourf(X, Y, f1[:,:], levels = levels, interpolation = 'bicubic', cmap='RdYlBu',extend='both')
    # Add colorbar
    divider = make_axes_locatable(ax[0])
    #cax = divider.append_axes("left", size = "5%", pad = 0.08)
#    cax1 = fig.add_axes([0.15, 0.42, 0.015, 0.16])
#    cbar1 = fig.colorbar(cs, cax = cax1, ticks = tick, ticklocation = 'left', format = '%1.0e')
#    cbar1.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 0, length = 6)

    # Make sure aspect ratio preserved
    ax[0].set_aspect('equal')

    # Turn off rectangular frame.
    ax[0].set_frame_on(False)

    # Turn off axis ticks.
    ax[0].set_xticks([])
    ax[0].set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
    ax[0].set_title(r'$B_{\phi}(0) \; {\rm [T]}$')
#    ax[0].set_title(r'$v_{\theta}(0) \; {\rm [m/s]}$')
    ax[0].plot(rmax*N.sin(g.th), rmax*N.cos(g.th),'k')
    ax[0].plot(rmin*N.sin(g.th), rmin*N.cos(g.th),'k')
    ax[0].plot(g.r*N.sin(min(g.th)), g.r*N.cos(min(g.th)), 'k')
    ax[0].plot(g.r*N.sin(max(g.th)), g.r*N.cos(max(g.th)), 'k')
    #ax[0].plot(g.r[27] * sin(g.th[2:-2]), g.r[27] * cos(g.th[2:-2]), '--k', alpha = .6)

    # define greater radius
    gr = N.linspace(g.r[0], 1.05*g.r[-1], 70)
    # Draw a circle in the rad-conv interface
    #ax[0].plot(gr * sin(N.pi/2.), gr * cos(N.pi/2.), '--k', alpha = .6, linewidth = 1.5)
    #ax[0].plot(gr * sin(N.pi/4.), gr * cos(N.pi/4.), '--k', alpha = .6, linewidth = 1.5)

################################################################################

    f1min = f1.min()
    f1max = f1.max()
    if abs(f1min) > abs(f1max):
        f1max = f1max*abs(f1min)/abs(f1max)
    else:
        f1min = f1min*abs(f1max)/abs(f1min)

    lp =  N.logspace(-3,N.log(adjust_color[1]),32)
    ln = -lp
    levels = N.concatenate([ln[::-1],lp])
    #levels = N.linspace(f1min, f1max, 50)
    #levels = N.linspace(-f2.max(),f2.max() , 50, endpoint=True)*adjust_color[1]

    tick = N.linspace(levels[0], levels[-1], 2, endpoint=True)
    cs = ax[1].contourf(X, Y, f2[:,:], levels = levels, interpolation = 'bicubic', cmap='RdYlBu',extend='both')
    # Add colorbar
    divider = make_axes_locatable(ax[1])
#    cax = divider.append_axes("left", size = "5%", pad = 0.08)


    #cax2 = fig.add_axes([0.42, 0.42, 0.015, 0.16])
    #cbar2 = fig.colorbar(cs, cax = cax2, ticks = tick, ticklocation = 'left', format = '%1.0e')
    #cbar2.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 0, length = 6)

    # Make sure aspect ratio preserved
    ax[1].set_aspect('equal')

    # Turn off rectangular frame.
    ax[1].set_frame_on(False)

    # Turn off axis ticks.
    ax[1].set_xticks([])
    ax[1].set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
    ax[1].set_title(r'$B_{\phi}(\pi/2) \; {\rm [T]}$')
#    ax[1].set_title(r'$v_{\theta}(\pi/4) \; {\rm [m/s]}$')
    ax[1].plot(rmax*N.sin(g.th), rmax*N.cos(g.th),'k')
    ax[1].plot(rmin*N.sin(g.th), rmin*N.cos(g.th),'k')
    ax[1].plot(g.r*N.sin(min(g.th)), g.r*N.cos(min(g.th)), 'k')
    ax[1].plot(g.r*N.sin(max(g.th)), g.r*N.cos(max(g.th)), 'k')
    #ax[1].plot(g.r[27] * sin(g.th[2:-2]), g.r[27] * cos(g.th[2:-2]), '--k', alpha = .6)
    # Draw a circle in the rad-conv interface and nssl
    #ax[1].plot(gr * sin(N.pi/2.), gr * cos(N.pi/2.), '--k', alpha = .6, linewidth = 1.5)
    #ax[1].plot(gr * sin(N.pi/4.), gr * cos(N.pi/4.), '--k', alpha = .6, linewidth = 1.5)

#

    f1min = f3.min()
    f1max = f3.max()
    if abs(f1min) > abs(f1max):
        f1max = f1max*abs(f1min)/abs(f1max)
    else:
        f1min = f1min*abs(f1max)/abs(f1min)

    lp =  N.logspace(-3,N.log(adjust_color[2]),32)
    ln = -lp
    levels = N.concatenate([ln[::-1],lp])

    #levels = N.linspace(f1min, f1max, 50)
    #levels = N.linspace(-1., 1., 50, endpoint=True)*adjust_color[2]

    tick = N.linspace(levels[0], levels[-1], 2, endpoint=True)
    cs = ax[2].contourf(X, Y, f3[:,:], levels = levels, interpolation = 'bicubic', cmap='RdYlBu',extend='both')
    # Add colorbar
    divider = make_axes_locatable(ax[1])
#    cax = divider.append_axes("left", size = "5%", pad = 0.08)

    #cax3 = fig.add_axes([0.70, 0.42, 0.015, 0.16])
    #cbar3 = fig.colorbar(cs, cax = cax3, ticks = tick, ticklocation = 'left', format = '%1.0e')
    #cbar3.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 0, length = 6)

    # Make sure aspect ratio preserved
    ax[2].set_aspect('equal')

    # Turn off rectangular frame.
    ax[2].set_frame_on(False)

    # Turn off axis ticks.
    ax[2].set_xticks([])
    ax[2].set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
    ax[2].set_title(r'$B_{\phi}(\pi)  \; {\rm [T]}$')
#    ax[2].set_title(r'$v_{\theta}(\pi/2)  \; {\rm [m/s]}$')
    ax[2].plot(rmax*N.sin(g.th), rmax*N.cos(g.th),'k')
    ax[2].plot(rmin*N.sin(g.th), rmin*N.cos(g.th),'k')
    ax[2].plot(g.r*N.sin(min(g.th)), g.r*N.cos(min(g.th)), 'k')
    ax[2].plot(g.r*N.sin(max(g.th)), g.r*N.cos(max(g.th)), 'k')
    #ax[2].plot(g.r[27] * sin(g.th[2:-2]), g.r[27] * cos(g.th[2:-2]), '--k', alpha = .6)
    # Draw a circle in the rad-conv interface and nssl
    #ax[2].plot(gr * sin(N.pi/2.), gr * cos(N.pi/2.), '--k', alpha = .6, linewidth = 1.5)
    #ax[2].plot(gr * sin(N.pi/4.), gr * cos(N.pi/4.), '--k', alpha = .6, linewidth = 1.5)

    P.savefig("bphi_%03d.png" %pr,bbox_inches='tight', dpi=300)
#   P.savefig(name+'merid'+'.png')
#   P.show()


########FF

def merid5(f1,f2,f3,f4,f5,adjust_color = (1., 1., 1.,1.,1.),label=' ',pr = 0):
    from scipy.integrate import simps
    g = E.read_grid()

    cut = 5. # Degrees to be cut off
    good = int(cut*g.m/180.)

    R, TH = N.meshgrid(g.r, g.th)
    X = R*N.sin(TH)
    Y = R*N.cos(TH)

    fig, ax = P.subplots(nrows = 1, ncols = 5, facecolor = None, edgecolor = None, linewidth = None)
    ax[0].text(0,1.2*6.96e8, label)
    f1min = f1.min()
    f1max = f1.max()
    if abs(f1min) > abs(f1max):
        f1max = f1max*abs(f1min)/abs(f1max)
    else:
        f1min = f1min*abs(f1max)/abs(f1min)

#   levels = N.linspace(-f1.max(), f1.max(), 50, endpoint=True)*adjust_color[0]
    lp =  N.logspace(-3,N.log(adjust_color[0]),32)
    ln = -lp
    levels = N.concatenate([ln[::-1],lp])

    tick = N.linspace(levels[0], levels[-1], 2, endpoint=True)
#Figure 1
    cs = ax[0].contourf(X, Y, f1[:,:], levels = levels, interpolation = 'bicubic', cmap='RdYlBu',extend='both')
    # Add colorbar
    divider = make_axes_locatable(ax[0])
    #cax = divider.append_axes("left", size = "5%", pad = 0.08)
#    cax1 = fig.add_axes([0.15, 0.42, 0.015, 0.16])
 #   cbar1 = fig.colorbar(cs, cax = cax1, ticks = tick, ticklocation = 'left', format = '%1.0e')
  #  cbar1.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 0, length = 6)

    # Make sure aspect ratio preserved
    ax[0].set_aspect('equal')

    # Turn off rectangular frame.
    ax[0].set_frame_on(False)

# Turn off axis ticks.
    ax[0].set_xticks([])
    ax[0].set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
#    ax[0].set_title(r'$B_{\phi}(0) \; {\rm [T]}$')
#    ax[0].set_title(r'$v_{\theta}(0) \; {\rm [m/s]}$')
    ax[0].set_title(r'$B_{\phi}(t/\omega_{A}= 1)$')
    ax[0].plot(rmax*N.sin(g.th), rmax*N.cos(g.th),'k')
    ax[0].plot(rmin*N.sin(g.th), rmin*N.cos(g.th),'k')
    ax[0].plot(g.r*N.sin(min(g.th)), g.r*N.cos(min(g.th)), 'k')
    ax[0].plot(g.r*N.sin(max(g.th)), g.r*N.cos(max(g.th)), 'k')
    #ax[0].plot(g.r[27] * sin(g.th[2:-2]), g.r[27] * cos(g.th[2:-2]), '--k', alpha = .6)

    # define greater radius
    gr = N.linspace(g.r[0], 1.05*g.r[-1], 70)
    # Draw a circle in the rad-conv interface
    #ax[0].plot(gr * sin(N.pi/2.), gr * cos(N.pi/2.), '--k', alpha = .6, linewidth = 1.5)
    #ax[0].plot(gr * sin(N.pi/4.), gr * cos(N.pi/4.), '--k', alpha = .6, linewidth = 1.5)

################################################################################

   # f1min = f1.min()
   # f1max = f1.max()
   # if abs(f1min) > abs(f1max):
    #    f1max = f1max*abs(f1min)/abs(f1max)
   # else:
    #    f1min = f1min*abs(f1max)/abs(f1min)

   # lp =  N.logspace(-3,N.log(adjust_color[1]),32)
   # ln = -lp
   # levels = N.concatenate([ln[::-1],lp])
    #levels = N.linspace(f1min, f1max, 50)
    #levels = N.linspace(-f2.max(),f2.max() , 50, endpoint=True)*adjust_color[1]
#Figure 2
    tick = N.linspace(levels[0], levels[-1], 2, endpoint=True)
    cs = ax[1].contourf(X, Y, f2[:,:], levels = levels, interpolation = 'bicubic', cmap='RdYlBu',extend='both')
    # Add colorbar
    divider = make_axes_locatable(ax[1])
#    cax = divider.append_axes("left", size = "5%", pad = 0.08)
 #cax2 = fig.add_axes([0.42, 0.42, 0.015, 0.16])
    #cbar2 = fig.colorbar(cs, cax = cax2, ticks = tick, ticklocation = 'left', format = '%1.0e')
    #cbar2.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 0, length = 6)

    # Make sure aspect ratio preserved
    ax[1].set_aspect('equal')

    # Turn off rectangular frame.
    ax[1].set_frame_on(False)

    # Turn off axis ticks.
    ax[1].set_xticks([])
    ax[1].set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
#    ax[1].set_title(r'$B_{\phi}(\pi/2) \; {\rm [T]}$')
#    ax[1].set_title(r'$v_{\theta}(\pi/4) \; {\rm [m/s]}$')
    ax[1].set_title(r'$B_{\phi}(t/\omega_{A}= 30)$')
    ax[1].plot(rmax*N.sin(g.th), rmax*N.cos(g.th),'k')
    ax[1].plot(rmin*N.sin(g.th), rmin*N.cos(g.th),'k')
    ax[1].plot(g.r*N.sin(min(g.th)), g.r*N.cos(min(g.th)), 'k')
    ax[1].plot(g.r*N.sin(max(g.th)), g.r*N.cos(max(g.th)), 'k')
    #ax[1].plot(g.r[27] * sin(g.th[2:-2]), g.r[27] * cos(g.th[2:-2]), '--k', alpha = .6)
    # Draw a circle in the rad-conv interface and nssl
    #ax[1].plot(gr * sin(N.pi/2.), gr * cos(N.pi/2.), '--k', alpha = .6, linewidth = 1.5)
    #ax[1].plot(gr * sin(N.pi/4.), gr * cos(N.pi/4.), '--k', alpha = .6, linewidth = 1.5)

#

   # f1min = f3.min()
   # f1max = f3.max()
   # if abs(f1min) > abs(f1max):
   #     f1max = f1max*abs(f1min)/abs(f1max)
   # else:
   #     f1min = f1min*abs(f1max)/abs(f1min)

   # lp =  N.logspace(-3,N.log(adjust_color[2]),32)
   # ln = -lp
   # levels = N.concatenate([ln[::-1],lp])

    #levels = N.linspace(f1min, f1max, 50)
    #levels = N.linspace(-1., 1., 50, endpoint=True)*adjust_color[2]
    tick = N.linspace(levels[0], levels[-1], 2, endpoint=True)
#Figure 3    
    cs = ax[2].contourf(X, Y, f3[:,:], levels = levels, interpolation = 'bicubic', cmap='RdYlBu',extend='both')
    # Add colorbar
    divider = make_axes_locatable(ax[1])
#    cax = divider.append_axes("left", size = "5%", pad = 0.08)

    #cax3 = fig.add_axes([0.70, 0.42, 0.015, 0.16])
    #cbar3 = fig.colorbar(cs, cax = cax3, ticks = tick, ticklocation = 'left', format = '%1.0e')
    #cbar3.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 0, length = 6)

    # Make sure aspect ratio preserved
    ax[2].set_aspect('equal')

    # Turn off rectangular frame.
    ax[2].set_frame_on(False)

    # Turn off axis ticks.
    ax[2].set_xticks([])
    ax[2].set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
#    ax[2].set_title(r'$B_{\phi}(\phi=0)  \; {\rm [T]}$')
#    ax[2].set_title(r'$v_{\theta}(\pi/2)  \; {\rm [m/s]}$')
    ax[2].set_title(r'$B_{\phi}(t/\omega_{A}= 70)$')
    ax[2].plot(rmax*N.sin(g.th), rmax*N.cos(g.th),'k')
    ax[2].plot(rmin*N.sin(g.th), rmin*N.cos(g.th),'k')
    ax[2].plot(g.r*N.sin(min(g.th)), g.r*N.cos(min(g.th)), 'k')
    ax[2].plot(g.r*N.sin(max(g.th)), g.r*N.cos(max(g.th)), 'k')
    #ax[2].plot(g.r[27] * sin(g.th[2:-2]), g.r[27] * cos(g.th[2:-2]), '--k', alpha = .6)
    # Draw a circle in the rad-conv interface and nssl
    #ax[2].plot(gr * sin(N.pi/2.), gr * cos(N.pi/2.), '--k', alpha = .6, linewidth = 1.5)
    #ax[2].plot(gr * sin(N.pi/4.), gr * cos(N.pi/4.), '--k', alpha = .6, linewidth = 1.5)

    tick = N.linspace(levels[0], levels[-1], 2, endpoint=True)
#Figure 4   
    cs = ax[3].contourf(X, Y, f4[:,:], levels = levels, interpolation = 'bicubic', cmap='RdYlBu',extend='both')
    # Add colorbar
    divider = make_axes_locatable(ax[1])
#    cax = divider.append_axes("left", size = "5%", pad = 0.08)

    #cax3 = fig.add_axes([0.70, 0.42, 0.015, 0.16])
    #cbar3 = fig.colorbar(cs, cax = cax3, ticks = tick, ticklocation = 'left', format = '%1.0e')
    #cbar3.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 0, length = 6)

    # Make sure aspect ratio preserved
    ax[3].set_aspect('equal')

    # Turn off rectangular frame.
    ax[3].set_frame_on(False)

    # Turn off axis ticks.
    ax[3].set_xticks([])
    ax[3].set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
#    ax[2].set_title(r'$B_{\phi}(\pi)  \; {\rm [T]}$')
#    ax[2].set_title(r'$v_{\theta}(\pi/2)  \; {\rm [m/s]}$')
    ax[3].set_title(r'$B_{\phi}(t/\omega_{A}= 90)$')
    ax[3].plot(rmax*N.sin(g.th), rmax*N.cos(g.th),'k')
    ax[3].plot(rmin*N.sin(g.th), rmin*N.cos(g.th),'k')
    ax[3].plot(g.r*N.sin(min(g.th)), g.r*N.cos(min(g.th)), 'k')
    ax[3].plot(g.r*N.sin(max(g.th)), g.r*N.cos(max(g.th)), 'k')
    #ax[2].plot(g.r[27] * sin(g.th[2:-2]), g.r[27] * cos(g.th[2:-2]), '--k', alpha = .6)
    # Draw a circle in the rad-conv interface and nssl
    #ax[2].plot(gr * sin(N.pi/2.), gr * cos(N.pi/2.), '--k', alpha = .6, linewidth = 1.5)
    #ax[2].plot(gr * sin(N.pi/4.), gr * cos(N.pi/4.), '--k', alpha = .6, linewidth = 1.5)

    tick = N.linspace(levels[0], levels[-1], 2, endpoint=True)
#Figure 5    
    cs = ax[4].contourf(X, Y, f5[:,:], levels = levels, interpolation = 'bicubic', cmap='RdYlBu',extend='both')
    # Add colorbar
    divider = make_axes_locatable(ax[1])
#    cax = divider.append_axes("left", size = "5%", pad = 0.08)

    cax3 = fig.add_axes([0.472, 0.445, 0.01, 0.10])
    cbar3 = fig.colorbar(cs, cax = cax3, ticks = tick, ticklocation = 'left', format = '%2.1f')
    cbar3.ax.tick_params(which = 'major', direction = 'inout', pad = 2, width = 0, length = 4)#which = 'major')
    cbar3.set_label(r'$[T]$',labelpad =-8, rotation=360)
    # Make sure aspect ratio preserved
    ax[4].set_aspect('equal')

    # Turn off rectangular frame.
    ax[4].set_frame_on(False)

    # Turn off axis ticks.
    ax[4].set_xticks([])
    ax[4].set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
#    ax[2].set_title(r'$B_{\phi}(\pi)  \; {\rm [T]}$')
    ax[4].set_title(r'$B_{\phi}(t/\omega_{A}= 110)$')
    ax[4].plot(rmax*N.sin(g.th), rmax*N.cos(g.th),'k')
    ax[4].plot(rmin*N.sin(g.th), rmin*N.cos(g.th),'k')
    ax[4].plot(g.r*N.sin(min(g.th)), g.r*N.cos(min(g.th)), 'k')
    ax[4].plot(g.r*N.sin(max(g.th)), g.r*N.cos(max(g.th)), 'k')
    #ax[2].plot(g.r[27] * sin(g.th[2:-2]), g.r[27] * cos(g.th[2:-2]), '--k', alpha = .6)
    # Draw a circle in the rad-conv interface and nssl
    #ax[2].plot(gr * sin(N.pi/2.), gr * cos(N.pi/2.), '--k', alpha = .6, linewidth = 1.5)
    #ax[2].plot(gr * sin(N.pi/4.), gr * cos(N.pi/4.), '--k', alpha = .6, linewidth = 1.5)


#   P.savefig(name+'merid'+'.png')
    P.show()

    P.savefig("bphi_%03d.png" %pr,bbox_inches='tight', dpi=300)
