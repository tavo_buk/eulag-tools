import eulag as E
import matplotlib.pylab as P
import numpy as N
import os
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc                  

g = E.read_grid()

time, em0, em1, em2 = N.load('modes.npy')
for k in range (0,g.l,4):
    P.plot(time,em1[k,:])

P.yscale('log')
P.ylim(1e-10,1e7)
P.show()

t0 = raw_input("Enter initial time to compute growth rate: ")
t0 = float(t0)
t1 = raw_input("Enter final time to compute growth rate: ")
t1 = float(t1)
###
P.axvline(x=t0,linestyle='--',c='b',linewidth=1)
P.axvline(x=t1,linestyle='--',c='b',linewidth=1)

# compute growth rate by computing the derivative 
it1, = N.where((time > t0) & (time < t1))  

dtime = time[2]-time[1]
grk = []

for k in range(g.l):
    gr = N.mean(E.deriv1(time[it1],N.log(em1[k,it1])))
    print ("Growth rate = ", k, gr )
    grk.append(gr)

grk = N.array(grk)
N.save('grk',grk)
