import eulag as E
import matplotlib.pylab as P
import numpy as N
import os
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc


matplotlib.rcParams['font.serif'] = 'cmr10'
matplotlib.rcParams['font.sans-serif'] = 'cmr10'

font = { 'size' : 20}
rc('font', **font)
rc('xtick', labelsize = 18)
rc('ytick', labelsize = 18)

def get_folder_name(folder):
    '''
    Returns the folder name, given a full folder path
    '''
    return folder.split(os.sep)[-1]

def Afreq2(bfield,g,s):
    """
    computes the square of the Alfven frequency)
    bfield = magnetic field array 
    s = object with the thermodinamical profiles
    """

    kmax, = N.where(bfield[(g.m/4),:] == bfield[(g.m/4),:].min())
    kmax = int(kmax)
    print 'kmax = ', kmax
    mu0 = 4.*N.pi*1e-7
    rho = s.rho_ad
    width = 0.5*6.96e8

    return kmax, (bfield[0:g.m/2,:].mean(axis=0))**2/(mu0*rho[:]*width**2)


s = E.read_strat()
g = E.read_grid()
bx0  = f.bx[0,:,:,:].mean(axis=0)
kmax,fA2 = Afreq2(bx0,g,s)
time = f.time*N.sqrt(fA2.mean())

fname = get_folder_name(os.getcwd())

bxrms2 = (f.bx[:,:,:,:]**2).mean(axis=(1,2,3))
byrms2 = (f.by[:,:,:,:]**2).mean(axis=(1,2,3))
bzrms2 = (f.bz[:,:,:,:]**2).mean(axis=(1,2,3))

bp2 = byrms2+bzrms2
bpbt = bp2/bxrms2

rho_mean = N.mean(s.rho_am)
mu0 = 4.*N.pi*1e-7
Em=(0.5/mu0)*(bp2+bxrms2)

uxrms2 = (f.u[:,:,:,:]**2).mean(axis=(1,2,3))
uyrms2 = (f.v[:,:,:,:]**2).mean(axis=(1,2,3))
uzrms2 = (f.w[:,:,:,:]**2).mean(axis=(1,2,3))
Ek=0.5*rho_mean*(uxrms2+uyrms2+uzrms2)
Et=(Em/(Ek+Em))
N.save('Em_Etot',Et)
N.save('bpbt',bpbt)



P.plot(time,Em/(Ek+Em),'-.', linewidth=4, color='black')
P.plot(time,Ek/(Ek+Em),'--', linewidth=4, color='red')
P.plot(time,bpbt, '-', linewidth=4, color='green')
P.xlabel(r'$t\; {[1/\omega_{\rm A}]}$',fontsize=18)
P.text(15, 0.85,r'$\frac{E_{mag}}{E_{tot}} $',color='black', fontsize=26)
#P.text(15, 2*10**(-2),r'$\frac{E_{mag}}{E_{tot}} $',color='black', fontsize=26)
P.text(60, 0.2, r'$\frac{E_{kin}}{E_{tot}} $', color='red', fontsize=26)
#P.text(20, 10**(-6), r'$\frac{E_{kin}}{E_{tot}} $', color='red', fontsize=26)
P.text(150, 0.3, r'$\frac{B_{pol}}{B_{tor}} $', color='green', fontsize=26)
#P.text(90, 10**(-6), r'$\frac{B_{pol}}{B_{tor}} $', color='green', fontsize=26)
P.title(r'$g=100\; , B=0.65 \; {[T]} $', fontsize=18)
#P.yscale('log')
P.show()
