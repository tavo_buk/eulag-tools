import eulag as E
import matplotlib.pylab as P
import numpy as N
import os
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc                  


matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 20}                         
rc('font', **font)                            
rc('xtick', labelsize = 18)                   
rc('ytick', labelsize = 18)                   

#def spectra(f,g,l):
#    bxt = E.turb(f.bx)
#    byt = E.turb(f.by)
#    bzt = E.turb(f.bz)
#    
#    br = bzt[it,:,:,l]
#    bt = byt[it,:,:,l]
#    bp = bxt[it,:,:,l]
#
#    e = N.zeros(g.m)
 
def get_folder_name(folder):
    '''
    Returns the folder name, given a full folder path
    '''
    return folder.split(os.sep)[-1]   

def get_gravity():
    import subprocess
    cmd = "grep '! SUN' src.F"
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    temp = process.communicate()[0]
    return float(temp[9:12])

def time_series(f,g):
    # number of time_steps
    nt = (f.bx.shape)[0]    
    
    # computing mean values of the quantities

    # 0 order 
    u0 = N.sqrt(N.mean((f.u[:,:,:]**2),axis=(1,2)))
    v0 = N.sqrt(N.mean((f.v[:,:,:]**2),axis=(1,2)))
    w0 = N.sqrt(N.mean((f.w[:,:,:]**2),axis=(1,2)))

    bx0 = N.sqrt(N.mean((f.bx[:,:,:]**2),axis=(1,2)))
    by0 = N.sqrt(N.mean((f.by[:,:,:]**2),axis=(1,2)))
    bz0 = N.sqrt(N.mean((f.bz[:,:,:]**2),axis=(1,2)))

    # non-axisymmetric
    ut2 = f.u2 - f.u**2
    vt2 = f.v2 - f.v**2
    wt2 = f.w2 - f.w**2

    bxt2 = f.bx2 - f.bx**2
    byt2 = f.by2 - f.by**2
    bzt2 = f.bz2 - f.bz**2

    bxt = N.abs(N.sqrt(N.mean((bxt2),axis=(1,2))))
    byt = N.sqrt(N.mean((byt2),axis=(1,2)))
    bzt = N.sqrt(N.mean((bzt2),axis=(1,2)))

    ut = N.sqrt(N.mean((ut2),axis=(1,2)))
    vt = N.sqrt(N.mean((vt2),axis=(1,2)))
    wt = N.sqrt(N.mean((wt2),axis=(1,2)))

    rho_mean = N.mean(s.rho_am)
    mu0 = 4.*N.pi*1e-7

    em0 = (0.5/mu0)*(bx0**2 + by0**2 + bz0**2)
    emt = (0.5/mu0)*(byt**2 + bzt**2)
    ek0 = 0.5*rho_mean*(u0**2  + v0**2  + w0**2)
    ekt = 0.5*rho_mean*(ut**2  + vt**2  + wt**2)
    
    return em0,emt,ekt

def BVfreq2(g,s):
    """
    computes the square of the Brunt-Vaisala frequency)
    g = object with the grid components
    s = object with the thermodinamical profiles
    """
 
#    gravity = raw_input("Enter gravity of model: ")
#    gravity = float(gravity)
    gravity = get_gravity()
    print 'gravity = ', gravity
    gr = gravity/(g.rf/g.rf[0])**2

    return gravity, (gr/s.the_am)*E.deriv(g.r,s.the_am)

def Afreq2(bfield,g,s):
    """
    computes the square of the Alfven frequency)
    bfield = magnetic field array 
    s = object with the thermodinamical profiles
    """

    kmax, = N.where(bfield[(g.m/4),:] == bfield[(g.m/4),:].min())
    kmax = int(kmax)
    print 'kmax = ', kmax
    mu0 = 4.*N.pi*1e-7
    rho = s.rho_ad
    width = 0.5*6.96e8

    return kmax, (bfield[0:g.m/2,:].mean(axis=0))**2/(mu0*rho[:]*width**2)

# --------------------------------------------------
# Main program, reading the data

# getting folder name
fname = get_folder_name(os.getcwd())

s = E.read_strat()
g = E.read_grid()
bx0  = f.bx[0,:,:,:].mean(axis=0)
bxmr = f.bx[:,:,0:g.m/2,:].mean(axis=(1,2))
bxrms2 = (f.bx[:,:,:,:]**2).mean(axis=(1,2,3))
byrms2 = (f.by[:,:,:,:]**2).mean(axis=(1,2,3))
bzrms2 = (f.bz[:,:,:,:]**2).mean(axis=(1,2,3))

bp2 = byrms2+bzrms2
bpbt = bp2/bxrms2

# PLOT STYLE #                                                                                                         
font = { 'size' : 22}
year = 365.*24.*3600.
mu0 = 4.*N.pi*1e-7

# compute Brunt-Vaisala, N2 and Alfven,fA2, frequencies 
gravity, N2  = BVfreq2(g,s)
kmax,fA2 = Afreq2(bx0,g,s)
# time normalized with the Alfven freq.
time = f.time*N.sqrt(fA2.mean())
# delta = Brunt-Vaisala Frequency / Alfven Frequency
delta = N.sqrt(N.mean(N2)/N.mean(fA2))

Dbx = N.zeros_like(f.bx)
v0  = N.zeros_like(f.bx)

for it in range((f.bx.shape)[0]):
    Dbx[it,:,:,:] = (f.bx[it,:,:,:] - f.bx[0,:,:,:])/f.bx[0,:,:,:] 

# time series data, saving too
print('computing spectra for different m')
em  = E.mspec_m(f.bx,f.by,f.bz,kmax)
ek  = E.mspec_m(f.u,f.v,f.w,kmax)
em0 = em[:,0]/(2.*mu0)
em1 = em[:,1]/(2.*mu0)
em2 = em[:,2]/(2.*mu0)

#ek1 = ek[:,1]/(2.*s.rho_am[kmax])

N.save('ts',(time, em0, em1, em2))
###
#### density contrast
drho = s.rho_am[0]/s.rho_am[-1]
print ("Density contrast = ", drho)

#### generating the figures
fig, ax = P.subplots(nrows=2, ncols=1, figsize=(6,6))
###
### figure No. 1:  time series
ax[0].plot(time,em0,linewidth=4,label=r'$\tilde{e}_{m0}$')
ax[0].plot(time,em1,linewidth=4,label=r'$\tilde{e}_{m1}$')
ax[0].plot(time,em2,linewidth=4,label=r'$\tilde{e}_{m2}$')
ax[0].set_xlabel(r'$t\; {[1/\omega_{\rm A}]}$',fontsize=18 )
ax[0].set_ylabel(r'$\tilde{e}_m \; {\rm [J/m^3]}$',fontsize=18 )
ax[0].set_xlim(0,time.max())
ax[0].set_ylim(1e-15,1e8)
ax[0].set_yscale('log')
ax[0].legend(loc=4)
ax[0].set_title('(a)  '+fname, loc='left',fontsize=18)
P.tight_layout()
P.show()

###
t0 = raw_input("Enter initial time to compute growth rate: ")
t0 = float(t0)
t1 = raw_input("Enter final time to compute growth rate: ")
t1 = float(t1)
###
itgrow = t1
itmax, = N.where(em1 == em1.max())[0]
itdec = -10
###
ax[0].axvline(x=t0,linestyle='--',c='b',linewidth=1)
ax[0].axvline(x=t1,linestyle='--',c='b',linewidth=1)
ax[0].axvline(x=time[itmax],linestyle='--',c='r',linewidth=0.8)

# compute growth rate by computing the derivative 
it1, = N.where((time > t0) & (time < t1))  
N.save('tgood',it1)

dtime = time[2]-time[1]
gr = N.mean(E.deriv1(time[it1],N.log(em1[it1])))
C =  N.mean(N.log(em1[it1]) - time[it1]*gr)
ax[0].plot(time[it1],N.exp(time[it1]*gr+C), linestyle='--', linewidth=3)
print ("Growth rate = ", gr )

####eta = Rotation Frequency / Alfven Frequency
###period = raw_input("Enter Rotation Period: ")
###period = float(period)*24.*3600.
###if (period == 0.):
###   omega = 0.
###else: 
###   omega = 2.*N.pi/period
###eta = 2.*omega/N.sqrt(fA2)
###print ('eta = <2 omega/f_A> = '),eta 
###

## ploting delta as a function of radius
#ax[1].plot(g.rf[3:-3],delta[3:-3],linewidth=3,label=r'$\delta = N_{BV}/f_A$')
#ax[1].axhline(y=N.mean(delta[3:-3]),c='k',linestyle='--')
#ax[1].set_ylabel(r'$\delta = N_{BV}/f_A$',fontsize=26)

bdif = (bxmr[0,:] - bxmr[int(it1[-1]),:])/bxmr[0,:]
ymax = bdif.max()
ymin = bdif.min()

N.save('eigen',(g.rf,bdif))

ax[1].plot(g.rf[3:-3],(bxmr[0,3:-3] - bxmr[int(it1[-1]),3:-3])/bxmr[0,3:-3],linewidth=3)
#ax[1].plot(g.rf[3:-3],(bxmr[0,3:-3] - bxmr[itdec,3:-3])/bxmr[0,3:-3],linewidth=3)
ax[1].set_xlabel(r'$r \; {[R_s]}$', fontsize=18)
ax[1].set_ylabel(r'$\langle \frac{B_{\phi}^0-B_{\phi}}{B_{\phi}^0}\rangle $',fontsize=18)
ax[1].xaxis.set_ticks(N.linspace(g.rf[0],g.rf[-1],3))
ax[1].xaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[1].yaxis.set_ticks(N.linspace(ymax,ymin,4))
ax[1].yaxis.set_major_formatter(T.FormatStrFormatter('$%.2f$'))

P.savefig("delta_"+fname+".pdf",format='pdf',dpi=400,bbox_inches='tight')


bx0max = N.abs(bx0.max())
N2mean = N.sqrt(N2.mean())
fA2mean = N.sqrt(fA2.mean())

print fname,'%12.4f'%gravity,   \
            '%12.4f'%bx0max,    \
            '%12.4e'%N2mean,  \
            '%12.4e'%fA2mean,  \
            '%12.4f'%drho,  \
            '%12.4f'%delta, \
            '%12.4f'%bpbt[itmax],\
            '%12.4f'%gr
