import numpy as N
import matplotlib.pylab as P
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc  
import eulag as E                
import os

# PLOT STYLE #                                                                                                         
matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 24}                         
rc('font', **font)                            
rc('xtick', labelsize = 24)                   
rc('ytick', labelsize = 24)                   
analysis_dir = '/home/guerrero/eulag-tools/python/tinst/'

root_dir = '/storage/Tayler/'
dir1 = 'BVthin/'
dirs1 = ['thinrh02', 'thinrh04',  'thinrh06', \
         'thinrh08']
labels1 = ['TiA02','TiA04','TiA06', 'TiA08']
dir2 = 'BVthick/'
dirs2 = ['thrh02', 'thrh04',  \
         'thrh07', 'thrh09']
labels2 = ['TiB02','TiB04','TiB07', 'TiA09']

fig, ax = P.subplots(2,1, figsize=(7, 10))
nd = len(dirs1)
ic = N.linspace(0,0.5,nd)
xini = 0.4
xend = 0.98 
yini = 0
yend = 40
ax[0].set_xlim(xini,xend) 
ax[0].set_ylim(yini,yend)
ax[0].set_ylabel(r'$\rho_{\rm ad,\; amb} \; {\rm [kg / m^3]}$',fontsize=26)

for id in range (nd): 
    os.chdir(root_dir+dir1+dirs1[id])
    print 'density  of model', id
    s = E.read_strat()
    ax[0].plot(s.radius,s.rho_ad,linewidth=3,color=P.cm.jet(ic[id]), label=labels1[id])
    ax[0].plot(s.radius,s.rho_am,linestyle='dashed',linewidth=3,color=P.cm.jet(ic[id]))

ax[0].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[0].yaxis.set_ticks(N.linspace(yini,yend,5))
ax[0].xaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[0].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[0].legend(loc=3,ncol=2,fontsize=16)

xini = 0.4
xend = 0.98 
yini = 0
yend = 40
ax[1].set_xlim(xini,xend) 
ax[1].set_ylim(yini,yend)
ax[1].set_xlabel(r'$ r [R_{\odot}]$' ,fontsize=26)
ax[1].set_ylabel(r'$\rho_{\rm ad, \; amb} \; {\rm [kg / m^3]}$',fontsize=26)
nd = len(dirs2)
ic = N.linspace(0.5,1,nd)
for id in range (nd): 
    os.chdir(root_dir+dir2+dirs2[id])
    print 'density  of model', id
    s = E.read_strat()
    ax[1].plot(s.radius,s.rho_ad,linewidth=3,color=P.cm.jet(ic[id]), label=labels2[id])
    ax[1].plot(s.radius,s.rho_am,linestyle='dashed',linewidth=3,color=P.cm.jet(ic[id]))

ax[1].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[1].yaxis.set_ticks(N.linspace(yini,yend,5))
ax[1].xaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[1].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[1].legend(loc=3,ncol=2,fontsize=16)
P.tight_layout()
os.chdir(analysis_dir)
P.savefig("rho_all.eps",format='eps',bbox_inches='tight')
P.show()


