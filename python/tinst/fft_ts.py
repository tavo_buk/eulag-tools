# python function to compute the spectral energy 
# in the azimuthal direction

import numpy as N
import matplotlib.pylab as P
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc  
import eulag as E                
import os

# PLOT STYLE #                                                                                                         
matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 24}                         
rc('font', **font)                            
rc('xtick', labelsize = 24)                   
rc('ytick', labelsize = 24)


def Afreq2(bfield,g,s):
    """
    computes the square of the Alfven frequency)
    bfield = magnetic field array 
    s = object with the thermodinamical profiles
    """

    kmax, = N.where(bfield[0,0,:] == bfield[0,0,:].min())
    kmax = int(kmax)
    mu0 = 4.*N.pi*1e-7
  
    rho = s.rho_am[kmax]
#    dr = g.r[-1]-g.r[0]
    dr = g.r[1]-g.r[0]
    bmax = bfield[0,0,:].max()

    return bmax**2/(mu0*rho*dr**2)

def fft_ts(field,g):
    '''
    This function computes the fft for each latitude
    and a given radius. The result is a 3D array (time, m mode, latitude)
    '''

    arrayx = (range(g.n/2+1)+range(-g.n/2+1,0))
    kxmin = 2.*N.pi/(2*N.pi)
    kxmax = g.n*kxmin
    kymin=2*N.pi/(N.pi)
    kymax=g.m*kymin

    arrayx=(range(g.n/2+1)+range(-g.n/2+1,0))
    arrayy=(range(g.m/2+1)+range(-g.n/2+1,0))
    kx=[arrayx[i]*kxmin for i in range(g.n)]
    ky=[arrayy[i]*kymin for i in range(g.m)]

    ks = ks=range(g.n/2)
    nt = (field.bx.shape)[0]
    Es=N.zeros((nt,g.n,g.m),'float32')
     
    for j in range (g.m):
        for it in range (nt):
            ftf_by = N.fft.fft(field.by[it,:,j])
            ftf_bz = N.fft.fft(field.bz[it,:,j])
            
            Es[it,:,j] = (ftf_by.real**2 + ftf_bz.real**2) + \
                         (ftf_by.imag**2 + ftf_bz.imag**2)
   
    return Es

root_dir = '/storage/Tayler/BVthick'
g = E.read_grid()
s = E.read_strat()

fA2  = Afreq2(f.bx,g,s) 
#time = f.time*N.sqrt(fA2)

fig, ax = P.subplots()
xini = 1.
xend = 10000 
ax.set_xlim(xini,xend) 
#yini = 0.
#yend = 0.
#ax[0].set_ylim(yini,yend)

ax.set_ylabel('Spectral energy in m=1',fontsize=26)
ax.set_xlabel(r'$t$',fontsize=26)
 
tm0 = fft_ts(f,g)

for j in range(2,g.m/2,4):
    lat = round(g.th[j]*180./N.pi)
    print '%12.2f'%lat
    ax.plot(f.time[1:]*N.sqrt(fA2),N.log10(tm0[1:,2,j]),lw=3,label=r'$\theta = $'+str(lat))

P.legend(loc=4)
P.tight_layout()
P.savefig("tsm1.png",format='png',bbox_inches='tight')
P.show()
    
