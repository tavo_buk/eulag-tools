import numpy as N
import matplotlib.pylab as P
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc

# PLOT STYLE #                                                                                                         
matplotlib.rcParams['font.serif'] = 'cmr10'
matplotlib.rcParams['font.sans-serif'] = 'cmr10'

font = { 'size' : 24}
rc('font', **font)
rc('xtick', labelsize = 24)
rc('ytick', labelsize = 24)
root_dir = '/home/guerrero/eulag-tools/python/tinst'

drho,delta,eta,bmax,lamb = N.loadtxt(root_dir+'/results_thick_R9.dat', unpack=True)

P.plot(2.*N.pi*eta,lamb,color='b',linewidth=3,label=r'$\delta = N_{BV}/\omega_A = 8.2$')
P.plot(2.*N.pi*eta,lamb,'o', color='b')
P.xlabel(r'$\eta = 2 \Omega_0 / \omega_A$' ,fontsize=26)
P.ylabel(r'$\lambda \; [{\rm s}^{-1}]$',fontsize=26)
#P.ylabel(r'${\rm max} \overline{B}_{\theta} \; [{\rm T}]$',fontsize=26)
P.tight_layout()

drho,delta,eta,bmax,lamb = N.loadtxt(root_dir+'/results_thick_R3.dat', unpack=True)
P.plot(2.*N.pi*eta,lamb,color='r',linewidth=3,label=r'$\delta = N_{BV}/\omega_A = 5.1$')
P.plot(2.*N.pi*eta,lamb,'o', color='r')
P.legend(loc=1)

P.show()
