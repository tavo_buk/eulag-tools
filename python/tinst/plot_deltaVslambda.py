import numpy as N
import matplotlib.pylab as P
import matplotlib
import matplotlib.ticker as T
import os
from matplotlib import rc

# PLOT STYLE #                                                                                                         
matplotlib.rcParams['font.serif'] = 'cmr10'
matplotlib.rcParams['font.sans-serif'] = 'cmr10'

font = { 'size' : 20}
rc('font', **font)
rc('xtick', labelsize = 20)
rc('ytick', labelsize = 20)
root_dir = '/home/guerrero/Projects/paper.tayler/'

#drho,delta, eta, by ,lamb = N.loadtxt(root_dir+'/results_thick.dat', unpack=True)
drhoT,deltaT, byT ,lambT = N.loadtxt(root_dir+'/results_thick.txt', unpack=True)
drhoH,deltaH, byH ,lambH = N.loadtxt(root_dir+'/results_thick_hr.txt', unpack=True)
drhoS,deltaS, byS ,lambS = N.loadtxt(root_dir+'/results_thin.txt', unpack=True)

P.plot(deltaT,lambT,'-o',color='r',linewidth=5,label=r'TiA')
P.plot(deltaS,lambS,'-o',color='b',linewidth=5,label=r'TiB')
P.plot(deltaH,lambH,'-o',color='g',linewidth=5,label=r'TiB_hr')
P.xlabel(r'$\delta = \omega_{BV} / \omega_A$' ,	fontsize=20)
P.ylabel(r'$\Gamma = \sigma\; \omega_A$', fontsize=20)
#P.ylabel(r'${\rm max} \overline{B}_{\theta} \; [{\rm T}]$',fontsize=26)
#P.ylim(0.5,2.5)
#P.xlim(4,13)
#P.title("Effect of stratification for m=1",fontsize=26)
P.yscale('log')
P.xscale('log')
analysis_dir = root_dir 
os.chdir(analysis_dir)
P.tight_layout()#
P.savefig("lamvsdelta.pdf",format='pdf',bbox_inches='tight')

P.legend(loc=3)

P.show()
