import numpy as N
import matplotlib.pylab as P
import eulag as E
from scipy.ndimage.filters import gaussian_filter
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc                  

# PLOT STYLE #                                                                                                         
matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 22}                         
rc('font', **font)                            
rc('xtick', labelsize = 20)                   
rc('ytick', labelsize = 20)                   

#######################################################################
# quantities to be plotted

#######################################################################
fig, ax = P.subplots(2, 3, figsize=(15,8))
root_dir = '/storage/Tayler/BVthick/'
dfile = 'ts.npy'
mu0 = 4.*N.pi*1e-7
enor  = 1./(2.*mu0)
#######################################################################
# FIRST COLUMN
#######################################################################
# B = 1T

f1_a = root_dir+'/TiA05/'+dfile
time_a, em0_a, em1_a, em2_a = N.load(f1_a)
enor = em0_a.max()

f1_b = root_dir+'/ts/tsg100b1.npy' 
time_b, em0_b, em1_b, em2_b = N.load(f1_b)

f1_c = root_dir+'/ts/tsg150b1.npy' 
time_c, em0_c, em1_c, em2_c = N.load(f1_c)
xini = 0.
xend = 120 
yini = 1e-18
yend = 1.e1
ax[0,0].set_title(r'(a) $B_0=1$ \;T',fontsize=20,loc='left')
ax[0,0].plot(time_a,em0_a/enor,linewidth=4,color='c',label=r'$m=0$')
ax[0,0].plot(time_a,em1_a/enor,linewidth=4,color='darkorange',label=r'$m=1$')

ax[0,0].plot(time_b,em0_b/enor,linewidth=4,color='c',linestyle= '--')
ax[0,0].plot(time_b,em1_b/enor,linewidth=4,color='darkorange',linestyle= '--')

ax[0,0].plot(time_c,em0_c/enor,linewidth=4,color='c',linestyle= '-.')
ax[0,0].plot(time_c,em1_c/enor,linewidth=4,color='darkorange',linestyle= '-.')

ax[0,0].set_yscale('log')
ax[0,0].set_xlim(xini,xend)
ax[0,0].set_ylim(yini,yend)
ax[0,0].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[0,0].tick_params(axis='x',which='both',labelbottom=False)
ax[0,0].set_ylabel(r'$\log(\tilde{e}_{B}/e_0)$')
ax[0,0].legend(loc=4,fontsize=16)
#ax[0,0].yaxis.set_ticks(N.linspace(yini,yend,5))
#ax[0,0].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))

#######################################################################
# B = 0.65 T

f2_a = root_dir+'/TiBb03/'+dfile
time_a, em0_a, em1_a, em2_a = N.load(f2_a)

f2_b = root_dir+'/ts/tsg100b065.npy' 
time_b, em0_b, em1_b, em2_b = N.load(f2_b)

f2_c = root_dir+'/ts/tsg150b065.npy' 
time_c, em0_c, em1_c, em2_c = N.load(f2_c)

xini = 0.
xend = 120.
yini = 1e-18
yend = 1.e1
ax[1,0].set_title(r'(b) $B_0=0.65$ \;T',fontsize=20,loc='left')

ax[1,0].plot(time_a,em0_a/enor,linewidth=4,color='c')
ax[1,0].plot(time_a,em1_a/enor,linewidth=4,color='darkorange')

ax[1,0].plot(time_b,em0_b/enor,linewidth=4,color='c',linestyle= '--')
ax[1,0].plot(time_b,em1_b/enor,linewidth=4,color='darkorange',linestyle= '--')

ax[1,0].plot(time_c,em0_c/enor,linewidth=4,color='c',linestyle= '-.')
ax[1,0].plot(time_c,em1_c/enor,linewidth=4,color='darkorange',linestyle= '-.')

ax[1,0].set_yscale('log')
ax[1,0].set_xlim(xini,xend)
ax[1,0].set_ylim(yini,yend)
ax[1,0].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[1,0].tick_params(axis='x',which='both',labelbottom=True)
ax[1,0].set_ylabel(r'$\log(\tilde{e}_{B}/e_0)$')
#ax[1,0].yaxis.set_ticks(N.linspace(yini,yend,5))
#ax[1,0].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[1,0].set_xlabel(r'$t [\omega_A]$')
ax[1,0].xaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[1,0].legend(loc=1,ncol=1,fontsize=13,frameon=False)

#######################################################################
# SECOND COLUMN
#######################################################################
# B = 0.5 T

f3_a = root_dir+'/TiBb02/'+dfile
time_a, em0_a, em1_a, em2_a = N.load(f3_a)

f3_b = root_dir+'/ts/tsg100b05.npy' 
time_b, em0_b, em1_b, em2_b = N.load(f3_b)

xini = 0.
xend = 100.
yini = 1e-15
yend = 1.e1
ax[0,1].set_title(r'(c) $B_0=0.5$ \;T',fontsize=20,loc='left')

ax[0,1].plot(time_a,em0_a/enor,linewidth=4,color='c')
ax[0,1].plot(time_a,em1_a/enor,linewidth=4,color='darkorange')

ax[0,1].plot(time_b,em0_b/enor,linewidth=4,color='c',linestyle= '--')
ax[0,1].plot(time_b,em1_b/enor,linewidth=4,color='darkorange',linestyle= '--')

ax[0,1].set_yscale('log')
ax[0,1].set_xlim(xini,xend)
ax[0,1].set_ylim(yini,yend)

ax[0,1].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[0,1].tick_params(axis='x',which='both',labelbottom=False)
ax[0,1].tick_params(axis='y',which='both',labelleft=False)
#ax[0,1].yaxis.set_ticks(N.linspace(yini,yend,5))
#ax[0,1].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))

#######################################################################
# B = 0.1 T

f4_a = root_dir+'/TiBb00/'+dfile
time_a, em0_a, em1_a, em2_a = N.load(f4_a)

f4_b = root_dir+'/ts/tsg100b01.npy' 
time_b, em0_b, em1_b, em2_b = N.load(f4_b)

xini = 0.
xend = 100.
yini = 1e-15
yend = 1.e1
ax[1,1].set_title(r'(d) $B_0=0.1$ \;T' ,fontsize=20,loc='left')

ax[1,1].plot(time_a,em0_a/enor,linewidth=4,color='c')
ax[1,1].plot(time_a,em1_a/enor,linewidth=4,color='darkorange')

ax[1,1].plot(time_b,em0_b/enor,linewidth=4,color='c',linestyle= '--')
ax[1,1].plot(time_b,em1_b/enor,linewidth=4,color='darkorange',linestyle= '--')

ax[1,1].set_yscale('log')
ax[1,1].set_xlim(xini,xend)
ax[1,1].set_ylim(yini,yend)
ax[1,1].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[1,1].tick_params(axis='x',which='both',labelbottom=True)
ax[1,1].tick_params(axis='y',which='both',labelleft=False)
#ax[1,1].yaxis.set_ticks(N.linspace(yini,yend,5))
#ax[1,1].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[1,1].set_xlabel(r'$t [\omega_A]$')
ax[1,1].xaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[1,1].legend(loc=1,ncol=1,fontsize=13,frameon=False)

#######################################################################
# THIRD COLUMN
#######################################################################
# B = 0.05 T

f5_a = root_dir+'/TiBb04/'+dfile
time_a, em0_a, em1_a, em2_a = N.load(f5_a)

f5_b = root_dir+'/ts/tsg100b005.npy' 
time_b, em0_b, em1_b, em2_b = N.load(f5_b)

xini = 0.
xend = 15.
yini = 1e-15
yend = 1.e1
ax[0,2].set_title(r'(e) $B_0=0.05$ \;T',fontsize=20,loc='left')

ax[0,2].plot(time_a,em0_a/enor,linewidth=4,color='c')
ax[0,2].plot(time_a,em1_a/enor,linewidth=4,color='darkorange')

ax[0,2].plot(time_b,em0_b/enor,linewidth=4,color='c',linestyle= '--')
ax[0,2].plot(time_b,em1_b/enor,linewidth=4,color='darkorange',linestyle= '--')

ax[0,2].set_yscale('log')
ax[0,2].set_xlim(xini,xend)
ax[0,2].set_ylim(yini,yend)

ax[0,2].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[0,2].tick_params(axis='x',which='both',labelbottom=False)
ax[0,2].tick_params(axis='y',which='both',labelleft=False)
#ax[0,2].yaxis.set_ticks(N.linspace(yini,yend,5))
#ax[0,2].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))

#######################################################################
# B = 0.01 T

f6_a = root_dir+'/TiBb05_dt/'+dfile
time_a, em0_a, em1_a, em2_a = N.load(f6_a)

xini = 0.
xend = 15.
yini = 1e-15
yend = 1.e1
ax[1,2].set_title(r'(f) $B_0=0.01$ \;T',fontsize=20,loc='left')

ax[1,2].plot(time_a,em0_a/enor,linewidth=4,color='c')
ax[1,2].plot(time_a,em1_a/enor,linewidth=4,color='darkorange')

ax[1,2].set_yscale('log')
ax[1,2].set_xlim(xini,xend)
ax[1,2].set_ylim(yini,yend)
ax[1,2].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[1,2].tick_params(axis='x',which='both',labelbottom=True)
ax[1,2].tick_params(axis='y',which='both',labelleft=False)
#ax[1,2].yaxis.set_ticks(N.linspace(yini,yend,5))
#ax[1,2].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[1,2].set_xlabel(r'$t [\omega_A]$')
ax[1,2].xaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[1,2].legend(loc=1,ncol=1,fontsize=13,frameon=False)

P.tight_layout()
P.savefig("multi_g.pdf",format='pdf',dpi=400,bbox_inches='tight')
#P.show()

