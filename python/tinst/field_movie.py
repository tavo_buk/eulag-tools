import numpy as N
import matplotlib.pylab as P
import eulag as E

nt = f.time.size
bxm = f.bx.mean(axis=1)
bym = f.by.mean(axis=1)
bzm = f.bz.mean(axis=1)

um = f.u.mean(axis=1)
vm = f.v.mean(axis=1)
wm = f.w.mean(axis=1)

bxt = E.turb(f.bx)
byt = E.turb(f.by)
bzt = E.turb(f.bz)

bxmax = f.bx.max()
bymax = byt[:,:,:,:].max()
bzmax = bzt[:,:,:,:].max()

uxmax = um[:,:,:].mean()
uymax = vm[:,:,:].mean()
uzmax = wm[:,:,:].mean()

for it in range (10,12):
    print it
    merid3(f.bx[it,2,:,:],f.bx[it,32,:,:],f.bx[it,64,:,:],adjust_color=[bxmax,bxmax,bxmax],pr=it)
    #merid3(um[it,:,:],vm[it,:,:],wm[it,:,:],adjust_color=[3,3,3],pr=it)
    P.pause(0.05)
    P.close()
    


