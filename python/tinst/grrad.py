import eulag as E
import matplotlib.pylab as P
import numpy as N
import os
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc                  


matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 20}                         
rc('font', **font)                            
rc('xtick', labelsize = 18)                   
rc('ytick', labelsize = 18)                   

#def spectra(f,g,l):
#    bxt = E.turb(f.bx)
#    byt = E.turb(f.by)
#    bzt = E.turb(f.bz)
#    
#    br = bzt[it,:,:,l]
#    bt = byt[it,:,:,l]
#    bp = bxt[it,:,:,l]
#
#    e = N.zeros(g.m)
 
def get_folder_name(folder):
    '''
    Returns the folder name, given a full folder path
    '''
    return folder.split(os.sep)[-1]   

def get_gravity():
    import subprocess
    cmd = "grep '! SUN' src.F"
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    temp = process.communicate()[0]
    return float(temp[9:12])

def time_series(f,g):
    # number of time_steps
    nt = (f.bx.shape)[0]    
    
    # computing mean values of the quantities

    # 0 order 
    u0 = N.sqrt(N.mean((f.u[:,:,:]**2),axis=(1,2)))
    v0 = N.sqrt(N.mean((f.v[:,:,:]**2),axis=(1,2)))
    w0 = N.sqrt(N.mean((f.w[:,:,:]**2),axis=(1,2)))

    bx0 = N.sqrt(N.mean((f.bx[:,:,:]**2),axis=(1,2)))
    by0 = N.sqrt(N.mean((f.by[:,:,:]**2),axis=(1,2)))
    bz0 = N.sqrt(N.mean((f.bz[:,:,:]**2),axis=(1,2)))

    # non-axisymmetric
    ut2 = f.u2 - f.u**2
    vt2 = f.v2 - f.v**2
    wt2 = f.w2 - f.w**2

    bxt2 = f.bx2 - f.bx**2
    byt2 = f.by2 - f.by**2
    bzt2 = f.bz2 - f.bz**2

    bxt = N.abs(N.sqrt(N.mean((bxt2),axis=(1,2))))
    byt = N.sqrt(N.mean((byt2),axis=(1,2)))
    bzt = N.sqrt(N.mean((bzt2),axis=(1,2)))

    ut = N.sqrt(N.mean((ut2),axis=(1,2)))
    vt = N.sqrt(N.mean((vt2),axis=(1,2)))
    wt = N.sqrt(N.mean((wt2),axis=(1,2)))

    rho_mean = N.mean(s.rho_am)
    mu0 = 4.*N.pi*1e-7

    em0 = (0.5/mu0)*(bx0**2 + by0**2 + bz0**2)
    emt = (0.5/mu0)*(byt**2 + bzt**2)
    ek0 = 0.5*rho_mean*(u0**2  + v0**2  + w0**2)
    ekt = 0.5*rho_mean*(ut**2  + vt**2  + wt**2)
    
    return em0,emt,ekt

def BVfreq2(g,s):
    """
    computes the square of the Brunt-Vaisala frequency)
    g = object with the grid components
    s = object with the thermodinamical profiles
    """
 
#    gravity = raw_input("Enter gravity of model: ")
#    gravity = float(gravity)
    gravity = get_gravity()
    print 'gravity = ', gravity
    gr = gravity/(g.rf/g.rf[0])**2

    return gravity, (gr/s.the_am)*E.deriv(g.r,s.the_am)

def Afreq2(bfield,g,s):
    """
    computes the square of the Alfven frequency)
    bfield = magnetic field array 
    s = object with the thermodinamical profiles
    """

    kmax, = N.where(bfield[(g.m/4),:] == bfield[(g.m/4),:].min())
    kmax = int(kmax)
    print 'kmax = ', kmax
    mu0 = 4.*N.pi*1e-7
    rho = s.rho_ad
    width = 0.5*6.96e8

    return kmax, (bfield[0:g.m/2,:].mean(axis=0))**2/(mu0*rho[:]*width**2)

# --------------------------------------------------
# Main program, reading the data

# getting folder name
fname = get_folder_name(os.getcwd())

s = E.read_strat()
g = E.read_grid()
bx0  = f.bx[0,:,:,:].mean(axis=0)
bxmr = f.bx[:,:,0:g.m/2,:].mean(axis=(1,2))
bxrms2 = (f.bx[:,:,:,:]**2).mean(axis=(1,2,3))
byrms2 = (f.by[:,:,:,:]**2).mean(axis=(1,2,3))
bzrms2 = (f.bz[:,:,:,:]**2).mean(axis=(1,2,3))

bp2 = byrms2+bzrms2
bpbt = bp2/bxrms2

# PLOT STYLE #                                                                                                         
font = { 'size' : 22}
year = 365.*24.*3600.
mu0 = 4.*N.pi*1e-7

# compute Brunt-Vaisala, N2 and Alfven,fA2, frequencies 
gravity, N2  = BVfreq2(g,s)
kmax,fA2 = Afreq2(bx0,g,s)
# time normalized with the Alfven freq.
time = f.time*N.sqrt(fA2.mean())
# delta = Brunt-Vaisala Frequency / Alfven Frequency
delta_r = N.sqrt(N2/fA2)
N.save('delta',delta_r)
delta   = N.sqrt(N.mean(N2)/N.mean(fA2))

Dbx = N.zeros_like(f.bx)
v0  = N.zeros_like(f.bx)

for it in range((f.bx.shape)[0]):
    Dbx[it,:,:,:] = (f.bx[it,:,:,:] - f.bx[0,:,:,:])/f.bx[0,:,:,:] 

m0 = []
m1 = []
m2 = []

for k in range (g.l):
    print 'k = ', k
    # time series data, saving too
    print('computing spectra for different m')
    em  = E.mspec_m(f.bx,f.by,f.bz,k)
    em0 = em[:,0]/(2.*mu0)
    em1 = em[:,1]/(2.*mu0)
    em2 = em[:,2]/(2.*mu0)

    m0.append(em0)
    m1.append(em1)
    m2.append(em2)

m0 = N.array(m0)
m1 = N.array(m1)
m2 = N.array(m2)

N.save('modes',((time,m0,m1,m2)))


