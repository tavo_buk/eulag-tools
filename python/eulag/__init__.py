from eulag.files.eudyn import * 
from eulag.files.grid import read_grid
from eulag.files.strat import read_strat, read_strat_atom
from eulag.files.modules import *
from eulag.files.rbox import read_f11
from eulag.files.rbox2mean import rf11m
from eulag.files.rbox2shell import rf11s
from eulag.files.rxav import rxav
from eulag.files.add_eulag_files import sum_f11, sum_f11_HD, sum_xav
from eulag.files.spectra import kspec,mspec2,mspec_m,mspec,mspec_low, kspec1D,restore_fields
from eulag.files.energy import energy, energy_xav, energy_HD
from eulag.files.reynolds import amflux, iflux_r, iflux_th, torque
from eulag.files.write_mff import write_mff_alpha, write_mff_omega
#from eulag.files.graphics import mollweide, longw, butterfly,butterfly_simple, ts, et, energy_r, merid_b, merid, merid_simple, omega, energy_harmonics, alpha, pif_th, pif_r, plot_torque_tot_MHD, plot_torque_tot, et_MHD, B_moll, B_moll_rf
from eulag.files.ts import ts
