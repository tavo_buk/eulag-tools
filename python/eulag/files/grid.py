import numpy as N
from math import pi
from eulag.files.strat import read_strat
from eulag.files.eudyn import parameters

def read_grid(*args, **kwargs):
    """
    Read grid from eulag.
    rstar = radius of the star, default solar radius
    """
    return Grid(*args, **kwargs)

class Grid(object):
    def __init__ (self,rstar=6.96e8,param=None):
        param = parameters()
        if param[3] == 0:         # For spherical simulations
            (n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver,\
                    nslice, nplot, nstore, nxaver, it, rb, rt) = param
            s = read_strat()
            r = N.linspace(rb, rt, l)
            rf = r/rstar
            th = pi*N.arange(0,m)/float(m-1)
            phi = 2.*pi*N.arange(0,n)/float(n-1)
            sin_th = N.sin(th)
            i_sin = N.where(N.abs(sin_th) < 1e-5)[0]
            if i_sin.size > 0:
                sin_th[i_sin] = 1
            R, TH = N.meshgrid(r, th)
            R, sTH = N.meshgrid(r,sin_th)
            x = R*N.sin(TH)
            y = R*N.cos(TH)
            rsinth = R*sTH

            ''' time '''
            self.t_f11 = dplot*dt
            self.t_xav = dxaver*dt

            ''' grid  '''
            self.n = n
            self.m = m
            self.l = l
            self.r = r
            self.th = th
            self.phi = phi
            self.rsinth = rsinth
            self.x = x
            self.y = y
            self.r = r
            self.rf = rf
            self.rs = rstar
        else:
            print ('This is a cartesian simulation.')
            (n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver, \
            nslice, nplot, nstore, nxaver, it) = param

            '''time'''
            self.t_f11 = dplot*dt
            self.t_xav = dxaver*dt

            ''' grid '''
            self.n = n
            self.m = m
            self.l = l
