# read eulag strat: GG
from scipy.io import FortranFile
from eulag.files.grid import read_grid
from eulag.files.eudyn import parameters
from eulag.files.modules import filter3D
import numpy as N

def rf11s(*args, **kwargs):
        """Read fort.11, short tape from eulag.
           call using
           import eulag as E
           f = rf11s(lev)
           f will be an object with the variables

           Parameters:
           lev = is the altitude where the slice will be taken
           start: = 0 (default) initializes the reading after 
                    n outputs                   
        """
        return Rbox(*args, **kwargs)

class Rbox(object):
    def __init__ (self, datafile='', lev = -1, start = 0):
        # Read xaverages file
        # Datafile is the name of the xaverages file to be read
        # ifl can be False or True, default is True
        if (not datafile):
            datafile='fort.11'

        param = parameters()
        if param[3] == 0:         # For spherical simulations
            (n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver,\
                    nslice, nplot, nstore, nxaver, it, rb, rt) = param
        else:
            print ('This is a cartesian simulation.')
            (n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver, \
            nslice, nplot, nstore, nxaver, it) = param

        filename = "define.h"
        imhd = open(filename)
        for line in imhd:
            if line.split()[1] == "MHD":
                MHD = int(line.split()[2])       # If MHD = 0 there is no magnetic field

        if MHD == 1:
            print ('MHD config.')
        else:
            print ('HD config.')

        ''' Defining variables '''
        counter = 0
        u  = [] ; v  = [] ; w  = []
        the  = []
        P  =  []
        if MHD == 1:
            bx  = [] ; by  = [] ; bz  = []

        ''' reading the file '''
        f11 = FortranFile(datafile)
        while True:
            try:
                ''' Reading variables '''
                # Velocity field
                tmp = N.reshape(f11.read_reals('f'), (n, m, l), order = 'F')
                u.append(tmp[:,:,lev])
                tmp = N.reshape(f11.read_reals('f'), (n, m, l), order = 'F')
                v.append(tmp[:,:,lev])
                tmp = N.reshape(f11.read_reals('f'), (n, m, l), order = 'F')
                w.append(tmp[:,:,lev])
                tmp = N.reshape(f11.read_reals('f'), (n, m, l), order = 'F')
                the.append(tmp[:,:,lev])
                tmp = N.reshape(f11.read_reals('f'), (n, m, l), order = 'F')
                P.append(tmp[:,:,lev])
                if MHD == 1:
                    # Magnetic field
                    tmp = N.reshape(f11.read_reals('f'), (n, m, l), order = 'F')
                    bx.append(tmp[:,:,lev])
                    tmp = N.reshape(f11.read_reals('f'), (n, m, l), order = 'F')
                    by.append(tmp[:,:,lev])
                    tmp = N.reshape(f11.read_reals('f'), (n, m, l), order = 'F')
                    bz.append(tmp[:,:,lev])
                print ('%i outs in %s ' % (counter, datafile))
                counter += 1
            except:
                break
        self.time = N.arange(counter)*dt*dplot

        uf = N.array(u) 
        vf = N.array(v)
        wf = N.array(w) 
        thef = N.array(the) 
        Pf = N.array(P) 
        del u,v,w,the,P

        if MHD == 1:
            bxf = N.array(bx)
            byf = N.array(by)
            bzf = N.array(bz)
            del bx,by,bz

        self.u  =  uf[0:counter,...]
        self.v  =  vf[0:counter,...]
        self.w  =  wf[0:counter,...]
        self.the = thef[0:counter,...]
        self.p = Pf[0:counter,...]
        if MHD == 1:
            self.bx  =  bxf[0:counter,...]
            self.by  =  byf[0:counter,...]
            self.bz  =  bzf[0:counter,...]

        print ('deleting unnecessary files')
        del uf,vf,wf,thef,Pf,tmp
        if MHD == 1:
            del bxf,byf,bzf

