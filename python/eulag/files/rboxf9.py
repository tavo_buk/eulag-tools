# read eulag strat: GG
from scipy.io import FortranFile
from eulag.files.grid import read_grid
from eulag.files.modules import filter3D
from eulag.files.eudyn import parameters
import numpy as N
import subprocess
import numpy as np

def read_f9(*args, **kwargs):
        """Read fort.9, short tape from eulag.
           call using
           import eulag as E
           f = read_f9()
           f will be an object with the variables

           Parameters:
           ifl = True:  filters the data
               = False: read without filtering
           start: = 0 (default) initializes the reading after 
                    n outputs                   
        """
        return Rbox(*args, **kwargs)

def read_block(f9,n,m,l,vtype='f'):
    for i in range(15):
        a = f9.read_reals(dtype='f').reshape((n,m,l),order='F')
    for i in range(26):
        b = f9.read_reals(vtype)
    a = f9.read_reals(dtype='f').reshape((n,m,l),order='F')
    f9.read_reals(vtype)
    for i in range(4):
        a = f9.read_reals(dtype='f').reshape((n,m,l),order='F')

def read_block_data(f9,n,m,l,u,v,w,ox,oy,oz,ox2,oy2,oz2,the,p,fx,fy,fz,ft,\
                   tke,chm,fchm,tmp,vtype='f'):
# hydro variables

    u.append(np.reshape(f9.read_reals(vtype),   (g.n, g.m, g.l), order = 'F'))
    v.append(np.reshape(f9.read_reals(vtype),   (g.n, g.m, g.l), order = 'F'))
    w.append(np.reshape(f9.read_reals(vtype),   (g.n, g.m, g.l), order = 'F'))
    ox.append(np.reshape(f9.read_reals(vtype),  (g.n, g.m, g.l), order = 'F'))
    oy.append(np.reshape(f9.read_reals(vtype),  (g.n, g.m, g.l), order = 'F'))
    oz.append(np.reshape(f9.read_reals(vtype),  (g.n, g.m, g.l), order = 'F'))
    ox2.append(np.reshape(f9.read_reals(vtype), (g.n, g.m, g.l), order = 'F'))
    oy2.append(np.reshape(f9.read_reals(vtype), (g.n, g.m, g.l), order = 'F'))                                         
    oz2.append(np.reshape(f9.read_reals(vtype), (g.n, g.m, g.l), order = 'F'))
    the.append(np.reshape(f9.read_reals(vtype), (g.n, g.m, g.l), order = 'F'))
    p.append(np.reshape(f9.read_reals(vtype),   (g.n, g.m, g.l), order = 'F'))
    fx.append(np.reshape(f9.read_reals(vtype),  (g.n, g.m, g.l), order = 'F'))
    fy.append(np.reshape(f9.read_reals(vtype),  (g.n, g.m, g.l), order = 'F'))
    fz.append(np.reshape(f9.read_reals(vtype),  (g.n, g.m, g.l), order = 'F'))
    ft.append(np.reshape(f9.read_reals(vtype),  (g.n, g.m, g.l), order = 'F'))
    for i in range(26):
        tmp = f9.read_reals(vtype)
    tke.append(np.reshape(f9.read_reals(vtype), (g.n, g.m, g.l), order = 'F'))
    tmp = f9.read_reals(vtype)
    chm.append(np.reshape(f9.read_reals(vtype), (g.n, g.m, g.l), order = 'F'))
    chm.append(np.reshape(f9.read_reals(vtype), (g.n, g.m, g.l), order = 'F'))
    fchm.append(np.reshape(f9.read_reals(vtype),(g.n, g.m, g.l), order = 'F'))
    fchm.append(np.reshape(f9.read_reals(vtype),(g.n, g.m, g.l), order = 'F'))

    return u,v,w,ox,oy,oz,ox2,oy2,oz2,the,p,fx,fy,fz,ft,tke,chm,fchm,tmp	

class Rbox(object):
    def __init__ (self, datafile='',vtype='f', ifl = True,  skip=1):
        # Read xaverages file
        # Datafile is the name of the xaverages file to be read
        # ifl can be False or True, default is True
        if (not datafile):
            datafile='fort.9'

        param = parameters()
        if param[3] == 0:         # For spherical simulations
            (n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver,\
                    nslice, nplot, nstore, nxaver, it, rb, rt) = param
        else:
            print ('This is a cartesian simulation.')
            (n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver, \
            nslice, nplot, nstore, nxaver, it) = param

        cmd = "grep 'MHD' define.h"
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        temp = process.communicate()[0]
        MHD = int(temp.split()[2])

        if MHD == 1:
            print ('MHD config.')
        else:
            print ('HD config.')

        ''' Defining variables '''
        tmp = 0.
        counter = 0
        u,v,w,ox,oy,oz,ox2,oy2,oz2,the,p,fx,fy,fz,ft,tke,chm,fchm = ([] for _ in range (18))
        if MHD == 1:
                pm,bx,by,bz,fbx,fby,fbz,bx1,by1,bz1,bxe,bye,bze = ([] for _ in range (13))

        ''' reading the file '''
        f9 = FortranFile(datafile,'r')

        while True:
            try:
                if (counter%skip != 0):
                    read_block(f9,n,m,l)
                    print('odd', counter)
                else:
                    u,v,w,ox,oy,oz,ox2,oy2,oz2,the,p,fx,fy,fz,ft,tke,chm,fchm,tmp = \
                        read_block_data(f9,n,m,l,u,v,w,ox,oy,oz,ox2,oy2,oz2,the,p,fx,\
                        fy,fz,ft,tke,chm,fchm,tmp)
                    print('data', counter)
                counter +=1
            except:
                break



        f9.close()

        self.time = N.arange(counter,step = skip)*dstore*dt
        print ('There are %i outputs in %s, only %i saved ' % (counter, datafile, len(N.array(u)[:,0,0,0])))
        print('Timestep =  %8.2f, saved at each %i times' % (dt, skip*dstore))
        print('Total time = %10.2f' % (dt*(len(N.array(u)[:,0,0,0])-1)*skip*dstore))

        u = N.array(u) 
        v = N.array(v)
        w = N.array(w) 
        ox = N.array(ox) 
        oy = N.array(oy) 
        oz = N.array(oz) 
        ox2= N.array(ox2)
        oy2= N.array(oy2) 
        oz2= N.array(oz2) 
        the = N.array(the) 
        p = N.array(p) 
        fx = N.array(fx)
        fy = N.array(fy) 
        fz = N.array(fz) 
        ft = N.array(ft) 
        if MHD == 1:
            pm = N.array(pm) 
            bx = N.array(bx)
            by = N.array(by)
            bz = N.array(bz)
            fbx = N.array(fbx)
            fby = N.array(fby)
            fbz = N.array(fbz)
        tke = N.array(tke) 
        chm = N.array(chm)
        chm = N.array(chm)
        fchm = N.array(fchm)
        fchm = N.array(fchm)

        if ifl:
            for i in range(len(u[:,0,0,0])):
                u[i,:,:,:] = filter3D(3, u[i, :, :, :], n, m, l, 1)
                v[i,:,:,:] = filter3D(3, v[i, :, :, :], n, m, l, 1)
                w[i,:,:,:] = filter3D(3, w[i, :, :, :], n, m, l, 0)
                if MHD == 1:
                    bx[i,:,:,:] = filter3D(3, bx[i, :, :, :], n, m, l, 1)
                    by[i,:,:,:] = filter3D(3, by[i, :, :, :], n, m, l, 1)
                    bz[i,:,:,:] = filter3D(3, bz[i, :, :, :], n, m, l, 0)
        else:
            print ('Filter OFF')

        self.u = u
        self.v = v
        self.w = w
        self.ox = ox
        self.oy = oy
        self.oz = oz
        self.ox2 = ox2
        self.oy2 = oy2
        self.oz2 = oz2
        self.the = the
        self.p = p
        self.fx = fx
        self.fy = fy
        self.fz = fz
        self.ft = ft
        if MHD == 1:
            self.pm = pm
            self.bx = bx
            self.by = by
            self.bz = bz
            self.fbx = fbx
            self.fby = fby
            self.fbz = fbz
        self.tke = tke
        self.chm = chm
        self.chm = chm
        self.fchm = fchm
        self.fchm = fchm
        self.tmp = tmp

