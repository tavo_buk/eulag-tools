import eulag as E
import numpy as N

def amflux(f, nn = 10, Prot = 7, MHD = False):
    # This function computes the angular momentum flux for the HD case by standard.
    # > f = E.read_f11();
    # > nn is the number of time steps used on mean;
    # > Prot ir the simulation's period of rotation;

    data = N.genfromtxt('strat.dat')
    rho_s = data[:, 5]
    g = E.read_grid()

    # Perturbations fields
    up = E.turb(f.u); vp = E.turb(f.v); wp = E.turb(f.w)
#    up = E.turb_tp(f.u); vp = E.turb_tp(f.v); wp = E.turb_tp(f.w)
    # Temporal and azimuthal mean on last nn time steps
    um = f.u[-nn::,...].mean(axis = (0, 1))
    vm = f.v[-nn::,...].mean(axis = (0, 1))
    wm = f.w[-nn::,...].mean(axis = (0, 1))
    ## Correlations
    vum = (vp*up)[-nn::,...].mean(axis = (0, 1))
    wum = (wp*up)[-nn::,...].mean(axis = (0, 1))

    Omega_0 = 2.*N.pi/(Prot*24.*3600.)
    umtot = um + Omega_0*g.x

    #### ANGULAR MOMENTUM FLUX ####
    # Componentes caused by Meridional Circulation
    Fmc_r = rho_s*g.x*umtot*wm
    Fmc_th = rho_s*g.x*umtot*vm
    # Componentes caused by small-scale correlations (Reynolds Stresses)
    Frs_r = rho_s*g.x*wum
    Frs_th = rho_s*g.x*vum

    i = N.ones((g.l))
    I, TH = N.meshgrid(i, g.th)
    Fmc_w = Fmc_r*N.sin(TH) + Fmc_th*N.cos(TH)
    Fmc_z = Fmc_r*N.sin(TH) - Fmc_th*N.cos(TH)

    Frs_w = Frs_r*N.sin(TH) + Frs_th*N.cos(TH)
    Frs_z = Frs_r*N.sin(TH) - Frs_th*N.cos(TH)

    # IF MHD IS TRUE
    if MHD:
        mu = 4.*N.pi*1.e-7
        # Perturbations fields
        bxp = E.turb(f.bx); byp = E.turb(f.by); bzp = E.turb(f.bz)
        # Temporal and azimuthal mean on last nn time steps
        bxm = f.bx[-nn::,...].mean(axis = (0, 1))
        bym = f.by[-nn::,...].mean(axis = (0, 1))
        bzm = f.bz[-nn::,...].mean(axis = (0, 1))
        ## Correlations
        bxbym = (bxp*byp)[-nn::,...].mean(axis = (0, 1))
        bxbzm = (bxp*bzp)[-nn::,...].mean(axis = (0, 1))

        # Componentes caused by Magnetic Tension
        Fmt_r = - g.x/mu*bxm*bzm
        Fmt_th = - g.x/mu*bxm*bym
        # Componentes caused by small-scale correlations (Maxwell Stresses)
        Fms_r = - g.x/mu*bxbzm
        Fms_th = - g.x/mu*bxbym

        i = N.ones((g.l))
        I, TH = N.meshgrid(i, g.th)
        Fmt_w = Fmt_r*N.sin(TH) + Fmt_th*N.cos(TH)
        Fmt_z = Fmt_r*N.sin(TH) - Fmt_th*N.cos(TH)

        Fms_w = Fms_r*N.sin(TH) + Fms_th*N.cos(TH)
        Fms_z = Fms_r*N.sin(TH) - Fms_th*N.cos(TH)

        return [Fmc_r, Fmc_th, Frs_r, Frs_th, Fmt_r, Fmt_th, Fms_r, Fms_th]
    else:
        return [Fmc_r, Fmc_th, Frs_r, Frs_th]

def amflux_xav(f, nn = 10, Prot = 7, MHD = False):
    # This function computes the angular momentum flux for the HD case by standard.
    # > f = E.rxav()
    # > nn is the number of time steps used on mean;
    # > Prot ir the simulation's period of rotation;

    data = N.genfromtxt('strat.dat')
    rho_s = data[:, 5]
    g = E.read_grid()

    # Perturbations fields
    up = E.turb_xav(f.u, f.u2); vp = E.turb_xav(f.v, f.v2); wp = E.turb_xav(f.w, f.w2)
    # Temporal and azimuthal mean on last nn time steps
    um = f.u[-nn::,...].mean(axis = 0)
    vm = f.v[-nn::,...].mean(axis = 0)
    wm = f.w[-nn::,...].mean(axis = 0)
    ## Correlations
    vum = (vp*up)[-nn::,...].mean(axis = 0)
    wum = (wp*up)[-nn::,...].mean(axis = 0)

    Omega_0 = 2.*N.pi/(Prot*24.*3600.)
    umtot = um + Omega_0*g.x

    #### ANGULAR MOMENTUM FLUX ####
    # Componentes caused by Meridional Circulation
    Fmc_r = rho_s*g.x*umtot*wm
    Fmc_th = rho_s*g.x*umtot*vm
    # Componentes caused by small-scale correlations (Reynolds Stresses)
    Frs_r = rho_s*g.x*wum
    Frs_th = rho_s*g.x*vum

    i = N.ones((g.l))
    I, TH = N.meshgrid(i, g.th)
    Fmc_w = Fmc_r*N.sin(TH) + Fmc_th*N.cos(TH)
    Fmc_z = Fmc_r*N.sin(TH) - Fmc_th*N.cos(TH)

    Frs_w = Frs_r*N.sin(TH) + Frs_th*N.cos(TH)
    Frs_z = Frs_r*N.sin(TH) - Frs_th*N.cos(TH)

    # IF MHD IS TRUE
    if MHD:
        mu = 4.*N.pi*1.e-7
        # Perturbations fields
        bxp = E.turb_xav(f.bx, f.bx2); byp = E.turb_xav(f.by, f.by2); bzp = E.turb_xav(f.bz, f.bz2)
        # Temporal and azimuthal mean on last nn time steps
        bxm = f.bx[-nn::,...].mean(axis = 0)
        bym = f.by[-nn::,...].mean(axis = 0)
        bzm = f.bz[-nn::,...].mean(axis = 0)
        ## Correlations
        bxbym = (bxp*byp)[-nn::,...].mean(axis = 0)
        bxbzm = (bxp*bzp)[-nn::,...].mean(axis = 0)

        # Componentes caused by Magnetic Tension
        Fmt_r = - g.x/mu*bxm*bzm
        Fmt_th = - g.x/mu*bxm*bym
        # Componentes caused by small-scale correlations (Maxwell Stresses)
        Fms_r = - g.x/mu*bxbzm
        Fms_th = - g.x/mu*bxbym

        i = N.ones((g.l))
        I, TH = N.meshgrid(i, g.th)
        Fmt_w = Fmt_r*N.sin(TH) + Fmt_th*N.cos(TH)
        Fmt_z = Fmt_r*N.sin(TH) - Fmt_th*N.cos(TH)

        Fms_w = Fms_r*N.sin(TH) + Fms_th*N.cos(TH)
        Fms_z = Fms_r*N.sin(TH) - Fms_th*N.cos(TH)

        return [Fmc_r, Fmc_th, Frs_r, Frs_th, Fmt_r, Fmt_th, Fms_r, Fms_th]
    else:
        return [Fmc_r, Fmc_th, Frs_r, Frs_th]

def iflux_r(flux):
    # Calculate the net flux at each radius
    g = E.read_grid()
    I = []
    for k in range(len(flux)):
       i = N.trapz(flux[k]*g.r*g.x, x = g.th, axis = 0)
       I.append(i)
    return I

def iflux_th(flux, ir):
    # Calculate the net flux through cones at each latitude
    g = E.read_grid()
    I = []
    for k in range(len(flux)):
       i = N.trapz((flux[k]*g.x)[:, ir], x = g.r[ir], axis = 1)
       I.append(i)
    return I

def iflux_cil(flux):
    # Calculate the net flux through cylinders
    g = E.read_grid()
    z = []
    z_o = N.unique(g.x)    
    for i in range(len(z_o)-1):
        if abs(z_o[i+1] - z_o[i]) <= 5.e-6:
            pass
        else:
            z.append(z_o[i+1])
    I = N.zeros(len(z))
    indx = 0
    for ez in z:
        counter = 0
        for i in range(g.m):
            for j in range(g.l):
                if (g.x[i,j] - ez) <= 5.e-6:
                    I[indx] += flux[i,j]
                    counter += 1
        I[indx] /= counter
        indx += 1
   
    return z, I

def torque(flux_th, flux_r): 
	# Calculates the divergence of a flux 
	g = E.read_grid() 
	fphi = N.zeros((g.n, g.m,g.l)); fth = N.zeros((g.n, g.m,g.l)); fr = N.zeros((g.n, g.m,g.l)) 
	for ip in range(g.n): 
		fth[ip, ...] = flux_th 
		fr[ip, ...] = flux_r 
	torque =  E.div(fphi, fth, fr, g.phi, g.th, g.r, coord = 'sph') 
	return -1.*torque.mean(axis = 0) 
