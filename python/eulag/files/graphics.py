from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib
from matplotlib.ticker import FormatStrFormatter
from matplotlib import rc
from scipy.ndimage.filters import gaussian_filter
import numpy as N
import matplotlib.pyplot as P
import eulag as E
#from mpl_toolkits.basemap import Basemap
from pylab import meshgrid
from numpy import pi, sin, cos
import cmocean
from mpl_toolkits.basemap import Basemap
################################################################################

# Choose Computer Modern Roman fonts by default
#matplotlib.rcParams['font.serif'] = 'cmr10'
#matplotlib.rcParams['font.sans-serif'] = 'cmr10'

# PLOT STYLE #

matplotlib.rcParams['font.family'] = 'serif'
matplotlib.rcParams['font.serif'] = 'sans-serif'

font = { 'size' : 20}
rc('font', **font)
rc('xtick', labelsize = 20)
rc('ytick', labelsize = 20)

legend = {'fontsize': 18, 'numpoints' : 3}
rc('legend',**legend)
axes = {'labelsize': 18, 'titlesize' : 20}
rc('axes', **axes)
rc('mathtext',fontset='cm')
rc('lines', linewidth = 2)
rc('patch', linewidth = .5)

#use this, but at the expense of slowdown of rendering
rc('text', usetex = True)
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]
################################################################################
# Define constants
year = 365.*24.*3600.
mu = 4*N.pi*1.e-7
################################################################################
def omega(f, nn = 40, name = '', lev = 0, Prot = 7., rad_core = False):
    # plot configured for ttauri star
    rc('lines', linewidth = 1)

    from scipy.integrate import simps
    g = E.read_grid()

    cut = 5. # Degrees to be cut off
    good = int(cut*g.m/180.)

    # Temporal and azimuthal means
    if len(f.u.shape) == 4: # reading fort.11 file
        um = f.u[-nn::, ...].mean(axis = (0, 1))
        vm = f.v[-nn::, ...].mean(axis = (0, 1))
        wm = f.w[-nn::, ...].mean(axis = (0, 1))

    elif len(f.u.shape) == 3: # reading xaverages file
        um = f.u[-nn::, ...].mean(axis = 0)
        vm = f.v[-nn::, ...].mean(axis = 0)
        wm = f.w[-nn::, ...].mean(axis = 0)

    # PLOT
    if (good < 2):
            good = 2
    theta_good = N.linspace(0+(cut/90)*pi/2., (1 - cut/180)*pi, num = g.m - 2*good)

    R, TH = meshgrid(g.r, theta_good)
    X = R*sin(TH)
    Y = R*cos(TH)

    Omega_0 = 2.*pi/(Prot*24.*3600.)
    Omega = (um[good:-good, :]/X)*1.e9/(2*pi) #Add a factor  #+ Omega_0

    fig, ax = P.subplots(nrows = 1, ncols = 1, facecolor = None, edgecolor = None, linewidth = None)

    matplotlib.rcParams['contour.negative_linestyle'] = 'solid'

    Omin = Omega.min()
    Omax = Omega.max()

    if abs(Omin) > abs(Omax):
        Omax = Omax*abs(Omin)/abs(Omax)
    else:
        Omin = Omin*abs(Omax)/abs(Omin)
    level = N.linspace(Omin, Omax, 50)
    if lev != 0:
        level = N.linspace(-lev, lev, 50)
    cs = ax.contourf(X, Y, Omega, levels = level, interpolation = 'bicubic', cmap = P.get_cmap("RdBu"), extend = 'both')
    ax.contour(X, Y, Omega, levels = N.linspace(Omin, .5*Omax, 7), interpolation = 'bicubic', colors = "k")
#    ax.contour(X, Y, Omega, levels = (0., 0.40*Omax, 0.60*Omax, 0.75*Omax, 0.85*Omax), interpolation = 'bicubic', colors = "k")

    # Add colorbar
    divider = make_axes_locatable(ax)
    cax = fig.add_axes([0.3, 0.35, 0.02, 0.3])

    tick = N.linspace(level[0], level[-1], num = 2, endpoint = True)
    cbar = fig.colorbar(cs, cax = cax, ticks = tick, ticklocation = 'left', format = '%1.0f')

    cbar.ax.set_ylabel(r"$\Omega/2\pi (\mathrm{nHz})$")
    #cbar.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 2, length = 6)
    cbar.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 0, length = 6)

    # Make sure aspect ratio preserved
    ax.set_aspect('equal')

    # Turn off rectangular frame.
    ax.set_frame_on(False)

    # Turn off axis ticks.
    ax.set_xticks([])
    ax.set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
    ax.plot(rmax*sin(g.th), rmax*cos(g.th),'k')
    ax.plot(rmin*sin(g.th), rmin*cos(g.th),'k')
    ax.plot(g.r*sin(min(g.th)), g.r*cos(min(g.th)), 'k')
    ax.plot(g.r*sin(max(g.th)), g.r*cos(max(g.th)), 'k')

    # Draw a circle in the rad-conv interface
    if rad_core:
        ax.plot(3.3 * g.r[0] * sin(g.th), 3.3 * g.r[0] * cos(g.th), '--k')
#        ax.plot(0.7 * 6.96e8 * sin(g.th), 0.7 * 6.96e8 * cos(g.th), '--k')
    P.savefig(name+'omega'+'.png', bbox_inches='tight')
    P.show()

################################################################################
def comp_stream(fy, fz):
    from scipy.integrate import cumtrapz
    g = E.read_grid()
    R, TH = meshgrid(g.r, g.th[2:-2])
    X = R*N.sin(TH)
    fz = fz[2:-2,:]
    dth = g.th[-1]-g.th[-2]; dr = g.r[-1] - g.r[-2]
    chi = g.r**2 * cumtrapz(y = (fz*N.sin(TH)), x = g.th[2:-2], dx = dth, axis = 0, initial = 0)
    return chi/X

def merid_b(f, name = '', adjust_color = (1.), rad_core = False):
    from scipy.integrate import simps

    rc('lines', linewidth = 1)
    g = E.read_grid()
    g.rf = g.r/6.07751e8

    cut = 5. # Degrees to be cut off
    good = int(cut*g.m/180.)
    convert = 10. # 10 to kilo gauss and 1 to tesla
    # Converting units and taking azimuthal mean
    if len(f.u.shape) == 4: # reading fort.11 file
        bxm = f.bx[-1, ...].mean(axis = 0)*convert
        bym = f.by[-1, ...].mean(axis = 0)*convert
        bzm = f.bz[-1, ...].mean(axis = 0)*convert

    elif len(f.u.shape) == 3: # reading xaverages file
        bxm = f.bx[-1, ...]*convert
        bym = f.by[-1, ...]*convert
        bzm = f.bz[-1, ...]*convert

    # Stream Function
    stream = comp_stream(bym, bzm)

    # PLOT
    if (good < 2):
            good = 2
    theta_good = N.linspace(0+(cut/90)*pi/2., (1 - cut/180)*pi, num = g.m - 2*good)
    R, TH = meshgrid(g.r, theta_good)
    X = R*sin(TH)
    Y = R*cos(TH)

    bzm = bzm[good:-good,:]
    bym = bym[good:-good,:]
    bxm = bxm[good:-good,:]

    fig, ax = P.subplots(nrows = 1, ncols = 1, facecolor = None, edgecolor = None, linewidth = None)
    bxmin = bxm.min()
    bxmax = bxm.max()
    if abs(bxmin) > abs(bxmax):
        bxmax = bxmax*abs(bxmin)/abs(bxmax)
    else:
        bxmin = bxmin*abs(bxmax)/abs(bxmin)
    levels = N.linspace(bxmin, bxmax, 50)*adjust_color
    cs = ax.contourf(X, Y, bxm, levels = levels, interpolation = 'bicubic', cmap = P.get_cmap("RdBu"))

    matplotlib.rcParams['contour.negative_linestyle'] = 'dashed'

    #  Stream
    smin = (stream).min()
    smax = (stream).max()
    print (smin, smax)
    lev = [smin, .7*smin, .4*smin, .2*smin, .03*smin, .1*smax,  .3*smax, .8*smax, smax]
    ax.contour(X, Y, (stream), levels = lev, colors = "k")

    # Add colorbar
    tick = N.linspace(levels[0], levels[-1], num = 2, endpoint = True)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("left", size = "3%", pad = .08)

    cbar = fig.colorbar(cs, cax = cax, ticks = tick, ticklocation = 'left', format = '%1.0f')
    cbar.ax.set_ylabel(r"$B_{\phi} (\mathrm{kG})$", fontsize = 25)
    cbar.ax.tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 6, width = 2, length = 6)

    # Make sure aspect ratio preserved
    ax.set_aspect('equal')

    # Turn off rectangular frame.
    ax.set_frame_on(False)

    # Turn off axis ticks.
    ax.set_xticks([])
    ax.set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
    ax.plot(rmax*sin(g.th), rmax*cos(g.th),'k')
    ax.plot(rmin*sin(g.th), rmin*cos(g.th),'k')
    ax.plot(g.r*sin(min(g.th)), g.r*cos(min(g.th)), 'k')
    ax.plot(g.r*sin(max(g.th)), g.r*cos(max(g.th)), 'k')

    # Draw a circle in the rad-conv interface
    if rad_core:
        ax.plot(3.3 * g.r[0] * sin(g.th), 3.3 * g.r[0] * cos(g.th), '--k')
    P.savefig(name+'merid_b'+'.png')
    P.show()


################################################################################
def merid(f, nn = 40, name = '', lev = 0., rad_core = False):
    from scipy.integrate import simps
    rc('lines', linewidth = 1)
    g = E.read_grid()

    cut = 5. # Degrees to be cut off
    good = int(cut*g.m/180.)

    # Temporal and azimuthal means
    if len(f.u.shape) == 4: # reading fort.11 file
        um = f.u[-nn::, ...].mean(axis = (0, 1))
        vm = f.v[-nn::, ...].mean(axis = (0, 1))
        wm = f.w[-nn::, ...].mean(axis = (0, 1))

    elif len(f.u.shape) == 3: # reading xaverages file
        um = f.u[-nn::, ...].mean(axis = 0)
        vm = f.v[-nn::, ...].mean(axis = 0)
        wm = f.w[-nn::, ...].mean(axis = 0)

    # Stream Function
    stratification = N.genfromtxt('strat.dat')    # PLOT
    rho_ad = stratification[:, 5] # Isentropic density
    stream = comp_stream(rho_ad*vm, rho_ad*wm)

    # PLOT
    if (good < 2):
            good = 2
    theta_good = N.linspace(0+(cut/90)*pi/2., (1 - cut/180)*pi, num = g.m - 2*good)
    R, TH = meshgrid(g.r, theta_good)
    X = R*sin(TH)
    Y = R*cos(TH)

    wm = wm[good:-good,:]
    vm = vm[good:-good,:]
    um = um[good:-good,:]

    fig, ax = P.subplots(nrows = 1, ncols = 1, facecolor = None, edgecolor = None, linewidth = None)
    Vmax = N.abs(vm).max()
    levels = N.linspace(-Vmax, Vmax, 64)
    if lev != 0:
        levels = N.linspace(-lev, lev, 50)
    cs = ax.contourf(X, Y, vm, levels = levels, interpolation = 'bicubic', cmap = P.get_cmap("RdYlGn"), extend = "both")

    matplotlib.rcParams['contour.negative_linestyle'] = 'dashed'

    #  Stream
    smin = (stream).min()
    smax = (stream).max()
#    cs = ax.contourf(X, Y, stream, levels = N.linspace(smin, smax, 64)*adjust_color, interpolation = 'bicubic', cmap = P.get_cmap("RdYlGn"))
    #lev = (.8*smin, .6*smin, .25*smin, abs(.3*smax), abs(.6*smax), abs(.9*smax))
    levmin = N.linspace(0.5*smin, 0.07*smin, 3)
    levmax = N.linspace(0.07*smax, 0.5*smax, 3)
    ax.contour(X, Y, (stream), levels = levmin, colors = "k", linewidth = 0.8)
    ax.contour(X, Y, (stream), levels = levmax, colors = "k", linewidth = 0.8)

    # Add colorbar
    tick = N.linspace(smin, smax, num = 2, endpoint = True)
    divider = make_axes_locatable(ax)
    cax = fig.add_axes([0.3, 0.35, 0.02, 0.3])

    cbar = fig.colorbar(cs, cax = cax, ticks = [levels[0], levels[-1]], ticklocation = 'left', format = '%1.0f')
    cbar.ax.set_ylabel(r"$v_{\theta} (\mathrm{m/s})$")
#    cbar = fig.colorbar(cs, cax = cax, ticks = [tick[0], tick[-1]], ticklocation = 'left', format = '%1.0f')
#    cbar.ax.set_yticklabels(['CCW', 'CW'])
    cbar.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 0, length = 6)

    # Make sure aspect ratio preserved
    ax.set_aspect('equal')

    # Turn off rectangular frame.
    ax.set_frame_on(False)

    # Turn off axis ticks.
    ax.set_xticks([])
    ax.set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
    ax.plot(rmax*sin(g.th), rmax*cos(g.th),'k')
    ax.plot(rmin*sin(g.th), rmin*cos(g.th),'k')
    ax.plot(g.r*sin(min(g.th)), g.r*cos(min(g.th)), 'k')
    ax.plot(g.r*sin(max(g.th)), g.r*cos(max(g.th)), 'k')

    # Draw a circle in the rad-conv interface
    if rad_core:
        ax.plot(3.3 * g.r[0] * sin(g.th), 3.3 * g.r[0] * cos(g.th), '--k')
#        ax.plot(0.7 * 6.96e8 * sin(g.th), 0.7 * 6.96e8 * cos(g.th), '--k')
    P.savefig(name+'merid'+'.png', bbox_inches='tight')
    P.show()


################################################################################

def merid_simple(field1,lev1,field2,lev2,time,i, name = '', adjust_color = (1.), rad_core = False):
    g = E.read_grid()
    R, TH = meshgrid(g.rf, g.th)
    X = R*sin(TH)
    Y = R*cos(TH)
    fig, ax = P.subplots(nrows = 1, ncols = 1, facecolor = None, edgecolor = None, linewidth = None)
    lp = N.logspace(N.log10(0.001) , N.log10(lev1),64)*adjust_color
    ln = -lp[::-1]
    levels1= ln+lp

    cs = ax.contourf(X, Y, field1, levels = levels1, interpolation = 'bicubic', cmap = P.get_cmap("PuOr"))

    # Add colorbar
    tick = N.linspace(levels1[0], levels1[-1], num = 2, endpoint = True)
    divider = make_axes_locatable(ax)

#    cax = divider.append_axes("left", size = "4%", pad = 0.1)
    cax = fig.add_axes([0.45, 0.35, 0.015, 0.3])
    cbar = fig.colorbar(cs, cax = cax, ticks = tick, ticklocation = 'left', format = '%1.0f')
    cbar.ax.set_ylabel(r"$B_{\phi} \rm [T]$", fontsize = 25)
    cbar.ax.tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 6, width = 2, length = 6)

    cs = ax.contour(X, Y, field2, 11, colors='k')
    # Make sure aspect ratio preserved
    ax.set_aspect('equal')

    # Turn off rectangular frame.
    ax.set_frame_on(False)

    # Turn off axis ticks.
    ax.set_xticks([])
    ax.set_yticks([])

    # writing the time
    tt = '%.1f' % (time)
    ax.text(0.2,1.05,r"$t = $"+tt+r" \rm yr ")

    # Draw a circle around the edge of the plot.
    rmax = max(g.rf)
    rmin = min(g.rf)
    ax.plot(rmax*sin(g.th), rmax*cos(g.th),'k')
    ax.plot(rmin*sin(g.th), rmin*cos(g.th),'k')
    ax.plot(g.rf*sin(min(g.th)), g.rf*cos(min(g.th)), 'k')
    ax.plot(g.rf*sin(max(g.th)), g.rf*cos(max(g.th)), 'k')

    print ('printing figure = ', i)
    fig_name = "bfield"+"%03d" % i+".png"
    P.savefig(fig_name,dpi=300)
    P.close(fig)

################################################################################
def longw(f, name = '', lev = 0, rad_core = False):
    # plot configured for ttauri star
    from scipy.integrate import simps
    rc('lines', linewidth = 1)
    g = E.read_grid()

    # plot
    R, TH = meshgrid(g.r, g.phi)
    X = R*sin(TH)
    Y = R*cos(TH)

    fig, ax = P.subplots(nrows = 1, ncols = 1, facecolor = None, edgecolor = None, linewidth = None)

    matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
    w = f.w[-5, :, 31, :]
    wmin = w.min()
    wmax = w.max()

    if abs(wmin) > abs(wmax):
        wmax = wmax*abs(wmin)/abs(wmax)
    else:
        wmin = wmin*abs(wmax)/abs(wmin)
    level = N.linspace(wmin, wmax, 50)
    if lev != 0:
        level = N.linspace(-lev, lev, 50)

    cs = ax.contourf(X, Y, w, levels = level, interpolation = 'bicubic', cmap = P.get_cmap("RdGy"), extend = 'both')

    # Add colorbar
    tick = N.linspace(level[0], level[-1], num = 2, endpoint = True)
    cbar = P.colorbar(cs, ticks = tick, format = '%1.0f', orientation = 'horizontal', fraction = 0.03, pad = 0.08)
    cbar.ax.set_xlabel(r"$v_\mathrm{r} (\mathrm{m/s})$", fontsize = 25) 
    cbar.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 2, length = 6, labelsize = 20) 

    # Make sure aspect ratio preserved
    ax.set_aspect('equal')

    # Turn off rectangular frame.
    ax.set_frame_on(False)

    # Turn off axis ticks.
    ax.set_xticks([])
    ax.set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
    ax.plot(rmax*sin(g.phi), rmax*cos(g.phi),'k')
    ax.plot(rmin*sin(g.phi), rmin*cos(g.phi),'k')

    # Draw a circle in the rad-conv interface
    if rad_core:
         ax.plot(3.3 * g.r[0] * sin(g.phi), 3.3 * g.r[0] * cos(g.phi), '--k')
    P.tight_layout()
    P.savefig(name + '-long_w.png', bbox_inches='tight')
    P.show()
    return None
################################################################################
def B_moll_rf(bz, lev, name = '', name2 = ''):
    # bz is the radial field restored from spherical harmonics decomposition;
    # lev defines the magnetic field levels, given in gauss
    g = E.read_grid()
    bz *= 1.e1 # Converting tesla to gauss
    level = N.linspace(-lev, lev, 50)
    PHI, TH = N.meshgrid(g.phi - N.pi, g.th - N.pi/2.)
    RAD = 180./N.pi
    # Create figure
    maps = Basemap(projection = 'moll', lon_0 = 0, resolution = 'c')
    cs = maps.contourf(PHI*RAD, TH*RAD, N.rot90(bz.T, 1), levels = level, cmap = cmocean.cm.balance_r, latlon = True, extend = 'both')
    # Colorbar
    tick = N.linspace(level[0], level[-1], num = 2, endpoint = True)
    cbar = P.colorbar(cs, ticks = tick, format = '%1.0f', orientation = 'horizontal', fraction = 0.03, pad = 0.08)
    cbar.ax.set_xlabel(r"$B_\mathrm{r} (\mathrm{kG})$", fontsize = 25)
    cbar.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 2, length = 6,  labelsize = 20) 
    P.tight_layout()
    P.savefig(name+'-Bmoll'+name2+'.png', bbox_inches='tight')
    P.close()
    return None

def B_moll(bz, lev, name = ''):
    # bz is the radial field;
    # lev defines the magnetic field levels, given in gauss
    g = E.read_grid()
    bz *= 1.e1 # Converting tesla to gauss
    level = N.linspace(-lev, lev, 50)
    PHI, TH = N.meshgrid(g.phi - N.pi, g.th - N.pi/2.)
    RAD = 180./N.pi
    # Create figure
    maps = Basemap(projection = 'moll', lon_0 = 0, resolution = 'c')
    cs = maps.contourf(PHI*RAD, TH*RAD, N.rot90(bz, 1), levels = level, cmap = cmocean.cm.balance_r, latlon = True, extend = 'both')
    # Colorbar
    tick = N.linspace(level[0], level[-1], num = 2, endpoint = True)
    cbar = P.colorbar(cs, ticks = tick, format = '%1.0f', orientation = 'horizontal', fraction = 0.03, pad = 0.08)
    cbar.ax.set_xlabel(r"$B_\mathrm{r} (\mathrm{kG})$", fontsize = 25)
    cbar.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 2, length = 6, labelsize = 20) 
    P.tight_layout()
    P.savefig(name+'-Bmoll.png', bbox_inches='tight')
    P.close()
    return None

################################################################################
def mollweide(f, rstar, wir, name = '', lev = 0):
    # Inputs:
    #       > f = E.read_f11()
    #       > rstar is the stars radius
    #       > wir is the fractional radius to be plotted
    from mpl_toolkits.basemap import Basemap
    g = E.read_grid()
    g.rf = g.r/rstar
    dr = g.rf[1] - g.rf[0]
    ir = N.where((g.rf > wir - dr) & (g.rf < wir + dr))[0][0]
    w = f.w[-5, :, : , ir]
    wmin = w.min()
    wmax = w.max()
    if abs(wmin) > abs(wmax):
        wmax = wmax*abs(wmin)/abs(wmax)
    else:
        wmin = wmin*abs(wmax)/abs(wmin)
    level = N.linspace(wmin, wmax, 50)
    if lev!= 0:
        level = N.linspace(-lev, lev, 50)
     
    PHI, TH = N.meshgrid(g.phi - N.pi, g.th - N.pi/2.)
    RAD = 180./N.pi
    # Create figure
    maps = Basemap(projection = 'moll', lon_0 = 0, resolution = 'c', rsphere = ir)
    cs = maps.contourf(PHI*RAD, TH*RAD, N.rot90(w, 1), levels = level, cmap = P.get_cmap("RdGy"), latlon = True, extend = 'both')
    # Colorbar
    tick = N.linspace(level[0], level[-1], num = 2, endpoint = True)
    cbar = P.colorbar(cs, ticks = tick, format = '%1.0f', orientation = 'horizontal', fraction = 0.03, pad = 0.08)
    cbar.ax.set_xlabel(r"$v_\mathrm{r} (\mathrm{m/s})$" %(g.rf[ir]), fontsize = 25)
    cbar.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 2, length = 6, labelsize = 20) 
    P.tight_layout()
    P.savefig(name + 'moll_%2.0f' %(g.rf[ir]*100) + '.png', bbox_inches = 'tight')
    P.show()
    return None
################################################################################
def et(ke, drke, mcke, cke, time, ymin = 1., ymax = 1.e7, name = ''):
    g = E.read_grid()
    P.plot(time, ke  , 'k'  , label = r'$KE$'  , linewidth = 1)
    P.plot(time, drke, 'g--', label = r'$TKE$', linewidth = 1)
    P.plot(time, mcke, 'r--', label = r'$PKE$', linewidth = 1)
    P.plot(time, cke , 'b--', label = r'$FKE$' , linewidth = 1)
    P.yscale('log')
    P.xlabel(r'$t \; \mathrm{(yr)}$')
    P.ylabel(r'$\mathrm{(J/m^3)}$')
    P.xlim([0., time.max()])
    P.ylim([ymin, ymax])
    ttick = N.arange(0, time.max(), 5)
    P.xticks(ttick)
    P.legend(bbox_to_anchor=(0., 1.01, 1., .08), handlelength = 1.5, loc = 3, ncol = 4, mode = "expand", borderaxespad = 0., shadow = False)
    P.subplots_adjust(bottom = .13)
    P.savefig(name+'.png')
    P.show()
################################################################################
def et_MHD(me, ke, time, ymin = 1., ymax = 1.e7, name = ''):
    g = E.read_grid()
    P.plot(time, ke, 'orange'  , label = r'$KE$')
    P.plot(time, me, 'k', label = r'$ME$')
    P.yscale('log')
    P.xlabel(r'$t \; \mathrm{(yr)}$')
    P.ylabel(r'$\mathrm{(J/m^3)}$')
    P.xlim([0., time.max()])
    P.ylim([ymin, ymax])
    ttick = N.arange(0, time.max(), 5)
    P.xticks(ttick)
    P.legend()#bbox_to_anchor=(0., 1.01, 1., .08), handlelength = 1.5, loc = 3, ncol = 2, mode = "expand", borderaxespad = 0., shadow = False)
    P.subplots_adjust(bottom = .13)
    P.savefig(name+'.png')
    P.show()
################################################################################
def energy_r(me_r, fme_r, pme_r, tme_r, rstar, ymin = 1.e7, ymax = 1.e8, name = '', loc = 'upper right'):
    g = E.read_grid()
    g.rf = g.r/rstar
    P.plot(g.rf, me_r,  'k', label = r'$ME$', linewidth = 1)
    P.plot(g.rf, tme_r,  '--g', label = r'$TME$', linewidth = 2)
    P.plot(g.rf, pme_r,  'r--', label = r'$PME$', linewidth = 2)
    P.plot(g.rf, fme_r,  '--b', label = r'$FME$', linewidth = 2)
    P.xlabel(r'$r \; (\mathrm{R_s})$')
    P.ylabel(r'$\mathrm{\left(J/m^3\right)}$')
    P.xlim([0.1, .95])
    P.ylim([ymin, ymax])
    P.yscale('log')
    #P.ticklabel_format(style = 'scientific', scilimits = (0,0), axis = 'y')
    P.legend(bbox_to_anchor=(0., 1.01, 1., .08), handlelength = 1.5, loc = 3, ncol = 4, mode = "expand", borderaxespad = 0., shadow = False)
    P.subplots_adjust(bottom = .13)
    P.savefig(name+'.png')
    P.show()
################################################################################
def energy_harmonics(energy, name):
    ############################# ATENCAO ##########################################
    P.bar(range(11), energy/energy.sum())
    P.xlabel(r'l')
    P.ylabel('Fractional Magnetic Energy ')
    P.xticks([0,1,2,3,4,5,6,7,8,9,10])
    P.xlim([0,10.5])
    fig_name = name + 'energy'+'lmax10' + '.eps'
    P.tight_layout()
    P.savefig(fig_name)
    P.show()
    return None
################################################################################
def butterfly(f, name = '', adjust_color = (1.,1.,1.,1.)):
    font = { 'size' : 18}
    rc('font', **font)
    rc('xtick', labelsize = 16)
    rc('ytick', labelsize = 16)
    
    legend = {'fontsize': 16, 'numpoints' : 3}
    rc('legend',**legend)
    axes = {'labelsize': 16, 'titlesize' : 18}
    rc('axes', **axes)
    ## Change units Tesla to kilo Gauss
    bxm = f.bx*1.e1
    bzm = f.bz*1.e1

    theta = N.linspace(-90, 90, num = bxm.shape[1], endpoint = True)
    g = E.read_grid()
    r = g.rf
    outputs_f11 = bxm.shape[0]
    time = f.time/year	# Time in years
    ttime = N.arange(0, time[-1], 5)
    fig, axes = P.subplots(nrows = 2, ncols = 2, facecolor = None, edgecolor = None, linewidth = None, sharex = True)

    ir1 = -3 # Fixed radius in butterfly diagram
    bx_min = bxm[:, :, ir1].min()
    bx_max = bxm[:, :, ir1].max()
    if abs(bx_min) > abs(bx_max):
        bx_max = bx_max*abs(bx_min)/abs(bx_max)
    else:
        bx_min = bx_min*abs(bx_max)/abs(bx_min)
    levels1 = N.linspace(bx_min, bx_max, 60, endpoint = True)*adjust_color[0]
    im1 = axes[0,0].contourf(time, theta, N.rot90(bxm[:,:,ir1]), levels = levels1 , interpolation = 'bicubic', cmap = cmocean.cm.balance_r, extend='both')  # atencao para a rotacao necessaria na matriz
    axes[0,0].set_ylabel(r'$90^{\circ} - \theta$')
    axes[0,0].set_yticks([90, 45, 0, -45, -90])
    axes[0,0].set_xticks(ttime)
    axes[0,0].set_yticklabels([r"$90^{\circ}$",r"$45^{\circ}$",r"$0^{\circ}$",r"$-45^{\circ}$",r"$-90^{\circ}$"])
    #axes[0,0].tick_params(which = 'major', direction = 'inout', labelsize = 15, pad = 10, width = 2, length = 6)
    axes[0,0].tick_params(which = 'major', direction = 'inout', pad = 3, width = 2, length = 4)
    axes[0,0].set_title(r'$ B_{\phi} \mathrm{(kG)}$')

    divider1 = make_axes_locatable(axes[0,0])
    cax1 = divider1.append_axes("right", size = "3%", pad = 0.08)

    tick1 = N.linspace(bx_min, bx_max, 2)*adjust_color[0]
    cbar_ax1 = fig.colorbar(im1, ticks = tick1, cax = cax1, format = '%1.0f')#, label = r"$ B_{\phi} \mathrm{[kG]}$")
    #cbar_ax1.ax.tick_params(which = 'major', direction = 'inout', labelsize = 15, pad = 6, width = 2, length = 6)
    cbar_ax1.ax.tick_params(which = 'major', direction = 'inout', pad = 3, width = 2, length = 4)

    ###################################################################################
    br_min = bzm[:, :, ir1].min()
    br_max = bzm[:, :, ir1].max()

    if abs(br_min) > abs(br_max):
        br_max = br_max*abs(br_min)/abs(br_max)
    else:
        br_min = br_min*abs(br_max)/abs(br_min)

    levels2 = N.linspace(br_min, br_max, 60, endpoint = True)*adjust_color[1]
    im2 = axes[0,1].contourf(time, theta, N.rot90(bzm[:, :, ir1]), levels = levels2 , interpolation = 'bicubic', cmap = cmocean.cm.balance_r, extend='both')
    axes[0,1].set_yticks([90, 45, 0, -45, -90])
    #axes[0,1].set_yticklabels([r"$90^{\circ}$",r"$45^{\circ}$",r"$0^{\circ}$",r"$-45^{\circ}$",r"$-90^{\circ}$"])
    #axes[0,1].tick_params(which = 'major', direction = 'inout', labelsize = 15, pad = 10, width = 2, length = 6)
    axes[0,1].tick_params(which = 'major', direction = 'inout', pad = 3, width = 2, length = 4)
    axes[0,1].set_title(r'$ B_\mathrm{r} \mathrm{(kG)}$')

    divider2 = make_axes_locatable(axes[0,1])
    cax2 = divider2.append_axes("right", size = "3%", pad = 0.08)

    tick2 = N.linspace(br_min, br_max, 2)*adjust_color[1]
    cbar_ax2 = fig.colorbar(im2, ticks = tick2, cax = cax2, format = '%1.0f')#, label = r"$ B_\mathrm{r} \mathrm{[kG]}$")
    #cbar_ax2.ax.tick_params(which = 'major', direction = 'inout', labelsize = 15, pad = 10, width = 2, length = 6)
    cbar_ax2.ax.tick_params(which = 'major', direction = 'inout', pad = 3, width = 2, length = 4)

    ###################################################################################
    nthe = N.linspace(90., -90., 64)
    jth1, = N.where((nthe > 15) & (nthe < 25))
    mth = jth1[0] # angulo theta para grafico do campo em funcao do raio e do tempo
    bx_minr = bxm[:, mth,:].min()
    bx_maxr = bxm[:, mth,:].max()
    if abs(bx_minr) > abs(bx_maxr):
        bx_maxr = bx_maxr*abs(bx_minr)/abs(bx_maxr)
    else:
        bx_minr = bx_minr*abs(bx_maxr)/abs(bx_minr)
    levels3 = N.linspace(bx_minr, bx_maxr, 60, endpoint = True)*adjust_color[2]
    im3 = axes[1,0].contourf(time, r, N.transpose(bxm[:, mth, :]), levels = levels3 , interpolation = 'bicubic', cmap = cmocean.cm.balance_r, extend='both')
    axes[1,0].set_yticks(N.linspace(.1, .95, 5, endpoint = True))
    axes[1,0].yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    axes[1,0].set_ylabel(r'$r \; (\mathrm{R_s})$', va = 'center', labelpad = 11)#, rotation = 'horizontal', fontsize = 30)
    axes[1,0].set_xlabel(r'$t \; \mathrm{(yr)}$')#, fontsize = 20)
    #axes[1,0].tick_params(which = 'major', direction = 'inout', labelsize = 15, pad = 10, width = 2, length = 6)
    axes[1,0].tick_params(which = 'major', direction = 'inout', pad = 3, width = 2, length = 4)

    divider3 = make_axes_locatable(axes[1,0])
    cax3 = divider3.append_axes("right", size = "3%", pad = 0.08)

    tick3 = N.linspace(bx_minr, bx_maxr, 2)*adjust_color[2]
    cbar_ax3 = fig.colorbar(im3, ticks = tick3, cax = cax3, format = '%1.0f')#, label = r"$ B_{\phi} \mathrm{[kG]}$")
    #cbar_ax3.ax.tick_params(which = 'major', direction = 'inout', labelsize = 15, pad = 10, width = 2, length = 6)
    cbar_ax3.ax.tick_params(which = 'major', direction = 'inout', pad = 3, width = 2, length = 4)

    ###################################################################################

    bz_minr = bzm[:, mth,:].min()
    bz_maxr = bzm[:, mth,:].max()
    if abs(bz_minr) > abs(bz_maxr):
        bz_maxr = bz_maxr*abs(bz_minr)/abs(bz_maxr)
    else:
        bz_minr = bz_minr*abs(bz_maxr)/abs(bz_minr)
    levels4 = N.linspace(bz_minr, bz_maxr, 60, endpoint = True)*adjust_color[3]
    im4 = axes[1,1].contourf(time, r, N.transpose(bzm[:, mth, :]), levels = levels4 , interpolation = 'bicubic', cmap = cmocean.cm.balance_r, extend='both')
    axes[1,1].set_yticks(N.linspace(.1, .95, 5, endpoint = True))
    axes[1,1].yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    axes[1,1].set_xlabel(r'$t \; \mathrm{(yr)}$')#, fontsize = 20)
    #axes[1,1].tick_params(which = 'major', direction = 'inout', labelsize = 15, pad = 10, width = 2, length = 6)
    axes[1,1].tick_params(which = 'major', direction = 'inout', pad = 3, width = 2, length = 4)

    divider4 = make_axes_locatable(axes[1,1])
    cax4 = divider4.append_axes("right", size = "3%", pad = 0.08)

    tick4 = N.linspace(bz_minr, bz_maxr, 2)*adjust_color[3]
    cbar_ax4 = fig.colorbar(im4, ticks = tick4, cax = cax4, format = '%1.0f')#, label = r"$ B_\mathrm{r} \mathrm{[kG]}$")
    #cbar_ax4.ax.tick_params(which = 'major', direction = 'inout', labelsize = 15, pad = 10, width = 2, length = 6)
    cbar_ax4.ax.tick_params(which = 'major', direction = 'inout', pad = 3, width = 2, length = 6)

    P.setp([axis.get_xticklabels() for axis in axes[0, :]], visible = False)
    P.setp([axis.get_yticklabels() for axis in axes[:, 1]], visible = False)
    fig.subplots_adjust(left = .16, right = .92, top = .9, bottom = .15, wspace = .25, hspace = .22)

    fig_name = name + 'butterfly'
#    P.tight_layout()
    P.savefig(fig_name + '.png', bbox_inches='tight')
    P.show()
    return Noyne

################################################################################

def butterfly_simple(f, lev1, lev2, tini = 0., tend = 1000., label= '', name = '', xlabel = False):
    bxm = gaussian_filter(f.bx,0.8)
    bzm = gaussian_filter(f.bz,0.8)

    P.rcParams['axes.linewidth'] = 1.3
    
    theta = N.linspace(-90, 90, num = bxm.shape[1], endpoint = True)
    g = E.read_grid()
    r = g.rf
    outputs_f11 = bxm.shape[0]
    time = f.time/year	# Time in years
    itg, = N.where((time >= tini) & (time <= tend) ) 

    fig, axes = P.subplots(nrows = 2, ncols = 1, facecolor = None, edgecolor = None, linewidth = 5, sharex = True)

    ir1 = 61 # Fixed radius in butterfly diagram
    levels1 = N.linspace(-lev1, lev1, 128, endpoint = True)
    im1 = axes[0].contour(time[itg], theta, N.rot90(bxm[itg,:,ir1]),8,colors='k') 
    im1 = axes[0].contourf(time[itg], theta, N.rot90(bzm[itg,:,ir1]*1.e2), levels = levels1 , cmap=cmocean.cm.balance_r, extend='both')  
    axes[0].text(tini,1.2*theta[-1],label) 
    axes[0].set_rasterization_zorder(-10)
    axes[0].set_ylabel(r'$90^{\circ} - \theta \; (^{\circ}) $',fontweight='bold')
    axes[0].set_yticks([90, 45, 0, -45, -90])
#   axes[0].set_xticks(N.linspace(time[itg[0]], time[itg[-1]], 5, endpoint = True))
    axes[0].set_xticks(N.linspace(tini, tend, 5, endpoint = True))
    axes[0].xaxis.set_major_formatter(FormatStrFormatter('$%3.0d$'))
    axes[0].set_yticklabels([r"$90^{\circ}$",r"$45^{\circ}$",r"$0^{\circ}$",r"$-45^{\circ}$",r"$-90^{\circ}$"])
    axes[0].tick_params(which = 'major', direction = 'inout', labelsize = 15, pad = 10, width = 3, length = 6)

    divider1 = make_axes_locatable(axes[0])
    cax1 = divider1.append_axes("right", size = "2%", pad = 0.2)
    tick1 = N.linspace(-lev1, lev1, 3)
    cbar_ax1 = fig.colorbar(im1, ticks = tick1, cax = cax1, format = '%2.1f', label = r"$ B_{\mathrm{r}} \mathrm{[T]} \times 10^{2}$")
    cbar_ax1.ax.tick_params(which = 'major', direction = 'inout', labelsize = 15, pad = 6, width = 2, length = 6)
    cbar_ax1.ax.set_rasterization_zorder(-10)

    ###################################################################################

    nthe = N.linspace(90., -90., 64)
    jth1, = N.where((nthe > 15) & (nthe < 25))
    mth = jth1[0] # angulo theta para grafico do campo em funcao do raio e do tempo
    levels2 = N.linspace(-lev2, lev2, 128, endpoint = True)
    im3 = axes[1].contour(time[itg], r, N.transpose(bxm[itg, mth, :]), 10, colors='k')
    im3 = axes[1].contourf(time[itg], r, N.transpose(bzm[itg, mth, :]*1.e2), levels = levels2 , cmap=cmocean.cm.balance_r ,extend='both')
    axes[1].set_rasterization_zorder(-10)
    axes[1].set_yticks(N.linspace(.61, .96, 4, endpoint = True))
#   axes[1].set_xticks(N.linspace(time[itg[0]], time[itg[-1]], 5, endpoint = True))
    axes[1].set_xticks(N.linspace(tini, tend, 5, endpoint = True))
    axes[1].yaxis.set_major_formatter(FormatStrFormatter('$%.2f$'))
    axes[1].xaxis.set_major_formatter(FormatStrFormatter('$%3.0d$'))
    axes[1].set_ylabel(r'$r \; [R_{\odot}] $', fontsize = 20, va = 'center', labelpad = 15)
    if xlabel == True:
        axes[1].set_xlabel(r'$ t \; \mathrm{[yr]} $', fontsize = 20, va = 'center', labelpad = 15)
    axes[1].tick_params(which = 'major', direction = 'inout', labelsize = 15, pad = 10, width = 2, length = 6)

    divider3 = make_axes_locatable(axes[1])
    cax3 = divider3.append_axes("right", size = "2%", pad = 0.2)
    tick3 = N.linspace(-lev2, lev2, 3)
    cbar_ax3 = fig.colorbar(im3, ticks = tick3, cax = cax3, format = '%2.1f', label = r"$ B_{\mathrm{r}} \mathrm{[T]} \times 10^{2}$")
    cbar_ax3.ax.tick_params(which = 'major', direction = 'inout', labelsize = 15, pad = 10, width = 2, length = 6)
    cbar_ax3.ax.set_rasterization_zorder(-10)

    ###################################################################################
    fig_name = 'but'+ name + '.png'
    fig.tight_layout()
    import os
    os.chdir('../')
    fig.savefig(fig_name, bbox_inches='tight')
    return None

    ###################################################################################

def ts(f):
    # the f needs to come from E.rxav()
    def deviation(ob, ob2):
        return N.mean((ob2 - N.multiply(ob, ob)), axis = (1, 2))

    # Turbulent Velocity
    du2 = deviation(f.u, f.u2)
    dv2 = deviation(f.v, f.v2)
    dw2 = deviation(f.w, f.w2)

    f.urms = N.power(du2 + dv2 + dw2, 0.5)

    # Magnetic field of equipartition
    dbx2 = deviation(f.bx, f.bx2)
    dby2 = deviation(f.by, f.by2)
    dbz2 = deviation(f.bz, f.bz2)

    f.brms = N.power(dbx2 + dby2 + dbz2, 0.5)

    fig = P.figure()
    sub1 = fig.add_subplot(2, 2, 1)
    sub1.set_title(r'$U_{rms}$')
    sub1.plot(f.time/year, f.urms)

    sub2 = fig.add_subplot(2, 2, 2)
    sub2.set_title(r'$\theta$')
    sub2.plot(f.time/year, f.the.mean(axis = (1, 2)))

    sub3 = fig.add_subplot(2, 2, 3)
    sub3.set_title(r'$ln \left(B_{rms}\right)$')
    sub3.plot(f.time/year, N.log(f.brms))

    sub4 = fig.add_subplot(2, 2, 4)
    sub4.plot(f.time/year, f.P.mean(axis = (1, 2)))
    sub4.set_yscale('log')
#    sub4.set_yscale('linear')
    sub4.set_title(r'$P$')
    P.tight_layout()
    P.show(False)
    return None
################################################################################
def plot_torque(F, label, adjust_color = 1., name = '', rad_core = False):
#    label defines which flux we are plotting
    rc('lines', linewidth = 1)

    # plot configured for ttauri star
    g = E.read_grid()

    cut = 5. # Degrees to be cut off
    good = int(cut*g.m/180.)

    # PLOT
    if (good < 2):
            good = 2
    theta_good = N.linspace(0+(cut/90)*pi/2., (1 - cut/180)*pi, num = g.m - 2*good)

    R, TH = meshgrid(g.r, theta_good)
    X = R*sin(TH)
    Y = R*cos(TH)
    scale = 1.e7
    fig, ax = P.subplots(nrows = 1, ncols = 1, facecolor = None, edgecolor = None, linewidth = None)
    F = F[good:-good, :]/scale
    Fmin = F.min()
    Fmax = F.max()
    if abs(Fmin) > abs(Fmax):
        Fmax = Fmax*abs(Fmin)/abs(Fmax)
    else:
        Fmin = Fmin*abs(Fmax)/abs(Fmin)
    level = N.linspace(Fmin, Fmax, 50)*adjust_color
    cs = ax.contourf(X, Y, F, levels = level, interpolation = 'bicubic', cmap = P.get_cmap("RdBu")) #RdYlBu

    # Add colorbar
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("left", size = "3%", pad = 0.08)

    tick = N.linspace(level[0], level[-1], num = 2, endpoint = True)
    cbar = fig.colorbar(cs, cax = cax, ticks = tick, ticklocation = 'left', format = '%3.0f')
    cbar.ax.set_ylabel(r"$[10^{7} \times \mathrm {kg \ m^{-1} s^{-2}} ]$")
    cbar.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 2, length = 6)
    if label == 1:
        name1 = 'MC'
        ax.set_title(r'$-\nabla . {\bar F}_{\mathrm{MC}}$')
    elif label == 2:
        name1 = 'RS'
        ax.set_title(r'$-\nabla . {\bar F}_{\mathrm{RS}}$')
    elif label == 3:
        name1 = 'MT'
        ax.set_title(r'-\nabla . {\bar F}_{\mathrm{MT}}$')
    elif label == 4:
        name1 = 'MS'
        ax.set_title(r'$-\nabla . {\bar F}_{\mathrm{MS}}$')
    elif label == 5:
        name1 = 'TOTAL'
        ax.set_title(r'$-\nabla . {\bar F}_{\mathrm{Total}}$')

    # Make sure aspect ratio preserved
    ax.set_aspect('equal')

    # Turn off rectangular frame.
    ax.set_frame_on(False)

    # Turn off axis ticks.
    ax.set_xticks([])
    ax.set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
    ax.plot(rmax*sin(g.th), rmax*cos(g.th),'k')
    ax.plot(rmin*sin(g.th), rmin*cos(g.th),'k')
    ax.plot(g.r*sin(min(g.th)), g.r*cos(min(g.th)), 'k')
    ax.plot(g.r*sin(max(g.th)), g.r*cos(max(g.th)), 'k')

    # Draw a circle in the rad-conv interface
    if rad_core:
         ax.plot(3.3 * g.r[0] * sin(g.th), 3.3 * g.r[0] * cos(g.th), '--k')
    P.tight_layout()
    P.savefig(name + 'torque-' + name1 + '.png')
    P.show()
################################################################################
def plot_torque_tot(F, F1, F2, lev = (0., 0., 0.), name = '', rad_core = False):
    # plot configured for ttauri star
    rc('lines', linewidth = 1)
    g = E.read_grid()

    cut = 5. # Degrees to be cut off
    good = int(cut*g.m/180.)

    # PLOT
    if (good < 2):
            good = 2
    theta_good = N.linspace(0+(cut/90)*pi/2., (1 - cut/180)*pi, num = g.m - 2*good)

    R, TH = meshgrid(g.r, theta_good)
    X = R*sin(TH)
    Y = R*cos(TH)
    scale = 1.e6
    fig, axes = P.subplots(nrows = 1, ncols = 3, figsize = (8,5),facecolor = None, edgecolor = None, linewidth = None)
    ax, ax1, ax2 = axes
    F = F[good:-good, :]/scale
    F1 = F1[good:-good, :]/scale
    F2 = F2[good:-good, :]/scale
    Fmax = N.abs(F).max()
    F1max = N.abs(F1).max()
    F2max = N.abs(F2).max()
    level = N.linspace(-Fmax, Fmax, 50)
    level1 = N.linspace(-F1max, F1max, 50)
    level2 = N.linspace(-F2max, F2max, 50)
    if lev[0] !=0:
        level  = N.linspace(-lev[0], lev[0], 50)
        level1 = N.linspace(-lev[1], lev[1], 50)
        level2 = N.linspace(-lev[2], lev[2], 50)
    cs  = ax.contourf( X, Y, F,  levels = level,  interpolation = 'bicubic', cmap = cmocean.cm.delta, extend = 'both')
    cs1 = ax1.contourf(X, Y, F1, levels = level1, interpolation = 'bicubic', cmap = cmocean.cm.delta, extend = 'both')
    cs2 = ax2.contourf(X, Y, F2, levels = level2, interpolation = 'bicubic', cmap = cmocean.cm.delta, extend = 'both')

    # Add colorbar
    divider = make_axes_locatable(ax)
    divider1 = make_axes_locatable(ax1)
    divider2 = make_axes_locatable(ax2)
    cax = divider.append_axes("left", size = "7%", pad = 0.08)
    cax1 = divider1.append_axes("left", size = "7%", pad = 0.08)
    cax2 = divider2.append_axes("left", size = "7%", pad = 0.08)

    tick = N.linspace(level[0], level[-1], num = 2, endpoint = True)
    tick1 = N.linspace(level1[0], level1[-1], num = 2, endpoint = True)
    tick2 = N.linspace(level2[0], level2[-1], num = 2, endpoint = True)
    cbar = fig.colorbar(cs, cax = cax, ticks = tick, ticklocation = 'left', format = '%3.0f')
    cbar1 = fig.colorbar(cs1, cax = cax1, ticks = tick1, ticklocation = 'left', format = '%3.0f')
    cbar2 = fig.colorbar(cs2, cax = cax2, ticks = tick2, ticklocation = 'left', format = '%3.0f')
    cbar.ax.set_ylabel(r"$[10^{6} \times \mathrm {kg \ m^{-1} s^{-2}} ]$")
    cbar.ax.tick_params(which = 'major', direction = 'inout', pad = 2, width = 1, length = 1, top = 0.97, bottom = 0.03)
    cbar1.ax.tick_params(which = 'major', direction = 'inout', pad = 2, width = 1, length = 1, top = 0.97, bottom = 0.03)
    cbar2.ax.tick_params(which = 'major', direction = 'inout', pad = 2, width = 1, length = 1, top = 0.97, bottom = 0.03)
    ax.set_title(r'$-\nabla . {\bar F}_{\mathrm{MC}}$')
    ax1.set_title(r'$-\nabla . {\bar F}_{\mathrm{RS}}$')
    ax2.set_title(r'$\nabla . {\bar F}_{\mathrm{Total}}$')

    # Make sure aspect ratio preserved
    ax.set_aspect('equal')
    ax1.set_aspect('equal')
    ax2.set_aspect('equal')

    # Turn off rectangular frame.
    ax.set_frame_on(False)
    ax1.set_frame_on(False)
    ax2.set_frame_on(False)

    # Turn off axis ticks.
    ax.set_xticks([])
    ax.set_yticks([])
    ax1.set_xticks([])
    ax1.set_yticks([])
    ax2.set_xticks([])
    ax2.set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
    ax.plot(rmax*sin(g.th), rmax*cos(g.th),'k')
    ax.plot(rmin*sin(g.th), rmin*cos(g.th),'k')
    ax.plot(g.r*sin(min(g.th)), g.r*cos(min(g.th)), 'k')
    ax.plot(g.r*sin(max(g.th)), g.r*cos(max(g.th)), 'k')
    ax1.plot(rmax*sin(g.th), rmax*cos(g.th),'k')
    ax1.plot(rmin*sin(g.th), rmin*cos(g.th),'k')
    ax1.plot(g.r*sin(min(g.th)), g.r*cos(min(g.th)), 'k')
    ax1.plot(g.r*sin(max(g.th)), g.r*cos(max(g.th)), 'k')
    ax2.plot(rmax*sin(g.th), rmax*cos(g.th),'k')
    ax2.plot(rmin*sin(g.th), rmin*cos(g.th),'k')
    ax2.plot(g.r*sin(min(g.th)), g.r*cos(min(g.th)), 'k')
    ax2.plot(g.r*sin(max(g.th)), g.r*cos(max(g.th)), 'k')

    # Draw a circle in the rad-conv interface
    if rad_core:
         ax.plot(3.3 * g.r[0] * sin(g.th), 3.3 * g.r[0] * cos(g.th), '--k')
         ax1.plot(3.3 * g.r[0] * sin(g.th), 3.3 * g.r[0] * cos(g.th), '--k')
         ax2.plot(3.3 * g.r[0] * sin(g.th), 3.3 * g.r[0] * cos(g.th), '--k')
    P.tight_layout()
    P.savefig(name + 'torque.png')
    P.show()
################################################################################
def plot_torque_tot_MHD(F, F1, F2, F3, F4, lev = (0., 0., 0., 0., 0.), name = '', rad_core = False):
    font = { 'size' : 18}
    rc('font', **font)
    rc('xtick', labelsize = 16)
    rc('ytick', labelsize = 16)
    
    legend = {'fontsize': 16, 'numpoints' : 3}
    rc('legend',**legend)
    axes = {'labelsize': 16, 'titlesize' : 18}
    rc('axes', **axes)
    # plot configured for ttauri star
    rc('lines', linewidth = 1)

    g = E.read_grid()

    cut = 5. # Degrees to be cut off
    good = int(cut*g.m/180.)

    # PLOT
    if (good < 2):
            good = 2
    theta_good = N.linspace(0+(cut/90)*pi/2., (1 - cut/180)*pi, num = g.m - 2*good)

    R, TH = meshgrid(g.r, theta_good)
    X = R*sin(TH)
    Y = R*cos(TH)
    scale = 1.e5
    fig, axes = P.subplots(nrows = 1, ncols = 5, figsize = (8,5),facecolor = None, edgecolor = None, linewidth = None)
    ax, ax1, ax2, ax3, ax4 = axes
    F = F[good:-good, :]/scale
    F1 = F1[good:-good, :]/scale
    F2 = F2[good:-good, :]/scale
    F3 = F3[good:-good, :]/scale
    F4 = F4[good:-good, :]/scale
    Fmax = N.abs(F).max()
    F1max = N.abs(F1).max()
    F2max = N.abs(F2).max()
    F3max = N.abs(F3).max()
    F4max = N.abs(F4).max()
    level = N.linspace(-Fmax, Fmax, 50)
    level1 = N.linspace(-F1max, F1max, 50)
    level2 = N.linspace(-F2max, F2max, 50)
    level3 = N.linspace(-F3max, F3max, 50)
    level4 = N.linspace(-F4max, F4max, 50)
    if lev[0] !=0:
        level  = N.linspace(-lev[0], lev[0], 50)
        level1 = N.linspace(-lev[1], lev[1], 50)
        level2 = N.linspace(-lev[2], lev[2], 50)
        level3 = N.linspace(-lev[3], lev[3], 50)
        level4 = N.linspace(-lev[4], lev[4], 50)
    cs  = ax.contourf( X, Y, F,  levels = level,  interpolation = 'bicubic', cmap = cmocean.cm.delta, extend = 'both')
    cs1 = ax1.contourf(X, Y, F1, levels = level1, interpolation = 'bicubic', cmap = cmocean.cm.delta, extend = 'both') 
    cs2 = ax2.contourf(X, Y, F2, levels = level2, interpolation = 'bicubic', cmap = cmocean.cm.delta, extend = 'both') 
    cs3 = ax3.contourf(X, Y, F3, levels = level3, interpolation = 'bicubic', cmap = cmocean.cm.delta, extend = 'both') 
    cs4 = ax4.contourf(X, Y, F4, levels = level4, interpolation = 'bicubic', cmap = cmocean.cm.delta, extend = 'both') 

    # Add colorbar
    divider = make_axes_locatable(ax)
    divider1 = make_axes_locatable(ax1)
    divider2 = make_axes_locatable(ax2)
    divider3 = make_axes_locatable(ax3)
    divider4 = make_axes_locatable(ax4)
    cax =   divider.append_axes("left", size = "7%", pad = 0.08)
    cax1 = divider1.append_axes("left", size = "7%", pad = 0.08)
    cax2 = divider2.append_axes("left", size = "7%", pad = 0.08)
    cax3 = divider3.append_axes("left", size = "7%", pad = 0.08)
    cax4 = divider4.append_axes("left", size = "7%", pad = 0.08)

    tick  = N.linspace(level[0], level[-1], num = 2, endpoint = True)
    tick1 = N.linspace(level1[0], level1[-1], num = 2, endpoint = True)
    tick2 = N.linspace(level2[0], level2[-1], num = 2, endpoint = True)
    tick3 = N.linspace(level3[0], level3[-1], num = 2, endpoint = True)
    tick4 = N.linspace(level4[0], level4[-1], num = 2, endpoint = True)
    cbar  = fig.colorbar(cs,  cax = cax,  ticks = tick,  ticklocation = 'left', format = '%3.0f')
    cbar1 = fig.colorbar(cs1, cax = cax1, ticks = tick1, ticklocation = 'left', format = '%3.0f')
    cbar2 = fig.colorbar(cs2, cax = cax2, ticks = tick2, ticklocation = 'left', format = '%3.0f')
    cbar3 = fig.colorbar(cs3, cax = cax3, ticks = tick3, ticklocation = 'left', format = '%3.0f')
    cbar4 = fig.colorbar(cs4, cax = cax4, ticks = tick4, ticklocation = 'left', format = '%3.0f')
    cbar.ax.set_ylabel(r"$(10^{5} \times \mathrm{kg \; m^{-1} s^{-2}})$")
    cbar.ax.tick_params( which = 'major', direction = 'inout', pad = 2, width = 1, length = 1, top = 0.97, bottom = 0.03)
    cbar1.ax.tick_params(which = 'major', direction = 'inout', pad = 2, width = 1, length = 1, top = 0.97, bottom = 0.03)
    cbar2.ax.tick_params(which = 'major', direction = 'inout', pad = 2, width = 1, length = 1, top = 0.97, bottom = 0.03)
    cbar3.ax.tick_params(which = 'major', direction = 'inout', pad = 2, width = 1, length = 1, top = 0.97, bottom = 0.03)
    cbar4.ax.tick_params(which = 'major', direction = 'inout', pad = 2, width = 1, length = 1, top = 0.97, bottom = 0.03)
    ax.set_title( r'$-\nabla . \overline{F}_\mathrm{MC}$')
    ax1.set_title(r'$-\nabla . \overline{F}_\mathrm{RS}$')
    ax2.set_title(r'$-\nabla . \overline{F}_\mathrm{MT}$')
    ax3.set_title(r'$-\nabla . \overline{F}_\mathrm{MS}$')
    ax4.set_title(r'$-\nabla . \overline{F}_\mathrm{Visc}$')

    # Make sure aspect ratio preserved
    ax.set_aspect('equal')
    ax1.set_aspect('equal')
    ax2.set_aspect('equal')
    ax3.set_aspect('equal')
    ax4.set_aspect('equal')

    # Turn off rectangular frame.
    ax.set_frame_on(False)
    ax1.set_frame_on(False)
    ax2.set_frame_on(False)
    ax3.set_frame_on(False)
    ax4.set_frame_on(False)

    # Turn off axis ticks.
    ax.set_xticks([])
    ax.set_yticks([])
    ax1.set_xticks([])
    ax1.set_yticks([])
    ax2.set_xticks([])
    ax2.set_yticks([])
    ax3.set_xticks([])
    ax3.set_yticks([])
    ax4.set_xticks([])
    ax4.set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
    ax.plot(rmax*sin(g.th), rmax*cos(g.th),'k')
    ax.plot(rmin*sin(g.th), rmin*cos(g.th),'k')
    ax.plot(g.r*sin(min(g.th)), g.r*cos(min(g.th)), 'k')
    ax.plot(g.r*sin(max(g.th)), g.r*cos(max(g.th)), 'k')
    ax1.plot(rmax*sin(g.th), rmax*cos(g.th),'k')
    ax1.plot(rmin*sin(g.th), rmin*cos(g.th),'k')
    ax1.plot(g.r*sin(min(g.th)), g.r*cos(min(g.th)), 'k')
    ax1.plot(g.r*sin(max(g.th)), g.r*cos(max(g.th)), 'k')
    ax2.plot(rmax*sin(g.th), rmax*cos(g.th),'k')
    ax2.plot(rmin*sin(g.th), rmin*cos(g.th),'k')
    ax2.plot(g.r*sin(min(g.th)), g.r*cos(min(g.th)), 'k')
    ax2.plot(g.r*sin(max(g.th)), g.r*cos(max(g.th)), 'k')
    ax3.plot(rmax*sin(g.th), rmax*cos(g.th),'k')
    ax3.plot(rmin*sin(g.th), rmin*cos(g.th),'k')
    ax3.plot(g.r*sin(min(g.th)), g.r*cos(min(g.th)), 'k')
    ax3.plot(g.r*sin(max(g.th)), g.r*cos(max(g.th)), 'k')
    ax4.plot(rmax*sin(g.th), rmax*cos(g.th),'k')
    ax4.plot(rmin*sin(g.th), rmin*cos(g.th),'k')
    ax4.plot(g.r*sin(min(g.th)), g.r*cos(min(g.th)), 'k')
    ax4.plot(g.r*sin(max(g.th)), g.r*cos(max(g.th)), 'k')

    # Draw a circle in the rad-conv interface
    if rad_core:
         ax.plot(3.3 * g.r[0] * sin(g.th), 3.3 * g.r[0] * cos(g.th), '--k')
         ax1.plot(3.3 * g.r[0] * sin(g.th), 3.3 * g.r[0] * cos(g.th), '--k')
         ax2.plot(3.3 * g.r[0] * sin(g.th), 3.3 * g.r[0] * cos(g.th), '--k')
         ax3.plot(3.3 * g.r[0] * sin(g.th), 3.3 * g.r[0] * cos(g.th), '--k')
         ax4.plot(3.3 * g.r[0] * sin(g.th), 3.3 * g.r[0] * cos(g.th), '--k')
    P.subplots_adjust(top = 0.95, bottom = .05, left = 0.10, right = .96, hspace = .15, wspace = .15)
    P.savefig(name + 'torque.png', bbox_inches='tight')
    P.show()
################################################################################
def alpha(alpha, adjust_color = 1., name = '', rad_core = True):
    g = E.read_grid()

    cut = 5. # Degrees to be cut off
    good = int(cut*g.m/180.)

    # PLOT
    if (good < 2):
            good = 2
    theta_good = N.linspace(0+(cut/90)*pi/2., (1 - cut/180)*pi, num = g.m - 2*good)

    R, TH = meshgrid(g.r, theta_good)
    X = R*sin(TH)
    Y = R*cos(TH)

    fig, ax = P.subplots(nrows = 1, ncols = 1, facecolor = None, edgecolor = None, linewidth = None)

    alpha = alpha[good:-good, :]
    alim = N.abs(alpha).max()
    level = N.linspace(-alim, alim, 50)*adjust_color
    cs = ax.contourf(X, Y, alpha, levels = level, interpolation = 'bicubic', cmap = P.get_cmap("RdBu")) #RdYlBu

    # Add colorbar
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("left", size = "3%", pad = 0.08)

    tick = N.linspace(level[0], level[-1], num = 2, endpoint = True)
    cbar = fig.colorbar(cs, cax = cax, ticks = tick, ticklocation = 'left', format = '%1.0f')

    cbar.ax.set_ylabel(r"$\alpha_\mathrm{t} \left(\mathrm{m/s^2}\right)$")
    cbar.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 0, length = 6)

    # Make sure aspect ratio preserved
    ax.set_aspect('equal')

    # Turn off rectangular frame.
    ax.set_frame_on(False)

    # Turn off axis ticks.
    ax.set_xticks([])
    ax.set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
    ax.plot(rmax*sin(g.th), rmax*cos(g.th),'k')
    ax.plot(rmin*sin(g.th), rmin*cos(g.th),'k')
    ax.plot(g.r*sin(min(g.th)), g.r*cos(min(g.th)), 'k')
    ax.plot(g.r*sin(max(g.th)), g.r*cos(max(g.th)), 'k')

    # Draw a circle in the rad-conv interface
    if rad_core:
        ax.plot(3.4 * g.r[0] * sin(g.th), 3.4 * g.r[0] * cos(g.th), '--k')
#        ax.plot(0.7 * 6.96e8 * sin(g.th), 0.7 * 6.96e8 * cos(g.th), '--k')
    P.tight_layout()
    P.savefig(name+'alpha_t'+'.png')
    P.show()

###############################################################################################
def pif_r(imc_r, irs_r, imt_r, ims_r, i_r, rstar, ymax = 2., name = ''):
    g = E.read_grid()
    g.rf = g.r/rstar
    factor = 1.e31
    imc_r /= factor
    irs_r /= factor
    imt_r /= factor
    ims_r /= factor
    i_r /= factor
    g = E.read_grid()
    g.rf = g.r/rstar
    P.plot(g.rf, imc_r,  'r', label = r'$\mathrm{MC}$')
    P.plot(g.rf, irs_r,  'g', label = r'$\mathrm{RS}$')
    P.plot(g.rf, imt_r,  'b', label = r'$\mathrm{MT}$')
    P.plot(g.rf, ims_r,  'k', label = r'$\mathrm{MS}$')
    P.plot(g.rf, i_r,  '--k', label = r'$\mathrm{Visc}$')
    P.xlabel(r'$r \; (\mathrm{R_s})$')
    P.ylabel(r'$I_\mathrm{r}/10^{31} \; \left(\mathrm{Kg} \; \mathrm{m}^2 \; \mathrm{s}^{-2} \right)$')
    P.xlim([0.0, 1.])
    P.ylim([-ymax, ymax])
    P.xticks(N.linspace(0.1,.9, 9))
    P.yticks(N.arange(-ymax,ymax+1, 3))
    P.legend(handlelength = 1.3, loc = 3, ncol = 5, mode = "expand", borderaxespad = 0., shadow = False)#bbox_to_anchor=(0., 1.01, 1., .08),  
    P.subplots_adjust(top = 0.88, bottom = .14, left = 0.15, right = .9)
    P.savefig(name+'-i_r.png', bbox_inches = 'both')
    P.show()
################################################################################
def pif_th(imc_th, irs_th, imt_th, ims_th, i_th, rstar, ymax = 2., name = ''):
    factor = 1.e31
    imc_th /= factor
    irs_th /= factor
    imt_th /= factor
    ims_th /= factor
    i_th /= factor
    g = E.read_grid()
    g.rf = g.r/rstar
    theta = N.linspace(-90, 90, num = i_th.shape[0], endpoint = True)
    P.plot(theta, imc_th,  'r', label = r'$\mathrm{MC}$')
    P.plot(theta, irs_th,  'g', label = r'$\mathrm{RS}$')
    P.plot(theta, imt_th,  'b', label = r'$\mathrm{MT}$')
    P.plot(theta, ims_th,  'k', label = r'$\mathrm{MS}$')
    P.plot(theta, i_th,  '--k', label = r'$\mathrm{Visc}$')
    P.xlabel(r'$\theta - 90^{\circ}$')
    P.xticks([90, 45, 0, -45, -90], [r"$90^{\circ}$",r"$45^{\circ}$",r"$0^{\circ}$",r"$-45^{\circ}$",r"$-90^{\circ}$"])
    P.ylabel(r'$I_{\theta}/10^{31} \; \left( \mathrm{Kg} \; \mathrm{m}^2 \; \mathrm{s}^{-2} \right)$')
    P.xlim([-90, 90])
    P.ylim([-ymax, ymax])
    P.yticks(N.arange(-ymax,ymax+1, 3))
#    P.legend(bbox_to_anchor=(0., 1.01, 1., .08), handlelength = 1.3, loc = 3, ncol = 5, mode = "expand", borderaxespad = 0., shadow = False)
    P.subplots_adjust(top = 0.88, bottom = .14, left = 0.15, right = .9)
    P.savefig(name+'-i_th.png', bbox_inches = 'both')
    P.show()
################################################################################
