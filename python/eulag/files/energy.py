import numpy as N
import eulag as E
################################################################################
###### Define constants #######
year = 365.*24.*3600.
mu = 4*N.pi*1.e-7
################################################################################
def energy(h, rstar, nn = 10):
    # Input :
    # > h = E.read_f11()
    # > rstar is the star radius
    # > nn  is the number of steps in temporal mean
    stratification = N.genfromtxt('strat.dat')
    rho_ad = stratification[:, 5]
    g = E.read_grid()
    g.rf = g.r/rstar
    irtac, = N.where(g.rf > .1)

    um = h.u.mean(axis = 1)
    vm = h.v.mean(axis = 1)
    wm = h.w.mean(axis = 1)
    # Differential Rotation Kinetic Energy
    drke = 0.5*(rho_ad*(um**2))[..., irtac].mean(axis = (1, 2))
    # Meridional Circulation Kinetic Energy
    mcke = 0.5*(rho_ad*(vm**2 + wm**2))[..., irtac].mean(axis = (1, 2))
    # Convective Kinetic Energy or Non-Axissymetric Kinetic energy
    tu2 = E.turb(h.u)**2
    tv2 = E.turb(h.v)**2
    tw2 = E.turb(h.w)**2
    cke = 0.5*(rho_ad*(tu2 + tv2 + tw2))[...,irtac].mean(axis = (1, 2, 3))
    # Total Kinetic Energy
    ke = drke + mcke + cke
    del tu2, tv2, tw2

    bxm = h.bx.mean(axis = 1)
    bym = h.by.mean(axis = 1)
    bzm = h.bz.mean(axis = 1)
    # Magnetic Energy
    me_r = (0.5/mu)*(h.bx**2 + h.by**2 + h.bz**2)[-nn::, ...].mean(axis = (0, 1, 2))
    # Poloidal Mean Magnetic Energy
    pme_r = (0.5/mu)*(bym**2 + bzm**2)[-nn::, ...].mean(axis = (0, 1))
    # Toroidal Mean Magnetic Energy
    tme_r = (0.5/mu)*(bxm**2)[-nn::, ...].mean(axis = (0, 1))
    # Fluctuating Magnetic Energy
    tbx2 = E.turb(h.bx)**2
    tby2 = E.turb(h.by)**2
    tbz2 = E.turb(h.bz)**2
    fme_r = (0.5/mu)*(tbx2 + tby2 + tbz2)[-nn::, :].mean(axis = (0, 1, 2))

    me = (0.5/mu)*(h.bx**2 + h.by**2 + h.bz**2)[..., irtac].mean(axis = (1, 2, 3))
    tme = (0.5/mu)*(bxm[..., irtac].mean(axis = (1, 2)))**2
    pme = (0.5/mu)*((bym[..., irtac].mean(axis = (1, 2)))**2 + (bzm[..., irtac].mean(axis = (1, 2)))**2)
    fme = (0.5/mu)*(tbx2 + tby2 + tbz2)[..., irtac].mean(axis = (1, 2, 3))
    del tbx2, tby2, tbz2
    return me_r, tme_r, pme_r, fme_r, me, tme, pme, fme, ke, drke, mcke, cke

def energy_HD(h, rstar, nn = 10):
    # Input :
    # > h = E.read_f11()
    # > rstar is the star radius
    # > nn  is the number of steps in temporal mean
    stratification = N.genfromtxt('strat.dat')
    rho_ad = stratification[:, 5]
    g = E.read_grid()
    g.rf = g.r/rstar
    irtac, = N.where(g.rf > .1)

    um = h.u.mean(axis = 1)
    vm = h.v.mean(axis = 1)
    wm = h.w.mean(axis = 1)
    # Differential Rotation Kinetic Energy
    drke = 0.5*(rho_ad*(um**2))[..., irtac].mean(axis = (1, 2))
    # Meridional Circulation Kinetic Energy
    mcke = 0.5*(rho_ad*(vm**2 + wm**2))[..., irtac].mean(axis = (1, 2))
    # Convective Kinetic Energy or Non-Axissymetric Kinetic energy
    tu2 = E.turb(h.u)**2
    tv2 = E.turb(h.v)**2
    tw2 = E.turb(h.w)**2
    cke = 0.5*(rho_ad*(tu2 + tv2 + tw2))[...,irtac].mean(axis = (1, 2, 3))
    # Total Kinetic Energy
    ke = drke + mcke + cke
    del tu2, tv2, tw2
    return ke, drke, mcke, cke

################################################################################
def energy_xav(h, rstar, nn = 10):
    # Input :
    # > h = E.read_f11()
    # > rstar is the star radius
    # > nn  is the number of steps in temporal mean
    stratification = N.genfromtxt('strat.dat')
    rho_ad = stratification[:, 5]
    g = E.read_grid()
    g.rf = g.r/rstar
    irtac, = N.where(g.rf > .1)

    um = h.u
    vm = h.v
    wm = h.w
    # Differential Rotation Kinetic Energy
    drke = 0.5*(rho_ad*(um**2))[..., irtac].mean(axis = (1, 2))
    # Meridional Circulation Kinetic Energy
    mcke = 0.5*(rho_ad*(vm**2 + wm**2))[..., irtac].mean(axis = (1, 2))
    # Convective Kinetic Energy or Non-Axissymetric Kinetic energy
    tu2 = E.turb_xav(h.u, h.u2)**2
    tv2 = E.turb_xav(h.v, h.v2)**2
    tw2 = E.turb_xav(h.w, h.w2)**2
    cke = 0.5*(rho_ad*(tu2 + tv2 + tw2))[...,irtac].mean(axis = (1, 2))
    # Total Kinetic Energy
    ke = drke + mcke + cke
    del tu2, tv2, tw2

    bxm = h.bx
    bym = h.by
    bzm = h.bz
    # Magnetic Energy
    me_r = (0.5/mu)*(h.bx2 + h.by2 + h.bz2)[-nn::, ...].mean(axis = (0, 1))
    # Poloidal Mean Magnetic Energy
    pme_r = (0.5/mu)*(bym**2 + bzm**2)[-nn::, ...].mean(axis = (0, 1))
    # Toroidal Mean Magnetic Energy
    tme_r = (0.5/mu)*(bxm**2)[-nn::, ...].mean(axis = (0, 1))
    # Fluctuating Magnetic Energy
    tbx2 = E.turb_xav(h.bx, h.bx2)**2
    tby2 = E.turb_xav(h.by, h.by2)**2
    tbz2 = E.turb_xav(h.bz, h.bz2)**2
    fme_r = (0.5/mu)*(tbx2 + tby2 + tbz2)[-nn::, ...].mean(axis = (0, 1))

#    me = (0.5/mu)*(h.bx2 + h.by2 + h.bz2)[..., irtac].mean(axis = (1, 2))
#    tme = (0.5/mu)*(bxm[..., irtac].mean(axis = (1, 2))**2)
#    pme = (0.5/mu)*((bym[..., irtac].mean(axis = (1, 2)))**2 + (bzm[..., irtac].mean(axis = (1, 2)))**2)
    me = (0.5/mu)*(h.bx2 + h.by2 + h.bz2)[..., irtac].mean(axis = (1, 2))
    tme = (0.5/mu)*(bxm**2)[..., irtac].mean(axis = (1, 2))
    pme = (0.5/mu)*((bym**2)[..., irtac].mean(axis = (1, 2)) + (bzm**2)[..., irtac].mean(axis = (1, 2)))
    fme = (0.5/mu)*(tbx2 + tby2 + tbz2)[..., irtac].mean(axis = (1, 2))
    del tbx2, tby2, tbz2
    return [me_r, tme_r, pme_r, fme_r, me, tme, pme, fme, ke, drke, mcke, cke]

def energy_HD_xav(h, rstar):
    # Input :
    # > h = E.read_f11()
    # > rstar is the star radius
    stratification = N.genfromtxt('strat.dat')
    rho_ad = stratification[:, 5]
    g = E.read_grid()
    g.rf = g.r/rstar
    irtac, = N.where(g.rf > .1)

    um = h.u
    vm = h.v
    wm = h.w
    # Differential Rotation Kinetic Energy
    drke = 0.5*(rho_ad*(um**2))[..., irtac].mean(axis = (1, 2))
    # Meridional Circulation Kinetic Energy
    mcke = 0.5*(rho_ad*(vm**2 + wm**2))[..., irtac].mean(axis = (1, 2))
    # Convective Kinetic Energy or Non-Axissymetric Kinetic energy
    tu2 = E.turb_xav(h.u, h.u2)**2
    tv2 = E.turb_xav(h.v, h.v2)**2
    tw2 = E.turb_xav(h.w, h.w2)**2
    cke = 0.5*(rho_ad*(tu2 + tv2 + tw2))[...,irtac].mean(axis = (1, 2))
    # Total Kinetic Energy
    ke = drke + mcke + cke
    del tu2, tv2, tw2
    return ke, drke, mcke, cke
