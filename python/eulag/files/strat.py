# read eulag strat: GG
import numpy as N

def read_strat(*args, **kwargs):
	""" Read stratification from eulag.
		call using
		import eulag as E
		s = read_strat()
	"""
	return Strat(*args, **kwargs)

class Strat(object):
	def __init__ (self, datafile = 'strat.dat'):
		s = N.genfromtxt(datafile)
		if (s.shape[1] == 7):
			radius = s[:,0]
			z      = s[:,1]
			t_am   = s[:,2]
			rho_am = s[:,3]
			rho_ad = s[:,4]
			the_am = s[:,5]
			kappa  = s[:,6]
			
			self.radius = radius
			self.t_am   = t_am
			self.rho_am = rho_am
			self.rho_ad = rho_ad
			self.the_am = the_am
		else:
			radius = s[:,0]
			z      = s[:,1]
			t_am   = s[:,2]
			t_ad   = s[:,3]
			rho_am = s[:,4]
			rho_ad = s[:,5]
			the_am = s[:,6]
			p_ad   = s[:,7]
			
			self.radius = radius
			self.t_am   = t_am
			self.t_ad   = t_ad
			self.rho_am = rho_am
			self.rho_ad = rho_ad
			self.the_am = the_am
			self.p_ad   = p_ad

def read_strat_atom(*args, **kwargs):
	''' 
	This function reads the file with stratification and calculates the gamma in function of the radius.
	Is expected that the file has 8 columns, arranged as follows: [r, P, T, rho, cpcv, dltdlp, grad_ad, grad_rad]. 
	   
	INPUTS:
	> inputfile: contains the path of the file to be read;
	
	OUTPUT: 
	> r: Radius array;
	> P: Pressure array;
	> T: Temperature array;
	> rho: Density array; 
	> cpcv: Ratio of the specific heat capacity at constant pressure and at constant volumn; 
	> dlntdlnp
	> grad_ad: Adiabatic gradient;
	> grad_rad: Radiative gradient;
	> gamma: Gamma array.
	'''	
	return Strat_atom(*args, **kwargs)

class Strat_atom(object):
	def __init__ (self, inputfile):
		f = open(inputfile, 'r')
		files = N.genfromtxt(f, usecols = (0, 1, 2, 3, 4, 5, 6, 7))
		self.radius = 1e-2*files[:,0]                                           # In meters 
		self.t   = files[:,2]					          # In Kelvin	
		self.P   = 1e-1*files[:,1]					          # In N/m**2 
		self.rho = 1e3*files[:,3]					  # In kg/m**3

