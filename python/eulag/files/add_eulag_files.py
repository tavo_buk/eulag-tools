import eulag as E
import numpy as N

def concat_time(of1, of2):
    return N.array(N.concatenate((of1, of2), axis = 0))

def sum_f11(f, f2):
	f2.time = f2.time + f2.time[1] + f.time[-1]
	f.time = concat_time(f.time, f2.time)
	del f2.time
	f.u = concat_time(f.u, f2.u)
	del f2.u
	f.v = concat_time(f.v, f2.v)
	del f2.v
	f.w = concat_time(f.w, f2.w)
	del f2.w
	f.the = concat_time(f.the, f2.the)
	del f2.the
	f.p = concat_time(f.p, f2.p)
	del f2.p
	f.bx = concat_time(f.bx, f2.bx)
	del f2.bx
	f.by = concat_time(f.by, f2.by)
	del f2.by
	f.bz = concat_time(f.bz, f2.bz)
	del f2
	return None

def sum_f11_HD(f, f2):
	f2.time = f2.time + f2.time[1] + f.time[-1]
	f.time = concat_time(f.time, f2.time)
	del f2.time
	f.u = concat_time(f.u, f2.u)
	del f2.u
	f.v = concat_time(f.v, f2.v)
	del f2.v
	f.w = concat_time(f.w, f2.w)
	del f2.w
	f.the = concat_time(f.the, f2.the)
	del f2.the
	f.p = concat_time(f.p, f2.p)
	del f2
	return None

def sum_xav(f, f2):
	f2.time = f2.time + f2.time[1] + f.time[-1]
	f.time = concat_time(f.time, f2.time)
	del f2.time
	f.u = concat_time(f.u, f2.u)
	del f2.u
	f.v = concat_time(f.v, f2.v)
	del f2.v
	f.w = concat_time(f.w, f2.w)
	del f2.w
	f.rwv = concat_time(f.rwv, f2.rwv)
	del f2.rwv
	f.rwu = concat_time(f.rwu, f2.rwu)
	del f2.rwu
	f.rvu = concat_time(f.rvu, f2.rvu)
	del f2.rvu
	f.u2 = concat_time(f.u2, f2.u2)
	del f2.u2
	f.v2 = concat_time(f.v2, f2.v2)
	del f2.v2
	f.w2 = concat_time(f.w2, f2.w2)
	del f2.w2
	f.the = concat_time(f.the, f2.the)
	del f2.the
	f.P = concat_time(f.P, f2.P)
	del f2.P
	f.bx = concat_time(f.bx, f2.bx)
	del f2.bx
	f.by = concat_time(f.by, f2.by)
	del f2.by
	f.bz = concat_time(f.bz, f2.bz)
	del f2.bz
	f.bxby = concat_time(f.bxby, f2.bxby)
	del f2.bxby
	f.bzby = concat_time(f.bzby, f2.bzby)
	del f2.bzby
	f.bzbx = concat_time(f.bzbx, f2.bzbx)
	del f2.bzbx
	f.bx2 = concat_time(f.bx2, f2.bx2)
	del f2.bx2
	f.by2 = concat_time(f.by2, f2.by2)
	del f2.by2
	f.bz2 = concat_time(f.bz2, f2.bz2)
	del f2
	return None
