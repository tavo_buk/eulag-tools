# read eulag strat: GG
from scipy.io import FortranFile
from eulag.files.grid import read_grid
from eulag.files.modules import filter3D
from eulag.files.eudyn import parameters_N
import numpy as N
import subprocess

def read_f9_N(*args, **kwargs):
        """Read fort.9, short tape from eulag.
           call using
           import eulag as E
           f = read_f9()
           f will be an object with the variables

           Parameters:
           ifl = True:  filters the data
               = False: read without filtering
           start: = 0 (default) initializes the reading after 
                    n outputs                   
        """
        return Rbox(*args, **kwargs)

class Rbox(object):
    def __init__ (self, datafile='',vtype='f', ifl = True, start = 0):
        # Read xaverages file
        # Datafile is the name of the xaverages file to be read
        # ifl can be False or True, default is True
        if (not datafile):
            datafile='fort.9'

        param = parameters_N()
        if param[3] == 0:         # For spherical simulations
            (n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver,\
                    nslice, nplot, nstore, nxaver, it, rb, rt) = param
        else:
            print ('This is a cartesian simulation.')
            (n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver, \
            nslice, nplot, nstore, nxaver, it) = param

        cmd = "grep 'MHD' define.h"
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        temp = process.communicate()[0]
        MHD = int(temp.split()[2])

        if MHD == 1:
            print ('MHD config.')
        else:
            print ('HD config.')

        ''' Defining variables '''
        counter = 0
        u  = []
        v  = []
        w  = []
        ox = []
        oy  = [] 
        oz  = [] 
        rh = []
        the = []
        p   = []
        fx  = []
        fy  = []
        fz  = []
        ft  = []

        ''' reading the file '''
        f9 = FortranFile(datafile)
        while True:
            try:
                ''' Reading variables '''
                # Velocity field
                u.append(N.reshape(f9.read_reals(vtype), (n, m, l), order = 'F'))
                v.append(N.reshape(f9.read_reals(vtype), (n, m, l), order = 'F'))
                w.append(N.reshape(f9.read_reals(vtype), (n, m, l), order = 'F'))
                ox.append(N.reshape(f9.read_reals(vtype), (n, m, l), order = 'F'))
                oy.append(N.reshape(f9.read_reals(vtype), (n, m, l), order = 'F'))
                oz.append(N.reshape(f9.read_reals(vtype), (n, m, l), order = 'F'))
                rh.append(N.reshape(f9.read_reals(vtype), (n, m, l), order = 'F'))
                the.append(N.reshape(f9.read_reals(vtype), (n, m, l), order = 'F'))
                p.append(N.reshape(f9.read_reals(vtype), (n, m, l), order = 'F'))
                fx.append(N.reshape(f9.read_reals(vtype), (n, m, l), order = 'F'))
                fy.append(N.reshape(f9.read_reals(vtype), (n, m, l), order = 'F'))
                fz.append(N.reshape(f9.read_reals(vtype), (n, m, l), order = 'F'))
                ft.append(N.reshape(f9.read_reals(vtype), (n, m, l), order = 'F'))
                counter += 1
            except:
                break
        print ('There are %i outputs in %s ' % (counter-start, datafile))
        self.time = N.arange(counter-start)*dplot*dt

        u = N.array(u) 
        v = N.array(v)
        w = N.array(w) 
        ox = N.array(ox) 
        oy = N.array(oy) 
        oz = N.array(oz) 
        rh= N.array(rh)
        the = N.array(the) 
        p = N.array(p) 
        fx = N.array(fx)
        fy = N.array(fy) 
        fz = N.array(fz) 
        ft = N.array(ft) 
        if ifl:
            for i in range(counter-start):
                u[i,:,:,:] = filter3D(3, u[i, :, :, :], n, m, l, 1)
                v[i,:,:,:] = filter3D(3, v[i, :, :, :], n, m, l, 1)
                w[i,:,:,:] = filter3D(3, w[i, :, :, :], n, m, l, 0)
                the[i,:,:,:] = filter3D(3, the[i, :, :, :], n, m, l, 1)
        else:
            print ('Filter OFF')

        self.u = u
        self.v = v
        self.w = w
        self.ox = ox
        self.oy = oy
        self.oz = oz
        self.rh = rh
        self.the = the
        self.p = p
        self.fx = fx
        self.fy = fy
        self.fz = fz
        self.ft = ft
