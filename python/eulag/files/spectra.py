import eulag as E
import numpy as N
import shtns

def field_thphi(u):
    n = u.shape[0]
    m = u.shape[1]
    ur = N.zeros((m,n))
    for j in range(m):
    	for i in range(n):
  	        ur[j,i] = u[i,j]
    return ur

def coef(ur, ut, up, lmax, g):
    '''
    input:
    ur = ur(phi, theta)
    ut = ut(phi, theta)
    up = up(phi, theta)
    g = object with grid parameters
    read grid before using this
    '''
    Vr = N.float64(field_thphi(ur))   # Transposing and incresing precision
    Vt = N.float64(field_thphi(ut))
    Vp = N.float64(field_thphi(up))
    sh = shtns.sht(lmax, norm = shtns.sht_fourpi )
    nlat, nphi = sh.set_grid(nlat = g.m, nphi = g.n)
    qlm, slm,tlm = sh.analys(Vr, Vt, Vp)
    return sh, qlm, slm, tlm

def coef1D(ur, lmax, g):
    '''
    input:
    ur = ur(phi, theta)
    lmax = largest l mode
    g = object with grid parameters
    read grid before using this
    '''

    Vr = N.float64(field_thphi(ur))   # Transposing and incresing precision
    sh = shtns.sht(lmax, norm = shtns.sht_fourpi )
    nlat, nphi = sh.set_grid(nlat = g.m, nphi = g.n)
    qlm = sh.analys(Vr)
    return sh, qlm

def energy_spectra(sh, qlm, slm, tlm, l):
        energy = 0
        m = 0
        while (m <= l):
            rqlm = qlm[sh.idx(l, m)].real
            iqlm = qlm[sh.idx(l, m)].imag
            rslm = slm[sh.idx(l, m)].real
            islm = slm[sh.idx(l, m)].imag
            rtlm = tlm[sh.idx(l, m)].real
            itlm = tlm[sh.idx(l, m)].imag
            if m != 0:
                energy += 2.*(rqlm**2 + iqlm**2 + l*(l + 1)*(rslm**2 + islm**2 + rtlm**2 + itlm**2))
            else:
                energy += rqlm**2 + iqlm**2 + l*(l + 1)*(rslm**2 + islm**2 + rtlm**2 + itlm**2)
            m += 1
        return energy*4.*N.pi # /(2.*l + 1.)

def energy_spectra_R(sh, qlm, l):
        energy = 0
        m = 0
        while (m <= l):
            rqlm = qlm[sh.idx(l, m)].real
            iqlm = qlm[sh.idx(l, m)].imag
            if m != 0:
                energy += 2.*(rqlm**2 + iqlm**2)
            else:
                energy += rqlm**2 + iqlm**2 
            m += 1
        return energy*4.*N.pi # /(2.*l + 1.)

def energy_spectra_S(sh, slm, l):
        energy = 0
        m = 0
        while (m <= l):
            rslm = slm[sh.idx(l, m)].real
            islm = slm[sh.idx(l, m)].imag
            if m != 0:
                energy += 2.*(l*(l + 1)*(rslm**2 + islm**2))
            else:
                energy += l*(l + 1)*(rslm**2 + islm**2)
            m += 1
        return energy*4.*N.pi # /(2.*l + 1.)

def energy_spectra_T(sh, tlm, l):
        energy = 0
        m = 0
        while (m <= l):
            rtlm = tlm[sh.idx(l, m)].real
            itlm = tlm[sh.idx(l, m)].imag
            if m != 0:
                energy += 2.*(l*(l + 1)*(rtlm**2 + itlm**2))
            else:
                energy += l*(l + 1)*(rtlm**2 + itlm**2)
            m += 1
        return energy*4.*N.pi # /(2.*l + 1.)

def energy_spectra1D(sh, qlm, l):
        energy = 0
        m = 0
        while (m <= l):
            rqlm = qlm[sh.idx(l, m)].real
            iqlm = qlm[sh.idx(l, m)].imag
            if m != 0:
                energy += 2.*(rqlm**2 + iqlm**2) 
            else:
                energy += rqlm**2 + iqlm**2 
            m += 1
        return energy*4.*N.pi # /(2.*l + 1.)

def energy_spectra_m(sh, qlm, slm, tlm, m, lmax):
        '''  
        function spec_m, this function computes the spectra
        adding over l>=m
        use:  
        
        '''
        energy = 0
        l = lmax
        while (l >= m):
            rqlm = qlm[sh.idx(l, m)].real
            iqlm = qlm[sh.idx(l, m)].imag
            rslm = slm[sh.idx(l, m)].real
            islm = slm[sh.idx(l, m)].imag
            rtlm = tlm[sh.idx(l, m)].real
            itlm = tlm[sh.idx(l, m)].imag
            if m != 0:
                energy += 2.*(rqlm**2 + iqlm**2 + l*(l + 1)*(rslm**2 + islm**2 + rtlm**2 + itlm**2))
            else:
                energy += rqlm**2 + iqlm**2 + l*(l + 1)*(rslm**2 + islm**2 + rtlm**2 + itlm**2)
            l -= 1
        return energy*4.*N.pi 

def restore_fields(f, nt, lev, lmax, g):
    '''
    input:
    f = object with variables 
    nt = number of outputs to be averaged 
    lev = radial level where to take the spectra
    lmax = largest l mode
    '''
    bx = f.bx[-nt, :, :, lev]
    by = f.by[-nt, :, :, lev]
    bz = f.bz[-nt, :, :, lev]
    sh, qlm, slm, tlm = coef(bz, by, bx, lmax, g )
    vr, vth, vphi = sh.synth(qlm, slm, tlm)
    return vr, vth, vphi

def mspec(f, g, nt, lev):
    '''
    input:
    f = object with variables 
    g = object with grid parameters
    nt = number of outputs to be averaged 
    lev = radial level where to take the spectra
    '''
    e = N.zeros(g.m)
    for n in range(1, nt + 1):
        bx = f.bx[-n, :, :, lev]
        by = f.by[-n, :, :, lev]
        bz = f.bz[-n, :, :, lev]
        sh, qlm, slm, tlm = coef(bz, by, bx, g.m - 1, g)
        for i in range(g.m):
            e[i] += energy_spectra(sh, qlm, slm, tlm, l = i)
    return e/float(nt)

def spec_lt(ax,ay,az,lev,g):
    '''
    return a 2D array with time and the l mode (latitude)

    input:
    ax,ay,az = components of vector field
    lev = radial level where to take the spectra
    g = object with grid parameters
    '''
    nt = ax.shape[0]
    e = N.zeros((nt,g.m))
    for it in range(nt):
        bx = ax[it,:,:,lev]
        by = ay[it,:,:,lev]
        bz = az[it,:,:,lev]
        sh, qlm, slm, tlm = coef(bz, by, bx, g.m - 1, g)
        for i in range(g.m):
            e[it,i] += energy_spectra(sh, qlm, slm, tlm, l = i)
    return e

def mspec2(ax,ay,az, nt, lev,g):
    '''
    input:
    ax,ay,az = components of vector field
    nt = number of outputs to average
    lev = radial level where to take the spectra
    g = object with grid parameters
    '''
    g = E.read_grid()
    e = N.zeros(g.m)
    for n in range(1, nt + 1):
        bx = ax[-n, :, :, lev]
        by = ay[-n, :, :, lev]
        bz = az[-n, :, :, lev]
        sh, qlm, slm, tlm = coef(bz, by, bx, g.m - 1, g)
        for i in range(g.m):
            e[i] += energy_spectra(sh, qlm, slm, tlm, l = i)
    return e/float(nt)

def mspecRST(ax,ay,az, nt, lev,g):
    '''
    input:
    ax,ay,az = components of vector field
    nt = number of outputs to average
    lev = radial level where to take the spectra
    g = object with grid parameters
    '''
    g = E.read_grid()
    eR = N.zeros(g.m)
    eS = N.zeros(g.m)
    eT = N.zeros(g.m)
    for n in range(1, nt + 1):
        bx = ax[-n, :, :, lev]
        by = ay[-n, :, :, lev]
        bz = az[-n, :, :, lev]
        sh, qlm, slm, tlm = coef(bz, by, bx, g.m - 1, g)
        for i in range(g.m):
            eR[i] += energy_spectra_R(sh, qlm, l = i)
            eS[i] += energy_spectra_S(sh, slm, l = i)
            eT[i] += energy_spectra_T(sh, tlm, l = i)
    return eR/float(nt),eS/float(nt),eT/float(nt)

def mspec_l(ax, ay, az, lev, g):
    '''
    return a 2D array with time and the l mode (latitude)

    input:
    ax,ay,az = components of vector field
    lev = radial level where to take the spectra
    g = object with grid parameters
    '''
    nt = ax.shape[0]
    e = N.zeros((nt,g.m))
    for it in range(nt):
        bx = ax[it, :, :, lev]
        by = ay[it, :, :, lev]
        bz = az[it, :, :, lev]
        sh, qlm, slm, tlm = coef(bz, by, bx, g.m - 1, g)
        for i in range(g.m):
            e[it,i] += energy_spectra(sh, qlm, slm, tlm, l = i)
    return e

def mspec_m(ax,ay,az, lev, g):
    '''
    return a 2D array with time and the m mode (longitude)

    input:
    ax,ay,az = components of vector field
    lev = radial level where to take the spectra
    g = object with grid parameters
    '''
    nt = ax.shape[0]
    e = N.zeros((nt,int(g.n/2)))
    for it in range(nt):
        bx = ax[it, :, :, lev]
        by = ay[it, :, :, lev]
        bz = az[it, :, :, lev]
        sh, qlm, slm, tlm = coef(bz, by, bx, g.m - 1, g)
        for i in range(int(g.n/2)):
            e[it,i] += energy_spectra_m(sh, qlm, slm, tlm, m = i, lmax = g.m-1)
    return e


def mspec_low(f, nt, lev, g):
    e = N.zeros(g.m)
    for n in range(1, nt + 1):
        bx = f.bx[-n, :, :, lev]
        by = f.by[-n, :, :, lev]
        bz = f.bz[-n, :, :, lev]
        sh, qlm, slm, tlm = coef(bz, by, bx, 5, g)
        for i in range(g.m):
            if (i <= 5):
               e[i] += energy_spectra(sh, qlm, slm, tlm, l = i)
            else:
               e[i] = 0.
    return e/float(nt)

def kspec(f, nt, lev, g):
    e = N.zeros(g.m)
    for n in range(1, nt + 1):
        u = f.u[-n, :, :, lev]
        v = f.v[-n, :, :, lev]
        w = f.w[-n, :, :, lev]
        sh, qlm, slm, tlm = coef(w, v, u, g.m - 1, g)
        for i in range(g.m):
            e[i] += energy_spectra(sh, qlm, slm, tlm, l = i)
    return e/float(nt)

def kspec1D(scalar, nt,lev,g):
    e = N.zeros(g.m)
    for n in range(1, nt + 1):
        w = scalar[-n, :, :, lev]
        sh, qlm = coef1D(w, lmax = g.m - 1)
        for i in range(g.m):
            e[i] += energy_spectra1D(sh, qlm, l = i)
    return e/float(nt)
