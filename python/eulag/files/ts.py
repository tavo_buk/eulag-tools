# plot time series
import  eulag  as E
import numpy as N
import matplotlib.pylab as P

def ts(*args, **kwargs):
    """Creates an object containing the time series data

       Use as:
       ts = ts(f), where f is an object with 3 or 4 elements
    """
    return calc_ts(*args, **kwargs)

class calc_ts(object):
    def __init__ (self, f):

        nelements = N.size(f.u.shape)
        year = 365.*24.*3600.

        filename = "define.h"
        imhd = open(filename)
        for line in imhd:
            if line.split()[1] == "MHD":
                MHD = int(line.split()[2])       # If MHD = 0 there is no magnetic field


        if(nelements == 4):

          print('objects with 4 elements coming from fort.11')
          um  = N.mean(f.u,axis=1)
          vm  = N.mean(f.v,axis=1)
          wm  = N.mean(f.w,axis=1)

          ut  = E.turb(f.u)
          vt  = E.turb(f.v)
          wt  = E.turb(f.w)
          urms = N.sqrt(ut**2 + vt**2 + wt**2)
          urms = N.mean(urms,axis=(1,2,3))
          trms = N.mean(f.the,axis=(1,2,3))
          prms = N.mean(f.p,axis=(1,2,3))
          umean = N.sqrt(um**2 + vm**2 + wm**2)
          umean = N.mean(umean,axis=(1,2))

          self.ut = urms         
          self.um = umean         
          self.the = trms
          self.p  = prms
          self.t = f.time/year     

          if MHD == 1:
             bxm = N.mean(f.bx,axis=1)
             bym = N.mean(f.by,axis=1)
             bzm = N.mean(f.bz,axis=1)
             
             bxt = E.turb(f.bx)
             byt = E.turb(f.by)
             bzt = E.turb(f.bz)
             brms = N.sqrt(bxt**2 + byt**2 + bzt**2)
             brms = N.mean(brms,axis=(1,2,3))
             bmean = N.sqrt(bxm**2 + bym**2 + bzm**2)
             bmean = N.mean(bmean,axis=(1,2))

             self.bt = brms         
             self.bm = bmean
    
        if(nelements == 3):
          print('objects with 3 elements coming from xav')

          ut  = E.turb_xav(f.u,f.u2)
          vt  = E.turb_xav(f.v,f.v2)
          wt  = E.turb_xav(f.w,f.w2)
          urms = N.sqrt(ut**2 + vt**2 + wt**2)
          urms = N.mean(urms,axis=(1,2))
          trms = N.mean(f.the,axis=(1,2))
          prms = N.mean(f.p,axis=(1,2))
          umean = N.sqrt(f.u**2 + f.v**2 + f.w**2)
          umean = N.mean(umean,axis=(1,2))

          self.ut = urms         
          self.um = umean         
          self.the = trms
          self.p  = prms
          self.t = f.time/year     

          if MHD == 1:
             
             bxt = E.turb_xav(f.bx,f.bx2)
             byt = E.turb_xav(f.by,f.by2)
             bzt = E.turb_xav(f.bz,f.bz2)
             brms = N.sqrt(bxt**2 + byt**2 + bzt**2)
             brms = N.mean(brms,axis=(1,2))
             bmean = N.sqrt(f.bx**2 + f.by**2 + f.bz**2)
             bmean = N.mean(bmean,axis=(1,2))

             self.bt = brms         
             self.bm = bmean

