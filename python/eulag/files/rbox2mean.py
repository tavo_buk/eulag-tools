# read eulag strat: GG
from scipy.io import FortranFile
from eulag.files.grid import read_grid
from eulag.files.eudyn import parameters
from eulag.files.modules import filter3D
import numpy as N

def rf11m(*args, **kwargs):
        """Read fort.11, short tape from eulag.
           call using
           import eulag as E
           f = read_f11()
           f will be an object with the variables

           Parameters:
           ifl = True:  filters the data
               = False: read without filtering
           start: = 0 (default) initializes the reading after 
                    n outputs                   
        """
        return Rbox(*args, **kwargs)

class Rbox(object):
    def __init__ (self, datafile='', ifl = True, start = 0):
        # Read xaverages file
        # Datafile is the name of the xaverages file to be read
        # ifl can be False or True, default is True
        if (not datafile):
            datafile='fort.11'

        param = parameters()
        if param[3] == 0:         # For spherical simulations
            (n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver,\
                    nslice, nplot, nstore, nxaver, it, rb, rt) = param
        else:
            print ('This is a cartesian simulation.')
            (n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver, \
            nslice, nplot, nstore, nxaver, it) = param

        filename = "define.h"
        imhd = open(filename)
        for line in imhd:
            if line.split()[1] == "MHD":
                MHD = int(line.split()[2])       # If MHD = 0 there is no magnetic field

        if MHD == 1:
            print ('MHD config.')
        else:
            print ('HD config.')

        ''' Defining variables '''
        counter = 0
        u  = [] ; v  = [] ; w  = []
        u2 = [] ; v2 = [] ; w2 = []
        uv = [] ; vw = [] ; wu = []
        the  = []
        P  =  []
        if MHD == 1:
            bx  = [] ; by  = [] ; bz  = []
            bx2 = [] ; by2 = [] ; bz2 = []
            bxby= [] ; bybz= [] ; bzbx= []

        ''' reading the file '''
        f11 = FortranFile(datafile)
        while True:
            try:
                ''' Reading variables '''
                # Velocity field
                tmp = N.reshape(f11.read_reals('f'), (n, m, l), order = 'F')
                if ifl: tmp = filter3D(3, tmp, n, m, l, 1)
                u.append(tmp[:,:,:].mean(axis = 0))
                u2.append((tmp[:,:,:]**2).mean(axis = 0))
                tmp = N.reshape(f11.read_reals('f'), (n, m, l), order = 'F')
                if ifl: tmp = filter3D(3, tmp, n, m, l, 1)
                v.append(tmp[:,:,:].mean(axis = 0))
                v2.append((tmp[:,:,:]**2).mean(axis = 0))
                tmp = N.reshape(f11.read_reals('f'), (n, m, l), order = 'F')
                if ifl: tmp = filter3D(3, tmp, n, m, l, 0)
                w.append(tmp[:,:,:].mean(axis = 0))
                w2.append((tmp[:,:,:]**2).mean(axis = 0))
                tmp = N.reshape(f11.read_reals('f'), (n, m, l), order = 'F')
                if ifl: tmp = filter3D(3, tmp, n, m, l, 1)
                the.append(tmp[:,:,:].mean(axis = 0))
                tmp = N.reshape(f11.read_reals('f'), (n, m, l), order = 'F')
                if ifl: tmp = filter3D(3, tmp, n, m, l, 1)
                P.append(tmp[:,:,:].mean(axis = 0))
                if MHD == 1:
                    # Magnetic field
                    tmp = N.reshape(f11.read_reals('f'), (n, m, l), order = 'F')
                    if ifl: tmp = filter3D(3, tmp, n, m, l, 1)
                    bx.append(tmp[:,:,:].mean(axis = 0))
                    bx2.append((tmp[:,:,:]**2).mean(axis = 0))
                    tmp = N.reshape(f11.read_reals('f'), (n, m, l), order = 'F')
                    if ifl: tmp = filter3D(3, tmp, n, m, l, 1)
                    by.append(tmp[:,:,:].mean(axis = 0))
                    by2.append((tmp[:,:,:]**2).mean(axis = 0))
                    tmp = N.reshape(f11.read_reals('f'), (n, m, l), order = 'F')
                    if ifl: tmp = filter3D(3, tmp, n, m, l, 0)
                    bz.append(tmp[:,:,:].mean(axis = 0))
                    bz2.append((tmp[:,:,:]**2).mean(axis = 0))
                counter += 1
            except:
                break
        print ('%i outs in %s ' % (counter, datafile))
        self.time = N.arange(counter)*dt*dplot

        uf = N.array(u) 
        uf2 = N.array(u2)
        vf = N.array(v)
        vf2 = N.array(v2)
        wf = N.array(w) 
        wf2 = N.array(w2) 
        thef = N.array(the) 
        Pf = N.array(P) 
        del u,u2,v,v2,w,w2,the,P

        if MHD == 1:
            bxf = N.array(bx)
            bxf2 = N.array(bx2)
            byf = N.array(by)
            byf2 = N.array(by2)
            bzf = N.array(bz)
            bzf2 = N.array(bz2)
            del bx,bx2,by,by2,bz,bz2

        self.u  =  uf[0:counter,...]
        self.u2 = uf2[0:counter,...]
        self.v  =  vf[0:counter,...]
        self.v2 = vf2[0:counter,...]
        self.w  =  wf[0:counter,...]
        self.w2 = wf2[0:counter,...]
        self.the = thef[0:counter,...]
        self.p = Pf[0:counter,...]
        if MHD == 1:
            self.bx  =  bxf[0:counter,...]
            self.bx2 = bxf2[0:counter,...]
            self.by  =  byf[0:counter,...]
            self.by2 = byf2[0:counter,...]
            self.bz  =  bzf[0:counter,...]
            self.bz2 = bzf2[0:counter,...]

        print ('deleting unnecessary files')
        del uf,uf2,vf,vf2,wf,wf2,thef,Pf,tmp
        if MHD == 1:
            del bxf,bxf2,byf,byf2,bzf,bzf2

