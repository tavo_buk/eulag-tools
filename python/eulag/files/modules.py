import numpy as N
import sys

def turb(field):
	tfield = N.zeros_like(field)
	fm = field.mean(axis = 1)
	n = field.shape[1]
	for i in range(n):
		tfield[:,i,:,:] = field[:,i,:,:] - fm
	return tfield

def turb_tp(field):
	tfield = N.zeros_like(field)
	fm = field.mean(axis = (0, 1))
	t = field.shape[0]
	n = field.shape[1]
	for i in range(t):
		for j in range(n):
			tfield[i,j,:,:] = field[i,j,:,:] - fm
	return tfield

def turb_xav(f, f2):
	return N.sqrt(f2 - f**2)

def filter3D(ftype,field,dim1,dim2,dim3,bc):
	'''
	filters a field the structure
		field = field[dim1, dim2, dim3]
	            bc = 0 for quantities with boundary values = 0
	            bc = 1 for quantities with boundary values dfield/dr = 0
	'''
	
	ffield = N.zeros((dim1,dim2,dim3),"float32")
	if bc == 0:
		ffield[:,:,0] = 0.
		ffield[:,:,dim3-1] = 0.
	elif bc == 1:
		ffield[:,:,0] = 0.5*(field[:,:,0]+field[:,:,1])
		ffield[:,:,dim3-1] = 0.5*(field[:,:,dim3-1]+field[:,:,dim3-2]) 
        
	for kk in range(1,dim3-1): 
		if ftype == 2:
			ffield[:,:,kk] = 0.5*(field[:,:,kk]+field[:,:,kk+1])
		elif ftype == 3:
			ffield[:,:,kk] = 0.25*(field[:,:,kk-1]+2.*field[:,:,kk]+field[:,:,kk+1])
	return ffield

def deriv1(x,y):
	nx = x.size
	ny = y.size
	yprime = N.zeros(nx)
	if nx != ny:
		sys.exit("elements of the arrays must be equal")
	else:
		x01=x[0]-x[1] ; x02=x[0]-x[2] ; x12=x[1]-x[2]
		yprime[0] = y[0]*(x01 + x02)/(x01*x02) - y[1]*x02/(x01*x12) + y[2]*x01/(x02*x12)
		x01 = x[nx-3]-x[nx-2] ; x02 = x[nx-3]-x[nx-1] ; x12 = x[nx-2]-x[nx-1]
		yprime[nx-1]=-y[nx-3]*x12/(x01*x02) + y[nx-2]*x02/(x01*x12) - y[nx-1]*(x02 + x12)/(x02*x12)
		for i in range(1,nx-1):
			x01 = x[i-1]-x[i] ; x02 = x[i-1]-x[i+1] ; x12 = x[i]-x[i+1]
			yprime[i] = y[i-1]*x12/(x01*x02) + y[i]*(1./x12-1./x01) - y[i+1]*x01/(x02*x12)
	return yprime

def deriv(x,y, axis=0):
# Derives y along axis, with coordinates corresponding to x
	# Used to make the apropriate broadcasting of x to divide y
	n = y.ndim
	slic = (None,)*axis + ( slice(None), ) + (None,)*(n-axis-1)
	return N.gradient(y,axis=axis)/(N.gradient(x))[slic]

def dot(a1,a2,a3,b1,b2,b3):
	"""take dot product of two vectors a & b with 
	
	a.shape = (n,m,l)
	a.shape = (nx,ny,nz)
	
	"""
	
	if (a1.ndim != 3 or a2.ndim != 3 or a3.ndim != 3 or
		b1.ndim != 3 or b2.ndim != 3 or b3.ndim != 3):
		print ("any of the vector components does not have the same dimension")
		raise ValueError
	
	return a1*b1+ a2*b2 + a3*b3

def cross(a1,a2,a3,b1,b2,b3):
	"""take cross of two vectors a & b with shape
	
	a.shape = (nx,ny,nz)
	a.shape = (n,m,l)
	
	"""
	if (a2.shape != b3.shape or a3.shape != b2.shape or 
           a3.shape != b1.shape or a1.shape != b3.shape or 
           a1.shape != b2.shape or a2.shape != b1.shape):
	   sys.exit("any of the vector components does not have the same dimension")
	
	# a x b = eps_ijk a_j b_k
	cross1 = a2*b3 - a3*b2
	cross2 = a3*b1 - a1*b3
	cross3 = a1*b2 - a2*b1
	
	return cross1,cross2,cross3

def grad(a,x,y,z,coord):
	# Routine for computing the gradient of a scalar
	grad_x = N.zeros_like(a)
	grad_y = N.zeros_like(a)
	grad_z = N.zeros_like(a)
	
	if (a.ndim != 3 or 
		a.shape[0] != x.size or a.shape[1] != y.size or a.shape[2] != z.size ):
		sys.exit("any of the vector components does not have the same dimension")
	
	for j in range(y.size):
		for k in range(z.size):
			grad_x[:,j,k] = deriv(x,a[:,j,k])
	
	for i in range(x.size):
		for k in range(z.size):
			grad_y[i,:,k] = deriv(y,a[i,:,k])
	
	for i in range(x.size):
		for j in range(y.size):
			grad_z[i,j,:] = deriv(z,a[i,j,:])
	
	if coord == 'sph':
		z_1 = N.zeros_like(a)
		zsin_1 = N.zeros_like(a)
		sin_y = N.sin(y)
		cos_y = N.cos(y)
		i_sin = N.where(N.abs(sin_y) < 1e-5)[0]
		if i_sin.size > 0:
			cos_y[i_sin] = 0.; sin_y[i_sin] = 1
		for i in range(x.size):
			for j in range(y.size):
				for k in range(z.size):
					z_1[i,j,k] = 1./z[k]
					zsin_1[i,j,k] = 1./(z[k]*sin_y[j])
		
		grad_x = zsin_1*grad_x
		grad_y = z_1*grad_y
	
	return grad_x, grad_y, grad_z


def div(a1,a2,a3,x,y,z,coord):
# Routine for  computing the divergence of a vector 
# a1 (v_phi)[phi,th,r], a2 (v_th)[phi,th,r] and a3 (v_r)[phi,th,r]
	
	da1dx = N.zeros_like(a1)
	da2dy = N.zeros_like(a1)
	da3dz = N.zeros_like(a1)
	
	if (a1.ndim != 3 or a2.ndim != 3 or a3.ndim != 3 or 
		a1.shape[0] != x.size or a1.shape[1] != y.size or a1.shape[2] != z.size or
		a2.shape[0] != x.size or a2.shape[1] != y.size or a2.shape[2] != z.size or
		a3.shape[0] != x.size or a3.shape[1] != y.size or a3.shape[2] != z.size  ):
		sys.exit("any of the vector components does not have the same dimension")
	
	for j in range(y.size):
		for k in range(z.size):
			da1dx[:,j,k] = deriv(x,a1[:,j,k])
	
	for i in range(x.size):
		for k in range(z.size):
			da2dy[i,:,k] = deriv(y,a2[i,:,k])
	
	for i in range(x.size):
		for j in range(y.size):
			da3dz[i,j,:] = deriv(z,a3[i,j,:])

#    i1 = 0 ; i2 = x.size-1
#    j1 = 0 ; j2 = y.size-1
#    k1 = 0 ; k2 = z.size-1
#    da1dx[i1:i2,...] = deriv(x[i1:i2],a1[i1:i2,...])
#    da2dy[:,j1:j2,:] = deriv(y[j1:j2],a2[:,j1:j2,:])
#    da3dz[...,k1:k2] = deriv(z[k1:k2],a3[...,k1:k2])

	div = da1dx + da2dy + da3dz
	
	if coord == 'sph':
		z_1 = N.zeros_like(a1)
		zsin_1 = N.zeros_like(a1)
		cotth = N.zeros_like(a1)
		sin_y = N.sin(y)
		cos_y = N.cos(y)
		i_sin = N.where(N.abs(sin_y) < 1e-5)[0]
		if i_sin.size > 0:
			cos_y[i_sin] = 0.; sin_y[i_sin] = 1
		for i in range(x.size):
			for j in range(y.size):
				for k in range(z.size):
					z_1[i,j,k] = 1./z[k]
					zsin_1[i,j,k] = 1./(z[k]*sin_y[j])
					cotth[i,j,k] = cos_y[j]/sin_y[j]
		div += da1dx*(zsin_1 - 1.) + z_1*cotth*a2 + da2dy*(z_1 - 1.) +\
		       2*z_1*a3
	
	return div


def curl(a1,a2,a3,x,y,z,coord):

# Routine for  computing the curl of a vector 
# a1 (v_phi)[phi,th,r], a2 (v_th)[phi,th,r] and a3 (v_r)[phi,th,r]
# returns the vector curl_x, curl_y, curl_z

	da2dx = N.zeros_like(a1)
	da3dx = N.zeros_like(a1)
	
	da1dy = N.zeros_like(a1)
	da3dy = N.zeros_like(a1)
	
	da1dz = N.zeros_like(a1)
	da2dz = N.zeros_like(a1)
	
	curl_x = N.zeros_like(a1)
	curl_y = N.zeros_like(a1)
	curl_z = N.zeros_like(a1)
	
	if (a1.ndim != 3 or a2.ndim != 3 or a3.ndim != 3 or 
		a1.shape[0] != x.size or a1.shape[1] != y.size or a1.shape[2] != z.size or
		a2.shape[0] != x.size or a2.shape[1] != y.size or a2.shape[2] != z.size or
		a3.shape[0] != x.size or a3.shape[1] != y.size or a3.shape[2] != z.size  ):
		print ('shape of a1 = ', a1.shape)
		sys.exit("any of the vector components does not have the same dimension")

	da2dx = deriv(x,a2,axis=0)
	da3dx = deriv(x,a3,axis=0)
	
	da2dy = deriv(y,a2,axis=1)
	da3dy = deriv(y,a3,axis=1)
	da1dy = deriv(y,a1,axis=1)

	da1dz = deriv(z,a1,axis=2)
	da2dz = deriv(z,a2,axis=2)
	
	curl_x = da3dy - da2dz
	curl_y = da1dz - da3dx
	curl_z = da2dx - da1dy
	
	if coord == 'sph':
		z_1 = N.zeros_like(a1)
		zsin_1 = N.zeros_like(a1)
		cotth = N.zeros_like(a1)
		sin_y = N.sin(y)
		cos_y = N.cos(y)
		sin_y[0]  = 0.5*(sin_y[0] + sin_y[1])
		sin_y[-1] = 0.5*(sin_y[-1] + sin_y[1])
		for i in range(x.size):
			for j in range(y.size):
				for k in range(z.size):
					z_1[i,j,k] = 1./z[k]
					zsin_1[i,j,k] = 1./(z[k]*sin_y[j])
					cotth[i,j,k] = cos_y[j]/sin_y[j]
		
		#curl_x += a2*z_1 + 2.*da2dz - da3dy*(1. + z_1)
		curl_x = da2dz + a2*z_1 - z_1*da3dy 
		#curl_y += da3dx*(zsin_1 + 1.) - a1*z_1 - 2.*da1dz
		curl_y = zsin_1*da3dx - da1dz - a1*z_1
		#curl_z += da1dy*(z_1 + 1.) + a1*cotth*z_1 - da2dx*(zsin_1 + 1.) 
		curl_z = z_1*da1dy + z_1*a1*cotth - zsin_1*da2dx
	
	return curl_x, curl_y, curl_z

def curl2d(a1,a2,a3,x,y,z,coord):

# Routine for  computing the curl of a vector 
# a1 (v_phi)[phi,th,r], a2 (v_th)[phi,th,r] and a3 (v_r)[phi,th,r]
# returns the vector curl_x, curl_y, curl_z

	da2dx = N.zeros_like(a1)
	da3dx = N.zeros_like(a1)
	
	da1dy = N.zeros_like(a1)
	da3dy = N.zeros_like(a1)
	
	da1dz = N.zeros_like(a1)
	da2dz = N.zeros_like(a1)
	
	curl_x = N.zeros_like(a1)
	curl_y = N.zeros_like(a1)
	curl_z = N.zeros_like(a1)
	
	if (a1.ndim != 2 or a2.ndim != 2 or a3.ndim != 2 or 
		a1.shape[0] != y.size or a1.shape[1] != z.size or 
		a2.shape[0] != y.size or a2.shape[1] != z.size or 
		a3.shape[0] != y.size or a3.shape[1] != z.size ):
		print ('shape of a1 = ', a1.shape)
		sys.exit("any of the vector components does not have the same dimension")
#    i1 = 0 ; i2 = x.size
#    j1 = 0 ; j2 = y.size
#    k1 = 0 ; k2 = z.size
#
	da2dy = deriv(y,a2,axis=0)
	da3dy = deriv(y,a3,axis=0)

	da1dz = deriv(z,a1,axis=1)
	da2dz = deriv(z,a2,axis=1)

	curl_x = da3dy - da2dz
	curl_y =  da1dz
	curl_z = -da1dy
	
	if coord == 'sph':
		z_1 = N.zeros_like(a1)
		zsin_1 = N.zeros_like(a1)
		cotth = N.zeros_like(a1)
		sin_y = N.sin(y)
		cos_y = N.cos(y)
		i_sin = N.where(N.abs(sin_y) < 1e-5)[0]
		if i_sin.size > 0:
			cos_y[i_sin] = 0.; sin_y[i_sin] = 1
		for j in range(y.size):
			for k in range(z.size):
				z_1[j,k] = 1./z[k]
				zsin_1[j,k] = 1./(z[k]*sin_y[j])
				cotth[j,k] = cos_y[j]/sin_y[j]
		
		curl_x +=  a2*z_1 + 2.*da2dz - da3dy*(1. + z_1)
		curl_y += -a1*z_1 - 2.*da1dz
		curl_z += da1dy*(z_1 + 1.) + a1*cotth*z_1  
	
	return curl_x, curl_y, curl_z
