# read eulag strat: GM
from scipy.io import FortranFile
from eulag.files.grid import read_grid
from eulag.files.modules import filter3D
from eulag.files.eudyn import parameters
import numpy as N
import subprocess

def read_f11(*args, **kwargs):
        """Read fort.11, short tape from eulag.
           call using
           import eulag as E
           f = read_f11()
           f will be an object with the variables

           Parameters:
           ifl = True:  filters the data
               = False: read without filtering
           skip: =1 (default) reads all outputs, otherwise
                  jumps skip times before reading
        """
        return Rbox(*args, **kwargs)

class Rbox(object):
    def __init__ (self, datafile='',vtype='f', ifl = True, skip=1):
        # Read xaverages file
        # Datafile is the name of the xaverages file to be read
        # ifl can be False or True, default is True
        if (not datafile):
            datafile='fort.11'

        param = parameters()
        if param[3] == 0:         # For spherical simulations
            (n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver,\
                    nslice, nplot, nstore, nxaver, it, rb, rt) = param
        else:
            print ('This is a cartesian simulation.')
            (n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver, \
            nslice, nplot, nstore, nxaver, it) = param

        cmd = "grep 'MHD' define.h"
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        temp = process.communicate()[0]
        MHD = int(temp.split()[2])

        if MHD == 1:
            print ('MHD config.')
        else:
            print ('HD config.')

        ''' Defining variables '''
        counter = 0
        counter_total = 0
        tmp = 0
        u  = []
        v  = []
        w  = []
        the  = []
        P  =  []
        if MHD == 1:
            bx  = []
            by  = []
            bz  = []

        ''' reading the file '''
        f11 = FortranFile(datafile)
        while True:
            try:
                #print(counter)
                if counter % skip !=0:
                    # Reading but not saving
                    tmp = N.reshape(f11.read_reals(vtype), (n, m, l), order = 'F')
                    tmp = N.reshape(f11.read_reals(vtype), (n, m, l), order = 'F')
                    tmp = N.reshape(f11.read_reals(vtype), (n, m, l), order = 'F')
                    tmp = N.reshape(f11.read_reals(vtype), (n, m, l), order = 'F')
                    tmp = N.reshape(f11.read_reals(vtype), (n, m, l), order = 'F')
                    if MHD == 1:
                        tmp = N.reshape(f11.read_reals(vtype), (n, m, l), order = 'F')
                        tmp = N.reshape(f11.read_reals(vtype), (n, m, l), order = 'F')
                        tmp = N.reshape(f11.read_reals(vtype), (n, m, l), order = 'F')
                   
                else:
                    ''' Reading variables '''
                    # Velocity field
                    u.append(N.reshape(f11.read_reals(vtype), (n, m, l), order = 'F'))
                    v.append(N.reshape(f11.read_reals(vtype), (n, m, l), order = 'F'))
                    w.append(N.reshape(f11.read_reals(vtype), (n, m, l), order = 'F'))
                    # Potential Temperature and pressure
                    the.append(N.reshape(f11.read_reals(vtype), (n, m, l), order = 'F'))
                    P.append(N.reshape(f11.read_reals(vtype), (n, m, l), order = 'F'))
                    if MHD == 1:
                        # Magnetic field
                        bx.append(N.reshape(f11.read_reals(vtype), (n, m, l), order = 'F'))
                        by.append(N.reshape(f11.read_reals(vtype), (n, m, l), order = 'F'))
                        bz.append(N.reshape(f11.read_reals(vtype), (n, m, l), order = 'F'))
                counter += 1
            except:
                break

        self.time = N.arange(counter,step = skip)*dplot*dt

        print ('There are %i outputs in %s, only %i saved ' % (counter, datafile, len(N.array(u)[:,0,0,0])))
        print('Timestep =  %8.2f, saved at each %i times' % (dt, skip*dplot))
        print('Total time = %10.2f' % (dt*(len(N.array(u)[:,0,0,0])-1)*skip*dplot))

        uf = N.array(u) 
        vf = N.array(v)
        wf = N.array(w) 
        thef = N.array(the) 
        Pf = N.array(P) 
        del u, v, w, the

        if MHD == 1:
            bxf = N.array(bx)
            byf = N.array(by)
            bzf = N.array(bz)
            del bx, by, bz

        if ifl:
            for i in range(len(uf[:,0,0,0])):
                uf[i,:,:,:] = filter3D(3, uf[i, :, :, :], n, m, l, 1)
                vf[i,:,:,:] = filter3D(3, vf[i, :, :, :], n, m, l, 1)
                wf[i,:,:,:] = filter3D(3, wf[i, :, :, :], n, m, l, 0)
                thef[i,:,:,:] = filter3D(3, thef[i, :, :, :], n, m, l, 1)
                Pf[i,:,:,:] = filter3D(3, Pf[i, :, :, :], n, m, l, 1)
                if MHD == 1:
                    bxf[i,:,:,:] = filter3D(3, bxf[i, :, :, :], n, m, l, 1)
                    byf[i,:,:,:] = filter3D(3, byf[i, :, :, :], n, m, l, 1)
                    bzf[i,:,:,:] = filter3D(3, bzf[i, :, :, :], n, m, l, 0)
        else:
            print ('Filter OFF')

        self.u = uf
        self.v = vf
        self.w = wf
        self.the = thef
        self.p = Pf
        if MHD == 1:
            self.bx = bxf
            self.by = byf
            self.bz = bzf
        print ('Deleting unnecessary files')
        del uf,vf,wf,thef,Pf,tmp
        if MHD == 1:
            del bxf,byf,bzf

