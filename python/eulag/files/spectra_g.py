import shtns
import eulag as E
import numpy as N

def power_law(vx, vy, vz, nn):
    # Computes the power law with a mean in the nn time steps
    # > vx: phi field
    # > vy: theta field
    # > vz: radial field
    # > nn: number of time steps in temporal mean
    g = E.read_grid()
    nk = int(N.sqrt(g.n**2 + g.m**2)/2)
    k = N.zeros(nk)
    kx = N.roll(N.arange(-(g.n/2-1), g.n/2 + 1), g.n/2 + 1)
    ky = N.roll(N.arange(-(g.m/2-1), g.m/2 + 1), g.m/2 + 1)
    spectrum = N.zeros((nk, nn))
    for it in range(nn): # Temporal loop for mean
        for iy in range(g.m):
            vx_fft = N.fft.fft(vx[-it, :, iy])
            rvx = vx_fft.real
            ivx = vx_fft.imag
            vy_fft = N.fft.fft(vy[-it, :, iy])
            rvy = vy_fft.real
            ivy = vy_fft.imag
            vz_fft = N.fft.fft(vz[-it, :, iy])
            rvz = vz_fft.real
            ivz = vz_fft.imag
            for ix in range(g.n):
                ik = int(N.sqrt(kx[ix]**2)) #  + ky[iy]**2
                if ik < (g.n/2):
                    k[ik] = ik
                    spectrum[ik, it] += 0.5*(rvx[ix]**2 + ivx[ix]**2  
                                           + rvy[ix]**2 + ivy[ix]**2   
                                           + rvz[ix]**2 + ivz[ix]**2)
    # Time averaged spectrum
    spectrum = spectrum.mean(axis = 1) 
    cut_spec = spectrum[k!=0]
    new_spec = N.zeros(len(cut_spec)+1)
    new_spec[0] = spectrum[0]
    new_spec[1::] = cut_spec
    return k[0:len(new_spec)], new_spec 

def kspectra(f, ir, nn):
    # Fixed radii
    vx = 0.*f.u[..., ir]
    vy = 0.*f.v[..., ir]
    vz = f.w[..., ir]
    k, spectrum = power_law(vx, vy, vz, nn = nn)
    return k, spectrum

def mspectra(f, ir, nn):
    # Fixed radii
    bx = f.bx[..., ir]
    by = f.by[..., ir]
    bz = f.bz[..., ir]
    k, spectrum = power_law(bx, by, bz, nn = nn)
    return k, spectrum
