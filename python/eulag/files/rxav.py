from scipy.io import FortranFile
from eulag.files.grid import read_grid
from  eulag.files.modules import filter3D
import numpy as N

def rxav(*args, **kwargs):
	"""Read xaverages.dat, short tape from eulag.
	   call using
	   import eulag as E
	   f = rxav()
	   f will be an object with the variables
	"""
	return Read_xaverages(*args, **kwargs)

class Read_xaverages(object):
	def __init__(self, datafile = ''):
		self.datafile = datafile
		if (not datafile):
		    self.datafile = 'xaverages.dat'
		g = read_grid()
		n = g.n
		m = g.m
		l = g.l
		try:
			print ("Reading xaverages file...")
			
			# List with the variables
			n_variables = 27
			data = [[] for i in range(n_variables)]
			
			''' Reading data from xaverages '''
			data_file = open(self.datafile, 'rb')
			counter = 0
			while True:
				try:
					for j in range(0, n_variables):
						N.fromfile(data_file, dtype = 'uint32', count = 1)
						data[j].append(N.reshape(N.fromfile(data_file, dtype = 'float64', count = m*l), (m, l), order = 'F'))
						N.fromfile(data_file, dtype = 'uint32', count = 1)
						N.fromfile(data_file, dtype = 'uint32', count = 1)
					counter += 1
				except:
					break
			data_file.close()
			print ('There is %i outputs in %s ' % (counter, self.datafile))
		except ValueError:
		    print("xaverages file doesn't exist")
		thew, u, v, w, ox , oy , oz, u2, v2, w2, ox2, oy2, oz2, rwv, rwu, rvu, bx, by, bz, bx2, by2, bz2 , bzby, bzbx, bxby, P, the = data
		self.time = N.arange(counter)*g.t_xav
		
		self.thew = N.array(thew)[0:counter,...]   # Enthalpy Flux
		self.u = N.array(u)[0:counter,...]
		self.v = N.array(v)[0:counter,...]
		self.w = N.array(w)[0:counter,...]         # Velocity field (phi, theta, r)
		self.ox = N.array(ox)[0:counter,...]
		self.oy = N.array(oy)[0:counter,...]
		self.oz = N.array(oz)[0:counter,...]
		self.u2 = N.array(u2)[0:counter,...]
		self.v2 = N.array(v2)[0:counter,...]
		self.w2 = N.array(w2)[0:counter,...]       # Square Velocity Field
		self.ox2 = N.array(ox2)[0:counter,...]
		self.oy2 = N.array(oy2)[0:counter,...]
		self.oz2 = N.array(oz2)[0:counter,...]
		self.rwv = N.array(rwv)[0:counter,...]
		self.rwu = N.array(rwu)[0:counter,...]
		self.rvu = N.array(rvu)[0:counter,...]
		self.bx = N.array(bx)[0:counter,...]
		self.by = N.array(by)[0:counter,...]
		self.bz = N.array(bz)[0:counter,...]       # Magnetic field
		self.bx2 = N.array(bx2)[0:counter,...]
		self.by2 = N.array(by2)[0:counter,...]
		self.bz2 = N.array(bz2)[0:counter,...]     # Square Magnetic field
		self.bzby = N.array(bzby)[0:counter,...]
		self.bzbx = N.array(bzbx)[0:counter,...]
		self.bxby = N.array(bxby)[0:counter,...]
		self.P = N.array(P)[0:counter,...]         # Pressure
		self.the = N.array(the)[0:counter,...]     # Potential Temperature
