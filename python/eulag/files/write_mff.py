#from astropy.io import ascii
from scipy import signal
import scipy as S
import numpy as N
from copy import deepcopy as dc
from eulag.files.congrid import congrid

def write_mff_omega(field, filename):
	''' Write matriz in the correct format for the mean field model'''
	normalized = dc(field)
	n = field.shape[0]
	for i in range(0, n/2):
		normalized[n-1-i, :] = field[i, :]
	constant = N.max(N.abs(normalized))
	    
	#normalized = congrid(normalized/constant,(128,128),minusone=True)
	normalized = normalized/constant
	normalized_fil = S.ndimage.filters.uniform_filter(normalized)
	
	ascii.write(normalized_fil, filename)	
	return normalized_fil,normalized

def write_mff_alpha(field, filename):
	''' Write matriz in the correct format for the mean field model'''
	normalized = dc(field)
	n = field.shape[0]
	for i in range(0, n/2):
		normalized[n-1-i, :] = - field[i, :]
	constant = N.max(N.abs(normalized))
	#normalized = congrid(normalized/constant,(128,128),minusone=True)
	normalized = normalized/constant
	normalized_fil = S.ndimage.filters.uniform_filter(normalized)
	
	ascii.write(normalized_fil, filename)	
	return normalized_fil,normalized


