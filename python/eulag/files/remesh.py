import eulag as E
import numpy as N
from scipy.ndimage import zoom
from scipy.io import FortranFile
#from fortranfile import *
#from scipy.io import FortranFile

def remeshf9(f,nn=2,mm=2,ll=2):
    ''' 
    Re-scales the simulation output by a given factor, and then writes the new data to a file
    fort.10, which can be used to re-start a simulation with large resolution.
    IN.t:
    f: object containing the variables
    g: object containing original grid structure
    factor: re-scaling factor, by default it doubles the resolution
    '''
      
    print('scaling factor =',nn,mm,ll)

    ru    = zoom(  f.u[-1,...],(nn,mm,ll))
    rv    = zoom(  f.v[-1,...],(nn,mm,ll))
    rw    = zoom(  f.w[-1,...],(nn,mm,ll))
    rox   = zoom( f.ox[-1,...],(nn,mm,ll))
    roy   = zoom( f.oy[-1,...],(nn,mm,ll))
    roz   = zoom( f.oz[-1,...],(nn,mm,ll))
    rox2  = zoom(f.ox2[-1,...],(nn,mm,ll))
    roy2  = zoom(f.oy2[-1,...],(nn,mm,ll))
    roz2  = zoom(f.oz2[-1,...],(nn,mm,ll))
    rthe  = zoom(f.the[-1,...],(nn,mm,ll))
    rp    = zoom(  f.p[-1,...],(nn,mm,ll))
    rfx   = zoom( f.fx[-1,...],(nn,mm,ll))
    rfy   = zoom( f.fy[-1,...],(nn,mm,ll))
    rfz   = zoom( f.fz[-1,...],(nn,mm,ll))
    rft   = zoom( f.ft[-1,...],(nn,mm,ll))
#    rpm   = zoom( f.pm[-1,...],(nn,mm,ll))
#    rbx   = zoom( f.bx[-1,...],(nn,mm,ll))
#    rby   = zoom( f.by[-1,...],(nn,mm,ll))
#    rbz   = zoom( f.bz[-1,...],(nn,mm,ll))
#    rfbx  = zoom(f.fbx[-1,...],(nn,mm,ll))
#    rfby  = zoom(f.fby[-1,...],(nn,mm,ll))
#    rfbz  = zoom(f.fbz[-1,...],(nn,mm,ll))
    rtke  = zoom(f.tke[-1,...],(nn,mm,ll))
    rchm  = zoom(f.chm[-1,...],(nn,mm,ll))
    rfchm = zoom(f.fchm[-1,...],(nn,mm,ll))

    outputdata = ru,rv,rw,rox,roy,roz,rox2,roy2,roz2,rthe,rp,rfx,rfy,\
                 rfz,rft,rtke,rchm,rfchm,f.tmp
		 #rpm,rbx,rby,rbz,rfbx,rfby,rfbz,\

    return outputdata

def writef9(outputdata):
	"""
	Creates a new binary file fort.9.n
	using as "iN.tdata" a list of matrices
	with the same order given by  readf9.
	------------------------------------------------------------
	returns : Nothing 
	WARNING = the iN.tdata needs to be in the same readf9 order
	"""
#	itt, nt, n, m, l, Lx00, Ly00, Lz00, dt00, nslice, noutp, N.ot, nstore, nxaver \
#	 = settings(folder)
	u,v,w,ox,oy,oz,ox2,oy2,oz2,th,p,fx,fy,fz,ft,tke,chm,fchm,time = outputdata
	#pm,bx,by,bz,fbx,fby,fbz, 
        
#	print "i target folder : ", folder
#	print "parameters: itt, nt, n, m, l, Lx00, Ly00, Lz00, dt00, nslice, noutp, N.ot, nstore, nxaver " 
#	print "values: ", itt, nt, n, m, l, Lx00, Ly00, \
#		Lz00, dt00, nslice, noutp, N.ot, nstore, nxaver
	
	newfile="fort.9.n"	
	print ("writting new fort.9.n file ... ")
	with FortranFile(newfile,mode='w') as ndata:	
	#with open(newf, 'wb') as newfile:
		# open new class for fort.9 file
		# ndata=FortranFile(newfile)
		# reconstruct original data for binary file
		ru=N.ravel(u, order = 'F')
		rv=N.ravel(v, order = 'F')
		rw=N.ravel(w, order = 'F')
		rox=N.ravel(ox, order = 'F')
		roy=N.ravel(oy, order = 'F')
		roz=N.ravel(oz, order = 'F')
		rox2=N.ravel(ox2, order = 'F')
		roy2=N.ravel(oy2, order = 'F')
		roz2=N.ravel(oz2, order = 'F')
		rth=N.ravel(th, order = 'F')
		rp=N.ravel(p, order = 'F')
		rfx=N.ravel(fx, order = 'F')
		rfy=N.ravel(fy, order = 'F')
		rfz=N.ravel(fz, order = 'F')
		rft=N.ravel(ft, order = 'F')

		#rpm=N.ravel(pm, order = 'F')
		#rbx=N.ravel(bx, order = 'F')
		#rby=N.ravel(by, order = 'F')
		#rbz=N.ravel(bz, order = 'F')
		#rfbx=N.ravel(fbx, order = 'F')
		#rfby=N.ravel(fby, order = 'F')
		#rfbz=N.ravel(fbz, order = 'F')

		rtke=N.ravel(tke, order = 'F')

		rchm=N.ravel(chm, order = 'F')
		rchm=N.ravel(chm, order = 'F')
		rfchm=N.ravel(fchm, order = 'F')
		rfchm=N.ravel(fchm, order = 'F')
		# write to new file in the exact same order of old fort.9

		ndata.write_record(ru,'f' )
		ndata.write_record(rv,'f' )
		ndata.write_record(rw,'f' )
		ndata.write_record(rox,'f')
		ndata.write_record(roy,'f')
		ndata.write_record(roz,'f')
		ndata.write_record(rox2,'f')
		ndata.write_record(roy2,'f')
		ndata.write_record(roz2,'f')
		ndata.write_record(rth,'f')
		ndata.write_record(rp,'f')
		ndata.write_record(rfx,'f')
		ndata.write_record(rfy,'f')
		ndata.write_record(rfz,'f')
		ndata.write_record(rft,'f')
                # 3 spaces
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
                # 
		#ndata.write_record(rpm,'f')
		#ndata.write_record(rbx,'f')
		#ndata.write_record(rby,'f')
		#ndata.write_record(rbz,'f')
		#ndata.write_record(rfbx,'f')
		#ndata.write_record(rfby,'f')
		#ndata.write_record(rfbz,'f')
		#
                #ndata.write_record(rbx1,'f')
		#ndata.write_record(rby1,'f')
		#ndata.write_record(rbz1,'f')
		#ndata.write_record(rbxe,'f')
		#ndata.write_record(rbye,'f')
		#ndata.write_record(rbze,'f')
                # no-MHD 13
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
                # 10 more
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')

		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
		ndata.write_record(time,'f')
                #
		ndata.write_record(rtke,'f')
                # 1 more
		ndata.write_record(time,'f')
                # 
		ndata.write_record(rchm,'f')
		ndata.write_record(rchm,'f')
		ndata.write_record(rfchm,'f')
		ndata.write_record(rfchm,'f')

	return None
