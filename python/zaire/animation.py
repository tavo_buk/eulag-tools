"""
A simple example of an animated plot
Define f = rxav or f = read_f11
"""
import matplotlib.animation as animation
import matplotlib
from matplotlib import rc
from scipy.integrate import simps
from mpl_toolkits.axes_grid1 import make_axes_locatable
from numpy import sin, cos, pi
from pylab import meshgrid
################################################################################
# PLOT STYLE #

# Choose Computer Modern Roman fonts by default
matplotlib.rcParams['font.serif'] = 'cmr10'
matplotlib.rcParams['font.sans-serif'] = 'cmr10'

font = { 'size' : 20}
rc('font', **font)
rc('xtick', labelsize = 20)
rc('ytick', labelsize = 20)

legend = {'fontsize': 20}
rc('legend',**legend)
axes = {'labelsize': 20}
rc('axes', **axes)
rc('mathtext',fontset='cm')
#use this, but at the expense of slowdown of rendering
rc('text', usetex = True)
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]
################################################################################

def anim(bx, by, bz):
	while True:
		if (len(bx.shape) != 3) and (len(bx.shape) != 4):
			break
		elif (len(bx.shape) != 3):
			bx = N.mean(bx, axis = 1) # Azimuthal mean
			by = N.mean(by, axis = 1) # Azimuthal mean
			bz = N.mean(bz, axis = 1) # Azimuthal mean
		else:
			pass
		convert = 10. # kilo gauss
		bx = bx*convert
		by = by*convert
		bz = bz*convert
		g = E.read_grid()
		X = g.x
		Y = g.y
		rsun = 6.96e8 
		g.rf = g.r/rsun
		stratification = N.genfromtxt('strat.dat')
		rho_ad = stratification[:, 5] # Isentropic density
		nt = bx.shape[0]
		global stream1
		stream1 = N.ones((nt, g.m, g.l))
		stream2 = N.ones((nt, g.m, g.l))
		R1, TH1 = meshgrid(g.r, g.th)
		for t in range(nt):
			c1 = g.x*bz[t,...]
			c2 = R1*by[t,...]
			for j in range(g.l-1):
				for i in range(g.m-3):
					ii = i + 2
					stream1[t, i, j] = simps(c1[0:ii, j], g.th[0:ii])
			for j in range(g.l-3):
				for i in range(g.m-1):
					jj = j + 2
					stream2[t, i, j] = simps(c2[i, 0:jj], g.r[0:jj])
		#######  PLOT  #######
		global omin; omin = bx.min()
		global omax; omax = bx.max()
		if abs(omin) < abs(omax):
			omin = - abs(omax)
		else:
			omax = abs(omin)
		stream1 /= N.sin(TH1)
		stream2 /= -R1	
		stream = (stream1 + stream2)/2.
		smin = (stream)[:, 2:-2,:].min()
		smax = (stream)[:, 2:-2,:].max()
		global lev; lev = [0.8*smin, 0.7*smin, 0.6*smin, 0.5*smin, 0.4*smin, 0.2*smin, 0.1*smin, 0.1*smax,  0.2*smax, 0.4*smax, 0.5*smax, 0.6*smax, 0.7*smax, 0.8*smax]
		print (smin, smax)

		for i in range(nt):
			fig, ax = P.subplots(nrows = 1, ncols = 1)
			matplotlib.rcParams['contour.negative_linestyle'] = 'dashed'

			X1 = X[2:-2,:]
			Y1 = Y[2:-2,:]
			cs = ax.contourf(X1, Y1, bx[i,2:-2,:], levels = N.linspace(omin, omax, 60), interpolation = 'bicubic', cmap = P.get_cmap("seismic"))
			ax.contour(X1, Y1, (stream[i,2:-2,:]), levels = lev, colors = "k")

			# Add colorbar
			tick = N.linspace(omin, omax, num = 6, endpoint = True)
			divider = make_axes_locatable(ax)
			cax = divider.append_axes("left", size = "3%", pad = .08)
			cbar = fig.colorbar(cs, cax = cax, ticks = tick, ticklocation = 'left', format = '%1.0f')
			cbar.ax.set_ylabel(r"$B_{\phi} [\mathrm{kG}]$", fontsize = 25)
			cbar.ax.tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 6, width = 2, length = 6)

			# Make sure aspect ratio preserved
			ax.set_aspect('equal')

			# Time text
			ax_text = fig.add_axes([0.75, 0.8, 0.05, 0.1])
			time_text = ax_text.text(0.,0., r'$Time  = %1.0f \ [\mathrm{years}]$' %(f.time[i]/(365.*24.*3600.)), fontsize = 18)

			# Turn off rectangular frame.
			ax.set_frame_on(False)
			ax_text.set_frame_on(False)

			# Turn off axis ticks.
			ax.set_xticks([])
			ax.set_yticks([])
			ax_text.set_xticks([])
			ax_text.set_yticks([])

			# Draw a circle around the edge of the plot.
			rmax = max(g.r)
			rmin = min(g.r)
			ax.plot(rmax*cos(g.th - pi/2.), rmax*sin(g.th - pi/2.), 'k')
			ax.plot(rmin*cos(g.th - pi/2.), rmin*sin(g.th - pi/2.), 'k')
			P.draw()
			P.savefig('fig%03d' % i,  transparent = True)
			P.show(False)
			P.close()
		break

anim(f.bx, f.by, f.bz)
