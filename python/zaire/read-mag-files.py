import matplotlib.pylab as P
import numpy as N
import eulag as E
import os
from matplotlib.ticker import FormatStrFormatter

# Read magnetic files to plot butterfly diagrams

root_dir = "/home/DATA/Solar"
LIST = {"/TAC/tac-7-rf/":(.95,.60,.75,.97), 
        "/TAC/tac-14-rf/":(.95,.60,.80,.97), 
        "/TAC/tac-21-rf/":(.97,.60,.92,.99),
        "/TAC/tac-28-rf/":(.99,.60,.89,.92), 
        "/TAC/tac-35-rf/":(.98,.60,.95,.98), 
        "/TAC/tac-56-rf/":(.97,.50,.88,.99),
        "/TAC/tac-112-rf/":(.99,.90,.99,.92), 
        "/TAC/tac-224-rf/":(.80,.30,.80,.75)}
nt_LIST = {"/NOTAC_rf/nt-07-rf/":(.85,.40,.55,.65), 
           "/NOTAC_rf/nt-14-rf/":(.97,.40,.50,.65),
           "/NOTAC_rf/nt-28-rf/":(.90,.50,.70,.40), 
           "/NOTAC_rf/nt-35-rf/":(.90,.50,.35,.60), 
           "/NOTAC_rf/nt-42-rf/":(.90,.50,.80,.70), 
           "/NOTAC_rf/nt-56-rf/":(.91,.90,.78,.80), 
           "/NOTAC_rf/nt-112-rf/":(.70,.70,.80,.80)}

bxm = []
bym = []
bzm = []
nt_bxm = []
nt_bym = []
nt_bzm = []

def butterfly(bxm, bzm, t_f11, Prot, name, adjust_color):
    ## Change units Tesla to Gauss
    bxm = bxm*1.e4
    bzm = bzm*1.e4

    theta = N.linspace(-90, 90, num = bxm.shape[1], endpoint = True)
    r = N.linspace(0.61, 0.96, num = bxm.shape[2], endpoint = True)
    year = 365.*24.*3600.  	# Year in seconds
    outputs_f11 = bxm.shape[0]
    time = N.multiply(t_f11,range(outputs_f11))/year	# Time in years
    fig, axes = P.subplots(nrows = 2, ncols = 2, figsize = (20,10), dpi = 50, facecolor = None, edgecolor = None, linewidth = None, sharex = True)

    fig.suptitle('Period of rotation = '+Prot+' days', size=25)
    ir1 = -3 # Fixed radius in butterfly diagram
    bx_min = bxm[:, :, ir1].min()
    bx_max = bxm[:, :, ir1].max()
    if abs(bx_min) > abs(bx_max):
        bx_max = bx_max*abs(bx_min)/abs(bx_max)
    else:
        bx_min = bx_min*abs(bx_max)/abs(bx_min)
    levels1 = N.linspace(bx_min, bx_max, 60, endpoint = True)*adjust_color[0]
    im1 = axes[0, 0].contourf(time, theta, N.rot90(bxm[:,:,ir1]), levels = levels1 , interpolation = 'bicubic', cmap = P.get_cmap("seismic"))  # atencao para a rotacao necessaria na matriz
    axes[0, 0].set_ylabel(r'$90^{\circ} - \theta$', fontsize = 30)
    axes[0, 0].set_yticks([90, 45, 0, -45, -90])
    axes[0, 0].set_yticklabels([r"$90^{\circ}$",r"$45^{\circ}$",r"$0^{\circ}$",r"$-45^{\circ}$",r"$-90^{\circ}$"])
    axes[0, 0].tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 10, width = 2, length = 6)

    tick1 = N.linspace(bx_min, bx_max, 6)*adjust_color[0] 
    cbar_ax1 = fig.add_axes([0.09, 0.54, 0.01, 0.36])
    fig.colorbar(im1, ticks = tick1, cax = cbar_ax1, ticklocation='left', format = '%1.0f')
    cbar_ax1.set_ylabel(r"$ B_{\phi} \mathrm{[G]}$", fontsize = 30)
    cbar_ax1.tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 6, width = 2, length = 6)

    ###################################################################################

    br_min = bzm[:, :, ir1].min()
    br_max = bzm[:, :, ir1].max()
    if abs(bx_min) > abs(bx_max):
        br_max = br_max*abs(br_min)/abs(br_max)
    else:
        br_min = br_min*abs(br_max)/abs(br_min)
    levels2 = N.linspace(br_min, br_max, 60, endpoint = True)*adjust_color[1]
    im2 = axes[0, 1].contourf(time, theta, N.rot90(bzm[:, :, ir1]), levels = levels2 , interpolation = 'bicubic', cmap = P.get_cmap("seismic"))
    axes[0, 1].set_ylabel(r'$90^{\circ} - \theta$', fontsize = 30)
    axes[0, 1].set_yticks([90, 45, 0, -45, -90])
    axes[0, 1].set_yticklabels([r"$90^{\circ}$",r"$45^{\circ}$",r"$0^{\circ}$",r"$-45^{\circ}$",r"$-90^{\circ}$"])
    axes[0, 1].tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 10, width = 2, length = 6)

    tick2 = N.linspace(br_min, br_max, 6)*adjust_color[1] 
    cbar_ax2 = fig.add_axes([0.58, 0.54, 0.01, 0.36])
    fig.colorbar(im2, ticks = tick2, cax = cbar_ax2, ticklocation='left', format = '%1.0f')
    cbar_ax2.set_ylabel(r"$ B_r \mathrm{[G]}$", fontsize = 30)
    cbar_ax2.tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 10, width = 2, length = 6)

    ###################################################################################
    nthe = N.linspace(90., -90., 64)
    jth1, = N.where((nthe > 15) & (nthe < 25))
    mth = jth1[0] # angulo theta para grafico do campo em funcao do raio e do tempo
    bx_minr = bxm[:, mth,:].min()
    bx_maxr = bxm[:, mth,:].max()
    if abs(bx_minr) > abs(bx_maxr):
        bx_maxr = bx_maxr*abs(bx_minr)/abs(bx_maxr)
    else:
        bx_minr = bx_minr*abs(bx_maxr)/abs(bx_minr)
    levels3 = N.linspace(bx_minr, bx_maxr, 60, endpoint = True)*adjust_color[2]
    im3 = axes[1, 0].contourf(time, r, N.transpose(bxm[:, mth, :]), levels = levels3 , interpolation = 'bicubic', cmap = P.get_cmap("seismic"))
    axes[1, 0].set_xlabel(r'$\mathrm{Time \ [years]}$', fontsize = 30)
    axes[1, 0].set_ylabel(r'$\frac{r}{r_*}$', fontsize = 50)
    axes[1, 0].yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    axes[1, 0].set_yticks(N.linspace(.61, .96, 9, endpoint = True))
    axes[1, 0].tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 10, width = 2, length = 6)

    tick3 = N.linspace(bx_minr, bx_maxr, 6)*adjust_color[2] 
    cbar_ax3 = fig.add_axes([0.09, 0.09, 0.01, 0.36])
    fig.colorbar(im3, ticks = tick3, cax = cbar_ax3, ticklocation='left', format = '%1.0f')
    cbar_ax3.set_ylabel(r"$ B_{\phi} \mathrm{[G]}$", fontsize = 30)
    cbar_ax3.tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 6, width = 2, length = 6)

    ###################################################################################

    bz_minr = bzm[:, mth,:].min()
    bz_maxr = bzm[:, mth,:].max()
    if abs(bz_minr) > abs(bz_maxr):
        bz_maxr = bz_maxr*abs(bz_minr)/abs(bz_maxr)
    else:
        bz_minr = bz_minr*abs(bz_maxr)/abs(bz_minr)
    levels4 = N.linspace(bz_minr, bz_maxr, 60, endpoint = True)*adjust_color[3]
    im4 = axes[1, 1].contourf(time, r, N.transpose(bzm[:, mth, :]), levels = levels4 , interpolation = 'bicubic', cmap = P.get_cmap("seismic"))
    axes[1, 1].set_yticks(N.linspace(.61, .96, 9))
    axes[1, 1].yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    axes[1, 1].set_xlabel(r'$\mathrm{Time \ [years]}$', fontsize = 30)
    axes[1, 1].set_ylabel(r'$\frac{r}{r_*}$', fontsize = 50)
    axes[1, 1].tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 10, width = 2, length = 6)

    tick4 = N.linspace(bz_minr, bz_maxr, 6)*adjust_color[3] 
    cbar_ax4 = fig.add_axes([0.58, 0.09, 0.01, 0.36])
    fig.colorbar(im4, ticks = tick4, cax = cbar_ax4, ticklocation='left', format = '%1.0f')
    cbar_ax4.set_ylabel(r"$ B_{r} \mathrm{[G]}$", fontsize = 30)
    cbar_ax4.tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 10, width = 2, length = 6)

    fig_name = name + 'butterfly'+ str(Prot) + '.eps'
    fig.subplots_adjust(left = 0.20, right = .99, top = .9, bottom = .1, wspace = 0.67, hspace = .18)
    os.chdir(root_dir)
    P.savefig(fig_name, dpi = 50)
#    P.show()

for path1, color_lev in LIST.items():
    os.chdir(root_dir+path1)
    g = E.read_grid()
    t_f11 = (g.t_f11)
    Prot = int(path1.strip('/TAC/tac-').strip('-rf/'))
    bxm, bym, bzm = N.load(root_dir+path1+'butterfly.npy')
    butterfly(bxm, bzm, t_f11, str(Prot), '', color_lev)

for path2, nt_color_lev in nt_LIST.items():
    os.chdir(root_dir+path2)
    g = E.read_grid()
    t_f11 = (g.t_f11)
    Prot = int(path2.strip('/NOTAC_rf/nt-').strip('-rf/'))
    nt_bxm, nt_bym, nt_bzm = N.load(root_dir+path2+'butterfly.npy')
    butterfly(nt_bxm, nt_bzm, t_f11, str(Prot), 'nt_', nt_color_lev)
