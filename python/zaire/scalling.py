import matplotlib.pyplot as P
import numpy as N

period = []
urms = []
rhom = []
ro = []
bx_tac = []
bx_nsl = []
bp_tac = []
bp_nsl = []
domega = []
domega_omega = []
alpha_r1 = []
alpha_r2 = []
omr_r1 = []
omr_r2 = []
omth_r1 = []
omth_r2 = []
eta = []

table = open('/home/DATA/Solar/TAC/table.txt', 'r')
for line in table:
	columns = line.split()
	period.append(float(columns[0]))
	urms.append(float(columns[1]))
	rhom.append(float(columns[2]))
	ro.append(float(columns[3]))
	bx_tac.append(float(columns[4]))
	bx_nsl.append(float(columns[5]))
	bp_tac.append(float(columns[6]))
	bp_nsl.append(float(columns[7]))
	domega.append(float(columns[8]))
	domega_omega.append(float(columns[9]))
	alpha_r1.append(float(columns[10]))
	alpha_r2.append(float(columns[11]))
	omr_r1.append(float(columns[12]))
	omr_r2.append(float(columns[13]))
	omth_r1.append(float(columns[14]))
	omth_r2.append(float(columns[15]))
	eta.append(float(columns[16]))
table.close()

bx_tac = N.array(bx_tac)
bp_tac = N.array(bp_tac)
bx_nsl = N.array(bx_nsl)
bp_nsl = N.array(bp_nsl)

b_tac = N.sqrt(bx_tac**2 + bp_tac**2)
b_nsl = N.sqrt(bx_nsl**2 + bp_nsl**2)

P.scatter(N.log10(ro), N.log10(b_tac), c = 'k', s = 50)
P.scatter(N.log10(ro), N.log10(b_nsl), c = 'b', s = 50)
P.title('Tachocline Models', fontsize = 25, y = 1.05)
P.legend(('TAC', 'NSSL'), loc = 3, fontsize = 15)
P.xscale('linear')
P.yscale('linear')
P.tick_params(axis = 'both', direction = 'inout', labelsize = 15, colors = 'k')
P.xlabel(r'$log(Ro)$', labelpad = 10, fontsize = 28)
P.ylabel(r'$log(\left < B \right >)$', labelpad = 10, fontsize = 30)
P.subplots_adjust(left = .15, bottom = .15, right = .95, top = .90, wspace = 0, hspace = 0)       
P.savefig('fig1.eps', dpi = 100)
P.show()

P.scatter(N.log10(ro), N.log10(bx_nsl), c = 'k', s = 50)
P.scatter(N.log10(ro), N.log10(bp_nsl), c = 'b', s = 50)
#N.polyfit(N.log10(ro[-3::]), N.log10(bx_nsl[-3::])),1)
P.text(.8,-1., r'$ \left < B_{\phi} \right > \propto $ Ro $^{-1.54}$', fontsize = 18)
P.plot(N.log10(ro[-3::]), (-1.54*N.log10(ro[-3::]) + 0.0),'k-', lw=1)
#N.polyfit(N.log10(ro[-3::]), N.log10(bp_nsl[-3::])),1)
P.text(0.0,-2.5, r'$ \left < B_{\rm P} \right > \propto $ Ro $^{-1.71}$', fontsize = 18)
P.plot(N.log10(ro[-3::]), (-1.71*N.log10(ro[-3::]) - 1.0),'b-', lw=1)
P.title('Tachocline Models: Fields near the surface', fontsize = 25, y = 1.05)
P.legend((r'$log(\left <B_\phi \right >)$', r'$log(\left <B_{\rm P} \right >)$'), loc = 3, fontsize = 15)
P.xscale('linear')
P.yscale('linear')
P.tick_params(axis = 'both', direction = 'inout', labelsize = 15, colors = 'k')
P.xlabel(r'$log(Ro)$', labelpad = 10, fontsize = 28)
P.subplots_adjust(left = .10, bottom = .15, right = .95, top = .90, wspace=0, hspace=0)       
P.savefig('fig2.eps', dpi = 100)
P.show()

P.scatter(N.log10(ro), N.log10(bx_tac), c = 'k', s = 50)
P.scatter(N.log10(ro), N.log10(bp_tac), c = 'b', s = 50)
P.title('Tachocline Models: Fields at the tachocline', fontsize = 25, y = 1.05)
P.legend((r'$log(\left < B_\phi \right >)$', r'$log( \left < B_{\rm P} \right >)$'), loc = 3, fontsize = 15)
P.xscale('linear')
P.yscale('linear')
P.tick_params(axis = 'both', direction = 'inout', labelsize = 15, colors = 'k')
P.xlabel(r'$log(Ro)$', labelpad = 10, fontsize = 28)
P.subplots_adjust(left = .10, bottom = .15, right = .95, top = .90, wspace=0, hspace=0)       
P.savefig('fig3.eps', dpi = 100)
P.show()

P.scatter(N.log10(ro), N.log10(bx_tac), c = 'k', s = 50)
P.scatter(N.log10(ro), N.log10(bx_nsl), c = 'b', s = 50)
P.title('Tachocline Models', fontsize = 25, y = 1.05)
P.legend(('TAC', 'NSSL'), loc = 3, fontsize = 15)
P.xscale('linear')
P.yscale('linear')
P.tick_params(axis = 'both', direction = 'inout', labelsize = 15, colors = 'k')
P.xlabel(r'$log(Ro)$', labelpad = 10, fontsize = 28)
P.ylabel(r'$log(\left < B_\phi \right >)$', labelpad = 10, fontsize = 30)
P.subplots_adjust(left = .15, bottom = .15, right = .95, top = .90, wspace=0, hspace=0)       
P.savefig('fig4.eps', dpi = 100)
P.show()

P.scatter(N.log10(ro), N.log10(bp_tac), c = 'k', s = 50)
P.scatter(N.log10(ro), N.log10(bp_nsl), c = 'b', s = 50)
P.title('Tachocline Models', fontsize = 25, y = 1.05)
P.legend(('TAC', 'NSSL'), loc = 3, fontsize = 15)
P.xscale('linear')
P.yscale('linear')
P.tick_params(axis = 'both', direction = 'inout', labelsize = 15, colors = 'k')
P.xlabel(r'$log(Ro)$', labelpad = 10, fontsize = 28)
P.ylabel(r'$log(\left < B_{\rm P} \right >)$', labelpad = 10, fontsize = 30)
P.subplots_adjust(left = .15, bottom = .15, right = .95, top = .90, wspace=0, hspace=0)       
P.savefig('fig5.eps', dpi = 100)
P.show()
