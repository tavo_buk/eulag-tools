import matplotlib.pyplot as plt
import numpy as np
from ttauri import rxav, parameters
import matplotlib
from numpy import sin, cos, pi
from pylab import meshgrid
from scipy.integrate import simps

(n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver, nslice, nplot, nstore, nxaver, it, rb, rt) = parameters()
r = np.linspace(rb, rt, num = l)
rs = rb*10.

filename_xav = raw_input('Type the name of xaverages file (without quotes): ')
if not filename_xav:
    thew, u, v, w, ox , oy , oz, u2, v2, w2, ox2, oy2, oz2, rwv, rwu, rvu, bx, by, bz, bx2, by2, bz2 , bzby, bzbx, bxby, P, the = rxav()
else:
    thew, u, v, w, ox , oy , oz, u2, v2, w2, ox2, oy2, oz2, rwv, rwu, rvu, bx, by, bz, bx2, by2, bz2 , bzby, bzbx, bxby, P, the = rxav(filename_xav)

cut = 5. # Degrees to be cut off
good = int(cut*m/180.)
theta = np.linspace(0, pi, num = m)
theta_good = np.linspace(0+(cut/90)*pi/2., (1 - cut/180)*pi, num = m - 2*good)

R, TH = meshgrid(r, theta_good)
nn = input("Number of steps in mean = ")
U = np.mean(u[-nn::,:,:], axis = 0)
V = np.mean(v[-nn::,:,:], axis = 0)
W = np.mean(w[-nn::,:,:], axis = 0)

X = R*sin(TH)
Y = R*cos(TH)

Omega_0 = 2.*pi/(7.*24.*3600.)
Omega = U[good:-good, :]/X + Omega_0
rtheta = np.linspace(-90., 90., m)

fig, ax = plt.subplots(dpi = 250, facecolor = None, edgecolor = None, linewidth = None, tight_layout = True) # Another option is tight_layout = True
fig.set_size_inches(10, 13)
ax.plot(r/rs, Omega[32,:]*1.e9, c = 'k', linewidth = 2.5)
ax.plot(r/rs, Omega[39,:]*1.e9, c = 'm', linewidth = 2.5)
ax.plot(r/rs, Omega[47,:]*1.e9, c = 'b', linewidth = 2.5)
ax.plot(r/rs, Omega[54,:]*1.e9, c = 'g', linewidth = 2.5)
ax.legend([r'$\theta = 0^\circ$',r'$\theta = 20 ^\circ$', r'$\theta = 45 ^\circ$', r'$\theta = 65 ^\circ$'], loc = 'upper left', fontsize = 30, markerscale = 1.)
ax.set_xlim([0.1,0.95])
ax.set_xlabel(r"$\frac{r}{r_*}$", fontsize = 60, labelpad = 30, ha = 'center')
ax.set_ylabel(r"$\Omega \mathrm{[nHz]}$", fontsize = 45,  labelpad = 30, ha = 'center')
ax.tick_params(which='major', direction = 'inout', labelsize = 30, pad = 6, width = 2, length = 6)
ax.tick_params(which='minor', direction = 'inout', labelsize = 30, pad = 6, width = 1, length = 4)

plt.savefig('lat-omega.eps')
plt.savefig('lat-omega.png')
plt.show()
