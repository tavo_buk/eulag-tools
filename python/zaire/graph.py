import matplotlib.pyplot as plt
from matplotlib.ticker import LinearLocator
from ttauri import parameters
import ttauri as tt
import numpy as np
from scipy import meshgrid
from numpy import pi

(n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver, nslice, nplot, nstore, nxaver, it, rb, rt) = parameters()

TMC, TRS, TVISC = tt.reynolds_stress_box()


r = np.linspace(rb, rt, num = l)
theta = np.linspace(0, pi, num = m)

R, TH = meshgrid(r, theta[1:-1])

X = R*np.sin(TH)
Y = R*np.cos(TH)

fig, axes = plt.subplots(nrows = 1, ncols = 3, figsize = (16, 8), dpi = 120)
(ax1, ax2, ax3) = axes.flat

if (TMC.min() < TRS.min()) and (TMC.min() < TVISC.min()):
    min_level = TMC.min()
elif (TRS.min() < TMC.min()) and (TRS.min() < TVISC.min()):
    min_level = TRS.min()
else:
    min_level = TVISC.min()

if (TMC.max() >= TRS.max()) and (TMC.max() >= TVISC.max()):
    max_level = TMC.max()
elif (TRS.max() >= TMC.max()) and (TRS.max() >= TVISC.max()):
    max_level = TRS.max()
else:
    max_level = TVISC.max()

level = np.linspace(min_level*1.e-6, max_level*1.e-6, num = 64, endpoint = True)
tick = np.linspace(min_level*1.e-6, max_level*1.e-6, num = 6, endpoint = True)

im1 = ax1.contourf(X, Y, TMC*1.e-6, levels = level, interpolation = 'bicubic', cmap = plt.get_cmap('RdYlBu'))
im2 = ax2.contourf(X, Y, TRS*1.e-6, levels = level, interpolation = 'bicubic', cmap = plt.get_cmap('RdYlBu'))
im3 = ax3.contourf(X, Y, TVISC*1.e-6, levels = level, interpolation = 'bicubic', cmap = plt.get_cmap('RdYlBu'))

fig.subplots_adjust(left = 0.2, right = 1., wspace = 0.01)
cbar_ax = fig.add_axes([0.15, 0.1, 0.05, 0.8])
fig.colorbar(im3, ticks = tick, cax = cbar_ax, ticklocation='left')
cbar_ax.set_ylabel(r"$ \rm (10^6 Kg m^{-1} s^{-2})$", fontsize = 26)
cbar_ax.tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 6, width = 2, length = 6)

# Titles
ax1.set_title(r"$ \rm - \nabla . F^{MC}$", fontsize = 26)
ax2.set_title(r"$ \rm - \nabla . F^{RS}$", fontsize = 26)
ax3.set_title(r"$ \rm - \nabla . F^{TOT}$", fontsize = 26)

# Make sure aspect ratio preserved
ax1.set_aspect('equal')
ax2.set_aspect('equal')
ax3.set_aspect('equal')

# Turn off rectangular frame.
ax1.set_frame_on(False)
ax2.set_frame_on(False)
ax3.set_frame_on(False)

# Turn off axis ticks.
ax1.set_xticks([])
ax1.set_yticks([])
ax2.set_xticks([])
ax2.set_yticks([])
ax3.set_xticks([])
ax3.set_yticks([])

# Draw a circle around the edge of the plot.
rmax = max(r)
rmin = min(r)
ax1.plot(rmax * np.sin(theta), rmax * np.cos(theta), 'k')
ax1.plot(rmin * np.sin(theta), rmin * np.cos(theta), 'k')
ax1.plot(r * np.sin(min(theta)), r * np.cos(min(theta)), 'k')
ax1.plot(r * np.sin(max(theta)), r * np.cos(max(theta)), 'k')

ax2.plot(rmax * np.sin(theta), rmax * np.cos(theta), 'k')
ax2.plot(rmin * np.sin(theta), rmin * np.cos(theta), 'k')
ax2.plot(r * np.sin(min(theta)), r * np.cos(min(theta)), 'k')
ax2.plot(r * np.sin(max(theta)), r * np.cos(max(theta)), 'k')

ax3.plot(rmax * np.sin(theta), rmax * np.cos(theta), 'k')
ax3.plot(rmin * np.sin(theta), rmin * np.cos(theta), 'k')
ax3.plot(r * np.sin(min(theta)), r * np.cos(min(theta)), 'k')
ax3.plot(r * np.sin(max(theta)), r * np.cos(max(theta)), 'k')

# Draw a circle in the rad-conv interface
rad_core = raw_input('There is a radiative-convective interface? (y/n)')
if (rad_core == 'y') or (rad_core == 't') or (rad_core == 'true') or (rad_core == 'True') or (rad_core == 'yes'):
    ax1.plot(3 * rb * np.sin(theta), 3 * rb * np.cos(theta), '--k')
    ax2.plot(3 * rb * np.sin(theta), 3 * rb * np.cos(theta), '--k')
    ax3.plot(3 * rb * np.sin(theta), 3 * rb * np.cos(theta), '--k')
plt.savefig('stress.eps', dpi = 120)
plt.show()
