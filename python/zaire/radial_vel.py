import matplotlib.pyplot as plt
from pylab import meshgrid
from mpl_toolkits.basemap import Basemap
from eudyn import rxav
import numpy as np
from numpy import pi, cos, sin
import ttauri as tt
from matplotlib.ticker import FormatStrFormatter


(n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver, nslice, nplot, nstore, nxaver, it, rb, rt) = tt.parameters()

filename = "define.h"
imhd = open(filename)
for line in imhd:
	if line.split()[1] == "MHD":
		MHD = int(line.split()[2])       # If MHD = 0 there is no magnetic field

# Reading fort.11
data_rbox = tt.rbox()
if len(data_rbox) == 8:
	u, v, w, the, P, bx, by, bz = data_rbox
else:
	u, v, w, the, P = data_rbox

theta = np.linspace(-pi/2., pi/2, m)
phi = np.linspace(0, 2*pi, n)
r = np.linspace(0.1, 0.95, num = l, endpoint = True)
DEC,ASC = np.meshgrid(phi, theta)
RAD = 180./np.pi
R, PHI = meshgrid(r, phi)
X = R*cos(PHI)
Y = R*sin(PHI)


fig = plt.figure(figsize = (6,14), dpi = 200, facecolor = None, edgecolor = None, linewidth = None)

ir1 = r % .30
in1 = np.where( ir1 <= ir1.min()*1.1)[0].min()
ir2 = r % .60
in2 = np.where( ir2 <= ir2.min()*1.1)[0].min()
ir3 = r % .90
in3 = np.where( ir3 <= ir3.min()*1.1)[0].min()
#ith = theta % (23./RAD) 
#ith_aux = np.where( ith <= ith.min()*1.05)[0]
#inth = ith_aux[abs(len(ith_aux)/2. - 1)]
inth = m/2  

r1 = r[in1]
r2 = r[in2]
r3 = r[in3]
nth = theta[inth]*RAD

W = w[-2, :, :, :]
w1_min = W[:, :, in1].min()
w1_max = W[:, :, in1].max()
w2_min = W[:, :, in2].min()
w2_max = W[:, :, in2].max()
w3_min = W[:, :, in3].min()
w3_max = W[:, :, in3].max()
w4_min = W[:, inth, :].min()
w4_max = W[:, inth, :].max()

level1 = np.linspace(w1_min, w1_max, 60, endpoint = True)
level2 = np.linspace(w2_min, w2_max, 60, endpoint = True)
level3 = np.linspace(w3_min, w3_max, 60, endpoint = True)
level4 = np.linspace(w4_min, w4_max, 60, endpoint = True)

tick1 = np.linspace(w1_min, w1_max, 4, endpoint = True)
tick2 = np.linspace(w2_min, w2_max, 4, endpoint = True)
tick3 = np.linspace(w3_min, w3_max, 4, endpoint = True)
tick4 = np.linspace(w4_min, w4_max, 4, endpoint = True)

'''
# TT01Myr - HD
level1 = np.linspace(-130, 130, 60, endpoint = True)
level2 = np.linspace(-145, 145, 60, endpoint = True)
level3 = np.linspace(-155, 155, 60, endpoint = True)
level4 = np.linspace(-130, 130, 60, endpoint = True)

tick1 = np.linspace(-130, 130, 4, endpoint = True)
tick2 = np.linspace(-145, 145, 4, endpoint = True)
tick3 = np.linspace(-155, 155, 4, endpoint = True)
tick4 = np.linspace(-130, 130, 4, endpoint = True)
'''
# TT01Myr - MHD
level1 = np.linspace(-170, 170, 60, endpoint = True)
level2 = np.linspace(-220, 220, 60, endpoint = True)
level3 = np.linspace(-170, 170, 60, endpoint = True)
level4 = np.linspace(-200, 200, 60, endpoint = True)

tick1 = np.linspace(-170, 170, 4, endpoint = True)
tick2 = np.linspace(-220, 220, 4, endpoint = True)
tick3 = np.linspace(-175, 170, 4, endpoint = True)
tick4 = np.linspace(-200, 200, 4, endpoint = True)
'''
# TT14Myr - HD
level1 = np.linspace(-110, 100, 60, endpoint = True)
level2 = np.linspace(-260, 180, 60, endpoint = True)
level3 = np.linspace(-60, 60, 60, endpoint = True)
level4 = np.linspace(-245, 200, 60, endpoint = True)

tick1 = np.linspace(-110, 100, 4, endpoint = True)
tick2 = np.linspace(-260, 180, 4, endpoint = True)
tick3 = np.linspace(-60, 60, 4, endpoint = True)
tick4 = np.linspace(-245, 200, 4, endpoint = True)

# TT14Myr - MHD
level1 = np.linspace(-150, 140, 60, endpoint = True)
level2 = np.linspace(-230, 160, 60, endpoint = True)
level3 = np.linspace(-240, 210, 60, endpoint = True)
level4 = np.linspace(-180, 150, 60, endpoint = True)

tick1 = np.linspace(-150, 140, 4, endpoint = True)
tick2 = np.linspace(-230, 160, 4, endpoint = True)
tick3 = np.linspace(-240, 210, 4, endpoint = True)
tick4 = np.linspace(-180, 150, 4, endpoint = True)
'''

cbar_ax1 = fig.add_axes([0.20, 0.78, 0.6, 0.01])
cbar_ax2 = fig.add_axes([0.20, 0.53, 0.6, 0.01])
cbar_ax3 = fig.add_axes([0.20, 0.28, 0.6, 0.01])
cbar_ax4 = fig.add_axes([0.20, 0.03, 0.6, 0.01])

##################################################################################

ax1 = fig.add_subplot(411)
maps = Basemap(projection='moll', lon_0=0, resolution='c')
im1 = maps.contourf(DEC*RAD, ASC*RAD, np.transpose(W[:,:,in1]), levels = level1, cmap = plt.get_cmap("RdYlBu"),latlon = True)
fig.colorbar(im1, ticks = tick1, cax = cbar_ax1, format = '%1d', orientation = 'horizontal')
cbar_ax1.set_ylabel(r"$ u_\mathrm{r} (r = %.2f r_*) \mathrm{[m/s]}$" %r1, rotation = 0, fontsize = 30)
cbar_ax1.yaxis.set_label_coords(.5,.90)
cbar_ax1.tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 6, width = 2, length = 6)

##################################################################################

ax2 = fig.add_subplot(412)
maps = Basemap(projection='moll', lon_0=0, resolution='c')
im2 = maps.contourf(DEC*RAD, ASC*RAD, np.transpose(W[:,:,in2]), levels = level2, cmap = plt.get_cmap("RdYlBu"),latlon = True)
fig.colorbar(im2, ticks = tick2, cax = cbar_ax2, format = '%1d', orientation = 'horizontal')
cbar_ax2.set_ylabel(r"$ u_\mathrm{r} (r = %.2f r_*) \mathrm{[m/s]}$" %r2, rotation = 0, fontsize = 30)
cbar_ax2.yaxis.set_label_coords(.5,.90)
cbar_ax2.tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 6, width = 2, length = 6)

##################################################################################

ax3 = fig.add_subplot(413)
maps = Basemap(projection='moll', lon_0=0, resolution='c')
im3 = maps.contourf(DEC*RAD, ASC*RAD, np.transpose(W[:,:,in3]), levels = level3, cmap = plt.get_cmap("RdYlBu"),latlon = True)
fig.colorbar(im3, ticks = tick3, cax = cbar_ax3, format = '%1d', orientation = 'horizontal')
cbar_ax3.set_ylabel(r"$ u_\mathrm{r} (r = %.2f r_*) \mathrm{[m/s]}$" %r3, rotation = 0, fontsize = 30)
cbar_ax3.yaxis.set_label_coords(.5,.90)
cbar_ax3.tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 6, width = 2, length = 6)

###################################################################################

ax4 = fig.add_subplot(414)
im4 = ax4.contourf(X, Y, W[:, inth, :], levels = level4, interpolation = 'bicubic', cmap = plt.get_cmap("RdYlBu")) # RdYlBu
fig.colorbar(im4, cax = cbar_ax4, ticks = tick4, format = '%1d', orientation = 'horizontal')
cbar_ax4.set_ylabel(r"$ u_\mathrm{r}\mathrm{[m/s]}$" %abs(nth), rotation = 0, fontsize = 30)
#cbar_ax4.set_ylabel(r"$ u_\mathrm{r} (\theta = {%2d}^\circ) \mathrm{[m/s]}$" %abs(nth), rotation = 0, fontsize = 30)
cbar_ax4.yaxis.set_label_coords(.5,.90)
cbar_ax4.tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 6, width = 2, length = 6)

ax4.set_aspect('equal')
ax4.set_frame_on(False)
ax4.set_xticks([])
ax4.set_yticks([])

rmax = max(r)
rmin = min(r)

ax4.plot(rmax*cos(phi),rmax*sin(phi),'k')
ax4.plot(rmin*cos(phi),rmin*sin(phi),'k')

fig.subplots_adjust(left = 0., right = 1., top = .99, bottom = .08, hspace = 0.50)
plt.savefig('rad_vel.eps', dpi = 100)
plt.show()

'''
##################################################################################

ax1 = fig.add_subplot(221)
maps = Basemap(projection='moll', lon_0=0, resolution='c')
im1 = maps.contourf(DEC*RAD, ASC*RAD, np.transpose(W[:,:,in1]), levels = level1, cmap = plt.get_cmap("RdYlBu"),latlon = True)
fig.colorbar(im1, ticks = tick1, cax = cbar_ax1, format = '%1d', orientation = 'horizontal')
cbar_ax1.set_ylabel(r"$ u_\mathrm{r} (r = %.2f r_*) \mathrm{[m/s]}$" %r1, rotation = 0, fontsize = 30)
cbar_ax1.yaxis.set_label_coords(.5,.90)
cbar_ax1.tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 6, width = 2, length = 6)

##################################################################################

ax2 = fig.add_subplot(222)
maps = Basemap(projection='moll', lon_0=0, resolution='c')
im2 = maps.contourf(DEC*RAD, ASC*RAD, np.transpose(W[:,:,in2]), levels = level2, cmap = plt.get_cmap("RdYlBu"),latlon = True)
fig.colorbar(im2, ticks = tick2, cax = cbar_ax2, format = '%1d', orientation = 'horizontal')
cbar_ax2.set_ylabel(r"$ u_\mathrm{r} (r = %.2f r_*) \mathrm{[m/s]}$" %r2, rotation = 0, fontsize = 30)
cbar_ax2.yaxis.set_label_coords(.5,.90)
cbar_ax2.tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 6, width = 2, length = 6)

##################################################################################

ax3 = fig.add_subplot(223)
maps = Basemap(projection='moll', lon_0=0, resolution='c')
im3 = maps.contourf(DEC*RAD, ASC*RAD, np.transpose(W[:,:,in3]), levels = level3, cmap = plt.get_cmap("RdYlBu"),latlon = True)
fig.colorbar(im3, ticks = tick3, cax = cbar_ax3, format = '%1d', orientation = 'horizontal')
cbar_ax3.set_ylabel(r"$ u_\mathrm{r} (r = %.2f r_*) \mathrm{[m/s]}$" %r3, rotation = 0, fontsize = 30)
cbar_ax3.yaxis.set_label_coords(.5,.90)
cbar_ax3.tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 6, width = 2, length = 6)

###################################################################################

ax4 = fig.add_subplot(224)
im4 = ax4.contourf(X, Y, W[:, inth, :], levels = level4, interpolation = 'bicubic', cmap = plt.get_cmap("RdYlBu")) # RdYlBu
fig.colorbar(im4, cax = cbar_ax4, ticks = tick4, format = '%1d', orientation = 'horizontal')
cbar_ax4.set_ylabel(r"$ u_\mathrm{r}\mathrm{[m/s]}$" %abs(nth), rotation = 0, fontsize = 30)
#cbar_ax4.set_ylabel(r"$ u_\mathrm{r} (\theta = {%2d}^\circ) \mathrm{[m/s]}$" %abs(nth), rotation = 0, fontsize = 30)
cbar_ax4.yaxis.set_label_coords(.5,.90)
cbar_ax4.tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 6, width = 2, length = 6)

ax4.set_aspect('equal')
ax4.set_frame_on(False)
ax4.set_xticks([])
ax4.set_yticks([])

rmax = max(r)
rmin = min(r)

ax4.plot(rmax*cos(phi),rmax*sin(phi),'k')
ax4.plot(rmin*cos(phi),rmin*sin(phi),'k')

fig.subplots_adjust(left = 0.01, right = .99, top = 1., bottom = .13, wspace = 0.2, hspace = .38)
plt.savefig('rad_vel.eps', dpi = 100)
plt.show()
'''
