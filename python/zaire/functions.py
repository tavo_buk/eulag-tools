'''
/* Functions			         *
 * Bonnie Romano Zaire                   *
 * Version 1.0                           *
 * Date March 28 2016                 */
'''

import matplotlib.pyplot  as plt
from mpl_toolkits.basemap import Basemap
import pylab as pl
import numpy as np
import math 

R = 8.3144621   # R is the gas constant [R]=[joule/(mol*kelvin)].

#	Figure in Mollweide	#

def moll_plot(Z, nlevels):
    '''
    This function creates a figure in Mollweide projection.
    
    INPUTS:
    > Z: field to be plotted;
    > nlevels: Number of levels in the colormap.
    
    OUTPUT:
    > Creates a picture with Mollweide projection in the directory where you are working
    '''
    cmap = plt.get_cmap("plasma")           # Set colormap
  #  cmap = plt.get_cmap("jet")
  #  m = Basemap(projection='moll', lon_0 = 0)
  #  th = np.linspace(-np.pi/2, np.pi/2, Z.shape[1])
  #  phi = np.linspace(-np.pi, np.pi, Z.shape[0])
  #  RAD = 180/np.pi
  #  X,Y = np.meshgrid(th*RAD, phi*RAD) 
    plt.subplot(111, projection = "mollweide")
    levels = np.linspace(Z.min(),Z.max(), nlevels)
    return plt.contourf(Z, cmap = cmap, levels = levels, latlon = True)


#	Read Stratification	#

def read_strat(inputfile): 
    ''' 
    This function reads the file with stratification and calculates the gamma in function of the radius.
    Is expected that the file has 8 columns, arranged as follows: [r, P, T, rho, cpcv, dltdlp, grad_ad, grad_rad]. 
       
    INPUTS:
    > inputfile: contains the path of the file to be read;
    
    OUTPUT: 
    > r: Radius array;
    > P: Pressure array;
    > T: Temperature array;
    > rho: Density array; 
    > cpcv: Ratio of the specific heat capacity at constant pressure and at constant volumn; 
    > dlntdlnp
    > grad_ad: Adiabatic gradient;
    > grad_rad: Radiative gradient;
    > gamma: Gamma array.
    '''	
    f = open(inputfile, 'r')
    files = np.genfromtxt(f, usecols = (0, 1, 2, 3, 4, 5, 6, 7))
    r = 1e-2*files[:,0]                                           # In meters 
    P = 1e-1*files[:,1]					          # In N/m**2 
    T = files[:,2]					          # In Kelvin	
    rho = 1e3*files[:,3]					  # In kg/m**3
    dlntdlnp = files[:,4]
    cpcv = files[:,5]
    grad_ad = files[:,6]
    grad_rad = files[:,7]
    gamma = 1./(1. - grad_ad) 
    return r, P, T, rho, grad_ad, grad_rad, gamma									
