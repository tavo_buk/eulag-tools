import numpy as np
from settings import radius, rad_core, rad_th, r, n, m, l, nplot
from scipy import meshgrid 
from numpy import sin, cos, pi
import matplotlib.pyplot as plt
import matplotlib

def p2d(field, title):
    cut = 5. # Degrees to be cut off
    good = int(cut*m/180.)
    if (good < 2):
        good = 2
    rad_th_good = np.linspace(0+(cut/90)*pi/2., (1 - cut/180)*pi, num = m - 2*good)

    R, TH = meshgrid(r, rad_th_good) 
    X = R*sin(TH) 
    Y = R*cos(TH) 

    field = field[good:-good,:]
    fig = plt.figure()
    ax = fig.add_subplot(111) 
    cs = ax.contourf(X, Y, 1.e-7*field, levels = np.linspace(1.e-7*field.min(), 1.e-7*field.max(), 40), interpolation = 'bicubic', cmap = plt.get_cmap("RdYlBu"))  

    # Add colorbar 
    cax = fig.add_axes([0.3, 0.2, 0.02, 0.6])
    cbar = fig.colorbar(cs, cax = cax, ticklocation='left', extend='neither', spacing='uniform')
    cbar_ax = cbar.ax
    cbar_ax.text(1.1, .55, r"$ \rm (10^7 Kg m^{-1} s^{-2})$", rotation = 90, fontsize= 18)

    if title == "MC":
        plt.title(r"$-\nabla . F^{MC} $", loc = 'right')
    else:
        if title == "RS":
            plt.title(r"$-\nabla . F^{RS}$", fontsize = 18, loc = 'right')
        else:
            pass
	# Make sure aspect ratio preserved 
    ax.set_aspect('equal') 

	# Turn off rectangular frame. 
    ax.set_frame_on(False) 

	# Turn off axis ticks. 
    ax.set_xticks([]) 
    ax.set_yticks([]) 

	# Draw a circle around the edge of the plot. 
    rmax = max(r) 
    rmin = min(r) 
    ax.plot(rmax*sin(rad_th),rmax*cos(rad_th),'k') 
    ax.plot(rmin*sin(rad_th),rmin*cos(rad_th),'k') 

    ax.plot(r*sin(min(rad_th)),r*cos(min(rad_th)), 'k')
    ax.plot(r*sin(max(rad_th)),r*cos(max(rad_th)), 'k')

	# Draw a circle in the rad-conv interface
    if rad_core:
        ax.plot(0.3*radius*sin(rad_th), 0.3*radius*cos(rad_th),'--k') 

    return plt.show()

