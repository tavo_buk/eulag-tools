import matplotlib.pyplot as plt
from ttauri import rxav, parameters
import numpy as np


(n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver, nslice, nplot, nstore, nxaver, it, rb, rt) = parameters()

thew, u, v, w, ox , oy , oz, u2, v2, w2, ox2, oy2, oz2, rwv, rwu, rvu, bx, by, bz, bx2, by2, bz2 , bzby, bzbx, bxby, P, the = rxav('../../HD/tt01Myr/xaverages.dat')

MHDthew, MHDu, MHDv, MHDw, MHDox , MHDoy , MHDoz, MHDu2, MHDv2, MHDw2, MHDox2, MHDoy2, MHDoz2, MHDrwv, MHDrwu, MHDrvu, MHDbx, MHDby, MHDbz, MHDbx2, MHDby2, MHDbz2 , MHDbzby, MHDbzbx, MHDbxby, MHDP, MHDthe = rxav('../../MHD/tt01Myr/xaverages.dat')

r = np.linspace(rb, rt, num = l)

# List with mean fields
fields = [u, v, w]
MHDfields = [MHDu, MHDv, MHDw]

# List with quadratic fields
ql = [u2, v2, w2]
MHDql = [MHDu2, MHDv2, MHDw2]

# List with quadratic deviations
du2 = np.empty((m, l))
dv2 = np.empty((m, l))
dw2 = np.empty((m, l))
qd = [du2, dv2, dw2]

MHDdu2 = np.empty((m, l))
MHDdv2 = np.empty((m, l))
MHDdw2 = np.empty((m, l))
MHDqd = [MHDdu2, MHDdv2, MHDdw2]

for k in range(0, len(fields)):
    # Spatial mean
    qd[k] = ql[k] - np.multiply(fields[k],fields[k])
    MHDqd[k] = MHDql[k] - np.multiply(MHDfields[k], MHDfields[k])

du2, dv2, dw2 = qd
MHDdu2, MHDdv2, MHDdw2 = MHDqd

# Turbulent Velocity
urms = np.power(du2 + dv2 + dw2, 0.5)
MHDurms = np.power(MHDdu2 + MHDdv2 + MHDdw2, 0.5)

path_01 = '../../../ttauri/tt01Myr/est_061M_1.1Myr_l0.out'
path_14 = '../../../ttauri/tt14Myr/est_061M_14Myr_l0.out'

what_path = input('Type 1 (TT01) or 2 (TT14) = ')

if what_path == 1:
    dat = np.genfromtxt(path_01)
elif what_path == 2:
    dat = np.genfromtxt(path_14)
rs = dat[:, 0] / 100. 
VEL = dat[:, -1] / 100.   # velocity in m/s
vel = np.interp(r, rs, VEL)

nn = int(raw_input('Number of steps in temporal mean = '))

urms = np.mean(urms[-nn::,:], axis = (0,1))
MHDurms = np.mean(MHDurms[-nn::,:], axis = (0,1))


fig, ax = plt.subplots(dpi = 250, facecolor = None, edgecolor = None, linewidth = None, tight_layout = True) # Another option is tight_layout = True
fig.set_size_inches(10, 10)
ax.plot(r/rs[-1], vel, c = 'k', linewidth = 2.5)
ax.plot(r/rs[-1], urms, "--", c = 'm', linewidth = 2.5)
ax.plot(r/rs[-1], MHDurms, "--", c = 'g', linewidth = 2.5) #
ax.legend([r'$u_\mathrm{sm}$', r'$u_\mathrm{rms} \ HD$',r'$u_\mathrm{rms} \ MHD$'], loc = 'lower right', fontsize = 40)
ax.set_xlim([0.1,0.95])
ax.set_ylim([1., 200.])
ax.set_yscale('log')
ax.set_xlabel(r"$\frac{r}{r_*}$", fontsize = 40, labelpad = 30, ha = 'center')
ax.set_ylabel(r'$u [m/s]$', fontsize = 30, labelpad = 10, ha = 'center')
ax.tick_params(which='major', direction = 'inout', labelsize = 20, pad = 6, width = 2, length = 6)
ax.tick_params(which='minor', direction = 'inout', labelsize = 20, pad = 6, width = 1, length = 4)
plt.savefig("vel_profile.png")
plt.savefig("vel_profile.eps")
plt.show()
