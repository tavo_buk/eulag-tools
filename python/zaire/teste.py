# Creates a movie                                                                                                                                                        
from settings import rad_core, r, radius, rad_th, m, l
from rbox import u, v, w                   
import numpy as np      
from math import pi, cos, sin
import matplotlib.pyplot as plt
import matplotlib.image as mgimg
import matplotlib.animation as animation
from pylab import *         
from math import pi     
import types                

n = 5#int(0.1*u.shape[0]) 
u = np.mean(u, axis = 1) # Mean in phi direction

# Generates n figures:
for i in range(n):
	U = u[-(n-i),:,:]
    
	cut = 5. # Degrees to be cut off
	good = int(cut*m/180.)
	rad_th_good = np.linspace(0+(cut/90)*pi/2., (1 - cut/180)*pi, num = m - 2*good)
	R, TH = meshgrid(r, rad_th_good) 

	X = R*sin(TH) 
	Y = R*cos(TH) 
	Omega_0 = 0. 
	Omega = U[good:-good, :]/X + Omega_0

	fig = plt.figure()
	ax = fig.add_subplot(111) 
	Omin = Omega.min()
	Omax = Omega.max()
	cs = ax.contourf(X, Y, Omega*1.e9/(2*pi), levels = np.linspace(Omin*1.e9/(2.*pi), Omax*1.e9/(2.*pi), 70), interpolation = 'bicubic', cmap = plt.get_cmap("RdYlBu"))  

	# Add colorbar 
	cax = fig.add_axes([0.3, 0.2, 0.02, 0.6])
	cbar = fig.colorbar(cs, cax = cax, ticklocation='left', extend='neither', spacing='uniform')
	cbar_ax = cbar.ax
	cbar_ax.text(1.1, .5, r" (nHz)", rotation = 90)

	# Make sure aspect ratio preserved 
	ax.set_aspect('equal') 

	# Turn off rectangular frame. 
	ax.set_frame_on(False) 

	# Turn off axis ticks. 
	ax.set_xticks([]) 
	ax.set_yticks([]) 

	# Draw a circle around the edge of the plot. 
	rmax = max(r) 
	rmin = min(r) 
	ax.plot(rmax*sin(rad_th),rmax*cos(rad_th),'k') 
	ax.plot(rmin*sin(rad_th),rmin*cos(rad_th),'k') 

	ax.plot(r*sin(min(rad_th)),r*cos(min(rad_th)), 'k')
	ax.plot(r*sin(max(rad_th)),r*cos(max(rad_th)), 'k')

	# Draw a circle in the rad-conv interface
	if rad_core:
		ax.plot(0.3*radius*sin(rad_th), 0.3*radius*cos(rad_th),'--k') 
	
	plt.savefig('fig%05d' % i,  transparent = True)

