# Cortesy of A. Strugarek  :-)
######################################################
### Typical use of this class:
######################################################
### > import EULAG_basic_module as eul
### > D=eul.EulagData(Path='mypath',Nskip=0,Niter=1)
######################################################
### > D.SaveStateVTK() # saves in mypath a vk file 
######################################################
from __future__ import print_function
import numpy as np
import os
import eudyn as E

try:
    import cPickle as pickle
except:
    print("To easily use read_elements and save_elements, install pickle")
try:
    from evtk.hl import gridToVTK
except:
    print("If you want to ouptut vtk files, download and install 'https://bitbucket.org/pauloh/pyevtk'")

class EulagData:
    """ Basic readers of output filesx from eulag 
    ----------------------------------
    self.n  : number of toroidal points
    self.m  : number of poloidal points
    self.l  : number of radial points
    self.rad  : radius coordinates
    self.lat  : latitude coordinates (classical r,theta,phi basis)
    self.lon  : longitude coordinates
    self.u   : Phi velocity (same for v, w, th, p, bx, by, bz)
    """

    def __init__(self,Filename='fort.11',Path='',Nskip=0,Niter=1,Mag=True):
        """ Required arguments to read eulag output file 
            Filename : output file name                             (default: fort.11)
            Path     : path where filename is                       (default: current dir)
            Nskip    : number of iterations to skip                 (default: 0)
            Niter    : number of iterations to read                 (default: 1)
            Mag      : magnetic run (for hydro set to False)        (default: True)
        """

        # Define name of saved variable
        if Mag:
            self.Qs=['u','v','w','th','p','bx','by','bz']
        else:
            self.Qs=['u','v','w','th','p']

        # Path cooking
        if Path == '':
            self.Path = './'
        elif Path[-1] != '/':
            self.Path = Path+'/'
        else:
            self.Path = Path

        # Get record size, number of records in file, and deduces nr,nt,np
        self.EulagFile = self.Path+Filename # define source file

        # Define the grids
        # self.rstar = 6.9599e8  # Sun 
        # self.rstar = 1.36206e9 # tt01My
        self.rstar = 6.07751e8 # tt14My
        self.GetDataInfo() # get basic information from the size of the EULAG file
        self.rad = np.linspace(self.rmin, self.rmax, num = self.l)
        dya = np.pi/self.m
        self.lat = np.zeros(self.m)
        for ii in range(self.m):
            self.lat[ii] = - np.pi/2.+ (ii + 0.5)*dya
        self.lon = np.linspace(0., 2.*np.pi, num = self.n + 1)[0:-1]

        # Sanity checks so that we are not trying to read out of the file
        if Nskip*self.BlockSize >= self.TotalSize:
            print(">> Went to the end of file skipping, parameters are wrong <<")
            print(">> Maximum rec is %i <<" % (self.TotalSize/self.BlockSize))
            return

        if Nskip*self.BlockSize + Niter*self.BlockSize >= self.TotalSize:
            print(">> Went to the end of file reading, parameters are wrong <<")
            print(">> Maximum rec is %i <<" % (self.TotalSize/self.BlockSize))
            return

        # Init Arrays
        for qq in self.Qs:
            exec "self.%s = np.zeros((self.l, self.m, self.n))" % (qq)

        # Open file
        fp = open(self.EulagFile, 'rb')
        # Skip first records
        fp.seek(Nskip*self.BlockSize, 0)
        # Read data 
        for it in range(Niter):
            EulagBlock = self.ReadBlock(fp) # Read Block
            # Here I do a temporal average, you could insert code hereafter to change how it behaves
            for qq in self.Qs:
                exec "self.%s += EulagBlock[qq].reshape((self.l, self.m, self.n))/float(Niter)" % (qq) 
        # Close file
        fp.close()
        
    def ReadBlock(self,fp):
        """ Reads a basic record in EULAG file
            fp : file stream 
        """
        EulagBlock = {} # Dictionnary containing a block of EULAG data, returned by this function
        for qq in self.Qs:
            numval = np.fromfile(fp, dtype = 'uint32', count = 1)[0]/np.dtype('float32').itemsize # Get size of block
            EulagBlock[qq] = np.fromfile(fp, dtype = 'float32', count = numval) # Read one block
            numval = np.fromfile(fp, dtype = 'uint32', count = 1)[0]/np.dtype('float32').itemsize # Skip this, written by fortran
        return EulagBlock
    
    def GetDataInfo(self):
        """ Deduces basic infos from the EULAG data file
        """
        # Open file
        fp = open(self.EulagFile, 'rb')
        # Read a block
        tmp=self.ReadBlock(fp)
        # Store size of one block
        self.BlockSize = fp.tell()
        # Go to end of file wihtout reading
        fp.seek(0,2)
        # Store total size
        self.TotalSize= fp.tell() 
        # Close file
        fp.close() 
        
        # Deduce number of grid points
        n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver, nslice, nplot, nstore, nxaver, it, rb, rt = E.parameters()
        self.rmin = rb/self.rstar; self.rmax = rt/self.rstar
        self.n = n; self.m = m; self.l = l

    def SaveStateVTK(self, outfile = 'test.vtk'):
        """ Saves A VTK file with all variables
            outfile: name of the output file in vk
        """
        # Some cooking...
        if '/' not in outfile:
            ofile = self.Path + outfile

        # Create grid for VTK output
        x = np.linspace(self.rad[0], self.rad[-1], num = self.l + 1) 
        y = np.linspace(0., np.pi, num = self.m + 1) 
        z = np.linspace(0., 2.*np.pi, num = self.n + 1)
        # Invert latitudinal direction
        for qq in self.Qs:
            exec "%s = np.zeros_like(self.%s)" % (qq,qq)
            for i in range(self.l):
                for k in range(self.n):
                    exec "%s[i,:,k] = self.%s[i,::-1,k]" % (qq,qq)
        # Dictionnary to save
        #DictVTK = {"Velocity": (w, v, u), "Magnetic_Field": (bx, by, bz), "Theta": th, "Pressure": p}
        DictVTK = {"Velocity": (w, v, u), "Magnetic_Field": (bx, by, bz), "W": w, "V": v, "U": u, "Bx": bx, "By": by, "Bz": bz, "Theta": th, "Pressure": p}
        # Save VTK File
        gridToVTK(ofile, x, y, z, cellData = DictVTK)

# Basic wrappers around pickle, to save/read a list of whatever you want
def save_elements(list_e,name):
    f=file(name,'w')
    for el in list_e:
        pickle.dump(el,f,1)
    f.close()

def read_elements(nb_el,name):
    f=file(name,'r')
    list_el = []
    for el in range(nb_el):
        list_el.append(pickle.load(f))
    f.close()
    return list_el
