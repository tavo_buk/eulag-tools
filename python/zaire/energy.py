''' This function calculates the total energy in simulation '''
from ttauri import parameters, rxav
import numpy as np
import matplotlib.pyplot as plt

(n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver, nslice, nplot, nstore, nxaver, it, rb, rt) = parameters()

stratification = np.genfromtxt('strat.dat')
rho_ad = stratification[:, 5]
mean_rho_ad = np.mean(rho_ad)

filename_xav = raw_input('Type the name of xaverages file (without quotes): ')

if not filename_xav:
    thew, u, v, w, ox , oy , oz, u2, v2, w2, ox2, oy2, oz2, rwv, rwu, rvu, bx, by, bz, bx2, by2, bz2 , bzby, bzbx, bxby, P, the = rxav()
else:
    thew, u, v, w, ox , oy , oz, u2, v2, w2, ox2, oy2, oz2, rwv, rwu, rvu, bx, by, bz, bx2, by2, bz2 , bzby, bzbx, bxby, P, the = rxav(filename_xav)

r = np.linspace(rb, rt, num = l)
rr = np.linspace(.1, .95, num = l)
theta = np.linspace(0, np.pi, num = m)
outputs_xaver = u2.shape[0]

mu = 4*np.pi*1.e-7
toroidal_magnetic = 0.5*bx2/mu
poloidal_magnetic = 0.5*(by2 + bz2)/mu
magnetic = toroidal_magnetic + poloidal_magnetic

rstar = rb*10.
kinetic_frame = np.mean(0.5*rho_ad*(2.*np.pi*r/(7.*24.*3600.))**2)
kinetic = 0.5*mean_rho_ad*(u2 + v2 + w2) + kinetic_frame

mean_pm = np.mean(poloidal_magnetic[-40::, :, :], axis = (0, 1, 2))
mean_tm = np.mean(toroidal_magnetic[-40::, :, :], axis = (0, 1, 2))
mean_m = np.mean(magnetic[-40::, :, :], axis = (0, 1, 2))
mean_k = np.mean(kinetic[-40::, :, :], axis = (0, 1, 2))

print 'Em/Ek =', mean_m/mean_k
print 'Epm/Ek =', mean_pm/mean_k
print 'Etm/Ek =', mean_tm/mean_k

year = 365.*24.*3600.  	# Year in seconds
time = np.multiply(dxaver*dt, range(outputs_xaver))/year	# Time in years

fig, axes = plt.subplots(nrows = 1, ncols = 1,figsize = (20, 10), dpi = 100, facecolor = None, edgecolor = None, linewidth = None, tight_layout = True) # Another option is tight_layout = True
#axes.plot(time, np.mean(toroidal_magnetic[:, :, :], axis = (1, 2)), '--', label = r'$E_{\mathrm{m} \ \phi}$')
#axes.plot(time, np.mean(poloidal_magnetic[:, :, :], axis = (1, 2)), '--', label = r'$E_{\mathrm{m} \ \mathbf{P}}$')
#axes.plot(time, np.mean(kinetic[:, :, :] + magnetic[:, :, :], axis = (1, 2))*1.e-5, label = r'$E_\mathrm{T}$')
axes.plot(time, np.mean(kinetic[:, :, :], axis = (1, 2))*1.e-5, color = 'k', label = r'$E_\mathrm{k}$')
axes.plot(time, np.mean(magnetic[:, :, :], axis = (1, 2))*1.e-5, '--', color = 'k', label = r'$E_\mathrm{m}$')
axes.set_xlabel(r'$\mathrm{Time \ [years]}$', fontsize = 30)
axes.set_ylabel(r"$ \mathrm{Energy \times 10^5 \ \left[\frac{J}{m^3}\right]}$", fontsize = 30)
axes.set_yscale('log')
axes.tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 10, width = 2, length = 6)
axes.legend(loc='lower right', shadow = True, fontsize = 40)
plt.savefig('energy.eps')
plt.show()
