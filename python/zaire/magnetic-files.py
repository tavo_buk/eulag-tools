from eulag import *
from sys import exit
import matplotlib.pylab as P
import numpy as N
import eulag as E
import os

# to run this script, execute first:
# f = E.read_f11()
# followed by
# %run -i /home/guerrero/eulag-tools/python/gustavo/alpha-omega.py

root_dir = "/home/DATA/Solar"
DIC = {"/TAC/tac-7-rf/":(7, 60), "/TAC/tac-14-rf/":(14, 0), "/TAC/tac-21-rf/":(21, 5),
       "/TAC/tac-28-rf/":(28, 0), "/TAC/tac-35-rf/":(35, 0),"/TAC/tac-56-rf/":(56, 0),
       "/TAC/tac-112-rf/":(112, 15), "/TAC/tac-224-rf/":(224, 20), # End of TAC directories
       "/NOTAC_rf/nt-07-rf/":(7, 0), "/NOTAC_rf/nt-14-rf/":(14, 0),
       "/NOTAC_rf/nt-28-rf/":(28, 0), "/NOTAC_rf/nt-35-rf/":(35, 0),
       "/NOTAC_rf/nt-42-rf/":(42, 0), "/NOTAC_rf/nt-56-rf/":(56, 14),
       "/NOTAC_rf/nt-112-rf/":(112, 0)} # End of NOTAC directories

for path, numb in DIC.items():
	period, tgood = numb
	os.chdir(root_dir+path)
	print "Simulation period = ", period, "days"
	f = E.read_f11()
        
        # Azimuthal average
	bxm = N.mean(f.bx,axis=1)  
	bym = N.mean(f.by,axis=1)
	bzm = N.mean(f.bz,axis=1)

	year = 365.*24.*3600.
	time = f.time/year
	nn, = N.where(time > tgood)
        # Saving the magnetic field in the stationary phase
	N.save('butterfly', (bxm[nn, ...], bym[nn, ...], bzm[nn, ...]))

	del bxm, bym, bzm
	del f
        del time, nn, period, tgood
