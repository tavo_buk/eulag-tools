# This function make a graph of the meridional circulation
# reading the xaverages.dat file.
import matplotlib.pyplot as plt
import matplotlib
from numpy import sin, cos, pi
from pylab import meshgrid
from scipy.integrate import simps
from eudyn import parameters, rxav, rbox
import numpy as np

(n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver, nslice, nplot, nstore, nxaver, it, rb, rt) = parameters()
r = np.linspace(rb, rt, num = l)

cut = 5. # Degrees to be cut off
good = int(cut*m/180.)

filename_xav = raw_input('Type the name of xaverages file (without cotes): ')
if not filename_xav:
    thew, u, v, w, ox , oy , oz, u2, v2, w2, ox2, oy2, oz2, rwv, rwu, rvu, bx, by, bz, bx2, by2, bz2 , bzby, bzbx, bxby, P, the = rxav()
else:
    thew, u, v, w, ox , oy , oz, u2, v2, w2, ox2, oy2, oz2, rwv, rwu, rvu, bx, by, bz, bx2, by2, bz2 , bzby, bzbx, bxby, P, the = rxav(filename_xav)

nn = input("Number of steps in mean = ")

U = np.mean(u[-nn::,:,:], axis = 0)
V = np.mean(v[-nn::,:,:], axis = 0)
W = np.mean(w[-nn::,:,:], axis = 0)

theta = np.linspace(0, pi, num = m)

# Stream Function
data = np.genfromtxt('strat.dat')
rho_ad = data[:, 5] # Isentropic density

stream1 = np.ones((m,l))
stream2 = np.ones((m,l))

R1, TH1 = meshgrid(r, theta)
c1 = R1**2*sin(TH1)*W
c2 = R1*sin(TH1)*V
for j in range(r.shape[0]):
    c1[:, j] = c1[:, j]*rho_ad[j]
    c2[:, j] = c2[:, j]*rho_ad[j]
for j in range(l-1):
    for i in range(m-3):
        ii = i + 2
        stream1[i, j] = simps(c1[0:ii, j], theta[0:ii])
for j in range(l-3):
    for i in range(m-1):
        jj = j + 2
        stream2[i, j] = simps(c2[i, 0:jj], r[0:jj])
# PLOT
if (good < 2):
        good = 2
theta_good = np.linspace(0+(cut/90)*pi/2., (1 - cut/180)*pi, num = m - 2*good)
R, TH = meshgrid(r, theta_good)
X = R*sin(TH)
Y = R*cos(TH)

W = W[good:-good,:]
V = V[good:-good,:]

fig, ax = plt.subplots(nrows = 1, ncols = 1, figsize = (8, 8), dpi = 120)
Vmin = V[good:-good,:].min()
Vmax = V[good:-good,:].max()
levels = np.linspace(Vmin*0.7, Vmax, 50)
cs = ax.contourf(X, Y, V, levels = levels, interpolation = 'bicubic', cmap = plt.get_cmap("RdYlBu"))

matplotlib.rcParams['contour.negative_linestyle'] = 'dashed'
stream1 = stream1[good:-good,:]

#  Stream
ax.contour(X, Y, stream1/X, levels = np.linspace((stream1/X).min(), (stream1/X).max(), 15), colors = "k")

# Add colorbar
tick = np.linspace(Vmin, Vmax, num = 6, endpoint = True)
fig.subplots_adjust(left = 0.3, right = 1.)
cbar_ax = fig.add_axes([0.25, 0.1, 0.05, 0.8])
fig.colorbar(cs, cax = cbar_ax, ticks = tick, ticklocation='left')
cbar_ax.set_ylabel(r"$u_{\theta} \rm (m/s)$", rotation = 90, fontsize = 26)
cbar_ax.tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 6, width = 2, length = 6)

# Make sure aspect ratio preserved
ax.set_aspect('equal')

# Turn off rectangular frame.
ax.set_frame_on(False)

# Turn off axis ticks.
ax.set_xticks([])
ax.set_yticks([])

# Draw a circle around the edge of the plot.
rmax = max(r)
rmin = min(r)
ax.plot(rmax*sin(theta),rmax*cos(theta),'k')
ax.plot(rmin*sin(theta),rmin*cos(theta),'k')
ax.plot(r*sin(min(theta)),r*cos(min(theta)), 'k')
ax.plot(r*sin(max(theta)),r*cos(max(theta)), 'k')

# Draw a circle in the rad-conv interface
rad_core = raw_input('There is a radiative-convective interface? (y/n)')
if (rad_core == 'y') or (rad_core == 't') or (rad_core == 'true') or (rad_core == 'True') or (rad_core == 'yes'):
    ax.plot(3 * rb * np.sin(theta), 3 * rb * np.cos(theta), '--k')
plt.savefig('meridional-circulation.eps', dpi = 120)
plt.show()
