# Plot the omega contour reading xaverages.dat file.
import matplotlib.pyplot as plt
import matplotlib
from numpy import sin, cos, pi
from pylab import meshgrid
from scipy.integrate import simps
from eudyn import parameters, rxav, rbox
import numpy as np

(n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver, nslice, nplot, nstore, nxaver, it, rb, rt) = parameters()
r = np.linspace(rb, rt, num = l)

# Reading fort.11
filename_rbox = raw_input('Type the name of fort.11 file (without cotes): ')
if not filename_rbox:
	data_rbox = rbox()
else:
	data_rbox = rbox(filename_rbox)
if len(data_rbox) == 8:
	u, v, w, the, P, bx, by, bz = data_rbox
else:
	u, v, w, the, P = data_rbox

phi = np.linspace(0, 2*pi, num = n)

R, PHI = meshgrid(r, phi)
nth = int(m/2)
U = u[-2, :, nth, :]
V = v[-2, :, nth, :]
W = w[-2, :, nth, :]
wmin = W.min()
wmax = W.max()

X = R*cos(PHI)
Y = R*sin(PHI)

#######  PLOT  #######
fig, ax = plt.subplots(nrows = 1, ncols = 1, figsize = (10, 8), dpi = 120)
matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
cs = ax.contourf(X, Y, W, levels = np.linspace(wmin, wmax, 60), interpolation = 'bicubic', cmap = plt.get_cmap("RdYlBu"))

# Add colorbar
tick = np.linspace(wmin, wmax, num = 6, endpoint = True)
fig.subplots_adjust(left = 0.25, right = .99)
cbar_ax = fig.add_axes([0.20, 0.1, 0.05, 0.8])
fig.colorbar(cs, cax = cbar_ax, ticks = tick, ticklocation='left', extend='neither', spacing='uniform')
cbar_ax.set_ylabel(r"$u_\mathrm{r}\mathrm{[m/s]}$", rotation = 90, fontsize = 30)
cbar_ax.tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 6, width = 2, length = 6)

# Make sure aspect ratio preserved
ax.set_aspect('equal')

# Turn off rectangular frame.
ax.set_frame_on(False)

# Turn off axis ticks.
ax.set_xticks([])
ax.set_yticks([])

# Draw a circle around the edge of the plot.
rmax = max(r)
rmin = min(r)
ax.plot(rmax*cos(phi),rmax*sin(phi),'k')
ax.plot(rmin*cos(phi),rmin*sin(phi),'k')
plt.show()
