# This routine computes the Reynolds Stresses using the data from rxav.py
from bcprof import rho
from settings import radius, rad_core, rad_th, r, n, m, l, nplot
from rxav import u, v, w, rwu, rvu 		# rwu, rvu are multiplied by rho*jacobian
import numpy as np
from math import pi
from pylab import meshgrid, sin, cos
import matplotlib.pyplot as plt

data = np.genfromtxt('strat.dat')
rho_s = data[:, 5]

# Velocities with temporal and azimutal mean
nn = input("Number of steps in temporal mean = ")
U = np.mean(u[-nn::, :, :], axis = 0) 
V = np.mean(v[-nn::, :, :], axis = 0) 
W = np.mean(w[-nn::, :, :], axis = 0) 

#---------------------------------------------------------------
######## Angular momentum flux #########
# Componentes caused by Meridional Circulation
Fmc_r = np.zeros((m, l))
Fmc_th = np.zeros((m, l))
rot_frame = 2.*pi*radius/(7.*24.*3600.)
Utot = np.add(U, rot_frame)
for i in range(m): 					# Theta Loop
    for j in range(l): 				# Radial Loop
        Fmc_r[i, j] = rho_s[j]*r[j]*np.sin(rad_th[i])*(Utot[i, j]*W[i, j])
        Fmc_th[i, j] = rho_s[j]*r[j]*np.sin(rad_th[i])*(Utot[i, j]*V[i, j])

######### Reynolds stresses ############ 
# Componentes caused by Small-scale Correlations
mcr1 = np.divide(np.mean(rwu, axis = 0), rho)  # Temporal and azimutal mean correlation
mcr2 = np.divide(np.mean(rvu, axis = 0), rho)  # Temporal and azimutal mean correlation

Frs_r = np.zeros((m, l))
Frs_th = np.zeros((m, l))
for i in range(m): 					# Theta Loop
	for j in range(l): 				# Radial Loop
		Frs_r[i, j] = rho_s[j]*r[j]*np.sin(rad_th[i])*(mcr1[i, j])
		Frs_th[i, j] = rho_s[j]*r[j]*np.sin(rad_th[i])*(mcr2[i, j])
		
#-----------------------------------------------------------------#
############ Torque ##############
# Divergence of the angular momentum flux 
dr = r[1] - r[0]
dth = rad_th[1] - rad_th[0]

Tmc_r = np.zeros((m,l))
Tmc_th = np.zeros((m,l))
c1 = np.zeros((m,l))
c2 = np.zeros((m,l))
for j in range(1,m-1):
	for k in range(l):
		c1[j, k] = Fmc_r[j, k]*r[k]**2
        c2[j, k] = Fmc_th[j, k]*np.sin(rad_th[j])
for j in range(m):
	Tmc_r[j, :] = np.gradient(c1[j, :], dr)
for k in range(l):
	Tmc_th[1:-1, k] = np.gradient(c2[1:-1, k], dth)

for k in range(l):
	Tmc_r[:, k] = Tmc_r[:, k]/r[k]**2
for j in range(1,m-1):
    for k in range(l):
	    Tmc_th[j, k] = Tmc_th[j, k]/(r[k]*np.sin(rad_th[j]))

Tmc = -1*(Tmc_r + Tmc_th)  # Minus the divergence



# Divergence of the flux caused by the Reynolds stresses
Trs_r = np.zeros((m,l))
Trs_th = np.zeros((m,l))
c3 = np.zeros((m,l))
c4 = np.zeros((m,l))

for j in range(1, m-1):
	for k in range(l):
		c3[j, k] = Frs_r[j, k]*r[k]**2
        c4[j, k] = Frs_th[j, k]*np.sin(rad_th[j])
for j in range(m):
	Trs_r[j, :] = np.gradient(c3[j, :], dr)
for k in range(l):
	Trs_th[1:-1, k] = np.gradient(c4[1:-1, k], dth)

for k in range(l):
    for j in range(1, m-1):
    	Trs_r[j, k] = Trs_r[j, k]/r[k]**2
        Trs_th[j, k] = Trs_th[j, k]/(r[k]*np.sin(rad_th[j]))
Trs = -1*(Trs_r + Trs_th)   # Minus the divergence

# In Steady State:
Tvisc =np.add(Tmc, Trs)

# ----------------------------------------------------------------------------------------- #
cut = 5. # Degrees to be cut off
good = int(cut*m/180.)
if (good < 2):
    good = 2
rad_th_good = np.linspace(0+(cut/90)*pi/2., (1 - cut/180)*pi, num = m - 2*good)

R, TH = meshgrid(r, rad_th_good) 
X = R*sin(TH) 
Y = R*cos(TH) 

Tmc = Tmc[good:-good,:]
fig = plt.figure()
ax = fig.add_subplot(111) 
cs = ax.contourf(X, Y, 1.e-7*Tmc, levels = np.linspace(1.e-7*Tmc.min(), 1.e-7*Tmc.max(), 40), interpolation = 'bicubic', cmap = plt.get_cmap("RdYlBu"))  
#cs = ax.contourf(X, Y, 1.e-5*Tmc, levels = np.linspace(1.e-5*Trs.min(), 1.e-5*Trs.max(), 40), interpolation = 'bicubic', cmap = plt.get_cmap("RdYlBu"))  

# Add colorbar 
cax = fig.add_axes([0.3, 0.2, 0.02, 0.6])
cbar = fig.colorbar(cs, cax = cax, ticklocation='left', extend='neither', spacing='uniform')
cbar_ax = cbar.ax
cbar_ax.text(1.1, .55, r"$ \rm (10^7 Kg m^{-1} s^{-2})$", rotation = 90, fontsize= 18)

plt.title(r"$-\nabla . F^{MC} $", loc = 'right')

# Make sure aspect ratio preserved 
ax.set_aspect('equal') 

# Turn off rectangular frame. 
ax.set_frame_on(False) 

# Turn off axis ticks. 
ax.set_xticks([]) 
ax.set_yticks([]) 

# Draw a circle around the edge of the plot. 
rmax = max(r) 
rmin = min(r) 
ax.plot(rmax*sin(rad_th),rmax*cos(rad_th),'k') 
ax.plot(rmin*sin(rad_th),rmin*cos(rad_th),'k') 
ax.plot(r*sin(min(rad_th)),r*cos(min(rad_th)), 'k')
ax.plot(r*sin(max(rad_th)),r*cos(max(rad_th)), 'k')

# Draw a circle in the rad-conv interface
if rad_core:
    ax.plot(0.3*radius*sin(rad_th), 0.3*radius*cos(rad_th),'--k')  
plt.show()

#-----------------------------------------------------------------------------------------------#

Trs = Trs[good:-good,:]
fig = plt.figure()
ax = fig.add_subplot(111) 
cs = ax.contourf(X, Y, 1.e-7*Trs, levels = np.linspace(1.e-7*Trs.min(), 1.e-7*Trs.max(), 40), interpolation = 'bicubic', cmap = plt.get_cmap("RdYlBu"))  

# Add colorbar 
cax = fig.add_axes([0.3, 0.2, 0.02, 0.6])
cbar = fig.colorbar(cs, cax = cax, ticklocation='left', extend='neither', spacing='uniform')
cbar_ax = cbar.ax
cbar_ax.text(1.1, .55, r"$ \rm (10^7 Kg m^{-1} s^{-2})$", rotation = 90, fontsize= 18)

plt.title(r"$-\nabla . F^{RS}$", loc = 'right')

# Make sure aspect ratio preserved 
ax.set_aspect('equal') 

# Turn off rectangular frame. 
ax.set_frame_on(False) 

# Turn off axis ticks. 
ax.set_xticks([]) 
ax.set_yticks([]) 

# Draw a circle around the edge of the plot. 
rmax = max(r) 
rmin = min(r) 
ax.plot(rmax*sin(rad_th),rmax*cos(rad_th),'k') 
ax.plot(rmin*sin(rad_th),rmin*cos(rad_th),'k') 
ax.plot(r*sin(min(rad_th)),r*cos(min(rad_th)), 'k')
ax.plot(r*sin(max(rad_th)),r*cos(max(rad_th)), 'k')

# Draw a circle in the rad-conv interface
if rad_core:
    ax.plot(0.3*radius*sin(rad_th), 0.3*radius*cos(rad_th),'--k')  
plt.show()

#-----------------------------------------------------------------------------------------------#

Tvisc = Tvisc[good:-good,:]
fig = plt.figure()
ax = fig.add_subplot(111) 
cs = ax.contourf(X, Y, 1.e-7*Tvisc, levels = np.linspace(1.e-7*Tvisc.min(), 1.e-7*Tvisc.max(), 40), interpolation = 'bicubic', cmap = plt.get_cmap("RdYlBu"))  
#cs = ax.contourf(X, Y, 1.e-5*Tvisc, levels = np.linspace(1.e-5*Trs.min(), 1.e-5*Trs.max(), 40), interpolation = 'bicubic', cmap = plt.get_cmap("RdYlBu"))  

# Add colorbar 
cax = fig.add_axes([0.3, 0.2, 0.02, 0.6])
cbar = fig.colorbar(cs, cax = cax, ticklocation='left', extend='neither', spacing='uniform')
cbar_ax = cbar.ax
cbar_ax.text(1.1, .55, r"$ \rm (10^7 Kg m^{-1} s^{-2})$", rotation = 90, fontsize= 18)

plt.title(r"$-\nabla . F^{Visc}$", loc = 'right')

# Make sure aspect ratio preserved 
ax.set_aspect('equal') 

# Turn off rectangular frame. 
ax.set_frame_on(False) 

# Turn off axis ticks. 
ax.set_xticks([]) 
ax.set_yticks([]) 

# Draw a circle around the edge of the plot. 
rmax = max(r) 
rmin = min(r) 
ax.plot(rmax*sin(rad_th),rmax*cos(rad_th),'k') 
ax.plot(rmin*sin(rad_th),rmin*cos(rad_th),'k') 
ax.plot(r*sin(min(rad_th)),r*cos(min(rad_th)), 'k')
ax.plot(r*sin(max(rad_th)),r*cos(max(rad_th)), 'k')

# Draw a circle in the rad-conv interface
if rad_core:
    ax.plot(0.3*radius*sin(rad_th), 0.3*radius*cos(rad_th),'--k')  
plt.show()
