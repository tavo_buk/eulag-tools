import visit_writer
import math
import sys
import numpy as np
from functions import rbox, rxav

nX=128 #phi
nY=64 #theta
nZ=64 #r
nt=77 

inputfile = './fort.11'
u, v, w, t, p, bx, by, bz = rbox(inputfile, nX, nY, nZ, nt)

dims = (nX, nY, nZ)
pts = []
rad = []
rmin=0.6
rmax=1.
thmin=0.
thmax=math.pi
phmin=-math.pi
phmax=math.pi

dph = (phmax-phmin)/(nX-1)
dth = (thmax-thmin)/(nY-1)
dr = (rmax-rmin)/(nZ-1)

#for i in range(nX):
#   ph=phmin+dph*i  
#   for j in range(nY):
#      th=thmin+dth*j
#      for k in range (nZ):
for k in range (nZ):
    r = rmin+dr*k
    for j in range(nY):
        th=thmin+dth*j
        for i in range(nX):
            ph=phmin+dph*i  
            x = r*math.sin(th)*math.cos(ph)
            y = r*math.sin(th)*math.sin(ph)
            z = r*math.cos(th)
            pts.extend([ x, y, z ])
            rad.append( math.sqrt(x*x + y*y + z*z) )

var_datum = [ "radius", 1, 1, rad ]
vars = [ var_datum ]

visit_writer.WriteCurvilinearMesh("circle1.vtk", 1, dims, pts, vars)	
