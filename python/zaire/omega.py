# Plot the omega contour reading xaverages.dat file.
import matplotlib.pyplot as plt
import matplotlib
from numpy import sin, cos, pi
from pylab import meshgrid
from scipy.integrate import simps
from eudyn import parameters, rxav, rbox
import numpy as np

(n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver, nslice, nplot, nstore, nxaver, it, rb, rt) = parameters()
r = np.linspace(rb, rt, num = l)

filename_xav = raw_input('Type the name of xaverages file (without cotes): ')
if not filename_xav:
    thew, u, v, w, ox , oy , oz, u2, v2, w2, ox2, oy2, oz2, rwv, rwu, rvu, bx, by, bz, bx2, by2, bz2 , bzby, bzbx, bxby, P, the = rxav()
else:
    thew, u, v, w, ox , oy , oz, u2, v2, w2, ox2, oy2, oz2, rwv, rwu, rvu, bx, by, bz, bx2, by2, bz2 , bzby, bzbx, bxby, P, the = rxav(filename_xav)

cut = 5. # Degrees to be cut off
good = int(cut*m/180.)
theta = np.linspace(0, pi, num = m)
theta_good = np.linspace(0+(cut/90)*pi/2., (1 - cut/180)*pi, num = m - 2*good)

R, TH = meshgrid(r, theta_good)
nn = input("Number of steps in mean = ")
U = np.mean(u[-nn::,:,:], axis = 0)
V = np.mean(v[-nn::,:,:], axis = 0)
W = np.mean(w[-nn::,:,:], axis = 0)

X = R*sin(TH)
Y = R*cos(TH)

Omega_0 = 2.*pi/(7.*24.*3600.)
Omega = U[good:-good, :]/X + Omega_0

#######  PLOT  #######
fig, ax = plt.subplots(nrows = 1, ncols = 1, figsize = (8, 8), dpi = 120)
matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
Omin = Omega.min()
Omax = Omega.max()
cs = ax.contourf(X, Y, Omega*1.e9/(2*pi), levels = np.linspace(Omin*1.e9/(2.*pi), Omax*1.e9/(2.*pi), 50), interpolation = 'bicubic', cmap = plt.get_cmap("RdYlBu"))
ax.contour(X, Y, Omega*1.e9/(2*pi), levels = [1625,1650,1675,1680, 1685,1690,1700,1725], interpolation = 'bicubic', colors = "k")

# Add colorbar
tick = np.linspace(Omin*1.e9/(2.*pi), Omax*1.e9/(2.*pi), num = 6, endpoint = True)
fig.subplots_adjust(left = 0.3, right = 1.)
cbar_ax = fig.add_axes([0.25, 0.1, 0.05, 0.8])
fig.colorbar(cs, cax = cbar_ax, ticks = tick, ticklocation='left', extend='neither', spacing='uniform')
cbar_ax.set_ylabel(r"$\Omega \rm (nHz)$", rotation = 90, fontsize = 24)
cbar_ax.tick_params(which = 'major', direction = 'inout', labelsize = 20, pad = 6, width = 2, length = 6)

# Make sure aspect ratio preserved
ax.set_aspect('equal')

# Turn off rectangular frame.
ax.set_frame_on(False)

# Turn off axis ticks.
ax.set_xticks([])
ax.set_yticks([])

# Draw a circle around the edge of the plot.
rmax = max(r)
rmin = min(r)
ax.plot(rmax*sin(theta),rmax*cos(theta),'k')
ax.plot(rmin*sin(theta),rmin*cos(theta),'k')

ax.plot(r*sin(min(theta)),r*cos(min(theta)), 'k')
ax.plot(r*sin(max(theta)),r*cos(max(theta)), 'k')

# Draw a circle in the rad-conv interface
rad_core = raw_input('There is a radiative-convective interface? (y/n)')
if (rad_core == 'y') or (rad_core == 't') or (rad_core == 'true') or (rad_core == 'True') or (rad_core == 'yes'):
    ax.plot(3 * rb * np.sin(theta), 3 * rb * np.cos(theta), '--k')
plt.savefig('omega.eps', dpi = 120)
plt.show()
