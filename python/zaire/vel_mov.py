"""
A simple example of an animated plot
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from rbox import u, v, w
from math import sqrt
from settings import rr

# List with lllvelocities                                                                                                                                                   
u2 = np.multiply(u,u)
v2 = np.multiply(v,v)
w2 = np.multiply(w,w)
vel2 = u2+v2+w2
vel = np.power(vel2, 0.5)
def velocity():
    fig, ax = plt.subplots()
    line, = ax.plot(rr, vel[0, 62, 32, :])
    plt.ylim([vel[:, 62, 32, :].min(),vel[:, 62, 32, :].max()])
    plt.xlim([0.1,0.95])
    def animate(i):
        line.set_ydata(vel[i, 62, 32, :])  # update the data
        return line,

	# Init only required for blitting to give a clean slate.
    def init():
        line.set_ydata(np.ma.array(rr, mask=True))
        return line,
    ani = animation.FuncAnimation(fig, animate, np.arange(0,vel.shape[0] ), init_func=init,
                              interval=100, blit=True)
    ani.save('vel.mp4', fps=20, extra_args=['-vcodec', 'libx264'], writer=animation.FFMpegWriter())
    plt.show()

def vr():
    fig, ax = plt.subplots()
    line, = ax.plot(rr, w[0, 62, 32, :])
    plt.ylim([w[:, 62, 32, :].min(),w[:, 62, 32, :].max()])
    plt.xlim([0.1,0.95])
    def animate(i):
        line.set_ydata(w[i, 62, 32, :])  # update the data
        return line,

	# Init only required for blitting to give a clean slate.
    def init():
        line.set_ydata(np.ma.array(rr, mask=True))
        return line,

    anim = animation.FuncAnimation(fig, animate, np.arange(0,w.shape[0] ), init_func=init,
                              interval=100, blit=True)
    anim.save('vr.mp4', ifps=20, extra_args=['-vcodec', 'libx264'], writer=animation.FFMpegWriter())
'''
bx2 = np.multiply(bx,bx)
by2 = np.multiply(by,by)
bz2 = np.multiply(bz,bz)

def br():
    fig, ax = plt.subplots()
    line, = ax.plot(rr, bz[0, 62, 32, :])
    plt.ylim([bz[:, 62, 32, :].min(), bz[:, 62, 32, :].max()])
    plt.xlim([0.1,0.95])
    def animate(i):
        line.set_ydata(bz[i, 62, 32, :])  # update the data
        return line,

	# Init only required for blitting to give a clean slate.
    def init():
        line.set_ydata(np.ma.array(rr, mask=True))
        return line,

    anim = animation.FuncAnimation(fig, animate, np.arange(0,bz.shape[0] ), init_func=init,
                              interval=100, blit=True)
    anim.save('bz.mp4', fps=20, extra_args=['-vcodec', 'libx264'], writer=animation.FFMpegWriter())
'''
