import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import numpy as np
import ttauri as tt
from math import pi

(n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver, nslice, nplot, nstore, nxaver, it, rb, rt) = tt.parameters()
u, v, w, the, P = tt.rbox()

theta = np.linspace(-pi/2., pi/2, m)
phi = np.linspace(-pi, pi, n)
X,Y = np.meshgrid(phi, theta)
RAD = 180./np.pi
fig = plt.figure(figsize = (8,6))
maps = Basemap(projection='moll', lon_0=0, resolution='c')
wmin = w[-2,:,:,-70].min()
wmax = w[-2,:,:,-70].max()
cs = maps.contourf(X*RAD, Y*RAD, np.transpose(w[-2,:,:,-70]), levels = np.linspace(-150, 250, 30), cmap=plt.get_cmap("Oranges"),latlon=True)
cbar = maps.colorbar(cs, location = "right", pad = 0.5, size = .2, ticklocation = 'left', extend = 'neither', spacing = 'uniform')
cbar_ax = cbar.ax
#cbar_ax.text(1.4, .65, r"$u_r (r = %.2f R)$" %s, rotation = 90, fontsize = 16)
#maps.drawparallels(np.arange(-90.,120.,30.))
#maps.drawmeridians(np.arange(0.,420.,60.))
#plt.colormap(maps)
plt.savefig("mol_r72_TT03HD", format = "png")
plt.show()
