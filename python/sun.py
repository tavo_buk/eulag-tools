from eudyn import rxav 
import ttauri as tt
import matplotlib.pyplot as plt
from pylab import meshgrid
import numpy as np
from numpy import pi, sin
import os

root_dir = "/home/DATA/Solar"  

os.chdir(root_dir+"/TAC/tac-7-rf/")
tac_7 =   root_dir+"/TAC/tac-7-rf/xaverages.dat"
tac_14 =  root_dir+"/TAC/tac-14-rf/xaverages.dat.2"
tac_21 =  root_dir+"/TAC/tac-21-rf/xaverages.dat"
tac_28 =  root_dir+"/TAC/tac-28-rf/xaverages.dat"
tac_35 =  root_dir+"/TAC/tac-35-rf/xaverages.dat"
tac_56 =  root_dir+"/TAC/tac-56-rf/xaverages.dat"
tac_112 = root_dir+"/TAC/tac-112-rf/xaverages.dat"
tac_224 = root_dir+"/TAC/tac-224-rf/xaverages.dat"

tac_28a = root_dir+"/MPOLY/tac-28a/xaverages.dat"
tac_28b = root_dir+"/MPOLY/tac-28b/xaverages.dat"
tac_28c = root_dir+"/MPOLY/tac-28c/xaverages.dat"

nt_07 = root_dir+'/NOTAC_good/nt-07-rf/xaverages.dat'
nt_14 = root_dir+'/NOTAC_good/nt-14-rf/xaverages.dat'
nt_28 = root_dir+'/NOTAC_good/nt-28-rf/xaverages.dat'
nt_35 = root_dir+'/NOTAC_good/nt-35-rf/xaverages.dat'
nt_39 = root_dir+'/NOTAC_good/nt-39-rf/xaverages.dat'
nt_42 = root_dir+'/NOTAC_good/nt-42-rf/xaverages.dat'
nt_56 = root_dir+'/NOTAC_good/nt-56-rf/xaverages.dat.1'

filelist = [tac_7, tac_14, tac_21, tac_28, tac_35, tac_56, tac_112, tac_224, tac_28a, tac_28b, tac_28c]
nt_filelist = [nt_07, nt_14, nt_28, nt_35, nt_39, nt_42, nt_56]
step_time = [100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100]
Prot = [7., 14., 21., 28., 35., 56., 112., 224., 28., 28., 28.]

nt_step_time = [100, 100, 100, 200, 100, 100, 100]
nt_Prot = [7., 14., 28., 35., 39., 42., 56.]

(n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver, nslice, nplot, nstore, nxaver, it, rb, rt) = tt.parameters()
theta = np.linspace(0, pi, m)
r = np.linspace(rb, rt, l)
rsun = 6.96e8
R, TH = meshgrid(r, theta)
X = R*sin(TH)
nth30 = int(m/3)

BxTAC = []
ByTAC = []
BzTAC = []
BxSSL = []
BySSL = []
BzSSL = []
ro = []
list_omega = []
delta_omega = []
delta_omega_norm = []

for element, st, p_days in zip(filelist, step_time, Prot):
    # Reading xaverages.dat
    thew, u, v, w, ox , oy , oz, u2, v2, w2, ox2, oy2, oz2, rwv, rwu, rvu, bx, by, bz, bx2, by2, bz2 , bzby, bzbx, bxby, P, the = rxav(element)
    c = tt.field_strength(st, bx, by, bz)
    BxTAC.append(c[0])
    ByTAC.append(c[1])
    BzTAC.append(c[2])
    BxSSL.append(c[3])
    BySSL.append(c[4])
    BzSSL.append(c[5])
    l, rossby = tt.rossby_sun(st, p_days, u, v, w, u2, v2, w2)
    ro.append(float(np.asarray(rossby)))
    omega0 = 2.*pi/(p_days*24*3600.)
    omega = np.mean((u/X)[-st::, :, :], axis = 0)
    omega30 = np.mean(omega[nth30, -5:-2])
    omega90 = np.mean(omega[31, -5:-2])
    domega = (omega90 - omega30)
    list_omega.append(omega)
    delta_omega.append(domega)
    delta_omega_norm.append(domega/omega0)

BTAC = np.add(np.add(BxTAC, ByTAC),BzTAC)
BSSL = np.add(np.add(BxSSL,BySSL),BzSSL)

os.chdir(root_dir+"/NOTAC_good/nt-14-rf")
(nt_n, nt_m, nt_l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver, nslice, nplot, nstore, nxaver, it, nt_rb, nt_rt) = tt.parameters()
nt_theta = np.linspace(0, pi, nt_m)
nt_r = np.linspace(nt_rb, nt_rt, nt_l)
nt_R, nt_TH = meshgrid(nt_r, nt_theta)
nt_X = nt_R*sin(nt_TH)
nt_nth30 = int(nt_m/3)

nt_BxTAC = []
nt_ByTAC = []
nt_BzTAC = []
nt_BxSSL = []
nt_BySSL = []
nt_B = []
nt_ro = []
nt_list_omega = []
nt_delta_omega = []
nt_delta_omega_norm = []


for nt_element, nt_st, nt_p_days in zip(nt_filelist, nt_step_time, nt_Prot):
    nt_thew, nt_u, nt_v, nt_w, nt_ox , nt_oy , nt_oz, nt_u2, nt_v2, nt_w2, nt_ox2, nt_oy2, nt_oz2, nt_rwv, nt_rwu, nt_rvu, nt_bx, nt_by, nt_bz, nt_bx2, nt_by2, nt_bz2 , nt_bzby, nt_bzbx, nt_bxby, nt_P, nt_the = rxav(nt_element)
    nt_c = tt.field_strength(st, nt_bx, nt_by, nt_bz)
    nt_BxTAC.append(nt_c[0])
    nt_ByTAC.append(nt_c[1])
    nt_BzTAC.append(nt_c[2])
    nt_BxSSL.append(nt_c[3])
    nt_BySSL.append(nt_c[4])
    nt_B.append(nt_c[5])
    l, rossby = tt.rossby_sun(nt_st, nt_p_days, nt_u, nt_v, nt_w, nt_u2, nt_v2, nt_w2)
    nt_ro.append(float(np.asarray(rossby)))
    nt_omega0 = 2.*pi/(nt_p_days*24*3600.)
    nt_omega = np.mean((nt_u/nt_X)[-st::, :, :], axis = 0)
    nt_omega30 = np.mean(nt_omega[nth30, -5:-2])
    nt_omega90 = np.mean(nt_omega[31, -5:-2])
    nt_domega = (nt_omega90 - nt_omega30)
    nt_list_omega.append(nt_omega)
    nt_delta_omega.append(nt_domega)
    nt_delta_omega_norm.append(nt_domega/nt_omega0)

BpTAC = []
BpSSL = []
nt_BpTAC = []
nt_BpSSL = []
for i in range(len(filelist)):
    BpTAC.append(ByTAC[i] + BzTAC[i])
    BpSSL.append(BySSL[i] + BzSSL[i])

for i in range(len(nt_filelist)):
    nt_BpTAC.append(nt_ByTAC[i] + nt_BzTAC[i])
    nt_BpSSL.append(nt_BySSL[i] + nt_B[i])

os.chdir(root_dir)
np.savetxt('tac_results.txt', np.c_[Prot, ro, BzTAC, BpTAC, BzSSL, BpSSL, delta_omega, delta_omega_norm], fmt = '%1.4e', delimiter = ' ', header = 'Prot - Ro - Bz TAC - Bp TAC - Bz SSL - Bp SSL - dOmega - dOmega/Omega0')
np.savetxt('notac_results.txt', np.c_[nt_Prot, nt_ro, nt_BxTAC, nt_BpTAC, nt_BxSSL, nt_BpSSL, nt_delta_omega, nt_delta_omega_norm], fmt = '%1.4e', delimiter = ' ', header = 'Prot - Ro - Bz TAC - Bp TAC - Bz SSL - Bp SSL - dOmega - dOmega/Omega0')
np.save('tac_omega', [i for i in list_omega])
np.save('notac_omega', [ j for j in nt_list_omega])

'''
plt.figure()
plt.ylim((-2.0,0))
plt.xlim((-1.0,1.2))
plt.title('Magnetic field at the TACHOCLINE (tachocline models)')
plt.xlabel('log(Ro)')
plt.scatter(np.log10(ro[0:8]), np.log10(np.sqrt(BxTAC[0:8])), c = 'r', s = 50, label='log($<B_\phi>$)')
plt.scatter(np.log10(ro[0:8]), np.log10(np.sqrt(BpTAC[0:8])), c = 'b', s = 50, label='log($<B_p>$)' )
#np.polyfit(np.log10(ro[5:8]), np.log10(np.sqrt(BxSSL[5:8])),1)
plt.text(0.25,-0.2,'$<B> \propto$ Ro$^{-1.79}$',fontsize=18)
plt.plot(np.log10(ro[4:8]), (-1.79*np.log10(ro[4:8]) + 0.2),'k-', lw=1)
#np.polyfit(np.log10(ro[0:4]), np.log10(np.sqrt(BxTAC[0:4])),1)
plt.plot(np.log10(ro[0:5]), (0.71*np.log10(ro[0:5]) - 0.4),'k-', lw=1)
plt.text(-0.95,-0.5,'$<B> \propto$ Ro$^{0.71}$',fontsize=18)
#plt.scatter(np.log10(ro[-3::]), np.log10(np.sqrt(BxSSL[-3::])), c = 'r', marker = "*", s = 70)
#plt.scatter(np.log10(ro[-3::]), np.log10(np.sqrt(BpSSL[-3::])), c = 'b', marker = "*", s = 70)
plt.legend(loc=3)
plt.show()

plt.figure()
plt.ylim((-3.0,-0.5))
plt.xlim((-1.0,1.2))
plt.title('Magnetic field at the NSSL (tachocline models)')
plt.xlabel('log(Ro)')
plt.scatter(np.log10(ro[0:8]), np.log10(np.sqrt(BxSSL[0:8])), c = 'r', s = 50, label='log($<B_\phi>$)')
plt.scatter(np.log10(ro[0:8]), np.log10(np.sqrt(BpSSL[0:8])), c = 'b', s = 50, label='log($<B_p>$)' )
#np.polyfit(np.log10(ro[5:8]), np.log10(np.sqrt(BxSSL[5:8])),1)
plt.text(0.2,-0.75,'$<B> \propto$ Ro$^{-1.79}$',fontsize=18)
plt.plot(np.log10(ro[4:8]), (-1.79*np.log10(ro[4:8]) - 0.5),'k-', lw=1)
plt.plot([-0.7,-0], [-1.1,-1.1], 'k-', lw=1)
#plt.scatter(np.log10(ro[-3::]), np.log10(np.sqrt(BxSSL[-3::])), c = 'r', marker = "*", s = 70)
#plt.scatter(np.log10(ro[-3::]), np.log10(np.sqrt(BpSSL[-3::])), c = 'b', marker = "*", s = 70)
plt.legend(loc=3)
plt.show()

plt.figure()
plt.xlabel('$<B_p>$')
plt.ylabel('$<B_\phi>$')
plt.scatter(np.log10(np.sqrt(BpSSL[0:8])), np.log10(np.sqrt(BxSSL[0:8])), c = 'r', s = 50)
#plt.scatter((np.sqrt(BpSSL[0:8])), (np.sqrt(BxSSL[0:8])), c = 'r', s = 50)
#plt.scatter(np.sqrt(BpTAC[0:8]), np.sqrt(BxTAC[0:8]), c = 'g', s = 50, label='$<B_\phi>$vs$<B_p>$')
#plt.scatter(np.sqrt(ntBpSSL[0:8]), np.sqrt(nt_BxSSL[0:8]), c = 'b', s = 50, label='$<B_\phi>$vs$<B_p>$')
plt.show()

plt.figure()
plt.ylim((-2.0,-0.5))
plt.title('Magnetic field at the NSSL (non-tachocline models)')
plt.xlabel('log(Ro)')
plt.scatter(np.log10(nt_ro), np.log10(np.sqrt(nt_BxSSL)), c = 'r', marker = "^", s = 70, label='log($<B_\phi>$)')
plt.scatter(np.log10(nt_ro), np.log10(np.sqrt(ntBpSSL)), c = 'b', marker = "^", s = 70, label='log($<B_p>$)')
plt.plot(np.log10(nt_ro[1:7]), (-1.2*np.log10(nt_ro[1:7]) - 1.0),'k-',lw=1)
plt.text(-0.2,-0.75,'$<B> \propto$ Ro$^{-1.2}$',fontsize=18)
plt.plot([-0.8,-0.3], [-0.9,-0.9], 'k-', lw=1)
#np.polyfit(np.log10(nt_ro[2:7]), np.log10(np.sqrt(nt_BxSSL[2:7])),1)
plt.legend(loc=3)
plt.show()
'''
