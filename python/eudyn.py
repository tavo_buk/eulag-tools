#
# modules required to read the EULAG files.
#
#############################################
import numpy as np
from math import pi

def parameters():
    # This function reads the files mylist.nml and ee.txt for take the parameters
    # of the simulation. The variable Lxyz return if the simulation is cartesian
    # or spherical.
    import subprocess
    import os
    mylist = "./mylist.nml" 		# Option: None or patch file
    ee = "./ee.txt"					# Option: None or patch file
    if mylist is not None:
        file_my = open(mylist, "r")
        data_my = [line.strip() for line in file_my]
        n = int(data_my[2].split()[2][0:-1])		# Number of divisions in phi domain
        m = int(data_my[3].split()[2][0:-1])		# Number of divisions in theta domain
        l = int(data_my[4].split()[2][0:-1])		# number of divisions in radial domain
        Lxyz = int(data_my[5].split()[2][0:-1])		# Zero if Spherical and 1 if is cartesian
        dx = float(data_my[6].split()[2][0:-1])		# Phi step
        dy = float(data_my[7].split()[2][0:-1])		# Theta step
        dz = float(data_my[8].split()[2][0:-1])		# Radial step
        dt = float(data_my[9].split()[2][0:-1])		# Step time
        dslice = int(data_my[10].split()[2][0:-1])	# Time interval to write slices
        dout = int(data_my[11].split()[2][0:-1])	# Time interval to write ee.txt
        dplot = int(data_my[12].split()[2][0:-1])	# Time interval to write fort.11
        dstore = int(data_my[13].split()[2][0:-1])	# Time interval to write forte.9
        dxaver = int(data_my[14].split()[2])		# Time interval to write xaverages.dat
    else:
        print ("File mylist.nml does not exist.")


    if not ee is None:
        subprocess.call("cat ee.txt | grep it= > it.txt", shell = True)
        proc = subprocess.Popen(['tail', '-1','it.txt'], stdout = subprocess.PIPE)
        it = int(proc.stdout.read().split()[1]) 	# Simulation iterations
        os.remove("./it.txt")
        if Lxyz == 0: 		# For spherical simulations
            file_ee = open("./ee.txt", "r")
            data_ee = [line.strip() for line in file_ee]
            rb = float(data_ee[5].split()[2])*1.e3  # Bottom domain in meters
            rt = rb + l*dz                          # Top domain in meters
        else:
            pass
    else:
        print ("File ee.txt does not exist.")

    if (dslice > 0 ): 
       nslice = it/dslice 
    else:
       nslice = 0		# Number of outputs in slices
    nout = it/dout              # Number of outputs in ee.txt
    nplot = it/dplot   		# Number of outputs in fort.11
    nstore = it/dstore 		# Number of outputs in fort.9
    nxaver = it/dxaver 		# Number of outputs in xaverages.dat

    if Lxyz == 0:     		# For spherical simulations
        return n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver, nslice, nplot, nstore, nxaver, it, rb, rt
    else:
        return n, m, l, Lxyz, dx, dy, dz, dt, dslice, dout, dplot, dstore, dxaver, nslice, nplot, nstore, nxaver, it
