# Instructions for analysis of cartesian simulations with EULAG-MHD

* A primitive attempt to reproduce parker instabilty was performed with EULAG-MHD (see the file /bin/parker12.f), some scripts for analysis can be found in the tar file *parker_beta.tar.gz* 

* Old versions and trash code from generic scripts for data analysis from cartesian simulations can be found in the folder **eucart**

* For analysis of cartesian simulations the package of scripts in the folder **cartesian** is the most recent version which includes a Jupyter notebook with an example of usage.
