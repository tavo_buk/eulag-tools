import numpy as np
import matplotlib.pyplot as plt
from pylab import *
import sys

def plt_nempi(qp,xy_qp,pgrid,grid,units):

    def smooth(var):
        # input -> var.shape = (tt,yy,zz)
        ys = var.shape[1]
        fwd_var = np.zeros(var.shape)
        fwd_var[:,0,:] = var[:,0,:] 
        bwd_var = np.zeros(var.shape)
        bwd_var[:,-1,:] = var[:,-1,:]
        for i in range(1,ys):
            fwd_var[:,i,:] = 0.5*(var[:,i-1,:]+var[:,i,:])
        for i in range(ys-2,1,-1):
            bwd_var[:,i,:] = 0.5*(var[:,i,:]+var[:,i+1,:])
        return np.multiply(fwd_var+bwd_var,0.5)
    
    pltvar = qp

    Y,X=np.meshgrid(pgrid.zz,pgrid.yy)
    tit = r'$\left(Q_p\right)^{x}$'
    estilo = '%.1f'
    lmin=0
    lmax=grid.l
    tt = int(input("choose time to plot: "))
    #mx = np.amax(pltvar[1:,:,:])
    #mn = np.amin(pltvar[1:,:,:])
    mx = 2
    mn = -5
    delta = mx - mn

    levels = np.arange(mn,mx,delta/140)
    print("max/min values = ", mx, mn)

    fig = plt.figure(figsize=(10, 8))
    ax  = fig.add_subplot(221)
    graf= plt.contourf(X[:,lmin:lmax],Y[:,lmin:lmax],\
            pltvar[tt,:,lmin:lmax],levels,alpha=0.9,\
            cmap = cm.rainbow,extend='both')
    plt.ylabel('z [Mm]', fontsize=18)
    plt.xlabel('Y [Mm]', fontsize=18)
    ax.set_aspect('auto',anchor='SW')
    cbax = fig.add_axes([-0.01,0.53,0.02,0.34])
    ticks = [mn,0,0.95*mx,mx]
    cb = fig.colorbar(graf,ticks=ticks,orientation='vertical',format=estilo,cax = cbax)
    cb.set_label(tit, position=(0.5,0.5),labelpad = -65, fontsize=18)

    #set0_z = np.multiply([48,47,46,44,42],grid.l/(grid.Lz00*units.l_0))
    set0_z = np.multiply([38,37,36,34,30],grid.l/(grid.Lz00*units.l_0))
    #set0_z = np.multiply([25,26,36,34,30],grid.l/(grid.Lz00*units.l_0))
    colors = ['y','b','r','k','g']
    ax2   = fig.add_subplot(223)
    sm_pltvar = smooth(smooth(smooth(smooth(pltvar))))
    for i, nn in enumerate(set0_z):
        nplot = int(nn)
        plt.plot(X[:,nplot],sm_pltvar[tt,:,nplot],colors[i],\
                label = \
                str(round(nplot*grid.Lz00*units.l_0/grid.l,1))+\
                " Mm")
        plt.plot(X[:,nplot],pltvar[tt,:,nplot],colors[i]+'--')
    plt.plot(X[:,nplot],np.ones(X[:,nplot].shape),'k--')
    ax2.set_xlim([0,50])
    #ax2.set_ylim([np.amin(X_t),np.amax(X_t)])
    ax2.set_ylim([mn,mx])
    lg1 = ax2.legend(loc=4,prop={'size': 7})
    lg1.get_frame().set_alpha(1.0)
    plt.ylabel(tit,fontsize=18)
    plt.xlabel('Y [Mm]', fontsize=18)

    ax3 = fig.add_subplot(222)
    zz = np.multiply(np.arange(xy_qp.shape[1]),grid.Lz00*units.l_0/grid.l)
    q0 = np.ones(zz.shape)
    for i in [1,tt,xy_qp.shape[0]-1]:
        plt.plot(xy_qp[i],zz,label = '# of steps = '+str(i))
        plt.plot(q0,zz,'k--')
    plt.xlabel(r'$\left(Q_p\right)^{xy}$',fontsize = 18)
    plt.ylabel('z [Mm]',fontsize=18)
    plt.legend() 

    plt_mode = int(input("save slice to eps file [yes (1)/ no (0)] ?"))
    if plt_mode==1:
        # fix for different versions of python
        if sys.version_info[0]<3:
            filename = raw_input('choose a filename : ')
        else:
            filename = str(input("choose a filename : "))
        plt.savefig(filename)
        plt.show(fig)
    else:
        plt.show(fig)
    return None
