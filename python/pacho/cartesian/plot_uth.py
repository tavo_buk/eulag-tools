import numpy as np
import matplotlib.pyplot as plt
from pylab import *

def plt_uth(wtp,pgrid,grid,units):

    def smooth(var):
        # input -> var.shape = (tt,yy,zz)
        ys = var.shape[1]
        fwd_var = np.zeros(var.shape)
        fwd_var[:,0,:] = var[:,0,:] 
        bwd_var = np.zeros(var.shape)
        bwd_var[:,-1,:] = var[:,-1,:]
        for i in range(1,ys):
            fwd_var[:,i,:] = 0.5*(var[:,i-1,:]+var[:,i,:])
        for i in range(ys-2,1,-1):
            bwd_var[:,i,:] = 0.5*(var[:,i,:]+var[:,i+1,:])
        return np.multiply(fwd_var+bwd_var,0.5)
    
    pltvar = np.multiply(wtp,0.01)

    Y,X=np.meshgrid(pgrid.zz,pgrid.yy)
    tit = r'$\overline{u_r^\prime\theta^\prime} $$\times 10^5$[$m K s^{-1}$]'
    estilo = '%.1f'
    lmin=int(40.0/50.0*grid.l)-1
    lmax=grid.l
    tt = int(input("choose time to plot: "))
    #mx = np.amax(pltvar[:,:,:])
    #mn = np.amin(pltvar[:,:,:])
    mx = 0.8
    mn = -0.8
    delta = mx - mn

    levels = np.arange(mn,mx,delta/140)
    print("max/min values = ", mx, mn)

    fig = plt.figure(figsize=(6, 8))
    ax  = fig.add_subplot(211)
    graf= plt.contourf(X[:,lmin:lmax],Y[:,lmin:lmax],\
            pltvar[tt,:,lmin:lmax],levels,alpha=0.9,\
            cmap = cm.rainbow,extend='both')
    plt.ylabel('z [Mm]', fontsize=18)
    ax.set_aspect('auto',anchor='SW')
    cbax = fig.add_axes([0.2,0.92,0.62,0.02])
    ticks = [mn,0,0.95*mx,mx]
    cb = fig.colorbar(graf,ticks=ticks,orientation='horizontal',format=estilo,cax = cbax)
    cb.set_label(tit, position=(0.5,0.3),labelpad = -55, fontsize=18)

    set0_z = np.multiply([38,37,36,34,30],grid.l/(grid.Lz00*units.l_0))
#    set0_z = np.multiply([48,47,46,44,42],grid.l/(grid.Lz00*units.l_0))
    colors = ['y','b','r','k','g']
    ax2   = fig.add_subplot(212)
    sm_pltvar = smooth(smooth(smooth(smooth(pltvar))))
    for i, nn in enumerate(set0_z):
        nplot = int(nn)
        plt.plot(X[:,nplot],sm_pltvar[tt,:,nplot],colors[i],\
                label = \
                str(round(nplot*grid.Lz00*units.l_0/grid.l,1))+\
                " Mm")
        plt.plot(X[:,nplot],pltvar[tt,:,nplot],colors[i]+'--')
    ax2.set_xlim([0,50])
    #ax2.set_ylim([np.amin(X_t),np.amax(X_t)])
    ax2.set_ylim([mn,mx])
    lg1 = ax2.legend(loc=4,prop={'size': 7})
    lg1.get_frame().set_alpha(1.0)
    plt.ylabel(tit,fontsize=18)
    plt.xlabel('Y [Mm]', fontsize=18)
    plt_mode = int(input("save slice to eps file [yes (1)/ no (0)] ?"))
    if plt_mode==1:
        if sys.version_info[0]<3:
            filename = raw_input('choose a filename : ')
        else:
            filename = str(input("choose a filename : "))
        plt.savefig(filename)
        plt.show(fig)
    else:
        plt.show(fig)
    return None
