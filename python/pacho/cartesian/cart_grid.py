# Class with the parameters of the 
# Cartesian Grid for Eulag-MHD

def read_params(*args, **kwargs):
    return params(*args,**kwargs)

def read_files(diag=True):
    """
    reads and assings variable names
    to parameters of current "folder"
    simulation
    --------------------------------
    returns a list of the relevant
    parameters for post-processing
    """
    ee = True
    mylist = True	

    if ee is True:
        ee_r='ee.txt'
        file_ee=open(ee_r, "r")
        data_ee=[line.strip() for line in file_ee]
        size_ee=len(data_ee)
        string="it="
        for i in range(size_ee):
            if string in data_ee[i].split():
                it = data_ee[i].split()[1]
        itt=int(it)
        print(ee_r+' file OK')

    if mylist is True:
        mylist="mylist.nml"
        file_my=open(mylist, "r")
        data_my=[line.strip() for line in file_my]
        size_my=len(data_my)
        nt=int(data_my[1].split()[2][0:-1])
        n=int(data_my[2].split()[2][0:-1])
        m=int(data_my[3].split()[2][0:-1])
        l=int(data_my[4].split()[2][0:-1])
        Lx00=float(data_my[6].split()[2][0:-1])
        Ly00=float(data_my[7].split()[2][0:-1])
        Lz00=float(data_my[8].split()[2][0:-1])
        dt00=float(data_my[9].split()[2][0:-1])
        nslice=int(data_my[10].split()[2][0:-1])
        noutp=int(data_my[11].split()[2][0:-1])
        nplot=int(data_my[12].split()[2][0:-1])
        nstore=int(data_my[13].split()[2][0:-1])
        nxaver=int(data_my[14].split()[2])
        print(mylist+' file OK')

    if diag is True:
        print('EULAG grid parameters:')
        print("(it,n,m,l) = ",itt,n,m,l)
        print("(Lx,Ly,Lz,dt) = ",Lx00,Ly00,Lz00,dt00)
    else:
        print('diagnostics OFF')

    return itt, nt, n, m, l, Lx00, Ly00, Lz00, dt00, nslice, noutp, nplot, nstore, nxaver

class params(object):
    def __init__ (self,param=None):
        parameters  = read_files()
        self.itt    = parameters[0]
        self.nt     = parameters[1]
        self.n      = parameters[2]
        self.m      = parameters[3]
        self.l      = parameters[4]
        self.Lx00   = parameters[5]
        self.Ly00   = parameters[6]
        self.Lz00   = parameters[7]
        self.dt00   = parameters[8]
        self.nslice = parameters[9]
        self.noutp  = parameters[10]
        self.nplot  = parameters[11]
        self.nstore = parameters[12]
        self.nxaver = parameters[13]
        self.help   = ['itt','nt','n','m','l','Lx00','Ly00',\
                'Lz00','dt00','nslice','noutp','nplot','nstore','nxaver']
        print('see attributes with self.help ')
