#### GIVES EUCART FIELDS IN PHYSICAL UNITS
import numpy as np
import units as un
import cart_grid as gr
cts = un.read_units()
gr_set = gr.read_files() 

def phy_vars(dat):
        '''
        - takes as argument fort.11 dat dictionary 
            -> u,v,w,t,p,bx,by,bz
        '''
        phy_dat = {}
        print('pressure not transformed !')
        print('working ...')
        phy_dat.update({'pux': np.multiply(cts.v_0,dat['u'])})
        phy_dat.update({'puy': np.multiply(cts.v_0,dat['v'])})
        phy_dat.update({'puz': np.multiply(cts.v_0,dat['w'])})
        phy_dat.update({'pt': np.multiply(cts.Tm_0,dat['t'])})
        phy_dat.update({'pp': dat['p']})
        phy_dat.update({'pbx': np.multiply(cts.b_0*1e3,dat['bx'])})
        phy_dat.update({'pby': np.multiply(cts.b_0*1e3,dat['by'])})
        phy_dat.update({'pbz': np.multiply(cts.b_0*1e3,dat['bz'])})
        print('DONE')

        return phy_dat

#### PHYSICAL GRID FOR PLOTTINGS

def read_phygrid(*args, **kwargs):
        '''
        returns physical grid for plt_boxsl
        '''
        return Phy_grid(*args, **kwargs)

class Phy_grid(object):
        '''
        creates physical grid for plt_boxsl
        '''
        def __init__ (self,param=None):
            itt, nt, n, m, l, Lx00, Ly00, Lz00, dt00, nslice, noutp, nplot, nstore, nxaver = gr_set
            Lx0_0 = cts.l_0*Lx00
            Ly0_0 = cts.l_0*Ly00
            Lz0_0 = cts.l_0*Lz00
            dx = Lx0_0/(n-1)
            if not m == 1:
                    dy = Ly0_0/(m-1)
            dz = Lz0_0/(l-1)
            xx = np.arange(0,Lx0_0+dx,dx)
            if not m == 1:
                    yy = np.arange(0,Ly0_0+dy,dy)
            zz = np.arange(0,Lz0_0+dz,dz)

            self.xx = xx
            self.yy = yy
            self.zz = zz
