import matplotlib.pyplot as plt
import numpy as np
from pylab import *

def plt_Xt(X_t,pgrid,grid,units):
    Y,X=np.meshgrid(pgrid.zz,pgrid.yy)
    tit = r'$\chi_t$$\times 10^9$[$m^2s^{-1}$]'
    estilo = '%.1f'
    lmin=int(30.0/40.0*grid.l)-1
    lmax=grid.l
    tt = int(input("choose time to plot: "))
    #mx = np.amax(X_t[tt,:,lmin:lmax])
    #mn = np.amin(X_t[tt,:,lmin:lmax])
    mx  = 2.0
    mn = -0.5
    delta = (mx-mn)
    levels = np.arange(mn,mx,delta/140)
    print("max/min values = ", mx, mn)

    fig = plt.figure(figsize=(6, 8))
    ax  = fig.add_subplot(311) 
    graf= plt.contourf(X[:,lmin:lmax],Y[:,lmin:lmax],X_t[tt,:,lmin:lmax],levels,\
                               alpha=0.9,cmap = cm.rainbow,extend='both')
    plt.ylabel('z [Mm]', fontsize=18)
    ax.set_aspect('auto',anchor='SW')
    cbax = fig.add_axes([0.2,0.92,0.62,0.02])
    ticks = [mn,0,0.95*mx,mx]
    cb = fig.colorbar(graf,ticks=ticks,\
            orientation='horizontal',format=estilo,cax = cbax)
    cb.set_label(tit, position=(0.5,0.3),labelpad = -50, fontsize=18)

    set0_z = [159,160,161,162,163]
#    set0_z = np.multiply([48,47,46,44,42],grid.l/(grid.Lz00*units.l_0))
    #fig2  = plt.figure()
    ax2   = fig.add_subplot(312)
    for nn in set0_z:
        nplot = int(nn)
        plt.plot(X[:,nplot],X_t[tt,:,nplot],\
                label=str(round(nplot*grid.Lz00\
                *units.l_0/grid.l,1))+" Mm")
    ax2.set_xlim([0,50])
    #ax2.set_ylim([np.amin(X_t),np.amax(X_t)])
    ax2.set_ylim([-2,2])
    lg1 = ax2.legend(loc=1,prop={'size': 7})
    lg1.get_frame().set_alpha(1.0)
    plt.ylabel(tit,fontsize=18)

    set_z = [139,140,141,142,143]
#    set_z = np.multiply([41,42,43,44,45],grid.l/(grid.Lz00*units.l_0))

    #fig3  = plt.figure()
    ax3   = fig.add_subplot(313)
    for nn in set_z:
        nplot = int(nn)
        plt.plot(X[:,nplot],X_t[tt,:,nplot],\
                label=str(round(nplot*grid.Lz00*\
                units.l_0/grid.l,1))+" Mm")
    plt.xlabel('y [Mm]',fontsize = 18)
    plt.ylabel(tit,fontsize=18)
    lg2=plt.legend(loc=1,prop={'size': 7})
    lg2.get_frame().set_alpha(1.0)
    ax3.set_xlim([0,50])
    #ax3.set_ylim([np.amin(X_t),np.amax(X_t)])
    ax3.set_ylim([-2,2])
    
    plt_mode = int(input("save slice to eps file [yes (1)/ no (0)] ?"))
    if plt_mode==1:
        if sys.version_info[0]<3:
            filename = raw_input('choose a filename : ')
        else:
            filename = str(input("choose a filename : "))
        plt.savefig(filename)
        plt.show(fig)
    else:
        plt.show(fig)
    return None

