import numpy as np

# this function rolls a set of eulag variables 
# a discrete amount n_roll along axis ax.

def rolling(phyf11,n_roll,ax):
    print('rolling fields ...')
    for key in phyf11.keys():
        phyf11[str(key)] = np.roll(phyf11[str(key)],n_roll,axis=ax)
    print('DONE')
    return phyf11
