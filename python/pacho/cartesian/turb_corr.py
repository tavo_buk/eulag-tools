import numpy as np

# this block replaces NEMPImod.py in base_code/
def average(var,ax,ln,rn):
    # makes average specified on "ax"
    # over selected field "var" in the form
    # var = array(t,n,m,l) 
    nn = rn - ln
    if ax == 1:
        svar = var[:,ln:rn-1,:,:]
    elif ax ==2:
        svar = var[:,:,ln:rn-1,:]
    mvar = np.sum(svar,axis=ax)/nn

    return mvar

def xy_average(var):
    # computes the standard horizontal average
    # over axes 'x' and 'y' of a numpy array (tt,n,m,l)
    dims = var.shape
    return np.sum(np.sum(var,axis=1)/dims[1],axis=1)/dims[2]

def MR_corrs(phyf11,Grid,units,mean_type):
    #################################################
    #  Consider:				    #
    #  		A = <A> + a  <--  Reynolds d.	    #
    #						    #
    #  This code calculates the Reynolds and	    #
    #  Maxwell stresses: 		    	    #
    #		<u(i)u(j)>			    #
    #		<b(i)b(j)>			    #
    #						    #
    #  Using the formula:			    #
    #						    #
    #	<a*b> = <A*B> - <A>*<B> 		    #
    #						    #
    #################################################
    # this function computes correlations           #
    # of variables:                                 #
    # <u'v'> = <uv> - <u><v>                        #
    #################################################

    #mean_type  = int(input('horizontal mean (1) or one-axis mean (2) ? '))

    if mean_type == 2:

        def ask():
            axis = str(input('choose axis for mean (x) / (y): '))
            if axis == 'x':
                ax = 1
                return ax
            elif axis == 'y':
                ax = 2
                return ax
            else:
                print('Error: non-valid axis')
                return ask()

        ax = int(ask())
        v_l = ['x','y']
        p_l = [[Grid.n,Grid.Lx00],[Grid.m,Grid.Ly00]]
        left = float(input('min '+v_l[ax-1]+' val = '))
        right= float(input('max '+v_l[ax-1]+' val = '))
        ln = int((p_l[ax-1][0]/p_l[ax-1][1])*left/units.l_0)
        rn = int((p_l[ax-1][0]/p_l[ax-1][1])*right/units.l_0)
        nn = rn - ln
        m_set = [ax,ln,rn]
       
        def corr(var1,var2):
            v12 = np.multiply(var1,var2)
            mv12 = average(v12,ax,ln,rn)
            mv1 = average(var1,ax,ln,rn)
            mv2 = average(var2,ax,ln,rn)
            return mv12 - np.multiply(mv1,mv2)

    elif mean_type == 1:
        def corr(var1,var2):
            v12  = np.multiply(var1,var2)
            mv12 = xy_average(v12)
            mv1  = xy_average(var1)
            mv2  = xy_average(var2)
            return mv12 - np.multiply(mv1,mv2)

    else:
        print('ERROR: non-valid mean type !')
        return None

    print('calculating turbulent correlations ... ')
    corrs ={} # new dictionary with correlations

    # this block replaces stressesf11.py in base_code/
    uu = corr(phyf11['pux'],phyf11['pux'])
    vv = corr(phyf11['puy'],phyf11['puy'])
    ww = corr(phyf11['puz'],phyf11['puz'])
    bxbx = corr(phyf11['pbx'],phyf11['pbx'])
    byby = corr(phyf11['pby'],phyf11['pby'])
    bzbz = corr(phyf11['pbz'],phyf11['pbz'])

    uv = corr(phyf11['pux'],phyf11['puy'])
    uw = corr(phyf11['pux'],phyf11['puz'])
    vw = corr(phyf11['puy'],phyf11['puz'])
    bxby = corr(phyf11['pbx'],phyf11['pby'])
    bxbz = corr(phyf11['pbx'],phyf11['pbz'])
    bybz = corr(phyf11['pby'],phyf11['pbz'])
    d_corr = {'uu':uu,'vv':vv,'ww':ww,\
            'uv':uv,'uw':uw,'vw':vw,\
            'bxbx':bxbx,'byby':byby,'bzbz':bzbz,\
            'bxby':bxby,'bxbz':bxbz,'bybz':bybz}
    print('DONE')
    
    if mean_type == 2:
        return d_corr, m_set
    elif mean_type == 1:
        return d_corr, 'no m_set'

def uth_eddy(phyf11,Grid,units):
    # Inputs:
    # - phyf11_dat (phyf11_dat = physical.phy_vars(f11_dat))
    # - grid (grid = cart_grid.read_params())
    # - units (units = read_units())
    #
    # Output:
    # - <uz'th'> turbulent heat correlation
    mode = int(input(' global mean(1) or custom (2): '))
    if mode == 2:
        left = float(input('min x val = '))
        right = float(input('max x val = '))
        ln = int((Grid.n/Grid.Lx00)*left/units.l_0)
        rn = int((Grid.n/Grid.Lx00)*right/units.l_0)
    elif mode == 1:
        ln = 0
        rn = Grid.n
    nn = rn - ln
    print('calculating turbulent heat correlations ... ')
    wt  = np.multiply(phyf11['puz'],phyf11['pt'])
    wtm = np.sum(wt[:,ln:rn-1,:,:],axis=1)/nn
    wm  = np.sum(phyf11['puz'][:,ln:rn-1,:,:],axis=1)/nn
    tm  = np.sum(phyf11['pt'][:,ln:rn-1,:,:],axis=1)/nn
    wmtm= np.multiply(wm,tm)
    wtp = wtm - wmtm
    print('DONE.')

    return wtp

def x_eddy(phyf11,Grid,units):
    # Computes turbulent heat diffusivity <u'th'>/(d th_e/dr) 
    # (see Kitchatinov & Mazur (2011)).
    # Inputs:
    # - phyf11_dat (phyf11_dat = physical.phy_vars(f11_dat))
    # - grid (grid = cart_grid.read_params())
    # - units (units = read_units())
    #
    # Output:
    # - <uz'th'> turbulent heat correlation
    
    # polytropic strat the_e
    print('calculating turbulent heat conductivity')
    wtp = uth_eddy(phyf11,Grid,units)
    dz = Grid.Lz00*units.l_0/(Grid.l-1)
    try:
        file_strat = open("polytropic.dat","r")
    except FileNotFoundError:
        print('polytropic file not found')
    data_strat = [line.strip() for line in file_strat]
    th_e = np.zeros(Grid.l,"float32")
    for ii in range(0,Grid.l):
        th_e[ii] = float(data_strat[ii].split()[3]) 
    Lth_e = np.log(th_e)

    # derivative of d(ln(th_e))/dr
    DrLth_e = [(Lth_e[ii+1]-Lth_e[ii])/dz for ii in \
                    range(0,Grid.l-1)]
    X_t = np.zeros(np.shape(wtp),"float32")
    for ii in range(0,Grid.l-1):
        if DrLth_e[ii] == 0: 
            X_t[:,:,ii] = 0
        if not DrLth_e[ii] == 0:
            X_t[:,:,ii] = - 1e-3*wtp[:,:,ii]/(th_e[ii]*DrLth_e[ii])

    return X_t

def nempi_qp(phyf11,corrs,m_set,Grid,units,mean_type):
    #mean_type = int(input('mean type : '))
    try:
        file_strat = open("polytropic.dat","r")
    except FileNotFoundError:
        print('polytropic file not found')
    print('computing coefficients ... ')
    data_strat = [line.strip() for line in file_strat]
    p_density = np.zeros(Grid.l,"float32")
    for ii in range(0,Grid.l):
        p_density[ii] = float(data_strat[ii].split()[2])
     
    arr = np.zeros(corrs['uu'].shape)
    ux2 = np.multiply(phyf11['pux'],phyf11['pux'])
    uy2 = np.multiply(phyf11['puy'],phyf11['puy'])
    uz2 = np.multiply(phyf11['puz'],phyf11['puz'])
    u2 = ux2 + uy2 + uz2
    Ekin = np.zeros(u2.shape)
    # Ekin stands for the term mu*rho*u2
    for ii in range(0,Grid.l):
        Ekin[:,:,:,ii] = np.multiply(units.mu_0,p_density[ii])*u2[:,:,:,ii]
        if mean_type == 2:
            arr[:,:,ii] = p_density[ii]*corrs['uu'][:,:,ii]
        elif mean_type == 1:
            arr[:,ii] = p_density[ii]*corrs['uu'][:,ii]    

    bx2 = np.multiply(phyf11['pbx'],phyf11['pbx'])
    by2 = np.multiply(phyf11['pby'],phyf11['pby'])
    bz2 = np.multiply(phyf11['pbz'],phyf11['pbz']) 
    b = np.sqrt(bx2+by2+bz2)
    if mean_type == 2:
        mb = average(b,m_set[0],m_set[1],m_set[2])
        mbeq = np.sqrt(average(Ekin,m_set[0],m_set[1],m_set[2]))
    elif mean_type == 1:
        mb = xy_average(b) 
        mbeq = np.sqrt(xy_average(Ekin))

    pi_t = arr +(0.5/units.mu_0)*(corrs['byby']+corrs['bzbz']-corrs['bxbx'])
    qp = np.zeros(mb.shape)
    beta = np.zeros(mb.shape)
    for tt in range(qp.shape[0]):
        if mean_type == 2:
            qp[tt,:,:] = np.divide(np.multiply(\
                    -2*units.mu_0,pi_t[tt,:,:]-pi_t[0,:,:]),\
                    np.multiply(mb[tt,:,:],mb[tt,:,:]))
            beta[tt,:,:] = np.divide(mb[tt,:,:],mbeq[tt,:,:])
        elif mean_type == 1:
            qp[tt,:] = np.divide(np.multiply(\
                    -2*units.mu_0,pi_t[tt,:]-pi_t[0,:]),\
                    np.multiply(mb[tt,:],mb[tt,:]))
            beta[tt,:] = np.divide(mb[tt,:],mbeq[tt,:])
    print('DONE')
    return qp, beta

