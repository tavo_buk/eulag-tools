# Cartesian Analysis
---
![Alt text](pictures/collage.png)

---

An example of usage of the scripts inside this folder can be found in the Jupyter notebook (.ipynb), to run this notebook using [Jupyter](http://jupyter.org/try) - [(installation instructions in your pc)](http://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html):

* move the notebook to the folder of your simulation.
* open a server of jupyter lab from the folder of the simulation.
* follow instruccions and examples in the notebook.

---

To create vtk files from eulag data for visualization in [VisIt](https://wci.llnl.gov/simulation/computer-codes/visit/) the example code is createVTK.py 
(this script makes use of eu2vtk.py which can be modified acordingly to your needs)
