import eu2vtk as eul
import subprocess as sp

init= int(raw_input('# of slices to jump before first vtk file = '))
Nsk = int(raw_input("# of slices to skip = "))
Nit = int(raw_input("# of iterations for time cut = "))
Ntt   = int(raw_input("# of time cuts to consider = "))

data={}

for i in range(1,Ntt):
    print('printing file = '+str(i))
    data[i] = eul.EulagData(Nskip=init+i*Nsk,Niter=Nit)
    data[i].SaveStateVTK('t'+str(init+i*Nsk)+'.vtk')


