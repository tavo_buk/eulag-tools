
#### CLASS OBJECT TO CREATE PHYSICAL UNITS FOR EUCART SIMULATIONS

def read_units(*args, **kwargs):
        '''
        returns an object with the physical constants for EUCART.
        '''
        return phy_units(*args, **kwargs)

def const():
        '''
        constants to convert to physical units
        '''
        import numpy as np
        import math as ma
        l0  = 10.                         # [Mm]
        Rh0 = 2.5*10**(-4)                # [Kg / m^3]
        T0  = 5800.                       # [K]
        G0  = float(275/49.5)                  # [m / s^2]
        mu0 = 4*ma.pi*10**(-7)            # [N / A^2]
        time0 = float(np.sqrt(l0/float(G0))*10**(3))
        vel0  = float(l0/float(time0))*10**3
        bb0   = float(np.sqrt(mu0*Rh0*G0*l0)) 
        print('time-scale     = ', time0,  ' [s]')
        print('velocity scale = ', vel0, ' [Km / s]')
        print('B field scale  = ', bb0, ' [kT]') 

        return time0, vel0, bb0, l0, Rh0, T0, G0, mu0

class phy_units(object):
        def __init__ (self,param=None):
            physconst  = const()
            self.t_0   = physconst[0]
            self.v_0   = physconst[1]
            self.b_0   = physconst[2]
            self.l_0   = physconst[3]
            self.Rh_0  = physconst[4]
            self.Tm_0  = physconst[5]
            self.G_0   = physconst[6]
            self.mu_0  = physconst[7]
