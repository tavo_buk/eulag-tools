# Functions that read data from EULAG-MHD
from fortranfile import *
from Congrid import *
import cart_grid as gd
import numpy as np
import scipy.interpolate
import scipy.ndimage

def read_f11(gr,fort11 = 'fort.11'):
    '''
    - Takes as input gr : grid = cart_grid.read_params() 
    - Returns dictionary with variables stored according to vrs
    '''
    ntime = int(gr.itt/gr.nplot)
    print('# of outputs in fort.11 file: ',ntime)
    nskip  = int(input("nskip = "))
    print('temporal slices to consider : '+str(nskip)+' to '+str(ntime))
    vrs = ['u','v','w','t','p','bx','by','bz']
    blocks = []
    for var in vrs:
        blocks.append(np.zeros((ntime-nskip,gr.n,gr.m,gr.l),'float32'))

    data={}
    for i,p in enumerate(vrs):
        data.update({p:blocks[i]})
    n_var=len(data)
    if nskip >0:
        print("Skipping data from first records of fort.11 ...")
    data_file=open(fort11, "rb")

    for i in range(0,nskip):
        for j in range(0,n_var):
            np.fromfile(data_file,dtype='uint32',count=1)
            np.fromfile(data_file,dtype='float32',count=gr.n*gr.m*gr.l)
            np.fromfile(data_file,dtype='uint32',count=1)
    print('Reading data from file fort.11 ...')
    for i in range(0,ntime-nskip):
        for key in data.keys():
            np.fromfile(data_file,dtype='uint32',count=1)
            data[key][i,:,:,:]=np.reshape(np.fromfile(data_file,\
                    dtype='float32',count=gr.n*gr.m*gr.l),(gr.n,gr.m,gr.l),order= 'F')
            np.fromfile(data_file,dtype='uint32',count=1)
    data_file.close()
    print('number of outputs = ',ntime-nskip)
    print('DONE')
    return data

# VARS ORDER IN F9
vrs =['u','v','w','ox','oy','oz','ox2','oy2','oz2','th','p','fx','fy','fz','ft',\
        'pm','bx','by','bz','fbx','fby','fbz','tke','chm','fchm','time']

def read_f9(gr,fort9='fort.9'):
    """
    - Takes as input gr : grid = cart_grid.read_params() 
    - Takes as input fort9 _ default value as in eulag output
    - Returns dictionary with variables stored according to vrs
    """
    itt    = gr.itt
    nt     = gr.nt
    n      = gr.n
    m      = gr.m
    l      = gr.l
    Lx00   = gr.Lx00
    Ly00   = gr.Ly00
    Lz00   = gr.Lz00
    dt00   = gr.dt00
    nslice = gr.nslice
    noutp  = gr.noutp
    nplot  = gr.nplot
    nstore = gr.nstore
    nxaver = gr.nxaver

    MHD=True
    if fort9 == 'fort.9.n':
        print("reading new file: ", fort9)
        print("pulses = 1")
        ntf9 = 1
    if fort9 == 'fort.9':
        ntf9=int(itt/nstore)
        print("reading file: ", fort9)
        print("pulses = ", ntf9)
   
    data = {}
    for i,p in enumerate(vrs):
        if p != 'time':
            data.update({p:np.zeros((n,m,l),'float32')})
    
    data9=FortranFile(fort9)
    for t in range(ntf9):
        data['u'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['v'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['w'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['ox'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['oy'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['oz'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['ox2'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['oy2'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['oz2'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['th'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['p'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['fx'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['fy'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['fz'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['ft'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        time = data9.readReals('f')
        time = data9.readReals('f')
        time = data9.readReals('f')
        data['pm'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['bx'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['by'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['bz'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['fbx'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['fby'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['fbz'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        time = data9.readReals('f')
        time = data9.readReals('f')
        time = data9.readReals('f')	
        time = data9.readReals('f')
        time = data9.readReals('f')
        time = data9.readReals('f')
        time = data9.readReals('f')
        time = data9.readReals('f')
        time = data9.readReals('f')
        time = data9.readReals('f')
        data['tke'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        time = data9.readReals('f')	
        data['chm'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['chm'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['fchm'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
        data['fchm'][:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
   
    data['time'] = time
    return data

# RESIZE DATA FROM FORT.9 (FOR REMESHING)
########################################################################################
########################################################################################

def resizef9(dat,nn,mm,ll):
    """
    - Resizes the data (dat) of matrices from read_f9 
      to new sizes (nn,mm,ll)
    - Returns dictionary with variables stored according to vrs
    """
    print("transforming matrices ... ")
    rs_dat = {}
    for i,p in enumerate(vrs):
        if p != 'time':
            rs_dat.update({p:congrid(dat[p],(nn,mm,ll))})
    print("resized variables ready")
    rs_dat.update({'time':dat['time']})

    return rs_dat

# TO CREATE NEW FORT.9 FILE FROM RESIZED DATA
########################################################################################
########################################################################################

def writef9(dat):
    """
    Creates a new binary file fort.9.n
    using as "inputdata" a list of matrices
    with the same order given by  readf9.
    ------------------------------------------------------------
    returns : Nothing 
    WARNING = the inputdata needs to be in the same readf9 order
    """
    rv_dat = {}
    for i,p in enumerate(vrs):
        if p != 'time':
            rv_dat.update({p:np.ravel(dat[p], order = 'F')})
    rv_dat.update({'time':dat['time']})
    newfile="fort.9.n"	
    print("writting new fort.9.n file ... ")
    with FortranFile(newfile,mode='w') as ndata:	
        # reconstruct original data for binary file
        # write to new file in the exact same order of old fort.9
        order1 = ['ru','rv','rw','rox','roy','roz','rox2','roy2','roz2',\
                'rth','rp','rfx','rfy','rfz','rft']
        for p in order:
            ndata.writeReals(rv_dat[p],'f')
        
        ndata.writeReals(rv_dat['time'],'f')
        ndata.writeReals(rv_dat['time'],'f')
        ndata.writeReals(rv_dat['time'],'f')
        
        order2 = ['rpm','rbx','rby','rbz','rfbx','rfby','rfbz']
        for p in order2:
            ndata.writeReals(rv_dat[p],'f')

        ndata.writeReals(rv_dat['time'],'f')
        ndata.writeReals(rv_dat['time'],'f')
        ndata.writeReals(rv_dat['time'],'f')
        ndata.writeReals(rv_dat['time'],'f')
        ndata.writeReals(rv_dat['time'],'f')
        ndata.writeReals(rv_dat['time'],'f')
        ndata.writeReals(rv_dat['time'],'f')
        ndata.writeReals(rv_dat['time'],'f')
        ndata.writeReals(rv_dat['time'],'f')
        ndata.writeReals(rv_dat['time'],'f')
        ndata.writeReals(rtke,'f')
        ndata.writeReals(time,'f')
        ndata.writeReals(rchm,'f')
        ndata.writeReals(rchm,'f')
        ndata.writeReals(rfchm,'f')
        ndata.writeReals(rfchm,'f')

    return None
