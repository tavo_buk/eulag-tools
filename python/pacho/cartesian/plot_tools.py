import matplotlib.pyplot as plt
from pylab import *
from math import pow
from math import pi
import sys

# MISCELLANEOUS FUNCTIONS FOR POST-PROCESSING
########################################################################################
########################################################################################
import numpy as np

def filter3D(ftype,field,dim1,dim2,dim3):
    '''
    filters a field with the structure
            field = field[dim1.dim2,dim3]

    '''
    ffield = np.zeros((dim1,dim2,dim3),"float32")
    if ftype == 2:
        print("2 pt filtering ... ")
        for kk in range(0,dim3):
            if kk == dim3-1:
                ffield[:,:,kk] = field[:,:,kk]
            else:
                ffield[:,:,kk] = 0.5*(field[:,:,kk]+field[:,:,kk+1])
    elif ftype == 3:
        print("3 pt filtering ... ")
        ffield[:,:,0]      = 0.5*(field[:,:,0]+field[:,:,1])
        ffield[:,:,dim3-1] = 0.5*(field[:,:,dim3-1]+field[:,:,dim3-2])	 
        for kk in range(1,dim3-1):
            ffield[:,:,kk] = 0.25*(field[:,:,kk-1]+field[:,:,kk]+field[:,:,kk+1])
    return ffield

def full_filter3D(ftype,field):
    '''
    filters a field f(nt,n,m,l)
    '''
    dims      = np.shape(field)
    aux_field = np.zeros(dims,"float32")
    
    if ftype == 2:
        print("2 pt filtering ... ")
        for kk in range(0,dims[3]):
            if kk == dims[3]-1:
                aux_field[:,:,:,kk] = field[:,:,:,kk]
            else:
                aux_field[:,:,:,kk] = 0.5*(field[:,:,:,kk]+field[:,:,:,kk+1])
    elif ftype == 3:
            print("3 pt filtering ... ")
            aux_field[:,:,:,0]      = 0.5*(field[:,:,:,0]+field[:,:,:,1])
            aux_field[:,:,:,dims[3]-1] = 0.5*(field[:,:,:,dims[3]-1]+field[:,:,:,dims[3]-2])	 
            for kk in range(1,dims[3]-1):
                aux_field[:,:,:,kk] = 0.25*(field[:,:,:,kk-1]+field[:,:,:,kk]+field[:,:,:,kk+1])
    return aux_field

#### TO SET PLOTTING PREFERENCES

def plot_mode(*args, **kwargs):
    '''
    returns class object with plotting preferences
    '''
    return Plt_mode(*args, **kwargs)

def plt_prefs():
    '''
    sets user plotting preferences
    extension, cut, saturation
    '''
    # fix for different versions of python
    if sys.version_info[0]<3:
        extension = raw_input('extension = ')
    else:
        extension = str(input('extension = '))
    cut       = int(input('cut = '))
    val       = int(input('saturation, True (1) or False (0) = '))
    if val == 1:
        saturation = True
    if val == 0:
        saturation = False

    return extension, cut, saturation

class Plt_mode(object):
    def __init__(self,param=None):
        ext, cut, sat = plt_prefs()
        self.ext = ext
        self.cut = cut
        self.sat = sat

#### PLOTTING SCRIPTS

def plt_boxsl(pgrid,prs,field,mapa,prefs,time):
        '''
        plot slices for a field given from fort.11 
        config:
        > pgrid : physical grid for plot (from physical.read_phygrids())
        > prs   : parameters of simulations (from cart_grid.params())
        > field : variable to plot 
        > mapa  : colormap
        > prefs : plotting preferences (from plot_mode())
        > time  : slice time
        '''
        import units as un
        extension = prefs.ext
        cut       = prefs.cut
        saturation= prefs.sat
        const = un.read_units()

        sctit = int(input('colorbar title for field (mag = 0, vel = 1) = '))
        if sctit == 0:
                cbtit = 'B [T]'
        elif sctit == 1:
                cbtit = r'$u$$\times 10^3$[m/s]'
        
        if mapa==1:
                bmap=cm.afmhot
        if mapa==2:
                bmap=cm.rainbow
        if mapa==3:
                bmap=cm.CMRmap
        if mapa==4:
                bmap=cm.gist_yarg

        estilo='%.2f'
        
        if extension=="xy":
                Y,X=np.meshgrid(pgrid.yy,pgrid.xx)
        if extension=="xz":
                Y,X=np.meshgrid(pgrid.zz,pgrid.xx)
        if extension=="yz":
                Y,X=np.meshgrid(pgrid.zz,pgrid.yy)

        filtro=True
        ftype=2

#        time = int(raw_input('slice time = '))
        if extension=='xz' or extension=='yz':
            temp = filter3D(ftype,field[time,:,:,:],prs.n,prs.m,prs.l)
            if extension=='xz':
                field_f   = temp[:,cut,:]
                strxlabel = 'x [Mm]'
                strylabel = 'z [Mm]'
                mmax      = np.amax(field[:,:,cut,:])
                mmin      = np.amin(field[:,:,cut,:])
                title     = 'y = '+str(round(cut*prs.Ly00*const.l_0/prs.m,1))+ ' Mm'
            else:
                field_f   = temp[cut,:,:]
                strxlabel = 'y [Mm]'
                strylabel = 'z [Mm]'
                mmax      = np.amax(field[:,cut,:,:])
                mmin      = np.amin(field[:,cut,:,:])
                title     = 'x = '+str(round(cut*prs.Lx00*const.l_0/prs.n,1))+ ' Mm'
            orient = 'horizontal'
        else:
            field_f     = field[time,:,:,cut]
            strxlabel   = 'x [Mm]'
            strylabel   = 'y [Mm]'
            mmax        = np.amax(field[:,:,:,cut])
            mmin        = np.amin(field[:,:,:,cut])
            orient      = 'vertical' 
            title       = 'z = '+str(round(cut*prs.Lz00*const.l_0/prs.l,1))+ ' Mm'

        fsize = 18  # fontsize of labels
        fig = plt.figure()
        ax  = fig.add_subplot(111)
        bmap.set_over(bmap(0.999))
        bmap.set_under(bmap(0))
        
        choice = int(input('for data: global slice scale [0] or local slice scale [1] or custom scale [2] ? '))
        if choice == 0:
            maxval = mmax
            minval = mmin
        if choice == 1:
            maxval=np.amax(field_f)
            minval=np.amin(field_f)
        if choice == 2:
            maxval=float(input('maxval = '))
            minval=float(input('minval = '))
        print('max/min scale from slice', maxval, '/', minval)

        ticks_cb=[minval,0,0.9*maxval,maxval]
        delta=maxval-minval
        levels=np.arange(minval,maxval,delta/60)
        if saturation is True:
                sat = 4.0
        else:
                sat = 1.0
        graf = plt.contourf(X,Y,field_f,levels,alpha=0.9,cmap=bmap,extend="both",\
                vmin=minval/sat,vmax=maxval/sat)
        cb = fig.colorbar(graf,ticks=ticks_cb,orientation=orient,format=estilo)
        cb.set_label(cbtit, fontsize = fsize)
        plt.xlabel(strxlabel, fontsize = fsize)
        plt.ylabel(strylabel, fontsize = fsize)
        plt.title(title)
        ax.set_aspect('equal')
        plt_mode = int(input("save slice to eps file [yes (1)/ no (0)] ?"))
        if plt_mode==1:
            filename = str(input("choose a filename : "))
            plt.savefig(filename)
            plt.show()
        else:
            plt.show()
