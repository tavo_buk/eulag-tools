# Cartesian adaptation by F. Camacho.
# #####################################################
# # Taken from original version in spherical coords.
# # (Cortesy of A. Strugarek  :-) )
# ######################################################
# ### Typical use of this class:
# ######################################################
# ### > import EULAG_basic_module as eul
# ### > D=eul.EulagData(Path='mypath',Nskip=0,Niter=1)
# ######################################################
# ### > D.SaveStateVTK() # saves in mypath a vtk file 
# ######################################################

from __future__ import print_function
import numpy as np
import os
#from psettings import *

import modules as mo
prs   = mo.read_params()    # import eucart preferences 
const = mo.read_physical()  # import physical constants for conversion

try:
        import cPickle as pickle
except:
        print("To easily use read_elements and save_elements, install pickle")
try:
        from evtk.hl import gridToVTK
except:
        print("To ouptut vtk files, download and install 'https://bitbucket.org/pauloh/pyevtk'")

class EulagData:
    """ Basic readers of output files from EULAG
    _____________________________
    self.n    : number of points in x direction
    self.m    : number of points in y direction
    self.l    : number of points in z direction
    self.xg   : x coordinates (same for yg, zg)
    self.u    : x velocity (same for v,w,p,th,bx,by,bz)
    """
    def __init__(self,Filename='fort.11',Path='',Nskip=0,Niter=1,Mag=True,Lx=50.0,Ly=50.0,Lz=30.0):
        """ Required arguments to read eulag output file 
            Filename : output file name                             (default: fort.11)
            Path     : path where filename is                       (default: current dir)
            Nskip    : number of iterations to skip                 (default: 0)
            Niter    : number of iterations to read                 (default: 1)
            Mag      : magnetic run (for hydro set to False)        (default: True)
            Lx       : x length of cartesian box                    (default: 5.0)
            Ly       : y length of cartesian box                    (default: 5.0)
            Lz       : z length of cartesian box                    (default: 3.0)
        """

        # Define name of saved variable
        if Mag:
            self.Qs=['u','v','w','th','p','bx','by','bz']
        else:
            self.Qs=['u','v','w','th','p']


        # Path cooking
        if Path == '':
            self.Path = './'
        elif Path[-1] != '/':
            self.Path = Path+'/'
        else:
            self.Path = Path

        # Get record size, number of records in file, and deduces nr,nt,np
        self.EulagFile = self.Path+Filename # define source file
        self.GetDataInfo() # get basic information from the size of the EULAG file

        #Define the grids
        self.xg = np.linspace(0,Lx,num=self.n)
        self.yg = np.linspace(0,Ly,num=self.m)
        self.zg = np.linspace(0,Lz,num=self.l)
 
        # Sanity checks so that we are not trying to read out of the file
        if Nskip*self.BlockSize >= self.TotalSize:
            print(">> Went to the end of file skipping, parameters are wrong <<")
            print(">> Maximum rec is %i <<" % (self.TotalSize/self.BlockSize))
            return
        if Nskip*self.BlockSize + Niter*self.BlockSize >= self.TotalSize:
            print(">> Went to the end of file reading, parameters are wrong <<")
            print(">> Maximum rec is %i <<" % (self.TotalSize/self.BlockSize))
            return

        # Init Arrays
        for qq in self.Qs:
            exec "self.%s = np.zeros((self.n,self.m,self.l))" % (qq)

        # Open file
        fp = open(self.EulagFile,'rb')
        # Skip first records
        fp.seek(Nskip*self.BlockSize,0)
        # Read data
        for it in range(Niter):
            EulagBlock=self.ReadBlock(fp) # Read Block
            # Here I do a temporal average, you could insert code hereafter to change how it behaves
            for qq in self.Qs:
                exec "self.%s += EulagBlock[qq].reshape((self.n,self.m,self.l))/float(Niter)" % (qq) 
        # Close file
        fp.close()


    def ReadBlock(self,fp):
        """ Reads a basic record in EULAG file
            fp : file stream 
        """
        nn = prs.n
        mm = prs.m
        ll = prs.l
        EulagBlock={} # Dictionnary containing a block of EULAG data, returned by this function
        for qq in self.Qs:
            numval=np.fromfile(fp,dtype='uint32',count=1)[0]/np.dtype('float32').itemsize 
#            EulagBlock[qq] = np.fromfile(fp,dtype='float32',count=numval) # Read one block
            EulagBlock[qq] = np.reshape(np.fromfile(fp,dtype='float32',count=numval),(nn,mm,ll),order='F')
            numval=np.fromfile(fp,dtype='uint32',count=1)[0]/np.dtype('float32').itemsize
        return EulagBlock


    def GetDataInfo(self):
        """ Deduces basic infos from the EULAG data file
        """
        # Open file
        fp = open(self.EulagFile,'rb')
        # Read a block
        tmp=self.ReadBlock(fp)
        # Store size of one block
        self.BlockSize = fp.tell()
        # Go to end of file wihtout reading
        fp.seek(0,2)
        # Store total size
        self.TotalSize= fp.tell() 
        # Close file
        fp.close() 
        
        # number of grid points from psettings.py output
        self.n = prs.n
        self.m = prs.m
        self.l = prs.l

    def SaveStateVTK(self,outfile='test.vtk'):
        """ Saves A VTK file with all variables
            outfile: name of the output file in vtk
        """
        # Some cooking...
        if '/' not in outfile:
            ofile=self.Path+outfile

        # Create grid for VTK output
        x=np.linspace(self.xg[0],self.xg[-1],num=self.n+1)
        y=np.linspace(self.yg[0],self.yg[-1],num=self.m+1)
        z=np.linspace(self.zg[0],self.zg[-1],num=self.l+1)

        # conversion of fields to physical units
        pux = np.multiply(const.v_0,self.u)
        puy = np.multiply(const.v_0,self.v)
        puz = np.multiply(const.v_0,self.w)
        pbx = np.multiply(const.b_0*1e3,self.bx)
        pby = np.multiply(const.b_0*1e3,self.by)
        pbz = np.multiply(const.b_0*1e3,self.bz)
        pth = np.multiply(const.Tm_0,self.th)

        # Dictionnary to save
#        DictVTK = {"V_eulag": (self.u,self.v,self.w), "B_eulag": (self.bx,self.by,self.bz), "u_z": self.w}
        DictVTK = {"u_eul": (pux,puy,puz), "B_eul": (pbx,pby,pbz), "u_z": puz, "B_z": pbz, "theta": pth}
        # Save VTK File
        gridToVTK(ofile,x,y,z,cellData=DictVTK)
