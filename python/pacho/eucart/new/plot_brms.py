#########################################
#
# TO PLOT B_RMS FROM xaverages data
# First run in ipython
#
#   >> import modules as mo
#   >> prs = mo.read_params()
#   >> Rp = mo.Rprofs()
#   >> tt, brms = mo.t_brms(prs,Rp)
#
#########################################

import matplotlib.pyplot as plt

plt.plot(tt, brms)
plt.xlabel('time [days]')
plt.ylabel('$B_{rms}^{\prime}$ [T]')
plt.show()

