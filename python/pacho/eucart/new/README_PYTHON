BY: FRANCISCO CAMACHO
The new version of modules.py comprisses most of the old 
scripts, now using class structures to optimize the handling 
of data.

####################################################################################
##### FOLLOW THE NEXT STEPS TO READ DATA FROM A EUCART RUN AND MAKE SOME PLOTTING:
####################################################################################

First import the module:

	>> import modules as mo

Read the grid and settings of run:

	>> prs = mo.read_params()

Read the file (ex. fort.11) -> to read file, grid prs is required
fort.11 file struct is : u,v,w,t,p,bx,by,bz

	>> boxdata = mo.read_f11(prs) 

Convert data to physical units

	>>  realdata t,p,= mo.phy_vars(boxdata)
	>>  u,v,w,t,p,bx,by,bz = realdata

create physical grid for plotting.

	>> pgrid = mo.read_phygrid()

set plotting preferences:

	>> prefs = mo.plot_mode()

plot a slice of a variable of the converted data for a given time.
(ex. take bz, see plt_boxsl arguments)
config: plt_boxsl(pgrid,prs,field,mapa,prefs,time)
> pgrid : physical grid for plot (from read_phygrid())
> prs   : parameters of simulations
> field : variable to plot
> mapa  : colormap
> prefs : plotting preferences (from plot_mode())
> time  : slice time
	
	>> mo.plt_boxsl(pgrid,prs,bz,mapa,prefs,time)

####################################################################################
##### FOLLOW THE NEXT STEPS TO CREATE VTK FILES FOR RENDERING WITH VISIT:
####################################################################################

First modify the dimensions of the cartesian box inside the file eu2vtk.py
> Lx = 
> Ly =
> Lz =

Then, just run the script createVTK.py and follow instrucions:

	>> python createVTK.py

####################################################################################
##### FOLLOW THE NEXT STEPS TO ANALIZE xaverages.dat
####################################################################################

First, import modules and import the eucart run parameters:

	>> import modules as mo
	>> prs = mo.read_params()

Then read the file xaverages.dat:

	>> mo.read_xaver(prs)

If instead, you want to analize radial profiles, then you can use a class that 
imports automatically the eucart parameters and xaverages.dat data. Two options
available
> Average in the y direction only --> gives arrays of the form [nt,l]
> Time and y-direction averages   --> gives arrays of the form [l]

	>> rxaver = mo.Rprofs()

It is also possible to plot averaged data, using

	>> mo.plt_rprofs(rxaver)

This function automatically detects the structure of arrays in rxaver.

####################################################################################
##### FOLLOW THE NEXT STEPS TO PLOT SPECTRA
####################################################################################

Now, to plot the spectrum of energy (kinetic and/or magnetic) do the following:

	>> import modules as mo
	>> prs = mo.read_params()
	>> box = mo.read_f11(prs)
	>> phy_box = mo.phy_vars(box)
	>> k, E_kin, E_mag = mo.k_spectrum(prs,box,depth,time)
	>> plt.plot(k,E_kin)  ## in log-log scale add other settings

or to plot complete analysis set:

	>> % run -i plot_spectra.py

