import numpy as np
import scipy.interpolate
import scipy.ndimage
from fortranfile import *
import matplotlib.pyplot as plt
from pylab import *
from math import pow
from math import pi


# GRID - PARAMETERS
########################################################################################
########################################################################################

def read_params(*args, **kwargs):
        '''
        reads main parameters of a EUCART run
        '''
        return params(*args, **kwargs)

def settings():
	"""
	reads and assings variable names
	to parameters of current "folder"
	simulation
	--------------------------------
	returns a list of the relevant
	parameters for post-processing
	"""
	ee = True
	mylist = True	
	diagnostics = False

	if ee is True:
		ee_r='ee.txt'
		file_ee=open(ee_r, "r")
		data_ee=[line.strip() for line in file_ee]
        	size_ee=len(data_ee)
        	string="it="
		for i in range(size_ee):
                	if string in data_ee[i].split():
                        	it = data_ee[i].split()[1]
		itt=int(it)

	if mylist is True:
		mylist="mylist.nml"
        	file_my=open(mylist, "r")
        	data_my=[line.strip() for line in file_my]
        	size_my=len(data_my)
        	nt=int(data_my[1].split()[2][0:-1])
        	n=int(data_my[2].split()[2][0:-1])
        	m=int(data_my[3].split()[2][0:-1])
        	l=int(data_my[4].split()[2][0:-1])
        	Lx00=float(data_my[6].split()[2][0:-1])
                Ly00=float(data_my[7].split()[2][0:-1])
        	Lz00=float(data_my[8].split()[2][0:-1])
        	dt00=float(data_my[9].split()[2][0:-1])
        	nslice=int(data_my[10].split()[2][0:-1])
        	noutp=int(data_my[11].split()[2][0:-1])
        	nplot=int(data_my[12].split()[2][0:-1])
        	nstore=int(data_my[13].split()[2][0:-1])
        	nxaver=int(data_my[14].split()[2])

	if diagnostics is True:
	        print "(it,n,m,l) = ", itt,n,m,l
	        print "(Lx,Ly,Lz,dt) = ", Lx00,Ly00,Lz00,dt00


	return itt, nt, n, m, l, Lx00, Ly00, Lz00, dt00, nslice, noutp, nplot, nstore, nxaver

class params(object):
    def __init__ (self,param=None):
        parameters  = settings()
        self.itt    = parameters[0]
        self.nt     = parameters[1]
        self.n      = parameters[2]
        self.m      = parameters[3]
        self.l      = parameters[4]
        self.Lx00   = parameters[5]
        self.Ly00   = parameters[6]
        self.Lz00   = parameters[7]
        self.dt00   = parameters[8]
        self.nslice = parameters[9]
        self.noutp  = parameters[10]
        self.nplot  = parameters[11]
        self.nstore = parameters[12]
        self.nxaver = parameters[13]

# READING DATA FUNCTIONS
########################################################################################
########################################################################################

def read_xaver(prs):
	"""
        prs : class object with eucart parameters

            prs = read_params()
        
        Read file "xaverages.dat" in 
	the working folder "folder"
	---------------------------
	returns the whole data set
	in a list of matrices
	"""
	xaver="xaverages.dat"

        itt    = prs.itt
        nt     = prs.nt
        n      = prs.n
        m      = prs.m
        l      = prs.l
        Lx00   = prs.Lx00
        Ly00   = prs.Ly00
        Lz00   = prs.Lz00
        dt00   = prs.dt00
        nslice = prs.nslice
        noutp  = prs.noutp
        nplot  = prs.nplot
        nstore = prs.nstore
        nxaver = prs.nxaver

#	itt,nt,n,m,l,Lx00,Ly00,Lz00,dt00,nslice,noutp,nplot,nstore,nxaver = \
#	settings(folder)

	ntxaver=int(itt/nxaver)
	if not xaver is None:
        	thew=np.empty((ntxaver,m,l),'float64')
        	u=np.empty((ntxaver,m,l),'float64')
        	v=np.empty((ntxaver,m,l),'float64')
        	w=np.empty((ntxaver,m,l),'float64')
        	ox=np.empty((ntxaver,m,l),'float64')
        	oy=np.empty((ntxaver,m,l),'float64')
        	oz=np.empty((ntxaver,m,l),'float64')
        	u2=np.empty((ntxaver,m,l),'float64')
        	v2=np.empty((ntxaver,m,l),'float64')
        	w2=np.empty((ntxaver,m,l),'float64')
        	ox2=np.empty((ntxaver,m,l),'float64')
        	oy2=np.empty((ntxaver,m,l),'float64')
        	oz2=np.empty((ntxaver,m,l),'float64')
        	rwv=np.empty((ntxaver,m,l),'float64')
        	rwu=np.empty((ntxaver,m,l),'float64')
        	rvu=np.empty((ntxaver,m,l),'float64')
        	bx=np.empty((ntxaver,m,l),'float64')
        	by=np.empty((ntxaver,m,l),'float64')
        	bz=np.empty((ntxaver,m,l),'float64')
        	bx2=np.empty((ntxaver,m,l),'float64')
        	by2=np.empty((ntxaver,m,l),'float64')
        	bz2=np.empty((ntxaver,m,l),'float64')
        	bzby=np.empty((ntxaver,m,l),'float64')
        	bzbx=np.empty((ntxaver,m,l),'float64')
        	bxby=np.empty((ntxaver,m,l),'float64')
        	p=np.empty((ntxaver,m,l),'float64')
        	the=np.empty((ntxaver,m,l),'float64')

		data2 = thew,u,v,w,ox,oy,oz,u2,v2,w2,ox2,oy2,oz2,rwv,rwu,rvu,bx,by,bz,bx2,by2,bz2,bzby,bzbx,bxby,p,the
		n_var2=len(data2)
		print("Reading data from file xaverages.dat")
        	data_file2=open(xaver, 'rb')
		for i in range(0,ntxaver):
                	for j in range(0,n_var2):
                        	np.fromfile(data_file2,dtype='uint32',count=1)
                        	data2[j][i,:,:]=np.reshape(np.fromfile(data_file2, \
					dtype='float64',count=m*l),(m,l),order='F')
                        	np.fromfile(data_file2,dtype='uint32',count=1)
                        	np.fromfile(data_file2,dtype='uint32',count=1)
        	data_file2.close()
		return data2

def read_f11(prs):

        fort11 = 'fort.11'

        itt    = prs.itt
        nt     = prs.nt
        n      = prs.n
        m      = prs.m
        l      = prs.l
        Lx00   = prs.Lx00
        Ly00   = prs.Ly00
        Lz00   = prs.Lz00
        dt00   = prs.dt00
        nslice = prs.nslice
        noutp  = prs.noutp
        nplot  = prs.nplot
        nstore = prs.nstore
        nxaver = prs.nxaver
        
        ntime  = int(itt/nplot)
        print '# of outputs in fort.11 file: ', ntime
        nskip  = int(raw_input("nskip = "))
        print 'temporal slices to consider : '+str(nskip)+' to '+str(ntime)

        u  = np.empty((ntime-nskip,n,m,l),'float32')
        v  = np.empty((ntime-nskip,n,m,l),'float32')
        w  = np.empty((ntime-nskip,n,m,l),'float32')
        t  = np.empty((ntime-nskip,n,m,l),'float32')
        p  = np.empty((ntime-nskip,n,m,l),'float32')
        bx = np.empty((ntime-nskip,n,m,l),'float32')
        by = np.empty((ntime-nskip,n,m,l),'float32')
        bz = np.empty((ntime-nskip,n,m,l),'float32')

        data= u, v, w, t, p, bx, by, bz
	n_var=len(data)
	print("Skipping data from first records of fort.11 ...")
	data_file=open(fort11, "rb")

	for i in range(0,nskip):
		for j in range(0,n_var):
			np.fromfile(data_file,dtype='uint32',count=1)
			np.fromfile(data_file,dtype='float32',count=n*m*l)
			np.fromfile(data_file,dtype='uint32',count=1)
	print('Reading data from file fort.11 ...')
	for i in range(0,ntime-nskip):
		for j in range(0,n_var):
			np.fromfile(data_file,dtype='uint32',count=1)
			data[j][i,:,:,:]=np.reshape(np.fromfile(data_file,dtype='float32',count=n*m*l),(n,m,l),order= 'F')
			np.fromfile(data_file,dtype='uint32',count=1)
	data_file.close()
	print('number of outputs = ',ntime-nskip)

        return data

def read_f9(prs,fort9):
	"""
	Reads the "fort9" file in the directory "folder"
	-----------------------------------------------
	returns the whole set as a list of matrices
	+ time value ...
	"""
#	from fortranfile import *

        itt    = prs.itt
        nt     = prs.nt
        n      = prs.n
        m      = prs.m
        l      = prs.l
        Lx00   = prs.Lx00
        Ly00   = prs.Ly00
        Lz00   = prs.Lz00
        dt00   = prs.dt00
        nslice = prs.nslice
        noutp  = prs.noutp
        nplot  = prs.nplot
        nstore = prs.nstore
        nxaver = prs.nxaver

	MHD=True
	if fort9 == 'fort.9.n':
		print "reading new file: ", fort9
		print "pulses = 1"	
		ntf9 = 1
	if fort9 == 'fort.9':
		ntf9=int(itt/nstore)
		print "reading file: ", fort9
		print "pulses = ", ntf9
	
	data9=FortranFile(fort9)
	u=np.zeros((n,m,l), 'float32')	
	v=np.zeros((n,m,l), 'float32')	
	w=np.zeros((n,m,l), 'float32')	
	ox=np.zeros((n,m,l), 'float32')	
	oy=np.zeros((n,m,l), 'float32')	
	oz=np.zeros((n,m,l), 'float32')		
	ox2=np.zeros((n,m,l), 'float32')	
	oy2=np.zeros((n,m,l), 'float32')	
	oz2=np.zeros((n,m,l), 'float32')	
	th=np.zeros((n,m,l), 'float32')	
	p=np.zeros((n,m,l), 'float32')	
	fx=np.zeros((n,m,l), 'float32')	
	fy=np.zeros((n,m,l), 'float32')	
	fz=np.zeros((n,m,l), 'float32')	
	ft=np.zeros((n,m,l), 'float32')	
	pm=np.zeros((n,m,l), 'float32')	
	bx=np.zeros((n,m,l), 'float32')	
	by=np.zeros((n,m,l), 'float32')	
	bz=np.zeros((n,m,l), 'float32')	
	fbx=np.zeros((n,m,l), 'float32')	
	fby=np.zeros((n,m,l), 'float32')	
	fbz=np.zeros((n,m,l), 'float32')	
	tke=np.zeros((n,m,l), 'float32')	
	chm=np.zeros((n,m,l), 'float32')	
	chm=np.zeros((n,m,l), 'float32')	
	fchm=np.zeros((n,m,l), 'float32')	
	fchm=np.zeros((n,m,l), 'float32')	
	
	for t in range(ntf9):
#		print t
		u[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		v[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		w[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		ox[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		oy[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		oz[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		ox2[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		oy2[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		oz2[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		th[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		p[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		fx[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		fy[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		fz[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		ft[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		time = data9.readReals('f')
		time = data9.readReals('f')
		time = data9.readReals('f')
		pm[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		bx[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		by[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		bz[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		fbx[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		fby[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		fbz[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		time = data9.readReals('f')
		time = data9.readReals('f')
		time = data9.readReals('f')	
		time = data9.readReals('f')
		time = data9.readReals('f')
		time = data9.readReals('f')
		time = data9.readReals('f')
		time = data9.readReals('f')
		time = data9.readReals('f')
		time = data9.readReals('f')
		tke[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		time = data9.readReals('f')	
		chm[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		chm[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		fchm[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		fchm[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
	
	return u,v,w,ox,oy,oz,ox2,oy2,oz2,th,p,fx,fy,fz,ft,pm,bx,by,bz,fbx,fby,fbz, \
		tke,chm,fchm,time

# RESIZE DATA FROM FORT.9 (FOR REMESHING)
########################################################################################
########################################################################################

def resizef9(inputdata,nn,mm,ll):
	"""
	Resizes the data (inputdata) of matrices from readf9 
	to desired sizes (nn,mm,ll)
	"""
	
	u,v,w,ox,oy,oz,ox2,oy2,oz2,th,p,fx,fy,fz,ft,pm,bx,by,bz,fbx,fby,fbz, \
		tke,chm,fchm,time = inputdata
	
	print "transforming matrices ... "
	ru = congrid(u,(nn,mm,ll))
	rv = congrid(v,(nn,mm,ll))
	rw = congrid(w,(nn,mm,ll))
	rox = congrid(ox,(nn,mm,ll))
	roy = congrid(oy,(nn,mm,ll))
	roz = congrid(oz,(nn,mm,ll))
	rox2 = congrid(ox2,(nn,mm,ll))
	roy2 = congrid(oy2,(nn,mm,ll))
	roz2 = congrid(oz2,(nn,mm,ll))
	rth = congrid(th,(nn,mm,ll))
	rp = congrid(p,(nn,mm,ll))
	rfx = congrid(fx,(nn,mm,ll))
	rfy = congrid(fy,(nn,mm,ll))
	rfz = congrid(fz,(nn,mm,ll))
	rft = congrid(ft,(nn,mm,ll))
	rpm = congrid(pm,(nn,mm,ll))
	rbx = congrid(bx,(nn,mm,ll))
	rby = congrid(by,(nn,mm,ll))
	rbz = congrid(bz,(nn,mm,ll))
	rfbx = congrid(fbx,(nn,mm,ll))
	rfby = congrid(fby,(nn,mm,ll))
	rfbz = congrid(fbz,(nn,mm,ll))
	rtke = congrid(tke,(nn,mm,ll))
	rchm = congrid(chm,(nn,mm,ll))
	rfchm = congrid(fchm,(nn,mm,ll))
	print "resized variables ready"
	
	outputdata = ru,rv,rw,rox,roy,roz,rox2,roy2,roz2,rth,rp,rfx,rfy,rfz,rft,rpm,rbx,rby,rbz,rfbx,rfby,rfbz,rtke,rchm,rfchm,time
	
	return outputdata	

# TO CREATE NEW FORT.9 FILE FROM RESIZED DATA
########################################################################################
########################################################################################

def writef9(inputdata):
	"""
	Creates a new binary file fort.9.n
	using as "inputdata" a list of matrices
	with the same order given by  readf9.
	------------------------------------------------------------
	returns : Nothing 
	WARNING = the inputdata needs to be in the same readf9 order
	"""
#	from fortranfile import *	
#	itt, nt, n, m, l, Lx00, Ly00, Lz00, dt00, nslice, noutp, nplot, nstore, nxaver \
#	 = settings(folder)
	u,v,w,ox,oy,oz,ox2,oy2,oz2,th,p,fx,fy,fz,ft,pm,bx,by,bz,fbx,fby,fbz, \
                tke,chm,fchm,time = inputdata
#	print "i target folder : ", folder
#	print "parameters: itt, nt, n, m, l, Lx00, Ly00, Lz00, dt00, nslice, noutp, nplot, nstore, nxaver " 
#	print "values: ", itt, nt, n, m, l, Lx00, Ly00, \
#		Lz00, dt00, nslice, noutp, nplot, nstore, nxaver
	
	newfile="fort.9.n"	
	print "writting new fort.9.n file ... "
	with FortranFile(newfile,mode='w') as ndata:	
	#with open(newf, 'wb') as newfile:
		# open new class for fort.9 file
	#	ndata=FortranFile(newfile)
		# reconstruct original data for binary file
		ru=np.ravel(u, order = 'F')
		rv=np.ravel(v, order = 'F')
		rw=np.ravel(w, order = 'F')
		rox=np.ravel(ox, order = 'F')
		roy=np.ravel(oy, order = 'F')
		roz=np.ravel(oz, order = 'F')
		rox2=np.ravel(ox2, order = 'F')
		roy2=np.ravel(oy2, order = 'F')
		roz2=np.ravel(oz2, order = 'F')
		rth=np.ravel(th, order = 'F')
		rp=np.ravel(p, order = 'F')
		rfx=np.ravel(fx, order = 'F')
		rfy=np.ravel(fy, order = 'F')
		rfz=np.ravel(fz, order = 'F')
		rft=np.ravel(ft, order = 'F')

		rpm=np.ravel(pm, order = 'F')
		rbx=np.ravel(bx, order = 'F')
		rby=np.ravel(by, order = 'F')
		rbz=np.ravel(bz, order = 'F')
		rfbx=np.ravel(fbx, order = 'F')
		rfby=np.ravel(fby, order = 'F')
		rfbz=np.ravel(fbz, order = 'F')

		rtke=np.ravel(tke, order = 'F')

		rchm=np.ravel(chm, order = 'F')
		rchm=np.ravel(chm, order = 'F')
		rfchm=np.ravel(fchm, order = 'F')
		rfchm=np.ravel(fchm, order = 'F')
		# write to new file in the exact same order of old fort.9

		ndata.writeReals(ru,'f')
		ndata.writeReals(rv,'f')
		ndata.writeReals(rw,'f')
		ndata.writeReals(rox,'f')
		ndata.writeReals(roy,'f')
		ndata.writeReals(roz,'f')
		ndata.writeReals(rox2,'f')
		ndata.writeReals(roy2,'f')
		ndata.writeReals(roz2,'f')
		ndata.writeReals(rth,'f')
		ndata.writeReals(rp,'f')
		ndata.writeReals(rfx,'f')
		ndata.writeReals(rfy,'f')
		ndata.writeReals(rfz,'f')
		ndata.writeReals(rft,'f')

		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')

		ndata.writeReals(rpm,'f')
		ndata.writeReals(rbx,'f')
		ndata.writeReals(rby,'f')
		ndata.writeReals(rbz,'f')
		ndata.writeReals(rfbx,'f')
		ndata.writeReals(rfby,'f')
		ndata.writeReals(rfbz,'f')

		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')

		ndata.writeReals(rtke,'f')

		ndata.writeReals(time,'f')

		ndata.writeReals(rchm,'f')
		ndata.writeReals(rchm,'f')
		ndata.writeReals(rfchm,'f')
		ndata.writeReals(rfchm,'f')

	return None


########################################################################################

def congrid(a, newdims, method='linear', centre=False, minusone=True):
#   6     Arbitrary resampling of source array to new dimension sizes.
#   7     Currently only supports maintaining the same number of dimensions.
#   8     To use 1-D arrays, first promote them to shape (x,1).
#   9     
#  10     Uses the same parameters and creates the same co-ordinate lookup points
#  11     as IDL''s congrid routine, which apparently originally came from a VAX/VMS
#  12     routine of the same name.
#  13 
#  14     method:
#  15     neighbour - closest value from original data
#  16     nearest and linear - uses n x 1-D interpolations using
#  17                          scipy.interpolate.interp1d
#  18     (see Numerical Recipes for validity of use of n 1-D interpolations)
#  19     spline - uses ndimage.map_coordinates
#  20 
#  21     centre:
#  22     True - interpolation points are at the centres of the bins
#  23     False - points are at the front edge of the bin
#  24 
#  25     minusone:
#  26     For example- inarray.shape = (i,j) & new dimensions = (x,y)
#  27     False - inarray is resampled by factors of (i/x) * (j/y)
#  28     True - inarray is resampled by(i-1)/(x-1) * (j-1)/(y-1)
#  29     This prevents extrapolation one element beyond bounds of input array.
	import numpy as n
	if not a.dtype in [n.float64, n.float32]:
		a=n.cast[float](a)
		
	m1=n.cast[int](minusone)
	ofs=n.cast[int](centre) * 0.5
	old=n.array(a.shape)
	ndims=len(a.shape)
	if len(newdims) != ndims:
		print "[congrid] dimensions error. " \
			"This routine currently only support" \
			"rebinning to the same number of dimensions."
		return None
	newdims=n.asarray(newdims, dtype=float)
	dimlist=[]

	if method == 'neighbour':
		for i in range(dims):
			base=n.indices(newdims)[i]
			dimlist.append((old[i]-m1) / (newdims[i]-m1) \
				* (base+ofs)-ofs)
		cd = n.array(dimlist).round().astype(int)
		newa=a[list(cd)]
		return newa

	elif method in ['nearest','linear']:
		# calculate new dims
		for i in range(ndims):
			base = n.arange(newdims[i])
			dimlist.append((old[i]-m1)/(newdims[i]-m1) \
				* (base+ofs)-ofs)
		# specify old dims
		olddims=[n.arange(i,dtype=n.float) for i in list(a.shape)]
		#first interpolation - for ndims = any
		mint=scipy.interpolate.interp1d(olddims[-1],a,kind=method)
		newa=mint(dimlist[-1])

		trorder = [ndims-1] + range(ndims-1)
		for i in range(ndims-2,-1,-1):
			newa = newa.transpose(trorder)
			mint = scipy.interpolate.interp1d(olddims[i], newa, kind=method)
			newa = mint(dimlist[i])

		if ndims > 1:
			#need one more transpose to return to original dimensions
			newa = newa.transpose(trorder)

		return newa

	elif method in ['spline']:
		oslices = [slice(0,j) for j in old]
		oldcoords = n.ogrid[oslices]
		nslices = [slice(0,j) for j in list(newdims)]
		newcoords = n.mgrid[nslices]

		newcoords_dims = range(n.rank(newcoords))
		#make first index last
		newcoords_dims.append(newcoords_dims.pop(0))
		newcoords_tr = newcoords.transpose(newcoords_dims)
		# makes a view that affects newcoords

		newcoords_tr += ofs
		
		deltas = (n.asarray(old)-m1)/(newdims-m1)
		newcoords_tr *= deltas
		
		newcoords_tr -= ofs
		
		newa=scipy.ndimage.map_coordinates(a, newcoords)
		return newa
	else:
		print "Congrid error: Unrecognized interpolation type.\n", \
			"Currently only \'neighbour\', \'nearest\',\'linear\',", \
			"and \'spline\' are supported."
		return none

# MISCELLANEOUS FUNCTIONS FOR POST-PROCESSING
########################################################################################
########################################################################################

def filter3D(ftype,field,dim1,dim2,dim3):
	'''
	filters a field with the structure
		field = field[dim1.dim2,dim3]

	'''
	ffield = np.zeros((dim1,dim2,dim3),"float32")
	if ftype == 2:
		print "2 pt filtering ... "
		for kk in range(0,dim3):
			if kk == dim3-1:
				ffield[:,:,kk] = field[:,:,kk]
			else:
				ffield[:,:,kk] = 0.5*(field[:,:,kk]+field[:,:,kk+1])
	elif ftype == 3:
		print "3 pt filtering ... "
		ffield[:,:,0]      = 0.5*(field[:,:,0]+field[:,:,1])
		ffield[:,:,dim3-1] = 0.5*(field[:,:,dim3-1]+field[:,:,dim3-2])	 
		for kk in range(1,dim3-1):
			ffield[:,:,kk] = 0.25*(field[:,:,kk-1]+field[:,:,kk]+field[:,:,kk+1])
	return ffield

def full_filter3D(ftype,field):
        '''
        filters a field f(nt,n,m,l)
        '''
        import numpy as np 
        dims      = np.shape(field)
        aux_field = np.zeros(dims,"float32")
        
	if ftype == 2:
		print "2 pt filtering ... "
		for kk in range(0,dims[3]):
			if kk == dims[3]-1:
                                aux_field[:,:,:,kk] = field[:,:,:,kk]
			else:
                                aux_field[:,:,:,kk] = 0.5*(field[:,:,:,kk]+field[:,:,:,kk+1])
	elif ftype == 3:
		print "3 pt filtering ... "
                aux_field[:,:,:,0]      = 0.5*(field[:,:,:,0]+field[:,:,:,1])
                aux_field[:,:,:,dims[3]-1] = 0.5*(field[:,:,:,dims[3]-1]+field[:,:,:,dims[3]-2])	 
		for kk in range(1,dims[3]-1):
                        aux_field[:,:,:,kk] = 0.25*(field[:,:,:,kk-1]+field[:,:,:,kk]+field[:,:,:,kk+1])
	return aux_field

#### CLASS OBJECT TO CREATE PHYSICAL UNITS FOR EUCART SIMULATIONS

def read_physical(*args, **kwargs):
        '''
        returns an object with the physical constants for EUCART.
        '''
        return Phys_const(*args, **kwargs)

def physical():
        '''
        constants to convert to physical units
        '''
        import numpy as np
        import math as ma
        l0  = 10.                         # [Mm]
        Rh0 = 2.5*10**(-4)                # [Kg / m^3]
        T0  = 5800.                       # [K]
        G0  = float(275/49.5)                  # [m / s^2]
        mu0 = 4*ma.pi*10**(-7)            # [N / A^2]
        time0 = float(np.sqrt(l0/float(G0))*10**(3))
        vel0  = float(l0/float(time0))*10**3
        bb0   = float(np.sqrt(mu0*Rh0*G0*l0)) 
        print 'time-scale     = ', time0,  ' [s]'
        print 'velocity scale = ', vel0, ' [Km / s]'
        print 'B field scale  = ', bb0, ' [kT]' 

        return time0, vel0, bb0, l0, Rh0, T0, G0, mu0

class Phys_const(object):
        def __init__ (self,param=None):
            physconst  = physical()
            self.t_0   = physconst[0]
            self.v_0   = physconst[1]
            self.b_0   = physconst[2]
            self.l_0   = physconst[3]
            self.Rh_0  = physconst[4]
            self.Tm_0  = physconst[5]
            self.G_0   = physconst[6]
            self.mu_0  = physconst[7]

#### GIVES EUCART FIELDS IN PHYSICAL UNITS

def phy_vars(inputdata):
        '''
        takes as argument fort.11 data -> u,v,w,t,p,bx,by,bz
        '''
        u,v,w,t,p,bx,by,bz = inputdata
        const = read_physical()

        print 'temperature and pressure still not transformed !'
        pux = np.multiply(const.v_0,u)
        puy = np.multiply(const.v_0,v)
        puz = np.multiply(const.v_0,w)
        pt  = t
        pp  = p
        pbx = np.multiply(const.b_0*1e3,bx)
        pby = np.multiply(const.b_0*1e3,by)
        pbz = np.multiply(const.b_0*1e3,bz)

        return pux,puy,puz,pt,pp,pbx,pby,pbz

#### PHYSICAL GRID FOR PLOTTINGS

def read_phygrid(*args, **kwargs):
        '''
        returns physical grid for plt_boxsl
        '''
        return Phy_grid(*args, **kwargs)

class Phy_grid(object):
        '''
        creates physical grid for plt_boxsl
        '''
        def __init__ (self,param=None):
            phy = read_physical()
            itt, nt, n, m, l, Lx00, Ly00, Lz00, dt00, nslice, noutp, nplot, nstore, nxaver = settings()
            Lx0_0 = phy.l_0*Lx00
            Ly0_0 = phy.l_0*Ly00
            Lz0_0 = phy.l_0*Lz00
            dx = Lx0_0/(n-1)
            if not m == 1:
                    dy = Ly0_0/(m-1)
            dz = Lz0_0/(l-1)
            xx = np.arange(0,Lx0_0+dx,dx)
            if not m == 1:
                    yy = np.arange(0,Ly0_0+dy,dy)
            zz = np.arange(0,Lz0_0+dz,dz)

            self.xx = xx
            self.yy = yy
            self.zz = zz

#### TO SET PLOTTING PREFERENCES

def plot_mode(*args, **kwargs):
        '''
        returns class object with plotting preferences
        '''
        return Plt_mode(*args, **kwargs)

def plt_prefs():
        '''
        sets user plotting preferences
        extension, cut, saturation
        '''
        extension = raw_input('extension = ')
        cut       = int(raw_input('cut = '))
        val       = int(raw_input('saturation, True (1) or False (0) = '))
        if val == 1:
            saturation = True
        if val == 0:
            saturation = False

        return extension, cut, saturation

class Plt_mode(object):
        def __init__(self,param=None):
            ext, cut, sat = plt_prefs()
            self.ext = ext
            self.cut = cut
            self.sat = sat

#### PLOTTING SCRIPTS

def plt_boxsl(pgrid,prs,field,mapa,prefs,time):
        '''
        plot slices for a field given from fort.11 
        config:
        > pgrid : physical grid for plot (from read_phygrid())
        > prs   : parameters of simulations
        > field : variable to plot
        > mapa  : colormap
        > prefs : plotting preferences (from plot_mode())
        > time  : slice time
        '''
        extension = prefs.ext
        cut       = prefs.cut
        saturation= prefs.sat
        const = read_physical()

        sctit = int(raw_input('colorbar title for field (mag = 0, vel = 1) = '))
        if sctit == 0:
                cbtit = 'B [T]'
        elif sctit == 1:
                cbtit = r'$u$$\times 10^3$[m/s]'
        
        if mapa==1:
                bmap=cm.afmhot
        if mapa==2:
                bmap=cm.rainbow
        if mapa==3:
                bmap=cm.CMRmap
        if mapa==4:
                bmap=cm.gray

        estilo='%.2f'
        
        if extension=="xy":
                Y,X=np.meshgrid(pgrid.yy,pgrid.xx)
        if extension=="xz":
                Y,X=np.meshgrid(pgrid.zz,pgrid.xx)
        if extension=="yz":
                Y,X=np.meshgrid(pgrid.zz,pgrid.yy)

        filtro=True
        ftype=2

#        time = int(raw_input('slice time = '))
        if extension=='xz' or extension=='yz':
            temp = filter3D(ftype,field[time,:,:,:],prs.n,prs.m,prs.l)
            if extension=='xz':
                field_f   = temp[:,cut,:]
                strxlabel = 'x [Mm]'
                strylabel = 'z [Mm]'
                mmax      = np.amax(field[:,:,cut,:])
                mmin      = np.amin(field[:,:,cut,:])
                title     = 'y = '+str(round(cut*prs.Ly00*const.l_0/prs.m,1))+ ' Mm'
            else:
                field_f   = temp[cut,:,:]
                strxlabel = 'y [Mm]'
                strylabel = 'z [Mm]'
                mmax      = np.amax(field[:,cut,:,:])
                mmin      = np.amin(field[:,cut,:,:])
                title     = 'x = '+str(round(cut*prs.Lx00*const.l_0/prs.n,1))+ ' Mm'
            orient = 'horizontal'
        else:
            field_f     = field[time,:,:,cut]
            strxlabel   = 'x [Mm]'
            strylabel   = 'y [Mm]'
            mmax        = np.amax(field[:,:,:,cut])
            mmin        = np.amin(field[:,:,:,cut])
            orient      = 'vertical' 
            title       = 'z = '+str(round(cut*prs.Lz00*const.l_0/prs.l,1))+ ' Mm'

        fsize = 18  # fontsize of labels
        fig = plt.figure()
        ax  = fig.add_subplot(111)
        bmap.set_over(bmap(0.999))
        bmap.set_under(bmap(0))
        
        choice = int(raw_input('for data: global slice scale [0] or local slice scale [1] or custom scale [2] ? '))
        if choice == 0:
            maxval = mmax
            minval = mmin
        if choice == 1:
            maxval=np.amax(field_f)
            minval=np.amin(field_f)
        if choice == 2:
            maxval=float(raw_input('maxval = '))
            minval=float(raw_input('minval = '))
        print 'max/min scale from slice', maxval, '/', minval

        ticks_cb=[minval,0,0.9*maxval,maxval]
        delta=maxval-minval
        levels=np.arange(minval,maxval,delta/60)
        if saturation is True:
                sat = 4.0
        else:
                sat = 1.0
        graf = plt.contourf(X,Y,field_f,levels,alpha=0.9,cmap=bmap,vmin=minval/sat,vmax=maxval/sat)
        cb = fig.colorbar(graf,ticks=ticks_cb,orientation=orient,format=estilo)
        cb.set_label(cbtit, fontsize = fsize)
        graf.cmap.set_under('k')
        plt.xlabel(strxlabel, fontsize = fsize)
        plt.ylabel(strylabel, fontsize = fsize)
        plt.title(title)
        ax.set_aspect('equal')
        plt_mode = int(raw_input("save slice to eps file [yes (1)/ no (0)] ?"))
        if plt_mode==1:
            filename = str(raw_input("choose a filename : "))
            plt.savefig(filename)
            plt.show()
        else:
            plt.show()

def plt_boxseq(prs,pgrid,field,mapa,prefs):
        '''
        plots and saves a sequence of .png files of slices.
        '''

        extension = prefs.ext
        cut       = prefs.cut
        saturation= prefs.sat
        const = read_physical()

        sctit = int(raw_input('colorbar title for field (mag = 0, vel = 1) = '))
        if sctit == 0:
                cbtit = 'B [T]'
        elif sctit == 1:
                cbtit = r'$u$$\times 10^3$[m/s]'
        
        if mapa==1:
                bmap=cm.afmhot
        if mapa==2:
                bmap=cm.rainbow
        if mapa==3:
                bmap=cm.CMRmap
        if mapa==4:
                bmap=cm.gray

        estilo='%.2f'
        
        if extension=="xy":
                Y,X=np.meshgrid(pgrid.yy,pgrid.xx)
        if extension=="xz":
                Y,X=np.meshgrid(pgrid.zz,pgrid.xx)
        if extension=="yz":
                Y,X=np.meshgrid(pgrid.zz,pgrid.yy)

        filtro=True
        ftype=2

        maxval = float(raw_input('maxval = '))
        minval = float(raw_input('minval = '))
        tinit  = int(raw_input('initial time = '))
        tend   = int(raw_input('final time   = '))
        strfield = raw_input('field name :')
        for time in range(tinit,tend):
            if extension=='xz' or extension=='yz':
                temp = filter3D(ftype,field[time,:,:,:],prs.n,prs.m,prs.l)
                if extension=='xz':
                    field_f   = temp[:,cut,:]
                    strxlabel = 'x [Mm]'
                    strylabel = 'z [Mm]'
                    mmax      = np.amax(field[:,:,cut,:])
                    mmin      = np.amin(field[:,:,cut,:])
                    title     = 'y = '+str(round(cut*prs.Ly00*const.l_0/prs.m,1))+ ' Mm'
                else:
                    field_f   = temp[cut,:,:]
                    strxlabel = 'y [Mm]'
                    strylabel = 'z [Mm]'
                    mmax      = np.amax(field[:,cut,:,:])
                    mmin      = np.amin(field[:,cut,:,:])
                    title     = 'x = '+str(round(cut*prs.Lx00*const.l_0/prs.n,1))+ ' Mm'
                orient = 'horizontal'
            else:
                field_f     = field[time,:,:,cut]
                strxlabel   = 'x [Mm]'
                strylabel    = 'y [Mm]'
                mmax      = np.amax(field[:,:,:,cut])
                mmin      = np.amin(field[:,:,:,cut])
                orient      = 'vertical' 
                title     = 'z = '+str(round(cut*prs.Lz00*const.l_0/prs.l,1))+ ' Mm'

            fsize = 18  # fontsize of labels
            fig = plt.figure()
            ax  = fig.add_subplot(111)
            bmap.set_over(bmap(0.999))
            bmap.set_under(bmap(0))
            
            ticks_cb=[minval,0,0.9*maxval,maxval]
            delta=maxval-minval
            levels=np.arange(minval,maxval,delta/60)
            if saturation is True:
                    sat = 4.0
            else:
                    sat = 1.0
            graf = plt.contourf(X,Y,field_f,levels,alpha=0.9,cmap=bmap,vmin=minval/sat,vmax=maxval/sat)
            cb = fig.colorbar(graf,ticks=ticks_cb,orientation='vertical',format=estilo)
            cb.set_label(cbtit, fontsize = fsize)
            plt.xlabel(strxlabel, fontsize = fsize)
            plt.ylabel(strylabel, fontsize = fsize)
            plt.title(title)
            ax.set_aspect('equal')
#            plt.show()
            if (time) < 10:
                timestr = '00'+str(time)
            elif (time) < 100:
                timestr = '0'+str(time)
            else:
                timestr = str(time)
            filename = strfield+'_'+str(cut)+'_'+extension+timestr+'.png'
            plt.savefig(filename)
            print 'slice saved to file '+filename
            plt.close()

#### TO COMPUTE RADIAL PROFILES (xaverages.dat)

class Rprofs():
        '''
        returns an object with the xy or xyt 
        averaged variables of xaverages.dat
        '''
        def __init__(self,prs=read_params()):
            inpu1 = raw_input('compute temporal average [y/n] ? ')
    #        prs   = read_params()
            rxav  = read_xaver(prs)
    #        thew,u,v,w,ox,oy,oz,u2,v2,w2,ox2,oy2,oz2,rwv,rwu,rvu,bx,by,bz,bx2,by2,bz2,bzby,bzbx,bxby,p,the = rxav
            self.Qs = ['thew','u','v','w','ox','oy','oz','u2', \
                    'v2','w2','ox2','oy2','oz2','rwv','rwu','rvu', \
                    'bx','by','bz','bx2','by2','bz2','bzby','bzbx', \
                    'bxby','p','the']
            count = 0
            for qq in self.Qs:
                if inpu1 == 'n':
                    exec 'self.%s = np.sum(rxav[count],axis=1)/prs.m' % (qq)
                if inpu1 == 'y':
                    exec 'self.%s = np.sum(np.sum(rxav[count],axis=1)/prs.m,\
                            axis=0)/np.shape(rxav)[0]' % (qq)
                print count
                count = count + 1
            self.inpu = inpu1        


#### PLOT OF RADIAL PROFILES (xaverages.dat)

def plt_rprofs(Rp):
        '''
        plot of radial profiles from class Rprofs = Rp
        '''
        inpu2 = int(raw_input('depth of ionization layer (<l) = '))
        print 'converting to physical units ...'
        const = read_physical()
        v2_0  = const.v_0*const.v_0
        b2_0  = const.b_0*const.b_0*1e6
        pu    = np.multiply(const.v_0,Rp.u)
        pv    = np.multiply(const.v_0,Rp.v)
        pw    = np.multiply(const.v_0,Rp.w)
        pu2   = np.multiply(v2_0,Rp.u2)
        pv2   = np.multiply(v2_0,Rp.v2)
        pw2   = np.multiply(v2_0,Rp.w2)
        pbx   = np.multiply(const.b_0*1e3,Rp.bx)
        pby   = np.multiply(const.b_0*1e3,Rp.by)
        pbz   = np.multiply(const.b_0*1e3,Rp.bz)
        pbx2  = np.multiply(b2_0,Rp.bx2)
        pby2  = np.multiply(b2_0,Rp.by2)
        pbz2  = np.multiply(b2_0,Rp.bz2)
        pth   = np.multiply(const.Tm_0,Rp.the)
        maxu2 = np.sqrt(np.amax([np.amax(pu2),np.amax(pv2),np.amax(pw2)]))
        maxb2 = np.sqrt(np.amax([np.amax(pbx2),np.amax(pby2),np.amax(pbz2)]))
        maxth = np.amax(pth)
        minth = np.amin(pth)
        prs   = read_params()
        depth = const.l_0*prs.Lz00
        grd   = np.arange(0,depth,depth/prs.l)
        if Rp.inpu == 'y':
            f1 = plt.figure()
            plt.plot(grd,np.sqrt(pu2),label='$u_x$')
            plt.plot(grd,np.sqrt(pv2),label='$u_y$')
            plt.plot(grd,np.sqrt(pw2),label='$u_z$')
            plt.plot(np.ones(20)*inpu2*depth/prs.l,np.arange(0,maxu2,maxu2/20),'k--')
            plt.xlabel('z [Mm]')
            plt.ylabel(r'$\langle u_{rms} \rangle_T \times 10^3$ [m/s]')
            plt.legend()

            f2 = plt.figure()
            plt.plot(grd,np.sqrt(pbx2),label='$B_x$')
            plt.plot(grd,np.sqrt(pby2),label='$B_y$')
            plt.plot(grd,np.sqrt(pbz2),label='$B_z$')
            plt.plot(np.ones(20)*inpu2*depth/prs.l,np.arange(0,maxb2,maxb2/20),'k--')
            plt.xlabel('z [Mm]')
            plt.ylabel(r'$\langle B_{rms} \rangle_T $ [T]')
            plt.legend()
        
            f3 = plt.figure()
            plt.plot(grd,pth)
            plt.plot(np.ones(20)*inpu2*depth/prs.l,np.arange(minth,maxth,\
                    (maxth-minth)/20),'k--')
            plt.xlabel('z [Mm]')
            plt.ylabel(r'$ \langle \theta_{rms}^{\prime} \rangle_T $ [K]')

            plt.show(f1)
            plt.show(f2)
            plt.show(f3)
        if Rp.inpu == 'n':
            mode = True
            nt_dic = []
            while mode:
                select          = int(raw_input('choose time to plot = '))
                nt_dic.append(select)
                opt             = raw_input('want to add another time [y/n] ? ')
                if opt == 'y':
                    print nt_dic
                elif opt == 'n':
                    print nt_dic
                    mode = False
                else:
                    print 'ERROR, please choose [y/n] options'
                    break
            f1 = plt.figure()
            for i in range(np.size(nt_dic)):
                plt.plot(grd,np.sqrt(pu2[i,:]),'b--',label='$u_x$')
                plt.plot(grd,np.sqrt(pv2[i,:]),'r--',label='$u_y$')
                plt.plot(grd,np.sqrt(pw2[i,:]),'g--',label='$u_z$')
            plt.plot(np.ones(20)*inpu2*depth/prs.l,np.arange(0,maxu2,maxu2/20),'k--')
            plt.xlabel('z [Mm]')
            plt.ylabel(r'$u_{rms} \times 10^3$ [m/s]')
            plt.legend()
            
            f2 = plt.figure()
            for i in range(np.size(nt_dic)):
                plt.plot(grd,np.sqrt(pbx2[i,:]),'b--',label='$B_x$')
                plt.plot(grd,np.sqrt(pby2[i,:]),'r--',label='$B_y$')
                plt.plot(grd,np.sqrt(pbz2[i,:]),label='$B_z$')
            plt.plot(np.ones(20)*inpu2*depth/prs.l,np.arange(0,maxb2/3,maxb2/3/20),'k--')
            plt.xlabel('z [Mm]')
            plt.ylabel('$B_{rms} $ [T]')
            plt.legend()

            f3 = plt.figure()
            for i in range(np.size(nt_dic)):
                plt.plot(grd,pth[i,:],label='nt = '+str(i))
            plt.plot(np.ones(20)*inpu2*depth/prs.l,np.arange(minth,maxth,\
                    (maxth-minth)/20),'k--')
            plt.xlabel('z [Mm]')
            plt.ylabel(r'$\theta_{rms}^{\prime}$ [K]')
            plt.legend()
            
            plt.show(f1)
            plt.show(f2)
            plt.show(f3)
            
#### SPECTRA FROM xaverages.dat and fort.11

def t_brms(prs,Rp):
        '''
        prs     : parameters of eucart run
        Rp      : Radial profiles  (xy only, not temporal) from xaverages.dat
        '''
        # From xaverages.dat ...
        const = read_physical()
        tbxbx  = np.multiply(Rp.bx,Rp.bx)
        tbyby  = np.multiply(Rp.by,Rp.by)
        tbzbz  = np.multiply(Rp.bz,Rp.bz)
        dbx2   = np.sum(Rp.bx2,axis=1)/prs.l - np.sum(tbxbx,axis=1)/prs.l
        dby2   = np.sum(Rp.by2,axis=1)/prs.l - np.sum(tbyby,axis=1)/prs.l
        dbz2   = np.sum(Rp.bz2,axis=1)/prs.l - np.sum(tbzbz,axis=1)/prs.l
        brms   = np.multiply(const.b_0*1e3,np.sqrt(dbx2+dby2+dbz2))
        time   = range(len(brms))
        tau0   = prs.nxaver*prs.dt00*const.t_0/(3600*24) # time scales in units of earth days
        for i in range(np.size(time)):
            time[i] = tau0*time[i]

        return time, brms


def k_spectrum(prs,phy_box,dp,tt):
        '''
        prs     : parameters of eucart run
        phy_box : fort.11 data in physical units.
        '''
        const = read_physical()
        # From fort.11 in physical units
        u,v,w,t,p,bx,by,bz = phy_box
        ksp = range(prs.m/2)
        for i in range(np.size(ksp)):
            ksp[i] = ksp[i]/const.l_0

        nn = prs.n
        mm = prs.m
        kxmin  = 2*pi/(const.l_0*prs.Lx00)
        kxmax  = kxmin*nn
        kymin  = 2*pi/(const.l_0*prs.Ly00)
        kymax  = kymin*mm
        arrayx = (range(nn/2+1)+range(-nn/2+1,0))
        arrayy = (range(mm/2+1)+range(-mm/2+1,0))
        kx     = [arrayx[i]*kxmin for i in range(nn)]
        ky     = [arrayy[i]*kymin for i in range(mm)]
        ks     = range(mm/2)
        sizes  = np.shape(u)
        nt     = sizes[0]

        fft_u  = np.fft.fftn(u[tt,:,:,dp])
        fft_v  = np.fft.fftn(v[tt,:,:,dp])
        fft_w  = np.fft.fftn(w[tt,:,:,dp])
        fft_bx = np.fft.fftn(bx[tt,:,:,dp])
        fft_by = np.fft.fftn(by[tt,:,:,dp])
        fft_bz = np.fft.fftn(bz[tt,:,:,dp])
        Es_kin = np.zeros((np.size(ks)),'float32') 
        Es_mag = np.zeros((np.size(ks)),'float32')

        for ix in range(nn):
            for iy in range(mm):
                k = int(np.sqrt(pow(kx[ix],2)+pow(ky[iy],2)))
                if k in ks:
                    Es_kin[k] = Es_kin[k]+0.5*(\
                            pow(np.abs(fft_u[ix,iy]),2)+\
                            pow(np.abs(fft_v[ix,iy]),2)+\
                            pow(np.abs(fft_w[ix,iy]),2))
                    Es_mag[k] = Es_mag[k]+0.5*(\
                            pow(np.abs(fft_bx[ix,iy]),2)+\
                            pow(np.abs(fft_by[ix,iy]),2)+\
                            pow(np.abs(fft_bz[ix,iy]),2))

        return ksp,Es_kin,Es_mag

#        fig1, axes1 = plt.subplots(dpi=90)
#        axes1.set_xlim([0.5e-1,2])
#        axes1.set_ylim([1e3,1e6])
#        axes1.plot(ksp,Es_kin)
#        axes1.set_yscale('log')
#        axes1.set_xscale('log')

#        axes2 = fig1.add_axes([0.25,0.21,0.35,0.2])
#        axes2.plot(time,brms)
#        plt.show(fig1)

        
        
