
#### SCRIPT TO PLOT SPECTRA OF KINETIC AND MAGNETIC ENERGY
#
#  First, do:
#       >> import modules as mo
#       >> prs     = mo.read_params()
#       >> box     = mo.read_f11(prs)
#       >> phy_box = mo.phy_vars(box)
#       
###########################################################
import matplotlib.pyplot as plt
import numpy as np
from math import pow

const  = mo.read_physical()
times  = [1,10,20,37]
tau0   = prs.nplot*prs.dt00*const.t_0/(3600*24) # time unit in earth days
zdepth = [190,200,206]
styles = ['--','-',':']
KK     = []
ESK    = []
ESM    = []
ksp    = []
Es_kin = []
Es_mag = []

for zz in zdepth:
    for i in times:
        temp1, temp2, temp3  = mo.k_spectrum(prs,phy_box,zz,i)
        ksp.append(temp1)
        Es_kin.append(temp2)
        Es_mag.append(temp3)
    KK.append(ksp)
    ESK.append(Es_kin)
    ESM.append(Es_mag)
    ksp = []
    Es_kin = []
    Es_mag = []


const = mo.read_physical()
knum=np.multiply(range(1,prs.m/2),1/const.l_0)
num = np.size(knum)
Kolmogorov = [10e4*pow(knum[i],-5/3) for i in range(num)]

f1 = plt.figure()
ax1 = f1.add_subplot(111)
ax1.set_xlim([0.1,1.1])
for zz in range(np.size(zdepth)):
    for i in range(np.size(times)):
        ax1.plot(KK[zz][i][1:-2],ESK[zz][i][1:-2],linestyle=styles[zz],\
                label='t='+str(round(times[i]*tau0,1))+' days '+'(z='+\
                str(round(zdepth[zz]*prs.Lz00*const.l_0/prs.l,1))+' Mm)')
ax1.plot(knum,Kolmogorov,'k--')
ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.set_xlabel(r'$k_{\perp}$ [Mm]$^{-1}$',fontsize=14)
ax1.set_ylabel(r'$E_k(k)$$\times 10^{12}$ [m$^3$s$^{-2}$]',fontsize=14)
ax1.legend()

f2 = plt.figure()
plt.xlim([0.1,1.1])
for zz in range(np.size(zdepth)):
    for i in range(np.size(times)):
        plt.plot(KK[zz][i],ESM[zz][i],linestyle=styles[zz],\
                label='t='+str(round(times[i]*tau0,1))+' days '+'(z='+\
                str(round(zdepth[zz]*prs.Lz00*const.l_0/prs.l,1))+' Mm)')
plt.plot(knum,Kolmogorov,'k--')
plt.xscale('log')
plt.yscale('log')
plt.xlabel(r'$k_{\perp}$ [Mm]$^{-1}$',fontsize=14)
plt.ylabel(r'$E_m(k)$$\times 10^{12}$ [m$^3$s$^{-2}$]',fontsize=14)
plt.legend()

plt.show(f1)
plt.show(f2)



