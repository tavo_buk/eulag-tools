from prxaver import *
import numpy as np
import matplotlib.pyplot as plt

uu=np.multiply(u,u)
vv=np.multiply(v,v)
ww=np.multiply(w,w)
bxbx=np.multiply(bx,bx)
byby=np.multiply(by,by)
bzbz=np.multiply(bz,bz)

tsu2=np.sum(np.sum(u2,axis=2)/l,axis=1)/m-np.sum(np.sum(uu,axis=2)/l,axis=1)/m
tsv2=np.sum(np.sum(v2,axis=2)/l,axis=1)/m-np.sum(np.sum(vv,axis=2)/l,axis=1)/m
tsw2=np.sum(np.sum(w2,axis=2)/l,axis=1)/m-np.sum(np.sum(ww,axis=2)/l,axis=1)/m
tsbx2=np.sum(np.sum(bx2,axis=2)/l,axis=1)/m-np.sum(np.sum(bxbx,axis=2)/l,axis=1)/m
tsby2=np.sum(np.sum(by2,axis=2)/l,axis=1)/m-np.sum(np.sum(byby,axis=2)/l,axis=1)/m
tsbz2=np.sum(np.sum(bz2,axis=2)/l,axis=1)/m-np.sum(np.sum(bzbz,axis=2)/l,axis=1)/m
tsp=np.sum(np.sum(p,axis=2)/l,axis=1)/m
tsth=np.sum(np.sum(the,axis=2)/l,axis=1)/m

urms=np.sqrt(tsu2+tsv2+tsw2)
brms=np.sqrt(tsbx2+tsby2+tsbz2)
year=1.
time= np.multiply(nxaver*dt00,range(len(tsu2)))/year


iplot = raw_input('plot tseries ? [y/n]')
if iplot == 'y':
	plt.figure(1)

	plt.subplot(311)
	plt.ylim(0,np.max(urms))
	plt.plot(time,urms)
	plt.ylabel('urms')
	
	plt.subplot(312)
	plt.ylim(0,np.max(brms))
	plt.plot(time,brms)
	plt.ylabel('brms')

	plt.subplot(313)
	plt.ylim(0,np.max(tsth)+0.01)
	plt.plot(time,tsth)
	plt.ylabel('tsth')
	plt.xlabel('time')

	plt.show()
