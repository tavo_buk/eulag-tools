# run with -i flag
# by default this works only with last = 1 (one output from fort.11)


from modules import *
import numpy as np
import matplotlib.pyplot as plt
from pylab import *
import sys
from matplotlib import gridspec

def convert(s):
	if s == 'True':
		return True
	elif s == 'False':
		return False
	else:
		sys.exit('ERROR : NON-boolean value') 

# preferences .........................................

maps = cm.afmhot, cm.rainbow, cm.CMRmap
#vars = u, v, w, t, p, bx, by, bz
print " u  -> 0"
print " v  -> 1"
print " w  -> 2"
print " t  -> 3"
print " p  -> 4"
print " bx -> 5"
print " by -> 6"
print " bz -> 7"
var        = raw_input("field to plot: ")
var        = int(var)

print 'y size = ', Ly00
cutxz      = raw_input('xz cut (y axis) = ')
cutxz      = float(cutxz)
cutxz1     = int(cutxz/dy1)+1
cutxz2     = int(cutxz/dy2)+1
cutxz3     = int(cutxz/dy3)+1
#print 'z size = ', Lz00
#cutxy      = raw_input('xy cut (z axis) = ')
#cutxy      = float(cutxy)
#cutxy1     = int(cutxy/dz1)+1
#cutxy2     = int(cutxy/dz2)+1
#cutxy3     = int(cutxy/dz3)+1

#aditional cuts 1 Mm below
#cutxy      = cutxy - 1.0
#cutxy11    = int(cutxy/dz1)+1
#cutxy22    = int(cutxy/dz2)+1
#cutxy33    = int(cutxy/dz3)+1

mapa       = raw_input("mapa = ")
mapa       = int(mapa)
saturation = raw_input("saturation = ") 
saturation = convert(saturation)

#print "filter types = 2 , 3 "
#ftype      = raw_input("ftype = ")
#ftype      = int(ftype)

output     = raw_input('output = ')
output     = int(output)

nf1 = data1[var]
nf2 = data2[var]
nf3 = data3[var]

# filter function (3D) ...................................

v1 = nf1
v2 = nf2
v3 = nf3
v4 = filter3D(2,nf1,n1,m1,l1)
v5 = filter3D(2,nf2,n2,m2,l2)
v6 = filter3D(2,nf3,n3,m3,l3)
v7 = filter3D(3,nf1,n1,m1,l1)
v8 = filter3D(3,nf2,n2,m2,l2)
v9 = filter3D(3,nf3,n3,m3,l3)


# conversion to physical units ..........................
if physical == 1:
	vp1 = np.zeros((n1,m1,l1),'float32') 
	vp2 = np.zeros((n2,m2,l2),'float32') 
	vp3 = np.zeros((n3,m3,l3),'float32') 
	vp4 = np.zeros((n1,m1,l1),'float32') 
	vp5 = np.zeros((n2,m2,l2),'float32') 
	vp6 = np.zeros((n3,m3,l3),'float32') 
	vp7 = np.zeros((n1,m1,l1),'float32') 
	vp8 = np.zeros((n2,m2,l2),'float32') 
	vp9 = np.zeros((n3,m3,l3),'float32') 
	vp1[:,:,:] = vel0*v1[:,:,:]
	vp2[:,:,:] = vel0*v2[:,:,:]
	vp3[:,:,:] = vel0*v3[:,:,:]
	vp4[:,:,:] = vel0*v4[:,:,:]
	vp5[:,:,:] = vel0*v5[:,:,:]
	vp6[:,:,:] = vel0*v6[:,:,:]
	vp7[:,:,:] = vel0*v7[:,:,:]
	vp8[:,:,:] = vel0*v8[:,:,:]
	vp9[:,:,:] = vel0*v9[:,:,:]

# grid construcion ......................................

Y1,X1 = np.meshgrid(yy1,xx1)
Y2,X2 = np.meshgrid(yy2,xx2)
Y3,X3 = np.meshgrid(yy3,xx3)

Z1,X11 = np.meshgrid(zz1,xx1)
Z2,X22 = np.meshgrid(zz2,xx2)
Z3,X33 = np.meshgrid(zz3,xx3)

# plot ..................................................

if saturation is True:
	limiter = 2.
else:
	limiter = 1.

estilo='%.2f'  # estio para labels en colorbar

maxval = np.amax([np.amax(vp1),np.amax(vp2),np.amax(vp3),\
		np.amax(vp4),np.amax(vp5),np.amax(vp6),\
		np.amax(vp7),np.amax(vp8),np.amax(vp9)]) 	
minval = np.amin([np.amin(vp1),np.amin(vp2),np.amin(vp3),\
		np.amin(vp4),np.amin(vp5),np.amin(vp6),\
		np.amin(vp7),np.amin(vp8),np.amin(vp9)])
ticks1 = [minval,0,0.8*maxval,maxval]
delta  = maxval - minval
levels = np.arange(minval,maxval,delta/60)

fig = plt.figure(figsize = (10,6))

gs = gridspec.GridSpec(3, 3, width_ratios = [5,5,5],\
	 height_ratios=[2,2,2])

# no filter
eje1 = fig.add_subplot(gs[0])
g1   = plt.contourf(X11,Z1,vp1[:,cutxz1,:],levels, alpha=0.9,\
	cmap = maps[mapa], vmin = minval/limiter, vmax = maxval/limiter)
eje1.set_aspect('equal')
eje1.set_ylabel('z [Mm]')
eje1.set_xlabel('x [Mm]')

eje2 = fig.add_subplot(gs[1])
g2   = plt.contourf(X22,Z2,vp2[:,cutxz2,:],levels, alpha=0.9,\
	cmap = maps[mapa], vmin = minval/limiter, vmax = maxval/limiter)
eje2.set_aspect('equal')
eje2.set_xlabel('x [Mm]')

eje3 = fig.add_subplot(gs[2])
g3   = plt.contourf(X33,Z3,vp3[:,cutxz3,:],levels, alpha=0.9,\
	cmap = maps[mapa], vmin = minval/limiter, vmax = maxval/limiter)
eje3.set_aspect('equal')
eje3.set_xlabel('x [Mm]')

# filter 2
eje4 = fig.add_subplot(gs[3])
g4   = plt.contourf(X1,Z1,vp4[:,cutxz1,:],levels, alpha=0.9,\
	cmap = maps[mapa], vmin = minval/limiter, vmax = maxval/limiter)
eje4.set_aspect('equal')
eje4.set_ylabel('z [Mm]')
eje4.set_xlabel('x [Mm]')

eje5 = fig.add_subplot(gs[4])
g5   = plt.contourf(X2,Z2,vp5[:,cutxz2,:],levels, alpha=0.9,\
	cmap = maps[mapa], vmin = minval/limiter, vmax = maxval/limiter)
eje5.set_aspect('equal')
eje5.set_xlabel('x [Mm]')

eje6 = fig.add_subplot(gs[5])
g6   = plt.contourf(X3,Z3,vp6[:,cutxz3,:],levels, alpha=0.9,\
	cmap = maps[mapa], vmin = minval/limiter, vmax = maxval/limiter)
eje6.set_aspect('equal')
eje6.set_xlabel('x [Mm]')

# filter 3
eje7 = fig.add_subplot(gs[6])
g7   = plt.contourf(X1,Z1,vp7[:,cutxz1,:],levels, alpha=0.9,\
	cmap = maps[mapa], vmin = minval/limiter, vmax = maxval/limiter)
eje7.set_aspect('equal')
eje7.set_ylabel('z [Mm]')
eje7.set_xlabel('x [Mm]')

eje8 = fig.add_subplot(gs[7])
g8   = plt.contourf(X2,Z2,vp8[:,cutxz2,:],levels, alpha=0.9,\
	cmap = maps[mapa], vmin = minval/limiter, vmax = maxval/limiter)
eje8.set_aspect('equal')
eje8.set_xlabel('x [Mm]')

eje9 = fig.add_subplot(gs[8])
g9   = plt.contourf(X3,Z3,vp9[:,cutxz3,:],levels, alpha=0.9,\
	cmap = maps[mapa], vmin = minval/limiter, vmax = maxval/limiter)
eje9.set_aspect('equal')
eje9.set_xlabel('x [Mm]')

fig.tight_layout()
plt.subplots_adjust(bottom = 0.2)

cax = plt.axes([0.15,0.1,0.63,0.02])

clb = plt.colorbar(ticks = ticks1,cax = cax, orientation = \
'horizontal', format = estilo)	
clb.set_label(r'$u_z$ [Km/s]',labelpad = -30, x = 1.1,fontsize = 15)
if output == 0:
	plt.show()
elif output == 1:
	filename ='nf-f-f11.png' 
	plt.savefig(filename)
	print 'saved to file : ', filename
