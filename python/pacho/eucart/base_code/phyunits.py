from physical import *
from psettings import *

Lx00 = l0*Lx00
Ly00 = l0*Ly00
Lz00 = l0*Lz00

dx = Lx00/(n-1)
if not m == 1:
	dy = Ly00/(m-1)
dz = Lz00/(l-1)
xx = np.arange(0,Lx00+dx,dx)
if not m == 1:
	yy = np.arange(0,Ly00+dy,dy)
zz = np.arange(0,Lz00+dz,dz)

print 'dimensionalizando arreglos .... '

#pux = np.zeros(np.shape(u),'float32')
#puy = np.zeros(np.shape(v),'float32')
#puz = np.zeros(np.shape(w),'float32')
#pbx = np.zeros(np.shape(bx),'float32')
#pby = np.zeros(np.shape(by),'float32')
#pbz = np.zeros(np.shape(bz),'float32')
pux  = np.multiply(vel0,u)
puy  = np.multiply(vel0,v)
puz  = np.multiply(vel0,w)
pbx  = np.multiply(bb0*1e3,bx) # Tesla units for B
pby  = np.multiply(bb0*1e3,by)
pbz  = np.multiply(bb0*1e3,bz)

u = pux
v = puy
w = puz
bx = pbx
by = pby
bz = pbz

phys = 1.0
