import numpy as np
import scipy.interpolate
import scipy.ndimage

########################################################################################

def settings(folder):
	"""
	reads and assings variable names
	to parameters of current "folder"
	simulation
	--------------------------------
	returns a list of the relevant
	parameters for post-processing
	"""
	ee = True
	mylist = True	
	diagnostics = False

	if ee is True:
		ee_r=folder+'ee.txt'
		file_ee=open(ee_r, "r")
		data_ee=[line.strip() for line in file_ee]
        	size_ee=len(data_ee)
        	string="it="
		for i in range(size_ee):
                	if string in data_ee[i].split():
                        	it = data_ee[i].split()[1]
		itt=int(it)

	if mylist is True:
		mylist=folder+"mylist.nml"
        	file_my=open(mylist, "r")
        	data_my=[line.strip() for line in file_my]
        	size_my=len(data_my)
        	nt=int(data_my[1].split()[2][0:-1])
        	n=int(data_my[2].split()[2][0:-1])
        	m=int(data_my[3].split()[2][0:-1])
        	l=int(data_my[4].split()[2][0:-1])
        	Lx00=float(data_my[6].split()[2][0:-1])
        	Ly00=float(data_my[7].split()[2][0:-1])
        	Lz00=float(data_my[8].split()[2][0:-1])
        	dt00=float(data_my[9].split()[2][0:-1])
        	nslice=int(data_my[10].split()[2][0:-1])
        	noutp=int(data_my[11].split()[2][0:-1])
        	nplot=int(data_my[12].split()[2][0:-1])
        	nstore=int(data_my[13].split()[2][0:-1])
        	nxaver=int(data_my[14].split()[2])

	if diagnostics is True:
	        print "(it,n,m,l) = ", itt,n,m,l
	        print "(Lx,Ly,Lz,dt) = ", Lx00,Ly00,Lz00,dt00


	return itt, nt, n, m, l, Lx00, Ly00, Lz00, dt00, nslice, noutp, nplot, nstore, nxaver


########################################################################################

def read_xaver(folder):
	"""
	Read file "xaverages.dat" in 
	the working folder "folder"
	---------------------------
	returns the whole data set
	in a list of matrices
	"""
	xaver=folder+"xaverages.dat"
	if folder=="":
                print 'currently on working folder'
        else:
                print 'working folder: '+folder

	itt,nt,n,m,l,Lx00,Ly00,Lz00,dt00,nslice,noutp,nplot,nstore,nxaver = \
	settings(folder)

	ntxaver=int(itt/nxaver)
	if not xaver is None:
        	thew=np.empty((ntxaver,m,l),'float64')
        	u=np.empty((ntxaver,m,l),'float64')
        	v=np.empty((ntxaver,m,l),'float64')
        	w=np.empty((ntxaver,m,l),'float64')
        	ox=np.empty((ntxaver,m,l),'float64')
        	oy=np.empty((ntxaver,m,l),'float64')
        	oz=np.empty((ntxaver,m,l),'float64')
        	u2=np.empty((ntxaver,m,l),'float64')
        	v2=np.empty((ntxaver,m,l),'float64')
        	w2=np.empty((ntxaver,m,l),'float64')
        	ox2=np.empty((ntxaver,m,l),'float64')
        	oy2=np.empty((ntxaver,m,l),'float64')
        	oz2=np.empty((ntxaver,m,l),'float64')
        	rwv=np.empty((ntxaver,m,l),'float64')
        	rwu=np.empty((ntxaver,m,l),'float64')
        	rvu=np.empty((ntxaver,m,l),'float64')
        	bx=np.empty((ntxaver,m,l),'float64')
        	by=np.empty((ntxaver,m,l),'float64')
        	bz=np.empty((ntxaver,m,l),'float64')
        	bx2=np.empty((ntxaver,m,l),'float64')
        	by2=np.empty((ntxaver,m,l),'float64')
        	bz2=np.empty((ntxaver,m,l),'float64')
        	bzby=np.empty((ntxaver,m,l),'float64')
        	bzbx=np.empty((ntxaver,m,l),'float64')
        	bxby=np.empty((ntxaver,m,l),'float64')
        	p=np.empty((ntxaver,m,l),'float64')
        	the=np.empty((ntxaver,m,l),'float64')

		data2 = thew,u,v,w,ox,oy,oz,u2,v2,w2,ox2,oy2,oz2,rwv,rwu,rvu,bx,by,bz,bx2,by2,bz2,bzby,bzbx,bxby,p,the
		n_var2=len(data2)
		print("Reading data from file xaverages.dat")
        	data_file2=open(xaver, 'rb')
		for i in range(0,ntxaver):
                	for j in range(0,n_var2):
                        	np.fromfile(data_file2,dtype='uint32',count=1)
                        	data2[j][i,:,:]=np.reshape(np.fromfile(data_file2, \
					dtype='float64',count=m*l),(m,l),order='F')
                        	np.fromfile(data_file2,dtype='uint32',count=1)
                        	np.fromfile(data_file2,dtype='uint32',count=1)
        	data_file2.close()
		return data2

def readf11(folder,fort11,ntime,last):
	itt, nt, n, m, l, Lx00, Ly00, Lz00, dt00, nslice, noutp, nplot, nstore, nxaver = settings(folder)
	print "parameters: itt, nt, n, m, l, Lx00, Ly00, Lz00, dt00, nslice, noutp, nplot, nstore, nxaver " 
	print "values: ", itt, nt, n, m, l, Lx00, Ly00, \
		Lz00, dt00, nslice, noutp, nplot, nstore, nxaver
	if last == 0:
		u  = np.empty((ntime,n,m,l),'float32')
		v  = np.empty((ntime,n,m,l),'float32')
		w  = np.empty((ntime,n,m,l),'float32')
		t  = np.empty((ntime,n,m,l),'float32')
		p  = np.empty((ntime,n,m,l),'float32')
		bx = np.empty((ntime,n,m,l),'float32')
		by = np.empty((ntime,n,m,l),'float32')
		bz = np.empty((ntime,n,m,l),'float32')

		data= u, v, w, t, p, bx, by, bz
		n_var=len(data)
		print("Reading data from file fort.11")
		file=folder+fort11
		data_file=open(file, "rb")
		for i in range(0,ntime):
			for j in range(0,n_var):
				np.fromfile(data_file,dtype='uint32',count=1)
				data[j][i,:,:,:]=np.reshape(np.fromfile(data_file,\
					dtype='float32',count=n*m*l),(n,m,l),order= 'F')
				np.fromfile(data_file,dtype='uint32',count=1)
		data_file.close()
		print('number of outputs = ',ntime)

	elif last == 1:
		u  = np.zeros((n,m,l),'float32')
		v  = np.zeros((n,m,l),'float32')
		w  = np.zeros((n,m,l),'float32')
		t  = np.zeros((n,m,l),'float32')
		p  = np.zeros((n,m,l),'float32')
		bx = np.zeros((n,m,l),'float32')
		by = np.zeros((n,m,l),'float32')
		bz = np.zeros((n,m,l),'float32')
		
		data = u, v, w, t, p, bx, by, bz
		n_var = len(data)
		print ("Reading data at nt = "+str(ntime)+' from file fort.11') 
		file=folder+fort11
		data_file=open(file, "rb")
		for i in range(0,ntime):
			for j in range(0,n_var):
				np.fromfile(data_file,dtype='uint32',count=1)
				if i == ntime-1:
					data[j][:,:,:]=np.reshape(np.fromfile(data_file,\
						dtype='float32',count=n*m*l),(n,m,l),order= 'F')
				else:
					np.fromfile(data_file,dtype='float32',count=n*m*l)
				np.fromfile(data_file,dtype='uint32',count=1)
		data_file.close()
		print('number of outputs = 1')
		

	return data

########################################################################################

def readf9(folder,fort9):
	"""
	Reads the "fort9" file in the directory "folder"
	-----------------------------------------------
	returns the whole set as a list of matrices
	+ time value ...
	"""
	from fortranfile import *
	itt, nt, n, m, l, Lx00, Ly00, Lz00, dt00, nslice, noutp, nplot, nstore, nxaver = settings(folder)
	print "parameters: itt, nt, n, m, l, Lx00, Ly00, Lz00, dt00, nslice, noutp, nplot, nstore, nxaver " 
	print "values: ", itt, nt, n, m, l, Lx00, Ly00, \
		Lz00, dt00, nslice, noutp, nplot, nstore, nxaver
	MHD=True
	if fort9 == 'fort.9.n':
		print "reading new file: ", folder+fort9
		print "pulses = 1"	
		ntf9 = 1
	if fort9 == 'fort.9':
		ntf9=int(itt/nstore)
		print "reading file: ", folder+fort9
		print "pulses = ", ntf9
	
	data9=FortranFile(fort9)
	u=np.zeros((n,m,l), 'float32')	
	v=np.zeros((n,m,l), 'float32')	
	w=np.zeros((n,m,l), 'float32')	
	ox=np.zeros((n,m,l), 'float32')	
	oy=np.zeros((n,m,l), 'float32')	
	oz=np.zeros((n,m,l), 'float32')		
	ox2=np.zeros((n,m,l), 'float32')	
	oy2=np.zeros((n,m,l), 'float32')	
	oz2=np.zeros((n,m,l), 'float32')	
	th=np.zeros((n,m,l), 'float32')	
	p=np.zeros((n,m,l), 'float32')	
	fx=np.zeros((n,m,l), 'float32')	
	fy=np.zeros((n,m,l), 'float32')	
	fz=np.zeros((n,m,l), 'float32')	
	ft=np.zeros((n,m,l), 'float32')	
	pm=np.zeros((n,m,l), 'float32')	
	bx=np.zeros((n,m,l), 'float32')	
	by=np.zeros((n,m,l), 'float32')	
	bz=np.zeros((n,m,l), 'float32')	
	fbx=np.zeros((n,m,l), 'float32')	
	fby=np.zeros((n,m,l), 'float32')	
	fbz=np.zeros((n,m,l), 'float32')	
	tke=np.zeros((n,m,l), 'float32')	
	chm=np.zeros((n,m,l), 'float32')	
	chm=np.zeros((n,m,l), 'float32')	
	fchm=np.zeros((n,m,l), 'float32')	
	fchm=np.zeros((n,m,l), 'float32')	
	
	for t in range(ntf9):
#		print t
		u[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		v[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		w[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		ox[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		oy[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		oz[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		ox2[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		oy2[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		oz2[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		th[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		p[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		fx[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		fy[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		fz[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		ft[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		time = data9.readReals('f')
		time = data9.readReals('f')
		time = data9.readReals('f')
		pm[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		bx[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		by[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		bz[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		fbx[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		fby[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		fbz[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		time = data9.readReals('f')
		time = data9.readReals('f')
		time = data9.readReals('f')	
		time = data9.readReals('f')
		time = data9.readReals('f')
		time = data9.readReals('f')
		time = data9.readReals('f')
		time = data9.readReals('f')
		time = data9.readReals('f')
		time = data9.readReals('f')
		tke[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		time = data9.readReals('f')	
		chm[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		chm[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		fchm[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
		fchm[:,:,:] = np.reshape(data9.readReals('f'), (n, m, l), order = 'F')
	
	return u,v,w,ox,oy,oz,ox2,oy2,oz2,th,p,fx,fy,fz,ft,pm,bx,by,bz,fbx,fby,fbz, \
		tke,chm,fchm,time

########################################################################################

def resizef9(inputdata,nn,mm,ll):
	"""
	Resizes the data of matrices from readf9 
	to desired sizes (nn,mm,ll)
	"""
	
	u,v,w,ox,oy,oz,ox2,oy2,oz2,th,p,fx,fy,fz,ft,pm,bx,by,bz,fbx,fby,fbz, \
		tke,chm,fchm,time = inputdata
	
	print "transforming matrices ... "
	ru = congrid(u,(nn,mm,ll))
	rv = congrid(v,(nn,mm,ll))
	rw = congrid(w,(nn,mm,ll))
	rox = congrid(ox,(nn,mm,ll))
	roy = congrid(oy,(nn,mm,ll))
	roz = congrid(oz,(nn,mm,ll))
	rox2 = congrid(ox2,(nn,mm,ll))
	roy2 = congrid(oy2,(nn,mm,ll))
	roz2 = congrid(oz2,(nn,mm,ll))
	rth = congrid(th,(nn,mm,ll))
	rp = congrid(p,(nn,mm,ll))
	rfx = congrid(fx,(nn,mm,ll))
	rfy = congrid(fy,(nn,mm,ll))
	rfz = congrid(fz,(nn,mm,ll))
	rft = congrid(ft,(nn,mm,ll))
	rpm = congrid(pm,(nn,mm,ll))
	rbx = congrid(bx,(nn,mm,ll))
	rby = congrid(by,(nn,mm,ll))
	rbz = congrid(bz,(nn,mm,ll))
	rfbx = congrid(fbx,(nn,mm,ll))
	rfby = congrid(fby,(nn,mm,ll))
	rfbz = congrid(fbz,(nn,mm,ll))
	rtke = congrid(tke,(nn,mm,ll))
	rchm = congrid(chm,(nn,mm,ll))
	rfchm = congrid(fchm,(nn,mm,ll))
	print "resized variables ready"
	
	outputdata = ru,rv,rw,rox,roy,roz,rox2,roy2,roz2,rth,rp,rfx,rfy,rfz,rft,rpm,rbx,rby,rbz,rfbx,rfby,rfbz,rtke,rchm,rfchm,time
	
	return outputdata	
########################################################################################

def writef9(inputdata):
	"""
	Creates a new binary file fort.9.n
	using as "inputdata" a list of matrices
	with the same order given by  readf9.
	------------------------------------------------------------
	returns : Nothing 
	WARNING = the inputdata needs to be in the same readf9 order
	"""
	from fortranfile import *	
#	itt, nt, n, m, l, Lx00, Ly00, Lz00, dt00, nslice, noutp, nplot, nstore, nxaver \
#	 = settings(folder)
	u,v,w,ox,oy,oz,ox2,oy2,oz2,th,p,fx,fy,fz,ft,pm,bx,by,bz,fbx,fby,fbz, \
                tke,chm,fchm,time = inputdata
#	print "i target folder : ", folder
#	print "parameters: itt, nt, n, m, l, Lx00, Ly00, Lz00, dt00, nslice, noutp, nplot, nstore, nxaver " 
#	print "values: ", itt, nt, n, m, l, Lx00, Ly00, \
#		Lz00, dt00, nslice, noutp, nplot, nstore, nxaver
	
	newfile="fort.9.n"	
	print "writting new fort.9.n file ... "
	with FortranFile(newfile,mode='w') as ndata:	
	#with open(newf, 'wb') as newfile:
		# open new class for fort.9 file
	#	ndata=FortranFile(newfile)
		# reconstruct original data for binary file
		ru=np.ravel(u, order = 'F')
		rv=np.ravel(v, order = 'F')
		rw=np.ravel(w, order = 'F')
		rox=np.ravel(ox, order = 'F')
		roy=np.ravel(oy, order = 'F')
		roz=np.ravel(oz, order = 'F')
		rox2=np.ravel(ox2, order = 'F')
		roy2=np.ravel(oy2, order = 'F')
		roz2=np.ravel(oz2, order = 'F')
		rth=np.ravel(th, order = 'F')
		rp=np.ravel(p, order = 'F')
		rfx=np.ravel(fx, order = 'F')
		rfy=np.ravel(fy, order = 'F')
		rfz=np.ravel(fz, order = 'F')
		rft=np.ravel(ft, order = 'F')

		rpm=np.ravel(pm, order = 'F')
		rbx=np.ravel(bx, order = 'F')
		rby=np.ravel(by, order = 'F')
		rbz=np.ravel(bz, order = 'F')
		rfbx=np.ravel(fbx, order = 'F')
		rfby=np.ravel(fby, order = 'F')
		rfbz=np.ravel(fbz, order = 'F')

		rtke=np.ravel(tke, order = 'F')

		rchm=np.ravel(chm, order = 'F')
		rchm=np.ravel(chm, order = 'F')
		rfchm=np.ravel(fchm, order = 'F')
		rfchm=np.ravel(fchm, order = 'F')
		# write to new file in the exact same order of old fort.9

		ndata.writeReals(ru,'f')
		ndata.writeReals(rv,'f')
		ndata.writeReals(rw,'f')
		ndata.writeReals(rox,'f')
		ndata.writeReals(roy,'f')
		ndata.writeReals(roz,'f')
		ndata.writeReals(rox2,'f')
		ndata.writeReals(roy2,'f')
		ndata.writeReals(roz2,'f')
		ndata.writeReals(rth,'f')
		ndata.writeReals(rp,'f')
		ndata.writeReals(rfx,'f')
		ndata.writeReals(rfy,'f')
		ndata.writeReals(rfz,'f')
		ndata.writeReals(rft,'f')

		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')

		ndata.writeReals(rpm,'f')
		ndata.writeReals(rbx,'f')
		ndata.writeReals(rby,'f')
		ndata.writeReals(rbz,'f')
		ndata.writeReals(rfbx,'f')
		ndata.writeReals(rfby,'f')
		ndata.writeReals(rfbz,'f')

		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')
		ndata.writeReals(time,'f')

		ndata.writeReals(rtke,'f')

		ndata.writeReals(time,'f')

		ndata.writeReals(rchm,'f')
		ndata.writeReals(rchm,'f')
		ndata.writeReals(rfchm,'f')
		ndata.writeReals(rfchm,'f')

	return None


########################################################################################

def congrid(a, newdims, method='linear', centre=False, minusone=True):
#   6     Arbitrary resampling of source array to new dimension sizes.
#   7     Currently only supports maintaining the same number of dimensions.
#   8     To use 1-D arrays, first promote them to shape (x,1).
#   9     
#  10     Uses the same parameters and creates the same co-ordinate lookup points
#  11     as IDL''s congrid routine, which apparently originally came from a VAX/VMS
#  12     routine of the same name.
#  13 
#  14     method:
#  15     neighbour - closest value from original data
#  16     nearest and linear - uses n x 1-D interpolations using
#  17                          scipy.interpolate.interp1d
#  18     (see Numerical Recipes for validity of use of n 1-D interpolations)
#  19     spline - uses ndimage.map_coordinates
#  20 
#  21     centre:
#  22     True - interpolation points are at the centres of the bins
#  23     False - points are at the front edge of the bin
#  24 
#  25     minusone:
#  26     For example- inarray.shape = (i,j) & new dimensions = (x,y)
#  27     False - inarray is resampled by factors of (i/x) * (j/y)
#  28     True - inarray is resampled by(i-1)/(x-1) * (j-1)/(y-1)
#  29     This prevents extrapolation one element beyond bounds of input array.
	import numpy as n
	if not a.dtype in [n.float64, n.float32]:
		a=n.cast[float](a)
		
	m1=n.cast[int](minusone)
	ofs=n.cast[int](centre) * 0.5
	old=n.array(a.shape)
	ndims=len(a.shape)
	if len(newdims) != ndims:
		print "[congrid] dimensions error. " \
			"This routine currently only support" \
			"rebinning to the same number of dimensions."
		return None
	newdims=n.asarray(newdims, dtype=float)
	dimlist=[]

	if method == 'neighbour':
		for i in range(dims):
			base=n.indices(newdims)[i]
			dimlist.append((old[i]-m1) / (newdims[i]-m1) \
				* (base+ofs)-ofs)
		cd = n.array(dimlist).round().astype(int)
		newa=a[list(cd)]
		return newa

	elif method in ['nearest','linear']:
		# calculate new dims
		for i in range(ndims):
			base = n.arange(newdims[i])
			dimlist.append((old[i]-m1)/(newdims[i]-m1) \
				* (base+ofs)-ofs)
		# specify old dims
		olddims=[n.arange(i,dtype=n.float) for i in list(a.shape)]
		#first interpolation - for ndims = any
		mint=scipy.interpolate.interp1d(olddims[-1],a,kind=method)
		newa=mint(dimlist[-1])

		trorder = [ndims-1] + range(ndims-1)
		for i in range(ndims-2,-1,-1):
			newa = newa.transpose(trorder)
			mint = scipy.interpolate.interp1d(olddims[i], newa, kind=method)
			newa = mint(dimlist[i])

		if ndims > 1:
			#need one more transpose to return to original dimensions
			newa = newa.transpose(trorder)

		return newa

	elif method in ['spline']:
		oslices = [slice(0,j) for j in old]
		oldcoords = n.ogrid[oslices]
		nslices = [slice(0,j) for j in list(newdims)]
		newcoords = n.mgrid[nslices]

		newcoords_dims = range(n.rank(newcoords))
		#make first index last
		newcoords_dims.append(newcoords_dims.pop(0))
		newcoords_tr = newcoords.transpose(newcoords_dims)
		# makes a view that affects newcoords

		newcoords_tr += ofs
		
		deltas = (n.asarray(old)-m1)/(newdims-m1)
		newcoords_tr *= deltas
		
		newcoords_tr -= ofs
		
		newa=scipy.ndimage.map_coordinates(a, newcoords)
		return newa
	else:
		print "Congrid error: Unrecognized interpolation type.\n", \
			"Currently only \'neighbour\', \'nearest\',\'linear\',", \
			"and \'spline\' are supported."
		return none

########################################################################################

def filter3D(ftype,field,dim1,dim2,dim3):
	'''
	filters a field the structure
		field = field[dim1.dim2,dim3]

	'''
	ffield = np.zeros((dim1,dim2,dim3),"float32")
	if ftype == 2:
		print "2 pt filtering ... "
		for kk in range(0,dim3):
			if kk == dim3-1:
				ffield[:,:,kk] = field[:,:,kk]
			else:
				ffield[:,:,kk] = 0.5*(field[:,:,kk]+field[:,:,kk+1])
	elif ftype == 3:
		print "3 pt filtering ... "
		ffield[:,:,0]      = 0.5*(field[:,:,0]+field[:,:,1])
		ffield[:,:,dim3-1] = 0.5*(field[:,:,dim3-1]+field[:,:,dim3-2])	 
		for kk in range(1,dim3-1):
			ffield[:,:,kk] = 0.25*(field[:,:,kk-1]+field[:,:,kk]+field[:,:,kk+1])
	return ffield
 
