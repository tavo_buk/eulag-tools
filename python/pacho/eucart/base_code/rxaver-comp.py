from modules import *
import numpy as np
import matplotlib.pyplot as plt
from physical import *

file="R500.cf_mhd_lr.2/"
file_lr="R500.cf_mhd_hr.remesh.2/"
file_hr="R500.cf_mhd_hr/"

dat=read_xaver(file)
settings_1=settings(file)
dat_lr=read_xaver(file_lr)
settings_lr=settings(file_lr)
dat_hr=read_xaver(file_hr)
settings_hr=settings(file_hr)
