from psettings import *

filtering=False

#ntime=int(itt/nplot)
ntime=int(itt/nplot)
nskip=int(raw_input("nskip = "))
print 'temporal slices to consider : '+str(nskip)+' to '+str(ntime)

if not fort11 is None:
	u=np.empty((ntime-nskip,n,m,l),'float32')
	v=np.empty((ntime-nskip,n,m,l),'float32')
	w=np.empty((ntime-nskip,n,m,l),'float32')
	t=np.empty((ntime-nskip,n,m,l),'float32')
	p=np.empty((ntime-nskip,n,m,l),'float32')
	bx=np.empty((ntime-nskip,n,m,l),'float32')
	by=np.empty((ntime-nskip,n,m,l),'float32')
	bz=np.empty((ntime-nskip,n,m,l),'float32')

	data= u, v, w, t, p, bx, by, bz
	n_var=len(data)
	print("Skipping data from first records of fort.11 ...")
	data_file=open(fort11, "rb")

	for i in range(0,nskip):
		for j in range(0,n_var):
			np.fromfile(data_file,dtype='uint32',count=1)
			np.fromfile(data_file,dtype='float32',count=n*m*l)
			np.fromfile(data_file,dtype='uint32',count=1)
	print('Reading data from file fort.11 ...')
	for i in range(0,ntime-nskip):
		for j in range(0,n_var):
			np.fromfile(data_file,dtype='uint32',count=1)
			data[j][i,:,:,:]=np.reshape(np.fromfile(data_file,dtype='float32',count=n*m*l),(n,m,l),order= 'F')
			np.fromfile(data_file,dtype='uint32',count=1)
	data_file.close()
	print('number of outputs = ',ntime-nskip)


if filtering is True:
        print "filtering ..."
	uf=np.empty((ntime,n,m,l),'float32')
	vf=np.empty((ntime,n,m,l),'float32')
	wf=np.empty((ntime,n,m,l),'float32')
	tf=np.empty((ntime,n,m,l),'float32')
	pf=np.empty((ntime,n,m,l),'float32')
	bxf=np.empty((ntime,n,m,l),'float32')
	byf=np.empty((ntime,n,m,l),'float32')
	bzf=np.empty((ntime,n,m,l),'float32')
	data_f= uf, vf, wf, tf, pf, bxf, byf, bzf
	for ii in range(0,n_var):
		for kk in range(1,l-1):
			data_f[ii][:,:,:,kk]=0.5*(data[ii][:,:,:,kk]+data[ii][:,:,:,kk+1])
	for ii in range(0,n_var):
		for kk in range(1,l-1):
			data[ii][:,:,:,kk]=data_f[ii][:,:,:,kk]

