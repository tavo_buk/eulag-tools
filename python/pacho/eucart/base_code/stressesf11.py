#  Consider:					#
#  		A = <A> + a  <--  Reynolds d.	#
#						#
#  This code calculates the Reynolds and	#
#  Maxwell stresses: 				#
#		<u(i)u(j)>			#
#		<b(i)b(j)>			#
#						#
#  Using the formula:				#
#						#
#	<a*b> = <A*B> - <A>*<B> 		#
#						#
#################################################
import NEMPImod as Nm
import numpy as np
import modules as mod

def stresses(u,v,w,bx,by,bz,mode):
	# HD mode
	if mode==1:
		# diagonal elements
		Tuu=Nm.TstressLast(u,u)
		Tvv=Nm.TstressLast(v,v)
		Tww=Nm.TstressLast(w,w)
		Tbxbx=Nm.TstressLast(bx,bx)
		Tbyby=Nm.TstressLast(by,by)
		Tbzbz=Nm.TstressLast(bz,bz)
		#off-diagonal elements
		Tuv=Nm.TstressLast(u,v)
		Tvw=Nm.TstressLast(v,w)
		Tuw=Nm.TstressLast(u,w)
		Tbxby=Nm.TstressLast(bx,by)
		Tbybz=Nm.TstressLast(by,bz)
		Tbxbz=Nm.TstressLast(bx,bz)
	# MHD mode
	if mode==2:
		# diagonal elements
		Tuu=Nm.Tstress(u,u)
		Tvv=Nm.Tstress(v,v)
		Tww=Nm.Tstress(w,w)
		Tbxbx=Nm.Tstress(bx,bx)
		Tbyby=Nm.Tstress(by,by)
		Tbzbz=Nm.Tstress(bz,bz)
		#off-diagonal elements
		Tuv=Nm.Tstress(u,v)
		Tvw=Nm.Tstress(v,w)
		Tuw=Nm.Tstress(u,w)
		Tbxby=Nm.Tstress(bx,by)
		Tbybz=Nm.Tstress(by,bz)
		Tbxbz=Nm.Tstress(bx,bz)
		
	
	return Tuu,Tvv,Tww,Tuv,Tvw,Tuw,\
		Tbxbx,Tbyby,Tbzbz,\
		Tbxby,Tbybz,Tbxbz

#settings of HD run
HDfolder="../R500.cf_mhd_lr.2/"
itt,nt,n,m,l,Lx00,Ly00,Lz00,dt00,nslice,noutp,nplot, \
nstore,nxaver = mod.settings(HDfolder)
ntime=int(itt/nplot)

# read HD run variables.
u,v,w,t,p,bx,by,bz = Nm.readf11Last(HDfolder,ntime)

# compute the Reynolds and Maxwell HD turbulent stresses.
print "calculating HD turbulent stresses"
Tuu0,Tvv0,Tww0,Tuv0,Tvw0,Tuw0,\
Tbxbx0,Tbyby0,Tbzbz0,\
Tbxby0,Tbybz0,Tbxbz0 = stresses(u,v,w,bx,by,bz,1)

#settings of MHD run
MHDfolder=""
itt1,nt1,n1,m1,l1,Lx001,Ly001,Lz001,dt001,nslice1,\
noutp1,nplot1,nstore1,nxaver1 = mod.settings(MHDfolder)
ntime1=int(itt1/nplot1)-300

# read MHD run variables.
u1,v1,w1,t1,p1,bx1,by1,bz1 = Nm.readf11(MHDfolder,ntime1)

# compute the Reynolds and Maxwell MHD turbulent stresses.
print "calculating MHD turbulent stresses"
Tuu1,Tvv1,Tww1,Tuv1,Tvw1,Tuw1,\
Tbxbx1,Tbyby1,Tbzbz1,\
Tbxby1,Tbybz1,Tbxbz1 = stresses(u1,v1,w1,bx1,by1,bz1,2)

print "Averaging magnetic field"
# take magnetic field horizontally 
# averaged

MBxBx = Nm.XYaver(np.multiply(bx1,bx1))
MByBy = Nm.XYaver(np.multiply(by1,by1))
MBzBz = Nm.XYaver(np.multiply(bz1,bz1))

MB = MBxBx + MByBy + MBzBz


