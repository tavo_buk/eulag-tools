import numpy as np

ee = True
mylist = True
diagnostics = True

fort11="fort.11"
xaver="xaverages.dat"

if ee is True:
	ee="ee.txt"
	file_ee=open(ee, "r")
	data_ee=[line.strip() for line in file_ee]
	size_ee=len(data_ee)
	string="it="
	for i in range(size_ee):
		if string in data_ee[i].split():
			it = data_ee[i].split()[1]
#	n=data_ee[4].split()[2]
#	m=data_ee[4].split()[3]
#	l=data_ee[4].split()[4]
        itt=int(it)

if mylist is True:
	mylist="mylist.nml"
	file_my=open(mylist, "r")
	data_my=[line.strip() for line in file_my]
	size_my=len(data_my)
	nt=int(data_my[1].split()[2][0:-1])
	n=int(data_my[2].split()[2][0:-1])
	m=int(data_my[3].split()[2][0:-1])
	l=int(data_my[4].split()[2][0:-1])
	Lx00=float(data_my[6].split()[2][0:-1])
	Ly00=float(data_my[7].split()[2][0:-1])
	Lz00=float(data_my[8].split()[2][0:-1])
	dt00=float(data_my[9].split()[2][0:-1])
	nslice=int(data_my[10].split()[2][0:-1])
	noutp=int(data_my[11].split()[2][0:-1])
	nplot=int(data_my[12].split()[2][0:-1])
	nstore=int(data_my[13].split()[2][0:-1])
	nxaver=int(data_my[14].split()[2])

if diagnostics is True:
	print "(it,n,m,l) = ", itt,n,m,l
        print "(Lx,Ly,Lz,dt) = ", Lx00,Ly00,Lz00,dt00
