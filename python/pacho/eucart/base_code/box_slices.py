# when uing ipython shell run with:
# % run -i ...

#from prbox import * 
from psettings import *
import matplotlib.pyplot as plt
import numpy as np
from pylab import *
import sys


if phys is not None:
	print 'Physical units mode ON'
if phys is None:
	dx=Lx00/(n-1)
	dz=Lz00/(l-1)
	if not m == 1:
		dy=Ly00/(m-1)	        
		yy=np.arange(0,Ly00+dy,dy)

	xx=np.arange(0,Lx00+dx,dx)
	zz=np.arange(0,Lz00+dz,dz)

try:
        print "slices for field ", strfield
        print "dimensions of array: ", np.shape(field)
        print '(time,extension,cut,mapa,saturation)= ', time, extension, cut, mapa,saturation
except NameError:
        sys.exit('Error: please define (time,extension,cut,mapa,saturation)')

if mapa==1:
        bmap=cm.afmhot
if mapa==2:
        bmap=cm.rainbow
if mapa==3:
        bmap=cm.CMRmap
if mapa==4:
        bmap=cm.grays

if extension=="xy":
        Y,X=np.meshgrid(yy,xx)
        fieldc=field[:,:,:,cut]
if extension=="xz":
        Z,X=np.meshgrid(zz,xx)
        fieldc=field[:,:,cut,:]
if extension=="yz":
        Z,Y=np.meshgrid(zz,yy)
        fieldc=field[:,cut,:,:]

#global scaling for colortables ...
estilo='%.2f'
bmap.set_over(bmap(0.999))
bmap.set_under(bmap(0))
maxval=np.amax(fieldc)
minval=np.amin(fieldc)
ticks_cb=[minval,0,0.9*maxval,maxval]
delta=maxval-minval
levels=np.arange(minval,maxval,delta/60)

def slice(time,campo,extension,filtro):
        campoin=campo[time,:,:]
        if filtro is True:
                print 'filtering slice ...', time
                if extension=='xz':
                        campoin_f=np.empty((n,l),'float32')
                        for kk in range(0,l):
                                if kk==l-1:
                                        campoin_f[:,kk]=campoin[:,kk]
                                else:
                                        campoin_f[:,kk]=0.5*(campoin[:,kk]+campoin[:,kk+1])
                if extension=='yz':
                        campoin_f=np.empty((m,l),'float32')
                        for kk in range(0,l):
                                if kk==l-1:
                                        campoin_f[:,kk]=campoin[:,kk]
                                else:
                                        campoin_f[:,kk]=0.5*(campoin[:,kk]+campoin[:,kk+1])
                if extension=='xy':
                        campoin_f=np.empty((n,m),'float32')
                        campoin_f[:,:]=campoin[:,:]
                campoin[:,:]=campoin_f[:,:]
        
        fig=plt.figure()
        ax=fig.add_subplot(111)

        if saturation is True:
                num=2.0
        else:
                num=1.0
        
        if extension=="xy":
                graf=plt.contourf(X,Y,campoin,levels,alpha=0.9,cmap=bmap,vmin=minval/num,vmax=maxval/num)
                cb2 = fig.colorbar(graf,ticks=ticks_cb,orientation='vertical',format=estilo)
	if extension=='xz':
                graf=plt.contourf(X,Z,campoin,levels,alpha=0.9,cmap=bmap,vmin=minval/num,vmax=maxval/num)
                cb2=fig.colorbar(graf,ticks=ticks_cb,orientation='horizontal',format=estilo)
        if extension=='yz':
                graf=plt.contourf(Y,Z,campoin,levels,alpha=0.9,cmap=bmap,vmin=minval/num,vmax=maxval/num)
                cb2=fig.colorbar(graf,ticks=ticks_cb,orientation='horizontal',format=estilo)
        
	cb2.set_label(cbtit, fontsize = fsize)
        ax.set_aspect('equal')
	plt.xlabel(strxlabel, fontsize = fsize)
       	plt.ylabel(strylabel, fontsize = fsize)
        if (time+nskip) < 10:
        	timestr='00'+str(time+nskip)
	elif (time+nskip) < 100:
		timestr='0'+str(time+nskip)
	else:
		timestr=str(time+nskip)
        filename=strfield+'_'+str(cut)+'_'+extension+timestr+'.png'
        plt.savefig(filename)
        print 'slice saved to file '+filename
#	plt.show()
        plt.close()

#tinit=400
tinit = int(raw_input('tinit = '))
tend  = int(raw_input('tend = '))
filtro=True
print 'filtro: ', filtro
for tt in range(tinit,tend):
        slice(tt,fieldc,extension,filtro)


