# when using ipython shell run with:
# 2D spectra at a certain depth.
# % run -i spectra.py

#from prbox import *
from psettings import *
import matplotlib.pyplot as plt
import numpy as np
from pylab import *
import sys
from math import pi
from math import pow

def spectra(fld,depth,time):

#	print 'setting cut-off wavenumbers ...'

	kxmin=2*pi/Lx00
	kxmax=n*kxmin
	kymin=2*pi/Ly00
	kymax=m*kymin

	arrayx=(range(n/2+1)+range(-n/2+1,0))
	arrayy=(range(m/2+1)+range(-n/2+1,0))
	kx=[arrayx[i]*kxmin for i in range(n)]
	ky=[arrayy[i]*kymin for i in range(m)]
	ks=range(m/2)

	#################################
	# possible fields for fft
	# fld = 1  -> magnetic field
	# fld = 2  -> flow field
	#################################

	if fld==1:
		print 'working on flow field'
		sizes=np.shape(u)
	if fld==2:
		print 'working on magnetic field'
		sizes=np.shape(bx)

	nt=sizes[0]

#	print 'reshaping arrays for choosen depth and time ...'
#	print 'setting fourier space arrays ...'
	print 'depth', depth
	print 'time', time

	if fld==2:
		fxrs=bx[time,:,:,depth]
		fyrs=by[time,:,:,depth]
		fzrs=bz[time,:,:,depth]
	if fld==1:
		fxrs=u[time,:,:,depth]
		fyrs=v[time,:,:,depth]
		fzrs=w[time,:,:,depth]

	fxft=np.zeros((n,m),'float32')
	fyft=np.zeros((n,m),'float32')
	fzft=np.zeros((n,m),'float32')
	Es=np.zeros((np.size(ks)),'float32')

#	print 'computing fourier transform ...'

	fxft=np.fft.fftn(fxrs)
	fyft=np.fft.fftn(fyrs)
	fzft=np.fft.fftn(fzrs)

#	print 'computing energy spectra ..'

	for ix in range(n):
		for iy in range(m):
			k = int(np.sqrt(pow(kx[ix],2)+pow(ky[iy],2)))
			if k in ks:
				Es[k]=Es[k]+0.5*( \
				pow(np.abs(fxft[ix,iy]),2) \
				+ pow(np.abs(fyft[ix,iy]),2) \
				+ pow(np.abs(fzft[ix,iy]),2) )
	print 'DONE'
	return Es

Es1=spectra(fld,120,117)
Es2=spectra(fld,115,117)
Es3=spectra(fld,100,117)
#Es4=spectra(fld,90,117)
#Es5=spectra(fld,80,117)
Es4=spectra(fld,70,117)

knum=np.arange(1,m/2)
num=np.size(knum)
kolmogorov=[3.3*100000*pow(knum[i],-5./3.) for i in range(num)]
#normk=[0.42*knum[i] for i in range(num)]

plt.plot(Es1,color='b',linewidth=2,label='l=120')
plt.plot(Es2,color='r',linewidth=2,label='l=115')
plt.plot(Es3,color='m',linewidth=2,label='l=100')
#plt.plot(Es4,color='c',linewidth=2,label='l=90')
#plt.plot(Es5,color='y',linewidth=2,label='l=80')
plt.plot(Es4,color='g',linewidth=2,label='l=70')
plt.plot(knum[10:num-10],kolmogorov[10:num-10],linestyle= '--' \
	, color='k',linewidth=3,label='Kolmogorov')
plt.yscale('log')
plt.xscale('log')
plt.xlabel('wavenumber k')
plt.ylabel('E(k)')
plt.legend(loc=3)
plt.show()
