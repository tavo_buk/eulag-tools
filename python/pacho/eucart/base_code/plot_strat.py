import numpy as np
from psettings import *
import matplotlib.pyplot as plt

if physical==1:
	
	fig, axes1 = plt.subplots(dpi=90)
	axes1.set_xlim(zcum_s[0],zcum_s[l-1])
	axes1.plot(zcum_s,the_s,'g-',linewidth=2,\
	label=r'$\theta_{max}-\theta_T = $'+str(round(dthe,2))+' K')
	axes1.set_ylabel(r'$\theta$'+' [K]',fontsize=16)
	axes1.set_xlabel('z'+' [Mm]',fontsize=16)
	axes1.legend(fontsize=13,loc=3)

	axes2 = fig.add_axes([0.5,0.22,0.3,0.3])
	axes2.plot(zcum_s,rho_s,'b-',label=r'$\rho_B/\rho_T = $'+str(round(drho,2)))
	axes2.plot(zcum_s,rhoad_s,'b--',label=r'$\rho_B/\rho_T = $'+str(round(drhoad,2)))
	axes2.set_yscale('log')
	axes2.set_ylabel(r"$\rho$"+' '+r'$[Kg / m^{3}]$',\
	color='b',fontsize=12)
	axes2.set_xlabel('z [Mm]',fontsize=12)
	for tl in axes2.get_yticklabels():
		tl.set_color('b')
	axes2.legend(ncol=1,fontsize=11,loc=3)

	axes3 = axes2.twinx()
	axes3.plot(zcum_s,t_s,'r-')
	axes3.set_yscale('log')
	axes3.set_ylabel(r"$T$"+" "+r"$[K]$",color='r',fontsize=12)
	for tl in axes3.get_yticklabels():
		tl.set_color('r')
else:
	fig, axes1 = plt.subplots(dpi=90)
	axes1.set_xlim(zcum_s[0],zcum_s[l-1])
	axes1.plot(zcum_s,the_s,'g-',linewidth=2,label=r'$\theta_{max}-\theta_T = $'+str(round(dthe,2)))
	axes1.set_ylabel(r'$\theta$',fontsize=20)
	axes1.set_xlabel('$z$',fontsize=20)
	axes1.legend(fontsize=15,loc=3)

	axes2 = fig.add_axes([0.5,0.22,0.3,0.3])
	axes2.plot(zcum_s,rho_s,'b-',label=r'$\rho_B/\rho_T = $'+str(round(drho,2)))
	axes2.plot(zcum_s,rhoad_s,'b--',label=r'$\rho_B/\rho_T = $'+str(round(drhoad,2)))
	axes2.set_yscale('log')
	axes2.set_ylabel(r"$\rho$",color='b',fontsize=16)
	for tl in axes2.get_yticklabels():
		tl.set_color('b')
	axes2.legend(ncol=1,fontsize=11,loc=3)

	axes3 = axes2.twinx()
	axes3.plot(zcum_s,t_s,'r-')
	axes3.set_yscale('log')
	axes3.set_ylabel(r"$T$",color='r',fontsize=16)
	for tl in axes3.get_yticklabels():
		tl.set_color('r')

input = raw_input("output = ")
output = int(input)
if output==1:
	filename='stratification.eps'
	plt.savefig(filename)
	print 'plot saved to file '+filename
	plt.close()
else:
	plt.show(fig)

