from physical import *
#from psettings import *
from prxaver import *
import numpy as np

# conversion de las longitudes caracteristicas esta en phyunits.py

print 'dimenzionalizando arreglos ...'

ptbx  = np.zeros(np.shape(tbx),'float64')
ptby  = np.zeros(np.shape(tby),'float64')
ptbz  = np.zeros(np.shape(tbz),'float64')
ptbx2 = np.zeros(np.shape(tbx2),'float64')
ptby2 = np.zeros(np.shape(tby2),'float64')
ptbz2 = np.zeros(np.shape(tbz2),'float64')

# last factor to compensate for less digits in the brms spectra.png

ptbx[:,:,:]  = bb0*1e3*tbx[:,:,:]*1e3
ptby[:,:,:]  = bb0*1e3*tby[:,:,:]*1e3
ptbz[:,:,:]  = bb0*1e3*tbz[:,:,:]*1e3
ptbx2[:,:,:] = bb0*bb0*1e6*tbx2[:,:,:]*1e6
ptby2[:,:,:] = bb0*bb0*1e6*tby2[:,:,:]*1e6
ptbz2[:,:,:] = bb0*bb0*1e6*tbz2[:,:,:]*1e6
