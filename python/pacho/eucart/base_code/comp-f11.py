from modules import *
import numpy as np
import matplotlib.pyplot as plt
import sys
from physical import *

folder1 = "R500.cf_mhd_lr.2/"
folder2 = "R500.cf_mhd_hr/"
folder3 = "R500.cf_mhd_hr.remesh.2/"
params1 = settings(folder1)
params2 = settings(folder2)
params3 = settings(folder3)
nt1     = int(params1[0]/params1[11])-450
nt2     = int(params2[0]/params2[11])
nt3     = int(params3[0]/params3[11])

n1, m1, l1 = params1[2], params1[3], params1[4]
n2, m2, l2 = params2[2], params2[3], params2[4]
n3, m3, l3 = params2[2], params2[3], params2[4]

#if params1[5] == params2[5]:	
#	if params1[6] == params2[6]:
#		if params1[7] == params2[7]:
#			print "comparing boxes of same size"
#		else:
#			sys.exit('ERROR: diferent box size')
#	else:
#		sys.exit('ERROR: diferent box size')
#else:
#	sys.exit('ERROR: diferent box size')
		
physical = 1
print 'physical = ', physical
if physical == 0:
	unit1 = 1.
else:
	unit1 = l0 

Lx00,Ly00,Lz00 = params1[5] , params1[6], params1[7] 
Lx00           = Lx00*unit1
Ly00           = Ly00*unit1
Lz00           = Lz00*unit1

dx1 ,dz1       = Lx00/(n1-1), Lz00/(l1-1)
dx2 ,dz2       = Lx00/(n2-1), Lz00/(l2-1)
dx3 ,dz3       = Lx00/(n3-1), Lz00/(l3-1)

xx1 ,zz1       = np.arange(0,Lx00+dx1,dx1), np.arange(0,Lz00+dz1,dz1)
xx2 ,zz2       = np.arange(0,Lx00+dx2,dx2), np.arange(0,Lz00+dz2,dz2)
xx3 ,zz3       = np.arange(0,Lx00+dx3,dx3), np.arange(0,Lz00+dz3,dz3)

if not m1 == 1:
	dy1  = Ly00/(m1-1)
	yy1  = np.arange(0,Ly00+dy1,dy1)
if not m2 == 1:
	dy2  = Ly00/(m2-1)
	yy2  = np.arange(0,Ly00+dy2,dy2)
if not m3 == 1:
	dy3  = Ly00/(m3-1)
	yy3  = np.arange(0,Ly00+dy3,dy3)

last = 1
print "reading "+folder1+" .."
data1 = readf11(folder1,"fort.11",nt1,last)
print "reading "+folder2+" .."
data2 = readf11(folder2,"fort.11",nt2,last)
print "reading "+folder3+" .."
data3 = readf11(folder3,"fort.11",nt3,last)
