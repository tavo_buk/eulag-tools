import numpy as np
from modules import *

# list of folders

fo1  = "D6.L3_0.05B/"
fo11 = "D6.L3_0.05B.2/"
fo2  = "D6.L3_0.1B/"
fo22 = "D6.L3_0.1B.2/"
fo3  = "D6.L3_0.2B/"
fo33 = "D6.L3_0.2B.2/"
fo4  = "D6.L5_0.05B/"
fo44 = "D6.L5_0.05B.2/"
fo5  = "D6.L5_0.1B/"
fo55 = "D6.L5_0.1B.2/"
fo6  = "D6.L5_0.2B/"
fo66 = "D6.L5_0.2B.2/"

list    = [fo1,fo11,fo2,fo22,fo3,fo33,fo4,fo44,fo5,fo55,fo6,fo66]
#camposmx = {'umx':[],'vmx':[],'wmx':[],'tmx':[],'pmx':[],'bxmx':[],'bymx':[],'bzmx':[]}
#camposmn = {'umn':[],'vmn':[],'wmn':[],'tmn':[],'pmn':[],'bxmn':[],'bymn':[],'bzmn':[]}
maxs = ([],[],[],[],[],[],[],[])
mins = ([],[],[],[],[],[],[],[])


for i in range(0,np.size(list)):
	params = settings(list[i])
	nt = int(params[0]/params[11])
	print "reading "+list[i]+" ..."
	dat = readf11(list[i],"fort.11",nt,0)
#	reddat = {'u':dat[0],'v':dat[1],'w':dat[2],'bx':dat[5],'by':dat[6],'bz':dat[7]}
	for j in range(0,8):
		maxs[j].append(np.amax(dat[j]))	
		mins[j].append(np.amin(dat[j]))
