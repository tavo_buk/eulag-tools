# when using ipython shell run with:
# 2D spectra at a certain depth.
# % run -i spectra.py

from physical import *
#from prxaver import *
print 'computing brms from xaverages.dat ...'

pbxbx=np.multiply(ptbx,ptbx)
pbyby=np.multiply(ptby,ptby)
pbzbz=np.multiply(ptbz,ptbz)

ptsbx2=np.sum(np.sum(ptbx2,axis=2)/l,axis=1)/m-np.sum(np.sum(pbxbx,axis=2)/l,axis=1)/m
ptsby2=np.sum(np.sum(ptby2,axis=2)/l,axis=1)/m-np.sum(np.sum(pbyby,axis=2)/l,axis=1)/m
ptsbz2=np.sum(np.sum(ptbz2,axis=2)/l,axis=1)/m-np.sum(np.sum(pbzbz,axis=2)/l,axis=1)/m
pbrms=np.sqrt(ptsbx2+ptsby2+ptsbz2)
time = range(len(pbrms))
tau0 = nxaver*dt00*time0/(3600*24) # time to units in terrestrial days
for i in range(np.size(time)):
	time[i] = tau0*time[i]

#from prbox import *
#from psettings import *
import matplotlib.pyplot as plt
from matplotlib import rc
import numpy as np
from pylab import *
import sys
from math import pi
from math import pow
from matplotlib.lines import Line2D

ksp = range(m/2)
for i in range(np.size(ksp)):
	ksp[i] = ksp[i]/l0	

def spectra(fld,depth,time):

#	print 'setting cut-off wavenumbers ...'

	kxmin=2*pi/Lx00*l0
	kxmax=n*kxmin
	kymin=2*pi/Ly00*l0
	kymax=m*kymin

	arrayx=(range(n/2+1)+range(-n/2+1,0))
	arrayy=(range(m/2+1)+range(-m/2+1,0))
	kx=[arrayx[i]*kxmin for i in range(n)]
	ky=[arrayy[i]*kymin for i in range(m)]
	ks=range(m/2)
	
	#################################
	# possible fields for fft
	# fld = 2  -> magnetic field
	# fld = 1  -> flow field
	#################################

	if fld==1:
		print 'working on flow field', 'depth', depth, 'time', time
		sizes=np.shape(pux)
	if fld==2:
		print 'working on magnetic field', 'depth', depth, 'time', time
		sizes=np.shape(pbx)

	nt=sizes[0]

#	print 'reshaping arrays for choosen depth and time ...'
#	print 'setting fourier space arrays ...'
#	print 'depth', depth
#	print 'time', time

	if fld==2:
		fxrs=pbx[time,:,:,depth]
		fyrs=pby[time,:,:,depth]
		fzrs=pbz[time,:,:,depth]
	if fld==1:
		fxrs=pux[time,:,:,depth]
		fyrs=puy[time,:,:,depth]
		fzrs=puz[time,:,:,depth]

	fxft=np.zeros((n,m),'float32')
	fyft=np.zeros((n,m),'float32')
	fzft=np.zeros((n,m),'float32')
	Es=np.zeros((np.size(ks)),'float32')

#	print 'computing fourier transform ...'

	fxft=np.fft.fftn(fxrs)
	fyft=np.fft.fftn(fyrs)
	fzft=np.fft.fftn(fzrs)

#	print 'computing energy spectra ..'

	for ix in range(n):
		for iy in range(m):
			k = int(np.sqrt(pow(kx[ix],2)+pow(ky[iy],2)))
			if k in ks:
				Es[k]=Es[k]+0.5*( \
				pow(np.abs(fxft[ix,iy]),2) \
				+ pow(np.abs(fyft[ix,iy]),2) \
				+ pow(np.abs(fzft[ix,iy]),2) )
#	print 'DONE'
	return Es

tt1=50
tt2=180
tt3=345

zz1=120
zz2=115
zz3=100

z1=round(zz1*Lz00/l,1)
z2=round(zz2*Lz00/l,1)
z3=round(zz3*Lz00/l,1)

Es1=spectra(fld,zz1,tt1)
Es2=spectra(fld,zz2,tt1)
Es3=spectra(fld,zz3,tt1)
Es4=spectra(fld,zz1,tt2)
Es5=spectra(fld,zz2,tt2)
Es6=spectra(fld,zz3,tt2)
Es7=spectra(fld,zz1,tt3)
Es8=spectra(fld,zz2,tt3)
Es9=spectra(fld,zz3,tt3)

knum=range(1,m/2)
for i in range(np.size(knum)):
	knum[i] = knum[i]/l0
num=np.size(knum)
kolmogorov=[33.9e4*pow(knum[i],-5./3.) for i in range(num)]

#fig=plt.figure()
fig, axes1 = plt.subplots(dpi=90)
axes1.set_xlim([1e-1, 7e0])
if fld == 1:
	axes1.set_ylim([5e2, 1e6])
if fld == 2:
	axes1.set_ylim([1e0, 2e2])

markers = []
for mark in Line2D.markers:
	try:
		if len(mark)  == 1 and mark != ' ':
			markers.append(mark)
	except TypeError:
		pass	

axes1.plot(ksp,Es1,color='b',linewidth=1.5,label='z ='+str(z1)+' [Mm]')
axes1.plot(ksp,Es2,color='r',linewidth=1.5,label='z ='+str(z2)+' [Mm]')
axes1.plot(ksp,Es3,color='g',linewidth=1.5,label='z ='+str(z3)+' [Mm]')
axes1.plot(ksp,Es4,'b-.',linewidth=2)
axes1.plot(ksp,Es5,'r-.',linewidth=2)
axes1.plot(ksp,Es6,'g-.',linewidth=2)
axes1.plot(ksp,Es7,'b--',linewidth=2)
axes1.plot(ksp,Es8,'r--',linewidth=2)
axes1.plot(ksp,Es9,'g--',linewidth=2)
axes1.plot(knum[10:num-10],kolmogorov[10:num-10],linestyle= '--' \
	, color='k',linewidth=1.5,label='Kolmogorov')
axes1.set_yscale('log')
axes1.set_xscale('log')
axes1.set_xlabel(r'$k_{\perp}$ [Mm]$^{-1}$',fontsize=14)
if fld == 1:
	axes1.set_ylabel(r'$E_k(k)$$\times 10^{12}$ [m$^3$s$^{-2}$]',fontsize=14)
if fld == 2:
	axes1.set_ylabel(r'$E_m(k)$$\times 10^{12}$ [m$^3$s$^{-2}$]',fontsize=14)
#axes1.legend(loc=3,fontsize=13)
if fld == 1:
	axes1.legend(loc=4,ncol=1,fontsize=9)
if fld == 2:
	axes1.legend(loc=3,ncol=1,fontsize=9)
# bbox_to_anchor=(0.5, 0.99)

if fld == 1:
	minbrms = np.amin(pbrms)
	maxbrms = np.amax(pbrms)

	axes2=fig.add_axes([0.25,0.21,0.35,0.2])
	axes2.plot(time,pbrms,'k', linewidth=2)
	axes2.plot(np.ones(len(pbrms))*tt1*tau0,arange(\
		minbrms,maxbrms,(maxbrms-minbrms)/len(pbrms)),'k')
	axes2.plot(np.ones(len(pbrms))*tt2*tau0,arange(\
		minbrms,maxbrms,(maxbrms-minbrms)/len(pbrms)),'k-.')
	axes2.plot(np.ones(len(pbrms))*tt3*tau0,arange(\
		minbrms,maxbrms,(maxbrms-minbrms)/len(pbrms)),'k--')
	#axes2.set_yscale('log')
	#axes2.set_xscale('log')
	axes2.set_xlabel('$t$ [days]',fontsize=13)
	axes2.set_ylabel(r'$B_{rms}$$\times 10^{-3}$[T]',fontsize=13)

print "show on screen:     output = 0"
print "save to a png file: output = 1"
input = raw_input("output = ")
output = int(input)
if output==1:
	if fld == 1:
		filename = 'flowspectra.png'
	if fld == 2:
		filename = 'magspectra.png'
	plt.savefig(filename)
	print 'slice saved to file '+filename
	plt.close()
else:
	plt.show(fig)
