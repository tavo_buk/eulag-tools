# when using ipython shell run with:
# % run -i ....

from psettings import *
import matplotlib.pyplot as plt
import numpy as np
from pylab import *
import matplotlib.ticker as ticker
import sys

dx=Lx00/(n-1)
dz=Lz00/(l-1)
if not m == 1:
	dy=Ly00/(m-1)
	yy=np.arange(0,Ly00+dy,dy)

xx=np.arange(0,Lx00+dx,dx)
zz=np.arange(0,Lz00+dz,dz)

try: 
     print 'plotting fields: (u,v,w,bx,by,bz)'
     print "(time,extension,cut) = ", time, extension, cut
except NameError:
     sys.exit('Error: please define (time,extension,cut)')

def fmt(x,pos):
        a, b = '{:.2e}'.format(x).split('e')
        b= int(b)
        return r'${} \times 10^{{{}}}$'.format(a, b)

filtro=True
estilo='%.2f'
#estilo='%1.2g'
textstr1='$u$'
textstr2='$v$'
textstr3='$w$'
textstr4='$B_x$'
textstr5='$B_y$'
textstr6='$B_z$'

if extension=="xy":
	Y,X=np.meshgrid(yy,xx)
        field1=u[time,:,:,cut]
        field2=v[time,:,:,cut]
        field3=w[time,:,:,cut]
        field4=bx[time,:,:,cut]
        field5=by[time,:,:,cut]
        field6=bz[time,:,:,cut]
if extension=="xz":
        Z,X=np.meshgrid(zz,xx)
        field1=u[time,:,cut,:]
        field2=v[time,:,cut,:]
        field3=w[time,:,cut,:]
        field4=bx[time,:,cut,:]
        field5=by[time,:,cut,:]
        field6=bz[time,:,cut,:]
if extension=="yz":
        Z,Y=np.meshgrid(zz,yy)
        field1=u[time,cut,:,:]
        field2=v[time,cut,:,:]
        field3=w[time,cut,:,:]
        field4=bx[time,cut,:,:]
        field5=by[time,cut,:,:]
        field6=bz[time,cut,:,:]

if extension=="xz":
        if filtro is True:
                print "filtering ..."
                field1_f=np.empty((n,l),'float32')
                field2_f=np.empty((n,l),'float32')
                field3_f=np.empty((n,l),'float32')
                field4_f=np.empty((n,l),'float32')
                field5_f=np.empty((n,l),'float32')
                field6_f=np.empty((n,l),'float32')
                for ii in range(0,n):
                        for kk in range(0,l):
                                if kk==l-1:
                                        field1_f[ii,kk]=field1[ii,kk]
                                        field2_f[ii,kk]=field2[ii,kk]
                                        field3_f[ii,kk]=field3[ii,kk]
                                        field4_f[ii,kk]=field4[ii,kk]
                                        field5_f[ii,kk]=field5[ii,kk]
                                        field6_f[ii,kk]=field6[ii,kk]
                                else:
                                        field1_f[ii,kk]=0.5*(field1[ii,kk]+field1[ii,kk+1])
                                        field2_f[ii,kk]=0.5*(field2[ii,kk]+field2[ii,kk+1])
                                        field3_f[ii,kk]=0.5*(field3[ii,kk]+field3[ii,kk+1])
                                        field4_f[ii,kk]=0.5*(field4[ii,kk]+field4[ii,kk+1])
                                        field5_f[ii,kk]=0.5*(field5[ii,kk]+field5[ii,kk+1])
                                        field6_f[ii,kk]=0.5*(field6[ii,kk]+field6[ii,kk+1])
                field1=field1_f
                field2=field2_f
                field3=field3_f
                field4=field4_f
                field5=field5_f
                field6=field6_f

if extension=="yz":
        if filtro is True:
                print "filtering ..."
                field1_f=np.empty((m,l),'float32')
                field2_f=np.empty((m,l),'float32')
                field3_f=np.empty((m,l),'float32')
                field4_f=np.empty((m,l),'float32')
                field5_f=np.empty((m,l),'float32')
                field6_f=np.empty((m,l),'float32')
                for jj in range(0,m):
                        for kk in range(0,l):
                                if kk==l-1:
                                        field1_f[jj,kk]=field1[jj,kk]
                                        field2_f[jj,kk]=field2[jj,kk]
                                        field3_f[jj,kk]=field3[jj,kk]
                                        field4_f[jj,kk]=field4[jj,kk]
                                        field5_f[jj,kk]=field5[jj,kk]
                                        field6_f[jj,kk]=field6[jj,kk]
                                else:
                                        field1_f[jj,kk]=0.5*(field1[jj,kk]+field1[jj,kk+1])
                                        field2_f[jj,kk]=0.5*(field2[jj,kk]+field2[jj,kk+1])
                                        field3_f[jj,kk]=0.5*(field3[jj,kk]+field3[jj,kk+1])
                                        field4_f[jj,kk]=0.5*(field4[jj,kk]+field4[jj,kk+1])
                                        field5_f[jj,kk]=0.5*(field5[jj,kk]+field5[jj,kk+1])
                                        field6_f[jj,kk]=0.5*(field6[jj,kk]+field6[jj,kk+1])
                field1=field1_f
                field2=field2_f
                field3=field3_f
                field4=field4_f
                field5=field5_f
                field6=field6_f


def plote(campo,extension,mapa):
        maxv=np.amax(campo)
        minv=np.amin(campo)
        d=maxv-minv
        lev=np.arange(minv,maxv,d/60)
        mapa.set_over(mapa(0.999))
        mapa.set_under(mapa(0))
        if extension=="xy":
                return plt.contourf(X,Y,campo,lev,alpha=0.9,cmap=mapa,vmin=minv/2.,vmax=maxv/2.)
        if extension=="xz":
                return plt.contourf(X,Z,campo,lev,alpha=0.9,cmap=mapa,vmin=minv/2.,vmax=maxv/2.)
        if extension=="yz":
                return plt.contourf(Y,Z,campo,lev,alpha=0.9,cmap=mapa,vmin=minv/2.,vmax=maxv/2.)

scinotation=False

def barticks(campo):
        mint=np.amin(campo)
        maxt=np.amax(campo)
        ticks_cb=[mint,0,0.9*maxt,maxt]
        return ticks_cb

def cbar(campo,figura,grafico,extension):
        if extension=="xy":
                if scinotation is True:
                        return figura.colorbar(grafico,ticks=barticks(campo),format=ticker.FuncFormatter(fmt))
                else:
                        return figura.colorbar(grafico,ticks=barticks(campo),format=estilo)
        else:
                if scinotation is True:
                        return figura.colorbar(grafico,ticks=barticks(campo),format=ticker.FuncFormatter(fmt),orientation='horizontal',shrink=0.9)
                else:
                        return figura.colorbar(grafico,ticks=barticks(campo),format=estilo,orientation='horizontal',shrink=0.9)

xloc=-0.4
yloc=0.55
if extension=="xy":
         changeloc=2*xloc
else:
         changeloc=0.8

props = dict(boxstyle='round', facecolor='wheat', alpha=0.8)

def tlabel(ax,textstr,xl,yl):
         return ax.text(xl,yl,textstr,transform=ax.transAxes,fontsize=16,verticalalignment='top',bbox=props)

############ setting plot .....

fig=plt.figure()
        	
ax1=fig.add_subplot(321)
graf1=plote(field1,extension,cm.afmhot)
cbar(field1,fig,graf1,extension)
ax1.set_aspect('equal')
tlabel(ax1,textstr1,xloc,yloc)

ax2=fig.add_subplot(323)
graf2=plote(field2,extension,cm.afmhot)
cbar(field2,fig,graf2,extension)
ax2.set_aspect('equal')
tlabel(ax2,textstr2,xloc,yloc)

ax3=fig.add_subplot(325)
graf3=plote(field3,extension,cm.afmhot)
cbar(field3,fig,graf3,extension)
ax3.set_aspect('equal')
tlabel(ax3,textstr3,xloc,yloc)

ax4=fig.add_subplot(322)
graf4=plote(field4,extension,cm.CMRmap)
cbar(field4,fig,graf4,extension)
ax4.set_aspect('equal')
tlabel(ax4,textstr4,changeloc-xloc,yloc)

ax5=fig.add_subplot(324)
graf5=plote(field5,extension,cm.CMRmap)
cbar(field5,fig,graf5,extension)
ax5.set_aspect('equal')
tlabel(ax5,textstr5,changeloc-xloc,yloc)

ax6=fig.add_subplot(326)
graf6=plote(field6,extension,cm.CMRmap)
cbar(field6,fig,graf6,extension)
ax6.set_aspect('equal')
tlabel(ax6,textstr6,changeloc-xloc,yloc)

plt.show()

