# when using ipython shell run with:
# 2D spectra at a certain depth.
# % run -i spectra.py

from prxaver import *
print 'computing brms from xaverages.dat ...'


bxbx=np.multiply(tbx,tbx)
byby=np.multiply(tby,tby)
bzbz=np.multiply(tbz,tbz)

tsbx2=np.sum(np.sum(tbx2,axis=2)/l,axis=1)/m-np.sum(np.sum(bxbx,axis=2)/l,axis=1)/m
tsby2=np.sum(np.sum(tby2,axis=2)/l,axis=1)/m-np.sum(np.sum(byby,axis=2)/l,axis=1)/m
tsbz2=np.sum(np.sum(tbz2,axis=2)/l,axis=1)/m-np.sum(np.sum(bzbz,axis=2)/l,axis=1)/m
brms=np.sqrt(tsbx2+tsby2+tsbz2)
time=range(len(brms))

#from prbox import *
from psettings import *
import matplotlib.pyplot as plt
import numpy as np
from pylab import *
import sys
from math import pi
from math import pow

def spectra(fld,depth,time):

#	print 'setting cut-off wavenumbers ...'

	kxmin=2*pi/Lx00
	kxmax=n*kxmin
	kymin=2*pi/Ly00
	kymax=m*kymin

	arrayx=(range(n/2+1)+range(-n/2+1,0))
	arrayy=(range(m/2+1)+range(-n/2+1,0))
	kx=[arrayx[i]*kxmin for i in range(n)]
	ky=[arrayy[i]*kymin for i in range(m)]
	ks=range(m/2)

	#################################
	# possible fields for fft
	# fld = 1  -> magnetic field
	# fld = 2  -> flow field
	#################################

	if fld==1:
		print 'working on flow field', 'depth', depth, 'time', time
		sizes=np.shape(u)
	if fld==2:
		print 'working on magnetic field', 'depth', depth, 'time', time
		sizes=np.shape(bx)

	nt=sizes[0]

#	print 'reshaping arrays for choosen depth and time ...'
#	print 'setting fourier space arrays ...'
#	print 'depth', depth
#	print 'time', time

	if fld==2:
		fxrs=bx[time,:,:,depth]
		fyrs=by[time,:,:,depth]
		fzrs=bz[time,:,:,depth]
	if fld==1:
		fxrs=u[time,:,:,depth]
		fyrs=v[time,:,:,depth]
		fzrs=w[time,:,:,depth]

	fxft=np.zeros((n,m),'float32')
	fyft=np.zeros((n,m),'float32')
	fzft=np.zeros((n,m),'float32')
	Es=np.zeros((np.size(ks)),'float32')

#	print 'computing fourier transform ...'

	fxft=np.fft.fftn(fxrs)
	fyft=np.fft.fftn(fyrs)
	fzft=np.fft.fftn(fzrs)

#	print 'computing energy spectra ..'

	for ix in range(n):
		for iy in range(m):
			k = int(np.sqrt(pow(kx[ix],2)+pow(ky[iy],2)))
			if k in ks:
				Es[k]=Es[k]+0.5*( \
				pow(np.abs(fxft[ix,iy]),2) \
				+ pow(np.abs(fyft[ix,iy]),2) \
				+ pow(np.abs(fzft[ix,iy]),2) )
#	print 'DONE'
	return Es

tt1=0
tt2=50
tt3=158

zz1=120
zz2=115
zz3=90

z1=round(zz1*Lz00/l,2)
z2=round(zz2*Lz00/l,2)
z3=round(zz3*Lz00/l,2)

Es1=spectra(fld,zz1,tt1)
Es2=spectra(fld,zz2,tt1)
Es3=spectra(fld,zz3,tt1)
Es4=spectra(fld,zz1,tt2)
Es5=spectra(fld,zz2,tt2)
Es6=spectra(fld,zz3,tt2)
Es7=spectra(fld,zz1,tt3)
Es8=spectra(fld,zz2,tt3)
Es9=spectra(fld,zz3,tt3)

knum=np.arange(1,m/2)
num=np.size(knum)
kolmogorov=[3.3*100000*pow(knum[i],-5./3.) for i in range(num)]

#fig=plt.figure()
fig, axes1 = plt.subplots(dpi=90)
axes1.set_xlim([1, 70])
axes1.set_ylim([10, 5*10000])
axes1.plot(Es1,color='b',linewidth=2,label='z ='+str(z1))
axes1.plot(Es2,color='r',linewidth=2,label='z ='+str(z2))
axes1.plot(Es3,color='m',linewidth=2,label='z ='+str(z3))
axes1.plot(Es4,'b:',linewidth=2)
axes1.plot(Es5,'r:',linewidth=2)
axes1.plot(Es6,'m:',linewidth=2)
axes1.plot(Es7,'b--',linewidth=2)
axes1.plot(Es8,'r--',linewidth=2)
axes1.plot(Es9,'m--',linewidth=2)
axes1.plot(knum[10:num-10],kolmogorov[10:num-10],linestyle= '--' \
	, color='k',linewidth=3,label='Kolmogorov')
axes1.set_yscale('log')
axes1.set_xscale('log')
axes1.set_xlabel('wavenumber $k_{\perp}$',fontsize=20)
axes1.set_ylabel('$E(k)$',fontsize=20)
#axes1.legend(loc=3,fontsize=13)
axes1.legend(loc='upper center',bbox_to_anchor=(0.5, 0.99),ncol=4,fontsize=11)

axes2=fig.add_axes([0.45,0.21,0.35,0.2])
axes2.plot(time,brms,'k', linewidth=2)
axes2.plot(np.ones(len(brms))*tt1,arange(0.01,0.1,(0.1-0.01)/len(brms)),'k')
axes2.plot(np.ones(len(brms))*tt2,arange(0.01,0.1,(0.1-0.01)/len(brms)),'k:')
axes2.plot(np.ones(len(brms))*tt3,arange(0.01,0.1,(0.1-0.01)/len(brms)),'k--')
axes2.set_yscale('log')
axes2.set_xscale('log')
axes2.set_xlabel('$time$',fontsize=16)
axes2.set_ylabel('$b_{rms}$',fontsize=16)
print "show on screen:     output = 0"
print "save to a png file: output = 1"
input = raw_input("output = ")
output = int(input)
if output==1:
	filename='spectra.png'
	plt.savefig(filename)
	print 'slice saved to file '+filename
	plt.close()
else:
	plt.show(fig)
