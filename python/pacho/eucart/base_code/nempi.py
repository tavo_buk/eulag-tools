# Compile with % run -i 
# 
# Calculation of the full momentum stress tensor
#
import NEMPImod as Nm
import numpy as np
from prstrat import *
mu=1.

print "density = 1 -> perfil adiabatico"
print "density = 2 -> perfil politropico"
option = raw_input("tipo de densidad = ")
den=int(option)
if den==1:
	density=rhoad_s
if den==2:
	density=rho_s

TensorComponents0 = Tuu0,Tvv0,Tww0,Tuv0,Tvw0,Tuw0,\
Tbxbx0,Tbyby0,Tbzbz0,Tbxby0,Tbybz0,Tbxbz0

TensorComponents1 = Tuu1,Tvv1,Tww1,Tuv1,Tvw1,Tuw1,\
Tbxbx1,Tbyby1,Tbzbz1,Tbxby1,Tbybz1,Tbxbz1

print "choose a limit time for MHD run to compute cofs."
input = raw_input("time = ")
tt=int(input)

def PiHD(TensorComponents):
	tuu,tvv,tww,tuv,tvw,tuw,tbxbx,tbyby,\
	tbzbz,tbxby,tbybz,tbxbz = TensorComponents
	# diagonal components
	pi_xx = np.multiply(density,tuu)\
		+ (tbyby+tbzbz)/(2.*mu)
	pi_yy = np.multiply(density,tvv)\
		+ (tbxbx+tbzbz)/(2.*mu)
	pi_zz = np.multiply(density,tww)\
		+ (tbxbx+tbzbz)/(2.*mu) 
	#off-diagonal components
	pi_xy = np.multiply(density,tuv)\
		- tbxby/(2.*mu)
	pi_yz = np.multiply(density,tvw)\
		- tbybz/(2.*mu)
	pi_xz = np.multiply(density,tuw)\
		- tbxbz/(2.*mu)

	return pi_xx, pi_yy, pi_zz, pi_xy, pi_yz, \
		pi_xz

def Pi(TensorComponents,time):	
	Tuu,Tvv,Tww,Tuv,Tvw,Tuw,Tbxbx,Tbyby,\
	Tbzbz,Tbxby,Tbybz,Tbxbz = TensorComponents
	tuu=Tuu[time,:]
	tvv=Tvv[time,:]
	tww=Tww[time,:]
	tuv=Tuv[time,:]
	tvw=Tvw[time,:]
	tuw=Tuw[time,:]
	tbxbx=Tbxbx[time,:]
	tbyby=Tbyby[time,:]
	tbzbz=Tbzbz[time,:]
	tbxby=Tbxby[time,:]
	tbybz=Tbybz[time,:]
	tbxbz=Tbxbz[time,:]
	# diagonal components
	pi_xx = np.multiply(density,tuu)\
		+ (tbyby+tbzbz)/(2.*mu)
	pi_yy = np.multiply(density,tvv)\
		+ (tbxbx+tbzbz)/(2.*mu)
	pi_zz = np.multiply(density,tww)\
		+ (tbxbx+tbzbz)/(2.*mu) 
	#off-diagonal components
	pi_xy = np.multiply(density,tuv)\
		- tbxby/(2.*mu)
	pi_yz = np.multiply(density,tvw)\
		- tbybz/(2.*mu)
	pi_xz = np.multiply(density,tuw)\
		- tbxbz/(2.*mu)

	return pi_xx, pi_yy, pi_zz, pi_xy, pi_yz, \
		pi_xz

# HD run momentum stress tensor
pi_xx0, pi_yy0, pi_zz0, pi_xy0, pi_yz0, pi_xz0 = \
	PiHD(TensorComponents0)

# compute NEMPI cofs.____________________________________
qp=[]
for step in range(tt):
	# MHD momentum stress tensor at step tt
	pi_xx1, pi_yy1, pi_zz1, pi_xy1, pi_yz1, pi_xz1 = \
		Pi(TensorComponents1,step)
	
	MBt=MB[tt,:]
	qp.append(-2.*mu*np.divide(pi_xx1-pi_xx0,MBt)) 
