print 'var options: '
print 'u_rms -> 0'
print 'tsth  -> 1'
inp = raw_input("var = ")
select = int(inp)

m_1=settings_1[3]
l_1=settings_1[3]
m_lr=settings_lr[3]
m_hr=settings_hr[3]
l_lr=settings_lr[4]
l_hr=settings_hr[4]
Lz00_lr=settings_lr[7]

rads_1=[Lz00_lr/l_1*i for i in range(l_1)]
rads_lr=[Lz00_lr/l_lr*i for i in range(l_lr)]
rads_hr=[Lz00_lr/l_hr*i for i in range(l_hr)]

if select==0:
	uu_1=np.multiply(dat[1],dat[1])
	vv_1=np.multiply(dat[1],dat[1])
	ww_1=np.multiply(dat[2],dat[2])
	uu_lr=np.multiply(dat_lr[3],dat_lr[3])
	vv_lr=np.multiply(dat_lr[2],dat_lr[2])
	ww_lr=np.multiply(dat_lr[3],dat_lr[3])
	uu_hr=np.multiply(dat_hr[1],dat_hr[1])
	vv_hr=np.multiply(dat_hr[2],dat_hr[2])
	ww_hr=np.multiply(dat_hr[3],dat_hr[3])

	fu2_1=np.sum(dat[7],axis=1)/m_1-np.sum(uu_1,axis=1)/m_1
	fv2_1=np.sum(dat[8],axis=1)/m_1-np.sum(vv_1,axis=1)/m_1
	fw2_1=np.sum(dat[9],axis=1)/m_1-np.sum(ww_1,axis=1)/m_1
	fu2_lr=np.sum(dat_lr[7],axis=1)/m_lr-np.sum(uu_lr,axis=1)/m_lr
	fv2_lr=np.sum(dat_lr[8],axis=1)/m_lr-np.sum(vv_lr,axis=1)/m_lr
	fw2_lr=np.sum(dat_lr[9],axis=1)/m_lr-np.sum(ww_lr,axis=1)/m_lr
	fu2_hr=np.sum(dat_hr[7],axis=1)/m_hr-np.sum(uu_hr,axis=1)/m_hr
	fv2_hr=np.sum(dat_hr[8],axis=1)/m_hr-np.sum(vv_hr,axis=1)/m_hr
	fw2_hr=np.sum(dat_hr[9],axis=1)/m_hr-np.sum(ww_hr,axis=1)/m_hr

	urms_1=np.sqrt(fu2_1+fv2_1+fw2_1)
	urms_lr=np.sqrt(fu2_lr+fv2_lr+fw2_lr)
	urms_hr=np.sqrt(fu2_hr+fv2_hr+fw2_hr)

	input0 = raw_input("physical = ")
	physical = int(input0)
	#print "physical = %d" % physical
	if physical==0:
		num1=1.
		num2=1.
	if physical==1:
		num1=l0   #physical length
		num2=vel0 #physical velocity

	rads_lr = [num1*rads_lr[i] for i in range(l_lr)]
	rads_hr = [num1*rads_hr[i] for i in range(l_hr)]
	rads_1  = [num1*rads_1[i]  for i in range(l_1) ]
	urms_lr_r = [num2*urms_lr[len(urms_lr)-1,i] for i in range(l_lr)]
	urms_hr_r = [num2*urms_hr[len(urms_hr)-1,i] for i in range(l_hr)]
	urms_1_r = [num2*urms_1[len(urms_1)-1,i] for i in range(l_1)]

	plt.plot(rads_lr,urms_lr_r,'r--',linewidth=3,label=r'$256^3$'+' (remesh)')
	plt.plot(rads_hr,urms_hr_r,'g',linewidth=3,label=r'$256^3$')
	plt.plot(rads_1,urms_1_r,'b--',linewidth=3,label=r'$128^3$')
	if physical==1:
		plt.xlabel('$z$ [Mm]',fontsize=20)
		plt.ylabel(r'$\widetilde{u}_{rms}(z)$'+' [Km/s]',fontsize=20)
	plt.legend(fontsize=15,loc=2,ncol=1)

	input = raw_input("output = ")
	output = int(input)
	if output==1:
		filename='radial-profiles.eps'
		plt.savefig(filename)
		print 'plot saved to file '+filename
		plt.close()
	else:
		plt.show()
else:
	the_1 = dat[26]
	the_lr = dat_lr[26]
	the_hr = dat_hr[26]

	tsth_1  = np.sum(the_1,axis=1)/m_1
	tsth_lr = np.sum(the_lr,axis=1)/m_lr
	tsth_hr = np.sum(the_hr,axis=1)/m_hr

	input0 = raw_input("physical = ")
	physical = int(input0)
	if physical == 0:
		num3 = 1.
	if physical == 1:
		num3 = T0 # physical unit of temperature

	tsth_1_r  = [num3/10000*tsth_1[len(tsth_1)-1,i] for i in range(l_1)]  	
	tsth_lr_r = [num3/10000*tsth_lr[len(tsth_lr)-1,i] for i in range(l_lr)]  	
	tsth_hr_r = [num3/10000*tsth_hr[len(tsth_hr)-1,i] for i in range(l_hr)]

		
	plt.plot(rads_lr,tsth_lr_r,'r--',linewidth=3,label=r'$256^3$'+' (remesh)')
	plt.plot(rads_hr,tsth_hr_r,'g',linewidth=3,label=r'$256^3$')
	plt.plot(rads_1,tsth_1_r,'b--',linewidth=3,label=r'$128^3$')
	if physical==1:
		plt.xlabel('$z$ [Mm]',fontsize=20)
		plt.ylabel(r'$\widetilde{\theta}^{\prime}(z)$'+r' $\times 10^4$'+'[K]',fontsize=20)
	plt.legend(fontsize=15,loc=2,ncol=1)

	input2 = raw_input("output = ")
	output = int(input2)
	if output==1:
		filename='radial-profiles2.eps'
		plt.savefig(filename)
		print 'plot saved to file '+filename
		plt.close()
	else:
		plt.show()



