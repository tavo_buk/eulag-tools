from __future__ import division
import numpy as np
import math as ma

l0  = 10.                         # [Mm]
Rh0 = 2.5*10**(-4)                # [Kg / m^3]
T0  = 5800.                       # [K]
G0  = float(275/49.5)                  # [m / s^2]
mu0 = 4*ma.pi*10**(-7)            # [N / A^2]

#typical timescale (s)
time0 = float(np.sqrt(l0/float(G0))*10**(3))

#typical velocity timescale (km/s)
vel0  = float(l0/float(time0))*10**3

# typical B field (kT)
bb0   = float(np.sqrt(mu0*Rh0*G0*l0)) 

# to convert to Gauss: 1[G] = 1e-4[T]

print 'time-scale     = ', time0,  ' [s]'
print 'velocity scale = ', vel0, ' [Km / s]'
print 'B field scale  = ', bb0, ' [kT]' 

