# when using ipython shell run with:
# % run -i plotbox_sl.py

#from prbox import * 
from psettings import *
import matplotlib.pyplot as plt
import numpy as np
from pylab import *
import sys



if phys is not None:
	print 'Physical units mode ON'
if phys is None:
	dx=Lx00/(n-1)
	dz=Lz00/(l-1)
	if not m == 1:
		dy=Ly00/(m-1)	        
		yy=np.arange(0,Ly00+dy,dy)

	xx=np.arange(0,Lx00+dx,dx)
	zz=np.arange(0,Lz00+dz,dz)

# define the parameters: (field, time,extension,cut,mapa,saturation) 
try:
     print 'field dimensions ...', np.shape(field)
     print "(time,extension,cut,mapa,saturation) = ", time, extension, cut, mapa, saturation
except NameError:
     sys.exit('Error: please define (time,extension,cut,mapa,saturation)')

sctit = int(raw_input('colorbar title for field (mag = 0, vel = 1) = '))
if sctit == 0:
	cbtit = 'B [T]'
elif sctit == 1:
	cbtit = r'$u$$\times 10^3$[m/s]'

if mapa==1:
	bmap=cm.afmhot
if mapa==2:
	bmap=cm.rainbow
if mapa==3:
	bmap=cm.CMRmap
if mapa==4:
        bmap=cm.gray

filtro=True
estilo='%.2f'
#estilo='%1.2g'

if extension=="xy":
        Y,X=np.meshgrid(yy,xx)
if extension=="xz":
        Z,X=np.meshgrid(zz,xx)
if extension=="yz":
        Z,Y=np.meshgrid(zz,yy)

#############################
#  type of filter
#  2 point -> ftype=2
#  3 point -> ftype=3
#############################

filtro=True
ftype=2

if filtro is True:
	if extension=='xz':
                if ftype==2:
		        print " (2 point filter) ->  filtering ..."
			field_f=np.empty((n,l),'float32')
			for kk in range(0,l):
				if kk==l-1:
					field_f[:,kk]=field[time,:,cut,kk]
				else:
					field_f[:,kk]=0.5*(field[time,:,cut,kk]+field[time,:,cut,kk+1])
		if ftype==3:
			print " (3 point filter) -> filtering ... "
			field_f=np.empty((n,l),'float32')
                        field_f[:,0]=0.5*(field[time,:,cut,0]+field[time,:,cut,1])
                        field_f[:,l-1]=0.5*(field[time,:,cut,l-1]+field[time,:,cut,l-2])
			for kk in range(1,l-1):
				field_f[:,kk]=0.25*(field[time,:,cut,kk-1]+2.0*field[time,:,cut,kk]+field[time,:,cut,kk+1])
        if extension=='yz':
		if ftype==2:
                	print " (2 point filter) -> filtering ..."
			field_f=np.empty((m,l),'float32')
			for kk in range(0,l):
				if kk==l-1:
					field_f[:,kk]=field[time,cut,:,kk]
				else:
					field_f[:,kk]=0.5*(field[time,cut,:,kk]+field[time,cut,:,kk+1])
		if ftype==3:
			print " (3 point filter) -> filtering ..."
			field_f=np.empty((m,l),'float32')
                        field_f[:,0]=0.5*(field[time,cut,:,0]+field[time,cut,:,1])
                        field_f[:,l-1]=0.5*(field[time,cut,:,l-1]+field[time,cut,:,l-2])
			for kk in range(1,l-1):
                                field_f[:,kk]=0.25*(field[time,cut,:,kk-1]+2.0*field[time,cut,:,kk]+field[time,cut,:,kk+1])
        if extension=='xy':
                print "no filter for this extension !"
                field_f=field[time,:,:,cut]
else:
        if extension=='xz':
                fieldnf=field[time,:,cut,:]
        if extension=='yz':
                fieldnf=field[time,cut,:,:]
        if extension=='xy':
                fieldnf=field[time,:,:,cut]

if phys is not None:
	if extension == 'xy':
		strxlabel = 'x [Mm]'
		strylabel = 'y [Mm]'
	else:
		strxlabel = 'x [Mm]'
		strylabel = 'z [Mm]'
else:
	if extension == 'xy':
		strxlabel = 'x'
		strylabel = 'y'
	else:
		strxlabel = 'x'
		strylabel = 'z'

fsize= 18

def plotf(campo,extension,saturation,file):
        fig=plt.figure()
        ax=fig.add_subplot(111)
        bmap.set_over(bmap(0.999))
        bmap.set_under(bmap(0))       
#        maxval=np.amax(campo)
#        minval=np.amin(campo)
        maxval=np.amax(field)
        minval=np.amin(field)
        ticks_cb=[minval,0,0.9*maxval,maxval]
        delta=maxval-minval
        levels=np.arange(minval,maxval,delta/60)
        if extension=="xy":
		if saturation is True: 
			graf=plt.contourf(X,Y,campo,levels,alpha=0.9,cmap=bmap,vmin=minval/4.,vmax=maxval/4.)
		else:
			graf=plt.contourf(X,Y,campo,levels,alpha=0.9,cmap=bmap,vmin=minval,vmax=maxval)
		cb = fig.colorbar(graf,ticks=ticks_cb,orientation='vertical',format=estilo)
		if phys is not None:
			cb.set_label(cbtit, fontsize = fsize)
		plt.xlabel(strxlabel, fontsize = fsize)
		plt.ylabel(strylabel, fontsize = fsize)
        if extension=='xz':
		if saturation is True:
			graf=plt.contourf(X,Z,campo,levels,alpha=0.9,cmap=bmap,vmin=minval/4.,vmax=maxval/4.)
		else:
			graf=plt.contourf(X,Z,campo,levels,alpha=0.9,cmap=bmap,vmin=minval,vmax=maxval)
		cb = fig.colorbar(graf,ticks=ticks_cb,orientation='horizontal',format=estilo)
		if phys is not None:
			cb.set_label(cbtit, fontsize = fsize)
		plt.autoscale(False)
#		plt.plot(arange(0,5,0.1),arange(0,5,0.1)*0+1.875,linewidth=2,color='b')
#		plt.plot(arange(0,5,0.1),arange(0,5,0.1)*0+1.796,linewidth=2,color='r')
#		plt.plot(arange(0,5,0.1),arange(0,5,0.1)*0+1.562,linewidth=2,color='m')
#		plt.plot(arange(0,5,0.1),arange(0,5,0.1)*0+1.093,linewidth=2,color='g')
		plt.xlabel(strxlabel, fontsize = fsize)
                plt.ylabel(strylabel, fontsize = fsize)
        if extension=='yz':
		if saturation is True:
			graf=plt.contourf(Y,Z,campo,levels,alpha=0.9,cmap=bmap,vmin=minval/4.,vmax=maxval/4.)
		else:
			graf=plt.contourf(Y,Z,campo,levels,alpha=0.9,cmap=bmap,vmin=minval,vmax=maxval)
		fig.colorbar(graf,ticks=ticks_cb,orientation='horizontal',format=estilo)
        ax.set_aspect('equal')
        plt.show()
#	plt.savefig(file)

if filtro is True:
	if ftype==2:
		figfile='fig_2pf.png'
	if ftype==3:
		figfile='fig_3pf.png'
        plotf(field_f,extension,saturation,figfile)
	print figfile
else:
	figfile='fig_nf.png'
        plotf(fieldnf,extension,saturation,figfile) 
	print figfile

