import numpy as np
from psettings import *
from physical import *

strat="strat.dat"

z_s     = np.empty((l),'float64')
zcum_s  = np.empty((l),'float64')
t_s     = np.empty((l),'float64')
rho_s   = np.empty((l),'float64')
rhoad_s = np.empty((l),'float64')
the_s   = np.empty((l),'float64')

file_strat=open(strat, "r")
data_strat=[line.strip() for line in file_strat]
size_strat=len(data_strat)

for i in range(size_strat):
	z_s[i]     = float(data_strat[i].split()[0])
	zcum_s[i]  = float(data_strat[i].split()[1])
	t_s[i]     = float(data_strat[i].split()[2])
	rho_s[i]   = float(data_strat[i].split()[3])
	rhoad_s[i] = float(data_strat[i].split()[4])
	the_s[i]   = float(data_strat[i].split()[5])

iphys = raw_input('use physical output ? [y/n]')
if iphys == 'y':
	physical=1
if iphys == 'n':
	physical=0

if physical==1:
	z_s = l0*z_s
	zcum_s = l0*zcum_s
	t_s = T0*t_s
	rho_s = Rh0*rho_s
	rhoad_s = Rh0*rhoad_s
	the_s = T0*the_s

drho=rho_s[0]/rho_s[size_strat-1]
drhoad=rhoad_s[0]/rhoad_s[size_strat-1]
maxthe=max(the_s)
dthe=maxthe-the_s[size_strat-1]

print "rho_poly(Bot) = %d" % rho_s[0]
print "rho_poly(Top) = %d" % rho_s[size_strat-1]
print "rho_ad(Bot) = %d" % rhoad_s[0]
print "rho_ad(Top) = %d" % rhoad_s[size_strat-1]
print "polytropic rho(Bot/Top) = %d" % drho
print "adiabatic rho(Bot/Top) = %d" % drhoad
print "Max(The)-The(Top) =  %d" % dthe

