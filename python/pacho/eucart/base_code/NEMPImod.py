import numpy as np
import modules as mod

def readf11(folder,ntime):
	file="fort.11"
	filepath=folder+file
	itt,nt,n,m,l,Lx00,Ly00,Lz00,dt00,nslice,noutp,nplot,nstore,\
		nxaver = mod.settings(folder)
	u=np.zeros((ntime,n,m,l),'float32')			
	v=np.zeros((ntime,n,m,l),'float32')			
	w=np.zeros((ntime,n,m,l),'float32')			
	t=np.zeros((ntime,n,m,l),'float32')			
	p=np.zeros((ntime,n,m,l),'float32')			
	bx=np.zeros((ntime,n,m,l),'float32')			
	by=np.zeros((ntime,n,m,l),'float32')			
	bz=np.zeros((ntime,n,m,l),'float32')			
	data=u,v,w,t,p,bx,by,bz
	n_var=len(data)
	print "reading data from file : "+filepath
	data_r=open(filepath,'rb')
	for i in range(0,ntime):
		for j in range(0,n_var):
			np.fromfile(data_r,dtype='uint32',count=1)
			data[j][i,:,:,:]= \
				np.reshape(np.fromfile(data_r,dtype= \
				'float32',count=n*m*l),(n,m,l), \
				order='F')
			np.fromfile(data_r,dtype='uint32',count=1)
	data_r.close()
	print('number of outputs = ',ntime)
	
	return data

def readf11Last(folder,ntime):
	file="fort.11"
	filepath=folder+file
	itt,nt,n,m,l,Lx00,Ly00,Lz00,dt00,nslice,noutp,nplot,nstore,\
		nxaver = mod.settings(folder)
	u=np.zeros((n,m,l),'float32')			
	v=np.zeros((n,m,l),'float32')			
	w=np.zeros((n,m,l),'float32')			
	t=np.zeros((n,m,l),'float32')			
	p=np.zeros((n,m,l),'float32')			
	bx=np.zeros((n,m,l),'float32')			
	by=np.zeros((n,m,l),'float32')			
	bz=np.zeros((n,m,l),'float32')			
	data=u,v,w,t,p,bx,by,bz
	n_var=len(data)
	print "reading data from file : "+filepath
	data_r=open(filepath,'rb')
	for i in range(0,ntime):
		for j in range(0,n_var):
			np.fromfile(data_r,dtype='uint32',count=1)
			if i==ntime-1:
				data[j][:,:,:]= \
					np.reshape(np.fromfile(data_r,dtype= \
					'float32',count=n*m*l),(n,m,l), \
					order='F')
			else:
				np.fromfile(data_r,dtype='float32',\
					count=n*m*l)
			np.fromfile(data_r,dtype='uint32',count=1)
			
	data_r.close()
	print('number of outputs = ',1)
	
	return data
	

def XYaver(var):
	'''
	needs as input a (t,n,m,l) matrix variable
	from (fort.11)
	'''
	dims=np.shape(var)
	n=dims[1]
	m=dims[2]
	Mvar=np.sum(np.sum(var,axis=1)/n,axis=2)/m
	
	return Mvar	

def XYaverLast(var):
	'''
	input as a (n,m,l) matrix from last step
	in (fort.11) 
	'''
	dims=np.shape(var)
	n=dims[0]
	m=dims[1]
	Mvar=np.sum(np.sum(var,axis=0)/n,axis=1)/m

	return Mvar

def Tstress(var1,var2):
	corr=np.multiply(var1,var2)
	Mcorr=XYaver(corr)
	Mvar1=XYaver(var1)
	Mvar2=XYaver(var2)
	Tstress=Mcorr-np.multiply(Mvar1,Mvar2)

	return Tstress

def TstressLast(var1,var2):
	corr=np.multiply(var1,var2)
	Mcorr=XYaverLast(corr)
	Mvar1=XYaverLast(var1)
	Mvar2=XYaverLast(var2)
	Tstress=Mcorr-np.multiply(Mvar1,Mvar2)

	return Tstress
	
