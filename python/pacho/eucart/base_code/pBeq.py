from tseries import *
from prstrat import *
import subprocess as sp
import numpy as np

mu=1.
tt=len(urms)
meanrho=np.sum(rhoad_s,axis=0)/l
ulast=urms[len(urms)-1]
ek=0.5*meanrho*ulast*ulast

beq=np.sqrt(2*ek*mu)

print "Equipartition field for this simulation = %f" % beq
sp.call("rm beq.dat",shell = True)
sp.call("touch beq.dat",shell = True)
bfile = "beq.dat"
bdat  = open(bfile,'w')
bdat.write(str(beq))
bdat.close()

print "equipartition B field stored in beq.dat" 
