#from phyunits import *
from pylab import *
from physical import *
import matplotlib.pyplot as plt
from matplotlib import gridspec
#from psettings import *

#........................................................
def convert(s):
	if s == 'True':
		return True
	elif s == 'False':
		return False
	else:
		sys.exit('ERROR : NON-boolean value') 

# filter function (3D) ...................................
def filter(ftype,field,dim1,dim2,dim3):
	ffield = np.zeros((dim1,dim2,dim3),"float32")
	if ftype == 2:
		print "2 pt filtering ... "
		for kk in range(0,dim3):
			if kk == dim3-1:
				ffield[:,:,kk] = field[:,:,kk]
			else:
				ffield[:,:,kk] = 0.5*(field[:,:,kk]+field[:,:,kk+1])
	elif ftype == 3:
		print "3 pt filtering ... "
		ffield[:,:,0]      = 0.5*(field[:,:,0]+field[:,:,1])
		ffield[:,:,dim3-1] = 0.5*(field[:,:,dim3-1]+field[:,:,dim3-2])	 
		for kk in range(1,dim3-1):
			ffield[:,:,kk] = 0.25*(field[:,:,kk-1]+field[:,:,kk]+field[:,:,kk+1])
	return ffield

#........................................................

sat = convert(raw_input('Saturation = '))
if sat is True:
	limiter = 2.
else:
	limiter = 1.

estilo = '%.2f' # estilo para labels en colorbar

maps = cm.afmhot, cm.rainbow, cm.CMRmap

tau  = nplot*dt00*time0/(3600*24)  # physical temporal factor (dias terrestres)
t_l  = [10,50,100,120,170,200] 	   # location in array
t_l2 = t_l                         # times to annotate

extension = raw_input('extension = ')
# filtering for vertical slices
if not extension == 'xy':
	pwf  = np.zeros((np.size(t_l),n,m,l),'float32')
	pbzf = np.zeros((np.size(t_l),n,m,l),'float32') 
	for i in range(np.size(t_l)):
		pwf[i,:,:,:]  = filter(3,pw[t_l[i],:,:,:],n,m,l)
		pbzf[i,:,:,:] = filter(3,pbz[t_l[i],:,:,:],n,m,l)
	t_l = range(np.size(t_l))
if extension == 'xy':
	pwf  = pw
	pbzf = pbz

# grid preferences already set in "phyunits.py"
# grid of xy slices
if extension == 'xy':
	print 'z size = ', Lz00
	raw_cut  = float(raw_input(' xy cut = '))
	cut      = int(raw_cut/dz)+1
	Y,X      = np.meshgrid(xx,yy)
	mesh1    = X
	mesh2    = Y


if extension == 'xz':
	print 'y size = ', Ly00
	raw_cut  = float(raw_input(' xz cut = '))
	cut      = int(raw_cut/dy)+1
	Z,X      = np.meshgrid(zz,xx)
	mesh1    = X
	mesh2    = Z


if extension == 'yz':
	print 'x size = ', Lx00
	raw_cut  = float(raw_input(' yz cut = '))
	cut      = int(raw_cut/dx)+1
	Z,Y      = np.meshgrid(zz,yy)
	mesh1    = Y
	mesh2    = Z

maxvel    = np.amax([np.amax(pw[:,:,:,:])])
minvel    = np.amin([np.amin(pw[:,:,:,:])])
maxbb     = np.amax([np.amax(pbz[:,:,:,:])])
minbb     = np.amin([np.amin(pbz[:,:,:,:])])

deltavel  = maxvel-minvel
deltabb   = maxbb-minbb
levelsvel = np.arange(minvel,maxvel,deltavel/60)
levelsbb  = np.arange(minbb,maxbb,deltabb/60)
ticksvel = [0.9*minvel,0,0.9*maxvel,maxvel]
ticksbb = [0.9*minbb,0,0.9*maxbb,maxbb]
	
############################################################################
def plotslice(mesh1,mesh2,field,type,mapa,time,extension,maxx,minn):
	# type = 0 (velocity field)
	#      = 1 (magnetic field)
	if type == 0:
		lev = levelsvel
	if type == 1:
		lev = levelsbb

	if extension == 'xy':	
		campo = field[time,:,:,cut]
	if extension == 'xz':
		campo = field[time,:,cut,:]
	if extension == 'yz':
		campo = field[time,cut,:,:]

	plt.contourf(mesh1,mesh2,campo,lev,alpha=0.9,\
	cmap = maps[mapa],vmin = minn/limiter, vmax = maxx/limiter)
	
############################################################################
axisx = 'x [Mm]'
axisy = 'y [Mm]'
axisz = 'z [Mm]'

if extension == 'xy':
	fig = plt.figure(figsize= (10,10))
if not extension == 'xy':
	fig = plt.figure(figsize= (10,7.5))
if extension == 'xy':
	gs  = gridspec.GridSpec(4,3,width_ratios = [1,1,1],\
		height_ratios = [1,1,1,1])
	titulox = axisx
	tituloy = axisy
if extension == 'xz' or extension == 'yz':
	gs  = gridspec.GridSpec(4,3,width_ratios = [5,5,5],\
		height_ratios = [2,2,2,2])
	if extension == 'xz':
		titulox = axisx
		tituloy = axisz
	if extension == 'yz': 
		titulox = axisy
		tituloy = axisz

output = int(raw_input('output = '))

# first set
eje1 = fig.add_subplot(gs[0])
g1   = plotslice(mesh1,mesh2,pwf,0,0,t_l[0],extension,maxvel,minvel) 
eje1.set_aspect('equal')
eje1.set_ylabel(tituloy)
eje1.annotate('t = '+str(round(t_l2[0]*tau,2))+' days',xy = (2,1),xytext=(3,1))
if extension == 'xy':
	cax1 = plt.axes([0.9,0.55,0.015,0.36])
	clb1  = plt.colorbar(ticks = ticksvel, cax = cax1, orientation = 'vertical',format = estilo) 
	clb1.set_label(r'$u_z$$\times 10^3$[m/s]',labelpad = -30, y = 1.1,fontsize = 14,rotation=0)
if extension == 'xz' or extension == 'yz':
	cax1 = plt.axes([0.25,0.12,0.55,0.015])
	clb1  = plt.colorbar(ticks = ticksvel, cax = cax1, orientation = 'horizontal',format = estilo)
	clb1.set_label(r'$u_z$$\times 10^3$[m/s]',labelpad = -30, x= 1.15, fontsize = 14)

eje2 = fig.add_subplot(gs[1])
g2   = plotslice(mesh1,mesh2,pwf,0,0,t_l[1],extension,maxvel,minvel)
eje2.annotate('t = '+str(round(t_l2[1]*tau,2))+' days',xy = (2,1),xytext=(3,1))
eje2.set_aspect('equal')

eje3 = fig.add_subplot(gs[2])
g3   = plotslice(mesh1,mesh2,pwf,0,0,t_l[2],extension,maxvel,minvel)
eje3.annotate('t = '+str(round(t_l2[2]*tau,2))+' days',xy = (2,1),xytext=(3,1))
eje3.set_aspect('equal')

eje4 = fig.add_subplot(gs[3])
g4   = plotslice(mesh1,mesh2,pbzf,1,2,t_l[0],extension,maxbb,minbb)
eje4.annotate('t = '+str(round(t_l2[0]*tau,2))+' days',xy = (2,1),xytext=(3,1))
eje4.set_aspect('equal')
eje4.set_ylabel(tituloy)
if extension == 'xy':
	cax2 = plt.axes([0.9,0.07,0.015,0.36])
	clb2 = plt.colorbar(ticks = ticksbb, cax = cax2, orientation = 'vertical',format = estilo) 
	clb2.set_label(r'$B_z$ [T]',labelpad = -30, y = 1.1,fontsize = 14,rotation=0)
if extension == 'xz' or extension == 'yz':
	cax2 = plt.axes([0.25,0.07,0.55,0.015])
	clb2 = plt.colorbar(ticks = ticksbb, cax = cax2, orientation = 'horizontal',format=estilo)
	clb2.set_label(r'$B_z$ [T]',labelpad = -30, x= 1.1, fontsize = 14)
eje5 = fig.add_subplot(gs[4])
g5   = plotslice(mesh1,mesh2,pbzf,1,2,t_l[1],extension,maxbb,minbb)
eje5.annotate('t = '+str(round(t_l2[1]*tau,2))+' days',xy = (2,1),xytext=(3,1))
eje5.set_aspect('equal')

eje6 = fig.add_subplot(gs[5])
g6   = plotslice(mesh1,mesh2,pbzf,1,2,t_l[2],extension,maxbb,minbb)
eje6.annotate('t = '+str(round(t_l2[2]*tau,2))+' days',xy = (2,1),xytext=(3,1))
eje6.set_aspect('equal')

# second set
eje7 = fig.add_subplot(gs[6])
g7   = plotslice(mesh1,mesh2,pwf,0,0,t_l[3],extension,maxvel,minvel) 
eje7.annotate('t = '+str(round(t_l2[3]*tau,2))+' days',xy = (2,1),xytext=(3,1))
eje7.set_aspect('equal')
eje7.set_ylabel(tituloy)

eje8 = fig.add_subplot(gs[7])
g8   = plotslice(mesh1,mesh2,pwf,0,0,t_l[4],extension,maxvel,minvel) 
eje8.annotate('t = '+str(round(t_l2[4]*tau,2))+' days',xy = (2,1),xytext=(3,1))
eje8.set_aspect('equal')

eje9 = fig.add_subplot(gs[8])
g9   = plotslice(mesh1,mesh2,pwf,0,0,t_l[5],extension,maxvel,minvel) 
eje9.annotate('t = '+str(round(t_l2[5]*tau,2))+' days',xy = (2,1),xytext=(3,1))
eje9.set_aspect('equal')

eje10 = fig.add_subplot(gs[9])
g10   = plotslice(mesh1,mesh2,pbzf,1,2,t_l[3],extension,maxbb,minbb) 
eje10.annotate('t = '+str(round(t_l2[3]*tau,2))+' days',xy = (2,1),xytext=(3,1))
eje10.set_aspect('equal')
eje10.set_ylabel(tituloy)
eje10.set_xlabel(titulox)

eje11 = fig.add_subplot(gs[10])
g11   = plotslice(mesh1,mesh2,pbzf,1,2,t_l[4],extension,maxbb,minbb) 
eje11.annotate('t = '+str(round(t_l2[4]*tau,2))+' days',xy = (2,1),xytext=(3,1))
eje11.set_aspect('equal')
eje11.set_xlabel(titulox)

eje12 = fig.add_subplot(gs[11])
g12   = plotslice(mesh1,mesh2,pbzf,1,2,t_l[5],extension,maxbb,minbb) 
eje12.annotate('t = '+str(round(t_l2[5]*tau,2))+' days',xy = (2,1),xytext=(3,1))
eje12.set_aspect('equal')
eje12.set_xlabel(titulox)

fig.tight_layout()
if extension == 'xy':
	plt.subplots_adjust(right = 0.9)
if extension == 'xz' or extension == 'yz':
	plt.subplots_adjust(bottom = 0.2)

if output == 0:
	plt.show()
elif output == 1:
	filename ='dp-lr_0.01_'+extension+str(cut)+'.png' 
	plt.savefig(filename)
	print 'saved to file : ', filename
