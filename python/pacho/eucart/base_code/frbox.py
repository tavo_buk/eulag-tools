import numpy as np
from psettings import *
from fortranfile import FortranFile
MHD=True
ntime=int(itt/nplot)-250
if not fort11 is None: 
    print "Reading fort.11 file..."
    data = FortranFile(fort11)

    u = np.zeros((ntime, n, m, l), 'float32')	# Phi field
    v = np.zeros((ntime, n, m, l), 'float32')	# Theta field
    w = np.zeros((ntime, n, m, l), 'float32')	# Radial field
    the = np.zeros((ntime, n, m, l), 'float32')
    P = np.zeros((ntime, n, m, l), 'float32')
    if MHD is True:
        bx = np.zeros((ntime, n, m, l), 'float32')	# Phi Field
        by = np.zeros((ntime, n, m, l), 'float32')	# Theta Field
        bz = np.zeros((ntime, n, m, l), 'float32')	# Radial Field

        for k in range(ntime):
            print k
            ''' Defining variables '''
            # Velocity field
            u[k,:,:,:] = np.reshape(data.readReals('f'), (n, m, l),order = 'F')
            v[k,:,:,:] = np.reshape(data.readReals('f'), (n, m, l),order = 'F')
            w[k,:,:,:] = np.reshape(data.readReals('f'), (n, m, l),order = 'F')
	
            # Potential Temperature and pressure
            the[k,:,:,:] = np.reshape(data.readReals('f'), (n, m, l),order= 'F')
            P[k,:,:,:] = np.reshape(data.readReals('f'), (n, m, l),order= 'F')
	
     		# Magnetic field
            bx[k,:,:,:] = np.reshape(data.readReals('f'), (n, m, l),order = 'F')
            by[k,:,:,:] = np.reshape(data.readReals('f'), (n, m, l),order = 'F')
            bz[k,:,:,:] = np.reshape(data.readReals('f'), (n, m, l),order = 'F')
	
    else:
        for k in range(0, nplot):
            print k
            ''' Defining variables '''
            # Velocity field
            u[k,:,:,:] = np.reshape(data.readReals('f'), (n, m, l),order = 'F')
            v[k,:,:,:] = np.reshape(data.readReals('f'), (n, m, l),order = 'F')
            w[k,:,:,:] = np.reshape(data.readReals('f'), (n, m, l),order = 'F')
	
            # Potential Temperature and pressure
            the[k,:,:,:] = np.reshape(data.readReals('f'), (n, m, l),order = 'F')
            P[k,:,:,:] = np.reshape(data.readReals('f'), (n, m, l),order = 'F')
else:
    print "File fort.11 does not exist."
