# execute with % run -i mhd_fluc.py 

mt = raw_input("To compute mhd fluctuations enter temporal step: ")
print "calculating MHD fluctuations for time", mt
uf=np.empty((n,m,l),'float32')
vf=np.empty((n,m,l),'float32')
wf=np.empty((n,m,l),'float32')
tf=np.empty((n,m,l),'float32')
pf=np.empty((n,m,l),'float32')
bxf=np.empty((n,m,l),'float32')
byf=np.empty((n,m,l),'float32')
bzf=np.empty((n,m,l),'float32')
for i in range(0,n):							
	for j in range(0,m):
		for k in range(0,l):
			uf[i,j,k]=u[mt,i,j,k]-up[mt,k]
			vf[i,j,k]=v[mt,i,j,k]-vp[mt,k]
			wf[i,j,k]=w[mt,i,j,k]-wp[mt,k]
			tf[i,j,k]=t[mt,i,j,k]-tp[mt,k]
			pf[i,j,k]=p[mt,i,j,k]-pp[mt,k]
			bxf[i,j,k]=bx[mt,i,j,k]-bxp[mt,k]
			byf[i,j,k]=by[mt,i,j,k]-byp[mt,k]
			bzf[i,j,k]=bz[mt,i,j,k]-bzp[mt,k]
