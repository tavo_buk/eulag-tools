# execute with the mode % run -i

from prstrat import *
#from pmhd_fluc import *
import numpy as np

u0f2=np.multiply(u0f,u0f)
v0f2=np.multiply(v0f,v0f)
w0f2=np.multiply(w0f,w0f)
t0f2=np.multiply(t0f,t0f)
p0f2=np.multiply(p0f,p0f)
bx0f2=np.multiply(bx0f,bx0f)
by0f2=np.multiply(by0f,by0f)
bz0f2=np.multiply(bz0f,bz0f)

uf2=np.multiply(uf,uf)
vf2=np.multiply(vf,vf)
wf2=np.multiply(wf,wf)
tf2=np.multiply(tf,tf)
pf2=np.multiply(pf,pf)
bxf2=np.multiply(bxf,bxf)
byf2=np.multiply(byf,byf)
bzf2=np.multiply(bzf,bzf)

pu0f2=np.sum(np.sum(u0f2,axis=2)/n,axis=1)/m
pv0f2=np.sum(np.sum(v0f2,axis=2)/n,axis=1)/m
pw0f2=np.sum(np.sum(w0f2,axis=2)/n,axis=1)/m
pt0f2=np.sum(np.sum(t0f2,axis=2)/n,axis=1)/m
pp0f2=np.sum(np.sum(p0f2,axis=2)/n,axis=1)/m
pbx0f2=np.sum(np.sum(bx0f2,axis=2)/n,axis=1)/m
pby0f2=np.sum(np.sum(by0f2,axis=2)/n,axis=1)/m
pbz0f2=np.sum(np.sum(bz0f2,axis=2)/n,axis=1)/m

puf2=np.sum(np.sum(uf2,axis=2)/n,axis=1)/m
pvf2=np.sum(np.sum(vf2,axis=2)/n,axis=1)/m
pwf2=np.sum(np.sum(wf2,axis=2)/n,axis=1)/m
ptf2=np.sum(np.sum(tf2,axis=2)/n,axis=1)/m
ppf2=np.sum(np.sum(pf2,axis=2)/n,axis=1)/m
pbxf2=np.sum(np.sum(bxf2,axis=2)/n,axis=1)/m
pbyf2=np.sum(np.sum(byf2,axis=2)/n,axis=1)/m
pbzf2=np.sum(np.sum(bzf2,axis=2)/n,axis=1)/m

#print "density = 1 -> perfil adiabatico"
#print "density = 2 -> perfil politropico"
#option = raw_input("tipo de densidad = ")
option=1
density=int(option)

if density==1:
	densidad=rhoad_s
if density==2:
	densidad=rho_s

duf2=np.multiply(densidad,puf2-pu0f2)
dvf2=np.multiply(densidad,pvf2-pv0f2)
dwf2=np.multiply(densidad,pwf2-pw0f2)
dbxf2=pbxf2-pbx0f2
dbyf2=pbyf2-pby0f2
dbzf2=pbzf2-pbz0f2

mu0=1.

bxp2=np.multiply(bxp[mt,:],bxp[mt,:])
byp2=np.multiply(byp[mt,:],byp[mt,:])
bzp2=np.multiply(bzp[mt,:],bzp[mt,:])
bp2=bxp2+byp2+bzp2

# NEMPI coefficients for the case of vertical field in 3D.
q0=np.empty((l),'float32')
qp=np.empty((l),'float32')
for kk in range(0,l):
	q0[kk]=mu0/(2*bp2[kk])*(2*dwf2[kk]-duf2[kk]-dvf2[kk]+(1/mu0)*(dbxf2[kk]+dbyf2[kk]-2*dbzf2[kk]))
	qp[kk]=-mu0/(2*bp2[kk])*((1/mu0)*dbzf2[kk]+duf2[kk]+dvf2[kk])

