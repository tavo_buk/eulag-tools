import numpy as np
from psettings import *
from fortranfile import *
MHD=True

newfile="fort.9.n"
with FortranFile(newf,mode='w') as ndata:
#with open(newf, 'wb') as newfile:
	# open new class for fort.9 file
#	ndata=FortranFile(newfile)
	# reconstruct original data for binary file
	ru=np.ravel(u, order = 'F')
	rv=np.ravel(v, order = 'F')
	rw=np.ravel(w, order = 'F')
	rox=np.ravel(ox, order = 'F')
	roy=np.ravel(oy, order = 'F')
	roz=np.ravel(oz, order = 'F')
	rox2=np.ravel(ox2, order = 'F')
	roy2=np.ravel(oy2, order = 'F')
	roz2=np.ravel(oz2, order = 'F')
	rth=np.ravel(th, order = 'F')
	rp=np.ravel(p, order = 'F')
	rfx=np.ravel(fx, order = 'F')
	rfy=np.ravel(fy, order = 'F')
	rfz=np.ravel(fz, order = 'F')
	rft=np.ravel(ft, order = 'F')

	rpm=np.ravel(pm, order = 'F')
	rbx=np.ravel(bx, order = 'F')
	rby=np.ravel(by, order = 'F')
	rbz=np.ravel(bz, order = 'F')
	rfbx=np.ravel(fbx, order = 'F')
	rfby=np.ravel(fby, order = 'F')
	rfbz=np.ravel(fbz, order = 'F')

	rtke=np.ravel(tke, order = 'F')

	rchm=np.ravel(chm, order = 'F')
	rchm=np.ravel(chm, order = 'F')
	rfchm=np.ravel(fchm, order = 'F')
	rfchm=np.ravel(fchm, order = 'F')
	# write to new file in the exact same order of old fort.9

	ndata.writeReals(ru,'f')
	ndata.writeReals(rv,'f')
	ndata.writeReals(rw,'f')
	ndata.writeReals(rox,'f')
	ndata.writeReals(roy,'f')
	ndata.writeReals(roz,'f')
	ndata.writeReals(rox2,'f')
	ndata.writeReals(roy2,'f')
	ndata.writeReals(roz2,'f')
	ndata.writeReals(rth,'f')
	ndata.writeReals(rp,'f')
	ndata.writeReals(rfx,'f')
	ndata.writeReals(rfy,'f')
	ndata.writeReals(rfz,'f')
	ndata.writeReals(rft,'f')

	ndata.writeReals(time,'f')
	ndata.writeReals(time,'f')
	ndata.writeReals(time,'f')

	ndata.writeReals(rpm,'f')
	ndata.writeReals(rbx,'f')
	ndata.writeReals(rby,'f')
	ndata.writeReals(rbz,'f')
	ndata.writeReals(rfbx,'f')
	ndata.writeReals(rfby,'f')
	ndata.writeReals(rfbz,'f')

	ndata.writeReals(time,'f')
	ndata.writeReals(time,'f')
	ndata.writeReals(time,'f')
	ndata.writeReals(time,'f')
	ndata.writeReals(time,'f')
	ndata.writeReals(time,'f')
	ndata.writeReals(time,'f')
	ndata.writeReals(time,'f')
	ndata.writeReals(time,'f')
	ndata.writeReals(time,'f')

	ndata.writeReals(rtke,'f')

	ndata.writeReals(time,'f')

	ndata.writeReals(rchm,'f')
	ndata.writeReals(rchm,'f')
	ndata.writeReals(rfchm,'f')
	ndata.writeReals(rfchm,'f')
