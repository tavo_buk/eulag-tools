from psettings import *
from prbox import *

full_fluc = False

hdfile="../R500.cf_mhd_lr.2/fort.11"
#modify ntime0 accordingly to the output in psettings of the HD folder 
time_file="../R500.cf_mhd_lr.2/itt.txt"
t_file=open(time_file, "r")
t_data=[line.strip() for line in t_file]
itt0=int(t_data[0])
ntime0=int(itt0/nplot)

print("itt0 = ",itt0)

if not hdfile is None:
	u0=np.empty((n,m,l),'float32')
	v0=np.empty((n,m,l),'float32')
	w0=np.empty((n,m,l),'float32')
	t0=np.empty((n,m,l),'float32')
	p0=np.empty((n,m,l),'float32')
	bx0=np.empty((n,m,l),'float32')
	by0=np.empty((n,m,l),'float32')
	bz0=np.empty((n,m,l),'float32')

	data0= u0, v0, w0, t0, p0, bx0, by0, bz0
	n_var0=len(data0)
	print('Passing data of file fort.11 of the HD steady state')
	data_f0=open(hdfile, "rb")
	for i in range(0,ntime0-1):
		for j in range(0,n_var0):
			np.fromfile(data_f0,dtype='uint32',count=1)
			np.fromfile(data_f0,dtype='float32',count=n*m*l)
			np.fromfile(data_f0,dtype='uint32',count=1)
	print('Reading data from the last time_step')
	for j in range(0,n_var0):
		np.fromfile(data_f0,dtype='uint32',count=1)
		data0[j][:,:,:]=np.reshape(np.fromfile(data_f0,dtype='float32',count=n*m*l),(n,m,l),order= 'F')
		np.fromfile(data_f0,dtype='uint32',count=1)
	data_f0.close()
	print('number of outputs = ',ntime0)
# Calculation of (xy) averages
print('calculating averages ')

u0p=np.sum(np.sum(u0[:,:,:],axis=2)/n,axis=1)/m
v0p=np.sum(np.sum(v0[:,:,:],axis=2)/n,axis=1)/m
w0p=np.sum(np.sum(w0[:,:,:],axis=2)/n,axis=1)/m
t0p=np.sum(np.sum(t0[:,:,:],axis=2)/n,axis=1)/m
p0p=np.sum(np.sum(p0[:,:,:],axis=2)/n,axis=1)/m
bx0p=np.sum(np.sum(bx0[:,:,:],axis=2)/n,axis=1)/m
by0p=np.sum(np.sum(by0[:,:,:],axis=2)/n,axis=1)/m
bz0p=np.sum(np.sum(bz0[:,:,:],axis=2)/n,axis=1)/m

up=np.sum(np.sum(u,axis=2)/n,axis=1)/m
vp=np.sum(np.sum(v,axis=2)/n,axis=1)/m
wp=np.sum(np.sum(w,axis=2)/n,axis=1)/m
tp=np.sum(np.sum(t,axis=2)/n,axis=1)/m
pp=np.sum(np.sum(p,axis=2)/n,axis=1)/m
bxp=np.sum(np.sum(bx,axis=2)/n,axis=1)/m
byp=np.sum(np.sum(by,axis=2)/n,axis=1)/m
bzp=np.sum(np.sum(bz,axis=2)/n,axis=1)/m

# Calculation of fluctuations
print('calculating HD fluctuations ')

u0f=np.empty((n,m,l),'float32')
v0f=np.empty((n,m,l),'float32')
w0f=np.empty((n,m,l),'float32')
t0f=np.empty((n,m,l),'float32')
p0f=np.empty((n,m,l),'float32')
bx0f=np.empty((n,m,l),'float32')
by0f=np.empty((n,m,l),'float32')
bz0f=np.empty((n,m,l),'float32')

for i in range(0,n):
	for j in range(0,m):
		for k in range(0,l):
			u0f[i,j,k]=u0[i,j,k]-u0p[k]
			v0f[i,j,k]=v0[i,j,k]-v0p[k]
			w0f[i,j,k]=w0[i,j,k]-w0p[k]
			t0f[i,j,k]=t0[i,j,k]-t0p[k]
			p0f[i,j,k]=p0[i,j,k]-p0p[k]
			bx0f[i,j,k]=bx0[i,j,k]-bx0p[k]
			by0f[i,j,k]=by0[i,j,k]-by0p[k]
			bz0f[i,j,k]=bz0[i,j,k]-bz0p[k]

if full_fluc is True:
	uf=np.empty((ntime,n,m,l),'float32')
	vf=np.empty((ntime,n,m,l),'float32')
	wf=np.empty((ntime,n,m,l),'float32')
	tf=np.empty((ntime,n,m,l),'float32')
	pf=np.empty((ntime,n,m,l),'float32')
	bxf=np.empty((ntime,n,m,l),'float32')
	byf=np.empty((ntime,n,m,l),'float32')
	bzf=np.empty((ntime,n,m,l),'float32')
	for tt in range(0,ntime):
		for i in range(0,n):
			for j in range(0,m):
				for k in range(0,l):
					uf[tt,i,j,k]=u[tt,i,j,k]-up[tt,k]
					vf[tt,i,j,k]=v[tt,i,j,k]-vp[tt,k]
					wf[tt,i,j,k]=w[tt,i,j,k]-wp[tt,k]
					tf[tt,i,j,k]=t[tt,i,j,k]-tp[tt,k]
					pf[tt,i,j,k]=p[tt,i,j,k]-pp[tt,k]
					bxf[tt,i,j,k]=bx[tt,i,j,k]-bxp[tt,k]
					byf[tt,i,j,k]=by[tt,i,j,k]-byp[tt,k]
					bzf[tt,i,j,k]=bz[tt,i,j,k]-bzp[tt,k]


