#--------------------------------------------------------
# move files needed to restart a run into a new folder
#--------------------------------------------------------

import subprocess
import sys
import os.path
import time

out1 = subprocess.check_output("pwd")
out2 = out1.strip().split("/")
outod = out2[-1]
outnd = out2[-1][:-2]


ostr = "cp fort.9 ../" + outnd + "/fort.10"
ostr2 = "cp fort.11 ../" + outnd + "/fort.12"

ostr3 = "cp " + outnd + ".pbs ../" + outnd

ostr5 = "cp settings.py ../" + outnd
ostr6 = "cp mylist.nml ../" + outnd


spul = "../" + outnd + "/pulses.dat"
mpul = "touch " + spul

file_pul=open(spul, 'w')

if os.path.isfile('./mylist.nml'):
    file_my=open('./mylist.nml', "r")
    data_my=[line.strip() for line in file_my]
    file_my.close()
else:
    print 'mylist.nml not found ...'
    time.sleep(2)
    print 'self destruct initiated ...'
    time.sleep(2)
    print '3'
    time.sleep(1)
    print '2'
    time.sleep(1)
    print '1'
    time.sleep(1)
    print '                             ____'
    print '                     __,-~~/~    `---.'
    print '                   _/_,---(      ,    )'
    print '               __ /        <    /   )  \___'
    print '- ------===;;;-====------------------===;;;===----- -  -'
    print '                  \/  ~"~"~"~"~"~\~"~)~"/'
    print '                  (_ (   \  (     >    \)'
    print '                   \_( _ <         >_>`'
    print '                      ~ `-i` ::>|--"'
    print '                          I;|.|.|'
    print '                         <|i::|i|`.'
    print '                        (` ^`"`-` ")'


    sys.exit()

nplot=int(data_my[13].split()[2][0:-1])


file_ee=open('./ee.txt', "r")
data_ee=[line.strip() for line in file_ee]
file_ee.close()

size_ee=len(data_ee)
string="it="
for i in range(size_ee):
    if string in data_ee[i].split():
        it = data_ee[i].split()[1]
        itt=int(it)

np="  " + str(itt/nplot)


print "Current Dir = " + outod
print "Move files to " + outnd + "? (y,n)"
inp = raw_input(">  ")


if inp == "ys":

    print "Copying fort.9 ..."
    subprocess.call(ostr, shell = True)

    print "Copying fort.11 ..."
    subprocess.call(ostr2, shell = True)

    print "Copying pbs file ..."
    subprocess.call(ostr3, shell = True)

    print "Copying settings.py ..."
    subprocess.call(ostr5, shell = True)

    print "Copying mylist.nml ..."
    subprocess.call(ostr6, shell = True)

    print "Writing pulses.dat ..."
    subprocess.call(mpul, shell = True)
    file_pul.write(np)
    file_pul.close()

    print "Done"

if inp == "y":

    print "Copying fort.9 ..."
    subprocess.call(ostr, shell = True)

    print "Copying pbs file ..."
    subprocess.call(ostr3, shell = True)

    print "Copying settings.py ..."
    subprocess.call(ostr5, shell = True)

    print "Copying mylist.nml ..."
    subprocess.call(ostr6, shell = True)

    print "Writing pulses.dat ..."
    subprocess.call(mpul, shell = True)
    file_pul.write(np)
    file_pul.close()

    print "Done"

else:
    file_pul.close()
    sys.exit()

