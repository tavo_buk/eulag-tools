from settings import rad_core, r, radius, rad_th, m, l
import numpy as np
from math import pi
import matplotlib.pyplot as plt
import matplotlib
from pylab import sin, cos, meshgrid
from matplotlib import animation
import eudyn as e
import sys

cut = 5. # Degrees to be cut off
good = int(cut*m/180.)
rad_th_good = np.linspace(0+(cut/90)*pi/2., (1 - cut/180)*pi, num = m - 2*good)
fig = plt.figure()
R, TH = meshgrid(r, rad_th_good)


X = R*sin(TH)
Y = R*cos(TH)

data = e.rxav()

ns = input("number of steps to animate:")
print "save or display? (s,d)"
s = raw_input(">  ")

if (s == "s"):
        print "save as:"
        nm = raw_input(">  ")
else:
        pass




bx = data[16]
by = data[17]
bz = data[18]

bx = bx[-ns::,:,:]
by = by[-ns::,:,:]
bz = bz[-ns::,:,:]


lmi = np.amin(np.mean(bx, axis=0))
lma = np.amax(np.mean(bx, axis=0))

lmin1 = np.amin(bx)
lmax1 = np.amax(bx)

lmin = ((lmin1) + (lmi))/2
lmax = ((lmax1) + (lma))/2

ax = fig.add_subplot(111)

ims = []

for i in range(ns):

	BX = np.array(bx[i,:,:])
	BY = np.array(by[i,:,:])
	BZ = np.array(bz[i,:,:])


	plotbx = BX[good:-good,:]
#-----------------------------------------------------------------------------#
# PLOT


	matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
	cs = ax.contourf(X, Y, plotbx, levels = np.linspace(lmin, lmax, 50), interpolation = 'bicubic', cmap = plt.get_cmap("RdYlBu"), extend='both')

        csa = cs.collections

        ims.append(csa)

# Add colorbar
cax = fig.add_axes([0.28, 0.22, 0.02, 0.6])
cbar = fig.colorbar(cs, cax = cax, ticklocation='left', extend='both', spacing='uniform')
cbar_ax = cbar.ax
cbar_ax.text(1.4, .7, "$B_{\phi} (Tesla)$", rotation = 90, fontsize = 14)



# Make sure aspect ratio preserved
ax.set_aspect('equal')

# Turn off rectangular frame.
ax.set_frame_on(False)

# Turn off axis ticks.
ax.set_xticks([])
ax.set_yticks([])

# Draw a circle around the edge of the plot.
rmax = max(r)
rmin = min(r)
ax.plot(rmax*sin(rad_th),rmax*cos(rad_th),'k')
ax.plot(rmin*sin(rad_th),rmin*cos(rad_th),'k')

ax.plot(r*sin(min(rad_th)),r*cos(min(rad_th)), 'k')
ax.plot(r*sin(max(rad_th)),r*cos(max(rad_th)), 'k')

# Draw a circle in the rad-conv interface
if rad_core:
	ax.plot(0.7*radius*sin(rad_th), 0.7*radius*cos(rad_th),'--k')


anim = animation.ArtistAnimation(fig, ims, interval=50)

if (s == "s"):
        anim.save(str(nm) + '.mp4')
elif (s == "d"):
        plt.show()
else:
        print "invalid option"
        sys.exit()
