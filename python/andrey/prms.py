#---------------------------------------
# Plot the potential Tempearture
#---------------------------------------
from settings import bottom_domain, top_domain
import matplotlib.pyplot as plt
import numpy as np
import eudyn as e

dat = e.rxav()
par = e.parameters()

rad = top_domain - bottom_domain
crad = 0.7 - bottom_domain

cpc = crad / rad

n = par[0]
m = par[1]
l = par[2]
t = dat[27] - 1

cpos = cpc * l

print n,m,l,t

u = dat[1]
v = dat[2]
w = dat[3]

um = np.mean(np.mean(u, axis = 0), axis = 0)
vm = np.mean(np.mean(v, axis = 0), axis = 0)
wm = np.mean(np.mean(w, axis = 0), axis = 0)

ume = np.empty((t,m,l))
vme = np.empty((t,m,l))
wme = np.empty((t,m,l))


for i in range(np.shape(u)[0]):
    for ii in range(np.shape(u)[1]):
        ume[i,ii,:] = um
        vme[i,ii,:] = vm
        wme[i,ii,:] = wm



up = np.subtract(u,ume)
vp = np.subtract(v,vme)
wp = np.subtract(w,wme)

up2 = np.multiply(up,up)
vp2 = np.multiply(vp,vp)
wp2 = np.multiply(wp,wp)

urms2 = up2+vp2+wp2

urms = np.sqrt(urms2)

urms = np.mean(np.mean(urms,axis=0),axis=0)


fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.plot(urms)
ax.set_xlabel('Radius $(r/R_{sun}$)')
ax.set_xticks(np.linspace(0,l,num=9))
#ax.locator_params(nbins=7)
ax.set_xticklabels(np.round(np.linspace(bottom_domain,top_domain,num=9),2))
ax.axvline(x=cpos,linestyle='--')

ax.set_ylabel('Dissipation')

plt.show()
