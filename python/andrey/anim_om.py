from settings import rad_core, r, radius, rad_th, m, l
import numpy as np
from math import pi
import matplotlib.pyplot as plt
import matplotlib
from pylab import sin, cos, meshgrid
from matplotlib import animation
import eudyn as e
import types
import sys


cut = 5. # Degrees to be cut off
good = int(cut*m/180.)
rad_th_good = np.linspace(0+(cut/90)*pi/2., (1 - cut/180)*pi, num = m - 2*good)
fig = plt.figure()


R, TH = meshgrid(r, rad_th_good)

X = R*sin(TH)
Y = R*cos(TH)

data = e.rxav()

n = input("Number of steps to animate =")
print "save or display? (s,d)"
s = raw_input(">  ")

if (s == "s"):
        print "save as:"
        nm = raw_input(">  ")
else:
        pass


u = data[1]
v = data[2]
w = data[3]

u = u[-n::,:,:]
v = v[-n::,:,:]
w = w[-n::,:,:]

ua = np.mean(u, axis=0)

Omega_0 = 2*pi/(28.*24.*3600.)
Omega1 = ua[good:-good, :]/X + Omega_0


lmin = np.amin(Omega1)
lmax = np.amax(Omega1)

ims=[]

ax = fig.add_subplot(111)


for i in range(n):

	U = u[i,:,:]
	V = v[i,:,:]
	W = w[i,:,:]

	Omega = U[good:-good, :]/X + Omega_0


#-----------------------------------------------------------------------------#
# PLOT

	matplotlib.rcParams['contour.negative_linestyle'] = 'solid'

	cs = ax.contourf(X, Y, Omega*1.e9/(2*pi), levels = np.linspace(lmin*1.e9/(2.*pi), lmax*1.e9/(2.*pi), 50), interpolation = 'bicubic', cmap = plt.get_cmap("RdYlBu"),extend = 'both')


        csa = cs.collections

        ims.append(csa)


#	ax.contour(X, Y, Omega*1.e9/(2*pi), 15, interpolation = 'bicubic', colors = "k")
# Add colorbar
cax = fig.add_axes([0.28, 0.22, 0.02, 0.6])
cbar = fig.colorbar(cs, cax = cax, ticklocation='left', extend='both', spacing='uniform')
cbar_ax = cbar.ax
cbar_ax.text(1.1, .55, r"$\Omega \rm (nHz)$", rotation = 90, fontsize = 18)


# Make sure aspect ratio preserved
ax.set_aspect('equal')

# Turn off rectangular frame.
ax.set_frame_on(False)

# Turn off axis ticks.
ax.set_xticks([])
ax.set_yticks([])

# Draw a circle around the edge of the plot.
rmax = max(r)
rmin = min(r)
ax.plot(rmax*sin(rad_th),rmax*cos(rad_th),'k')
ax.plot(rmin*sin(rad_th),rmin*cos(rad_th),'k')

ax.plot(r*sin(min(rad_th)),r*cos(min(rad_th)), 'k')
ax.plot(r*sin(max(rad_th)),r*cos(max(rad_th)), 'k')

# Draw a circle in the rad-conv interface
if rad_core:
        ax.plot(0.7*radius*sin(rad_th), 0.7*radius*cos(rad_th),'--k')


anim = animation.ArtistAnimation(fig, ims, interval=10)

if (s == "s"):
        anim.save(str(nm) + '.mp4')
elif (s == "d"):
        plt.show()
else:
        print "invalid option"
        sys.exit()
