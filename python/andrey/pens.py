#---------------------------------------
# Plot the vorticity
#---------------------------------------
from settings import bottom_domain, top_domain, radius, r, rad_th, rad_th1, rad_phi
import matplotlib.pyplot as plt
import numpy as np
import eudyn as e
from math import pi, sin, cos

#dat = e.rbox()
par = e.parameters()

rad = top_domain - bottom_domain
crad = 0.7 - bottom_domain

cpc = crad / rad

n = par[0]
m = par[1]
l = par[2]
#t = dat[27] - 1

cpos = cpc * l

print n,m,l

#u = dat[1]
#v = dat[2]
#w = dat[3]

# Building Grids with corresponding distances in meters
#-------------------------------------------------------


alx = np.empty((n,m,l))
aly = np.empty((m,l))
alz = r

rad_phi1 = np.linspace(-pi, pi, num=n, endpoint=True)


for i in range(m):
    for ii in range(l):
        aly[i,ii] = r[ii] * rad_th[i]


for i in range(n):
    for ii in range(m):
        for iii in range(l):
            alx[i,ii,iii] = rad_phi[i] * (r[iii] * abs(cos(rad_th1[ii])))



# Taking the derivatives (finite difference)
#---------------------------------------------



dudy = np.empty((n,m,l))
dudz = np.empty((n,m,l))

dvdx = np.empty((n,m,l))
dvdz = np.empty((n,m,l))

dwdx = np.empty((n,m,l))
dwdy = np.empty((n,m,l))


for i in range(n):
    for ii in range(m):
        for iii in range(l):
            if (i == 0):
                dvdx[i,ii,iii] = (v[i+1,ii,iii] - v[i,ii,iii]) / (alx[i+1,ii,iii] - alx[i,ii,iii])
                dwdx[i,ii,iii] = (w[i+1,ii,iii] - w[i,ii,iii]) / (alx[i+1,ii,iii] - alx[i,ii,iii])
            else:
                dvdx[i,ii,iii] = (v[i,ii,iii] - v[i-1,ii,iii]) / (alx[i,ii,iii] - alx[i-1,ii,iii])
                dwdx[i,ii,iii] = (w[i,ii,iii] - w[i-1,ii,iii]) / (alx[i,ii,iii] - alx[i-1,ii,iii])

            if (ii == 0):
                dudy[i,ii,iii] = (u[i,ii+1,iii] - u[i,ii,iii]) / (aly[ii+1,iii] - aly[ii,iii])
                dwdy[i,ii,iii] = (w[i,ii+1,iii] - w[i,ii,iii]) / (aly[ii+1,iii] - alz[ii,iii])
            else:
                dudy[i,ii,iii] = (u[i,ii,iii] - u[i,ii-1,iii]) / (aly[ii,iii] - aly[ii-1,iii])
                dwdy[i,ii,iii] = (w[i,ii,iii] - w[i,ii-1,iii]) / (aly[ii,iii] - alz[ii-1,iii])

            if (iii == 0):
                dudz[i,ii,iii] = (u[i,ii,iii+1] - u[i,ii,iii]) / (alz[iii+1] - alz[iii])
                dvdz[i,ii,iii] = (v[i,ii,iii+1] - v[i,ii,iii]) / (alz[iii+1] - alz[iii])
            else:
                dudz[i,ii,iii] = (u[i,ii,iii] - u[i,ii,iii-1]) / (alz[iii] - alz[iii-1])
                dvdz[i,ii,iii] = (v[i,ii,iii] - v[i,ii,iii-1]) / (alz[iii] - alz[iii-1])





plt.plot(alx[1,10,:])
#plt.contourf(alx[:,50,:])
plt.show()

#x =
#y
#z

#um = np.mean(np.mean(u, axis = 0), axis = 0)
#vm = np.mean(np.mean(v, axis = 0), axis = 0)
#wm = np.mean(np.mean(w, axis = 0), axis = 0)
#
#ume = np.empty((t,m,l))
#vme = np.empty((t,m,l))
#wme = np.empty((t,m,l))
#
#
#for i in range(np.shape(u)[0]):
#    for ii in range(np.shape(u)[1]):
#        ume[i,ii,:] = um
#        vme[i,ii,:] = vm
#        wme[i,ii,:] = wm
#
#
#
#up = np.subtract(u,ume)
#vp = np.subtract(v,vme)
#wp = np.subtract(w,wme)
#
#up2 = np.multiply(up,up)
#vp2 = np.multiply(vp,vp)
#wp2 = np.multiply(wp,wp)
#
#urms2 = up2+vp2+wp2
#
#urms = np.sqrt(urms2)
#
#urms = np.mean(np.mean(urms,axis=0),axis=0)
#
#
#fig = plt.figure()
#ax = fig.add_subplot(1,1,1)
#ax.plot(urms)
#ax.set_xlabel('Radius $(r/R_{sun}$)')
#ax.set_xticks(np.linspace(0,l,num=9))
##ax.locator_params(nbins=7)
#ax.set_xticklabels(np.round(np.linspace(bottom_domain,top_domain,num=9),2))
#ax.axvline(x=cpos,linestyle='--')
#
#ax.set_ylabel('Dissipation')
#
#plt.show()
