#---------------------------------------
# Plot the potential Tempearture
#---------------------------------------

import matplotlib.pyplot as plt
import numpy as np

stra = "./strat.dat"
strat = open(stra, "r")

x=[]
x2=[]
y=[]
y2=[]

fig, pl1 = plt.subplots()

for line in strat:
    x.append(float(line.split()[0]))
    y.append(float(line.split()[5])/(1000000))

pl2 = fig.add_axes([0.37, 0.2, 0.5, 0.5])

x2 = x[20:]
y2 = y[20:]

pl1.plot(x,y)
pl1.set_xlabel('Percent of Solar Radius (r/R)')
pl1.set_ylabel('Potential Temperature ($10^6$ K)')
pl1.set_title('Potential Temperature')

pl2.plot(x2,y2)

#plt.savefig('tplot3.png')
plt.show()
