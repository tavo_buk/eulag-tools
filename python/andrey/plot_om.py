from settings import rad_core, r, radius, rad_th, m, l
from rxav import u, v, w
import numpy as np
from math import pi
import matplotlib.pyplot as plt
import matplotlib
from pylab import sin, cos, meshgrid

cut = 10. # Degrees to be cut off
good = int(cut*m/180.)
rad_th_good = np.linspace(0+(cut/90)*pi/2., (1 - cut/180)*pi, num = m - 2*good)

R, TH = meshgrid(r, rad_th_good)
n = input("Number of steps in mean =")
U = np.mean(u[-n::,:,:], axis = 0)
V = np.mean(v[-n::,:,:], axis = 0)
W = np.mean(w[-n::,:,:], axis = 0)

X = R*sin(TH)
Y = R*cos(TH)

Omega_0 = 2*pi/(28.*24.*3600.)
Omega = U[good:-good, :]/X + Omega_0

#-----------------------------------------------------------------------------#
# PLOT

fig = plt.figure()
ax = fig.add_subplot(111)
matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
Omin = Omega.min()
Omax = Omega.max()
cs = ax.contourf(X, Y, Omega*1.e9/(2*pi), levels = np.linspace(Omin*1.e9/(2.*pi), Omax*1.e9/(2.*pi), 50), interpolation = 'bicubic', cmap = plt.get_cmap("RdYlBu"))
ax.contour(X, Y, Omega*1.e9/(2*pi), 15, interpolation = 'bicubic', colors = "k")
# Add colorbar
cax = fig.add_axes([0.28, 0.22, 0.02, 0.6])
cbar = fig.colorbar(cs, cax = cax, ticklocation='left', extend='neither', spacing='uniform')
cbar_ax = cbar.ax
cbar_ax.text(1.1, .55, r"$\Omega \rm (nHz)$", rotation = 90, fontsize = 18)

# Make sure aspect ratio preserved
ax.set_aspect('equal')

# Turn off rectangular frame.
ax.set_frame_on(False)

# Turn off axis ticks.
ax.set_xticks([])
ax.set_yticks([])

# Draw a circle around the edge of the plot.
rmax = max(r)
rmin = min(r)
ax.plot(rmax*sin(rad_th),rmax*cos(rad_th),'k')
ax.plot(rmin*sin(rad_th),rmin*cos(rad_th),'k')

ax.plot(r*sin(min(rad_th)),r*cos(min(rad_th)), 'k')
ax.plot(r*sin(max(rad_th)),r*cos(max(rad_th)), 'k')

# Draw a circle in the rad-conv interface
if rad_core:
	ax.plot(0.7*radius*sin(rad_th), 0.7*radius*cos(rad_th),'--k')

plt.savefig("ns3_omp.png")

plt.show()


"""
plt.plot(r/radius, Omega[20,:]*1.e9/(2*pi), r/radius, Omega[28,:]*1.e9/(2*pi), r/radius, Omega[32,:]*1.e9/(2*pi))
plt.show()
"""
