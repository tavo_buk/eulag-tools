import numpy as np
from settings import m, l, nxaver, dt, dxaver
import matplotlib.pyplot as plt
from rxav import u, u2, v, v2, w, w2, bx, bx2, by, by2, bz, bz2, the, P

# List with mean fields
fields = [u, v, w, bx, by, bz]

# List with quadratic fields
ql = [u2, v2, w2, bx2, by2, bz2]

# List with quadratic deviations
du2 = np.empty((nxaver, m, l))
dv2 = np.empty((nxaver, m, l))
dw2 = np.empty((nxaver, m, l))
dbx2 = np.empty((nxaver, m, l))
dby2 = np.empty((nxaver, m, l))
dbz2 = np.empty((nxaver, m, l))
qd = [du2, dv2, dw2, dbx2, dby2, dbz2]

for k in range(0, len(fields)):
	# Spatial mean 
	qd[k] = np.mean(ql[k] - np.multiply(fields[k],fields[k]), axis = (1,2))

du2, dv2, dw2, dbx2, dby2, dbz2 = qd

# Turbulent Velocity 
urms = np.power(du2 + dv2 + dw2, 0.5)

# Magnetic field of equipartition 
brms = np.power(dbx2 + dby2 + dbz2, 0.5)

# Spatial mean of Potential temperature and Pressure
mthe = np.mean(the, axis = (1,2))
mP = np.mean(P, axis = (1,2))

year = 365.*24.*3600.  	# Year in seconds
time = np.multiply(dxaver*dt,range(nxaver))/year	# Time in years

plt.figure(1)

#plt.subplot(411)
#plt.plot(time, urms)
#plt.ylabel("Urms")

#plt.subplot(412)
plt.plot(time, brms)
plt.ylabel("brms")

#plt.subplot(413)
#plt.plot(time, mthe)
#plt.ylabel("Mean Potential Temp.")

#plt.subplot(414)
#plt.plot(time, mP)
#plt.ylabel("Mean pressure")

plt.show()
