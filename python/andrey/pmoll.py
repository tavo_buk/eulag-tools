from mpl_toolkits.basemap import Basemap
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import eudyn as e

#
#------------------------------------------------

f = raw_input("Field(u, v, w, bx, by ,bz) = ")

if f == "u":
    g = 0
elif f == "v":
    g = 1
elif f == "w":
    g = 2
elif f == "bx":
    g = 5
elif f == "by":
    g = 6
elif f == "bz":
    g = 7
else:
    print "Nope"
#-------------------------------------------------

data = e.rbox()

t = input("Time Step = ")

#r = input("Radial Step = ")
r = 62

dat = np.transpose(np.array(data[g][t,:,:,r]))

#lmi = np.amin(np.mean(data[g], axis=0))
#lma = np.amax(np.mean(bx, axis=0))

lmin1 = np.amin(dat)
lmax1 = np.amax(dat)

#lmin = ((lmin1) + (lmi))/2
#lmax = ((lmax1) + (lma))/2


ra = np.linspace(-np.pi, np.pi, 128)
dec= np.linspace(-np.pi/2, np.pi/2, 64)
X,Y = np.meshgrid(ra,dec)
RAD = 180/np.pi

m = Basemap(projection='moll',lon_0=0,resolution='c')
#m.contour(X*RAD, Y*RAD, dat, 15, colors='k',latlon=True)
m.contourf(X*RAD, Y*RAD, dat, levels = np.linspace(lmin1, lmax1, 50), cmap=plt.cm.hot,latlon=True)

plt.savefig('rfmoll.png')
plt.show()
