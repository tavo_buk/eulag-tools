import matplotlib.pyplot as plt
import eudyn as e
import numpy as np

data = e.rxav()

par = e.parameters()

ti = (par[12] * par[7]) / (3600 * 24 * 365)

bx = data[16]
by = data[17]
bz = data[18]

sh = np.shape(bx)

rl = int(0.98 * sh[2])


bx  = bx[:,:,rl]

theta = np.linspace(-90, 90, num = sh[1], endpoint = True)
time = np.linspace(0, sh[0]*ti, num = sh[0], endpoint = True)
#
#if fmin is None and fmax is None:
#    fmin = field.min()
#    fmax = field.max()
#else:
#    pass
#
#plt.figure(dpi = 90, facecolor = None, edgecolor = None, linewidth = None, tight_layout = True)
#levels = np.linspace(fmin, fmax, 60, endpoint = True)
#
plt.contourf(time, theta, np.transpose(bx) , interpolation = 'bicubic', cmap = plt.get_cmap("RdYlBu"))

plt.xlabel('Time (Years)', fontsize = 18)
plt.ylabel('Latitude', fontsize = 18)
plt.ticklabel_format(style = "plain", axis = "y")
plt.tick_params(labelsize = 14, pad = 6, width = 2, length = 3)
plt.title('Time-Latitude Diagram of $B_{\phi}$', fontsize = 18)
plt.savefig('ns_tl.png')
plt.show()
