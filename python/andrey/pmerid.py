from settings import rad_core, r, radius, rad_th, m, l
from rxav import u, v, w
import numpy as np
from math import pi
import matplotlib.pyplot as plt
import matplotlib
from pylab import *
from math import pi
from scipy.integrate import cumtrapz, simps
from copy import deepcopy

nn = input("Number of steps in mean =")
U = np.mean(u[-nn::,:,:], axis = 0)
V = np.mean(v[-nn::,:,:], axis = 0)
W = np.mean(w[-nn::,:,:], axis = 0)

# Stream Function
data = np.genfromtxt('strat.dat')

rho_s = data[:, 5]


stream1 = np.ones((m,l))
stream2 = np.ones((m,l))

R1, TH1 = meshgrid(r, rad_th)
c1 = R1**2*sin(TH1)*W
c2 = R1*sin(TH1)*V
for j in range(r.shape[0]):
	c1[:, j] = c1[:, j]*rho_s[j]
	c2[:, j] = c2[:, j]*rho_s[j]
for j in range(l-1):
    for i in range(m-3):
		ii = i + 2
		stream1[i, j] = simps(c1[0:ii, j], rad_th[0:ii])
for j in range(l-3):
    for i in range(m-1):
		jj = j + 2
		stream2[i, j] = simps(c2[i, 0:jj], r[0:jj])

#stream1 = stream1[0:-2,:]

#-----------------------------------------------------------------------------#
# PLOT
cut = 5. # Degrees to be cut off
good = int(cut*m/180.)
if (good < 2):
	good = 2

rad_th_good = np.linspace(0+(cut/90)*pi/2., (1 - cut/180)*pi, num = m - 2*good)

R, TH = meshgrid(r, rad_th_good)

X = R*sin(TH)
Y = R*cos(TH)

W = W[good:-good,:]
V = V[good:-good,:]

fig = plt.figure()
ax = fig.add_subplot(111)
Vmin = V[good:-good,:].min()
Vmax = V[good:-good,:].max()
#levels = np.linspace(-15, 15, 50)
levels = np.linspace(Vmin*0.5, Vmax*0.4, 50)
cs = ax.contourf(X, Y, V, levels = levels, interpolation = 'bicubic', cmap = plt.get_cmap("RdYlBu"),extend='both')


matplotlib.rcParams['contour.negative_linestyle'] = 'dashed'
stream1 = stream1[good:-good,:]

print stream1
#  Stream
ax.contour(X, Y, stream1/X, levels = np.linspace((stream1/X).min(), (stream1/X).max(), 4), colors = "k")
#ax.contour(X, Y, stream2/R, 25, colors = "k")
# Add colorbar
cax = fig.add_axes([0.3, 0.2, 0.02, 0.6])
cbar = fig.colorbar(cs, cax = cax, ticklocation='left', extend='both', spacing='uniform')
cbar_ax = cbar.ax
cbar_ax.text(1.0, .55, r"$u_{\theta} \rm (m/s)$", rotation = 90, fontsize = 18)

# Make sure aspect ratio preserved
ax.set_aspect('equal')

# Turn off rectangular frame.
ax.set_frame_on(False)

# Turn off axis ticks.
ax.set_xticks([])
ax.set_yticks([])

# Draw a circle around the edge of the plot.
rmax = max(r)
rmin = min(r)
ax.plot(rmax*sin(rad_th),rmax*cos(rad_th),'k')
ax.plot(rmin*sin(rad_th),rmin*cos(rad_th),'k')

ax.plot(r*sin(min(rad_th)),r*cos(min(rad_th)), 'k')
ax.plot(r*sin(max(rad_th)),r*cos(max(rad_th)), 'k')

# Draw a circle in the rad-conv interface
if rad_core:
	ax.plot(0.7*radius*sin(rad_th), 0.7*radius*cos(rad_th),'--k')

#plt.savefig("ns3_merid.png", format = "png")
plt.show()
