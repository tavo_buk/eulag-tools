''' T Tauri Settings '''
# Modules:
import numpy as np
from math import pi
import subprocess
import os

# Data files:
fort11 = "./fort.11" 			# Option: None or patch file
averages = "./xaverages.dat" 	# Option: None or patch file
mylist = "./mylist.nml" 		# Option: None or patch file
ee = "./ee.txt"					# Option: None or patch file
MHD = False 					# Option: True or False
rad_core = True					# Option: True or False

''' Simulation settings '''
# Read mylist.nml file:
if mylist is not None:
	file_my = open(mylist, "r")
	data_my = [line.strip() for line in file_my]
	n = int(data_my[2].split()[2][0:-1])		# Number of divisions in phi domain
	m = int(data_my[3].split()[2][0:-1])		# Number of divisions in theta domain
	l = int(data_my[4].split()[2][0:-1])		# number of divisions in radial domain
	Lxyz = int(data_my[5].split()[2][0:-1])		# Zero if Spherical and 1 if is cartesian
	dx = float(data_my[6].split()[2][0:-1])		# Phi step
	dy = float(data_my[7].split()[2][0:-1])		# Theta step
	dz = float(data_my[8].split()[2][0:-1])		# Radial step
	dt = float(data_my[9].split()[2][0:-1])		# Step time
	dslice = int(data_my[10].split()[2][0:-1])	# Time interval to write slices
	dout = int(data_my[11].split()[2][0:-1])	# Time interval to write ee.txt
	dplot = int(data_my[12].split()[2][0:-1])	# Time interval to write fort.11
	dstore = int(data_my[13].split()[2][0:-1])	# Time interval to write forte.9
	dxaver = int(data_my[14].split()[2])		# Time interval to write xaverages.dat
else:
	print "File mylist does not  exist."

subprocess.call("cat ee.txt | grep it= > it.txt", shell = True)
proc = subprocess.Popen(['tail', '-1','it.txt'], stdout = subprocess.PIPE)
it = int(proc.stdout.read().split()[1]) 		# Simulation iterations
os.remove("./it.txt")

nslice = it/dslice  
nout = it/dout  
nplot = it/dplot   
nstore = it/dstore 
nxaver = it/dxaver 

bottom_domain = 0.10 # !!!! DEFINE !!!!

# Read ee.txt file:
if not ee is None:
	file_ee = open("./ee.txt", "r")
	data_ee = [line.strip() for line in file_ee]
	radius = float(data_ee[5].split()[2])*1.e3/bottom_domain # In meters
else:
	print "File ee.txt does not exist."


# Star settings:
sun_mass = 1.98855e30		# In kilograms
star_mass = 0.7*sun_mass 	# In kilograms


# Radial part
top_domain = 0.95
rr = np.linspace(bottom_domain, top_domain, num = l, endpoint = True) 				# Fractional radius
r = np.linspace(bottom_domain*radius, top_domain*radius, num = l, endpoint = True) 	# Radius array

# Angular part
degree_phi = np.linspace(0, 360, num=n, endpoint=True) 	# Phi array
rad_phi = np.linspace(0, 2*pi, num=n, endpoint=True) 	# Phi array
degree_th = np.linspace(0, 180, num=m, endpoint=True) 	# Theta array
rad_th = np.linspace(0, pi, num=m, endpoint=True) 		# Theta array
rad_th1 = np.linspace(-pi/2., pi/2., num=m, endpoint=True) 		# Theta array

