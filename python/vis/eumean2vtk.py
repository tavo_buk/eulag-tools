import visit_writer
from pylab import meshgrid, arange, cos, sin
import eulag as E
import numpy as np
from evtk.hl import gridToVTK
from scipy.ndimage import gaussian_filter

def ntn(fn):
    return getattr(fn, "tolist", lambda x=fn: x)()

del f.the,f.p,f.u,f.v,f.w
# Curvilinear mesh points stored x0,y0,z0,x1,y1,z1,...
g = E.read_grid()
nt = (f.bx.shape)[0]
NX = g.n
NY = g.m
NZ = g.l
dims = (NZ, NY, NX)

pts = []

bxm = f.bx.mean(axis=1)
bym = f.by.mean(axis=1)
bzm = f.bz.mean(axis=1)

bx  = np.zeros([NZ,NY,NX])
by  = np.zeros([NZ,NY,NX])
bz  = np.zeros([NZ,NY,NX])
ux  = np.zeros([NZ,NY,NX])
uy  = np.zeros([NZ,NY,NX])
uz  = np.zeros([NZ,NY,NX])

bphi  = np.zeros([NZ,NY,NX])
bth   = np.zeros([NZ,NY,NX])
brr   = np.zeros([NZ,NY,NX])
u     = np.zeros([NZ,NY,NX])
v     = np.zeros([NZ,NY,NX])
w     = np.zeros([NZ,NY,NX])
the   = np.zeros([NZ,NY,NX])

# Here we define the sphere grid points x, y, z
for i in range(NX):
    for j in range(NY):
       for k in range(NZ):
           x = g.rf[k]*np.sin(g.th[j])*np.cos(g.phi[i])
           y = g.rf[k]*np.sin(g.th[j])*np.sin(g.phi[i])
           z = g.rf[k]*np.cos(g.th[j])
           pts.append(x)
           pts.append(y)
           pts.append(z)

for it in range(0,50):
    print 'frame #', it

    bp = []
    bt = []
    br = []
    bv = []
    
    up = []
    ut = []
    ur = []
    vv = []

    th = []

    for k in range(NZ):
#        for j in range (NY):
        for i in range(NX):
               # scalars
                bphi[k,:,i] = gaussian_filter( bxm[it,:,k],sigma=0.4) 
                bth [k,:,i] = gaussian_filter( bym[it,:,k],sigma=0.4)
                brr [k,:,i] = gaussian_filter( bzm[it,:,k],sigma=0.4)
              
                # vectors
                bx[k,:,i] = N.sin(g.th[:])*N.cos(g.phi[i])*bzm[it,:,k] + N.cos(g.th[:])*N.cos(g.phi[i])*bym[it,:,k] - N.sin(g.phi[i])*bxm[it,:,k]
                by[k,:,i] = N.sin(g.th[:])*N.sin(g.phi[i])*bzm[it,:,k] + N.cos(g.th[:])*N.sin(g.phi[i])*bym[it,:,k] + N.cos(g.phi[i])*bxm[it,:,k]
                bz[k,:,i] =                 N.cos(g.th[:])*bzm[it,:,k] -                 N.sin(g.th[:])*bym[it,:,k] 

    index = 0
    
    for i in range(NX):
        for j in range(NY):
           for k in range(NZ):
               # writing scalars B
               bp.append(ntn(bphi[k,j,i]))
               bt.append(ntn(bth [k,j,i]))
               br.append(ntn(brr [k,j,i]))
               #writing vector B
               bv.append(bx[k,j,i])
               bv.append(by[k,j,i])
               bv.append(bz[k,j,i])
               index = index + 1

    # Pass data to visit_writer to write a binary VTK file.
    vars1 = (("Bphi", 1, 1, bp), ("B_vec", 3, 1, bv))
    vars2 = (("B_phi", 1, 1, bp),("B_th", 1, 1, bt),("B_r", 1, 1, br) )
    
    visit_writer.WriteCurvilinearMesh("Geulag_Bvec"+"%03d"%it+".vtk", 1, dims, pts, vars1)
    visit_writer.WriteCurvilinearMesh("Geulag_Scalars"+"%03d"%it+".vtk", 0, dims, pts, vars2)
    del  bp,bt,br,bv,up,ut,ur,vv,th
