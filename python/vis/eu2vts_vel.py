#import visit_writer
from pylab import meshgrid, arange, cos, sin
import eulag as E
import numpy as np
#from evtk.hl import gridToVTK
import evtk
import numpy as N
def ntn(fn):
    return getattr(fn, "tolist", lambda x=fn: x)()

# READING EULAG DATA and getting rid of unwanted variables
#f = E.read_f11('fort.11')
#GM:
gridToVTK = evtk.hl.gridToVTK
f = E.read_f11('f11')
del f.bx,f.by,f.bz,f.the,f.p

# Curvilinear mesh points stored x0,y0,z0,x1,y1,z1,...
g = E.read_grid()
nt = (f.u.shape)[0]
NX = g.n
NY = g.m
NZ = g.l
dims = (NZ, NY, NX)

pts = []
vx  = np.zeros([NZ,NY,NX])
vy  = np.zeros([NZ,NY,NX])
vz  = np.zeros([NZ,NY,NX])

x = np.zeros((NZ, NY, NX))
y = np.zeros((NZ, NY, NX))
z = np.zeros((NZ, NY, NX))

vphi  = np.zeros([NZ,NY,NX])
vth   = np.zeros([NZ,NY,NX])
vrr   = np.zeros([NZ,NY,NX])

# Here we define the sphere grid points x, y, z
for i in range(NX):
    #for j in range(NY):
       for k in range(NZ):
           x[k, :, i]= g.rf[k]*np.sin(g.th)*np.cos(g.phi[i])
           y[k, :, i]= g.rf[k]*np.sin(g.th)*np.sin(g.phi[i])
           z[k, :, i]= g.rf[k]*np.cos(g.th)
           pts.append(x)
           pts.append(y)
           pts.append(z)
#---------------------------------
##GM:
#casa = 24    #Counter for the counters 13
## Counter over time
#icounter = 786 + 9*casa

#nouts = 1000
##GM: needed to use fort.11.x withou concatanating
#second_counter = casa*9
#for it in range(second_counter,nouts):
#-----------------------------------

icounter = 0 
nouts = 100
for it in range(icounter,nouts):


    print('frame #', icounter)
    print('output #', it)

    vp = []
    vt = []
    vr = []
    vv = []

    for k in range(NZ):
#        for j in range (NY):
        for i in range(NX):
               # for scalars
                vphi[k,:,i] = f.u[it,i,:,k]
                vth [k,:,i] = f.v[it,i,:,k]
                vrr [k,:,i] = f.w[it,i,:,k]
              
                # for vectors
                vx[k,:,i] = N.sin(g.th[:])*N.cos(g.phi[i])*f.w[it,i,:,k] + N.cos(g.th[:])*N.cos(g.phi[i])*f.v[it,i,:,k] - N.sin(g.phi[i])*f.u[it,i,:,k]
                vy[k,:,i] = N.sin(g.th[:])*N.sin(g.phi[i])*f.w[it,i,:,k] + N.cos(g.th[:])*N.sin(g.phi[i])*f.v[it,i,:,k] + N.cos(g.phi[i])*f.u[it,i,:,k]
                vz[k,:,i] =                 N.cos(g.th[:])*f.w[it,i,:,k] -                 N.sin(g.th[:])*f.v[it,i,:,k] 
    index = 0
    
    for i in range(NX):
        for j in range(NY):
           for k in range(NZ):
               # writing scalars V 
               vp.append(ntn(vphi[k,j,i]))
               vt.append(ntn(vth [k,j,i]))
               vr.append(ntn(vrr [k,j,i]))
               #writing vector V
               vv.append(vx[k,j,i])
               vv.append(vy[k,j,i])
               vv.append(vz[k,j,i])

               index = index + 1

    # Writing using visit_writer to create a binary VTK file.
    # vars1 has individual components and also the vector field
    vars1 = (("Vphi", 1, 1, vp),("Vth", 1, 1, vt),("Vr", 1, 1, vr),("V_vec", 3, 1, vv))
    #visit_writer.WriteCurvilinearMesh("eulag_BT"+"%03d"%icounter+".vtk", 1, dims, pts, vars1)
    filename = 'velocity_{0:04d}'.format(it)
    #gridToVTK(filename, x, y, z, cellData={"vphi" : f.u[it,...], "vtheta" : f.v[it,...], "vr" : f.w[it,...]})
    gridToVTK(filename, x, y, z, pointData={ "Vphi_sca" : vphi})

    icounter += 1
    del  vp,vt,vr,vv
