import numpy as N
import matplotlib.pylab as P
from   mpl_toolkits.basemap import Basemap

''' This routine projects a 2D field, U, in the
    mollweide projection.  In case a second field, V,
    is provided, it may overplot the field lines
    Parameters:
    phi: longitude
    th:  latitude
'''
def eumoll(phi,th,U,V,it=0):
    
    if (U.size == V.size): print("Making field lines")
    
    RAD = 180./N.pi
    PHI, TH = N.meshgrid((phi - N.pi), (th - N.pi/2 ))
    col = 2 * N.log(N.hypot(U.T, V.T))
    speed = N.sqrt((U.T)**2 + (V.T)**2)
    # Create figure
    fig = P.figure()
    ax = fig.add_subplot(111, projection="mollweide")
    ax.xaxis.set_major_formatter(P.NullFormatter())
    ax.yaxis.set_major_locator(P.NullLocator())
#    maps = Basemap(projection = 'moll', lon_0 = 0, resolution = 'c',rsphere=1.2)
#    ax.contourf(PHI, TH, N.rot90(U, 1),64, cmap = P.get_cmap("RdGy"), latlon = True)
    ax.streamplot(PHI,TH,N.rot90(U,1),N.rot90(V,1),color=U.T,linewidth=4*speed/speed.max(),cmap=P.cm.spectral,density=2, arrowstyle='->', arrowsize=3.5)
    P.tight_layout()
    fig_name = "bfield"+"%03d" % it+".png"
    P.savefig(fig_name,dpi=300,bbox_inches='tight')
    P.close(fig)
    return None

def mollmov(f,g):
    nt = f.bx.shape[0]
    for it in range(nt):
        print ('out file number ', it)
        eumoll(g.phi,g.th,f.bx[it,:,:,30],f.by[it,:,:,30],it)




    # Colorbar
#    tick = N.linspace(level[0], level[-1], num = 2, endpoint = True)
#    cbar = P.colorbar(cs, ticks = tick, format = '%1.0f', orientation = 'horizontal', fraction = 0.03, pad = 0.08)
#    cbar.ax.set_xlabel(r"$v_\mathrm{r} (\mathrm{r = %1.2f \ R_\mathrm{s}}) [\mathrm{m/s}]$" %(g.rf[ir])) #, fontsize = 35)
#    cbar.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 2, length = 6) #  labelsize = 30, 
