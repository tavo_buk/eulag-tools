import visit_writer
from pylab import meshgrid, arange, cos, sin
import eulag as E
import numpy as np
from evtk.hl import gridToVTK

def ntn(fn):
    return getattr(fn, "tolist", lambda x=fn: x)()


# Curvilinear mesh points stored x0,y0,z0,x1,y1,z1,...
g = E.read_grid()
nt = (f.bx.shape)[0]
NX = g.n
NY = g.m
NZ = g.l
dims = (NZ, NY, NX)

pts = []

bx  = np.zeros([NZ,NY,NX])
by  = np.zeros([NZ,NY,NX])
bz  = np.zeros([NZ,NY,NX])
ux  = np.zeros([NZ,NY,NX])
uy  = np.zeros([NZ,NY,NX])
uz  = np.zeros([NZ,NY,NX])

bphi  = np.zeros([NZ,NY,NX])
bth   = np.zeros([NZ,NY,NX])
brr   = np.zeros([NZ,NY,NX])
u     = np.zeros([NZ,NY,NX])
v     = np.zeros([NZ,NY,NX])
w     = np.zeros([NZ,NY,NX])
the   = np.zeros([NZ,NY,NX])

# Here we define the sphere grid points x, y, z
for i in range(NX):
    for j in range(NY):
       for k in range(NZ):
           x = g.rf[k]*np.sin(g.th[j])*np.cos(g.phi[i])
           y = g.rf[k]*np.sin(g.th[j])*np.sin(g.phi[i])
           z = g.rf[k]*np.cos(g.th[j])
           pts.append(x)
           pts.append(y)
           pts.append(z)

for it in range(6,51):
    print 'frame #', it

    bp = []
    bt = []
    br = []
    bv = []
    
    up = []
    ut = []
    ur = []
    vv = []

    th = []

    for k in range(NZ):
#        for j in range (NY):
        for i in range(NX):
               # scalars
                bphi[k,:,i] = f.bx[it,i,:,k]
                bth [k,:,i] = f.by[it,i,:,k]
                brr [k,:,i] = f.bz[it,i,:,k]
                u   [k,:,i] = f.u[it,i,:,k]
                v   [k,:,i] = f.v[it,i,:,k]
                w   [k,:,i] = f.w[it,i,:,k]
                the [k,:,i] = f.the[it,i,:,k]
              
                # vectors
                bx[k,:,i] = N.sin(g.th[:])*N.cos(g.phi[i])*f.bz[it,i,:,k] + N.cos(g.th[:])*N.cos(g.phi[i])*f.by[it,i,:,k] - N.sin(g.phi[i])*f.bx[it,i,:,k]
                by[k,:,i] = N.sin(g.th[:])*N.sin(g.phi[i])*f.bz[it,i,:,k] + N.cos(g.th[:])*N.sin(g.phi[i])*f.by[it,i,:,k] + N.cos(g.phi[i])*f.bx[it,i,:,k]
                bz[k,:,i] =                 N.cos(g.th[:])*f.bz[it,i,:,k] -                 N.sin(g.th[:])*f.by[it,i,:,k] 

                ux[k,:,i] = N.sin(g.th[:])*N.cos(g.phi[i])*f.w[it,i,:,k] + N.cos(g.th[:])*N.cos(g.phi[i])*f.v[it,i,:,k] - N.sin(g.phi[i])*f.u[it,i,:,k]
                uy[k,:,i] = N.sin(g.th[:])*N.sin(g.phi[i])*f.w[it,i,:,k] + N.cos(g.th[:])*N.sin(g.phi[i])*f.v[it,i,:,k] + N.cos(g.phi[i])*f.u[it,i,:,k]
                uz[k,:,i] =                 N.cos(g.th[:])*f.w[it,i,:,k] -                 N.sin(g.th[:])*f.v[it,i,:,k] 


    index = 0
    
    for i in range(NX):
        for j in range(NY):
           for k in range(NZ):
               # writing scalars B
               bp.append(ntn(bphi[k,j,i]))
               bt.append(ntn(bth [k,j,i]))
               br.append(ntn(brr [k,j,i]))
               # writing scalars B
               up.append(ntn(u[k,j,i]))
               ut.append(ntn(v[k,j,i]))
               ur.append(ntn(w[k,j,i]))
               th.append(ntn(the[k,j,i]))
    
               #writing vector B
               bv.append(bx[k,j,i])
               bv.append(by[k,j,i])
               bv.append(bz[k,j,i])
               #writing vector V
               vv.append(ux[k,j,i])
               vv.append(uy[k,j,i])
               vv.append(uz[k,j,i])
    
               
               index = index + 1

    
    # Pass data to visit_writer to write a binary VTK file.
    vars1 = (("Bphi", 1, 1, bp), ("B_vec", 3, 1, bv))
    vars2 = (("U", 1, 1, up),("V", 1, 1, ut),("W", 1, 1, ur) )
    
    visit_writer.WriteCurvilinearMesh("eulag_Bvec"+"%03d"%it+".vtk", 1, dims, pts, vars1)
    visit_writer.WriteCurvilinearMesh("eulag_Scalars"+"%03d"%it+".vtk", 0, dims, pts, vars2)
    del  bp,bt,br,bv,up,ut,ur,vv,th
