#import visit_writer
from pylab import meshgrid, arange, cos, sin
import eulag as E
import numpy as np
#from evtk.hl import gridToVTK
import evtk
import numpy as N
def ntn(fn):
    return getattr(fn, "tolist", lambda x=fn: x)()

# READING EULAG DATA and getting rid of unwanted variables
#f = E.read_f11('fort.11')
#GM:
gridToVTK = evtk.hl.gridToVTK
f = E.read_f11('fort.11.1')
del f.u,f.v,f.w,f.the,f.p

# Curvilinear mesh points stored x0,y0,z0,x1,y1,z1,...
g = E.read_grid()
nt = (f.bx.shape)[0]
NX = g.n
NY = g.m
NZ = g.l
dims = (NZ, NY, NX)

pts = []
bx  = np.zeros([NZ,NY,NX])
by  = np.zeros([NZ,NY,NX])
bz  = np.zeros([NZ,NY,NX])

x = np.zeros([NZ, NY, NX])
y = np.zeros([NZ, NY, NX])
z = np.zeros([NZ, NY, NX])

bphi  = np.zeros([NZ,NY,NX])
bth   = np.zeros([NZ,NY,NX])
brr   = np.zeros([NZ,NY,NX])

# Here we define the sphere grid points x, y, z
xr=N.linspace(g.rf[0],g.rf[-1],num=g.l+1)
yr=N.linspace(g.th[0],g.th[-1],num=g.m+1)
zr=N.linspace(g.phi[0],g.phi[-1],num=g.n+1)

for i in range(NX):
    #for j in range(NY):
       for k in range(NZ):
           #x[k, :, i]= g.rf[k]*np.sin(g.th[::-1])*np.cos(g.phi[i])
           #y[k, :, i]= g.rf[k]*np.sin(g.th[::-1])*np.sin(g.phi[i])
           #z[k, :, i]= g.rf[k]*np.cos(g.th[::-1])
           x[k, :, i]= g.rf[k]*np.sin(g.th)*np.cos(g.phi[i])
           y[k, :, i]= g.rf[k]*np.sin(g.th)*np.sin(g.phi[i])
           z[k, :, i]= g.rf[k]*np.cos(g.th)

           pts.append(x)
           pts.append(y)
           pts.append(z)
# Create grid for VTK output
#x=N.linspace(g.rf[0],g.rf[-1],num=g.l+1)
#y=N.linspace(g.th[0],g.th[-1],num=g.m+1)
#z=N.linspace(g.phi[0],g.phi[-1],num=g.n+1)

#---------------------------------
##GM:
#casa = 24    #Counter for the counters 13
## Counter over time
#icounter = 786 + 9*casa

#nouts = 1000
##GM: needed to use fort.11.x withou concatanating
#second_counter = casa*9
#for it in range(second_counter,nouts):
#-----------------------------------

icounter = 0
nouts = 1000
for it in range(icounter,nouts):


    print('frame #', icounter)
    print('output #', it)

    bp = []
    bt = []
    br = []
    bv = []

    for k in range(NZ):
#        for j in range (NY):
        for i in range(NX):
               # for scalars
                bphi[k,:,i] = f.bx[it,i,:,k] #- f.bx[0,i, :, k]      # Done to calculate the difference in the field
                bth [k,:,i] = f.by[it,i,:,k]
                brr [k,:,i] = f.bz[it,i,:,k]
              
                # for vectors
                bx[k,:,i] = N.sin(g.th[:])*N.cos(g.phi[i])*f.bz[it,i,:,k] + N.cos(g.th[:])*N.cos(g.phi[i])*f.by[it,i,:,k] - N.sin(g.phi[i])*f.bx[it,i,:,k]
                by[k,:,i] = N.sin(g.th[:])*N.sin(g.phi[i])*f.bz[it,i,:,k] + N.cos(g.th[:])*N.sin(g.phi[i])*f.by[it,i,:,k] + N.cos(g.phi[i])*f.bx[it,i,:,k]
                bz[k,:,i] =                 N.cos(g.th[:])*f.bz[it,i,:,k] -                 N.sin(g.th[:])*f.by[it,i,:,k] 
    index = 0
    
    for i in range(NX):
        for j in range(NY):
           for k in range(NZ):
               # writing scalars B 
               bp.append(ntn(bphi[k,j,i]))
               bt.append(ntn(bth [k,j,i]))
               br.append(ntn(brr [k,j,i]))
               #writing vector B 
               bv.append(bx[k,j,i])
               bv.append(by[k,j,i])
               bv.append(bz[k,j,i])

               index = index + 1

    # Writing using visit_writer to create a binary VTK file.
    # vars1 has individual components and also the vector field
    vars1 = (("Vphi", 1, 1, bp),("Vth", 1, 1, bt),("Vr", 1, 1, br),("V_vec", 3, 1, bv))
    #visit_writer.WriteCurvilinearMesh("eulag_BT"+"%03d"%icounter+".vtk", 1, dims, pts, vars1)
    filename = 'mag_field_{0:04d}'.format(it)
    #gridToVTK(filename, x, y, z, cellData={"vphi" : f.u[it,...], "vtheta" : f.v[it,...], "vr" : f.w[it,...]})
    #gridToVTK(filename, x, y, z, cellData={"bphi" : N.array(bp), "bth": N.array(bt), "br": N.array(br)})
    gridToVTK(filename, x, y, z, pointData={"B_vec" : (bx, by, bz), "Bphi_scal": bphi})
    #gridToVTK(filename, x, y, z, pointData={"Bphi":bphi})
    icounter += 1 
    del  bp,bt,br,bv
