import numpy as N
import matplotlib.pylab as P
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc                  

# PLOT STYLE #                                                                                                         
matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 24}                         
rc('font', **font)                            
rc('xtick', labelsize = 22)                   
rc('ytick', labelsize = 22)                   
                                              
legend = {'fontsize': 16}                     
rc('legend',**legend)                         
axes = {'labelsize': 24}                      
rc('axes', **axes)                            
rc('mathtext',fontset='cm')                   
#use this, but at the expense of slowdown of rendering
rc('text', usetex = True)                     
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]
################################################################################

R = 6.96e8
LCZ  = 0.1*R
LNSL = (0.1*R)
LTAC = (0.1*R)

hd_alpha_NSL = N.abs(hd_alpha_k_NSL) # + alpha_m_NSL)
hd_alpha_CZ =  N.abs(hd_alpha_k_CZ ) # + alpha_m_CZ)
hd_alpha_TAC = N.abs(hd_alpha_k_TAC) # + alpha_m_TAC)

hd_calp_NSL = LNSL*hd_alpha_NSL          /hd_eta_NSL
hd_comr_NSL = LNSL**2*N.abs(hd_omr_NSL)  /hd_eta_NSL
hd_comt_NSL = LNSL**2*N.abs(hd_omth_NSL) /hd_eta_NSL
hd_calp_CZ  = LCZ*hd_alpha_CZ            /hd_eta_CZ
hd_comr_CZ  = LCZ**2*hd_omr_CZ           /hd_eta_CZ
hd_comt_CZ  = LCZ**2*hd_omth_CZ          /hd_eta_CZ
hd_calp_TAC = LTAC*hd_alpha_TAC          /hd_eta_TAC  
hd_comr_TAC = LTAC**2*N.abs(hd_omr_TAC)  /hd_eta_TAC
hd_comt_TAC = LTAC**2*N.abs(hd_omth_TAC) /hd_eta_TAC



