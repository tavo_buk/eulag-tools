import numpy as N
import matplotlib.pylab as P
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc                  

# PLOT STYLE #                                                                                                         
matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 24}                         
rc('font', **font)                            
rc('xtick', labelsize = 24)                   
rc('ytick', labelsize = 24)                   
                                              
legend = {'fontsize': 20}                     
rc('legend',**legend)                         
axes = {'labelsize': 24}                      
rc('axes', **axes)                            
rc('mathtext',fontset='cm')                   
#use this, but at the expense of slowdown of rendering
rc('text', usetex = True)                     
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]
################################################################################

mu0 = 4.*N.pi*1e-7
rho_mean = 130.1
ek = rho_mean*.5*urms**2
mu0 = 4.*N.pi*1e-7

ebx_TAC = 0.5* bx_TAC**2/mu0 
ebp_TAC = 0.5* bp_TAC**2/mu0 
eb_TAC  = 0.5*(bx_TAC**2+bp_TAC**2)/mu0 

ebx_CZ = 0.5* bx_CZ**2/mu0 
ebp_CZ = 0.5* bp_CZ**2/mu0 
eb_CZ  = 0.5*(bx_CZ**2+bp_CZ**2)/mu0 

eb_TOT = (eb_TAC + eb_CZ + eb_NSL)/3.

line15 = -1.5*N.ones(20)
line10 = -1.0*N.ones(20)
xRo1 = N.linspace(-0.35,0.15,20) 
xRo2 = N.linspace( 0.23,.8,20) 

#P.title("Tachocline magnetic fields --  RC models")

fig,ax=P.subplots()
ax.scatter(N.log10(ro1/(2.*N.pi)),N.log10(ebx_TAC/ek),marker=(5,0), c='b',label=r'$\log (\langle e_{B_{\phi}  }\rangle^{\rm TAC}/e_{\rm k}) $',s=100)
ax.scatter(N.log10(ro1/(2.*N.pi)),N.log10(ebp_TAC/ek),marker='o', c='r',  label=r'$\log (\langle e_{B_{\rm p} }\rangle^{\rm TAC}/e_{\rm k}) $',s=100)
#ax.scatter(N.log10(ro1/(2.*N.pi)),N.log10(eb_TAC/ek),marker='o', c='k',  label=r'$\log (\langle B_{\rm p} [T]\rangle^{\rm TAC}) $',s=80)
ax.scatter(N.log10(ro1/(2.*N.pi)),N.log10(eb_TOT/ek),marker='o', c='k',    label=r'$\log (e_{B}/e_{\rm k}) $',s=50)
P.plot(N.log10(ro1/(2.*N.pi)),N.log10(eb_TOT/ek),c='k',linestyle='dotted')
#P.plot(N.log10(ro),N.log10(0.017*D2_aor),'--',c='r')
#ax.scatter(N.log10(ro[:]),N.log10(btot[:]),marker=(5,0), c='k',label=r'$\log (B_{\rm p} [T])|_{\rm tac} $',s=100)
#ax.scatter(N.log10(ro1[:]),N.log10((comr_TAC+comt_TAC)*bp_CZ),marker=(5,0),c='orange',label=r'$\log ({C^{\rm nsl}_{\Omega_r} B^{\rm cz}_{p}})$',s=100,alpha=0.3)
#ax.scatter(N.log10(ro1[:]),N.log10((calp_CZ)*bx_TAC[:]),marker='o',c='c',label=r'$\log ({C^{\rm cz}_{\alpha} B^{\rm nsl}_{\phi}})$',s=100,alpha=0.3)
P.plot(xRo1,2.5*xRo1-0.37,'--',c='k')
#P.plot(xRo2,-1.9*xRo2-0.05,'--',c='k')
ax.text(-0.35,-0.89,r"${\rm Ro}^{2.5}$")

# AXIS

xini = -0.55
xend =  0.35
yini = -2.4
yend =  0.2
P.ylim(yini,yend)
ax.yaxis.set_ticks(N.linspace(yini,yend,5))
ax.yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
P.xlim(xini,xend)
ax.xaxis.set_ticks(N.linspace(xini,xend,5))
ax.xaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
P.title(r'{\bf (a)}',loc='left',fontsize=20)
P.xlabel(r'${\rm \log Ro}$')
P.legend(loc=3,scatterpoints=1,ncol=1)
P.tight_layout()
P.savefig("BtacRo.eps",format='eps',dpi=400,bbox_inches='tight')
P.show()


