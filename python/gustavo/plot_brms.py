from eulag import *
from sys import exit
import matplotlib.pylab as P
import numpy as N

#restoring time series files and plotting
bmean = N.zeros(8)

P.figure(1)
P.subplot(211)

root_dir = '/home/DATA/Solar/TAC/'
b, u, t  = N.load(root_dir+'tac-7-rf/'+'ts.npy')
P.plot(t,b)
bmean[0] = N.mean(b[-100:])
b, u, t  = N.load(root_dir+'tac-14-rf/'+'ts.npy')
P.plot(t,b)
bmean[1] = N.mean(b[-100:])
b, u, t  = N.load(root_dir+'tac-21-rf/'+'ts.npy')
P.plot(t,b)
bmean[2] = N.mean(b[-100:])
b, u, t  = N.load(root_dir+'tac-28-rf/'+'ts.npy')
P.plot(t,b)
bmean[3] = N.mean(b[-100:])
b, u, t  = N.load(root_dir+'tac-35-rf/'+'ts.npy')
P.plot(t,b)
bmean[4] = N.mean(b[-100:])
b, u, t  = N.load(root_dir+'tac-56-rf/'+'ts.npy')
P.plot(t,b)
bmean[5] = N.mean(b[-100:])
b, u, t  = N.load(root_dir+'tac-112-rf/'+'ts.npy')
P.plot(t,b)
bmean[6] = N.mean(b[-100:])
b, u, t  = N.load(root_dir+'tac-224-rf/'+'ts.npy')
P.plot(t,b)
bmean[7] = N.mean(b[-100:])

P.yscale('log') 
P.ylim([5e-6,1.])
P.ylabel('$b_{rms}$')
P.xlabel('Time [yr]')
P.ylabel('$<B>_{rms}$')
P.legend(['7', '14', '21', '28', '35', '56', '112', '224'],loc=4)

P.subplot(212)
P.scatter(['7', '14', '21', '28', '35', '56', '112', '224'],N.log10(bmean))
P.show()
