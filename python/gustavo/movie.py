import numpy as N
import matplotlib.pylab as P
import eulag as E
from scipy.integrate import simps,cumtrapz
from scipy.ndimage.filters import gaussian_filter

def tabulated_int(a,x):
    na = a.size
    tint = N.zeros_like(a)
    for i in range(na-1,0,-1):
        ii = na - i
        tint[i] = simps(a[-ii:],x[-ii:])
    tint[0] = tint[1]
    return -tint

g = E.read_grid()
nt = f.time.size
year = 365.*24.*3600.

bphi = gaussian_filter(f.bx,0.7)
bth  = gaussian_filter(f.by,0.7)
br   = gaussian_filter(f.bz,0.7)
bpol = N.sqrt(bth**2 + br**2)

tinit = 1500

lev1 = (N.abs(bphi[tinit:,...])).max()
lev2 = (N.abs(bpol[tinit:,...])).max()

ruthsinth = N.zeros((g.m,g.l))
stream = N.zeros((nt,g.m,g.l))

i = 0
for it in range(tinit,nt):
    ursinth = N.sin(g.th)*br[it,:,-1]
    cte = tabulated_int(ursinth,g.th)
    
    for j in range(g.m):
        for k in range(g.l):
            ruthsinth[j,k] = bth[it,j,k]*g.rsinth[j,k]
    for j in range(g.m):
        stream[it,j,:]  = -tabulated_int(ruthsinth[j,:],g.r) + g.r[-1]**2*cte[j]

    E.merid_simple(bphi[it,:,:],lev1,stream[it,:,:],lev2,time=f.time[it]/year,i=i,adjust_color = (0.98))
    i = i+1

