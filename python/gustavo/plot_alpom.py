import eulag as E
import matplotlib.pylab as P
import numpy as N

root = '/storage/TAC/'
dir = ['tac-7-rf','tac-14-rf', 'tac-21-rf', 'tac-28-rf', 
      'tac-35-rf', 'tac-42-rf','tac-49-rf', 'tac-56-rf','tac-63-rf']

lab = ['RC07', 'RC14', 'RC21', 'RC28', 'RC35', 'RC42', 'RC49', 'RC56', 'RC63']
col = ['r','g','b','k','orange','c','m','k', 'y']

nd = len(dir)
file1 = '/alphaomega.npy'
file2 = '/alpha.npy'
L = 0.05*6.96e8

g = E.read_grid()
ir1, = N.where((g.rf > 0.9) & (g.rf < 0.96))
ir2, = N.where((g.rf > 0.63) & (g.rf < 0.74))
pr = N.array([7.,14.,21.,28.,35.,42.,49.,56.,63.])

d1n = []
d2n = []
d1t = []
d2t = []

for id in range (nd): 
    print 'directory: ',  dir[id]
    alpha_k, alpha_m, etat_r = N.load(root+dir[id]+file2)
    a,omr,omt = N.load(root+dir[id]+file1)
    tau_d = L**2/eta_NSL[id]
    ca  = (alpha_k+alpha_m)*tau_d/L
    cor = omr*tau_d
    cot = omt*tau_d
    d1n.append(N.sqrt((ca[:,ir1]**2).mean())*N.sqrt((cor[:,ir1]**2).mean()))
    d2n.append(N.sqrt((ca[:,ir1]**2).mean())*N.sqrt(((cot+cor)[:,ir1]**2).mean()))
    d1t.append(N.sqrt((ca[:,ir2]**2).mean())*N.sqrt((cor[:,ir2]**2).mean()))
    d2t.append(N.sqrt((ca[:,ir2]**2).mean())*N.sqrt((cot[:,ir2]**2).mean()))

d1n = N.array(d1n)
d1t = N.array(d1t)
d2n = N.array(d2n)
d2t = N.array(d2t)


P.scatter(N.log10(ro1[:]),N.log10(bx_NSL[:]) ,s=100, c='r')
P.scatter(N.log10(ro1[:]),N.log10(bp_NSL[:]) ,s=100, c='b')
P.scatter(N.log10(ro1[:]),N.log10(d1n[:]) ,s=100, c='k')
P.scatter(N.log10(ro1[:]),N.log10(d2n[:]) ,s=100, c='g')
#P.scatter(N.log10(ro1[:]),N.log10(D3_NSL[:]) ,s=100, c='b')
#P.scatter(N.log10(ro1[:]),N.log10(D4_NSL[:]) ,s=100, c='k')
P.show()

