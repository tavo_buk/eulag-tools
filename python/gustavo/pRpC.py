import numpy as N
import matplotlib.pylab as P
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc                  

# PLOT STYLE #                                                                                                         
                              
# Choose Computer Modern Roman fonts by default
matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 24}                         
rc('font', **font)                            
rc('xtick', labelsize = 22)                   
rc('ytick', labelsize = 22)                   
                                              
legend = {'fontsize': 10}                     
rc('legend',**legend)                         
axes = {'labelsize': 24}                      
rc('axes', **axes)                            
rc('mathtext',fontset='cm')                   
#use this, but at the expense of slowdown of rendering
rc('text', usetex = True)                     
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]
################################################################################
year = 365.*24.*3600.

FG_pRotI = N.array([8.5,11.4,15.0,9.7,14.0,31.,12.3,22.5,22.7,13.9,29.8,10.9])
FG_pCycI = N.array([1.6,1.7,2.5,5.9,3.6,7.3,9.6,19.2,7.1,2.6,7.4,1.5])

FG_pRotA = N.array([7.8,9.2,11.4,9.7,14.0,12.3,11.4,13.9])
FG_pCycA = N.array([9.1,5.6,12.2,12.2,12.9,16.6,10.9,16.9])


K_pRotA = N.array([11.1,18.5, 21.1,21.0, 19.9])
K_pCycA = N.array([12.7,12.4,17.4,21.0,15.5])

K_pRotI = N.array([44.,38.5,35.2,48.0,11.1,43.0,48.0,40.2,36.2,21.1,36.4,19.9,42.4,35.4,37.8,42.0,43.0])
K_pCycI = N.array([13.8,8.6,9.6,13.2,2.9,10.1,11.1,8.2,8.1,4.0,7.0,5.1,15.8,7.3,11.7,21.1,10.2])

sun_pRot = N.array([25.4,25.4,25.4])
sun_pCyc = N.array([2.,11.1,80.])


spRot = N.array([7.,14.,21.,24.,28.,35.,42.,49.])
spCyc = N.array([3.0,9.58,30.1,16.3, 16.3,19.1,22.9,26.2])


spRot2 = N.array([7.,14.,28.,35.])
spCyc2 = N.array([2.79,9.37,15.94,20.4])

pRot = N.arange(7.,45.,1.)

# theoretical approach 
# approach 1
rm = 0.73*6.96e8
dodr = omr_TAC_p/rm
dodt = omth_TAC_p/rm
#spCyct1 = 1./N.sqrt(N.abs(ca_TAC_p*comr_TAC_p))
#spCyct2 = 1./N.sqrt(N.abs(ca_TAC_p*comt_TAC_p))
spCyct1 = 1./N.sqrt(N.abs(dyr_num_period))
spCyct2 = 1./N.sqrt(N.abs(dyp_num_period))
spCyct3 = 6.96e8**2/eta_CZ/year
spCyct4 = bx_TAC_p
spCyct5 = 1./N.sqrt(N.abs(dyr_num_period_nsl))
spCyct6 = 1./N.sqrt(N.abs(dyp_num_period_nsl))

c1 = spCyc[3]/spCyct1[2]   
c2 = spCyc[3]/spCyct2[2]
c3 = spCyc[3]/spCyct3[2]
c4 = spCyc[3]/bx_TAC_p[2]
c5 = spCyc[0]/spCyct5[2]

#P.scatter(FG_pRotI,FG_pCycI,c='r',marker=(5,1),label='Inactive G and F',s=95,alpha=0.5)
#P.scatter(FG_pRotA,FG_pCycA,c='b',marker=(5,1),label='Active G and F',s=95,alpha=0.5)
#P.scatter(K_pRotI,K_pCycI,c='r',marker=(5,2),label='Inactive K',s=95,alpha=0.5)
#P.scatter(K_pRotA,K_pCycA,c='b',marker=(5,2),label='Active K',s=95,alpha=0.5)
#P.scatter(sun_pRot,sun_pCyc,c='orange',marker=(5,1),label='Sun',s=110,alpha=0.5)
#P.scatter(spRot,spCyc,c='k',marker='o',label='RC07-49',s=95)
#P.plot(spRot[2:],c1*spCyct1[2:7],'--',c='g',label=r'$D_r^{\prime}$')
#P.plot(spRot[2:],c3*spCyct3[2:7],'--',c='orange')

P.scatter(FG_pRotI,FG_pCycI,c='r',marker=(5,1),s=95,alpha=0.5)
P.scatter(FG_pRotA,FG_pCycA,c='b',marker=(5,1),s=95,alpha=0.5)
P.scatter(K_pRotI,K_pCycI,c='r',marker=(5,2),s=95,alpha=0.5)
P.scatter(K_pRotA,K_pCycA,c='b',marker=(5,2),s=95,alpha=0.5)
P.scatter(sun_pRot,sun_pCyc,c='orange',marker=(5,1),s=110,alpha=0.5)
P.scatter(spRot,spCyc,c='k',marker='o',s=125)
P.plot(spRot[:], c1*N.delete(spCyct1[0:9],2),'--',c='g',      label=r'$\propto \sqrt{D_r^{\prime \; TAC}}$')
P.plot(spRot[:], c3*N.delete(spCyct3[0:9],2),'--',c='orange', label=r'$\propto \tau_{\rm dyn}$')
P.plot(spRot[:], c4*N.delete(spCyct4[0:9],2),linestyle='dotted',c='k', label=r'$\propto \langle \overline{B}_{\phi} \rangle^{TAC}$')
#P.plot(spRot[0:2],c5*spCyct5[0:2],'-',c='g',      label=r'$\propto (D_r^{\prime})^{-1/2}_{NSL}$')

xini = 5
xend = 50
yini = 0.
yend = 43

P.xlim(xini,xend)
P.ylim(yini,yend)

P.title(r'{\bf (a)}',loc='left',fontsize=20)
P.xlabel(r'$P_{\rm rot}[{\rm d}]$')
P.ylabel(r'$P_{\rm cyc}[{\rm yr}]$')
P.legend(loc=2,fontsize=16)
P.tight_layout()
# para salvar como eps
P.savefig("prpc.eps",format='eps',dpi=400,bbox_inches='tight')
# para salvar como png
P.savefig("prpc.eps",format='eps',bbox_inches='tight')

P.show()
