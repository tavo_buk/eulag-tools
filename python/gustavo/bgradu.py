import numpy as N
from eulag import *

g = read_grid()

Prot = input('What is the period of rotation? ')
omega_0 = 2.*pi/(Prot*24.*3600.)
cut = 5. # Degrees to be cut off
good = int(cut*g.m/180.)
theta = g.th
theta_good = N.linspace(0+(cut/90)*pi/2., (1 - cut/180)*pi, num = g.m - 2*good)

R, TH = meshgrid(g.r, theta_good)
X = R*N.sin(TH)
Y = R*N.cos(TH)

velocity = N.zeros_like(f.u)
for s in range(f.u.shape[0]):
	for i in range(g.n):
		for j in range(good, g.m - good -1):
			for k in range(g.l):
				velocity[s, i, j, k] = f.u[s, i, j, k] + omega_0*X[j,k]
mzero = N.zeros_like(bx)

bgradu = N.zeros_like(bxm)
for i in range(f.bx.shape[0]):
	graduphi_phi, graduphi_theta, graduphi_r =  grad(velocity[i, ...], g.phi, g.th, g.r, 'sph')
	bgradu[i,...] = N.mean(dot(mzero, f.by[i,...], f.bz[i,...], mzero, graduphi_theta, graduphi_r), axis = 0)

N.save('bgradu', bgradu)
