import numpy as N
import matplotlib.pylab as P
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc                  

# PLOT STYLE #                                                                                                         
matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 24}                         
rc('font', **font)                            
rc('xtick', labelsize = 22)                   
rc('ytick', labelsize = 22)                   
                                              
legend = {'fontsize': 16}                     
rc('legend',**legend)                         
axes = {'labelsize': 24}                      
rc('axes', **axes)                            
rc('mathtext',fontset='cm')                   
#use this, but at the expense of slowdown of rendering
rc('text', usetex = True)                     
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]
################################################################################

R = 6.96e8
LCZ  = R
LNSL = R
LTAC = R
tau_NSL = LNSL**2/eta_CZ 
tau_CZ  = LNSL**2/eta_CZ
tau_TAC = LNSL**2/eta_CZ

alpha_NSL = (alpha_k_NSL + alpha_m_NSL)
alpha_CZ =  (alpha_k_CZ  + alpha_m_CZ)
alpha_TAC = (alpha_k_TAC + alpha_m_TAC)

#error bars:
# number of steps in ir1:       11
# number of steps in th[3:31]:  28
# number of steps in th[5:31]:  26
ntimes = N.array([44,150,123,128,112,79,87,80,94,79,42])


#err2_alph_NSL = N.abs(LNSL/eta_TAC)**2*std_alpha_k_NSL**2  + N.abs(LNSL*alpha_NSL/eta_TAC**2)**2*std_eta_TAC**2
#err2_comr_NSL = N.abs(LNSL**2/eta_TAC)**2* std_omr_NSL**2  + N.abs(LNSL**2*omr_NSL/eta_TAC**2)**2*std_eta_TAC**2
#err2_comt_NSL = N.abs(LNSL**2/eta_TAC)**2* std_omth_NSL**2 + N.abs(LNSL**2*omth_NSL/eta_TAC**2)**2*std_eta_TAC**2
#
#err2_alph2   = 2.*calp_NSL**2*err2_alph_NSL
#err2_alphomr = comr_NSL**2*err2_alph_NSL + calp_NSL**2*err2_comr_NSL
#err2_alphomt = comt_NSL**2*err2_alph_NSL + calp_NSL**2*err2_comt_NSL
#
#erra2o = N.sqrt((err2_alph2 + err2_alphomr + err2_alphomt)/(ntimes*11.*26))
#errao  = N.sqrt((err2_alphomr + err2_alphomt)/(ntimes*11.*26))

calp_NSL = tau_NSL*alpha_NSL/LNSL 
comr_NSL = tau_NSL*omr_NSL*LNSL  
comt_NSL = tau_NSL*omth_NSL*LNSL 
cmc_NSL  = tau_NSL*mc_NSL/LNSL

calp_CZ = tau_CZ*alpha_CZ/LNSL
comr_CZ = tau_CZ*omr_CZ*LNSL  
comt_CZ = tau_CZ*omth_CZ*LNSL
cmc_CZ  = tau_CZ*mc_CZ/LNSL

calp_TAC = tau_TAC*alpha_TAC/LNSL
comr_TAC = tau_TAC*(omr_TAC)*LNSL 
comt_TAC = tau_TAC*(omth_TAC)*LNSL
cmc_TAC  = tau_TAC*mc_TAC/LNSL

#D1_NSL = N.abs((comr_NSL+comt_NSL+calp_NSL)*calp_NSL) # + eta_CZ/eta_TAC
DNSL_aor = N.abs(comr_NSL*calp_NSL) 
DNSL_aot = N.abs(comt_NSL*calp_NSL)
DNSL_a2  = N.abs(calp_NSL**2) 
DNSL     = DNSL_aor + DNSL_aot + DNSL_a2 

DCZ_aor = N.abs(comr_CZ*calp_CZ) 
DCZ_aot = N.abs(comt_CZ*calp_CZ)
DCZ_a2  = N.abs(calp_CZ**2) 
DCZ     = DCZ_aor + DCZ_aot + DCZ_a2 

DTAC_aor = (comr_TAC*calp_TAC) 
DTAC_aot = (comt_TAC*calp_TAC)
DTAC_a2  = (calp_TAC**2) 
DTAC     = DTAC_aor + DTAC_aot + DTAC_a2 

#P.title("Dynamo numbers  --  RC models")
fig,ax=P.subplots()
ax.scatter( N.log10(ro1[:]),N.log10(DTAC),c='r',marker=(5,0),label=r'$\log (D_{\alpha^2\Omega})$',s=80)
ax.scatter( N.log10(ro1[:]),N.log10(DTAC_aor[:]),c='orange',marker=(5,0),label=r'$\log (D_{\alpha\Omega_r})$',s=80)
ax.scatter( N.log10(ro1[:]),N.log10(DTAC_aot[:]),c='b',marker=(5,0),label=r'$\log (D_{\alpha\Omega_{\theta}})$',s=80)
ax.scatter( N.log10(ro1[:]),N.log10(DTAC_a2[:]),c='g',marker=(5,0),label=r'$\log (D_{\alpha^2})$',s=80)

#ax.errorbar(N.log10(ro1[:]),N.log10(D3[:]),yerr= erra2o[:], linestyle="None",ecolor='r',elinewidth=1.5  )
#ax.scatter(N.log10(ro1[:]),N.log10(D0_NSL[:]),c='r',label=r'$\log (D^r_{TAC})$',s=100)
#ax.errorbar(N.log10(ro1[:]),N.log10(D0_NSL[:]),yerr= errao, linestyle="None")
#ax.scatter(N.log10(ro1[:]),N.log10(D1_TAC),c='g',label=r'$\log (D^r_{TAC})$',s=100)
#ax.scatter(N.log10(ro1[:-2]),N.log10(D1_NSL[:-2]*D1_TAC[:-2]),c='g',marker=(5,0),label=r'$\log (D^r_{NSL})$',s=100)
#ax.scatter(N.log10(ro1[:-2]),N.log10((calp_TAC*com1_TAC + calp_NSL*com1_NSL)[:]),c='orange',marker=(5,1),label=r'$\log(D^r_{NSL} + D^r_{TAC})$',s=190)

#ax.xaxis.set_ticks(N.linspace(-0.5,0.5,5))
#ax.xaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
#ax.yaxis.set_ticks(N.linspace(2,4.5,5))
#ax.yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
#P.xlim(-0.8,1.6)
#ax.xaxis.set_ticks(N.linspace(-0.5,1.,5))
#ax.xaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
P.xlabel(r'${\log \rm Ro}$')
P.legend(loc=3,scatterpoints=1) #,frameon=False)
P.tight_layout()
P.show()

