import eulag as E
import numpy as N
from scipy.ndimage.filters import gaussian_filter
import os
import sys
sys.path.append("/home/guerrero/eulag-tools/python/gustavo/")
from phel import merid
from scipy.integrate import simps

def get_folder_name(folder):
    '''
    Returns the folder name, given a full folder path
    '''
    return folder.split(os.sep)[-1]   


# Main Program

# verifing if the models is HD or MHD

filename = "define.h"
imhd = open(filename)
for line in imhd:
    if line.split()[1] == "MHD":
        MHD = int(line.split()[2])       # If MHD = 0 there is no magnetic field

if MHD == 1:
    print ('MHD config.')
else:
    print ('HD config.')

if (MHD == 1):
   root = '/home/DATA/Solar/TAC/'
   dir = ['tac-7-rf','tac-14-rf', 'tac-21-rf', 'tac-28-rf', 
         'tac-35-rf', 'tac-42-rf','tac-49-rf', 'tac-56-rf', 'tac-63-rf'] #,'tac-112-rf', 'tac-140-rf']
   lab = ['(a) RC07', '(b) RC14', '(c) RC21', '(d) RC28', '(e) RC35', \
          '(f) RC42', '(g) RC49', '(h) RC56', '(i) RC63'] #, 'RC112','RC140']
   col = ['r','g','b','k','orange','c','m','tomato','darkcyan']
   year = 365.*24.*3600.
   day  = 24.*3600.
   pr = N.array([7.,14.,21.,28.,35.,42.,49.,56.,63]) #,112,140])
   istart = N.array([200,0,0,0,0,0,0,0,0])
else:
   root = '/home/DATA/Solar/HD/'
   dir = ['tac-7-rf','tac-14-rf', 'tac-21-rf', 'tac-28-rf', 
          'tac-35-rf', 'tac-42-rf','tac-49-rf'] 
   lab = ['RC07', 'RC14', 'RC21', 'RC28', 'RC35', 'RC42','RC49']
   col = ['r','g','b','k','orange','c']
   year = 365.*24.*3600.
   day  = 24.*3600.
   pr = N.array([7.,14.,21.,28.,35.,42.,49.])
   istart = N.array([0,550,480,0,500,390,0])
   
Ro = N.zeros_like(pr)
nd = len(dir)
mu0 = 4.*N.pi*1e-7

for id in range (3,7): 
    os.chdir(root+dir[id])
    # getting folder name
    fname = get_folder_name(os.getcwd())
    g = E.read_grid()
    s = E.read_strat()
    f = E.rf11m(ifl = True)
    b2 = f.bx**2 + f.by**2 + f.bz**2
    bmr = N.sqrt((b2[:,0:31,:]).mean(axis=(0,1)))
    kg, = N.where(bmr >= bmr[0:35].mean())
    kf = kg[-1]
    ki = kg[0]
    reff = g.rf[kf] - g.rf[ki]
    y_mean = (bmr[0:35].mean())*N.ones(kg.size)
    x_mean = N.linspace(g.rf[ki],g.rf[kf],kg.size)
    #P.plot(g.rf,bmr,col[id], linewidth=3,label=lab[id])
    #P.plot(x_mean,y_mean,col[id],linestyle='--')
    #P.xlabel(r'$r [R_{\odot}]$')
    #P.ylabel(r'$\overline{B} [{\rm T}]$')
    #P.legend(loc=1,fontsize=13,ncol=1)
    print fname, bmr[0:35].mean(), reff,kg.size
    
#P.tight_layout()
#P.savefig("spectra.eps",format='eps',dpi=400,bbox_inches='tight')
#P.show()

