from eulag import *
import matplotlib.pylab as P
from scipy.ndimage.filters import gaussian_filter
import numpy as N
import os

def compute_a():
    g = read_grid()
    ir1, = N.where((g.rf > 0.9) & (g.rf < 0.96))
    ir2, = N.where((g.rf > 0.63) & (g.rf < 0.74))
    s = read_strat()
    #f  = rxav('xav.dat')
    f = E.rf11m(ifl=True)
    
    u   =   gaussian_filter(f.u[:,:,:],0.0)
    v   =   gaussian_filter(f.v[:,:,:],0.0)
    w   =   gaussian_filter(f.w[:,:,:],0.0)
    bxm =  gaussian_filter(f.bx[:,:,:],0.0)  
    bym =  gaussian_filter(f.by[:,:,:],0.0)
    bzm =  gaussian_filter(f.bz[:,:,:],0.0)
    
    ''' time series to decide the goodness of the data '''
    year = 365.*24.*3600.
    time = f.time[-500:]/year
    dt = (time[1] - time[0])*year/10.
    nelements = time.size
    
    ''' alpha effect -- FOSA '''
    alpha_k, alpha_m, etat_r = N.load('alpha.npy')
    etat_r *= 0.05

    D = 0.05*(alpha_k+alpha_m)
    
    ur   = N.zeros_like(bxm)
    uth  = N.zeros_like(bxm)
    uphi = N.zeros_like(bxm)
    shear_r   = N.zeros_like(bxm)
    shear_th  = N.zeros_like(bxm)
    shear_phi = N.zeros_like(bxm)
    mer_r   = N.zeros_like(bxm)
    mer_th  = N.zeros_like(bxm)
    mer_phi = N.zeros_like(bxm)
    curb_r = N.zeros_like(bxm)
    curb_th = N.zeros_like(bxm)
    curb_phi = N.zeros_like(bxm)
    
    br0   = N.zeros_like(bxm)
    bth0  = N.zeros_like(bxm)
    bphi0 = N.zeros_like(bxm)
    br1   = N.zeros_like(bxm)
    bth1  = N.zeros_like(bxm)
    bphi1 = N.zeros_like(bxm)
    br2   = N.zeros_like(bxm)
    bth2  = N.zeros_like(bxm)
    bphi2 = N.zeros_like(bxm)
    br2   = N.zeros_like(bxm)
    bth2  = N.zeros_like(bxm)
    bphi2 = N.zeros_like(bxm)
    br3   = N.zeros_like(bxm)
    bth3  = N.zeros_like(bxm)
    bphi3 = N.zeros_like(bxm)
    
    for it in range(nelements):
        shear_phi[it,...],shear_th[it,...],shear_r[it,...] = cross(u[it,...],0.*v[it,...],0.*w[it,...],bxm[it,...],bym[it,...],bzm[it,...])   
        mer_phi[it,...],mer_th[it,...],mer_r[it,...] = cross(0.*u[it,...],v[it,...],w[it,...],bxm[it,...],bym[it,...],bzm[it,...])   
        curb_phi[it,...],curb_th[it,...],curb_r[it,...] = curl2d(bxm[it,...],bym[it,...],bzm[it,...],g.phi,g.th,g.r,coord='sph')
        bphi0[it,...],bth0[it,...],br0[it,...] = curl2d(-shear_phi[it,...],-shear_th[it,...],-shear_r[it,...],g.phi,g.th,g.r,coord='sph')
        bphi1[it,...],bth1[it,...],br1[it,...] = curl2d(-mer_phi[it,...],-mer_th[it,...],-mer_r[it,...],g.phi,g.th,g.r,coord='sph')
        bphi2[it,...],bth2[it,...],br2[it,...] = curl2d(D*bxm[it,:,:],D*bym[it,:,:],D*bzm[it,:,:],g.phi,g.th,g.r,coord='sph')
        bphi3[it,...],bth3[it,...],br3[it,...] = curl2d(etat_r*curb_phi[it,:,:], etat_r*curb_th[it,:,:],etat_r*curb_r[it,:,:],g.phi,g.th,g.r,coord='sph')
    
    bphi0_NSL = dt*N.sqrt(N.mean(bphi0[:,1:31,ir1]**2))
    bphi1_NSL = dt*N.sqrt(N.mean((bphi0+bphi1)[:,1:31,ir1]**2))
    bphi2_NSL = dt*N.sqrt(N.mean((bphi0+bphi1+bphi2)[:,1:31,ir1]**2))
    bphi3_NSL = dt*N.sqrt(N.mean((bphi0+bphi1+bphi2-bphi3)[:,1:31,ir1]**2))

    br0 = br0
    br1 = br0+br1
    br2 = br1+br2
    br3 = br2-br3

    bth0 = bth0
    bth1 = bth0+bth1
    bth2 = bth1+bth2
    bth3 = bth2-bth3
    
    bp0_NSL = dt*N.sqrt(N.mean((br0**2+bth0**2)[:,1:31,ir1]))
    bp1_NSL = dt*N.sqrt(N.mean((br1**2+bth1**2)[:,1:31,ir1]))
    bp2_NSL = dt*N.sqrt(N.mean((br2**2+bth2**2)[:,1:31,ir1]))
    bp3_NSL = dt*N.sqrt(N.mean((br3**2+bth3**2)[:,1:31,ir1]))

    return bphi0_NSL, bphi1_NSL, bphi2_NSL, bphi3_NSL, bp0_NSL, bp1_NSL, bp2_NSL, bp3_NSL
#
# Main Program
root = '/storage/TAC/'
dir = ['tac-7-rf','tac-14-rf', 'tac-21-rf', 'tac-28-rf', 
      'tac-35-rf', 'tac-42-rf','tac-49-rf', 'tac-56-rf', 'tac-63-rf']

lab = ['RC07', 'RC14', 'RC21', 'RC28', 'RC35', 'RC42', 'RC49', 'RC56', 'RC63']
col = ['r','g','b','k','orange','c','m','k', 'y']
istart = N.array([0,0,0,0,0,0,0,0,0])

pr = N.array([7.,14.,21.,28.,35.,42.,49.,56.,63.])
nd = len(dir)

bt0_nsl =[] 
bt1_nsl =[] 
bt2_nsl =[] 
bt3_nsl =[] 

bp0_nsl =[] 
bp1_nsl =[] 
bp2_nsl =[] 
bp3_nsl =[] 

for id in range (nd): 
    os.chdir(root+dir[id])
    print 'dir = ', os.getcwd(), 'reading fort.11 ... '
    bt0, bt1, bt2, bt3, bp0, bp1, bp2, bp3  = compute_a()

    bt0_nsl.append(bt0)
    bt1_nsl.append(bt1)
    bt2_nsl.append(bt2)
    bt3_nsl.append(bt3)

    bp0_nsl.append(bp0)
    bp1_nsl.append(bp1)
    bp2_nsl.append(bp2)
    bp3_nsl.append(bp3)

bt0_nsl =  N.array(bt0_nsl)
bt1_nsl =  N.array(bt1_nsl)
bt2_nsl =  N.array(bt2_nsl)
bt3_nsl =  N.array(bt3_nsl)

bp0_nsl = N.array(bp0_nsl)
bp1_nsl = N.array(bp1_nsl)
bp2_nsl = N.array(bp2_nsl)
bp3_nsl = N.array(bp3_nsl)


#P.scatter(N.log10(ro1[:]),N.log10(bx_NSL[:]) ,s=100, c='r')
#P.scatter(N.log10(ro1[:]),N.log10(bp_NSL[:]) ,s=100, c='b')
#P.scatter(N.log10(ro1[:]),N.log10(bt0_nsl[:]) ,s=100, c='c')
#P.scatter(N.log10(ro1[:]),N.log10(bt1_nsl[:]) ,s=100, c='k')
#P.scatter(N.log10(ro1[:]),N.log10(bt2_nsl[:]) ,s=100, c='g')
#P.show()

