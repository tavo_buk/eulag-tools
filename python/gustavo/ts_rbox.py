from eulag import *
from sys import exit
import matplotlib.pylab as P
import numpy as N

# to run this script, execute first: 
# a = E.rxav()
# followed by
# %run -i /home/guerrero/eulag-tools/python/gustavo/pbrms.py

g = read_grid()
s = read_strat()
Rg = 13732.
Cp = 2.5*Rg
gamma = Cp/(Cp - Rg)
Pad_bottom = 1.7788e13
Pad = Pad_bottom*(s.rho_ad/s.rho_ad[0])**(gamma)
Hp = N.divide(-1., deriv(g.r,N.log(Pad)))

nt = a.u.shape[0]
ir1, = N.where((g.rf > 0.94) & (g.rf < 0.96))

u2   = N.mean(a.u2[:,:,ir1],axis=(1,2)) - N.mean( a.u[:,:,ir1]**2,axis=(1,2)) 
v2   = N.mean(a.v2[:,:,ir1],axis=(1,2)) - N.mean( a.v[:,:,ir1]**2,axis=(1,2))
w2   = N.mean(a.w2[:,:,ir1],axis=(1,2)) - N.mean( a.w[:,:,ir1]**2,axis=(1,2))
bx2 = N.mean(a.bx2[:,:,ir1],axis=(1,2)) - N.mean(a.bx[:,:,ir1]**2,axis=(1,2))
by2 = N.mean(a.by2[:,:,ir1],axis=(1,2)) - N.mean(a.by[:,:,ir1]**2,axis=(1,2))
bz2 = N.mean(a.bz2[:,:,ir1],axis=(1,2)) - N.mean(a.bz[:,:,ir1]**2,axis=(1,2))

urms  = N.sqrt(u2 + v2 + w2)
brms  = N.sqrt(bx2 + by2 + bz2)
brmsm = N.sqrt(N.mean((a.bx**2 + a.by**2 + a.bz**2),axis=(1,2)))
year = 365.*24.*3600.
time = a.time/year

N.save('ts',(brmsm,urms,time))

P.figure(1)
P.plot(time,brms**2,c='b')
P.plot(time,brmsm**2,c='r')
P.plot(time,urms**2)
P.yscale('log') 
P.ylabel('$b_{rms}$')
P.xlabel('Time [yr]')
P.ylabel('$<B>_{rms}$')
P.show()
