import numpy as N
import matplotlib.pylab as P
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc                  

# PLOT STYLE #                                                                                                         
matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 22}                         
rc('font', **font)                            
rc('xtick', labelsize = 24)                   
rc('ytick', labelsize = 24)                   

root = '/storage/TAC/'
dir = ['tac-7-rf','tac-18-rf', 'tac-21-rf', 
      'tac-24-rf','tac-35-rf','tac-42-rf', 'tac-49-rf','tac-56-rf','tac-63-rf']

lab = ['RC07', 'RC18', 'RC21', 'RC24', 'RC35', 'RC42', 'RC49', 'RC56', 'RC63']
col = ['r','g','b','k','orange','c','m','tomato','darkcyan']
nd = len(dir)
file = '/spec2d.npy'
k = N.arange(64)
lev = 52
mu0 = 4.*N.pi*1e-7
x =  N.linspace(10,40,30)

P.figure(figsize = (15,6))
P.subplot(121)
lev = 17
for id in range (nd): 
    s = E.read_strat()
    tme,tke = N.load(root+dir[id]+'/turb_spec.npy')
    P.plot(k[1:],0.5*s.rho_ad[1:]*tke[1:,lev], col[id], linewidth=3,label=lab[id])
    P.plot(k[1:],0.5*tme[1:,lev]/mu0, col[id], linestyle='--', linewidth=3)
P.yscale('log')
P.xscale('log')
P.xlabel(r'$k = l+1/2 \; [m^{-1}] $')
P.ylabel(r'$\tilde{E}_{k,m} \; {\rm [J\; m^{-3}]} \;, \;  (r=0.7R_{\odot}) $')
P.legend(loc=3,fontsize=13,ncol=3)
P.plot(x,2e6*x**(-5/3),c='k',linestyle='--')
P.text(20,1e4, r'$k^{-5/3}$')
P.xlim(1.,64.)
P.ylim(1,2e5)

P.subplot(122)
lev = 52
for id in range (nd): 
    s = E.read_strat()
    tme,tke = N.load(root+dir[id]+'/turb_spec.npy')
    P.plot(k[1:],0.5*s.rho_ad[1:]*tke[1:,lev], col[id], linewidth=3,label=lab[id])
    P.plot(k[1:],0.5*tme[1:,lev]/mu0, col[id], linestyle='--', linewidth=3)
P.yscale('log')
P.xscale('log')
P.xlabel(r'$k = l+1/2 \; [m^{-1}] $')
P.ylabel(r'$\tilde{E_{k,m}} \; {\rm [J\; m^{-3}]} \;, \;  (r=0.9R_{\odot}) $')
P.plot(x,1e8*x**(-5/3),c='k',linestyle='--')
P.text(20,3e5, r'$k^{-5/3}$')
P.ylim(30,2e6)
P.xlim(1.,64.)

P.tight_layout()
P.savefig("spectra.eps",format='eps',dpi=400,bbox_inches='tight')
P.show()

