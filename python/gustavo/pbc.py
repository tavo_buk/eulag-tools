import numpy as N
import matplotlib.pylab as P
import eulag as E
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib
from matplotlib.ticker import FormatStrFormatter
from matplotlib import rc
import cmocean

def bf_merid(f1,f2,f3,f4,f5,adjust_color = (1.,1.,1.,1.,1.),label='Fig'):
    from scipy.integrate import simps
    g = E.read_grid()

    cut = 5. # Degrees to be cut off
    good = int(cut*g.m/180.)

    theta_good = N.linspace(0+(cut/90)*N.pi/2., (1 - cut/180)*N.pi, num = g.m - 2*good)
    R, TH = N.meshgrid(g.r, theta_good)
    X = R*N.sin(TH)
    Y = R*N.cos(TH)

    fig, ax = P.subplots(nrows = 1, ncols = 5, facecolor = None, edgecolor = None, linewidth = None,figsize = (15,5))
    fig.suptitle(label,horizontalalignment='left', fontsize=18)
    f1min = f1.min()
    f1max = f1.max()
    if abs(f1min) > abs(f1max):
        f1max = f1max*abs(f1min)/abs(f1max)
    else:
        f1min = f1min*abs(f1max)/abs(f1min)
    levels = N.linspace(f1min, f1max, 50)*adjust_color[0]

    cs = ax[0].contourf(X, Y, f1[1:-1,:], levels = levels, interpolation = 'bicubic', cmap=cmocean.cm.delta,extend='both')
    # Add colorbar
    tick = N.linspace(f1min, f1max, num = 2, endpoint = True)*adjust_color[0]
    divider = make_axes_locatable(ax[0])
#    cax = divider.append_axes("left", size = "5%", pad = 0.08)
    cax1 = fig.add_axes([0.15, 0.35, 0.010, 0.3])

    tick = N.linspace(levels[0], levels[-1], num = 2, endpoint = True)
    cbar1 = fig.colorbar(cs, cax = cax1, ticks = tick, ticklocation = 'left', format = '%4.2f')
    cbar1.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 0, length = 6)

    # Make sure aspect ratio preserved
    ax[0].set_aspect('equal')

    # Turn off rectangular frame.
    ax[0].set_frame_on(False)

    # Turn off axis ticks.
    ax[0].set_xticks([])
    ax[0].set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
    ax[0].set_title(r'$\alpha_{\rm k} \; {\rm [m/s]}$')
    ax[0].plot(rmax*N.sin(g.th), rmax*N.cos(g.th),'k')
    ax[0].plot(rmin*N.sin(g.th), rmin*N.cos(g.th),'k')
    ax[0].plot(g.r*N.sin(min(g.th)), g.r*N.cos(min(g.th)), 'k')
    ax[0].plot(g.r*N.sin(max(g.th)), g.r*N.cos(max(g.th)), 'k')
    ax[0].plot(g.r[23] * N.sin(g.th), g.r[23] * N.cos(g.th), '--k')
    ax[0].plot(g.r[51] * N.sin(g.th), g.r[51] * N.cos(g.th), '--k')

################################################################################

    f1min = f2.min()
    f1max = f2.max()
    if abs(f1min) > abs(f1max):
        f1max = f1max*abs(f1min)/abs(f1max)
    else:
        f1min = f1min*abs(f1max)/abs(f1min)
    levels = N.linspace(f1min, f1max, 50)*adjust_color[1]
    cs = ax[1].contourf(X, Y, f2[1:-1,:], levels = levels, interpolation = 'bicubic', cmap=cmocean.cm.delta,extend='both')
    # Add colorbar
    tick = N.linspace(f1min, f1max, num = 2, endpoint = True)*adjust_color[1]
    divider = make_axes_locatable(ax[1])
#    cax = divider.append_axes("left", size = "5%", pad = 0.08)

    cax2 = fig.add_axes([0.31, 0.35, 0.010, 0.3])
    tick = N.linspace(levels[0], levels[-1], num = 2, endpoint = True)
    cbar2 = fig.colorbar(cs, cax = cax2, ticks = tick, ticklocation = 'left', format = '%4.2f')
    cbar2.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 0, length = 6)

    # Make sure aspect ratio preserved
    ax[1].set_aspect('equal')

    # Turn off rectangular frame.
    ax[1].set_frame_on(False)

    # Turn off axis ticks.
    ax[1].set_xticks([])
    ax[1].set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
    ax[1].set_title(r'$\alpha_{\rm m} \; {\rm [m/s]}$')
    ax[1].plot(rmax*N.sin(g.th), rmax*N.cos(g.th),'k')
    ax[1].plot(rmin*N.sin(g.th), rmin*N.cos(g.th),'k')
    ax[1].plot(g.r*N.sin(min(g.th)), g.r*N.cos(min(g.th)), 'k')
    ax[1].plot(g.r*N.sin(max(g.th)), g.r*N.cos(max(g.th)), 'k')
    ax[1].plot(g.r[23] * N.sin(g.th), g.r[23] * N.cos(g.th), '--k')
    ax[1].plot(g.r[51] * N.sin(g.th), g.r[51] * N.cos(g.th), '--k')

################################################################################

    f1min = f3.min()
    f1max = f3.max()
    if abs(f1min) > abs(f1max):
        f1max = f1max*abs(f1min)/abs(f1max)
    else:
        f1min = f1min*abs(f1max)/abs(f1min)
    levels = N.linspace(f1min, f1max, 50)*adjust_color[2]
    cs = ax[2].contourf(X, Y, f3[1:-1,:], levels = levels, interpolation = 'bicubic', cmap=cmocean.cm.delta,extend='both')
    # Add colorbar
    tick = N.linspace(f1min, f1max, num = 2, endpoint = True)*adjust_color[2]
    divider = make_axes_locatable(ax[1])
#    cax = divider.append_axes("left", size = "5%", pad = 0.08)

    cax3 = fig.add_axes([0.47, 0.35, 0.010, 0.3])
    tick = N.linspace(levels[0], levels[-1], num = 2, endpoint = True)
    cbar3 = fig.colorbar(cs, cax = cax3, ticks = tick, ticklocation = 'left', format = '%4.2f')
    cbar3.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 0, length = 6)

    # Make sure aspect ratio preserved
    ax[2].set_aspect('equal')

    # Turn off rectangular frame.
    ax[2].set_frame_on(False)

    # Turn off axis ticks.
    ax[2].set_xticks([])
    ax[2].set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
    ax[2].set_title(r'$\alpha \; {\rm [m/s]}$')
    ax[2].plot(rmax*N.sin(g.th), rmax*N.cos(g.th),'k')
    ax[2].plot(rmin*N.sin(g.th), rmin*N.cos(g.th),'k')
    ax[2].plot(g.r*N.sin(min(g.th)), g.r*N.cos(min(g.th)), 'k')
    ax[2].plot(g.r*N.sin(max(g.th)), g.r*N.cos(max(g.th)), 'k')
    ax[2].plot(g.r[23] * N.sin(g.th), g.r[23] * N.cos(g.th), '--k')
    ax[2].plot(g.r[51] * N.sin(g.th), g.r[51] * N.cos(g.th), '--k')

################################################################################

    f1min = f4.min()
    f1max = f4.max()
    if abs(f1min) > abs(f1max):
        f1max = f1max*abs(f1min)/abs(f1max)
    else:
        f1min = f1min*abs(f1max)/abs(f1min)
    levels = N.linspace(f1min, f1max, 50)*adjust_color[3]
    cs = ax[3].contourf(X, Y, f4[1:-1,:], levels = levels, interpolation = 'bicubic', cmap=cmocean.cm.delta,extend='both')
    # Add colorbar
    tick = N.linspace(f1min, f1max, num = 2, endpoint = True)*adjust_color[3]
    divider = make_axes_locatable(ax[1])
#    cax = divider.append_axes("left", size = "5%", pad = 0.08)

    cax4 = fig.add_axes([0.63, 0.35, 0.010, 0.3])
    tick = N.linspace(levels[0], levels[-1], num = 2, endpoint = True)
    cbar4 = fig.colorbar(cs, cax = cax4, ticks = tick, ticklocation = 'left', format = '%4.2f')
    cbar4.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 0, length = 6)

    # Make sure aspect ratio preserved
    ax[3].set_aspect('equal')

    # Turn off rectangular frame.
    ax[3].set_frame_on(False)

    # Turn off axis ticks.
    ax[3].set_xticks([])
    ax[3].set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
    ax[3].set_title(r'$\alpha \; {\rm [m/s]}$')
    ax[3].plot(rmax*N.sin(g.th), rmax*N.cos(g.th),'k')
    ax[3].plot(rmin*N.sin(g.th), rmin*N.cos(g.th),'k')
    ax[3].plot(g.r*N.sin(min(g.th)), g.r*N.cos(min(g.th)), 'k')
    ax[3].plot(g.r*N.sin(max(g.th)), g.r*N.cos(max(g.th)), 'k')
    ax[3].plot(g.r[23] * N.sin(g.th), g.r[23] * N.cos(g.th), '--k')
    ax[3].plot(g.r[51] * N.sin(g.th), g.r[51] * N.cos(g.th), '--k')


################################################################################

    f1min = f5.min()
    f1max = f5.max()
    if abs(f1min) > abs(f1max):
        f1max = f1max*abs(f1min)/abs(f1max)
    else:
        f1min = f1min*abs(f1max)/abs(f1min)
    levels = N.linspace(f1min, f1max, 50)*adjust_color[4]
    cs = ax[4].contourf(X, Y, f5[1:-1,:], levels = levels, interpolation = 'bicubic', cmap=cmocean.cm.delta,extend='both')
    # Add colorbar
    tick = N.linspace(f1min, f1max, num = 2, endpoint = True)*adjust_color[4]
    divider = make_axes_locatable(ax[1])
#    cax = divider.append_axes("left", size = "5%", pad = 0.08)

    cax5 = fig.add_axes([0.79, 0.35, 0.010, 0.3])
    tick = N.linspace(levels[0], levels[-1], num = 2, endpoint = True)
    cbar5 = fig.colorbar(cs, cax = cax5, ticks = tick, ticklocation = 'left', format = '%4.2f')
    cbar5.ax.tick_params(which = 'major', direction = 'inout', pad = 6, width = 0, length = 6)

    # Make sure aspect ratio preserved
    ax[4].set_aspect('equal')

    # Turn off rectangular frame.
    ax[4].set_frame_on(False)

    # Turn off axis ticks.
    ax[4].set_xticks([])
    ax[4].set_yticks([])

    # Draw a circle around the edge of the plot.
    rmax = max(g.r)
    rmin = min(g.r)
    ax[4].set_title(r'$\alpha \; {\rm [m/s]}$')
    ax[4].plot(rmax*N.sin(g.th), rmax*N.cos(g.th),'k')
    ax[4].plot(rmin*N.sin(g.th), rmin*N.cos(g.th),'k')
    ax[4].plot(g.r*N.sin(min(g.th)), g.r*N.cos(min(g.th)), 'k')
    ax[4].plot(g.r*N.sin(max(g.th)), g.r*N.cos(max(g.th)), 'k')
    ax[4].plot(g.r[23] * N.sin(g.th), g.r[23] * N.cos(g.th), '--k')
    ax[4].plot(g.r[51] * N.sin(g.th), g.r[51] * N.cos(g.th), '--k')

    P.savefig("recons.png",dpi=600,bbox_inches='tight')
#   P.savefig(name+'merid'+'.png')
#   P.show()
