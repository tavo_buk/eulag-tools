# PLOT STYLE #
import matplotlib
from matplotlib.ticker import FormatStrFormatter
from matplotlib import rc
# Choose Computer Modern Roman fonts by default
matplotlib.rcParams['font.serif'] = 'cmr10'
matplotlib.rcParams['font.sans-serif'] = 'cmr10'

font = { 'size' : 20}
rc('font', **font)
rc('xtick', labelsize = 20)
rc('ytick', labelsize = 20)

legend = {'fontsize': 20}
rc('legend',**legend)
axes = {'labelsize': 20}
rc('axes', **axes)
rc('mathtext',fontset='cm')
#use this, but at the expense of slowdown of rendering
rc('text', usetex = True)
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]
################################################################################
###### Define constants #######
year = 365.*24.*3600.
mu = 4*N.pi*1.e-7
###############################
try:
    h
except:
    h = E.read_f11('fort.11')
stratification = N.genfromtxt('strat.dat')
rho_ad = stratification[:, 5]
g = E.read_grid()

rstar = 6.96e8
g.rf = g.r/rstar
irtac, = N.where(g.rf > .71)

# Differential Rotation Kinetic Energy
drke = 0.5*(rho_ad*(h.u**2))[..., irtac].mean(axis = (1, 2, 3))
# Meridional Circulation Kinetic Energy
mcke = 0.5*(rho_ad*(h.v**2 + h.w**2))[..., irtac].mean(axis = (1, 2, 3))
# Convective Kinetic Energy or Non-Axissymetric Kinetic energy
tu2 = E.turb1(h.u)**2
tv2 = E.turb1(h.v)**2
tw2 = E.turb1(h.w)**2
cke = 0.5*(rho_ad*(tu2 + tv2 + tw2))[...,irtac].mean(axis = (1, 2, 3))
# Total Kinetic Energy
ke = drke + mcke + cke
del tu2, tv2, tw2

nn = 10 # Temporal mean in the nn steps
# Magnetic Energy
bxm = h.bx.mean(axis = 1)
bym = h.by.mean(axis = 1)
bzm = h.bz.mean(axis = 1)

me_r = (0.5/mu)*(h.bx**2 + h.by**2 + h.bz**2)[-nn::, :, :, :].mean(axis = (0, 1, 2))
# Poloidal Mean Magnetic Energy
pme_r = (0.5/mu)*(bym**2 + bzm**2)[-nn::, :, :].mean(axis = (0, 1))
# Toroidal Mean Magnetic Energy
tme_r = (0.5/mu)*(bxm**2)[-nn::, :, :].mean(axis = (0, 1))
# Fluctuating Magnetic Energy
tbx2 = E.turb1(h.bx)**2
tby2 = E.turb1(h.by)**2
tbz2 = E.turb1(h.bz)**2
fme_r = (0.5/mu)*(tbx2 + tby2 + tbz2)[-nn::, :].mean(axis = (0, 1, 2))

me = (0.5/mu)*(h.bx**2 + h.by**2 + h.bz**2)[..., irtac].mean(axis = (1, 2, 3))
tme = (0.5/mu)*(bxm[..., irtac].mean(axis = (1, 2)))**2
pme = (0.5/mu)*((bym[..., irtac].mean(axis = (1, 2)))**2 + (bzm[..., irtac].mean(axis = (1, 2)))**2)
fme = (0.5/mu)*(tbx2 + tby2 + tbz2)[..., irtac].mean(axis = (1, 2, 3))
del tbx2, tby2, tbz2

P.plot(g.rf, me_r,  'k', label = r'$ME$', linewidth = 1)
P.scatter(g.rf, fme_r, 5, 'k', label = r'$FME$')
P.plot(g.rf, pme_r,  'k-.', label = r'$PME$', linewidth = 1)
P.plot(g.rf, tme_r,  '--k', label = r'$TME$', linewidth = 1)
#P.yscale('log')
P.xlabel(r'$\frac{r}{r_*}$', fontsize = 40)
#P.xlim([.1, .95])
#P.ylim([0., 1.e8])
P.ylabel("Magnetic Energy Density")
#P.legend(loc = 'upper left', shadow = True)
P.savefig('tt01_energy_r.eps', dpi = 98)
P.show()


klen = len(ke)
P.plot(h.time/year, ke,  'k',  label = r'$KE$',  linewidth = 2)
P.plot(h.time/year, me,  'k',  label = r'$ME$',  linewidth = 1)
P.plot(h.time/year, cke,  '--k',  label = r'$CME$',  linewidth = 1)
P.yscale('log')
P.xlabel(r'$\mathrm{Time \ [year]}$', fontsize = 20)
P.xlim([(h.time/year).min(), (h.time/year).max()])
#P.ylim([1.e3, 1.e8])
P.legend(loc = 'lower right', shadow = True)
P.savefig('tt01_energy_time.eps', dpi = 98)
P.show()

'''
P.plot((h.time/year)[-300::], ke[-300::],  'k',  label = r'$KE$',  linewidth = 2)
P.plot((h.time/year)[-300::], me[-300::],  'k',  label = r'$ME$',  linewidth = 1)
P.plot((h.time/year)[-300::], cke[-300::],  '--k',  label = r'$CME$',  linewidth = 1)
P.yscale('log')
P.xlabel(r'$\mathrm{Time \ [year]}$', fontsize = 20)
P.xlim([(h.time/year)[-300::].min(), (h.time/year)[-300::].max()])
P.ylim([1.e3, 1.e8])
P.legend(loc = 'lower right', shadow = True)
P.savefig('tt01_energy_time_zooming.eps', dpi = 98)
P.show()
'''

print 'cke/ke', (cke/ke)[-1]
print 'tme/me', (tme/me)[-1]
print 'me/ke', (me/ke)[-1]
print 'fme/me', (fme/me)[-1]
