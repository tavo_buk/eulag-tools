import numpy as N
import matplotlib.pylab as P
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc                  

# PLOT STYLE #                                                                                                         
matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 24}                         
rc('font', **font)                            
rc('xtick', labelsize = 24)                   
rc('ytick', labelsize = 24)                   
                                              
legend = {'fontsize': 20}                     
rc('legend',**legend)                         
axes = {'labelsize': 24}                      
rc('axes', **axes)                            
rc('mathtext',fontset='cm')                   
#use this, but at the expense of slowdown of rendering
rc('text', usetex = True)                     
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]
################################################################################

btot = N.sqrt(bx_CZ**2 +bp_CZ**2)
line15 = -1.5*N.ones(20)
line10 = -1.0*N.ones(20)
xRo1 = N.linspace(-0.33,0.2,20) 
xRo2 = N.linspace( 0.23,.8,20) 

#P.title("Tachocline magnetic fields --  RC models")

fig,ax=P.subplots()
ax.scatter(N.log10(ro1[:]),N.log10(bx_CZ[:]),marker=(5,0), c='r',label=r'$\log (B_{\phi} [T]) |_{\rm tac} $',s=100)
ax.scatter(N.log10(ro1[:]),N.log10(bp_CZ[:]),marker='o', c='b',label=r'$\log (B_{\rm p} [T])|_{\rm tac} $',s=100)
ax.scatter(N.log10(ro1[:]),N.log10(btot[:]),marker=(5,0), c='k',label=r'$\log (B_{\rm p} [T])|_{\rm tac} $',s=100)
P.plot(xRo1,0.9*xRo1-0.55,'--',c='k')
P.plot(xRo2,-1.9*xRo2-0.05,'--',c='k')
ax.text(-.18,-0.85,r"${\rm Ro}^{0.9}$")
ax.text(0.5,-0.85,r"${\rm Ro}^{-1.9}$")
P.ylim(-2,-0.1)
ax.yaxis.set_ticks(N.linspace(-2,-0.1,5))
ax.yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
P.xlim(-0.5,1.)
ax.xaxis.set_ticks(N.linspace(-0.5,1.,5))
ax.xaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
P.xlabel(r'$\log {\rm Ro}$')
P.legend(loc=3,scatterpoints=1)
P.tight_layout()
P.show()

