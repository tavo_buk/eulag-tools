import numpy as N
import matplotlib.pylab as P
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc                  

# PLOT STYLE #                                                                                                         
matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 22}                         
rc('font', **font)                            
rc('xtick', labelsize = 24)                   
rc('ytick', labelsize = 24)                   

root = '/storage/TAC/'
dir = ['tac-7-rf','tac-14-rf', 'tac-21-rf','tac-24-rf', 'tac-28-rf', 
      'tac-35-rf','tac-42-rf','tac-49-rf']

lab = ['RC07', 'RC14', 'RC21','RC24', 'RC28', 'RC35','RC42','RC49']
col = ['r','g','b','k','orange','c','m','tomato','darkcyan']
nd = len(dir)
file  = '/period.npy'
file2 = '/period_bottom.npy'

fig, ax = P.subplots(nrows = 1, ncols = 2, facecolor = None, edgecolor = None, linewidth = None,figsize = (12,5))
for id in range (0,3): 
    freq_nodc,pspec_nodc = N.load(root+dir[id]+file)
    i_peak = N.argmax(pspec_nodc) 
    freq = freq_nodc[i_peak]
    period0 = 1./freq
    print 'Period0 of the model', dir[id], ': ', period0
    ax[0].plot(freq_nodc,pspec_nodc, col[id], linewidth=3,label=lab[id])
    ax[0].axvline(x=freq,color=col[id],linestyle='dotted') 
ax[0].axvline(x=0.33,color='r',linestyle='dotted') 
ax[0].set_xlabel(r'$f \; [{\rm yr}^{-1}] $')
ax[0].set_ylabel(r'{\rm Spectral \; Density}')
ax[0].set_yscale('log')
ax[0].set_xscale('log')
yini = 1e-9
yend = 1e4
ax[0].yaxis.set_ticks(N.linspace(yini,yend,10))
ax[0].set_ylim(yini,yend)
ax[0].set_xlim(7e-3,3)
ax[0].legend(loc=3,fontsize=13,ncol=1)


lev = 52
for id in range (3,8): 
    freq_nodc,pspec_nodc = N.load(root+dir[id]+file)
    i_peak = N.argmax(pspec_nodc) 
    freq = freq_nodc[i_peak]
    period1 = 1./freq
    print 'Period1 of the model', dir[id], ': ', period1
    ax[1].plot(freq_nodc,pspec_nodc, col[id], linewidth=3,label=lab[id])
    ax[1].axvline(x=freq,color=col[id],linestyle='dotted') 
    freq_nodc,pspec_nodc = N.load(root+dir[id]+file2)
    i_peak = N.argmax(pspec_nodc) 
    freq = freq_nodc[i_peak]
    period2 = 1./freq
    print 'Period1 of the model', dir[id], ': ', period2
    ax[1].plot(freq_nodc,100.*pspec_nodc, col[id], linestyle= 'dashed', linewidth=3)
    ax[1].axvline(x=freq,color=col[id],linestyle='dotted') 
ax[1].set_xlabel(r'$f \; [{\rm yr}^{-1}] $')
ax[1].set_yscale('log')
ax[1].set_xscale('log')
yini = 1e-9
yend = 1e4
ax[1].set_yticks([])
#ax[1].yaxis.set_ticks(N.linspace(yini,yend,10))
ax[1].set_ylim(yini,yend)
ax[1].set_xlim(7e-3,3)
ax[1].legend(loc=3,fontsize=13,ncol=1)

P.tight_layout()
P.savefig("period_spec.eps",format='eps',dpi=400,bbox_inches='tight')
P.show()

