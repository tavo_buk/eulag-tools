import numpy as N
import matplotlib.pylab as P
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc                  

# PLOT STYLE #                                                                                                         
matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 22}                         
rc('font', **font)                            
rc('xtick', labelsize = 24)                   
rc('ytick', labelsize = 24)                   
                                              
legend = {'fontsize': 18}                     
rc('legend',**legend)                         
axes = {'labelsize': 21}                      
rc('axes', **axes)                            
rc('mathtext',fontset='cm')                   
#use this, but at the expense of slowdown of rendering
rc('text', usetex = True)                     
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]
################################################################################

line15 = -1.4*N.ones(20)
line10 = -0.95*N.ones(20)
xRo1 = N.linspace(-0.65,0.3,20) 
xRo2 = N.linspace(-0.65,0.1,20) 
xRo3 = N.linspace(0.15,0.5,20) 

#P.title("Tachocline magnetic fields --  RC models")

ftig,ax=P.subplots()
P.plot(    N.log10(ro1/(2.*N.pi)),    N.log10(eta_NSL),'-',c='r',linewidth=2)
ax.scatter(N.log10(ro1/(2.*N.pi)),N.log10(eta_NSL),marker='o',c='r', label=r'${\rm \log}(\langle \eta_{\rm t} \; [{\rm m^2 \; s^{-1}}]  \rangle^{NSL})$',s=80)
P.plot(    N.log10(ro1/(2.*N.pi)),    N.log10(eta_CZ),'-',c='orange',linewidth=2)
ax.scatter(N.log10(ro1/(2.*N.pi)),N.log10(eta_CZ),marker='o',c='orange', label=r'${\rm \log}(\langle \eta_{\rm t} \; [{\rm m^2 \; s^{-1}}]\rangle^{CZ})$',s=80)
P.plot(    N.log10(ro1/(2.*N.pi)),    N.log10(eta_TAC),'-',c='b',linewidth=2)
ax.scatter(N.log10(ro1/(2.*N.pi)),N.log10(eta_TAC),marker='o',c='b', label=r'${\rm \log}(\langle \eta_{\rm t} \; [{\rm m^2 \; s^{-1}}]\rangle^{TAC})$',s=80)

#P.plot(N.log10(hd_ro),N.log10(hd_eta_NSL),'--',c='r',linewidth=1)
#ax.scatter(N.log10(hd_ro),N.log10(hd_eta_NSL),marker='.',c='r',s=80,alpha=0.5)
#P.plot(N.log10(hd_ro),N.log10(hd_eta_CZ),'--',c='orange',linewidth=1)
#ax.scatter(N.log10(hd_ro),N.log10(hd_eta_CZ),marker='.',c='orange', s=80,alpha=0.5)
#P.plot(N.log10(hd_ro),N.log10(hd_eta_TAC),'--',c='b',linewidth=1) 
#ax.scatter(N.log10(hd_ro),N.log10(hd_eta_TAC),marker='.',c='b', s=80,alpha=0.5)
#ax.text(-0.65,10.05, "(e)")

# AXIS
xini = -0.55
xend =  0.35
yini =  7.6
yend =  9.5
P.ylim(yini,yend)
ax.yaxis.set_ticks(N.linspace(yini,yend,5))
ax.yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
P.xlim(xini,xend)
ax.xaxis.set_ticks(N.linspace(xini,xend,5))
ax.xaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
P.xlabel(r'${\rm \log Ro}$')
P.legend(loc=2,scatterpoints=1)
P.title(r'{\bf (f)}',loc='left',fontsize=20)
P.tight_layout()
P.savefig("quen_eta.eps",format='eps',dpi=400,bbox_inches='tight')
P.show()

