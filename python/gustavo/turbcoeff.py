import eulag as E
import numpy as N
from scipy.ndimage.filters import gaussian_filter
import os
import sys
sys.path.append("/home/guerrero/eulag-tools/python/gustavo/")
from phel import merid
from scipy.integrate import simps

def compute_eddie_size():

    g = E.read_grid()
    s = E.read_strat()
    # Load the 2d power spectrum (l,r)
#   e2d = N.load('spec2d.npy')
    tme,tke = N.load('turb_spec.npy')
    e2d = tke
    # find the maximum for each radius
    k    = N.arange(g.l) + 0.5
    lmax = N.zeros(g.l)
    ledd = N.zeros(g.l)
    lcur = N.zeros(g.l)
    Hp = N.divide(-1., E.deriv(g.r,N.log(s.rho_ad)))

    for i in range(g.l):
        # compute integral scale for the spectra at every level
        if (i >= 2):
#           ledd[i] = Hp[i]
#           lcur[i] = Hp[i]
           ledd[i] = g.r[i]*simps(tke[:,i]/k[:],k[:])/simps(tke[:,i],k[:]) 
           lcur[i] = g.r[i]*simps(tme[:,i]/k[:],k[:])/simps(tme[:,i],k[:])
        else:
           ledd[i]  = 0.
           lcur[i]  = 0.

#    for i in range(g.l):
#        lmax[i], = (N.where(e2d[:,i] == e2d[:,i].max()))[0]
#        if (i >= 17):
#           ledd[i]  = 2.*N.pi*g.r[i]/(lmax[i]+0.5) 
#        else:
#           ledd[i]  = 0.

    return ledd,lcur

# Main Program

# verifing if the models is HD or MHD

filename = "define.h"
imhd = open(filename)
for line in imhd:
    if line.split()[1] == "MHD":
        MHD = int(line.split()[2])       # If MHD = 0 there is no magnetic field

if MHD == 1:
    print ('MHD config.')
else:
    print ('HD config.')

if (MHD == 1):
   root = '/storage/TAC/'
   dir = ['tac-7-rf', 'tac-14-rf','tac-18-rf', 'tac-21-rf','tac-24-rf','tac-28-rf', \
          'tac-35-rf','tac-42-rf','tac-49-rf', 'tac-56-rf', 'tac-63-rf'] 
   lab = ['(a) RC07', '(b) RC14', '(c) RC18', '(d) RC21', '(e) RC24', \
          '(f) RC28', '(f) RC35', '(f) RC42', '(g) RC49', '(g) RC56', \
          '(h) RC63']
   col = ['r','g','b','k','orange','c','m','k', 'y']
   year = 365.*24.*3600.
   day  = 24.*3600.
   pr = N.array([7.,14.,18.,21.,24.,28.,35.,42.,49.,56.,63])
   istart = N.array([100,0,180,0,100,0,0,0,0,0,0])
   alev = [[10,40,40],[8,6,8],[5,25,25],[5,30,30], \
           [6,4,6],[6,4,6],[7,5,7],[8,6,8],[9,9,9],[12,15,15],[13,9,13]]
else:
   root = '/storage/Solar/HD/'
   
Ro = N.zeros_like(pr)
nd = len(dir)
mu0 = 4.*N.pi*1e-7

for id in range (nd): 
    os.chdir(root+dir[id])
    print 'computing mean fields and fil. factors for', pr[id], 'days, period'
    g = E.read_grid()
    s = E.read_strat()
    f = E.read_f11(ifl = False,start=istart[id])
    ut  = E.turb(f.u)
    vt  = E.turb(f.v)
    wt  = E.turb(f.w)
    # Alfven speed
    bxt = E.turb(f.bx)
    byt = E.turb(f.by)
    bzt = E.turb(f.bz)
    b2 = bxt**2 + byt**2 + bzt**2
    b2_rt = N.mean(b2,axis=(0,1))
    bmr = N.sqrt(b2_rt[0:31,:].mean(axis=0))
    vA = bmr/N.sqrt(mu0*s.rho_ad)
    vA[0] = vA[1] 
    vA[-1] = vA[-2]
    # Turbulent flow speed
    urms2  = ut**2 + vt**2 + wt**2
    urms2_rt = N.mean(urms2,axis=(0,1))
    urms_r = N.sqrt(N.mean(urms2_rt,axis=0))
    time = f.time/year
    del f

    # computing eddie size, from it, tauc and etat
    ledd, lcur   = compute_eddie_size()
    leddie = gaussian_filter(ledd,0.3)
    lcurr  = gaussian_filter(lcur,0.3)
    tauc_r = gaussian_filter(leddie/urms_r   ,0.4)
    tauc_a = gaussian_filter(lcurr/vA        ,0.4)
#   etat_r = gaussian_filter(leddie*urms_r/3.,1)
    etat_r = N.zeros([g.m,g.l])
    for j in range(g.m):
        etat_r[j,:] = tauc_r[:]*urms2_rt[j,:]/3.
    
    #computing alpha_effect using helicities and tauc
    khm = N.load('khelicity.npy')
    khm = gaussian_filter(khm,0.6)
    alpha_k = N.zeros_like(khm)
    if (MHD == 1):
       chm = N.load('chelicity.npy')
       chm = gaussian_filter(chm,0.6)
       alpha_m = N.zeros_like(khm)
    
    for i in range(g.l):
        alpha_k[:,i] = -tauc_r[i]*khm[:,i]/3.
        if (MHD == 1):
           if (i <= 23): 
              alpha_m[:,i] =  tauc_a[i]*chm[:,i]/3.
           else:
              alpha_m[:,i] =  tauc_r[i]*chm[:,i]/3.

    # writing the data
    ircz,= N.where((g.rf > 0.87) & (g.rf < 0.95))
    Ro[id] = day*pr[id]/N.mean(tauc_r[ircz])
    print 'period = ', pr[id], 'Ro = ', Ro[id]
    N.save('scales',(urms_r,vA,leddie,lcurr,tauc_r,tauc_a))
    if (MHD == 1):
       N.save('alpha',(alpha_k,alpha_m,etat_r))
    else:
       N.save('alpha',(alpha_k,etat_r))
    merid(alpha_k,alpha_m,alpha_k+alpha_m,label=lab[id], pr = pr[id], adjust_color = alev[id])
