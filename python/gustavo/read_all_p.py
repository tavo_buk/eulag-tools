import numpy as N
root_dir = '/home/DATA/Solar/'
root_dir = '/storage/'

print('1. reading MHD results')
period,urms,ledd,tauc,ro,ro1,\
Domega,Domega_omega,         \
bx_NSL_p,bx_TAC_p,               \
bx_CZ_p,bp_NSL_p,bp_TAC_p,bp_CZ_p,   \
kx_NSL_p,kx_TAC_p,               \
kx_CZ_p,kp_NSL_p,kp_TAC_p,kp_CZ_p    = N.loadtxt(root_dir+'TAC/sim_results-mhd_p.txt', unpack=True)

print('1. reading MHD errors')
Uperiod,Uurms,Uledd,Utauc,Uro,Uro1,\
UDomega,UDomega_Uomega,         \
Ubx_NSL_e,Ubx_TAC_e,           \
Ubx_CZ_e, Ubp_NSL_e,Ubp_TAC_e,Ubp_CZ_e  = N.loadtxt(root_dir+'TAC/std_results-mhd_p.txt', unpack=True)

print('2. reading MHD mean-flows')
period,                      \
omr_NSL_p,omr_TAC_p,             \
omr_CZ_p,omth_NSL_p,omth_TAC_p,    \
omth_CZ_p,mc_NSL_p,              \
mc_TAC_p,mc_CZ_p                 = N.loadtxt(root_dir+'TAC/sim_mflows-mhd_p.txt', unpack=True)

print('3. reading MHD coeffs')
period,alpha_NSL_p,            \
alpha_TAC_p,alpha_CZ_p,          \
alpha_k_NSL_p,                 \
alpha_k_TAC_p,alpha_k_CZ_p,      \
alpha_m_NSL_p,alpha_m_TAC_p,     \
alpha_m_CZ_p,eta_NSL,          \
eta_TAC,eta_CZ               = N.loadtxt(root_dir+'TAC/sim_coeff-mhd_p.txt', unpack=True)

