import numpy as N
import matplotlib.pylab as P

fig, ax = P.subplots(nrows = 2, ncols = 1)
im1 = ax[0].contourf(phi,90-th,br,64,cmap = P.get_cmap("RdBu"))
ax[0].set_title(r'$B_r$ [G]',fontsize=20)
ax[0].set_ylabel(r' latitude [$^{\circ} $]',fontweight='bold')
im2 = ax[1].contourf(phi,90-th,bphi,64,cmap = P.get_cmap("RdBu"))
ax[1].set_title(r'$B_{\phi}$ [G]',fontsize=20)
ax[1].set_ylabel(r' latitude [$^{\circ} $]',fontweight='bold')
ax[1].set_xlabel(r' longitude [$^{\circ} $]',fontweight='bold')
fig.tight_layout()

fig.show()

