# computing magnetic energy
from eulag import *
from sys import exit
import matplotlib.pylab as P
import numpy as N
import time as ct

g = read_grid()
s = read_strat()

nt = f.u.shape[0]
year = 365.*24.*3600.
time = f.time/year

''' computing mean and turbulent fields '''

um  = N.mean(f.u,axis=1)
vm  = N.mean(f.v,axis=1)
wm  = N.mean(f.w,axis=1)
bxm = N.mean(f.bx,axis=1)
bym = N.mean(f.by,axis=1)
bzm = N.mean(f.bz,axis=1)

ut  = turb(f.u,um,g.n)
vt  = turb(f.v,vm,g.n)
wt  = turb(f.w,wm,g.n)
bxt = turb(f.bx,bxm,g.n)
byt = turb(f.by,bym,g.n)
bzt = turb(f.bz,bzm,g.n)

#total energy
Em  = f.bx**2 + f.by**2 + f.bz**2
Emm = bxm**2 
Emt = bxt**2 + byt**2 + bzt**2

#time series
Em_av = N.mean(Em,  axis=(1,2,3))
Emm_av = N.mean(Emm, axis=(1,2))
Emt_av = N.mean(Emt, axis=(1,2,3))

#P.plot(time,Em_av)
#P.plot(time,Emm_av, c='b')
#P.plot(time,Emt_av, c='r')
#P.show()

nn=10
mu = 4.*N.pi*1e-7
mu2 = 2.*mu
Em_r  =  N.mean(Em[-nn::,...], axis=(0,1,2))/mu2
Emm_r =   N.mean(Emm[-nn::,...], axis=(0,1))/mu2
Emt_r = N.mean(Emt[-nn::,...], axis=(0,1,2))/mu2

P.plot(g.r,Em_r)
P.plot(g.r,Emm_r,c='b')
P.plot(g.r,Emt_r,c='r')
P.show()

