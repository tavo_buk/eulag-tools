import numpy as N
import matplotlib.pylab as P
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc                  

# PLOT STYLE #                                                                                                         
                              
# Choose Computer Modern Roman fonts by default
matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 24}                         
rc('font', **font)                            
rc('xtick', labelsize = 22)                   
rc('ytick', labelsize = 22)                   
                                              
legend = {'fontsize': 14}                     
rc('legend',**legend)                         
axes = {'labelsize': 24}                      
rc('axes', **axes)                            
rc('mathtext',fontset='cm')                   
#use this, but at the expense of slowdown of rendering
rc('text', usetex = True)                     
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]

################################################################################

day = 24.*3600.
year = 365.*24.*3600.

################################################################################

# Active stars
FG_pRotI = day*N.array ([8.5,11.4,15.0,9.7,14.0,31.,12.3,22.5,22.7,13.9,29.8,10.9])
FG_pCycI = year*N.array([1.6,1.7,2.5,5.9,3.6,7.3,9.6,19.2,7.1,2.6,7.4,1.5])
FG_RhkI  = N.array     ([-4.6,-4.49,-4.66,-4.61,-4.92,-4.90,-4.75,-5.0,-4.93,-4.8,-5.0,-4.69])

K_pRotA = day*N.array([11.1,18.5, 21.1,21.0, 19.9])
K_pCycA = year*N.array([12.7,12.4,17.4,21.0,15.5])
K_RhkA  = N.array([-4.46,-4.48,-4.58,-4.66,-4.55])

# Inactive stars
FG_pRotA = day*N.array ([7.8,9.2,11.4,9.7,14.0,12.3,11.4,13.9])
FG_pCycA = year*N.array([9.1,5.6,12.2,12.2,12.9,16.6,10.9,16.9])
FG_RhkA  = N.array     ([-4.43,-4.42,-4.49,-4.61,-4.92,-4.75,-4.45,-4.80])

K_pRotI = day*N.array([44.,38.5,35.2,48.0,11.1,43.0,48.0,40.2,36.2,21.1,36.4,19.9,42.4,35.4,37.8,42.0,43.0])
K_pCycI = year*N.array([13.8,8.6,9.6,13.2,2.9,10.1,11.1,8.2,8.1,4.0,7.0,5.1,15.8,7.3,11.7,21.1,10.2])
K_RhkI = N.array([-4.99,-4.85,-4.91,-4.96,-4.46,-4.87,-4.95,-4.92,-4.93,-4.58,-4.79,-4.55,-4.96,-4.76,-4.89,-5.07,-4.94])

# Sun
sun_pRot = day*N.array([25.4,25.4,25.4])
sun_pCyc = year*N.array([80.0,11.0,2.0])
sun_Rhk  = N.array([-4.9,-4.9,-4.9])

# RC simulations
spRot = day*N.array([7.,14.,21.,28.,35.,42.,49.])
spCyc = year*N.array([2.8,9.8,28.1,17.1,19.2,22.8,24.8])

spRot2 = day*N.array([7.,14.,28.,35.,42.,49.])
spCyc2 = year*N.array([2.8,9.8,17.1,19.2,22.8,24.8])

Ro =  N.array([0.622,   1.35 ,   2.08 ,   2.77 ,   3.5  ,   4.28 ,   5.13 ])
Ro1 = N.array([0.475,  0.721,  1.05 ,  1.29 ,  1.47 ,  1.68 ,  1.87       ])
c = 2.338e-5
sRhk = c*(1./Ro1)**0.99

Ro12 = N.array([0.89,1.15,1.38])

# Ghizaru simulation
#gsRot = day*28.
#gsCyc = year*30.
#gsRo = 0.12
#gsRhk = c*(1./gsRo)**0.99

# Plotting
#P.title(r'BST diagram for F and G dwarfs and RC simulations',fontsize=18)

pRhk = N.arange(-5,-4.,0.2)
fig,ax=P.subplots()
P.xlim(-5.1,-4.15)
P.ylim(-3.2,-1.3)
ax.scatter(FG_RhkI,N.log10(FG_pRotI/FG_pCycI),c='r',marker=(5,1),label='Inactive G and F',s=95,alpha=0.5)
ax.scatter(FG_RhkA,N.log10(FG_pRotA/FG_pCycA),c='b',marker=(5,1),label='Active G and F',s=95,alpha=0.5)
ax.scatter(K_RhkI, N.log10( K_pRotI/ K_pCycI),c='r',marker=(5,2),label='Inactive K ',s=95,alpha=0.5)
ax.scatter(K_RhkA, N.log10( K_pRotA/ K_pCycA),c='b',marker=(5,2),label='Active K ',s=95,alpha=0.5)
ax.scatter(sun_Rhk, N.log10( sun_pRot/ sun_pCyc),c='orange',marker='o',label='Sun ',s=95,alpha=0.5)
ax.scatter(N.log10(sRhk),N.log10(spRot/spCyc),c='k',marker='s',label='RC-Sims',s=75)
#ax.scatter(N.log10(gsRhk),N.log10(gsRot/gsCyc),c='g', marker = 'o', s=95)
ax.plot(pRhk,0.59*pRhk + 1.0,'--',c='r')
ax.plot(pRhk,0.12*pRhk - 1.92,'--',c='b')
ax.axvline(x=-4.75,ls='dotted',c='k')
ax.axvline(x=-4.95,ls='dotted',c='k')
ax.text(-4.3,-2.35,r"${\bf A}$")
ax.text(-4.3,-1.45,r"${\bf I}$")
start,end = ax.get_xlim()
ax.xaxis.set_ticks(N.linspace(start,end,5))
start,end = ax.get_ylim()
ax.yaxis.set_ticks(N.linspace(start,end,5))

ax.xaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax.yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
P.xlabel(r"${\rm log} \langle R'_{HK}   \rangle $")
P.ylabel(r'${\rm log} (P_{\rm rot}/P_{\rm cyc})$')
#P.legend(loc=4,scatterpoints=1)
P.tight_layout()

P.show()
