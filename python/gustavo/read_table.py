import numpy as N
import os
import eulag as E

period = []
urms_NSL = []
urms_TAC = []
urms_CZ = []
ro = []
ro1 = []
bx_NSL = []
bx_TAC = []
bx_CZ = []
bp_NSL = []
bp_TAC = []
bp_CZ = []
domega = []
domega_omega = []
alpha_NSL = []
alpha_TAC = []
alpha_CZ = []
omr_NSL = []
omr_TAC = []
omr_CZ = []
omth_NSL = []
omth_TAC = []
omth_CZ = []
eta_NSL = []
eta_TAC = []
eta_CZ = []

table = open('/home/DATA/Solar/TAC/table.txt', 'r')
for line in table:
	columns = line.split()
	period.append(float(columns[0]))
	urms_NSL.append(float(columns[1]))
	urms_TAC.append(float(columns[2]))
	urms_CZ.append(float(columns[3]))
	ro.append(float(columns[4]))
	ro1.append(float(columns[5]))
	bx_NSL.append(float(columns[6]))
	bx_TAC.append(float(columns[7]))
	bx_CZ.append(float(columns[8]))
	bp_NSL.append(float(columns[9]))
	bp_TAC.append(float(columns[10]))
	bp_CZ.append(float(columns[11]))
	domega.append(float(columns[12]))
	domega_omega.append(float(columns[13]))
	alpha_NSL.append(float(columns[14]))
	alpha_TAC.append(float(columns[15]))
	alpha_CZ.append(float(columns[16]))
	omr_NSL.append(float(columns[17]))
	omr_TAC.append(float(columns[18]))
	omr_CZ.append(float(columns[19]))
	omth_NSL.append(float(columns[20]))
	omth_TAC.append(float(columns[21]))
	omth_CZ.append(float(columns[22]))
	eta_NSL.append(float(columns[23]))
	eta_TAC.append(float(columns[24]))
	eta_CZ.append(float(columns[25]))
table.close()

period = N.array(period) 
urms_NSL = N.array(urms_NSL) 
urms_TAC = N.array(urms_TAC) 
urms_CZ = N.array(urms_CZ) 
ro     = N.array(ro) 
bx_NSL = N.array(bx_NSL) 
bx_TAC = N.array(bx_TAC) 
bx_CZ = N.array(bx_CZ) 
bp_NSL = N.array(bp_NSL) 
bp_TAC = N.array(bp_TAC) 
bp_CZ = N.array(bp_CZ) 
domega = N.array(domega) 
domega_omega = N.array(domega_omega) 
alpha_NSL = N.array(alpha_NSL) 
alpha_TAC = N.array(alpha_TAC) 
alpha_CZ = N.array(alpha_CZ) 
omr_NSL = N.array(omr_NSL)
omr_TAC = N.array(omr_TAC)
omr_CZ = N.array(omr_CZ)
omth_NSL = N.array(omth_NSL)
omth_TAC = N.array(omth_TAC)
omth_CZ = N.array(omth_CZ)
eta_NSL = N.array(eta_NSL) 
eta_TAC = N.array(eta_TAC) 
eta_CZ = N.array(eta_CZ) 

#--------------------------------#
g = E.read_grid()
ir1, = N.where((g.rf > 0.93) & (g.rf < 0.95))
ir2, = N.where((g.rf > 0.68) & (g.rf < 0.75))
ircz, = N.where(g.rf > 0.72)

Lnsl  = g.r[ir1].max() - g.r[ir1].min()
Ltac = g.r[ir2].max() - g.r[ir2].min()
Lcz = g.r[ircz].max() - g.r[ircz].min()

#--------------------------------#
os.chdir('/home/DATA/Solar/TAC/tac-14-rf/')
s = E.read_strat() # Density from simulation, equal for all models
rho_NSL = N.mean(s.rho_ad[ir1])
rho_TAC = N.mean(s.rho_ad[ir2])
rho_CZ = N.mean(s.rho_ad[ircz])
os.chdir('/home/DATA/Solar/TAC/')

#----------- ENERGY -------------#
mu = 4*N.pi*1.e-7
em_NSL = (bx_NSL**2 + bp_NSL**2)/(2*mu)
em_TAC = (bx_TAC**2 + bp_TAC**2)/(2*mu)
em_CZ = (bx_CZ**2 + bp_CZ**2)/(2*mu)
ek_NSL = rho_NSL/2.*urms_NSL**2
ek_TAC = rho_TAC/2.*urms_TAC**2
ek_CZ = rho_CZ/2.*urms_CZ**2
f_NSL = em_NSL/ek_NSL
f_TAC = em_TAC/ek_TAC
f_CZ = em_CZ/ek_CZ

