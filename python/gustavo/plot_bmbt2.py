import numpy as N
import matplotlib.pylab as P
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc                  

# PLOT STYLE #                                                                                                         
matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 24}                         
rc('font', **font)                            
rc('xtick', labelsize = 24)                   
rc('ytick', labelsize = 24)                   
                                              
legend = {'fontsize': 20}                     
rc('legend',**legend)                         
axes = {'labelsize': 24}                      
rc('axes', **axes)                            
rc('mathtext',fontset='cm')                   
#use this, but at the expense of slowdown of rendering
rc('text', usetex = True)                     
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]
################################################################################

c = 2.338e-5
sRhk = c*(1./ro1)**0.99
btot = N.sqrt(bx_NSL**2 +bp_NSL**2)
line1 = -0.07*N.ones(20)
line10 = -1.1*N.ones(20)
xRo1 = N.linspace(-0.,0.3,20) 
xRo2 = N.linspace(0.33,0.9,20) 

#P.title("Tachocline magnetic fields --  RC models")
bmbt2 = N.load('bmbt2.npy')

fig,ax=P.subplots()
ax.scatter(N.log10(ro1[:]),N.log10(N.sqrt(bmbt2[:])),marker=(5,0),c='r',label=r'$\log (B_{\phi} [T]) |_{\rm nsl} $',s=100)
ax.plot(xRo1,line1,'--',c='k')
##P.plot(xRo1,1.2*xRo2-1.1,'--',c='k')
ax.plot(xRo2,-1.05*xRo2 + .5,'--',c='k')
ax.text(0.5,0.1,r"${\rm Ro}^{-1}$")
#P.text(0.4,-0.42,r"${\rm Ro}^{-0.7}$")
#P.text(-0.4,-0.62,r"${\rm Ro}^{1.2}$")
#ax.yaxis.set_ticks(N.linspace(-3.3,-0.5,5))
#ax.yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
P.xlim(-0.5,1.)
ax.xaxis.set_ticks(N.linspace(-0.5,1.,5))
ax.xaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
#P.ylim(-3.,-0.6)
P.xlabel(r'${\rm \log Ro}$')
P.ylabel(r'${\rm \log } \langle {\overline B}^2 \rangle / \langle b^2 \rangle  $')
#P.legend(loc=3,scatterpoints=1)
P.tight_layout()
P.show()

