import numpy as N
import matplotlib.pylab as P
import eulag as E

# verifing if the model is HD or MHD
filename = "define.h"
imhd = open(filename)
for line in imhd:
    if line.split()[1] == "MHD":
        MHD = int(line.split()[2])       # If MHD = 0 there is no magnetic field

if MHD == 1:
    print ('MHD config.')
else:
    print ('HD config.')
if (MHD == 1):
   root = '/home/DATA/Solar/TAC/'
   dir = ['tac-7-rf','tac-14-rf', 'tac-21-rf', 'tac-28-rf', 
         'tac-35-rf', 'tac-42-rf','tac-49-rf', 'tac-56-rf', 'tac-63-rf']
   lab = ['RC07', 'RC14', 'RC21', 'RC28', 'RC35', 'RC42', 'RC49', 'RC56', 'RC63']
   col = ['r','g','b','k','orange','c','m','k', 'y']
   year = 365.*24.*3600.
   day  = 24.*3600.
   pr = N.array([7.,14.,21.,28.,35.,42.,49.,56.,63])
else:
   root = '/home/DATA/Solar/HD/'
   dir = ['tac-7-rf','tac-14-rf', 'tac-21-rf', 'tac-28-rf', 
          'tac-35-rf', 'tac-42-rf','tac-49-rf'] 
   lab = ['RC07', 'RC14', 'RC21', 'RC28', 'RC35', 'RC42','RC49']
   col = ['r','g','b','k','orange','c','m']
   year = 365.*24.*3600.
   day  = 24.*3600.
   pr = N.array([7.,14.,21.,28.,35.,42.,49.])
   istart = N.array([0,550,480,0,500,390,0])

nd = len(dir)
file = '/scales.npy'
k = N.arange(64)

P.xlim(0.63,0.955)
P.ylim(5e6,1.5e11)
for id in range (nd): 
    g = E.read_grid()
    leddie,tauc_r,etat_r = N.load(root+dir[id]+file)
    P.plot(g.rf,(etat_r), col[id], linewidth=3, label=lab[id])

P.show()
P.xlabel(r'$r [R_{\odot}]$')
P.ylabel(r'$\eta_{\rm t} [{\rm m}^2/{\rm s}]$')
P.legend(loc=2,fontsize=13)
P.tight_layout()

