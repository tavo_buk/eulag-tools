import eulag as E
import numpy as N
import matplotlib.pylab as P
from scipy.fftpack import fft
import os

def comp_period(f,ist):
    g = E.read_grid()
    ir1, = N.where((g.rf > 0.9) & (g.rf < 0.96))
    ir2, = N.where((g.rf > 0.62) & (g.rf < 0.76))
    year = 365.*24.*3600.

    # field to plot
    y2 = f.by**2 + f.bz**2
    #y2 = f.bx**2 
    y = (N.mean(y2[-ist:,2:31,ir2],axis=(1,2)))
    time_years = f.time[-ist:]/year
    
    
    sampling_interval = g.t_xav/year
    n_field = (y.shape)[0]
    freq_nyquist = 0.5/sampling_interval
    freqs = N.arange(n_field/2)/(n_field/2.-1)*freq_nyquist

    freqs2 = N.fft.fftfreq(n_field/2)/(n_field/2-1)*freq_nyquist

    print 'field size = ', n_field
    print 'freq size = ',  freqs2.size, freqs2[0],freqs2[-1]
    freq_nodc = freqs[1:n_field/2]
    period = []
    pspec_nodc_array = []
    k = 0
    
    bperiod = y
    mspec = N.abs(N.fft.fft(bperiod))
    pspec = mspec**2
    pspec_nodc = 2.*pspec[1:n_field/2]
    i_peak = N.argmax(pspec_nodc) 
    freq = freq_nodc[i_peak]
    period = 1./freq
    
    print '***computing the period'
    print 'gt_xav = ', sampling_interval
    print 'cycle period, k = ', period, i_peak
    N.save('period_bottom',(freq_nodc,pspec_nodc))
    
    fig, (ax1, ax2) = P.subplots(2, 1)
    ax1.set_yscale(u'linear')
    ax1.set_xscale(u'linear')
    #ax1.set_ylim(0,0.04)
    ax1.set_xlim(time_years.min(),time_years.max())
    ax1.plot(time_years,y)
    #
    ax2.set_yscale(u'log', nonposx='clip')
    ax2.set_xscale(u'log', nonposx='clip')
    #ax2.set_ylim(1e-1,1e3)
    ax2.set_xlim(0.01,4)
    ax2.plot(freq_nodc,pspec_nodc)
    P.show()
    return period


# Main Program
root = '/storage/TAC/'
dir = ['tac-7-rf','tac-14-rf', 'tac-21-rf', 'tac-24-rf', 'tac-28-rf', 
      'tac-35-rf', 'tac-42-rf','tac-49-rf']

pr  = N.array([ 7.,  14.,  21.,  24.,  28.,  35.,  42.,  49.])
ist = N.array([500,  500,  500,  400,  700,  700,  700,  800])

nd = len(dir)
period = []

for id in range (3,8): 
    os.chdir(root+dir[id])
    print 'dir = ', os.getcwd(), 'reading fort.11 ... '
    f = E.rxav('xav.dat')
    p = comp_period(f,ist[id])
    period.append(p)

period =  N.array(period)
