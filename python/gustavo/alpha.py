'''
script to compute the kinetic and magnetic alpha effect,
it results in a 2D array (r and th) with u'.(curl u') and b'.(curl b')
'''

from eulag import *
from sys import exit
import matplotlib.pylab as P
import numpy as N
import time as ct

# verifing if the models is HD or MHD
filename = "define.h"
imhd = open(filename)
for line in imhd:
    if line.split()[1] == "MHD":
        MHD = int(line.split()[2])       # If MHD = 0 there is no magnetic field

if MHD == 1:
    print ('MHD config.')
else:
    print ('HD config.')


g = read_grid()
s = read_strat()

nt = f.u.shape[0]
year = 365.*24.*3600.
time = f.time/year
tgood1 = int(input("after what time the simulation is steady? = "))
tgood2 = int(input("after what time the simulation is steady? = "))
nn, = N.where((time > tgood1) & (time < tgood2))
print ("it corresponds to the last", nn.size, "steps")
start= ct.clock()

''' computing mean and turbulent fields '''

um  = N.mean(f.u,axis=1)
vm  = N.mean(f.v,axis=1)
wm  = N.mean(f.w,axis=1)
if (MHD == 1):
   bxm = N.mean(f.bx,axis=1)
   bym = N.mean(f.by,axis=1)
   bzm = N.mean(f.bz,axis=1)

ut  = turb(f.u)
vt  = turb(f.v)
wt  = turb(f.w)
if (MHD == 1):
   bxt = turb(f.bx)
   byt = turb(f.by)
   bzt = turb(f.bz)

''' then the turbulent vorticity and current 
    and finally the kinetic and current helicities
'''

wphit=N.zeros((nn.size,g.phi.size,g.th.size,g.r.size))
wtht =N.zeros((nn.size,g.phi.size,g.th.size,g.r.size))
wrt  =N.zeros((nn.size,g.phi.size,g.th.size,g.r.size))
khel =N.zeros((nn.size,g.phi.size,g.th.size,g.r.size))

if (MHD == 1):
   jphit=N.zeros((nn.size,g.phi.size,g.th.size,g.r.size))
   jtht =N.zeros((nn.size,g.phi.size,g.th.size,g.r.size))
   jrt  =N.zeros((nn.size,g.phi.size,g.th.size,g.r.size))
   chel =N.zeros((nn.size,g.phi.size,g.th.size,g.r.size))

for it in range(nn.size):
    itm = (nt - nn.size) + it
    wphit[it,...],wtht[it,...],wrt[it,...] = \
            curl(ut[itm,...],vt[itm,...],wt[itm,...],g.phi,g.th,g.r,coord='sph')
    khel[it,...] = dot(ut[itm,...],vt[itm,...],wt[itm,...],\
            wphit[it,...],wtht[it,...],wrt[it,...])
    if (MHD == 1):
       jphit[it,...],jtht[it,...],jrt[it,...] = \
               curl(bxt[itm,...],byt[itm,...],bzt[itm,...],g.phi,g.th,g.r,coord='sph')
       chel[it,...] = dot(bxt[itm,...],byt[itm,...],bzt[itm,...],\
               jphit[it,...],jtht[it,...],jrt[it,...])
    print (it, itm)


khm = N.mean(khel,axis=(0,1))
if (MHD == 1):
   chm = N.mean(chel,axis=(0,1))
   nu0 = 4.*pi*1e-7
   nu0rho_1 =N.zeros((g.th.size,g.r.size))
   for j in range(g.th.size):
       for k in range(g.r.size):
           nu0rho_1[j,k] = 1./(nu0*s.rho_am[k])
   chm = chm*nu0rho_1

N.save('khelicity',khm)
if (MHD == 1):
   N.save('chelicity',chm)

end = ct.clock()
time = (end-start)
print ('time = ', time)
