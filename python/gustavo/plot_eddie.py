import numpy as N
import matplotlib.pylab as P
import eulag as E
from scipy.ndimage.filters import gaussian_filter
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc                  

# PLOT STYLE #                                                                                                         
matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 24}                         
rc('font', **font)                            
rc('xtick', labelsize = 23)                   
rc('ytick', labelsize = 23)                   

#use this, but at the expense of slowdown of rendering
rc('text', usetex = True)
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]

# verifing if the model is HD or MHD
filename = "define.h"
imhd = open(filename)
for line in imhd:
    if line.split()[1] == "MHD":
        MHD = int(line.split()[2])       # If MHD = 0 there is no magnetic field

colormap = P.cm.Spectral #nipy_spectral, Set1,Paired   

if MHD == 1:
    print ('MHD config.')
else:
    print ('HD config.')
if (MHD == 1):
   root = '/storage/TAC/'
   dir = ['tac-7-rf','tac-18-rf', 'tac-21-rf', 'tac-24-rf', 
         'tac-35-rf', 'tac-42-rf','tac-49-rf', 'tac-56-rf', 'tac-63-rf']
   lab = ['RC07', 'RC18', 'RC21', 'RC24', 'RC35', 'RC42', 'RC49', 'RC56', 'RC63']
   col = ['r','g','b','k','orange','c','m','tomato','darkcyan']
#   col = [colormap(i) for i in N.linspace(0, 1,9)]
#   col = ['r','g','b','k','orange','c','m','k', 'y']
#   col = [P.cm.RdYlBu(i) for i in range(9)]
   year = 365.*24.*3600.
   day  = 24.*3600.
   pr = N.array([7.,18.,21.,24.,35.,42.,49.,56.,63])
else:
   root = '/home/DATA/Solar/HD/'

nd = len(dir)
file = '/scales.npy'
k = N.arange(64)


P.figure(figsize = (15,10))
P.subplot(221)
P.xlim(0.65,0.955)
P.ylim(1.,6.)
for id in range (nd): 
    g = E.read_grid()
    urms_r,vA,leddie,lcurr,tauc_r,tauc_a = N.load(root+dir[id]+file)
    Ro = day*pr[id]/tauc_r
    P.plot(g.rf,1e-7*gaussian_filter(leddie,0.7), color=col[id], linewidth=3, label=lab[id])
#    P.plot(g.rf,gaussian_filter(lcurr,0.8) , color=col[id],linestyle='dashed', linewidth=3, label=lab[id])
#P.yscale('log')
P.tick_params(axis='x',which='both',labelbottom=False)
P.title(r'{\bf (a)}',loc='left',fontsize=20)
P.ylabel(r'$\ell_k \times 10^7 \; [{\rm m}] $')

P.subplot(222)
P.xlim(0.65,0.955)
P.ylim(1e5,3e7)
for id in range (nd): 
    g = E.read_grid()
    urms_r,vA,leddie,lcurr,tauc_r,tauc_a = N.load(root+dir[id]+file)
    P.plot(g.rf,(tauc_r), color=col[id], linewidth=3, label=lab[id])
    P.plot(g.rf,(tauc_a), color=col[id],linestyle='dashed', linewidth=3, label=lab[id])
P.yscale('log')
P.tick_params(axis='x',which='both',labelbottom=False)
P.title(r'{\bf (b)}',loc='left',fontsize=20)
P.ylabel(r'$\tau_{\rm c}\;,\tau_{\rm a}\;    [{\rm s}]$')

P.subplot(223)
P.xlim(0.65,0.955)
P.ylim(0,110)
for id in range (nd): 
    g = E.read_grid()
    urms_r,vA,leddie,lcurr,tauc_r,tauc_a = N.load(root+dir[id]+file)
    P.plot(g.rf,urms_r,color= col[id], linewidth=3, label=lab[id])
#   P.plot(g.rf,vA,    color= col[id],linestyle='dashed', linewidth=3, label=lab[id])
P.xlabel(r'$r [R_{\odot}]$')
P.ylabel(r'$u_{\rm rms} \; [{\rm m}/{\rm s}]$')
P.title(r'{\bf (c)}',loc='left',fontsize=20)

P.subplot(224)
P.xlim(0.65,0.955)
P.ylim(2e7,2e9)
for id in range (nd): 
    g = E.read_grid()
    alpha_k,alpha_m,etat_r = N.load(root+dir[id]+'/alpha.npy')
    P.plot(g.rf,N.mean(etat_r,axis=0), color=col[id], linewidth=3, label=lab[id])
P.yscale('log')
P.xlabel(r'$r [R_{\odot}]$')
P.ylabel(r'$\eta_{\rm t} \; [{\rm m}^2/{\rm s}]$')
P.title(r'{\bf (d)}',loc='left',fontsize=20)
P.legend(loc=4,ncol=3,fontsize=13)
P.savefig("elltau.eps",format='eps',dpi=400,bbox_inches='tight')
P.show()

