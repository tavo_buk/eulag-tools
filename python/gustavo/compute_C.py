from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib
from matplotlib.ticker import FormatStrFormatter
from matplotlib import rc
from scipy.ndimage.filters import gaussian_filter
import numpy as N
import matplotlib.pyplot as P
from eulag import * 
#from mpl_toolkits.basemap import Basemap
from pylab import meshgrid
from numpy import pi, sin, cos
import cmocean
from mpl_toolkits.basemap import Basemap
import os

matplotlib.rcParams['font.family'] = 'serif'
matplotlib.rcParams['font.serif'] = 'sans-serif'

font = { 'size' : 12}
rc('font', **font)
rc('xtick', labelsize = 12)
rc('ytick', labelsize = 12)

legend = {'fontsize': 12, 'numpoints' : 3}
rc('legend',**legend)
axes = {'labelsize': 12, 'titlesize' : 12}
rc('axes', **axes)
rc('mathtext',fontset='cm')
rc('lines', linewidth = 2)
rc('patch', linewidth = .5)

############################################################3

#def compute_a():
g = read_grid()
ir1, = N.where((g.rf > 0.9) & (g.rf < 0.96))
ir2, = N.where((g.rf > 0.76) & (g.rf < 0.89))
ir3, = N.where((g.rf > 0.70) & (g.rf < 0.76))
s = read_strat()
cdir = os.getcwd()

root = '/storage/TAC/'
if cdir == root+'tac-21-rf':
   title = '(a) ' + r'${\bf RC21} \;\;$'
   cdir = 'RC21'
if cdir == root+'tac-24-rf':
   title = '(a) ' + r'${\bf RC24} \;\;$'
   cdir = 'RC24'
if cdir == root+'tac-28-rf':
   title = '(a) ' + r'${\bf RC28}$'
   cdir = 'RC28'
   time0 = 100.
   tini = 120.
   tend = 165.
if cdir == root+'tac-35-rf':
   title = '(b) ' + r'${\bf RC35} \;\;$'
   cdir = 'RC35'
   time0 = 120.
   tini = 120.
   tend = 170.
if cdir == root+'tac-42-rf':
   title = '(b) ' + r'${\bf RC42} \;\;$'
   cdir = 'RC42'
if cdir == root+'tac-49-rf':
   title = '(c) ' + r'${\bf RC49} \;\;$'
   cdir = 'RC49'
   time0 = 250.
   tini = 250
   tend = 310.
if cdir == root+'tac-63-rf':
   title = '(c) ' + r'${\bf RC63} \;\;$'
   cdir = 'RC63'
   time0 = 250.
   tini = 250
   tend = 320.

#f = E.rf11m(ifl=True)

u   =   gaussian_filter(f.u[:,:,:],0.0)
v   =   gaussian_filter(f.v[:,:,:],0.0)
w   =   gaussian_filter(f.w[:,:,:],0.0)
bxm =  gaussian_filter(f.bx[:,:,:],0.0)  
bym =  gaussian_filter(f.by[:,:,:],0.0)
bzm =  gaussian_filter(f.bz[:,:,:],0.0)

''' time series to decide the goodness of the data '''
year = 365.*24.*3600.
time = f.time[-500:]/year + time0
dt = (time[1] - time[0])*year/10.
nelements = time.size

''' alpha effect -- FOSA '''
a_k,a_m,etat_r = N.load('alpha.npy')
khm = N.load('khel.npy')
khm = khm.mean(axis=(1))
alpha_k = N.zeros_like(khm)
chm = N.load('chel.npy')
chm = chm.mean(axis=(1))
alpha_m = N.zeros_like(khm)

urms_r, vA, leddie, lcurr, tauc_r, tauc_a = N.load('scales.npy')

alpha_k[:,:,0:-1] = gaussian_filter((-khm[:,:,0:-1]*tauc_a[0:-1]/3.),0.7)
alpha_m[:,:,0:-1] = gaussian_filter(( chm[:,:,0:-1]*tauc_a[0:-1]/3.),0.7)

#etat_r *= 0.05
D = (alpha_m+alpha_k)

ur   = N.zeros_like(bxm)
uth  = N.zeros_like(bxm)
uphi = N.zeros_like(bxm)
shear_r   = N.zeros_like(bxm)
shear_th  = N.zeros_like(bxm)
shear_phi = N.zeros_like(bxm)
mer_r   = N.zeros_like(bxm)
mer_th  = N.zeros_like(bxm)
mer_phi = N.zeros_like(bxm)
curb_r = N.zeros_like(bxm)
curb_th = N.zeros_like(bxm)
curb_phi = N.zeros_like(bxm)

br0   = N.zeros_like(bxm)
bth0  = N.zeros_like(bxm)
bphi0 = N.zeros_like(bxm)
br1   = N.zeros_like(bxm)
bth1  = N.zeros_like(bxm)
bphi1 = N.zeros_like(bxm)
br2   = N.zeros_like(bxm)
bth2  = N.zeros_like(bxm)
bphi2 = N.zeros_like(bxm)
br2   = N.zeros_like(bxm)
bth2  = N.zeros_like(bxm)
bphi2 = N.zeros_like(bxm)
br3   = N.zeros_like(bxm)
bth3  = N.zeros_like(bxm)
bphi3 = N.zeros_like(bxm)

for it in range(nelements):
    shear_phi[it,...],shear_th[it,...],shear_r[it,...] = cross(u[it,...],0.*v[it,...],0.*w[it,...],bxm[it,...],bym[it,...],bzm[it,...])   
    mer_phi[it,...],mer_th[it,...],mer_r[it,...] = cross(0.*u[it,...],v[it,...],w[it,...],bxm[it,...],bym[it,...],bzm[it,...])   
    curb_phi[it,...],curb_th[it,...],curb_r[it,...] = curl2d(bxm[it,...],bym[it,...],bzm[it,...],g.phi,g.th,g.r,coord='sph')
    bphi0[it,...],bth0[it,...],br0[it,...] = curl2d(-shear_phi[it,...],-shear_th[it,...],-shear_r[it,...],g.phi,g.th,g.r,coord='sph')
    bphi1[it,...],bth1[it,...],br1[it,...] = curl2d(-mer_phi[it,...],-mer_th[it,...],-mer_r[it,...],g.phi,g.th,g.r,coord='sph')
    bphi2[it,...],bth2[it,...],br2[it,...] = curl2d(D[it,:,:]*bxm[it,:,:],D[it,:,:]*bym[it,:,:],D[it,:,:]*bzm[it,:,:],g.phi,g.th,g.r,coord='sph')
    bphi3[it,...],bth3[it,...],br3[it,...] = curl2d(etat_r*curb_phi[it,:,:], etat_r*curb_th[it,:,:],etat_r*curb_r[it,:,:],g.phi,g.th,g.r,coord='sph')

N.save('B_phi_mf',(bphi0,bphi1,bphi2,bphi3))
N.save('B_th_mf', (bth0,bth1,bth2,bth3))
N.save('B_r_mf',  (br0,br1,br2,br3))

# Graphics

theta = N.linspace(-90, 90, num = g.m, endpoint = True)

tini = tini
tend = tend
yini = 0.
yend = 90.

fig, axes = P.subplots(5,1, linewidth = 5, sharex = True,figsize=(4,9))

# plotting the shear term
f1  = f.by[:,:,ir1].mean(axis=2)

lev  = N.linspace(-f1.max(),f1.max(),64) 
im1 = axes[0].contourf(time[:], theta, N.rot90(f1), levels = lev, cmap='RdYlBu', extend='both')  
axes[0].contour(time[:], theta, N.rot90(f.by[:,:,ir1].mean(axis=2)), 9, colors='k',extend='both',linewidths=0.75)  
axes[0].set_ylabel(r'$90^{\circ} - \theta \; (^{\circ}) $',fontweight='bold')
axes[0].set_yticks([90, 45, 0])
axes[0].set_xticks(N.linspace(tini, tend, 5, endpoint = True))
axes[0].xaxis.set_major_formatter(FormatStrFormatter('$%3.0d$'))
axes[0].set_yticklabels([r"$90^{\circ}$",r"$45^{\circ}$",r"$0^{\circ}$",r"$-45^{\circ}$",r"$-90^{\circ}$"])
#axes[0].set_yticklabels([r"$90^{\circ}$",r"$45^{\circ}$",r"$0^{\circ}$"])
axes[0].tick_params(which = 'major', direction = 'inout', labelsize = 12, pad = 10, width = 3, length = 6)
axes[0].set_xlim(tini,tend)
axes[0].set_ylim(yini,yend)
axes[0].set_title(title,loc='left')
axes[0].set_title(r'$  \overline{B}_{r}  \;,\; \overline{B}_{\phi}$',loc='right')

divider1 = make_axes_locatable(axes[0])
cax1 = divider1.append_axes("right", size = "3%", pad = 0.2)
tick1 = N.linspace(lev[0], lev[-1], 3)
#cbar_ax1 = fig.colorbar(im1, ticks = tick1, cax = cax1, format = '%2.1f', label = r"$ du_{\mathrm{r}} \mathrm{[m/s]} $")
cbar_ax1 = fig.colorbar(im1, ticks = tick1, cax = cax1, format = '%3.1f') 
cbar_ax1.ax.tick_params(which = 'major', direction = 'inout', labelsize = 12, pad = 6, width = 2, length = 6)
cbar_ax1.ax.set_rasterization_zorder(-10)

###################################################################################

f2  = f.by[:,:,ir2].mean(axis=2)
#f2  = (bth1+bth2)[:,:,ir2].mean(axis=2)/1e-8
lev  = N.linspace(-f2.max(),f2.max(),64) 
im2 = axes[1].contourf(time[:], theta, N.rot90(f2), levels = lev , cmap="RdYlBu" , extend='both')  
axes[1].contour(time[:], theta, N.rot90(f.by[:,:,ir2].mean(axis=2)), 9, colors='k',extend='both', linewidths=0.75 )  
axes[1].set_rasterization_zorder(-10)
axes[1].set_ylabel(r'$90^{\circ} - \theta \; (^{\circ}) $',fontweight='bold')
axes[1].set_yticks([90, 45, 0, -45, -90])
axes[1].set_xticks(N.linspace(tini, tend, 5, endpoint = True))
#axes[1].xaxis.set_major_formatter(FormatStrFormatter('$%3.0d$'))
axes[1].set_yticklabels([r"$90^{\circ}$",r"$45^{\circ}$",r"$0^{\circ}$",r"$-45^{\circ}$",r"$-90^{\circ}$"])
axes[1].set_yticklabels([r"$90^{\circ}$",r"$45^{\circ}$",r"$0^{\circ}$"])
axes[1].tick_params(which = 'major', direction = 'inout', labelsize = 12, pad = 10, width = 3, length = 6)
axes[1].set_title(r'$(r \sin\theta \overline{\bf B}_p \cdot \nabla \overline{\Omega})  \;,\; \overline{B}_{\phi}$',loc='right')
axes[1].set_xlim(tini,tend)
axes[1].set_ylim(yini,yend)

divider1 = make_axes_locatable(axes[1])
cax1 = divider1.append_axes("right", size = "3%", pad = 0.2)
tick1 = N.linspace(lev[0], lev[-1], 3)
#cbar_ax1 = fig.colorbar(im1, ticks = tick1, cax = cax1, format = '%2.1f', label = r"$ du \mathrm{[m/s]}$")
cbar_ax1 = fig.colorbar(im2, ticks = tick1, cax = cax1, format = '%3.1f')
cbar_ax1.ax.tick_params(which = 'major', direction = 'inout', labelsize = 12, pad = 6, width = 2, length = 6)
cbar_ax1.ax.set_rasterization_zorder(-10)

xlabel=False
if xlabel == True:
    axes[1].set_xlabel(r'$ t \; \mathrm{[yr]} $', fontsize = 20, va = 'center', labelpad = 15)
axes[1].tick_params(which = 'major', direction = 'inout', labelsize = 12, pad = 10, width = 2, length = 6)
#axes[1].set_title('(b) '+ r'$r=$'+str(format(g.rf[ir1],'9.2f')),loc='left')

f3  = f.by[:,:,ir3].mean(axis=2)
lev  = 1*N.linspace(-f3.max(),f3.max(),64) 
im3 = axes[2].contourf(time[:], theta, N.rot90(f3), levels = lev , cmap="RdYlBu", extend='both')  
axes[2].contour(time[:], theta, N.rot90(f.by[:,:,ir3].mean(axis=2)), 9, colors='k',extend='both', linewidths=0.75 )  
axes[2].set_rasterization_zorder(-10)
axes[2].set_ylabel(r'$90^{\circ} - \theta \; (^{\circ}) $',fontweight='bold')
axes[2].set_yticks([90, 45, 0, -45, -90])
axes[2].set_xticks(N.linspace(tini, tend, 5, endpoint = True))
#axes[2].xaxis.set_major_formatter(FormatStrFormatter('$%3.0d$'))
axes[2].set_yticklabels([r"$90^{\circ}$",r"$45^{\circ}$",r"$0^{\circ}$",r"$-45^{\circ}$",r"$-90^{\circ}$"])
axes[2].set_yticklabels([r"$90^{\circ}$",r"$45^{\circ}$",r"$0^{\circ}$"])
axes[2].tick_params(which = 'major', direction = 'inout', labelsize = 12, pad = 10, width = 3, length = 6)
axes[2].set_title(r'$(\nabla \times \alpha \overline{\bf B})|_{\phi} \;,\; \overline{B}_{\phi} $',loc='right')
axes[2].set_xlim(tini,tend)
axes[2].set_ylim(yini,yend)

divider1 = make_axes_locatable(axes[2])
cax1 = divider1.append_axes("right", size = "3%", pad = 0.2)
tick1 = N.linspace(lev[0], lev[-1], 3)
#cbar_ax1 = fig.colorbar(im1, ticks = tick1, cax = cax1, format = '%2.1f', label = r"$ du \mathrm{[m/s]}$")
cbar_ax1 = fig.colorbar(im3, ticks = tick1, cax = cax1, format = '%3.1f') 
cbar_ax1.ax.tick_params(which = 'major', direction = 'inout', labelsize = 12, pad = 6, width = 2, length = 6)
cbar_ax1.ax.set_rasterization_zorder(-10)

xlabel=False
if xlabel == True:
    axes[2].set_xlabel(r'$ t \; \mathrm{[yr]} $', fontsize = 20, va = 'center', labelpad = 15)
axes[2].tick_params(which = 'major', direction = 'inout', labelsize = 12, pad = 10, width = 2, length = 6)

f4  = f.bx[:,:,ir2].mean(axis=2)
lev  = 1.*N.linspace(-f4.max(),f4.max(),64) 
im4 = axes[3].contourf(time[:], theta, N.rot90(f4), levels = lev , cmap="RdYlBu", extend='both')  
axes[3].contour(time[:], theta, N.rot90(f.bx[:,:,ir2].mean(axis=2)), 12, colors='k',extend='both', linewidths=0.75)  
axes[3].set_rasterization_zorder(-10)
axes[3].set_ylabel(r'$90^{\circ} - \theta \; (^{\circ}) $',fontweight='bold')
axes[3].set_yticks([90, 45, 0, -45, -90])
axes[3].set_xticks(N.linspace(tini, tend, 5, endpoint = True))
axes[3].xaxis.set_major_formatter(FormatStrFormatter('$%3.0d$'))
axes[3].set_yticklabels([r"$90^{\circ}$",r"$45^{\circ}$",r"$0^{\circ}$",r"$-45^{\circ}$",r"$-90^{\circ}$"])
#axes[3].set_yticklabels([r"$90^{\circ}$",r"$45^{\circ}$",r"$0^{\circ}$"])
axes[3].tick_params(which = 'major', direction = 'inout', labelsize = 12, pad = 10, width = 3, length = 6)
axes[3].set_title(r'$(\nabla \times \alpha \overline{\bf B})|_{r} \;,\; \overline{B}_{r}$',loc='right')
axes[3].set_xlim(tini,tend)
axes[3].set_ylim(yini,yend)

divider1 = make_axes_locatable(axes[3])
cax1 = divider1.append_axes("right", size = "3%", pad = 0.2)
tick1 = N.linspace(lev[0], lev[-1], 3)
#cbar_ax1 = fig.colorbar(im1, ticks = tick1, cax = cax1, format = '%2.1f', label = r"$ du \mathrm{[m/s]}$")
cbar_ax1 = fig.colorbar(im4, ticks = tick1, cax = cax1, format = '%3.1f') 
cbar_ax1.ax.tick_params(which = 'major', direction = 'inout', labelsize = 12, pad = 6, width = 2, length = 6)
cbar_ax1.ax.set_rasterization_zorder(-10)


xlabel=False
if xlabel == True:
    axes[3].set_xlabel(r'$ t \; \mathrm{[yr]} $', fontsize = 20, va = 'center', labelpad = 15)
axes[3].tick_params(which = 'major', direction = 'inout', labelsize = 12, pad = 10, width = 2, length = 6)

f5  = f.bx[:,:,ir3].mean(axis=2)
lev  =0.5*N.linspace(-f5.max(),f5.max(),64) 
im5 = axes[4].contourf(time[:], theta, N.rot90(f5), levels = lev , cmap='RdYlBu', extend='both')  
axes[4].contour(time[:], theta, N.rot90(f.bx[:,:,ir3].mean(axis=2)), 10, colors='k',extend='both', linewidths=0.75)  
axes[4].set_rasterization_zorder(-10)
axes[4].set_ylabel(r'$90^{\circ} - \theta \; (^{\circ}) $',fontweight='bold')
axes[4].set_yticks([90, 45, 0, -45, -90])
axes[4].set_xticks(N.linspace(tini, tend, 5, endpoint = True))
axes[4].xaxis.set_major_formatter(FormatStrFormatter('$%3.0d$'))
axes[4].set_yticklabels([r"$90^{\circ}$",r"$45^{\circ}$",r"$0^{\circ}$",r"$-45^{\circ}$",r"$-90^{\circ}$"])
#axes[4].set_yticklabels([r"$90^{\circ}$",r"$45^{\circ}$",r"$0^{\circ}$"])
axes[4].tick_params(which = 'major', direction = 'inout', labelsize = 12, pad = 10, width = 3, length = 6)
#axes[4].set_title('(d) '+ r'$r=$'+str(format(g.rf[ir4],'9.2f')),loc='left')
axes[4].set_title(r'$\alpha \;,\; \overline{B}_{\phi}  $',loc='right')
axes[4].set_xlim(tini,tend)
axes[4].set_ylim(yini,yend)

divider1 = make_axes_locatable(axes[4])
cax1 = divider1.append_axes("right", size = "3%", pad = 0.2)
tick1 = N.linspace(lev[0], lev[-1], 3)
#cbar_ax1 = fig.colorbar(im1, ticks = tick1, cax = cax1, format = '%2.1f', label = r"$ du \mathrm{[m/s]}$")
cbar_ax1 = fig.colorbar(im5, ticks = tick1, cax = cax1, format = '%2.1f') 
cbar_ax1.ax.tick_params(which = 'major', direction = 'inout', labelsize = 12, pad = 6, width = 2, length = 6)
cbar_ax1.ax.set_rasterization_zorder(-10)

xlabel=True
if xlabel == True:
    axes[4].set_xlabel(r'$ t \; \mathrm{[yr]} $', fontsize = 12, va = 'center', labelpad = 15)
axes[4].tick_params(which = 'major', direction = 'inout', labelsize = 12, pad = 10, width = 2, length = 6)

###################################################################################
fig.subplots_adjust(hspace = .001)
fig.tight_layout()
P.show()
#import os
#os.chdir('../')
fig_name = 'sources_NSL_'+cdir+'.png'
fig.savefig(fig_name, bbox_inches='tight')

