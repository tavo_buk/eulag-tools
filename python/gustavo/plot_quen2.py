import numpy as N
import matplotlib.pylab as P
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc                  

# PLOT STYLE #                                                                                                         
matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 22}                         
rc('font', **font)                            
rc('xtick', labelsize = 24)                   
rc('ytick', labelsize = 24)                   
                                              
legend = {'fontsize': 18}                     
rc('legend',**legend)                         
axes = {'labelsize': 21}                      
rc('axes', **axes)                            
rc('mathtext',fontset='cm')                   
#use this, but at the expense of slowdown of rendering
rc('text', usetex = True)                     
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]
################################################################################

line15 = -1.4*N.ones(20)
line10 = -0.95*N.ones(20)
xRo1 = N.linspace(-0.65,0.3,20) 
xRo2 = N.linspace(-0.65,0.1,20) 
xRo3 = N.linspace(0.15,0.5,20) 

L = 6.96e8

comr_NSL = L*L*L*omr_NSL/eta_CZ 
comr_CZ  = L*L*L*omr_CZ /eta_CZ
comr_TAC = L*L*L*omr_TAC/eta_CZ

comt_NSL = L*L*L*omth_NSL/eta_CZ 
comt_CZ  = L*L*L*omth_CZ /eta_CZ
comt_TAC = L*L*L*omth_TAC/eta_CZ

#P.title("Tachocline magnetic fields --  RC models")
ftig,ax=P.subplots()
P.plot(    N.log10(ro1/(2.*N.pi)),(comr_NSL ),'-',c='r',linewidth=2)
ax.scatter(N.log10(ro1/(2.*N.pi)),(comr_NSL ),marker='o',c='r', label=r'$\langle C_{\Omega}^r \rangle^{NSL}$',s=80)
P.plot(    N.log10(ro1/(2.*N.pi)),(comr_TAC ),'-',c='b',linewidth=2)
ax.scatter(N.log10(ro1/(2.*N.pi)),(comr_TAC ),marker='o',c='b', label=r'$\langle C_{\Omega}^r \rangle^{TAC}$',s=80)
P.plot(    N.log10(ro1/(2.*N.pi)),(comt_NSL),c='tomato',linewidth=2)
ax.scatter(N.log10(ro1/(2.*N.pi)),(comt_NSL),marker='o',c='tomato', label=r'$\langle C_{\Omega}^{\theta} \rangle^{NSL})$',s=80)
P.plot(    N.log10(ro1/(2.*N.pi)),(comt_TAC),c='darkcyan',linewidth=2)
ax.scatter(N.log10(ro1/(2.*N.pi)),(comt_TAC),marker='o',c='darkcyan', label=r'$\langle C_{\Omega}^{\theta} \rangle^{TAC})$',s=80)

rot = N.log10(ro1[:]/(2.*N.pi))
cat = 11.2*rot - 0.63
cor = 627.8*rot - 3.1
cot = 78.2*rot + 128.

P.plot(rot,cor,'-',c='b',       linestyle='dashed',linewidth=1)
P.plot(rot,cot,'-',c='darkcyan',linestyle='dashed',linewidth=1)



line0 = N.zeros(20)
xy0 = N.linspace(xini,xend,20) 
ax.plot(xy0,line0,linestyle='dotted',c='k')

#P.plot(N.log10(hd_ro),N.log10(hd_omr_NSL),'--',c='r',linewidth=1)
#ax.scatter(N.log10(hd_ro),N.log10(hd_omr_NSL),marker='.',c='r',s=80,alpha=0.5)
#P.plot(N.log10(hd_ro),N.log10(hd_omr_TAC),'--',c='b',linewidth=1)
#ax.scatter(N.log10(hd_ro),N.log10(hd_omr_TAC),marker='.',c='b', s=80,alpha=0.5)
#P.plot(N.log10(hd_ro),N.log10(hd_omth_NSL),'--',c='tomato',linewidth=1)
#ax.scatter(N.log10(hd_ro),N.log10(hd_omth_NSL),marker='.',c='tomato',s=80,alpha=0.5)
#P.plot(N.log10(hd_ro),N.log10(hd_omth_TAC),'--',c='darkcyan',linewidth=1)
#ax.scatter(N.log10(hd_ro),N.log10(hd_omth_TAC),marker='.',c='darkcyan', s=80,alpha=0.5)
#ax.text(-0.65,-5.13, "(d)")

# AXIS

xini = -0.55
xend =  0.35
yini =  -2300.0
yend =  2300
P.ylim(yini,yend)
ax.yaxis.set_ticks(N.linspace(yini,yend,5))
ax.yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
P.xlim(xini,xend)
ax.xaxis.set_ticks(N.linspace(xini,xend,5))
ax.xaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
P.xlabel(r'${\rm \log Ro}$')
P.legend(loc=3,ncol=2,scatterpoints=1)
P.title(r'{\bf (d)}',loc='left',fontsize=20)
P.tight_layout()
P.savefig("quen_omega.eps",format='eps',dpi=400,bbox_inches='tight')
P.show()

