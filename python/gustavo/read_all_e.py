import numpy as N
root_dir = '/home/DATA/Solar/'
root_dir = '/storage/'

print('1. reading MHD results')
period,urms,ledd,tauc,ro,ro1,\
Domega,Domega_omega,         \
bx_NSL_e,bx_TAC_e,               \
bx_CZ_e,bp_NSL_e,bp_TAC_e,bp_CZ_e,   \
kx_NSL_e,kx_TAC_e,               \
kx_CZ_e,kp_NSL_e,kp_TAC_e,kp_CZ_e    = N.loadtxt(root_dir+'TAC/sim_results-mhd_e.txt', unpack=True)

print('1. reading MHD errors')
Uperiod,Uurms,Uledd,Utauc,Uro,Uro1,\
UDomega,UDomega_Uomega,         \
Ubx_NSL_e,Ubx_TAC_e,           \
Ubx_CZ_e, Ubp_NSL_e,Ubp_TAC_e,Ubp_CZ_e  = N.loadtxt(root_dir+'TAC/std_results-mhd_e.txt', unpack=True)

print('2. reading MHD mean-flows')
period,                      \
omr_NSL_e,omr_TAC_e,             \
omr_CZ_e,omth_NSL_e,omth_TAC_e,    \
omth_CZ_e,mc_NSL_e,              \
mc_TAC_e,mc_CZ_e                 = N.loadtxt(root_dir+'TAC/sim_mflows-mhd_e.txt', unpack=True)

print('3. reading MHD coeffs')
period,alpha_NSL_e,            \
alpha_TAC_e,alpha_CZ_e,          \
alpha_k_NSL_e,                 \
alpha_k_TAC_e,alpha_k_CZ_e,      \
alpha_m_NSL_e,alpha_m_TAC_e,     \
alpha_m_CZ_e,eta_NSL,          \
eta_TAC,eta_CZ               = N.loadtxt(root_dir+'TAC/sim_coeff-mhd_e.txt', unpack=True)

