import numpy as N
import matplotlib.pylab as P
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc                  

# PLOT STYLE #                                                                                                         
matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 24}                         
rc('font', **font)                            
rc('xtick', labelsize = 24)                   
rc('ytick', labelsize = 24)                   
                                              
legend = {'fontsize': 20}                     
rc('legend',**legend)                         
axes = {'labelsize': 24}                      
rc('axes', **axes)                            
rc('mathtext',fontset='cm')                   
#use this, but at the expense of slowdown of rendering
rc('text', usetex = True)                     
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]
################################################################################

c = 2.338e-5
sRhk = c*(1./ro1)**0.99
btot = N.sqrt(bx_NSL**2 +bp_NSL**2)
line15 = -1.49*N.ones(20)
line10 = -1.1*N.ones(20)
xRo1 = N.linspace(-0.3,0.15,20) 
xRo2 = N.linspace(0.25,0.8,20) 

#P.title("Tachocline magnetic fields --  RC models")

fig,ax=P.subplots()
#ax.scatter(N.log10(ro1[:-3]),N.log10(bx_NSL[:-3]),marker=(5,0),c='r',label=r'$\log (B_{\phi} [T]) |_{\rm nsl} $',s=100)
ax.scatter(N.log10(ro1[:-3]),N.log10(bp_NSL[:-3]),marker='o',c='b',label=r'$\log (B_{\rm p} [T])|_{\rm nsl} $',s=100)
#ax.scatter(N.log10(ro1[:]),N.log10(btot[:]),marker=(5,0),c='k',label=r'$\log (B_{\rm p} [T])|_{\rm nsl} $',s=100)
#P.scatter(N.log10(ro[0:-1]),N.log10(btot[0:-1]),c='k',label=r'$B_T$',s=50)
#ax.plot(xRo1,line15,'--',c='k')
#ax.plot(xRo1,line10,'--',c='k')
##P.plot(xRo1,1.2*xRo2-1.1,'--',c='k')
#ax.plot(xRo2,-2.*xRo2-.75,'--',c='k')
#ax.text(0.4,-1.45,r"${\rm Ro}^{-2}$")
#P.text(0.4,-0.42,r"${\rm Ro}^{-0.7}$")
#P.text(-0.4,-0.62,r"${\rm Ro}^{1.2}$")
ax.yaxis.set_ticks(N.linspace(-1.7,-1.2,5))
ax.yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
P.xlim(-0.4,0.4)
ax.xaxis.set_ticks(N.linspace(-0.4,0.4,5))
ax.xaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
P.ylim(-1.7,-1.2)
P.xlabel(r'${\rm \log Ro}$')
P.legend(loc=3,scatterpoints=1)
P.tight_layout()
P.show()

