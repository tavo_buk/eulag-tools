import numpy as N
import matplotlib.pylab as P
import eulag as E
from scipy.ndimage.filters import gaussian_filter
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc                  

# PLOT STYLE #                                                                                                         
matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 22}                         
rc('font', **font)                            
rc('xtick', labelsize = 20)                   
rc('ytick', labelsize = 20)                   

#######################################################################
# quantities to be plotted

# 1. Magnetic field at the poles
mu0 = 4.*N.pi*1e-7
rho_mean = 130.1
ek = rho_mean*.5*urms**2
mu0 = 4.*N.pi*1e-7

ebx_NSL_e = 0.5* bx_NSL_e**2/mu0 
ebp_NSL_e = 0.5* bp_NSL_e**2/mu0 
eb_NSL_e  = 0.5*(bx_NSL_e**2+bp_NSL_e**2)/mu0 
ebx_TAC_e = 0.5* bx_TAC_e**2/mu0 
ebp_TAC_e = 0.5* bp_TAC_e**2/mu0 
eb_TAC_e  = 0.5*(bx_TAC_e**2+bp_TAC_e**2)/mu0 
ebx_CZ_e  = 0.5*  bx_CZ_e**2/mu0 
ebp_CZ_e  = 0.5*  bp_CZ_e**2/mu0 
eb_CZ_e   = 0.5*( bx_CZ_e**2+bp_CZ_e**2)/mu0 
eb_TOT_e  = (eb_TAC_e + eb_CZ_e + eb_NSL_e)/3.

ebx_NSL_p = 0.5* bx_NSL_p**2/mu0 
ebp_NSL_p = 0.5* bp_NSL_p**2/mu0 
eb_NSL_p  = 0.5*(bx_NSL_p**2+bp_NSL_p**2)/mu0 
ebx_TAC_p = 0.5* bx_TAC_p**2/mu0 
ebp_TAC_p = 0.5* bp_TAC_p**2/mu0 
eb_TAC_p  = 0.5*(bx_TAC_p**2+bp_TAC_p**2)/mu0 
ebx_CZ_p  = 0.5*  bx_CZ_p**2/mu0 
ebp_CZ_p  = 0.5*  bp_CZ_p**2/mu0 
eb_CZ_p   = 0.5*( bx_CZ_p**2+bp_CZ_p**2)/mu0 
eb_TOT_p  = (eb_TAC_p + eb_CZ_p + eb_NSL_p)/3.

eUbx_TAC_e = 0.5* Ubx_TAC_e**2/mu0

eb_TAC = 0.5*(eb_TAC_e + eb_TAC_p) 
eb_CZ  = 0.5*(eb_CZ_e + eb_CZ_p  )
eb_NSL = 0.5*(eb_NSL_e + eb_NSL_p)

# 2. r'$C_{\alpha}$'
ytitle2 = r'$C_{\alpha}$'
L = 6.96e8
ca_NSL_e = (alpha_k_NSL_e + alpha_m_NSL_e)*L/eta_CZ 
ca_CZ_e  = ( alpha_k_CZ_e +  alpha_m_CZ_e)*L/eta_CZ
ca_TAC_e = (alpha_k_TAC_e + alpha_m_TAC_e)*L/eta_CZ

ca_NSL_p = (alpha_k_NSL_p + alpha_m_NSL_p)*L/eta_CZ 
ca_CZ_p  = ( alpha_k_CZ_p +  alpha_m_CZ_p)*L/eta_CZ
ca_TAC_p = (alpha_k_TAC_p + alpha_m_TAC_p)*L/eta_CZ

# 3. r'$C_{\Omega}^r$ and $C_{\Omega}^{\theta}$a
ytitle3 = r'$C_{\Omega} \times 10^{-3}$' 
comr_NSL_e = L*L*L*omr_NSL_e/eta_CZ 
comr_CZ_e  = L*L*L* omr_CZ_e/eta_CZ
comr_TAC_e = L*L*L*omr_TAC_e/eta_CZ

comr_NSL_p = L*L*L*omr_NSL_p/eta_CZ 
comr_CZ_p  = L*L*L* omr_CZ_p/eta_CZ
comr_TAC_p = L*L*L*omr_TAC_p/eta_CZ

comt_NSL_e = L*L*L*omth_NSL_e/eta_CZ 
comt_CZ_e  = L*L*L* omth_CZ_e/eta_CZ
comt_TAC_e = L*L*L*omth_TAC_e/eta_CZ

comt_NSL_p = L*L*L*omth_NSL_p/eta_CZ 
comt_CZ_p  = L*L*L* omth_CZ_p/eta_CZ
comt_TAC_p = L*L*L*omth_TAC_p/eta_CZ

# 4. r'$C_u$'
ytitle4= r'$C_u$' 
cu_NSL_e = L*mc_NSL_e/eta_CZ 
cu_CZ_e  = L* mc_CZ_e/eta_CZ
cu_TAC_e = L*mc_TAC_e/eta_CZ

cu_NSL_p = L*mc_NSL_p/eta_CZ 
cu_CZ_p  = L* mc_CZ_p/eta_CZ
cu_TAC_p = L*mc_TAC_p/eta_CZ

#5. Fits to dynamo numbers $D_r^{\prime}, \; D_{\theta}^{\prime}$
ytitle5 = r'$D_r^{\prime}, \; D_{\theta}^{\prime} \times 10^{-3}$'
xini = -0.51
xend =  0.38

#######################################################################
fig, ax = P.subplots(5, 3, figsize=(15,17))

#######################################################################
# FIRST COLUMN
#######################################################################

yini = -2
yend = 0.2
ax[0,0].set_title('TACHOCLINE',fontsize=20)
ax[0,0].set_xlim(xini,xend)
ax[0,0].set_ylim(yini,yend)
ax[0,0].plot(N.log10(ro1/(2.*N.pi)),N.log10(eb_TAC/ek), c='k',linestyle='solid',\
        label='Total',linewidth=1)
ax[0,0].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebx_TAC_p/ek),'o', c='r')
ax[0,0].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebx_TAC_p/ek), c='r',label='Toroidal')
ax[0,0].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebp_TAC_p/ek),'o', c='b')
ax[0,0].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebp_TAC_p/ek), c='b',label='Poloidal')
#ax[0,0].plot(N.log10(ro1/(2.*N.pi)),N.log10(eb_TAC_p/ek),'o', c='k')

ax[0,0].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebx_TAC_e/ek),'o', c='r')
ax[0,0].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebx_TAC_e/ek), '--',c='r')
ax[0,0].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebp_TAC_e/ek),'o', c='b')
ax[0,0].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebp_TAC_e/ek), '--',c='b')

# PLOT ERROR 
#ep = N.log10((ebx_TAC_e + 50.*eUbx_TAC_e)/ek)
#em = N.log10((ebx_TAC_e - 50.*eUbx_TAC_e)/ek)
#ax[0,0].fill_between(N.log10(ro1/(2.*N.pi)),em,ep,alpha=0.2,\
#                     edgecolor='#1B2ACC', facecolor='#089FFF',
#                     linewidth=1, antialiased=True   )

ax[0,0].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[0,0].tick_params(axis='x',which='both',labelbottom=False)
ax[0,0].set_ylabel(r'$\log(e_{B_{\phi,p}}/e_{\rm k})$')
ax[0,0].yaxis.set_ticks(N.linspace(yini,yend,5))
ax[0,0].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))

#######################################################################

yini = -10
yend = 7.7
line0 = N.zeros(20)
xy0 = N.linspace(xini,xend,20) 
xy1 = N.linspace(N.log10(ro1[0]/(2.*N.pi)),N.log10(ro1[-1]/(2.*N.pi)),20) 
#ax[2].set_title(r'${\bf (c)}$',loc='left',fontsize=20)
ax[1,0].set_xlim(xini,xend)
ax[1,0].set_ylim(yini,yend)
ax[1,0].plot( N.log10(ro1/(2.*N.pi)),ca_TAC_e,'-', c='darkgreen',linewidth=3,label='EQU')
ax[1,0].plot( N.log10(ro1/(2.*N.pi)),ca_TAC_e,'o', c='darkgreen')
ax[1,0].plot( N.log10(ro1/(2.*N.pi)),ca_TAC_p,'--',c='darkgreen',linewidth=3,label='POL')
ax[1,0].plot( N.log10(ro1/(2.*N.pi)),ca_TAC_p,'o', c='darkgreen')
ax[1,0].plot(xy0,line0,linestyle='dotted',c='k')
ax[1,0].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[1,0].tick_params(axis='x',which='both',labelbottom=False)
ax[1,0].set_ylabel(ytitle2)
ax[1,0].yaxis.set_ticks(N.linspace(yini,yend,5))
ax[1,0].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))

# Fit
xRo4 = N.linspace(-0.5,0.33,20) 
f = N.polyfit(N.log10(ro1/(2.*N.pi)),ca_TAC_p,1)
zf_ca_p = N.poly1d(f)
f = N.polyfit(N.log10(ro1/(2.*N.pi)),ca_TAC_e,3)
zf_ca_e = N.poly1d(f)
ax[1,0].plot(xRo4,zf_ca_e(xRo4),c='k',linewidth=0.8)
ax[1,0].plot(xRo4,zf_ca_p(xRo4),'--',c='k',linewidth=0.8)


#######################################################################

yini = -2.7
yend =  1.7
#ax[3].set_title(r'${\bf (d)}$',loc='left',fontsize=20)
ax[2,0].set_xlim(xini,xend)
ax[2,0].set_ylim(yini,yend)
ax[2,0].plot(N.log10(ro1/(2.*N.pi)),1e-3*comr_TAC_e,'o', c='darkcyan')
ax[2,0].plot(N.log10(ro1/(2.*N.pi)),1e-3*comr_TAC_e,     c='darkcyan',label='pol')
ax[2,0].plot(N.log10(ro1/(2.*N.pi)),1e-3*comt_TAC_e,'o', c='darkorange')
ax[2,0].plot(N.log10(ro1/(2.*N.pi)),1e-3*comt_TAC_e,     c='darkorange',label='pol')

ax[2,0].plot(N.log10(ro1/(2.*N.pi)),1e-3*comr_TAC_p,'o',  c='darkcyan')
ax[2,0].plot(N.log10(ro1/(2.*N.pi)),1e-3*comr_TAC_p,'--', c='darkcyan',label='pol')
ax[2,0].plot(N.log10(ro1/(2.*N.pi)),1e-3*comt_TAC_p,'o',  c='darkorange')
ax[2,0].plot(N.log10(ro1/(2.*N.pi)),1e-3*comt_TAC_p,'--', c='darkorange',label='pol')

ax[2,0].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[2,0].tick_params(axis='x',which='both',labelbottom=False)
ax[2,0].set_ylabel(ytitle3)
ax[2,0].yaxis.set_ticks(N.linspace(yini,yend,5))
ax[2,0].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[2,0].plot(xy0,line0,linestyle='dotted',c='k')

# Fit
xRo4 = N.linspace(-0.5,0.33,20) 
f = N.polyfit(N.log10(ro1/(2.*N.pi)),comr_TAC_p,2)
zf_comr_p = N.poly1d(f)
f = N.polyfit(N.log10(ro1/(2.*N.pi)),comr_TAC_e,2)
zf_comr_e = N.poly1d(f)

f = N.polyfit(N.log10(ro1/(2.*N.pi)),comt_TAC_p,1)
zf_comt_p = N.poly1d(f)
f = N.polyfit(N.log10(ro1/(2.*N.pi)),comt_TAC_e,1)
zf_comt_e = N.poly1d(f)

ax[2,0].plot(xRo4,1e-3*zf_comr_e(xRo4),c='k',linewidth=0.8)
ax[2,0].plot(xRo4,1e-3*zf_comr_p(xRo4),'--',c='k',linewidth=0.8)
ax[2,0].plot(xRo4,1e-3*zf_comt_e(xRo4),c='k',linewidth=0.8)
ax[2,0].plot(xRo4,1e-3*zf_comt_p(xRo4),'--',c='k',linewidth=0.8)
#######################################################################

yini = -0.1
yend = 9.5
#ax[4].set_title(r'${\bf (e)}$',loc='left',fontsize=20)
ax[3,0].set_xlim(xini,xend)
ax[3,0].set_ylim(yini,yend)
ax[3,0].plot(N.log10(ro1/(2.*N.pi)),cu_TAC_e,'o', c='tomato')
ax[3,0].plot(N.log10(ro1/(2.*N.pi)),cu_TAC_e,     c='tomato',label='Equator')
ax[3,0].plot(N.log10(ro1/(2.*N.pi)),cu_TAC_p,'o', c='tomato')
ax[3,0].plot(N.log10(ro1/(2.*N.pi)),cu_TAC_p,'--',c='tomato',label='Pole')
ax[3,0].set_ylabel(ytitle4)
ax[3,0].yaxis.set_ticks(N.linspace(yini,yend,5))
ax[3,0].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[3,0].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[3,0].tick_params(axis='x',which='both',labelbottom=False)

#######################################################################

#FITS
ax[4,0].axhspan(-1e-3*192,1e-3*192,alpha=0.6, color='lightgray')
ax[4,0].axvspan(N.log10(ro1[3]/(2.*N.pi)),N.log10(ro1[9]/(2.*N.pi)),alpha=0.5, color='lightgray')

dr_p=zf_ca_p*zf_comr_p 
dr_e=zf_ca_e*zf_comr_e 
dt_p=zf_ca_p*zf_comt_p 
dt_e=zf_ca_e*zf_comt_e 

dyr_num_period = 1e-3*dr_p(N.log10(ro1/(2.*N.pi)))
dyp_num_period = 1e-3*dt_p(N.log10(ro1/(2.*N.pi)))

yini =-3
yend = 5
line0 = N.zeros(20)
xy0 = N.linspace(xini,xend,20) 
ax[4,0].set_xlim(xini,xend)
ax[4,0].set_ylim(yini,yend)
ax[4,0].plot((xy1),1e-3*dr_e(xy1),'-',c='k', \
        label=r'$D_r^{\prime}=C_{\alpha}^{\prime} C_{\Omega}^{\prime r}$')
ax[4,0].plot((xy1),1e-3*dr_p(xy1),'--',c='k')
ax[4,0].plot((xy1),1e-3*dt_e(xy1),'-',c='r', \
        label=r'$D_{\theta}^{\prime}=C_{\alpha}^{\prime} C_{\Omega}^{\prime \theta}$')
ax[4,0].plot((xy1),1e-3*dt_p(xy1),'--',c='r')
#ax[4,0].plot(N.log10(ro1/(2.*N.pi)),1e-3*dr_e(N.log10(ro1/(2.*N.pi))),'-',c='k', \
#        label=r'$D_r^{\prime}=C_{\alpha}^{\prime} C_{\Omega}^{\prime r}$')
#ax[4,0].plot(N.log10(ro1/(2.*N.pi)),1e-3*dr_p(N.log10(ro1/(2.*N.pi))),'--',c='k')
#ax[4,0].plot(N.log10(ro1/(2.*N.pi)),1e-3*dt_e(N.log10(ro1/(2.*N.pi))),'-',c='r', \
#        label=r'$D_{\theta}^{\prime}=C_{\alpha}^{\prime} C_{\Omega}^{\prime \theta}$')
#ax[4,0].plot(N.log10(ro1/(2.*N.pi)),1e-3*dt_p(N.log10(ro1/(2.*N.pi))),'--',c='r')

print 1e-3*dr_p(N.log10(ro1/(2.*N.pi)))
print 1e-3*dt_p(N.log10(ro1/(2.*N.pi)))

ax[4,0].plot(xy0,line0,linestyle='dotted',c='k')
ax[4,0].set_ylabel(ytitle5)
ax[4,0].yaxis.set_ticks(N.linspace(yini,yend,5))
ax[4,0].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[4,0].set_xlabel(r'${\rm \log Ro}$')
ax[4,0].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[4,0].xaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[4,0].legend(loc=1,ncol=1,fontsize=13,frameon=False)

#######################################################################
# SECOND COLUMN
#######################################################################

yini = -4.2
yend = -0.8
ax[0,1].set_title('CONVECTION ZONE',fontsize=20)
ax[0,1].set_xlim(xini,xend)
ax[0,1].set_ylim(yini,yend)
ax[0,1].plot(N.log10(ro1/(2.*N.pi)),N.log10( eb_CZ/ek), c='k',linestyle='solid', \
        linewidth=1,label='Total')
ax[0,1].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebx_CZ_p/ek),'o', c='r')
ax[0,1].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebx_CZ_p/ek), c='r',label='Toroidal')
ax[0,1].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebp_CZ_p/ek),'o', c='b')
ax[0,1].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebp_CZ_p/ek), c='b',label='Poloidal')
#ax[0,1].plot(N.log10(ro1/(2.*N.pi)),N.log10( eb_CZ_p/ek),'o', c='k')

ax[0,1].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebx_CZ_e/ek),'o', c='r')
ax[0,1].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebx_CZ_e/ek), '--',c='r')
ax[0,1].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebp_CZ_e/ek),'o', c='b')
ax[0,1].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebp_CZ_e/ek), '--',c='b')

ax[0,1].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[0,1].tick_params(axis='x',which='both',labelbottom=False)
ax[0,1].legend(loc=3,ncol=1,fontsize=12,frameon=False)
ax[0,1].yaxis.set_ticks(N.linspace(yini,yend,5))
ax[0,1].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))

#######################################################################

yini = -10
yend = 7.7
line0 = N.zeros(20)
xy0 = N.linspace(xini,xend,20) 
#ax[2].set_title(r'${\bf (c)}$',loc='left',fontsize=20)
ax[1,1].set_xlim(xini,xend)
ax[1,1].set_ylim(yini,yend)
ax[1,1].plot( N.log10(ro1/(2.*N.pi)),ca_CZ_e,'-', c='darkgreen',linewidth=2,label='EQU')
ax[1,1].plot( N.log10(ro1/(2.*N.pi)),ca_CZ_e,'o', c='darkgreen')
ax[1,1].plot( N.log10(ro1/(2.*N.pi)),ca_CZ_p,'--',c='darkgreen',linewidth=2,label='POL')
ax[1,1].plot( N.log10(ro1/(2.*N.pi)),ca_CZ_p,'o', c='darkgreen')
ax[1,1].plot(xy0,line0,linestyle='dotted',c='k')
ax[1,1].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[1,1].tick_params(axis='x',which='both',labelbottom=False)
ax[1,1].yaxis.set_ticks(N.linspace(yini,yend,5))
ax[1,1].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[1,1].legend(loc=3,ncol=1,fontsize=14,frameon=False)

# Fit
xRo4 = N.linspace(-0.5,0.33,20) 
f = N.polyfit(N.log10(ro1/(2.*N.pi)),ca_CZ_p,1)
zf_ca_p = N.poly1d(f)
f = N.polyfit(N.log10(ro1/(2.*N.pi)),ca_CZ_e,2)
zf_ca_e = N.poly1d(f)
ax[1,1].plot(xRo4,zf_ca_e(xRo4),c='k',linewidth=0.8)
ax[1,1].plot(xRo4,zf_ca_p(xRo4),'--',c='k',linewidth=0.8)
#######################################################################

yini = -2.7
yend =  1.7
#ax[3].set_title(r'${\bf (d)}$',loc='left',fontsize=20)
ax[2,1].set_xlim(xini,xend)
ax[2,1].set_ylim(yini,yend)
ax[2,1].plot(N.log10(ro1/(2.*N.pi)),1e-3*comr_CZ_e,'o', c='darkcyan')
ax[2,1].plot(N.log10(ro1/(2.*N.pi)),1e-3*comr_CZ_e,     c='darkcyan',label=r'$C_{\Omega}^r$')
ax[2,1].plot(N.log10(ro1/(2.*N.pi)),1e-3*comt_CZ_e,'o', c='darkorange')
ax[2,1].plot(N.log10(ro1/(2.*N.pi)),1e-3*comt_CZ_e,     c='darkorange',label=r'$C_{\Omega}^{\theta}$')

ax[2,1].plot(N.log10(ro1/(2.*N.pi)),1e-3*comr_CZ_p,'o',  c='darkcyan')
ax[2,1].plot(N.log10(ro1/(2.*N.pi)),1e-3*comr_CZ_p,'--', c='darkcyan')
ax[2,1].plot(N.log10(ro1/(2.*N.pi)),1e-3*comt_CZ_p,'o',  c='darkorange')
ax[2,1].plot(N.log10(ro1/(2.*N.pi)),1e-3*comt_CZ_p,'--', c='darkorange')

ax[2,1].legend(loc=3,ncol=1,fontsize=14,frameon=False)
ax[2,1].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[2,1].tick_params(axis='x',which='both',labelbottom=False)
ax[2,1].yaxis.set_ticks(N.linspace(yini,yend,5))
ax[2,1].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[2,1].plot(xy0,line0,linestyle='dotted',c='k')

# Fit
xRo4 = N.linspace(-0.5,0.33,20) 
f = N.polyfit(N.log10(ro1/(2.*N.pi)),comr_CZ_p,2)
zf_comr_p = N.poly1d(f)
f = N.polyfit(N.log10(ro1/(2.*N.pi)),comr_CZ_e,3)
zf_comr_e = N.poly1d(f)

f = N.polyfit(N.log10(ro1/(2.*N.pi)),comt_CZ_p,2)
zf_comt_p = N.poly1d(f)
f = N.polyfit(N.log10(ro1/(2.*N.pi)),comt_CZ_e,2)
zf_comt_e = N.poly1d(f)

ax[2,1].plot(xRo4,1e-3*zf_comr_e(xRo4),c='k',linewidth=0.8)
ax[2,1].plot(xRo4,1e-3*zf_comr_p(xRo4),'--',c='k',linewidth=0.8)
ax[2,1].plot(xRo4,1e-3*zf_comt_e(xRo4),c='k',linewidth=0.8)
ax[2,1].plot(xRo4,1e-3*zf_comt_p(xRo4),'--',c='k',linewidth=0.8)

#######################################################################

yini = -0.1
yend = 9.5
#ax[4].set_title(r'${\bf (e)}$',loc='left',fontsize=20)
ax[3,1].set_xlim(xini,xend)
ax[3,1].set_ylim(yini,yend)
ax[3,1].plot(N.log10(ro1/(2.*N.pi)),cu_CZ_e,'o', c='tomato')
ax[3,1].plot(N.log10(ro1/(2.*N.pi)),cu_CZ_e,     c='tomato',label='Equator')
ax[3,1].plot(N.log10(ro1/(2.*N.pi)),cu_CZ_p,'o', c='tomato')
ax[3,1].plot(N.log10(ro1/(2.*N.pi)),cu_CZ_p,'--',c='tomato',label='Pole')
ax[3,1].yaxis.set_ticks(N.linspace(yini,yend,5))
ax[3,1].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[3,1].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[3,1].tick_params(axis='x',which='both',labelbottom=False)

#######################################################################
#FITS

dr_p=zf_ca_p*zf_comr_p 
dr_e=zf_ca_e*zf_comr_e 
dt_p=zf_ca_p*zf_comt_p 
dt_e=zf_ca_e*zf_comt_e 

yini =-3
yend = 5
line0 = N.zeros(20)
xy0 = N.linspace(xini,xend,20) 
ax[4,1].set_xlim(xini,xend)
ax[4,1].set_ylim(yini,yend)
ax[4,1].plot((xy1),1e-3*dr_e(xy1),'-',c='k')
ax[4,1].plot((xy1),1e-3*dr_p(xy1),'--',c='k')
ax[4,1].plot((xy1),1e-3*dt_e(xy1),'-',c='r')
ax[4,1].plot((xy1),1e-3*dt_p(xy1),'--',c='r')
#ax[4,1].plot(N.log10(ro1/(2.*N.pi)),1e-3*dr_e(N.log10(ro1/(2.*N.pi))),'-',c='k')
#ax[4,1].plot(N.log10(ro1/(2.*N.pi)),1e-3*dr_p(N.log10(ro1/(2.*N.pi))),'--',c='k')
#ax[4,1].plot(N.log10(ro1/(2.*N.pi)),1e-3*dt_e(N.log10(ro1/(2.*N.pi))),'-',c='r')
#ax[4,1].plot(N.log10(ro1/(2.*N.pi)),1e-3*dt_p(N.log10(ro1/(2.*N.pi))),'--',c='r')
ax[4,1].plot(xy0,line0,linestyle='dotted',c='k')
ax[4,1].yaxis.set_ticks(N.linspace(yini,yend,5))
ax[4,1].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[4,1].set_xlabel(r'${\rm \log Ro}$')
ax[4,1].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[4,1].xaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
#FITS

#######################################################################
# THIRD COLUMN
#######################################################################

yini = -4.
yend = -0.5
ax[0,2].set_title('NEAR SURFACE LAYER',fontsize=20)
ax[0,2].set_xlim(xini,xend)
ax[0,2].set_ylim(yini,yend)
ax[0,2].plot(N.log10(ro1/(2.*N.pi)),N.log10( eb_NSL/ek), c='k',linestyle='solid',\
        linewidth=1,label='Total')
ax[0,2].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebx_NSL_p/ek),'o', c='r')
ax[0,2].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebx_NSL_p/ek), c='r',label='Toroidal')
ax[0,2].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebp_NSL_p/ek),'o', c='b')
ax[0,2].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebp_NSL_p/ek), c='b',label='Poloidal')
#ax[0,2].plot(N.log10(ro1/(2.*N.pi)),N.log10( eb_NSL_p/ek),'o', c='k')

ax[0,2].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebx_NSL_e/ek),'o', c='r')
ax[0,2].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebx_NSL_e/ek), '--',c='r')
ax[0,2].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebp_NSL_e/ek),'o', c='b')
ax[0,2].plot(N.log10(ro1/(2.*N.pi)),N.log10(ebp_NSL_e/ek), '--',c='b')

ax[0,2].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[0,2].tick_params(axis='x',which='both',labelbottom=False)
ax[0,2].yaxis.set_ticks(N.linspace(yini,yend,5))
ax[0,2].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))

xRo3 = N.linspace(0.09,0.33,20) 
ax[0,2].plot(xRo3,-2.6*xRo3 - 1.45,'--',c='k')
ax[0,2].plot(xRo3,-5.7*xRo3 - 2.1,'--',c='k')
ax[0,2].text(0.08,-3.6,r"${\rm Ro}^{-5.7}$", fontsize=13)
ax[0,2].text(0.24,-2.85,r"${\rm Ro}^{-2.6}$",  fontsize=13)

#######################################################################

yini = -10
yend = 7.7
line0 = N.zeros(20)
xy0 = N.linspace(xini,xend,20) 
#ax[2].set_title(r'${\bf (c)}$',loc='left',fontsize=20)
ax[1,2].set_xlim(xini,xend)
ax[1,2].set_ylim(yini,yend)
ax[1,2].plot( N.log10(ro1/(2.*N.pi)),ca_NSL_e,'-', c='darkgreen',linewidth=2,label='Equator')
ax[1,2].plot( N.log10(ro1/(2.*N.pi)),ca_NSL_e,'o', c='darkgreen')
ax[1,2].plot( N.log10(ro1/(2.*N.pi)),ca_NSL_p,'--',c='darkgreen',linewidth=2,label='Pole')
ax[1,2].plot( N.log10(ro1/(2.*N.pi)),ca_NSL_p,'o', c='darkgreen')
ax[1,2].plot(xy0,line0,linestyle='dotted',c='k')
ax[1,2].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[1,2].tick_params(axis='x',which='both',labelbottom=False)
ax[1,2].yaxis.set_ticks(N.linspace(yini,yend,5))
ax[1,2].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))

# Fit
xRo4 = N.linspace(-0.5,0.33,20) 
f = N.polyfit(N.log10(ro1/(2.*N.pi)),ca_NSL_p,2)
zf_ca_p = N.poly1d(f)
f = N.polyfit(N.log10(ro1/(2.*N.pi)),ca_NSL_e,2)
zf_ca_e = N.poly1d(f)
ax[1,2].plot(xRo4,zf_ca_e(xRo4),c='k',linewidth=0.8)
ax[1,2].plot(xRo4,zf_ca_p(xRo4),'--',c='k',linewidth=0.8)
#######################################################################

yini = -2.7
yend =  1.7
#ax[3].set_title(r'${\bf (d)}$',loc='left',fontsize=20)
ax[2,2].set_xlim(xini,xend)
ax[2,2].set_ylim(yini,yend)
ax[2,2].plot(N.log10(ro1/(2.*N.pi)),1e-3*comr_NSL_e,'o', c='darkcyan')
ax[2,2].plot(N.log10(ro1/(2.*N.pi)),1e-3*comr_NSL_e,     c='darkcyan',label='pol')
ax[2,2].plot(N.log10(ro1/(2.*N.pi)),1e-3*comt_NSL_e,'o', c='darkorange')
ax[2,2].plot(N.log10(ro1/(2.*N.pi)),1e-3*comt_NSL_e,     c='darkorange',label='pol')

ax[2,2].plot(N.log10(ro1/(2.*N.pi)),1e-3*comr_NSL_p,'o',  c='darkcyan')
ax[2,2].plot(N.log10(ro1/(2.*N.pi)),1e-3*comr_NSL_p,'--', c='darkcyan',label='pol')
ax[2,2].plot(N.log10(ro1/(2.*N.pi)),1e-3*comt_NSL_p,'o',  c='darkorange')
ax[2,2].plot(N.log10(ro1/(2.*N.pi)),1e-3*comt_NSL_p,'--', c='darkorange',label='pol')

ax[2,2].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[2,2].tick_params(axis='x',which='both',labelbottom=False)
ax[2,2].yaxis.set_ticks(N.linspace(yini,yend,5))
ax[2,2].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[2,2].plot(xy0,line0,linestyle='dotted',c='k')

# Fit
xRo4 = N.linspace(-0.5,0.33,20) 
f = N.polyfit(N.log10(ro1/(2.*N.pi)),comr_NSL_p,3)
zf_comr_p = N.poly1d(f)
f = N.polyfit(N.log10(ro1/(2.*N.pi)),comr_NSL_e,3)
zf_comr_e = N.poly1d(f)

f = N.polyfit(N.log10(ro1/(2.*N.pi)),comt_NSL_p,2)
zf_comt_p = N.poly1d(f)
f = N.polyfit(N.log10(ro1/(2.*N.pi)),comt_NSL_e,2)
zf_comt_e = N.poly1d(f)

ax[2,2].plot(xRo4,1e-3*zf_comr_e(xRo4),c='k',linewidth=0.8)
ax[2,2].plot(xRo4,1e-3*zf_comr_p(xRo4),'--',c='k',linewidth=0.8)
ax[2,2].plot(xRo4,1e-3*zf_comt_e(xRo4),c='k',linewidth=0.8)
ax[2,2].plot(xRo4,1e-3*zf_comt_p(xRo4),'--',c='k',linewidth=0.8)
#######################################################################

yini = -0.1
yend = 9.5
#ax[4].set_title(r'${\bf (e)}$',loc='left',fontsize=20)
ax[3,2].set_xlim(xini,xend)
ax[3,2].set_ylim(yini,yend)
ax[3,2].plot(N.log10(ro1/(2.*N.pi)),cu_NSL_e,'o', c='tomato')
ax[3,2].plot(N.log10(ro1/(2.*N.pi)),cu_NSL_e,     c='tomato',label='Equator')
ax[3,2].plot(N.log10(ro1/(2.*N.pi)),cu_NSL_p,'o', c='tomato')
ax[3,2].plot(N.log10(ro1/(2.*N.pi)),cu_NSL_p,'--',c='tomato',label='Pole')
ax[3,2].yaxis.set_ticks(N.linspace(yini,yend,5))
ax[3,2].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[3,2].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[3,2].tick_params(axis='x',which='both',labelbottom=False)

#######################################################################

#FITS
ax[4,2].axvspan(N.log10(ro1[4]/(2.*N.pi)),N.log10(ro1[-1]/(2.*N.pi)),alpha=0.5, color='lightgray')
dr_p=zf_ca_p*zf_comr_p 
dr_e=zf_ca_e*zf_comr_e 
dt_p=zf_ca_p*zf_comt_p 
dt_e=zf_ca_e*zf_comt_e 

dyr_num_period_nsl = 1e-3*dr_p(N.log10(ro1/(2.*N.pi)))
dyp_num_period_nsl = 1e-3*dt_p(N.log10(ro1/(2.*N.pi)))

yini =-3
yend = 5
line0 = N.zeros(20)
xy0 = N.linspace(xini,xend,20) 
ax[4,2].set_xlim(xini,xend)
ax[4,2].set_ylim(yini,yend)
ax[4,2].plot((xy1),1e-3*dr_e(xy1),'-',c='k')
ax[4,2].plot((xy1),1e-3*dr_p(xy1),'--',c='k')
ax[4,2].plot((xy1),1e-3*dt_e(xy1),'-',c='r')
ax[4,2].plot((xy1),1e-3*dt_p(xy1),'--',c='r')
#ax[4,2].plot(N.log10(ro1/(2.*N.pi)),1e-3*dr_e(N.log10(ro1/(2.*N.pi))),'-',c='k')
#ax[4,2].plot(N.log10(ro1/(2.*N.pi)),1e-3*dr_p(N.log10(ro1/(2.*N.pi))),'--',c='k')
#ax[4,2].plot(N.log10(ro1/(2.*N.pi)),1e-3*dt_e(N.log10(ro1/(2.*N.pi))),'-',c='r')
#ax[4,2].plot(N.log10(ro1/(2.*N.pi)),1e-3*dt_p(N.log10(ro1/(2.*N.pi))),'--',c='r')
ax[4,2].plot(xy0,line0,linestyle='dotted',c='k')
ax[4,2].yaxis.set_ticks(N.linspace(yini,yend,5))
ax[4,2].yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
ax[4,2].set_xlabel(r'${\rm \log Ro}$')
ax[4,2].xaxis.set_ticks(N.linspace(xini,xend,5))
ax[4,2].xaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))



P.savefig("dyn_all.eps",format='eps',dpi=400,bbox_inches='tight')
P.show()

