import numpy as N
import matplotlib.pylab as P
import matplotlib
import matplotlib.ticker as T
from matplotlib import rc 
from matplotlib.lines import Line2D                  

# PLOT STYLE #                                                                                                         
matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 22}                         
rc('font', **font)                            
rc('xtick', labelsize = 24)                   
rc('ytick', labelsize = 24)                   
                                              
legend = {'fontsize': 18}                     
rc('legend',**legend)                         
axes = {'labelsize': 21}                      
rc('axes', **axes)                            
rc('mathtext',fontset='cm')                   
#use this, but at the expense of slowdown of rendering
rc('text', usetex = True)                     
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]
################################################################################

line15 = -1.4*N.ones(20)
line10 = -0.95*N.ones(20)
xRo1 = N.linspace(-0.65,0.3,20) 
xRo2 = N.linspace(-0.65,0.1,20) 
xRo3 = N.linspace(0.15,0.5,20) 

L = 6.96e8

ca_NSL = (alpha_k_NSL+alpha_m_NSL)*L/eta_CZ 
ca_CZ  = (alpha_k_CZ +alpha_m_CZ )*L/eta_CZ
ca_TAC = (alpha_k_TAC+alpha_m_TAC)*L/eta_CZ
cam_NSL = (alpha_m_NSL)*L/eta_CZ 
cam_CZ  = (alpha_m_CZ )*L/eta_CZ
cam_TAC = (alpha_m_TAC)*L/eta_CZ

#P.title("Tachocline magnetic fields --  RC models")

ftig,ax=P.subplots()
P.plot(    N.log10(ro1/(2.*N.pi)),ca_NSL,'-',c='r',linewidth=2)
ax.scatter(N.log10(ro1/(2.*N.pi)),ca_NSL,marker='o',c='r', label=r'$\langle C_{\alpha} \rangle^{NSL} $',s=80)
P.plot(    N.log10(ro1/(2.*N.pi)),ca_CZ ,'-',c='orange',linewidth=2)
ax.scatter(N.log10(ro1/(2.*N.pi)),ca_CZ ,marker='o',c='orange', label=r'$\langle C_{\alpha} \rangle^{CZ} $',s=80)
P.plot(    N.log10(ro1/(2.*N.pi)),ca_TAC,'-',c='b',linewidth=2)
ax.scatter(N.log10(ro1/(2.*N.pi)),ca_TAC,marker='o',c='b', label=r'$\langle C_{\alpha} \rangle^{TAC} $',s=80)

#P.plot(    N.log10(ro1/(2.*N.pi)),cam_NSL,linestyle='dashed',c='r',linewidth=1)
#P.plot(    N.log10(ro1/(2.*N.pi)),cam_CZ ,linestyle='dashed',c='orange',linewidth=1)
#P.plot(    N.log10(ro1/(2.*N.pi)),cam_TAC,linestyle='dashed',c='b',linewidth=1)

rot = N.log10(ro1[:]/(2.*N.pi))
cat = 11.2*rot - 0.63
P.plot(    rot,cat,'-',c='b',linestyle='dashed', linewidth=1)


#P.plot(N.log10(hd_ro),    (hd_alpha_k_NSL),'--',c='r',linewidth=1)
#ax.scatter(N.log10(hd_ro),(hd_alpha_k_NSL),marker='.',c='r',s=80,alpha=0.5)
#P.plot(N.log10(hd_ro),    (hd_alpha_k_CZ),'--',c='orange',linewidth=1)
#ax.scatter(N.log10(hd_ro),(hd_alpha_k_CZ),marker='.',c='orange', s=80,alpha=0.5)
#P.plot(N.log10(hd_ro),    (hd_alpha_k_TAC),'--',c='b',linewidth=1) 
#ax.scatter(N.log10(hd_ro),(hd_alpha_k_TAC),marker='.',c='b', s=80,alpha=0.5)
##ax.text(-0.65,1.84, "(c)")

# AXIS
xini = -0.55
xend =  0.35
yini = -9.	
yend =  6.

line0 = N.zeros(20)
xy0 = N.linspace(xini,xend,20) 
ax.plot(xy0,line0,linestyle='dotted',c='k')

P.ylim(yini,yend)
ax.yaxis.set_ticks(N.linspace(yini,yend,5))
ax.yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
P.xlim(xini,xend)
ax.xaxis.set_ticks(N.linspace(xini,xend,5))
ax.xaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
P.xlabel(r'${\rm \log Ro}$')
P.legend(loc=4,scatterpoints=1,ncol=1)
P.title(r'{\bf (c)}',loc='left',fontsize=20)
P.tight_layout()
P.savefig("quen_alpha.eps",format='eps',dpi=400,bbox_inches='tight')
P.show()

