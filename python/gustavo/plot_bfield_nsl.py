import numpy as N
import matplotlib.pylab as P
import matplotlib
import matplotlib.ticker as t
from matplotlib import rc                  

# PLOT STYLE #                                                                                                         
matplotlib.rcParams['font.serif'] = 'cmr10'   
matplotlib.rcParams['font.sans-serif'] = 'cmr10'
                                              
font = { 'size' : 22}                         
rc('font', **font)                            
rc('xtick', labelsize = 24)                   
rc('ytick', labelsize = 24)                   
                                              
legend = {'fontsize': 18}                     
rc('legend',**legend)                         
axes = {'labelsize': 21}                      
rc('axes', **axes)                            
rc('mathtext',fontset='cm')                   
#use this, but at the expense of slowdown of rendering
rc('text', usetex = True)                     
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]
################################################################################

c = 2.338e-5
sRhk = c*(1./ro1)**0.99

mu0 = 4.*N.pi*1e-7
rho_mean = 130.1
btot = N.sqrt(bx_NSL**2 +bp_NSL**2)
ek = rho_mean*.5*urms**2
ebx_NSL = 0.5*bx_NSL**2/mu0 
ebp_NSL = 0.5*bp_NSL**2/mu0 
eb_NSL  = 0.5*(bx_NSL**2+bp_NSL**2)/mu0 

ebx_TAC = 0.5* bx_TAC**2/mu0 
ebp_TAC = 0.5* bp_TAC**2/mu0 
eb_TAC  = 0.5*(bx_TAC**2+bp_TAC**2)/mu0 

ebx_CZ = 0.5* bx_CZ**2/mu0 
ebp_CZ = 0.5* bp_CZ**2/mu0 
eb_CZ  = 0.5*(bx_CZ**2+bp_CZ**2)/mu0 

line15 = -2.*N.ones(20)
line10 = -1.7*N.ones(20)
xRo1 = N.linspace(-0.5,0.29,20) 
xRo2 = N.linspace(-0.5,0.05,20) 
xRo3 = N.linspace(0.09,0.3,20) 

#P.title("Tachocline magnetic fields --  RC models")

fig,ax=P.subplots()
#bta_nsl,btb_nsl,bpa_nsl,bpb_nsl = N.load('/home/DATA/Solar/TAC/rec_field_nsl.npy')
ax.scatter(N.log10(ro1/(2.*N.pi)),N.log10(ebx_NSL/ek),marker=(5,0),c='b',label=r'$\log (\langle e_{B_{\phi}}  \rangle^{\rm NSL}/e_{\rm k})$',s=100)
ax.scatter(N.log10(ro1/(2.*N.pi)),N.log10(ebp_NSL/ek),marker='o',c='r',  label=r'$\log (\langle e_{B_{\rm p}} \rangle^{\rm NSL}/e_{\rm k})$',s=100)
P.plot(    N.log10(ro1/(2.*N.pi)),N.log10(eb_NSL/ek),'--',c='r')
#P.plot(    N.log10(ro1),N.log10(2.5*bta_nsl),':',c='r')
#P.plot(    N.log10(ro1),N.log10(3.5*bpa_nsl),':',c='r')

ax.plot(xRo2,line15,'--',c='k')
ax.plot(xRo1,line10,'--',c='k')
##P.plot(xRo1,1.2*xRo2-1.1,'--',c='k')
ax.plot(xRo3,-3.4*xRo3 - 1.7,'--',c='k')
ax.text(0.2,-2.2,r"${\rm Ro}^{-3.4}$")
#P.text(0.4,-0.42,r"${\rm Ro}^{-0.7}$")
#P.text(-0.4,-0.62,r"${\rm Ro}^{1.2}$")

# AXIS
xini = -0.55
xend =  0.35

yini = -6.2
yend = -0.6
P.ylim(yini,yend)
ax.yaxis.set_ticks(N.linspace(yini,yend,5))
ax.yaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
P.xlim(xini,xend)
ax.xaxis.set_ticks(N.linspace(xini,xend,5))
ax.xaxis.set_major_formatter(T.FormatStrFormatter('$%.1f$'))
P.xlabel(r'${\rm \log Ro}$')
P.legend(loc=3,scatterpoints=1)
P.title(r'{\bf (b)}',loc='left',fontsize=20)
P.tight_layout()
P.savefig("BnslRo.eps",format='eps',dpi=400,bbox_inches='tight')
P.show()

