'''
script to compute the kinetic and magnetic alpha effect,
it results in a 2D array (r and th) with u'.(curl u') and b'.(curl b')
'''
from eulag import *
from sys import exit
import matplotlib.pylab as P
import matplotlib
import numpy as N
from pylab import meshgrid
from scipy import trapz
from scipy.integrate import simps


g = read_grid()
s = read_strat()

nt = f.u.shape[0]
year = 365.*24.*3600.       # Year in seconds
time = np.multiply(g.t_f11,range(nt))/year

''' computing mean and turbulent fields '''
# Only azimuthal mean
um  = N.mean(f.u, axis = 1)
vm  = N.mean(f.v, axis = 1)
wm  = N.mean(f.w, axis = 1)
bxm = N.mean(f.bx, axis = 1)
bym = N.mean(f.by, axis = 1)
bzm = N.mean(f.bz, axis = 1)

# Turbulent fields
ut  = turb(f.u, um, g.n)
vt  = turb(f.v, vm, g.n)
wt  = turb(f.w, wm, g.n)
bxt = turb(f.bx, bxm, g.n)
byt = turb(f.by, bym, g.n)
bzt = turb(f.bz, bzm, g.n)

brms = N.mean(N.sqrt(bxt**2 + byt**2 + bzt**2), axis = (1,2,3))
P.plot(time, brms)
P.show()

mean_around_time = input("Around what time you whant to perform the mean? ")
inter, = N.where(time > mean_around_time)
nstep_mean = 4
nn = [inter[0] - int(nstep_mean/2) + i for i in range(nstep_mean)]

# Only temporal mean
u = N.mean(f.u[nn,...], axis = 0)
v = N.mean(f.v[nn,...], axis = 0)
w = N.mean(f.w[nn,...], axis = 0)
bx = N.mean(f.bx[nn,...], axis = 0)
by = N.mean(f.by[nn,...], axis = 0)
bz = N.mean(f.bz[nn,...], axis = 0)


# Temporal and  azimuthal mean
utt = N.mean(u, axis = 0)
vtt = N.mean(v, axis = 0)
wtt = N.mean(w, axis = 0)
bxtt = N.mean(bx, axis = 0)
bytt = N.mean(by, axis = 0)
bztt = N.mean(bz, axis = 0)

Prot = input('What is the period of rotation? ')
omega_0 = 2.*pi/(Prot*24.*3600.)

cut = 5. # Degrees to be cut off
good = int(cut*g.m/180.)
theta = g.th
theta_good = N.linspace(0+(cut/90)*pi/2., (1 - cut/180)*pi, num = g.m - 2*good)

R, TH = meshgrid(g.r, theta_good)

X = R*N.sin(TH)
Y = R*N.cos(TH)

bgradu = N.load('bgradu.npy')

rhs = (time[nn[-1]] - time[nn[0]])*(365.*24.*3600.)*N.mean(bgradu[nn,...], axis = 0) # temporal mean

rhs_new = N.zeros(bgradu.shape[1:3])
for i in range(bgradu.shape[1]):
	for j in range(bgradu.shape[2]):
		rhs_new[i,j] = simps(bgradu[nn,i,j], time[nn]*(365.*24.*3600.))

r1, = N.where((g.rf > 0.7) & (g.rf < .74))
th1, = N.where((g.th > N.pi/4.) & (g.th < N.pi/2.))
tsbgradu = N.mean(bgradu[:,th1,:][...,r1], axis = (1,2))
tsbxm = N.mean(bxm[:,th1,:][...,r1], axis = (1,2))

P.plot(time, tsbxm/tsbxm.max(), c = 'k')
P.plot(time, tsbgradu/tsbgradu.max(), c = 'r')
P.ylim((-1.,1.))
P.legend(( r'$B_\phi$', r'$B.\nabla v$'))
P.title('Fields phase')
P.show()

#######  PLOT  #######
fig, ax = P.subplots(dpi = 100, facecolor = None, edgecolor = None, linewidth = None, tight_layout = False) # Another option is tight_layout = True
fig.subplots_adjust(left = 0.4, right = .95, bottom = 0.05, top = .95)
fig.set_size_inches(6, 10)
matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
Omin = -70#rhs[1:-1,:].min()
Omax =  70#rhs[1:-1,:].max()
cs = ax.contourf(X, Y, rhs[1:-1,:], levels = N.linspace(-abs(Omax), (Omax), 50), interpolation = 'bicubic', cmap = P.get_cmap("RdYlBu"))
ax.contour(X, Y, rhs[1:-1,:], 6, interpolation = 'bicubic', colors = "k")

# Add colorbar
cax = fig.add_axes([0.20, 0.20, 0.04, 0.60])
cbar = fig.colorbar(cs, cax = cax, ticks = N.linspace(-abs(Omax), (Omax), 5), ticklocation = 'left', extend = 'neither', spacing = 'uniform')#, format = '%2.3d')

# Make sure aspect ratio preserved
ax.set_aspect('equal')

# Turn off rectangular frame.
ax.set_frame_on(False)

# Turn off axis ticks.
ax.set_xticks([])
ax.set_yticks([])

P.show()

#######  PLOT  #######
fig, ax = P.subplots(dpi = 100, facecolor = None, edgecolor = None, linewidth = None, tight_layout = False) # Another option is tight_layout = True
fig.subplots_adjust(left = 0.4, right = .95, bottom = 0.05, top = .95)
fig.set_size_inches(6, 10)
matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
Omin = -70#rhs_new[1:-1,:].min()
Omax =  70#rhs_new[1:-1,:].max()
cs = ax.contourf(X, Y, rhs_new[1:-1,:], levels = N.linspace(-abs(Omax), (Omax), 50), interpolation = 'bicubic', cmap = P.get_cmap("RdYlBu"))
ax.contour(X, Y, rhs_new[1:-1,:], 6, interpolation = 'bicubic', colors = "k")

# Add colorbar
cax = fig.add_axes([0.20, 0.20, 0.04, 0.60])
cbar = fig.colorbar(cs, cax = cax, ticks = N.linspace(-abs(Omax), (Omax), 5), ticklocation = 'left', extend = 'neither', spacing = 'uniform')#, format = '%2.3d')

# Make sure aspect ratio preserved
ax.set_aspect('equal')

# Turn off rectangular frame.
ax.set_frame_on(False)

# Turn off axis ticks.
ax.set_xticks([])
ax.set_yticks([])

P.show()



#######  PLOT  #######
fig, ax = P.subplots(dpi = 100, facecolor = None, edgecolor = None, linewidth = None, tight_layout = False) # Another option is tight_layout = True
fig.subplots_adjust(left = 0.4, right = .95, bottom = 0.05, top = .95)
fig.set_size_inches(6, 10)
matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
bmin = -.70#bxtt[1:-1,:].min()
bmax =  .70#bxtt[1:-1,:].max()
cs = ax.contourf(X, Y, bxtt[1:-1,:], levels = N.linspace(-bmax, bmax, 50, endpoint = True), interpolation = 'bicubic', cmap = P.get_cmap("RdYlBu"))
ax.contour(X, Y, bxtt[1:-1,:], 6, interpolation = 'bicubic', colors = "k")

# Add colorbar
cax = fig.add_axes([0.20, 0.20, 0.04, 0.60])
cbar = fig.colorbar(cs, ticks = N.linspace(-bmax, bmax, 5, endpoint = True), cax = cax, ticklocation = 'left', extend = 'neither', spacing = 'uniform')#, format = '%2.3d')

# Make sure aspect ratio preserved
ax.set_aspect('equal')

# Turn off rectangular frame.
ax.set_frame_on(False)

# Turn off axis ticks.
ax.set_xticks([])
ax.set_yticks([])

P.show()
