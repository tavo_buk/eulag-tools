import eulag as E
import numpy as N
import matplotlib.pylab as P

f  = E.read_f11(ifl = False)
g  = E.read_grid()

bxt = E.turb(f.bx)
byt = E.turb(f.by)
bzt = E.turb(f.bz)

ut = E.turb(f.u)
vt = E.turb(f.v)
wt = E.turb(f.w)

tme = N.zeros((g.m,g.l))
tke = N.zeros((g.m,g.l))
mme = N.zeros((g.m,g.l))
mke = N.zeros((g.m,g.l))
k   = N.arange(64)

for i in range (g.l):
    print  i 
    # turbulent spectra
    tme[:,i] = E.mspec2(bxt,byt,bzt,20,i)
    tke[:,i] = E.mspec2(ut,vt,wt,20,i)
    # total spectra
    mme[:,i] = E.mspec2(f.bx,f.by,f.bz,20,i)
    mke[:,i] = E.mspec2(f.u,f.v,f.w,20,i)

N.save('turb_spec',(tme,tke))
N.save('mean_spec',(mme,mke))

P.figure(0)
P.contourf(g.rf,(k[1:]),(tme[1:,:]),64)
P.figure(1)
P.contourf(g.rf,(k[1:]),(tke[1:,:]),64)

P.show()

