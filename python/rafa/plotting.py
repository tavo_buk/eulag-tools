from eulag.files.grid import read_grid
from eulag.files.rbox import read_f11
from eulag.files.ts import ts
from eulag.files.modules import turb
import matplotlib.pyplot as plt
import numpy as np
import sys

def urms(star, grid):
    ut  = turb(star.u)
    vt  = turb(star.v)
    wt  = turb(star.w)

    urms = np.mean(np.sqrt(ut ** 2 + vt ** 2 + wt ** 2), axis=(0, 1, 2))

    r = np.linspace(0.6, 0.96, grid.l)

    return r, urms

def bm_by_time(time_series):
    plt.title('field x time')
    plt.plot(time_series.t, time_series.bm, label='mean')
    plt.plot(time_series.t, time_series.bt, label='turbulent')
    plt.yscale('log')
    plt.legend()

    plt.savefig('meanfield.png')
    plt.show()

def um_by_time(time_series):
    plt.title('speed x time')
    plt.plot(time_series.t, time_series.um, label='mean')
    plt.plot(time_series.t, time_series.ut, label='turbulent')
    plt.yscale('log')
    plt.legend()

    plt.savefig('meanomega.png')
    plt.show()

def butterfly_theta(star, r):
    t = star.time / (365. * 24. * 3600.)

    bxm = np.mean(star.bx, axis=1)

    theta = np.linspace(-90, 90, num=bxm.shape[1])
    radius = np.linspace(0.6, 0.96, num=bxm.shape[2])

    plt.xlabel('time (years)')
    plt.ylabel('theta (degree)')

    for ind, item in enumerate(radius):
        if item >= r:
            ir = ind
            break

    plt.contourf(t, theta, bxm[:,:,ir].T, 64, extend='both')

    plt.savefig('butter_th.png')

def butterfly_radius(star, th):
    t = star.time / (365. * 24. * 3600.)

    bxm = np.mean(star.bx, axis=1)

    radius = np.linspace(0.6, 0.96, num=bxm.shape[2])
    theta = np.linspace(-90, 90, num=bxm.shape[1])

    plt.xlabel('time (years)')
    plt.ylabel('radius')

    for ind, item in enumerate(theta):
        if item >= th:
            it = ind
            break

    plt.contourf(t, radius, bxm[:,it,:].T, 70)

    plt.savefig('butter_r.png')
