import eulag as E
import numpy as N
import matplotlib.pylab as P
from scipy.fftpack import fft
import os

def comp_period(f, radius, ist):
    g = E.read_grid(radius)
    ir1, = N.where((g.rf > 0.9) & (g.rf < 0.96))
    ir2, = N.where((g.rf > 0.62) & (g.rf < 0.76))
    year = 365.*24.*3600.

    # field to plot
    y2 = f.by**2 + f.bz**2
    #y2 = f.bx**2
    y = (N.mean(y2[-ist:,2:31,ir1],axis=(1,2)))
    time_years = f.time[-ist:]/year


    sampling_interval = g.t_f11/year
    n_field = (y.shape)[0]
    freq_nyquist = 0.5/sampling_interval
    freqs = N.arange(n_field/2)/(n_field/2.-1)*freq_nyquist
    freqs2 = N.fft.fftfreq(int((n_field/2)/(n_field)*freq_nyquist))

    print('field size = ', n_field)
    print('freq size = ',  freqs2.size, freqs2[0],freqs2[-1])
    freq_nodc = freqs[1:int(n_field/2)]
    period = []
    pspec_nodc_array = []
    k = 0

    bperiod = y
    mspec = N.abs(N.fft.fft(bperiod))
    pspec = mspec**2
    pspec_nodc = 2.*pspec[1:int(n_field/2)]
    i_peak = N.argmax(pspec_nodc)
    freq = freq_nodc[i_peak]
    period = 1./freq

    print('***computing the period')
    print('gt_xav = ', sampling_interval)
    print('cycle period, k = ', period, i_peak)
    N.save('period_bottom',(freq_nodc,pspec_nodc))

    fig, (ax1, ax2) = P.subplots(2, 1, tight_layout=True)
    ax1.set_yscale(u'linear')
    ax1.set_xscale(u'linear')
    #ax1.set_ylim(0,0.04)
    ax1.set_xlim(time_years.min(),time_years.max())
    ax1.plot(time_years,y)
    #
    ax2.set_yscale(u'log', nonposx='clip')
    ax2.set_xscale(u'log', nonposx='clip')
    #ax2.set_ylim(1e-1,1e3)
    ax2.set_xlim(0.01,4)
    ax2.plot(freq_nodc,pspec_nodc)
    P.show()
    return period
