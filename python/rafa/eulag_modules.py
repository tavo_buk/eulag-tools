import matplotlib.pyplot as plt
import numpy as np
import eulag

def urms_r(star):
    # star fom eulag.rf11m
    ut = eulag.turb_xav(star.u, star.u2)
    vt = eulag.turb_xav(star.v, star.v2)
    wt = eulag.turb_xav(star.w, star.w2)

    return np.sqrt(np.mean(ut ** 2 + vt ** 2 + wt ** 2, axis=(0, 1)))


def ro_stand(grid, star, base, Prot):
    alpha = 2.
    press = star.p[-100,:,:].mean(axis=0)

    Hp = press / eulag.deriv(grid.r, press)
    plt.plot(grid.rf, Hp)
    plt.show()

    indb = abs(grid.rf - base).argmin()

    Hp2 = alpha * Hp[indb] / 2.
    alpHp = alpha * Hp[indb]
    locv = grid.r[indb] + Hp2

    indv = abs(grid.r - locv).argmin()

    v = urms_r(star)[indv]
    tauc = alpHp / v
    Prots = Prot * 24. * 60. * 60.

    Ro = Prots / tauc

    print('Mixing length at the base of convective zone: %e' % alpHp)
    print('Velocity at half of mixing length above base of convective zone: %f' % v)
    print('Convective turnover time: %e' % tauc)
    print('Ro = %f' % Ro)
