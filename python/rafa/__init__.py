from butterfly import butter_r, butter_phi
from hd_velocity import conv_velocity4, conv_velocity2
from diff_rot import omega, merid_w
from time_series import bmean_eq, time_2series, bmean_equip
from eulag_modules import ro_stand
from mag_cicle import comp_period
import profiles
