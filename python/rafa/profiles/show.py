from matplotlib.ticker import FormatStrFormatter
from state_class import Star, Isentropic, Ambient
import matplotlib.pyplot as plt
import numpy as np
import matplotlib


def temperature(name='temp.eps'):
    plt.style.use('~/eulag-tools/python/rafa/profiles/strat_profile.mplstyle')
    matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]

    star = Star()
    star_amb = Ambient()
    star_isen = Isentropic()

    x0 = 0.25
    ind0 = (np.abs(star.r - x0)).argmin()

    ax = plt.axes()

    ax.plot(star.r[ind0:], star.temp[ind0:], label=r'$T_{\rm MLT}$', linestyle='dotted', color='C2')
    ax.plot(star_isen.r, star_isen.temp, label=r'$T_{\rm s}$', color='C1')
    ax.plot(star_amb.r, star_amb.temp, label=r'$T_{\rm e}$', color='C0')

    ax.set_xlabel(r'$r\, [R_{\star}]$')
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))
    ax.set_xticks([0.6, 0.7, 0.8, 0.9, 1.0])
    ax.set_xlim(0.6, 1.0)

    yticks = ax.yaxis.get_major_ticks()
    yticks[0].label1.set_visible(False)

    ax.set_ylabel(r'$T\, [\rm{K}]$')
    ax.set_ylim(star.temp[-1], star_isen.temp[0])
    plt.yscale('log')

    ax.legend()
    plt.tight_layout()
    plt.savefig('./figures/' + name)


def theta(name='theta.eps'):
    plt.style.use('~/eulag-tools/python/rafa/profiles/strat_profile.mplstyle')
    matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]

    star_amb = Ambient()
    star_isen = Isentropic()

    ax = plt.axes()

    ax.plot(star_isen.r, star_isen.theta, label=r'$\Theta_{\rm s}$')
    ax.plot(star_amb.r, star_amb.theta, label=r'$\Theta_{\rm e}$')

    ax.set_xlabel(r'$r\, [R_{\star}]$')
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.set_xticks([0.6, 0.7, 0.8, 0.9])
    ax.set_xlim(0.60, 0.96)

    yticks = ax.yaxis.get_major_ticks()
    yticks[0].label1.set_visible(False)

    ax.set_ylabel((r'$\Theta\, [\rm{K}]$'))
    ax.set_ylim(star_amb.theta[0] - 1e4, star_isen.theta[0] + 1e4)

    ax_small = plt.axes([0.625, 0.5, 0.3, 0.3])
    ax_small.plot(star_amb.r, star_amb.theta, color='C1')

    ax_small.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax_small.set_xticks([0.76, 0.86, 0.96])
    ax_small.tick_params('x', labelsize=14.)
    ax_small.set_xlim(0.74, 0.98)

    ax_small.set_yticks([])
    ax_small.set_ylim(1.45e6, 1.453e6)

    ax.legend()
    plt.tight_layout()
    plt.savefig('./figures/' + name)


def density(name='dens.eps'):
    plt.style.use('~/eulag-tools/python/rafa/profiles/strat_profile.mplstyle')
    matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]

    star = Star()
    star_amb = Ambient()
    star_isen = Isentropic()

    x0 = 0.25
    ind0 = (np.abs(star.r - x0)).argmin()

    ax = plt.axes()

    ax.plot(star.r[ind0:], star.dens[ind0:], label=r'$\rho_{\rm{MLT}}$', linestyle='dotted', color='C2')
    ax.plot(star_isen.r, star_isen.dens, label=r'$\rho_{\rm s}$', color='C0')
    ax.plot(star_amb.r, star_amb.dens, label=r'$\rho_{\rm e}$', color='C1')

    ax.set_xlabel(r'$r\, [R_{\star}]$')
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))
    ax.set_xticks([0.6, 0.7, 0.8, 0.9, 1.0])
    ax.set_xlim(0.60, 1.0)

    yticks = ax.yaxis.get_major_ticks()
    yticks[0].label1.set_visible(False)

    ax.set_ylabel((r'$\rho\, [\rm{kg/m^{3}}]$'))
    ax.set_ylim(star.dens[-1], star_isen.dens[0])
    plt.yscale('log')

    ax.legend()
    plt.tight_layout()
    plt.savefig('./figures/' + name)


def pressure(name='press.eps'):
    plt.style.use('~/eulag-tools/python/rafa/profiles/strat_profile.mplstyle')
    matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]

    star = Star()
    star_amb = Ambient()
    star_isen = Isentropic()

    x0 = 0.25
    ind0 = (np.abs(star.r - x0)).argmin()

    ax = plt.axes()

    ax.plot(star.r[ind0:], star.press[ind0:], label=r'$p_{\rm{MLT}}$', linestyle='dotted', color='C2')
    ax.plot(star_isen.r, star_isen.press, label=r'$p_{s}$', color='C0')
    ax.plot(star_amb.r, star_amb.press, label=r'$p_{e}$', color='C1')

    ax.set_xlabel(r'$r\, [R_{\star}]$')
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.set_xticks([0.6, 0.7, 0.8, 0.9, 1.0])
    ax.set_xlim(0.60, 1.0)

    yticks = ax.yaxis.get_major_ticks()
    yticks[0].label1.set_visible(False)

    ax.set_ylabel((r'$p\, [\rm{Pa}]$'))
    ax.set_ylim(star.press[-1], star_isen.press[0])
    plt.yscale('log')

    ax.legend()
    plt.tight_layout()
    plt.savefig('./figures/' + name)
