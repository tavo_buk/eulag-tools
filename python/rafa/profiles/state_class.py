import numpy as np

class Star(object):
    def __init__(self):
        r, temp, dens = np.loadtxt('data.txt', unpack=True)

        self.radius = r[-1]
        self.r = r / self.radius
        self.temp = temp
        self.dens = dens
        self.press = 13732. * self.dens * self.temp

    def radius_informations(self, rad):
        ind0 = (np.abs(self.r - rad)).argmin()

        print('radius = {:6.4e}'.format(self.r[ind0] * self.radius))
        print('temperature = {:6.4e}'.format(self.temp[ind0]))
        print('density = {:6.4e}'.format(self.dens[ind0]))
        print('press = {:6.4e}'.format(self.press[ind0]))

    def tachocline_informations(self, rtach=None):
        if rtach == None:
            r, speed = np.loadtxt('speed.txt', unpack=True)
            ind0 = np.where(speed > 0.0)[0][0]

        else:
            ind0 = (np.abs(self.r - rtach)).argmin()

        print('tachocline radius = {:5.4f}'.format(self.r[ind0]))
        print('temperature = {:6.4e}'.format(self.temp[ind0]))
        print('density = {:6.4e}'.format(self.dens[ind0]))
        print('press = {:6.4e}'.format(self.press[ind0]))


class Isentropic(object):
    def __init__(self):
        r, temp, dens, press, theta = np.loadtxt('isentropic.txt', unpack=True)

        self.r = r
        self.temp = temp
        self.dens = dens
        self.theta = theta
        self.press = 13732. * self.dens * self.temp
        self.delta_theta = abs(self.theta[0] - self.theta[-1])


class Ambient(object):
    def __init__(self):
        r, temp, dens, press, theta = np.loadtxt('ambient.txt', unpack=True)

        self.r = r
        self.temp = temp
        self.dens = dens
        self.theta = theta
        self.press = 13732. * self.dens * self.temp
        self.delta_theta = np.max(self.theta) - self.theta[-1]
