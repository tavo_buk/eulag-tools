from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.ndimage.filters import gaussian_filter
from matplotlib.ticker import FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np
import matplotlib
import eulag


def butter_r(star, radius, nlev1, nlevf1, nlev2, nlevf2, rad=0.96, the=15., t0=100., tf=200., name=' '):
    plt.style.use('~/eulag-tools/python/rafa/contour.mplstyle')
    matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]

    # star from rf11m
    bxm = gaussian_filter(star.bx,0.8)
    bzm = gaussian_filter(star.bz,0.8)
    theta = np.linspace(-90, 90, num=bxm.shape[1], endpoint=True)

    grid = eulag.read_grid(radius)
    r = grid.rf
    year = 365. * 24. * 3600.
    time = star.time/year	# Time in years
    validt = np.linspace(t0, tf, 5, endpoint=True)

    fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(8, 6))
    itg = np.where((time >= t0) & (time <= tf))[0]

#############################################################################

    ir1 = (np.abs(r - rad)).argmin() # Fixed radius

    bmax = np.array([abs(bzm[itg,:,ir1].min()), abs(bzm[itg,:,ir1].max())]).max() * 1e2 - 0.3
    levf1 = np.linspace(-bmax, bmax, nlevf1)

    im1 = axes[0].contourf(time[itg], theta, np.rot90(bzm[itg,:,ir1] * 1.e2), levels=levf1, extend='both')
    axes[0].contour(time[itg], theta, np.rot90(bxm[itg,:,ir1]), nlev1, colors='k', linewidths=0.8)

    axes[0].set_ylabel(r'$90^{\circ} - \theta \, [^{\circ}] $')
    axes[0].set_yticklabels([r"$90^{\circ}$",r"$45^{\circ}$",r"$0^{\circ}$",r"$-45^{\circ}$",r"$-90^{\circ}$"])
    axes[0].set_yticks([90, 45, 0, -45, -90])

    axes[0].xaxis.set_major_formatter(FormatStrFormatter('$%3.0d$'))
    axes[0].set_xticks(validt)
    axes[0].set_xticklabels([])
    axes[0].set_rasterization_zorder(-10)

    divider1 = make_axes_locatable(axes[0])
    cax1 = divider1.append_axes('right', size='2%', pad=0.2)
    ticks1 = np.linspace(levf1[0], levf1[-1], 3)
    cbar_ax1 = fig.colorbar(im1, cax=cax1, ticks=ticks1, format='%2.1f', label=r"$ B_{\mathrm{r}} \mathrm{[T]} \times 10^{2}$")
    cbar_ax1.ax.set_rasterization_zorder(-10)

#############################################################################

    nthe = np.linspace(90., -90., num=bxm.shape[1])
    ith = (np.abs(nthe - the)).argmin() # Fixed theta

    bmax = np.array([abs(bzm[itg, ith, :].min()), abs(bzm[itg, ith, :].max())]).max() * 1e2 - 1.
    levf2 = np.linspace(-bmax, bmax, nlevf2)

    im2 = axes[1].contourf(time[itg], r, np.transpose(bzm[itg, ith, :] * 1.e2), levels=levf2, extend='both')
    axes[1].contour(time[itg], r, np.transpose(bxm[itg, ith, :]), nlev2, colors='k', linewidths=0.8)

    axes[1].xaxis.set_major_formatter(FormatStrFormatter('$%3.0d$'))
    axes[1].set_xticks(validt)
    axes[1].set_xlabel(r'$ t \, \mathrm{[yr]} $')

    axes[1].yaxis.set_major_formatter(FormatStrFormatter('$%.2f$'))
    axes[1].set_yticks(np.linspace(grid.rf[0], grid.rf[-1], 4, endpoint=True))
    axes[1].set_ylabel(r'$ r \, [R_{\star}] $')
    axes[1].set_rasterization_zorder(-10)

    divider2 = make_axes_locatable(axes[1])
    cax2 = divider2.append_axes('right', size='2%', pad=0.2)
    tick2 = np.linspace(levf2[0], levf2[-1], 3)
    cbar_ax2 = fig.colorbar(im2, ticks=tick2, cax=cax2, format='%2.1f', label=r"$ B_{\mathrm{r}} \mathrm{[T]} \times 10^{2}$")
    cbar_ax2.ax.set_rasterization_zorder(-10)

#############################################################################

    namefig = 'butter_r_r' + str(rad)[-2:] + '_th' + str(the)[-4:-2] if name == ' ' else name
    plt.savefig('./figures/' + namefig + '.eps')
    plt.show()


def butter_phi(star, radius, nlev1, nlevf1, nlev2, nlevf2, rad=0.96, the=15., t0=100., tf=200., name=' '):
    plt.style.use('~/eulag-tools/python/rafa/contour.mplstyle')
    matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]

    # star from rf11m
    bxm = gaussian_filter(star.bx,0.8)
    bzm = gaussian_filter(star.bz,0.8)
    theta = np.linspace(-90, 90, num=bxm.shape[1], endpoint=True)

    grid = eulag.read_grid(radius)
    r = grid.rf
    year = 365. * 24. * 3600.
    time = star.time/year	# Time in years
    validt = np.linspace(t0, tf, 5, endpoint=True)

    fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(9, 6))
    itg = np.where((time >= t0) & (time <= tf))[0]

#############################################################################

    ir1 = (np.abs(r - rad)).argmin() # Fixed radius

    bmax = np.array([abs(bxm[itg,:,ir1].min()), abs(bxm[itg,:,ir1].max())]).max() * 1e2 - 1.
    levf1 = np.linspace(-bmax, bmax, nlevf1)

    im1 = axes[0].contourf(time[itg], theta, np.rot90(bxm[itg,:,ir1] * 1.e2), levels=levf1, extend='both')
    axes[0].contour(time[itg], theta, np.rot90(bzm[itg,:,ir1]), nlev1, colors='k', linewidths=0.8)

    axes[0].set_ylabel(r'$90^{\circ} - \theta \, [^{\circ}] $')
    axes[0].set_yticklabels([r"$90^{\circ}$",r"$45^{\circ}$",r"$0^{\circ}$",r"$-45^{\circ}$",r"$-90^{\circ}$"])
    axes[0].set_yticks([90, 45, 0, -45, -90])

    axes[0].xaxis.set_major_formatter(FormatStrFormatter('$%3.0d$'))
    axes[0].set_xticks(validt)
    axes[0].set_xticklabels([])
    axes[0].set_rasterization_zorder(-10)

    divider1 = make_axes_locatable(axes[0])
    cax1 = divider1.append_axes('right', size='2%', pad=0.2)
    ticks1 = np.linspace(levf1[0], levf1[-1], 3)
    cbar_ax1 = fig.colorbar(im1, cax=cax1, ticks=ticks1, format='%2.1f', label=r"$ B_{\mathrm{\phi}} \mathrm{[T]} \times 10^{2}$")
    cbar_ax1.ax.set_rasterization_zorder(-10)

#############################################################################

    nthe = np.linspace(90., -90., num=bxm.shape[1])
    ith = (np.abs(nthe - the)).argmin() # Fixed theta

    bmax = np.array([abs(bxm[itg, ith, :].min()), abs(bxm[itg, ith, :].max())]).max() * 1e2 - 20.
    levf2 = np.linspace(-bmax, bmax, nlevf2)

    im2 = axes[1].contourf(time[itg], r, np.transpose(bxm[itg, ith, :] * 1.e2), levels=levf2, extend='both')
    axes[1].contour(time[itg], r, np.transpose(bzm[itg, ith, :]), nlev2, colors='k', linewidths=0.8)

    axes[1].xaxis.set_major_formatter(FormatStrFormatter('$%3.0d$'))
    axes[1].set_xticks(validt)
    axes[1].set_xlabel(r'$ t \, \mathrm{[yr]} $')

    axes[1].yaxis.set_major_formatter(FormatStrFormatter('$%.2f$'))
    axes[1].set_yticks(np.linspace(grid.rf[0], grid.rf[-1], 4, endpoint=True))
    axes[1].set_ylabel(r'$ r \, [R_{\star}] $')
    axes[1].set_rasterization_zorder(-10)

    divider2 = make_axes_locatable(axes[1])
    cax2 = divider2.append_axes('right', size='2%', pad=0.2)
    tick2 = np.linspace(levf2[0], levf2[-1], 3)
    cbar_ax2 = fig.colorbar(im2, ticks=tick2, cax=cax2, format='%2.1f', label=r"$ B_{\mathrm{\phi}} \mathrm{[T]} \times 10^{2}$")
    cbar_ax2.ax.set_rasterization_zorder(-10)

#############################################################################

    namefig = 'butter_phi_r' + str(rad)[-2:] + '_th' + str(the)[-4:-2] if name == ' ' else name
    plt.savefig('./figures/' + namefig + '.eps')
    plt.show()
