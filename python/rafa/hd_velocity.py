import matplotlib.pyplot as plt
import numpy as np
import matplotlib
import eulag

def urms_r(star):
    ut = eulag.turb_xav(star.u, star.u2)
    vt = eulag.turb_xav(star.v, star.v2)
    wt = eulag.turb_xav(star.w, star.w2)

    return np.sqrt(np.mean(ut ** 2 + vt ** 2 + wt ** 2, axis=(0, 1)))

def conv_velocity4(grid, star1, star2, star3):
    #star fom rf11m

    plt.style.use('~/eulag-tools/python/rafa/profiles/strat_profile.mplstyle')
    matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]

    urms1 = urms_r(star1)
    urms2 = urms_r(star2)
    urms3 = urms_r(star3)

    radius, speed = np.loadtxt('./speed.txt', unpack=True)
    r = grid.rf

    fig, ax = plt.subplots(1, 1, figsize=(5,5))

    ax.plot(radius, speed, label='TGEC', color='C0')
    ax.plot(r, urms1, label='P29', color='C1')
    ax.plot(r, urms2, label='P25', color='C2')
    ax.plot(r, urms3, label='P21', color='C3')

    ax.set_xlabel(r'$r\, [R_{\star}]$')
    ax.set_xlim(r[0], r[-1])

    yticks = ax.yaxis.get_major_ticks()
    yticks[0].label1.set_visible(False)

    ax.set_ylabel(r'$u_{\rm{RMS}}\, [\rm{m/s}]$')
    ax.set_ylim(0, urms1[-1])

    plt.legend()

    plt.tight_layout()
    plt.show()
    plt.savefig('./conv_velocity.eps')



def conv_velocity(grid, star1, star2=[], star3=[]):
    #star fom rf11m

    plt.style.use('~/eulag-tools/python/rafa/profiles/strat_profile.mplstyle')
    matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]

    urms1 = urms_r(star1)
    urms2 = urms_r(star2)
    urms3 = urms_r(star3)

    radius, speed = np.loadtxt('speed.txt', unpack=True)
    r = grid.rf

    ax = plt.axes()

    ax.plot(r, urms1, label='EULAG-HD')
    ax.plot(radius, speed, label='MLT')
    ax.plot(r, urms2, label='P28')
    ax.plot(r, urms3, label='P21')

    ax.set_xlabel(r'$r\, [R_{\star}]$')
    ax.set_xlim(r[0], r[-1])

    yticks = ax.yaxis.get_major_ticks()
    yticks[0].label1.set_visible(False)

    ax.set_ylabel(r'$u_{\rm{RMS}}\, [\rm{m/s}]$')
    ax.set_ylim(0, urms1[-1])

    plt.legend()

    plt.tight_layout()
    plt.show()
    plt.savefig('../../conv_velocity.eps')


def conv_velocity2(grid, star):
    plt.style.use('/home/rafaella/eulag-tools/python/rafa/profiles/strat_profile.mplstyle')
    matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]

    urms = urms_r(star)

    radius, speed = np.loadtxt('../speed.txt', unpack=True)

    r = np.linspace(0.6, 0.96, grid.l)

    fig, ax = plt.subplots(1, 1, figsize=(5, 5), tight_layout=True)

    ax.plot(r, urms, label='EULAG-HD',color='C3')
    ax.plot(radius, speed, label='MLT',color='C0')

    ax.set_xlabel(r'$r\, [R_{\star}]$')
    ax.set_ylabel(r'$u_{\rm{RMS}}\, [\rm{m/s}]$')

    for index, item in enumerate(r):
        if item >= 0.96:
            ind = index
            break

    yticks = ax.yaxis.get_major_ticks()
    yticks[0].label1.set_visible(False)

    ax.set_xlim(0.6, 0.98)
    ax.set_ylim(0, urms[ind])

    ax.legend()

    plt.tight_layout()

    plt.savefig('./figures/conv_velocity.eps')

def conv_velocity3(grid, star1, star2):
    plt.style.use('presentation')
    matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]

    urms1 = urms_r(star1)
    urms2 = urms_r(star2)

    radius, speed = np.loadtxt('speed.txt', unpack=True)

    r = np.linspace(0.6, 0.96, grid.l)

    plt.plot(radius, speed, label='MLT', color='C0')
    plt.plot(r, urms2, label='P28', color='C1')
    plt.plot(r, urms1, label='P16', color='C2')

    plt.xlabel(r'$r\, [R_{\star}]$')
    plt.ylabel(r'$u_{\rm{RMS}}\, [\rm{m/s}]$')

    for index, item in enumerate(radius):
        if item >= 0.98:
            ind = index
            break

    plt.yticks([100, 200, 300, 400])
    plt.xlim(0.6, 0.98)
    plt.ylim(0, speed[ind])

    plt.legend()

    plt.tight_layout()
    plt.show()
    plt.savefig('./conv_velocity.eps')
