from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import eulag


def merid_w(star, radius, nlev=10, nlevf=20, rad_core=0.8, name=' ', prot=0., nn=100):
    plt.style.use('~/eulag-tools/python/rafa/contour.mplstyle')
    matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]

    grid = eulag.read_grid(radius)

    cut = 5    # Excluding 5 degrees
    good = 2   # For grid.m = 64, its necessary exclude 2 steps in each theta side
    theta_good = np.linspace((0. + cut / 180.) * np.pi, (1. - cut / 180.) * np.pi, num=(grid.m - 2 * good))

    # reading from rf11m
 #   um = star.u[-nn::].mean(axis=0)
 #   vm = star.v[-nn::].mean(axis=0)
    wm = star.w[-nn::].mean(axis=0)

    R, TH = np.meshgrid(grid.r, theta_good)
    X = R * np.sin(TH)
    Y = R * np.cos(TH)

 #   Omega_0 = 0. if prot == 0. else (2. * np.pi) / (prot * 24. * 3600.)
 #   Omega = ((um[good:-good:] / X) + Omega_0)  * (1.e9 / (2. * np.pi))

    fig, ax = plt.subplots(nrows=1, ncols=1)

    wmin = wm[-30,:].min() - 6.
    wmax = wm[-30,:].max() + 6.

    levf = np.linspace(wmin, wmax, nlevf)
#    lev = np.linspace(Omin, Omax, nlev)

#    if prot == 0.:
#        if abs(Omin) > abs(Omax): Omax =  abs(Omin)
#        else: Omin = -Omax
#
#        levf = np.linspace(Omin, Omax, nlevf)
#        lev = np.linspace(Omin, 0.5 * Omax, nlev)

#    cs = ax.contourf(X, Y, wm[good:-good,:], levels=levf, extend='both',  cmap='YlGn')
    cs = ax.contourf(X, Y, wm[good:-good,:], levels=levf, extend='both',  cmap='YlGn')

#    ax.contour(X, Y, Omega, levels=lev, colors='k', linewidths=0.8)

    # Add colorbar
    divider = make_axes_locatable(ax)
    cax = fig.add_axes([0.43, 0.35, 0.02, 0.3])
    cbar = fig.colorbar(cs, cax=cax, ticks=[levf[0], levf[-1]], ticklocation='left', format='%1.0f')

    cbar.ax.set_ylabel(r'$\bar{U_r}$')

    # Make sure aspect ratio preserved
    ax.set_aspect('equal')
    # Turn off rectangular frame.
    ax.set_frame_on(False)
    # Turn off axis ticks.
    ax.set_xticks([])
    ax.set_yticks([])

    # Draw a circle around the edge of the plot.
    ax.plot(max(grid.r) * np.sin(theta_good), max(grid.r) * np.cos(theta_good), 'k')
    ax.plot(min(grid.r) * np.sin(theta_good), min(grid.r) * np.cos(theta_good), 'k')
    ax.plot(grid.r * np.sin(min(theta_good)), grid.r * np.cos(min(theta_good)), 'k')
    ax.plot(grid.r * np.sin(max(theta_good)), grid.r * np.cos(max(theta_good)), 'k')

    # Draw a circle in the rad-conv interface
    tac = (np.abs(grid.rf - rad_core)).argmin()
    if rad_core: ax.plot(grid.r[tac] * np.sin(theta_good), grid.r[tac] * np.cos(theta_good), '--k', linewidth=0.8)

    namefig = 'merid_w' if name == ' ' else name
    plt.savefig('./figures/' + namefig + '.eps')
    plt.show()
