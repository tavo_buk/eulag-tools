from matplotlib.ticker import FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np
import matplotlib
import eulag


def time_2series(ts, t0=0., tf=200.):
    matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]
    plt.style.use('~/eulag-tools/python/rafa/profiles/strat_profile.mplstyle')

    it = np.where((ts.t >= t0) & (ts.t <= tf))[0]

    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(9, 4.5))

    ax[0].plot(ts.t[it], ts.bm, label=r'Mean')
    ax[0].plot(ts.t[it], ts.bt, label=r'Turbulent')
    ax[0].legend()

    ax[0].xaxis.set_major_formatter(FormatStrFormatter('$%3.0d$'))
    ax[0].set_xlabel(r'$t\, [\rm{yr}]$')
    ax[0].set_ylabel(r'$B_{\rm{RMS}}\, [\, \textrm{T}\, ]$')
    ax[0].set_yscale('log')

    ax[1].plot(ts.t[it], ts.um)
    ax[1].plot(ts.t[it], ts.ut)

    ax[1].xaxis.set_major_formatter(FormatStrFormatter('$%3.0d$'))
    ax[1].set_xlabel(r'$t\,[\rm{yr}]$')
    ax[1].set_ylabel(r'$U_{\rm{RMS}}\, [\rm{m/s}]$')
    ax[1].set_yscale('log')

    plt.tight_layout()

    plt.savefig('./figures/time_series.eps')


def urms2_xav(star):
    ut = eulag.turb_xav(star.u, star.u2)
    vt = eulag.turb_xav(star.v, star.v2)
    wt = eulag.turb_xav(star.w, star.w2)

    return np.mean(ut ** 2 + vt ** 2 + wt ** 2, axis=(1, 2))


def bmean_xav(star):
    bxm2 = np.mean(star.bx ** 2, axis=(1, 2))
    bym2 = np.mean(star.by ** 2, axis=(1, 2))
    bzm2 = np.mean(star.bz ** 2, axis=(1, 2))

    return np.sqrt(bxm2 + bym2 + bzm2)


def bmean_equip(star, t0=100., tf=200., name=' '):
    plt.style.use('~/eulag-tools/python/rafa/profiles/strat_profile.mplstyle')
    matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]

    year = 365 * 24 * 3600
    mu0 = 4 * np.pi * 1e-7

    time = star.time / year
    indt = np.where((time >= t0) & (time <= tf))[0]
    validt = np.linspace(t0, tf, 5, endpoint=True)

    rho = np.mean(eulag.read_strat().rho_ad)

    um2 = np.mean(urms2_xav(star)[-50:])

    bm = bmean_xav(star)
    beq = np.sqrt(rho * mu0) * um2

    fig, ax = plt.subplots(1, 1, figsize=(5, 5), tight_layout=True)

    ax.plot(time[indt], bm[indt]/beq)

    ax.xaxis.set_major_formatter(FormatStrFormatter('$%3.0d$'))
    ax.set_xlabel(r'$t\, [\rm yr]$')
    ax.set_ylabel(r'$B / B_{\rm eq}$')
    ax.set_xticks(validt)
    ax.set_yscale('log')

    namefig = 'bmean_equip' if name == ' ' else name
    plt.savefig('./figures' + namefig + '.eps')


def bmean_eq(star1, star2, t0=100., tf=200., name=' '):
    plt.style.use('~/eulag-tools/python/rafa/profiles/strat_profile.mplstyle')
    matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]

    year = 365 * 24 * 3600
    mu0 = 4 * np.pi * 1e-7

    time1 = star1.time / year
    indt1 = np.where((time1 >= t0) & (time1 <= tf))[0]
    time2 = star2.time / year
    indt2 = np.where((time2 >= t0) & (time2 <= tf))[0]
    validt = np.linspace(t0, tf, 5, endpoint=True)

    urms21 = urms2_xav(star1)
    urms22 = urms2_xav(star2)
    um21 = np.mean(urms21[-50:])
    um22 = np.mean(urms22[-50:])

    rho = np.mean(eulag.read_strat().rho_ad)

    bm1 = bmean_xav(star1)
    bm2 = bmean_xav(star2)
    beq1 = np.sqrt(rho * mu0) * um21
    beq2 = np.sqrt(rho * mu0) * um22

    ax = plt.axes()

    ax.plot(time1[indt1], bm1[indt1]/beq1, label='P28')
    ax.plot(time2[indt2], bm2[indt2]/beq2, label='P21')

    ax.xaxis.set_major_formatter(FormatStrFormatter('$%3.0d$'))
    ax.set_xlabel(r'$t\, [\rm yr]$')
    ax.set_xticks(validt)
    ax.set_xlim(t0, tf)

    yticks = ax.yaxis.get_major_ticks()
    yticks[0].label1.set_visible(False)

    ax.set_ylabel(r'$B / B_{\rm eq}$')
    ax.set_ylim(0., 0.02)

    ax.legend()

    plt.show()
    namefig = 'bmean_eq' if name == ' ' else name
    plt.savefig('../' + 'namefig' + '.png')


def norm_urms_bmean_time(star1, star2):
    plt.style.use('presentation')
    matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amssymb,amsmath}"]

    mu0 = 4 * np.pi * 1e-7

    urms21 = urms2_xav(star1)
    urms22 = urms2_xav(star2)

    um21 = np.mean(urms21[-50:])
    um22 = np.mean(urms22[-50:])

    rho = np.mean(eulag.read_strat().rho_ad)

    bm1 = bmean_xav(star1)
    bm2 = bmean_xav(star2)

    beq1 = np.sqrt(rho * mu0) * um21
    beq2 = np.sqrt(rho * mu0) * um22

    year = 365 * 24 * 3600

    ax = plt.axes()

    ax.plot(star2.time[5:]/year, np.sqrt(urms22[5:]/um22), label=r'P28' + r'$\,\, \tilde{u}$', color='C3', linestyle = '--')
    ax.plot(star2.time[5:]/year, bm2[5:]/beq2, label='P28' + r'$\,\, \tilde{B}$', color='C3')
    ax.plot(star1.time[5:]/year, np.sqrt(urms21[5:]/um21), label='P21' + r'$\,\, \tilde{u}$', color='C0', linestyle = '--')
    ax.plot(star1.time[5:]/year, bm1[5:]/beq1, label='P21' + r'$\,\, \tilde{B}$', color='C0')

    ax.set_xlabel(r'$t\, [\rm yr]$')
    ax.set_ylabel(r'$u_{\rm rms} / \langle u_{\rm rms}\rangle,\, B / B_{\rm eq}$')

    plt.yscale('log')

    ax.legend()

    plt.tight_layout()
    plt.show()
    plt.savefig('norm_urms_bmean_time.eps')
