if initslice eq !NULL then begin
 print, 'ERROR ... '
 print, 'please set a value for the initial time slice "initslice" '
endif else begin
 
 print, 'creating arrays and computing MLT vals ...'
 
 ger1=make_array(z_num,t_num)
 ger2=make_array(z_num,t_num)
 ger3=make_array(z_num,t_num)
 for ii=initslice, t_num do begin
  for jj=0, z_num do begin
   ger1[jj,ii] = the_aver[jj,ii]/(the_aver[jj,ii]+theta_strat[jj])  
   ger2[jj,ii] = w2_aver[jj,ii]/((cs[jj])^2.)
   ger3[jj,ii] = (abs(fconv[jj,ii])/(rho_strat[jj]*(cs[jj])^3.))^(2./3.)
  endfor
 endfor

 print, 'initializing plots sequence ... '
 for ii=initslice,t_num-1 do begin
  
  if !d.name eq 'PS' then begin
    device,xsize=22,ysize=12,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
  end
  !x.margin=[12,3]
  !p.charsize=2

  !p.multi=[0,1,3] & !p.charsize=2.5
  
  plot, z, ger1[*,ii], xtitle='(z)', thick=3
  plot, z, ger2[*,ii], xtitle='(z)', LINESTYLE=5, THICK=3
  plot, z, ger3[*,ii], xtitle='(z)', LINESTYLE=5, THICK=3
  
  wait=1
  print, 'time slice', ii
 endfor
endelse

end  
