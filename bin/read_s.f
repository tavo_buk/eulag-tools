#define PARALLEL 0
#define PVM_IO 0
#define ANALIZE 0
#define NETCDFO 0
#define GKS 0
#define V5D 0
#define PLOTPL 0
#define COLORPL 0
#define TURBPL 0
#define SPCTPL 0
#define VORTPL 0
#define ENERGY 0
#define CRAYT3D 0
#define CRAYPVP 0
#define CRAYT3E 0
#define SGI_O2K 0
#define HP 0
#define LNX 0
#define GGG 1
#define WORKS 0
#define FUJI_VPP 0
#define IBM 0
#define CPQ 0
#define PRECISION 2
C############################################################################
C                                                                           #
C   !!!  Now define PRECOMPILER options to set model configuration   !!!   #
C                                                                           #
C############################################################################
C THE FOLLOWING PARAMETERS ARE MANDATORY FOR PROPER WORKING SETUP 
C============================================================================
#define SEMILAG 0   /* 0=EULERIAN, 1=SEMI-LAGRANGIAN                       */
#define MOISTMOD 0  /* 0=DRY, 1=WARM MOIST, 2=ICE A+B (NOT READY YET)      */
#define J3DIM 1     /* 0=2D MODEL, 1=3D MODEL                              */
#define SGS 1       /* DIFFUSION: 0=ILES, 1=OLD DISSIP, 2=DISSIP AMR       */
#define CHEMIS 0    /* PASSIVE TRACER: 0=OFF/1=ON                          */
#define IMRSB 0     /* IMMERSED BOUNDARY: 0=OFF/1=ON                       */
#define PRECF 0     /* 0=PRECON_DF or PRECON_BCZ, 1=PRECON_F               */
#define POLES 0     /* 0=NO FLOW OVER POLES, 1=FLOW OVER POLES ON          */
C============================================================================
C THE FOLLOWING PARAMETERS ARE CUSTOM MADE FOR SOME EXPERIMENTS
C============================================================================
#define BCREF 0     /* Use reference profiles for BC instead environmental */
#define H_SUAREZ 0  /* Held suarez forcing: use only with spherical coord  */
#define VARENV 0    /* Use variable in time environmental profiles         */
C============================================================================
C REDEFINING THE FOLLOWING PARAMETRS IS FULLY OPTIONAL
C============================================================================
#define ISEND 2     /* 1=SENDRECV, 2=IRECV+SEND, 3=ISEND+IRECV             */ 
#define IORFLAG 0   /* 0=tape in serial mode, 1=tape in parallel mode      */
#define IOWFLAG 0   /* 0=tape in serial mode, 1=tape in parallel mode      */
#define IORTAPE 3   /* 1=default, 2=single, 3=double precision             */
#define IOWTAPE 3   /* 1=default, 2=single, 3=double precision             */
#define NETCDFD 2   /* 2=single precision, 1=double precision              */
#define TIMEPLT 0   /* 1=time counts                                       */
#define DIAGPLT 0   /* 1=prints for debuging                               */
C============================================================================
C NOW CHOOSE THE PREDEFINED TEST CASE (*) OR CREATE YOUR OWN EXPERIMENT (**)
C (*) UNCOMMENT THE PRESELECTED WORKING TEST CASE FROM THE FOLLOWING LIST
C  NOTE: ONLY THE LAST UNCOMMENTED LINE IS ACTIVE - SUBSTITUTE "C" with "#"
C (**) CREATE NEW ACTIVE TESTCASE NUMBER, REDEFINE ALL APPROPRIATE PARAMETERS 
C============================================================================
cdefine TESTCASE 4  /* Ocean model .....  not ready yet                    */
cdefine TESTCASE 10 /* 3D real orographic flow: terrain-following/IMB      */
cdefine TESTCASE 20 /* NWP - COSMO COUPLING                                */
cdefine TESTCASE 21 /* LAM - WRF COUPLING                                  */
cdefine TESTCASE 22 /* GCM - CAM COUPLING  .... not ready yet              */
cdefine TESTCASE 30 /* MICRO - buble test ... not ready yet                */
cdefine TESTCASE 31 /* MICRO - DNS/chamber turbulence ... not ready yet    */
cdefine TESTCASE 32 /* MICRO - warm (BOMEX/RICO/IMPACT) .. not ready yet   */
cdefine TESTCASE 33 /* MICRO - cold ... not ready yet                      */
cdefine TESTCASE 40 /* IMB - POROUS MEDIA .... not ready yet               */
cdefine TESTCASE 41 /* IMB - OBSTACLE/IDEL INPUT .... not ready yet        */
cdefine TESTCASE 42 /* IMB - URBAN FLOWS/POZNAN/IDEAL INPUT                */
cdefine TESTCASE 43 /* IMB - URBAN FLOWS/OKLAHOMA CITY/REAL SOUNDING       */
cdefine TESTCASE 44 /* IMB - URBAN FLOWS/OKLAHOMA CITY/WRF INPUT           */
cdefine TESTCASE 45 /* IMB - EULAG-D (DARCY) .... not ready yet            */
C--------------------------------------------------------------------------*/
cdefine TESTCASE 3  /* Exoplanets ... Zonal flow                           */
cdefine TESTCASE 2  /* EARTH ... JW model imbalance                        */
#define TESTCASE 1  /* SOLAR - MHD model in the future                     */
cdefine TESTCASE 5  /* PBL - CARTESIAN, CONVECTIVE                         */
cdefine TESTCASE 6  /* PBL - CARTESIAN, SHEAR DRIVEN/NEUTRAL               */
cdefine TESTCASE 7  /* PBL - CARTESIAN, STABLE                             */
cdefine TESTCASE 0  /* DEFAULT-CARTESIAN 2D OROGRAOPHICAL FLOW             */
C--------------------------------------------------------------------------*/
cdefine TESTCASE 44 /* IMB - URBAN FLOWS/OKLAHOMA CITY/WRF INPUT           */
cdefine TESTCASE 43 /* IMB - IDEALISTIC URBAN FLOWS/POZNAN                 */
C--------------------------------------------------------------------------*/
cdefine TESTCASE 50 /* WAKE EDDIES, 2D XY plane                            */
C############################################################################
C                                                                           #
C     !!!      Redefine options to be consistent with TESTCASES       !!!   #
C                                                                           #
C############################################################################
#if (TESTCASE == 0)
#define SGS 1
#define J3DIM 0
#define IMRSB 0
#endif
#if (TESTCASE == 1 || TESTCASE == 2 || TESTCASE == 3)
#define SGS 0
#define POLES 1
#define H_SUAREZ 0
#endif
#if (TESTCASE == 5 || TESTCASE == 6 || TESTCASE == 7)
#define SGS 1
#endif
#if (TESTCASE == 20)
#define SGS 1
#define POLES 0
#define BCREF 1
#define VARENV 0
#define MOISTMOD 1
#endif
#if (TESTCASE == 42 || TESTCASE == 43 || TESTCASE == 44)
#define IMRSB 1
#define SGS 1
#endif
#if (TESTCASE == 50)
#define J3DIM 0
#define SGS 0 
#define CHEMIS 1    /* PASSIVE TRACER: 0=OFF/1=ON                          */
#define NETCDFD 0   /* 2=single precision, 1=double precision              */
#endif
!.....7..0.........0.........0.........0.........0.........0.........012
      PROGRAM RSLICES           
!.....7..0.........0.........0.........0.........0.........0.........012
#include "param.nml"
      integer :: iplane
      character(len=5) :: sett
      character(len=5) :: true='y'
      character(len=5) :: false='n'
      logical :: file_exists

      common/grid/ time,dt,dx,dy,dz,dti,dxi,dyi,dzi,zb,igrid,j3

c igrid=0 for A grid (default); igrid=1 for B grid (special option)
      data igrid/0/
      common/metric0/ xcr(n,m),ycr(n,m)
      common/cyclbc/ ibcx,ibcy,ibcz,irlx,irly,irdbc

create model variables
      real x(n),y(m),z(l)
      real u0(n,m),v0(n,m),o0(n,m),w0(n,m),th(n,m),p(n,m)
      real ox(n,m),oy(n,m),oz(n,m)

create semi-lagrangian variables
      common/profl/th0(n,m),rho(n,m),the(n,m),
     1             ue(n,m),ve(n,m),zcr(l)

      common/metric/ zs(n,m),zh(n,m),gi(n,m),gmus(l),gmul(l),
     .               c13(n,m),c23(n,m),h13(n,m),h23(n,m)

      data zs/nm*0./, zh/nm*0./

create moist model variables
      real qv(nms,mms), qc(nms,mms), qr(nms,mms),
     1    fqv(nms,mms),fqc(nms,mms),fqr(nms,mms),
     1    qia(nic,mic),qib(nic,mic)

      real chm(nch,mch,nspc)

create moist ambient profiles
      common/profm/tme(nms,mms),qve(nms,mms)
close moist model
     
c     IMPORTANT SLICE PARAMETERS
      parameter(msl=3)
      parameter(nsl=3)
      parameter(lsl=7)
!
      call wparam ()
      print *,'START CODE'
      print *, 'You are considering the next number of slides:'
      print *, 'msl= ',msl
      print *, 'nsl= ',nsl
      print *, 'lsl= ',lsl
      print *, 'Is this correct? (y/n)'
      read (*,*), sett
       if(sett.ne.true) then 
         print *, 'STOP, check your vales of msl, nsl and lsl'
         stop
      endif

      if (icyz.eq.1) then
       if (st.ne.0.) stop 'with icyz=1 st must be 0.'
      endif

compute some relevant constants
constants for computational grid 
      j3=1                   !j3=1 if there is a third dimension y
      if(m.eq.1) j3=0
      if(lxyz.eq.1) then
      dx=dx/float(n-1)
      dy=dy/float(m-j3)
      dz=dz/float(l-1)
      endif
      dy=dy*j3+float(1-j3)
      dxi=1./dx
      dyi=1./dy*j3
      dzi=1./dz
      dti=1./dt
      gc1=dt*dxi
      gc2=dt*dyi
      gc3=dt*dzi

constants for reference state and lateral boundary conditions
      pi=acos(-1.)
      fcor1=0.
      fcor2=icorio*fcr0*cos(pi/180.*ang)
      fcor3=icorio*fcr0*sin(pi/180.*ang)
      ibcx=icyx
      ibcy=icyy*j3
      ibcz=icyz
c     if(ibcz.eq.1) igrid=0
      irlx=irelx
      irly=irely*j3
      irdbc=iradbc
      if(j3.eq.1.and.ibcx*ibcy.eq.1
     .           .or.j3.eq.0.and.ibcx.eq.1) irdbc=0

conditions of the initial state ***********************************
      do 1 i=1,n
      x(i)=(i-1)*dx-((n+1)*.5-1)*dx*0.
    1 continue
      do 2 j=1,m
    2 y(j)=(j-1)*dy-.5*(m-1)*dy*(1-ibcy)*0.
      do 3 k=1,l
      z(k)=(k-1)*dz
    3 continue
      zb=z(l)

      print *,'START SLICES'
      print *, 'Choose the plane that you want to read:'
      print *, '1 -- xz plane '
      print *, '2 -- yz plane '
      print *, '3 -- xy plane '
      read (*,*) iplane
! Plane XZ      
      if(iplane.eq.1) then
!
        INQUIRE(FILE="uxz.dat", EXIST=file_exists)
        if(file_exists) then
           print *, 'File uxz.dat exist = ',file_exists
           open(1,file="uxz.dat")
           close(1,status='delete')
        endif
        INQUIRE(FILE="vxz.dat", EXIST=file_exists)
        if(file_exists) then
           print *, 'File vxz.dat exist = ',file_exists
           open(1,file="vxz.dat")
           close(1,status='delete')
        endif
        INQUIRE(FILE="wxz.dat", EXIST=file_exists)
        if(file_exists) then
           print *, 'File wxz.dat exist = ',file_exists
           open(1,file="wxz.dat")
           close(1,status='delete')
        endif
!
        inr=31
        read(inr) n1,m1,l1,lch1,nspc1,lms1,lic1,msl1
        print *,  n ,m ,l ,lch ,nspc ,lms ,lic ,msl 
        print *,  n1,m1,l1,lch1,nspc1,lms1,lic1,msl1
        do kf=0,nfil-1
          print *, 'kf =', kf
          call slicesxz(x,y,z,kf,inr,msl)
        enddo
! Plane YZ
      elseif(iplane.eq.2) then
!
        INQUIRE(FILE="uyz.dat", EXIST=file_exists)
        if(file_exists) then
           print *, 'File uyz.dat exist = ',file_exists
           open(1,file="uyz.dat")
           close(1,status='delete')
        endif
        INQUIRE(FILE="vyz.dat", EXIST=file_exists)
        if(file_exists) then
           print *, 'File vyz.dat exist = ',file_exists
           open(1,file="vyz.dat")
           close(1,status='delete')
        endif
        INQUIRE(FILE="wyz.dat", EXIST=file_exists)
        if(file_exists) then
           print *, 'File wyz.dat exist = ',file_exists
           open(1,file="wyz.dat")
           close(1,status='delete')
        endif
!
        inr=32
        read(inr) n1,m1,l1,lch1,nspc1,lms1,lic1,nsl1
        print *,  n ,m ,l ,lch ,nspc ,lms ,lic ,nsl
        print *,  n1,m1,l1,lch1,nspc1,lms1,lic1,nsl1
        do kf=0,nfil-1
           call slicesyz(x,y,z,kf,inr,nsl)
        enddo
! Plane XY
      else
!
        INQUIRE(FILE="uxy.dat", EXIST=file_exists)
        if(file_exists) then
           print *, 'File uxy.dat exist = ',file_exists
           open(1,file="uxy.dat")
           close(1,status='delete')
        endif
        INQUIRE(FILE="vxy.dat", EXIST=file_exists)
        if(file_exists) then
           print *, 'File vxy.dat exist = ',file_exists
           open(1,file="vxy.dat")
           close(1,status='delete')
        endif
        INQUIRE(FILE="wxy.dat", EXIST=file_exists)
        if(file_exists) then
           print *, 'File wxy.dat exist = ',file_exists
           open(1,file="wxy.dat")
           close(1,status='delete')
        endif
!
        inr=33
        print *,'START READ'
        read(inr) n1,m1,l1,lch1,nspc1,lms1,lic1,lsl1
        print *,  n ,m ,l ,lch ,nspc ,lms ,lic ,lsl
        print *,  n1,m1,l1,lch1,nspc1,lms1,lic1,lsl1
        do kf=0,nfil-1
          call slicesxy(x,y,z,kf,inr,lsl)
        enddo
      endif

      stop 'main'
      end   
c.....7..0.........0.........0.........0.........0.........0.........012
      real function rmaxval(a)
#include "param.nml"
c     include 'param2.nml'
      dimension a(n,m)

      fmax=-1.0e15
      do j=1,m
       do i=1,n
       fmax=max(fmax,a(i,j))
       enddo
      enddo

      rmaxval=fmax

      return 
      end

      subroutine fminmaxsum(a,n,m,l,str)
      character*5 str
      dimension a(n,m,l)

      fmax=-1.0e15
      fmin= 1.0e15
      fsum= 0.0
      do k=1,l
       do j=1,m
        do i=1,n
        fmax=max(fmax,a(i,j,k))
        fmin=min(fmin,a(i,j,k))
        fsum=fsum+a(i,j,k)
        enddo
       enddo
      enddo
      print *,str,fmin,fmax,fsum/float(n*m*l)
 10   format(3x,a5,' min,max,sum: ',3e12.4)

      return
      end

      subroutine metryc(x,y,z)
#include "param.nml"
c     include 'param2.nml'
      dimension x(n),y(m),z(l)
      common/metric/ zs(n,m),zh(n,m),gi(n,m),gmus(l),gmul(l),
     .               s13(n,m),s23(n,m),h13(n,m),h23(n,m)
      dimension zsd(n,m),zhd(n,m),strxd(n,m),stryd(n,m),
     +      strxx(n,m),strxy(n,m),stryx(n,m),stryy(n,m)
      common/grid/ time,dt,dx,dy,dz,dti,dxi,dyi,dzi,zb,igrid,j3
      common/cyclbc/ ibcx,ibcy,ibcz,irlx,irly,irdbc

      dx0=dxi*0.5
      dy0=dyi*0.5

      do 1 k=1,l
      gmus(k)=1.
    1 gmul(k)=(zb-z(k))
      do 2 j=1,m
      do 2 i=1,n
      gi(i,j)=zb/(zb-zs(i,j))
      strxx(i,j)= 1.                         ! d(xb)/dx
      strxy(i,j)= 0.                         ! d(xb)/dy
      strxd(i,j)= 0.                         ! d(xb)/dt
      stryx(i,j)= 0.                         ! d(yb)/dx
      stryy(i,j)= 1.                         ! d(yb)/dy
    2 stryd(i,j)= 0.                         ! d(yb)/dt

C --- compute gradients of zs for g13, g23 coefficients
      do 20 i=2,n-1
c --- interior points
        do 21 j=1+j3,m-j3
          zsxb=dx0*(1./gi(i+1,j)-1./gi(i-1,j))*gi(i,j)   !zsxb/(H-zs)
          zsyb=dy0*(1./gi(i,j+j3)-1./gi(i,j-j3))*gi(i,j) !zsyb/(H-zs)
          zhxb=dx0*(zh(i+1, j)-zh(i-1, j))*gi(i,j)       !zhxb/G
          zhyb=dy0*(zh(i,j+j3)-zh(i,j-j3))*gi(i,j)       !zhyb/G
          s13(i,j)=strxx(i,j)*zsxb+stryx(i,j)*zsyb
          s23(i,j)=strxy(i,j)*zsxb+stryy(i,j)*zsyb
          h13(i,j)=strxx(i,j)*zhxb+stryx(i,j)*zhyb
   21     h23(i,j)=strxy(i,j)*zhxb+stryy(i,j)*zhyb
c --- Southern boundary region
        zsxb=dx0*(1./gi(i+1,1)-1./gi(i-1,1))*gi(i,1)
        zsyb=ibcy*dy0*(1./gi(i,1+j3)-1./gi(i,m-j3))*gi(i,1)
        zhxb=dx0*(zh(i+1, 1)-zh(i-1, 1))*gi(i,1)
        zhyb=ibcy*dy0*(zh(i,1+j3)-zh(i,m-j3))*gi(i,1)
        s13(i,1)=strxx(i,1)*zsxb+stryx(i,1)*zsyb
        s23(i,1)=strxy(i,1)*zsxb+stryy(i,1)*zsyb
        h13(i,1)=strxx(i,1)*zhxb+stryx(i,1)*zhyb
        h23(i,1)=strxy(i,1)*zhxb+stryy(i,1)*zhyb
c --- Northern boundary region
        zsxb=dx0*(1./gi(i+1,m)-1./gi(i-1,m))*gi(i,m)
        zsyb=ibcy*dy0*(1./gi(i,1+j3)-1./gi(i,m-j3))*gi(i,m)
        zhxb=dx0*(zh(i+1, m)-zh(i-1, m))*gi(i,m)
        zhyb=ibcy*dy0*(zh(i,1+j3)-zh(i,m-j3))*gi(i,m)
        s13(i,m)=strxx(i,m)*zsxb+stryx(i,m)*zsyb
        s23(i,m)=strxy(i,m)*zsxb+stryy(i,m)*zsyb
        h13(i,m)=strxx(i,m)*zhxb+stryx(i,m)*zhyb
   20   h23(i,m)=strxy(i,m)*zhxb+stryy(i,m)*zhyb
        do j=1+j3,m-j3
c --- Western boundary region
          zsxb=ibcx*dx0*(1./gi(2,j)-1./gi(n-1,j))*gi(1,j) ! zsxb/(H-zs)
          zsyb=dy0*(1./gi(1,j+j3)-1./gi(1,j-j3))*gi(1,j) !zsyb/(H-zs)
          zhxb=ibcx*dx0*(zh(2,j)-zh(n-1,j))*gi(1,j)       !zhxb/G
          zhyb=dy0*(zh(1,j+j3)-zh(1,j-j3))*gi(1,j)       !zhyb/G
          s13(1,j)=strxx(1,j)*zsxb+stryx(1,j)*zsyb
          s23(1,j)=strxy(1,j)*zsxb+stryy(1,j)*zsyb
          h13(1,j)=strxx(1,j)*zhxb+stryx(1,j)*zhyb
          h23(1,j)=strxy(1,j)*zhxb+stryy(1,j)*zhyb
c --- Estern boundary region
          zsxb=ibcx*dx0*(1./gi(2,j)-1./gi(n-1,j))*gi(n,j) ! zsxb/(H-zs)
          zsyb=dy0*(1./gi(n,j+j3)-1./gi(n,j-j3))*gi(n,j) !zsyb/(H-zs)
          zhxb=ibcx*dx0*(zh(2,j)-zh(n-1,j))*gi(n,j)       !zhxb/G
          zhyb=dy0*(zh(n,j+j3)-zh(n,j-j3))*gi(n,j)       !zhyb/G
          s13(n,j)=strxx(n,j)*zsxb+stryx(n,j)*zsyb
          s23(n,j)=strxy(n,j)*zsxb+stryy(n,j)*zsyb
          h13(n,j)=strxx(n,j)*zhxb+stryx(n,j)*zhyb
          h23(n,j)=strxy(n,j)*zhxb+stryy(n,j)*zhyb
        enddo
c --- ES corner
         zsxb=ibcx*dx0*(1./gi(2,1)-1./gi(n-1,1))*gi(n,1) ! zsxb/(H-zs)
         zsyb=ibcy*dy0*(1./gi(n,1+j3)-1./gi(n,m-j3))*gi(n,1) !zsyb/(H-zs)
         zhxb=ibcx*dx0*(zh(2,1)-zh(n-1,1))*gi(n,1)          !zhxb/G
         zhyb=ibcy*dy0*(zh(n,1+j3)-zh(n,m-j3))*gi(n,1)      !zhyb/G
         s13(n,1)=strxx(n,1)*zsxb+stryx(n,1)*zsyb
         s23(n,1)=strxy(n,1)*zsxb+stryy(n,1)*zsyb
         h13(n,1)=strxx(n,1)*zhxb+stryx(n,1)*zhyb
         h23(n,1)=strxy(n,1)*zhxb+stryy(n,1)*zhyb
c --- EN corner
         zsxb=ibcx*dx0*(1./gi(2,m)-1./gi(n-1,m))*gi(n,m) ! zsxb/(H-zs)
         zsyb=ibcy*dy0*(1./gi(n,1+j3)-1./gi(n,m-j3))*gi(n,m) !zsyb/(H-zs)
         zhxb=ibcx*dx0*(zh(2,m)-zh(n-1,m))*gi(n,m)          !zhxb/G
         zhyb=ibcy*dy0*(zh(n,1+j3)-zh(n,m-j3))*gi(n,m)      !zhyb/G
         s13(n,m)=strxx(n,m)*zsxb+stryx(n,m)*zsyb
         s23(n,m)=strxy(n,m)*zsxb+stryy(n,m)*zsyb
         h13(n,m)=strxx(n,m)*zhxb+stryx(n,m)*zhyb
         h23(n,m)=strxy(n,m)*zhxb+stryy(n,m)*zhyb
c --- WN corner
         zsxb=ibcx*dx0*(1./gi(2,m)-1./gi(n-1,m))*gi(1,m) ! zsxb/(H-zs)
         zsyb=ibcy*dy0*(1./gi(1,1+j3)-1./gi(1,m-j3))*gi(1,m) !zsyb/(H-zs)
         zhxb=ibcx*dx0*(zh(2,m)-zh(n-1,m))*gi(1,m)          !zhxb/G
         zhyb=ibcy*dy0*(zh(1,1+j3)-zh(1,m-j3))*gi(1,m)      !zhyb/G
         s13(1,m)=strxx(1,m)*zsxb+stryx(1,m)*zsyb
         s23(1,m)=strxy(1,m)*zsxb+stryy(1,m)*zsyb
         h13(1,m)=strxx(1,m)*zhxb+stryx(1,m)*zhyb
         h23(1,m)=strxy(1,m)*zhxb+stryy(1,m)*zhyb
c --- WS corner
         zsxb=ibcx*dx0*(1./gi(2,1)-1./gi(n-1,1))*gi(1,1) ! zsxb/(H-zs)
         zsyb=ibcy*dy0*(1./gi(1,1+j3)-1./gi(1,m-j3))*gi(1,1) !zsyb/(H-zs)
         zhxb=ibcx*dx0*(zh(2,1)-zh(n-1,1))*gi(1,1)          !zhxb/G
         zhyb=ibcy*dy0*(zh(1,1+j3)-zh(1,m-j3))*gi(1,1)      !zhyb/G
         s13(1,1)=strxx(1,1)*zsxb+stryx(1,1)*zsyb
         s23(1,1)=strxy(1,1)*zsxb+stryy(1,1)*zsyb
         h13(1,1)=strxx(1,1)*zhxb+stryx(1,1)*zhyb
         h23(1,1)=strxy(1,1)*zhxb+stryy(1,1)*zhyb

      return
      end


      subroutine filstr(a,n1,n2,n3)
      dimension a(n1,n2,n3)
#include "param.nml"
c     include 'param2.nml'
      dimension sx(n),sy(m),sz(l)
      common/grid/ time,dt,dx,dy,dz,dti,dxi,dyi,dzi,zb,igrid,j3
      common/cyclbc/ ibcx,ibcy,ibcz,irlx,irly,irdbc

      do k=1,l
       do j=1,m
        do i=2,n-1
         sx(i)=0.25*(a(i+1,j,k)+2.*a(i,j,k)+a(i-1,j,k))
        enddo
         sx(1)=ibcx*0.25*(a(2,j,k)+2.*a(1,j,k)+a(n-1,j,k))
     .        +(1-ibcx)*a(1,j,k)
         sx(n)=ibcx*sx(1)+(1-ibcx)*a(n,j,k)
        do i=1,n
         a(i,j,k)=sx(i)
        enddo
       enddo
      enddo

      if(j3.eq.1) then
      do k=1,l
        do i=1,n
        do j=1+j3,m-j3
         sy(j)=0.25*(a(i,j+j3,k)+2.*a(i,j,k)+a(i,j-j3,k))
        enddo
         sy(1)=ibcy*0.25*(a(i,1+j3,k)+2.*a(i,1,k)+a(i,m-j3,k))
     .        +(1-ibcy)*a(i,1,k)
         sy(m)=ibcy*sy(1)+(1-ibcy)*a(i,m,k)
        do j=1,m
         a(i,j,k)=sy(j)
        enddo
       enddo
      enddo
      endif
 
      do j=1,m
       do i=1,n
        do k=2,l-1
         sz(k)=0.25*(a(i,j,k+1)+2.*a(i,j,k)+a(i,j,k-1))
        enddo
        if(ibcz.eq.0) then
         sz(1)=a(i,j,1)
         sz(l)=a(i,j,l)
        else
         sz(1)=0.25*(a(i,j,2)+2.*a(i,j,1)+a(i,j,l-1))
         sz(l)=0.25*(a(i,j,2)+2.*a(i,j,l)+a(i,j,l-1))
        endif
        do k=1,l
         a(i,j,k)=sz(k)
         enddo
       enddo
      enddo
 
      return
      end

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine slicesxz(x,y,z,it,inr,msl)
#include "param.nml"
c      include 'param2.nml'
      real x(n),y(m),z(l)
      dimension jindx(msl)
      dimension u(n,l),
     .          v(n,l),
     .          w(n,l),
     .         ox(n,l),
     .         oy(n,l),
     .         oz(n,l),
     .         th(n,l),
     .          p(n,l),
     .        rho(n,l),
     .        the(n,l),
     .        chm(nch, lch, nspc),
     .         qv(nms, lms),
     .         qc(nms, lms),
     .         qr(nms, lms),
     .        qia(nic, lic),
     .        qib(nic, lic),
     .         zs(n,l),
     .         zh(n,l)
      real*8 timesl
      dimension uxy(n,l),vxy(n,l),fxy(n,l)
      common/gora/ xml0,yml0,amp
      common/grid/ time,dt,dx,dy,dz,dti,dxi,dyi,dzi,zb,igrid,j3


      read(inr) jindx
      read(inr) inz
      read(inr) it,timesl
      time=timesl

      print *,'readslicexz:',inr,' iind ',jindx
      print *,'readslicexz:',inr,' it,time ',it,time

      call readslice(zs,n,m,inr,'zs   ')
C     call readslice(zh,n,m,inr,'zh   ')

      do jsl=1,msl
      call readslice(u  ,n,l,inr,'u    ')
      call readslice(v  ,n,l,inr,'v    ')
      call readslice(w  ,n,l,inr,'w    ')
      call readslice(ox ,n,l,inr,'ox   ')
      call readslice(oy ,n,l,inr,'oy   ')
      call readslice(oz ,n,l,inr,'oz   ')
      call readslice(th ,n,l,inr,'th   ')
      call readslice(p  ,n,l,inr,'p    ')
      if(lch.gt.1) then
        do ispc=1,nspc
          call readslice(chm(1,1,ispc),n,l,inr,'chm  ')
        enddo
      endif
      if(lms.gt.1) then
      call readslice(qv ,n,l,inr,'qv   ')
      call readslice(qc ,n,l,inr,'qc   ')
      call readslice(qr ,n,l,inr,'qr   ')
      endif
      if(lic.gt.1) then
      call readslice(qia,n,l,inr,'qia  ')
      call readslice(qib,n,l,inr,'qib  ')
      endif

      amp = rmaxval(zs)
      call metryc(x,y,z)

      nclv=20

      enddo
      
      open(1,file='uxz.dat',form='unformatted',position='append')
      open(2,file='vxz.dat',form='unformatted',position='append')
      open(3,file='wxz.dat',form='unformatted',position='append')
      write(1) u,time
      write(2) u,time
      write(3) u,time
      close(1)
      close(2)
      close(3)
      
      return
      end

      subroutine slicesyz(x,y,z,it,inr,nsl)
#include "param.nml"
c      include 'param2.nml'
      real x(n),y(m),z(l)
      dimension iindx(nsl)
      dimension u(m,l),
     .          v(m,l),
     .          w(m,l),
     .         ox(m,l),
     .         oy(m,l),
     .         oz(m,l),
     .         th(m,l),
     .          p(m,l),
     .        rho(m,l),
     .        the(m,l),
     .        chm(mch, lch, nspc),
     .         qv(mms, lms),
     .         qc(mms, lms),
     .         qr(mms, lms),
     .        qia(mic, lic),
     .        qib(mic, lic),
     .         zs(m,l),
     .         zh(m,l)
      real*8 timesl
      dimension uxy(m,l),vxy(m,l),fxy(m,l)
      common/gora/ xml0,yml0,amp
      common/grid/ time,dt,dx,dy,dz,dti,dxi,dyi,dzi,zb,igrid,j3


      read(inr) iindx
      read(inr) inz
      read(inr) it,timesl
      time=timesl

      print *,'readsliceyz:',inr,' kind ',iindx
      print *,'readsliceyz:',inr,' it,time ',it,time

      call readslice(zs,n,m,inr,'zs   ')

      do isl=1,nsl
         call readslice(u  ,m,l,inr,'u    ')
         call readslice(v  ,m,l,inr,'v    ')
         call readslice(w  ,m,l,inr,'w    ')
         call readslice(ox ,m,l,inr,'ox   ')
         call readslice(oy ,m,l,inr,'oy   ')
         call readslice(oz ,m,l,inr,'oz   ')
         call readslice(th ,m,l,inr,'th   ')
         call readslice(p  ,m,l,inr,'p    ')
         if(lch.gt.1) then
            do ispc=1,nspc
               call readslice(chm(1,1,ispc),m,l,inr,'chm  ')
            enddo
         endif
         if(lms.gt.1) then
            call readslice(qv ,m,l,inr,'qv   ')
            call readslice(qc ,m,l,inr,'qc   ')
            call readslice(qr ,m,l,inr,'qr   ')
         endif
         if(lic.gt.1) then
            call readslice(qia,m,l,inr,'qia  ')
            call readslice(qib,m,l,inr,'qib  ')
         endif
         
         amp = rmaxval(zs)
         call metryc(x,y,z)
         
         nclv=20
      enddo
!
      open(1,file='uyz.dat',form='unformatted',position='append')
      open(2,file='vyz.dat',form='unformatted',position='append')
      open(3,file='wyz.dat',form='unformatted',position='append')
      write(1) u,time
      write(2) u,time
      write(3) u,time
      close(1)
      close(2)
      close(3)
!
      return
      end

      subroutine slicesxy(x,y,z,it,inr,lsl)
#include "param.nml"
c      include 'param2.nml'

      real x(n),y(m),z(l)
      dimension kindx(lsl)
      dimension u(n,m),
     .          v(n,m),
     .          w(n,m),
     .         ox(n,m),
     .         oy(n,m),
     .         oz(n,m),
     .         th(n,m),
     .          p(n,m),
     .        rho(n,m),
     .        the(n,m),
     .        chm(nch, mch, nspc),
     .         qv(nms, mms),
     .         qc(nms, mms),
     .         qr(nms, mms),
     .        qia(nic, mic),
     .        qib(nic, mic),
     .         zs(n,m),
     .         zh(n,m)
      real*8 timesl
      dimension uxy(n,m),vxy(n,m),fxy(n,m)
      common/gora/ xml0,yml0,amp
      common/grid/ time,dt,dx,dy,dz,dti,dxi,dyi,dzi,zb,igrid,j3

      read(inr) kindx
      read(inr) inz
      read(inr) it,timesl
      time=timesl

      print *,'readslicexy:',inr,' kind ',kindx
      print *,'readslicexy:',inr,' it,time ',it,time

      call readslice(zs,n,m,inr,'zs   ')
C     call readslice(zh,n,m,inr,'zh   ')

CCCCCCCCCCCCCCCCCCCCCCCC

      do ksl=1,lsl
      call readslice(u  ,n,m,inr,'u    ')
      call readslice(v  ,n,m,inr,'v    ')
      call readslice(w  ,n,m,inr,'w    ')
      call readslice(ox ,n,m,inr,'ox   ')
      call readslice(oy ,n,m,inr,'oy   ')
      call readslice(oz ,n,m,inr,'oz   ')
      call readslice(th ,n,m,inr,'th   ')
      call readslice(p  ,n,m,inr,'p    ')
C     call readslice(rho,n,m,inr,'rho  ')
C     call readslice(the,n,m,inr,'the  ')
      if(lch.gt.1) then
       do ispc=1,nspc
        call readslice(chm(1,1,ispc),n,m,inr,'chm  ')
       enddo
      endif
      if(lms.gt.1) then
      call readslice(qv ,n,m,inr,'qv   ')
      call readslice(qc ,n,m,inr,'qc   ')
      call readslice(qr ,n,m,inr,'qr   ')
      endif
      if(lic.gt.1) then
      call readslice(qia,n,m,inr,'qia  ')
      call readslice(qib,n,m,inr,'qib  ')
      endif

CCCCCCCCCCCCCCCCCCCCCCCC

      amp = rmaxval(zs)
      call metryc(x,y,z)

CCCCCCCCCCCCCCCCCCCCCCCC

      nclv=20

      enddo
!
      open(1,file='uxy.dat',form='unformatted',position='append')
      open(2,file='vxy.dat',form='unformatted',position='append')
      open(3,file='wxy.dat',form='unformatted',position='append')
      write(1) u,time
      write(2) u,time
      write(3) u,time
      close(1)
      close(2)
      close(3)
!
      return
      end

      subroutine readslice(f,n1,n2,inr,str)
#include "param.nml"
      dimension f(n1,n2)
      character*5 str
      real*8 f8(n1,n2)
!
      read(inr) f8
      do j=1,n2
         do i=1,n1
            f(i,j)=f8(i,j)
         end do
      end do
      call fminmaxsum(f,n1,n2,1,str)

      return
      end
! ccccccccccccccccccccccccccccccccccccccccccccccc

      subroutine wparam ()
      integer :: nt,n,m,l
      real :: dx00,dy00,dz00,dt00
      integer :: lxyz
      call getpars (nt,n,m,l)
      print *,'nt = ',nt
      namelist / mylist / nt,n,m,l

      open(8,file='mylist.nml',delim='apostrophe')
      write(unit=8,NML=mylist)
      print *,'writing ... '
      close(8)

      return
      end

      subroutine getpars(ntt,nn,mm,ll,llxyz,ddx00)
#include "param.nml"
      ntt = nt
      mm = m
      nn = n
      ll = l
      return
      end
