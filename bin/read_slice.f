#include "define.h"
!.....7..0.........0.........0.........0.........0.........0.........012
      PROGRAM RSLICES           
!.....7..0.........0.........0.........0.........0.........0.........012
#include "param.nml"
      integer :: iplane
      character(len=5) :: sett
      character(len=5) :: true='y'
      character(len=5) :: false='n'
      character(len=1024) :: cmd
      logical :: file_exists

      common/grid/ time,dt,dx,dy,dz,dti,dxi,dyi,dzi,zb,igrid,j3

c igrid=0 for A grid (default); igrid=1 for B grid (special option)
      data igrid/0/
      common/metric0/ xcr(n,m),ycr(n,m)
      common/cyclbc/ ibcx,ibcy,ibcz,irlx,irly,irdbc

create model variables
      real x(n),y(m),z(l)
      real u0(n,m),v0(n,m),o0(n,m),w0(n,m),th(n,m),p(n,m)
      real ox(n,m),oy(n,m),oz(n,m)

create semi-lagrangian variables
      common/profl/th0(n,m),rho(n,m),the(n,m),
     1             ue(n,m),ve(n,m),zcr(l)

      common/metric/ zs(n,m),zh(n,m),gi(n,m),gmus(l),gmul(l),
     .               c13(n,m),c23(n,m),h13(n,m),h23(n,m)

create moist model variables
      real qv(nms,mms), qc(nms,mms), qr(nms,mms),
     1    fqv(nms,mms),fqc(nms,mms),fqr(nms,mms),
     1    qia(nic,mic),qib(nic,mic)

      real chm(nch,mch,nspc)

create moist ambient profiles
      common/profm/tme(nms,mms),qve(nms,mms)
close moist model
     
c     IMPORTANT SLICE PARAMETERS
      parameter(msl=1)
      parameter(nsl=1)
      parameter(lsl=4)
!
      call wparam ()
      print *,'START CODE'
      print *, 'You are considering the next number of slides:'
      print *, 'msl= ',msl
      print *, 'nsl= ',nsl
      print *, 'lsl= ',lsl
      print *, 'Is this correct? (y/n)'
      read (*,*), sett
       if(sett.ne.true) then 
         print *, 'STOP, check your vales of msl, nsl and lsl'
         stop
      endif

      if (icyz.eq.1) then
       if (st.ne.0.) stop 'with icyz=1 st must be 0.'
      endif

compute some relevant constants
constants for computational grid 
      j3=1                   !j3=1 if there is a third dimension y
      if(m.eq.1) j3=0
      if(lxyz.eq.1) then
      dx=dx/float(n-1)
      dy=dy/float(m-j3)
      dz=dz/float(l-1)
      endif
      dy=dy*j3+float(1-j3)
      dxi=1./dx
      dyi=1./dy*j3
      dzi=1./dz
      dti=1./dt
      gc1=dt*dxi
      gc2=dt*dyi
      gc3=dt*dzi

constants for reference state and lateral boundary conditions
      pi=acos(-1.)
      fcor1=0.
      fcor2=icorio*fcr0*cos(pi/180.*ang)
      fcor3=icorio*fcr0*sin(pi/180.*ang)
      ibcx=icyx
      ibcy=icyy*j3
      ibcz=icyz
c     if(ibcz.eq.1) igrid=0
      irlx=irelx
      irly=irely*j3
      irdbc=iradbc
      if(j3.eq.1.and.ibcx*ibcy.eq.1
     .           .or.j3.eq.0.and.ibcx.eq.1) irdbc=0

conditions of the initial state ***********************************
      do 1 i=1,n
      x(i)=(i-1)*dx-((n+1)*.5-1)*dx*0.
    1 continue
      do 2 j=1,m
    2 y(j)=(j-1)*dy-.5*(m-1)*dy*(1-ibcy)*0.
      do 3 k=1,l
      z(k)=(k-1)*dz
    3 continue
      zb=z(l)

      print *,'START SLICES'
      print *, 'Choose the plane that you want to read:'
      print *, '1 -- xz plane '
      print *, '2 -- yz plane '
      print *, '3 -- xy plane '
      read (*,*) iplane
! Plane XZ      
      if(iplane.eq.1) then
!
        INQUIRE(FILE="uxz01", EXIST=file_exists)
        if(file_exists) then
           print *, 'File uxz01 exist, deleting files ...  = ',file_exists
           cmd = "rm uxz* vxz* wxz* oxxz* oyxz* ozxz*" 
           call system(cmd)
        endif

!        INQUIRE(FILE="vxz.dat", EXIST=file_exists)
!        if(file_exists) then
!           print *, 'File vxz.dat exist = ',file_exists
!           open(1,file="vxz.dat")
!           close(1,status='delete')
!        endif
!        INQUIRE(FILE="wxz.dat", EXIST=file_exists)
!        if(file_exists) then
!           print *, 'File wxz.dat exist = ',file_exists
!           open(1,file="wxz.dat")
!           close(1,status='delete')
!        endif
!
        inr=31
        read(inr) n1,m1,l1,lch1,nspc1,lms1,lic1,msl1
        print *, 'n, m, l, lch, nspc, lms, lic, msl,'
        print *,  n ,m ,l ,lch ,nspc ,lms ,lic ,msl 
        print *,  n1,m1,l1,lch1,nspc1,lms1,lic1,msl1
        do kf=0,nt/nslice-1
          print *, 'kf =', kf
          call slicesxz(x,y,z,inr,msl)
        enddo
! Plane YZ
        elseif(iplane.eq.2) then
!
        INQUIRE(FILE="uyz01", EXIST=file_exists)
        if(file_exists) then
           print *, 'File uxz01 exist = ',file_exists
           cmd = "rm uyz* vyz* wyz* oxyz* oyyz* ozyz*"
           call system(cmd) 
        endif

!        INQUIRE(FILE="uyz.dat", EXIST=file_exists)
!        if(file_exists) then
!           print *, 'File uyz.dat exist = ',file_exists
!           open(1,file="uyz.dat")
!           close(1,status='delete')
!        endif
!        INQUIRE(FILE="vyz.dat", EXIST=file_exists)
!        if(file_exists) then
!           print *, 'File vyz.dat exist = ',file_exists
!           open(1,file="vyz.dat")
!           close(1,status='delete')
!        endif
!        INQUIRE(FILE="wyz.dat", EXIST=file_exists)
!        if(file_exists) then
!           print *, 'File wyz.dat exist = ',file_exists
!           open(1,file="wyz.dat")
!           close(1,status='delete')
!        endif
!
        inr=32
        read(inr) n1,m1,l1,lch1,nspc1,lms1,lic1,nsl1
        print *,  n ,m ,l ,lch ,nspc ,lms ,lic ,nsl
        print *,  n1,m1,l1,lch1,nspc1,lms1,lic1,nsl1
        do kf=0,nt/nslice-1
           print *, "kf =", kf
           call slicesyz(x,y,z,inr,nsl)
        enddo
! Plane XY
      else
!
        INQUIRE(FILE="uxy01", EXIST=file_exists)
        if(file_exists) then
           print *, 'File uxz01 exist = ',file_exists
           cmd = "rm uxy* vxy* wxy* oxxy* oyxy* ozyx* "
           call system(cmd) 
        endif
        
!        INQUIRE(FILE="uxy.dat", EXIST=file_exists)
!        if(file_exists) then
!           print *, 'File uxy.dat exist = ',file_exists
!           open(1,file="uxy.dat")
!           close(1,status='delete')
!        endif
!        INQUIRE(FILE="vxy.dat", EXIST=file_exists)
!        if(file_exists) then
!           print *, 'File vxy.dat exist = ',file_exists
!           open(1,file="vxy.dat")
!           close(1,status='delete')
!        endif
!        INQUIRE(FILE="wxy.dat", EXIST=file_exists)
!        if(file_exists) then
!           print *, 'File wxy.dat exist = ',file_exists
!           open(1,file="wxy.dat")
!           close(1,status='delete')
!        endif
!
        inr=33
        print *,'START READ'
        read(inr) n1,m1,l1,lch1,nspc1,lms1,lic1,lsl1
        print *,  n ,m ,l ,lch ,nspc ,lms ,lic ,lsl
        print *,  n1,m1,l1,lch1,nspc1,lms1,lic1,lsl1
        do kf=0,nt/nslice-1
          print *, "kf =", kf
          call slicesxy(x,y,z,inr,lsl)
        enddo
      endif

      stop 'main'
      end   
c.....7..0.........0.........0.........0.........0.........0.........012
      real function rmaxval(a)
#include "param.nml"
c     include 'param2.nml'
      dimension a(n,m)

      fmax=-1.0e15
      do j=1,m
       do i=1,n
       fmax=max(fmax,a(i,j))
       enddo
      enddo

      rmaxval=fmax

      return 
      end

      subroutine fminmaxsum(a,n,m,l,str)
      character*5 str
      dimension a(n,m,l)

      fmax=-1.0e15
      fmin= 1.0e15
      fsum= 0.0
      do k=1,l
       do j=1,m
        do i=1,n
        fmax=max(fmax,a(i,j,k))
        fmin=min(fmin,a(i,j,k))
        fsum=fsum+a(i,j,k)
        enddo
       enddo
      enddo
      print *,str,fmin,fmax,fsum/float(n*m*l)
 10   format(3x,a5,' min,max,sum: ',3e12.4)

      return
      end

      subroutine metryc(x,y,z)
#include "param.nml"
c     include 'param2.nml'
      dimension x(n),y(m),z(l)
      common/metric/ zs(n,m),zh(n,m),gi(n,m),gmus(l),gmul(l),
     .               s13(n,m),s23(n,m),h13(n,m),h23(n,m)
      dimension zsd(n,m),zhd(n,m),strxd(n,m),stryd(n,m),
     +      strxx(n,m),strxy(n,m),stryx(n,m),stryy(n,m)
      common/grid/ time,dt,dx,dy,dz,dti,dxi,dyi,dzi,zb,igrid,j3
      common/cyclbc/ ibcx,ibcy,ibcz,irlx,irly,irdbc

      dx0=dxi*0.5
      dy0=dyi*0.5

      do 1 k=1,l
      gmus(k)=1.
    1 gmul(k)=(zb-z(k))
      do 2 j=1,m
      do 2 i=1,n
      gi(i,j)=zb/(zb-zs(i,j))
      strxx(i,j)= 1.                         ! d(xb)/dx
      strxy(i,j)= 0.                         ! d(xb)/dy
      strxd(i,j)= 0.                         ! d(xb)/dt
      stryx(i,j)= 0.                         ! d(yb)/dx
      stryy(i,j)= 1.                         ! d(yb)/dy
    2 stryd(i,j)= 0.                         ! d(yb)/dt

C --- compute gradients of zs for g13, g23 coefficients
      do 20 i=2,n-1
c --- interior points
        do 21 j=1+j3,m-j3
          zsxb=dx0*(1./gi(i+1,j)-1./gi(i-1,j))*gi(i,j)   !zsxb/(H-zs)
          zsyb=dy0*(1./gi(i,j+j3)-1./gi(i,j-j3))*gi(i,j) !zsyb/(H-zs)
          zhxb=dx0*(zh(i+1, j)-zh(i-1, j))*gi(i,j)       !zhxb/G
          zhyb=dy0*(zh(i,j+j3)-zh(i,j-j3))*gi(i,j)       !zhyb/G
          s13(i,j)=strxx(i,j)*zsxb+stryx(i,j)*zsyb
          s23(i,j)=strxy(i,j)*zsxb+stryy(i,j)*zsyb
          h13(i,j)=strxx(i,j)*zhxb+stryx(i,j)*zhyb
   21     h23(i,j)=strxy(i,j)*zhxb+stryy(i,j)*zhyb
c --- Southern boundary region
        zsxb=dx0*(1./gi(i+1,1)-1./gi(i-1,1))*gi(i,1)
        zsyb=ibcy*dy0*(1./gi(i,1+j3)-1./gi(i,m-j3))*gi(i,1)
        zhxb=dx0*(zh(i+1, 1)-zh(i-1, 1))*gi(i,1)
        zhyb=ibcy*dy0*(zh(i,1+j3)-zh(i,m-j3))*gi(i,1)
        s13(i,1)=strxx(i,1)*zsxb+stryx(i,1)*zsyb
        s23(i,1)=strxy(i,1)*zsxb+stryy(i,1)*zsyb
        h13(i,1)=strxx(i,1)*zhxb+stryx(i,1)*zhyb
        h23(i,1)=strxy(i,1)*zhxb+stryy(i,1)*zhyb
c --- Northern boundary region
        zsxb=dx0*(1./gi(i+1,m)-1./gi(i-1,m))*gi(i,m)
        zsyb=ibcy*dy0*(1./gi(i,1+j3)-1./gi(i,m-j3))*gi(i,m)
        zhxb=dx0*(zh(i+1, m)-zh(i-1, m))*gi(i,m)
        zhyb=ibcy*dy0*(zh(i,1+j3)-zh(i,m-j3))*gi(i,m)
        s13(i,m)=strxx(i,m)*zsxb+stryx(i,m)*zsyb
        s23(i,m)=strxy(i,m)*zsxb+stryy(i,m)*zsyb
        h13(i,m)=strxx(i,m)*zhxb+stryx(i,m)*zhyb
   20   h23(i,m)=strxy(i,m)*zhxb+stryy(i,m)*zhyb
        do j=1+j3,m-j3
c --- Western boundary region
          zsxb=ibcx*dx0*(1./gi(2,j)-1./gi(n-1,j))*gi(1,j) ! zsxb/(H-zs)
          zsyb=dy0*(1./gi(1,j+j3)-1./gi(1,j-j3))*gi(1,j) !zsyb/(H-zs)
          zhxb=ibcx*dx0*(zh(2,j)-zh(n-1,j))*gi(1,j)       !zhxb/G
          zhyb=dy0*(zh(1,j+j3)-zh(1,j-j3))*gi(1,j)       !zhyb/G
          s13(1,j)=strxx(1,j)*zsxb+stryx(1,j)*zsyb
          s23(1,j)=strxy(1,j)*zsxb+stryy(1,j)*zsyb
          h13(1,j)=strxx(1,j)*zhxb+stryx(1,j)*zhyb
          h23(1,j)=strxy(1,j)*zhxb+stryy(1,j)*zhyb
c --- Estern boundary region
          zsxb=ibcx*dx0*(1./gi(2,j)-1./gi(n-1,j))*gi(n,j) ! zsxb/(H-zs)
          zsyb=dy0*(1./gi(n,j+j3)-1./gi(n,j-j3))*gi(n,j) !zsyb/(H-zs)
          zhxb=ibcx*dx0*(zh(2,j)-zh(n-1,j))*gi(n,j)       !zhxb/G
          zhyb=dy0*(zh(n,j+j3)-zh(n,j-j3))*gi(n,j)       !zhyb/G
          s13(n,j)=strxx(n,j)*zsxb+stryx(n,j)*zsyb
          s23(n,j)=strxy(n,j)*zsxb+stryy(n,j)*zsyb
          h13(n,j)=strxx(n,j)*zhxb+stryx(n,j)*zhyb
          h23(n,j)=strxy(n,j)*zhxb+stryy(n,j)*zhyb
        enddo
c --- ES corner
         zsxb=ibcx*dx0*(1./gi(2,1)-1./gi(n-1,1))*gi(n,1) ! zsxb/(H-zs)
         zsyb=ibcy*dy0*(1./gi(n,1+j3)-1./gi(n,m-j3))*gi(n,1) !zsyb/(H-zs)
         zhxb=ibcx*dx0*(zh(2,1)-zh(n-1,1))*gi(n,1)          !zhxb/G
         zhyb=ibcy*dy0*(zh(n,1+j3)-zh(n,m-j3))*gi(n,1)      !zhyb/G
         s13(n,1)=strxx(n,1)*zsxb+stryx(n,1)*zsyb
         s23(n,1)=strxy(n,1)*zsxb+stryy(n,1)*zsyb
         h13(n,1)=strxx(n,1)*zhxb+stryx(n,1)*zhyb
         h23(n,1)=strxy(n,1)*zhxb+stryy(n,1)*zhyb
c --- EN corner
         zsxb=ibcx*dx0*(1./gi(2,m)-1./gi(n-1,m))*gi(n,m) ! zsxb/(H-zs)
         zsyb=ibcy*dy0*(1./gi(n,1+j3)-1./gi(n,m-j3))*gi(n,m) !zsyb/(H-zs)
         zhxb=ibcx*dx0*(zh(2,m)-zh(n-1,m))*gi(n,m)          !zhxb/G
         zhyb=ibcy*dy0*(zh(n,1+j3)-zh(n,m-j3))*gi(n,m)      !zhyb/G
         s13(n,m)=strxx(n,m)*zsxb+stryx(n,m)*zsyb
         s23(n,m)=strxy(n,m)*zsxb+stryy(n,m)*zsyb
         h13(n,m)=strxx(n,m)*zhxb+stryx(n,m)*zhyb
         h23(n,m)=strxy(n,m)*zhxb+stryy(n,m)*zhyb
c --- WN corner
         zsxb=ibcx*dx0*(1./gi(2,m)-1./gi(n-1,m))*gi(1,m) ! zsxb/(H-zs)
         zsyb=ibcy*dy0*(1./gi(1,1+j3)-1./gi(1,m-j3))*gi(1,m) !zsyb/(H-zs)
         zhxb=ibcx*dx0*(zh(2,m)-zh(n-1,m))*gi(1,m)          !zhxb/G
         zhyb=ibcy*dy0*(zh(1,1+j3)-zh(1,m-j3))*gi(1,m)      !zhyb/G
         s13(1,m)=strxx(1,m)*zsxb+stryx(1,m)*zsyb
         s23(1,m)=strxy(1,m)*zsxb+stryy(1,m)*zsyb
         h13(1,m)=strxx(1,m)*zhxb+stryx(1,m)*zhyb
         h23(1,m)=strxy(1,m)*zhxb+stryy(1,m)*zhyb
c --- WS corner
         zsxb=ibcx*dx0*(1./gi(2,1)-1./gi(n-1,1))*gi(1,1) ! zsxb/(H-zs)
         zsyb=ibcy*dy0*(1./gi(1,1+j3)-1./gi(1,m-j3))*gi(1,1) !zsyb/(H-zs)
         zhxb=ibcx*dx0*(zh(2,1)-zh(n-1,1))*gi(1,1)          !zhxb/G
         zhyb=ibcy*dy0*(zh(1,1+j3)-zh(1,m-j3))*gi(1,1)      !zhyb/G
         s13(1,1)=strxx(1,1)*zsxb+stryx(1,1)*zsyb
         s23(1,1)=strxy(1,1)*zsxb+stryy(1,1)*zsyb
         h13(1,1)=strxx(1,1)*zhxb+stryx(1,1)*zhyb
         h23(1,1)=strxy(1,1)*zhxb+stryy(1,1)*zhyb

      return
      end


      subroutine filstr(a,n1,n2,n3)
      dimension a(n1,n2,n3)
#include "param.nml"
c     include 'param2.nml'
      dimension sx(n),sy(m),sz(l)
      common/grid/ time,dt,dx,dy,dz,dti,dxi,dyi,dzi,zb,igrid,j3
      common/cyclbc/ ibcx,ibcy,ibcz,irlx,irly,irdbc

      do k=1,l
       do j=1,m
        do i=2,n-1
         sx(i)=0.25*(a(i+1,j,k)+2.*a(i,j,k)+a(i-1,j,k))
        enddo
         sx(1)=ibcx*0.25*(a(2,j,k)+2.*a(1,j,k)+a(n-1,j,k))
     .        +(1-ibcx)*a(1,j,k)
         sx(n)=ibcx*sx(1)+(1-ibcx)*a(n,j,k)
        do i=1,n
         a(i,j,k)=sx(i)
        enddo
       enddo
      enddo

      if(j3.eq.1) then
      do k=1,l
        do i=1,n
        do j=1+j3,m-j3
         sy(j)=0.25*(a(i,j+j3,k)+2.*a(i,j,k)+a(i,j-j3,k))
        enddo
         sy(1)=ibcy*0.25*(a(i,1+j3,k)+2.*a(i,1,k)+a(i,m-j3,k))
     .        +(1-ibcy)*a(i,1,k)
         sy(m)=ibcy*sy(1)+(1-ibcy)*a(i,m,k)
        do j=1,m
         a(i,j,k)=sy(j)
        enddo
       enddo
      enddo
      endif
 
      do j=1,m
       do i=1,n
        do k=2,l-1
         sz(k)=0.25*(a(i,j,k+1)+2.*a(i,j,k)+a(i,j,k-1))
        enddo
        if(ibcz.eq.0) then
         sz(1)=a(i,j,1)
         sz(l)=a(i,j,l)
        else
         sz(1)=0.25*(a(i,j,2)+2.*a(i,j,1)+a(i,j,l-1))
         sz(l)=0.25*(a(i,j,2)+2.*a(i,j,l)+a(i,j,l-1))
        endif
        do k=1,l
         a(i,j,k)=sz(k)
         enddo
       enddo
      enddo
 
      return
      end

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine slicesxz(x,y,z,inr,msl)
#include "param.nml"
c      include 'param2.nml'
      real x(n),y(m),z(l)
      dimension jindx(msl)
      dimension u(n,l),
     .          v(n,l),
     .          w(n,l),
     .         ox(n,l),
     .         oy(n,l),
     .         oz(n,l),
     .         th(n,l),
     .          p(n,l),
     .        rho(n,l),
     .        the(n,l),
     .        chm(nch, lch, nspc),
     .         qv(nms, lms),
     .         qc(nms, lms),
     .         qr(nms, lms),
     .        qia(nic, lic),
     .        qib(nic, lic),
     .         zs(n,l),
     .         zh(n,l)
      real*8 timesl
      dimension uxy(n,l),vxy(n,l),fxy(n,l)
      common/gora/ xml0,yml0,amp
      common/grid/ time,dt,dx,dy,dz,dti,dxi,dyi,dzi,zb,igrid,j3
      character(len=1024) :: filename1,filename2,filename3,x1
      character(len=1024) :: filename4,filename5,filename6
      character(len=1024) :: filename7,filename8

      read(inr) jindx
      read(inr) inz
      read(inr) it,timesl
      time=timesl

      print *,'readslicexz:',inr,' iind ',jindx
      print *,'readslicexz:',inr,' it,time ',it,time

      call readslice(zs,n,m,inr,'zs   ')
C     call readslice(zh,n,m,inr,'zh   ')

      do jsl=1,msl
      call readslice(u  ,n,l,inr,'u    ')
      call readslice(v  ,n,l,inr,'v    ')
      call readslice(w  ,n,l,inr,'w    ')
      call readslice(ox ,n,l,inr,'ox   ')
      call readslice(oy ,n,l,inr,'oy   ')
      call readslice(oz ,n,l,inr,'oz   ')
      call readslice(th ,n,l,inr,'th   ')
      call readslice(p  ,n,l,inr,'p    ')

      amp = rmaxval(zs)
      call metryc(x,y,z)

      nclv=20
!      
      write(x1,'(I2.2)') jsl
      filename1 = 'uxz' // x1 //'.dat'
      filename2 = 'vxz' // x1 //'.dat'
      filename3 = 'wxz' // x1 //'.dat'
      filename4 = 'oxxz' // x1 //'.dat'
      filename5 = 'oyxz' // x1 //'.dat'
      filename6 = 'ozxz' // x1 //'.dat'
      filename7 = 'pxz' // x1 //'.dat'
      filename8 = 'thxz' // x1 //'.dat'

!
      open(1,file=filename1,form='unformatted',position='append')
      open(2,file=filename2,form='unformatted',position='append')
      open(3,file=filename3,form='unformatted',position='append')
      open(4,file=filename4,form='unformatted',position='append')
      open(5,file=filename5,form='unformatted',position='append')
      open(6,file=filename6,form='unformatted',position='append')
      open(7,file=filename7,form='unformatted',position='append')
      open(8,file=filename8,form='unformatted',position='append')
      write(1) u,time
      write(2) v,time
      write(3) w,time
      write(4) ox,time
      write(5) oy,time
      write(6) oz,time
      write(7) p,time
      write(8) th,time
      close(1)
      close(2)
      close(3)
      close(4)
      close(5)
      close(6)
      close(7)
      close(8)

      enddo
!      
      return
      end

      subroutine slicesyz(x,y,z,inr,nsl)
#include "param.nml"
c      include 'param2.nml'
      real x(n),y(m),z(l)
      dimension iindx(nsl)
      dimension u(m,l),
     .          v(m,l),
     .          w(m,l),
     .         ox(m,l),
     .         oy(m,l),
     .         oz(m,l),
     .         th(m,l),
     .          p(m,l),
     .        rho(m,l),
     .        the(m,l),
     .        chm(mch, lch, nspc),
     .         qv(mms, lms),
     .         qc(mms, lms),
     .         qr(mms, lms),
     .        qia(mic, lic),
     .        qib(mic, lic),
     .         zs(m,l),
     .         zh(m,l)
      real*8 timesl
      dimension uxy(m,l),vxy(m,l),fxy(m,l)
      common/gora/ xml0,yml0,amp
      common/grid/ time,dt,dx,dy,dz,dti,dxi,dyi,dzi,zb,igrid,j3
      character(len=1024) :: filename1,filename2,filename3,x1
      character(len=1024) :: filename4,filename5,filename6
      character(len=1024) :: filename7,filename8


      read(inr) iindx
      read(inr) inz
      read(inr) it,timesl
      time=timesl

      print *,'readsliceyz:',inr,' kind ',iindx
      print *,'readsliceyz:',inr,' it,time ',it,time

      call readslice(zs,n,m,inr,'zs   ')

      do isl=1,nsl
         call readslice(u  ,m,l,inr,'u    ')
         call readslice(v  ,m,l,inr,'v    ')
         call readslice(w  ,m,l,inr,'w    ')
         call readslice(ox ,m,l,inr,'ox   ')
         call readslice(oy ,m,l,inr,'oy   ')
         call readslice(oz ,m,l,inr,'oz   ')
         call readslice(th ,m,l,inr,'th   ')
         call readslice(p  ,m,l,inr,'p    ')
         amp = rmaxval(zs)
         call metryc(x,y,z)
         
         nclv=20
!      
      write(x1,'(I2.2)') isl
      filename1 = 'uyz' // x1 //'.dat'
      filename2 = 'vyz' // x1 //'.dat'
      filename3 = 'wyz' // x1 //'.dat'
      filename4 = 'oxyz' // x1 //'.dat'
      filename5 = 'oyyz' // x1 //'.dat'
      filename6 = 'ozyz' // x1 //'.dat'
      filename7 = 'pyz' // x1 //'.dat'
      filename8 = 'thyz' // x1 //'.dat'
!
      open(1,file=filename1,form='unformatted',position='append')
      open(2,file=filename2,form='unformatted',position='append')
      open(3,file=filename3,form='unformatted',position='append')
      open(4,file=filename4,form='unformatted',position='append')
      open(5,file=filename5,form='unformatted',position='append')
      open(6,file=filename6,form='unformatted',position='append')
      open(7,file=filename7,form='unformatted',position='append')
      open(8,file=filename8,form='unformatted',position='append')
      write(1) u,time
      write(2) v,time
      write(3) w,time
      write(4) ox,time
      write(5) oy,time
      write(6) oz,time
      write(7) p,time
      write(8) th,time
      close(1)
      close(2)
      close(3)
      close(4)
      close(5)
      close(6)
      close(7)
      close(8)
!
      enddo
!
      return
      end

      subroutine slicesxy(x,y,z,inr,lsl)
#include "param.nml"

      real x(n),y(m),z(l)
      dimension kindx(lsl)
      dimension u(n,m),
     .          v(n,m),
     .          w(n,m),
     .         ox(n,m),
     .         oy(n,m),
     .         oz(n,m),
     .         th(n,m),
     .          p(n,m),
     .        rho(n,m),
     .        the(n,m),
     .        chm(nch, mch, nspc),
     .         qv(nms, mms),
     .         qc(nms, mms),
     .         qr(nms, mms),
     .        qia(nic, mic),
     .        qib(nic, mic),
     .         zs(n,m),
     .         zh(n,m)
      real*8 timesl
      character(len=1024) :: filename1,filename2,filename3,x1
      character(len=1024) :: filename4,filename5,filename6
      character(len=1024) :: filename7,filename8
      dimension uxy(n,m),vxy(n,m),fxy(n,m)
      common/gora/ xml0,yml0,amp
      common/grid/ time,dt,dx,dy,dz,dti,dxi,dyi,dzi,zb,igrid,j3

      read(inr) kindx
      read(inr) inz
      read(inr) it,timesl
      time=timesl

      print *, 'kf=',it
      print *,'readslicexy:',inr,' kind ',kindx
      print *,'readslicexy:',inr,' it,time ',it,time

      call readslice(zs,n,m,inr,'zs   ')
C     call readslice(zh,n,m,inr,'zh   ')

CCCCCCCCCCCCCCCCCCCCCCCC

      do ksl=1,lsl
      call readslice(u  ,n,m,inr,'u    ')
      call readslice(v  ,n,m,inr,'v    ')
      call readslice(w  ,n,m,inr,'w    ')
      call readslice(ox ,n,m,inr,'ox   ')
      call readslice(oy ,n,m,inr,'oy   ')
      call readslice(oz ,n,m,inr,'oz   ')
      call readslice(th ,n,m,inr,'th   ')
      call readslice(p  ,n,m,inr,'p    ')
CCCCCCCCCCCCCCCCCCCCCCCC

      amp = rmaxval(zs)
      call metryc(x,y,z)

CCCCCCCCCCCCCCCCCCCCCCCC

      nclv=20
!
      write(x1,'(I2.2)') ksl
      filename1 = 'uxy' // x1 //'.dat'
      filename2 = 'vxy' // x1 //'.dat'
      filename3 = 'wxy' // x1 //'.dat'
      filename4 = 'oxxy' // x1 //'.dat'
      filename5 = 'oyxy' // x1 //'.dat'
      filename6 = 'ozxy' // x1 //'.dat'
      filename7 = 'pxy' // x1 //'.dat'
      filename8 = 'thxy' // x1 //'.dat'
!
      open(1,file=filename1,form='unformatted',position='append')
      open(2,file=filename2,form='unformatted',position='append')
      open(3,file=filename3,form='unformatted',position='append')
      open(4,file=filename4,form='unformatted',position='append')
      open(5,file=filename5,form='unformatted',position='append')
      open(6,file=filename6,form='unformatted',position='append')
      open(7,file=filename7,form='unformatted',position='append')
      open(8,file=filename8,form='unformatted',position='append')
      write(1) u,time
      write(2) v,time
      write(3) w,time
      write(4) ox,time
      write(5) oy,time
      write(6) oz,time
      write(7) p,time
      write(8) th,time
      close(1)
      close(2)
      close(3)
      close(4)
      close(5)
      close(6)
      close(7)
      close(8)
!
      enddo
!
      return
      end

      subroutine readslice(f,n1,n2,inr,str)
#include "param.nml"
      dimension f(n1,n2)
      character*5 str
      real*8 f8(n1,n2)
!
      read(inr) f8
      do j=1,n2
         do i=1,n1
            f(i,j)=f8(i,j)
         end do
      end do
      call fminmaxsum(f,n1,n2,1,str)

      return
      end
! ccccccccccccccccccccccccccccccccccccccccccccccc

      subroutine wparam ()
      integer :: n,m,l
      integer :: nt,noutp,nplot,nstore
      integer :: lxyz
      integer :: implgw,isphere,icylind,icorio,ideep,ipsinc
      real :: dx00,dy00,dz00,dt00
      call getpars (n,m,l,lxyz,nt,noutp,
     * nplot,nstore,nslice,nxaver,
     * dx00,dy00,dz00,dt00)

      namelist / mylist / nt,n,m,l,lxyz,
     * dx00,dy00,dz00,dt00,nslice,noutp,nplot,nstore,nxaver

      open(8,file='mylist.nml',delim='apostrophe')
      write(unit=8,NML=mylist)
      print *,'writing ... '
      close(8)

      return
      end

      subroutine getpars(i01,i02,i03,i04,i05,i06,i07,i08,i09,i10,
     * p01,p02,p03,p04)
#include "param.nml"
      i01=n
      i02=m
      i03=l
      i04=lxyz
      i05=nt
      i06=noutp
      i07=nplot
      i08=nstore
      i09=nslice
      i10=nxaver
      p01=dx00
      p02=dy00
      p03=dz00
      p04=dt00
      return
      end
