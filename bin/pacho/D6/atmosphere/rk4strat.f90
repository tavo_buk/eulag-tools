!************************************************************
! block data of common parameters for the stratification 
! equations
!************************************************************
module commondat
 implicit none

 integer (kind=4), public :: l 
 integer (kind=4), public :: imr
 real (kind=8),    public :: x0,xmax
 real (kind=8),    public :: tt00, rho00, p00
 real (kind=8),    public :: rg, cp, g
 real (kind=8),    public :: m1, m2, m3    !three layer index param. 
 real (kind=8),    public :: m0            !one layer index param.
 real (kind=8),    public :: m4            ! fourth layer
 real (kind=8),    public :: d, xtac, xtop 
 real (kind=8),    public :: x1, x2, x3    ! four layer poly profile

 !GRID SETTINGS________________________
 !_____________________________________    
 data l             / 132 /  !240
 !data x0,xmax       / -0.2, 1.2 /  ! set1_2D - set4_2D
 !data x0, xmax      / -0.2, 1.8 /  ! set5_2D
 !data x0, xmax      / -0.2, 2.8 /   ! test
 data x0, xmax      / 0.0, 3.0 /

 !INITIAL THERMODYNAMICS_______________
 !_____________________________________       
 data tt00, rho00,p00   / 100., 200., 8000./ ! test
 data rg, cp, g         /  0.4,   1., 33.2/    ! test 
 
 !POLYTROPIC INDEX PROFILE PARAMETERS__
 !_____________________________________ 
 data m1, m2, m3, m4   /   3., 1.50, 1.47, 5.0/ 
 data d, x1, x2, x3    / 0.05,  0.2,  2.3, 2.8/
 data m0               /  1.5/
 
 !FLAGS________________________________
 !_____________________________________
 !
 !   imr = n choose an n-layer poly-
 !           tropic profile.          
 !_____________________________________
 data imr           /  4  /  

 
end module commondat
!************************************************************
!************************************************************
! MAIN ...
!************************************************************
!************************************************************

program main

 use commondat
 implicit none

 call star ()

 stop
end

!************************************************************
!************************************************************

subroutine star ()
 
 use commondat
 implicit none

 integer (kind=4),parameter :: n=3
 integer (kind=4), parameter :: out_unit=20
 integer (kind=8), parameter :: out_unit2=10
 integer (kind=8), parameter :: out_unit3=30
 integer (kind=8), parameter :: out_unit4=40
 integer (kind=4) k
 external star_f
 real (kind=8) x, xaux, dx
 real (kind=8) u0(n),u1(n), uad(n) ! for any field u the order is -> (1,2,3)=(T, rho, P)
 real (kind=8) the0, the1
 real (kind=8) cap
 real (kind=8) ss
 real (kind=8) hs(n), hs0(n)               ! stellar scale heights 
 real (kind=8) h(n), h0(n)                 ! " " " adiabatic
 real (kind=8) mr_out
 logical :: file_exist
 character (len=10) :: foldername
 character (len=12) :: pprof
 character (len=13) :: adiab
 character (len=13) :: strat
 character (len=10) :: scal
 character (len=12) :: adscal
 character (len=1024) :: cmd
 
 dx=(xmax-x0)/(l-1)
 cap=rg/cp
 u0(1)=tt00
 u0(2)=rho00
 u0(3)=rg*rho00*tt00
 the0=u0(1)
 ss=g/(cp*the0)

 !foldername="set5_2D_HR"
 foldername="D5.x0.2"

 !verify existence of the run
 INQUIRE(FILE=foldername, EXIST=file_exist)
 if(file_exist) then
  WRITE(*,*)  "foldername already exist, deleting ... "
  cmd = 'rm -rf '//foldername
  call system(cmd)
 endif 
 
 !creates new working directory
 WRITE(*,*)  "Creating new working folder ..."
 cmd = "mkdir "//foldername
 call system(cmd)
 
 !prints poly profile
 WRITE(*,*)  "printing polytropic index profile ..."
 x=x0
 do k=1,l
  call polyprof(x,mr_out)
  pprof = "polyprof.txt"
  open(unit=out_unit3,file=pprof,action="write", position="append")
  write(out_unit3,'(2x,g14.6,2x,g14.6)') x-xmax, mr_out
  close(out_unit3)
  xaux=x+dx
  x=xaux
 end do

 !computes and prints:
 !-> adiabatic stratification profs.
 !-> polytropic stratification profs.
 !-> stellar scale heights.

 WRITE(*,*)  "calculating stratification profiles and scale heights ..."
 x=x0

 !Computes bottom scales to normalize 
 k=l
 call scaleh(x,n,u0,hs)
 hs0(1)=hs(1)
 hs0(2)=hs(2)
 hs0(3)=hs(3)
 call adscaleh(ss,cap,k,dx,n,h)
 h0(1)=h(1)
 h0(2)=h(2)
 h0(3)=h(3)

 !integrating loop start
 do k=1,l
  call adiabatic(ss,cap,k,dx,n,uad)
  adiab="adiabatic.txt"
  open(unit=out_unit,file=adiab, action="write",position="append")
  write(out_unit,'(2x,g14.6,2x,g14.6,2x,g14.6,2x,g14.6)') x-xmax,uad(1),uad(2),uad(3)
  close(out_unit)
 
  strat="polystrat.txt"
  open(unit=out_unit,file=strat, action="write",position="append")
  write(out_unit,'(2x,g14.6,2x,g14.6,2x,g14.6,2x,g14.6)') x-xmax,u0(1),u0(2),the0
  close(out_unit)
 
  call scaleh(x,n,u0,hs)
  scal="scales.txt"
  open(unit=out_unit2,file=scal, action="write",position="append")
  write(out_unit2,'(2x,g14.6,2x,g14.6,2x,g14.6,2x,g14.6)') x-xmax,hs(1)/hs0(1),hs(2)/hs0(2),hs(3)/hs0(3)
  close(out_unit2)
 
  call adscaleh(ss,cap,k,dx,n,h)
  adscal="adscales.txt"
  open(unit=out_unit2,file=adscal, action="write",position="append")
  write(out_unit2,'(2x,g14.6,2x,g14.6,2x,g14.6,2x,g14.6)') x,h(1)/h0(1),h(2)/h0(2),h(3)/h0(3)
  close(out_unit2) 

  xaux=x+dx
  call rk4vec(x,n,u0,dx,star_f,u1)
  the1=u1(1)*((tt00*rho00)/(u1(1)*u1(2)))**cap
  
  x=xaux
  u0(1:n)=u1(1:n)
  the0=the1
 end do

 !moves files to the working directory 
 cmd ="mv "//pprof//" "//foldername
 call system(cmd)
 cmd ="mv "//adiab//" "//foldername
 call system(cmd)
 cmd ="mv "//strat//" "//foldername
 call system(cmd)
 cmd ="mv "//scal//" "//foldername
 call system(cmd)
 cmd="mv "//adscal//" "//foldername
 call system(cmd)
 
 WRITE(*,*)  "DONE"

 return
end

!************************************************************

subroutine star_f (x,n,u,uprime)

 use commondat
 implicit none

 integer (kind=4) n
 real (kind=8) x
 real (kind=8) u(n)
 real (kind=8) devs(n)
 real (kind=8) uprime(n)
 real (kind=8) mr
 
 call fielddevs(x,n,u,devs)
 uprime(1)=devs(1)
 uprime(2)=devs(2)
 uprime(3)=devs(3)

 return
end

!************************************************************
!Computes the correspoding adiabatic profiles
!************************************************************
subroutine adiabatic (ss,cap,k,dx,n,uad)

 use commondat
 implicit none
 
 integer (kind=4) n, k
 real (kind=8) uad(n)
 real (kind=8) ss,cap,dx

 uad(1)=tt00*(1-ss*(k-1)*dx)                 !isoentropic (T) prof.
 uad(2)=rho00*(1.-ss*(k-1)*dx)**((1./cap)-1) !isoentropic (rho) prof.
 uad(3)=p00*(1-ss*(k-1)*dx)**(1./cap)         !isoentropic (P) prof.

 return
end

!************************************************************
!Computes the correspoding adiabatic scale heights
!************************************************************
subroutine adscaleh (ss,cap,k,dx,n,h)

 use commondat
 implicit none

 integer (kind=4) n, k
 real (kind=8) h(n)
 real (kind=8) ss,cap,dx

 h(1)= (1/ss)*(1-ss*(k-1)*dx)                !adiabatic T scale.
 h(2)=cap/(ss*(1-cap))*(1-ss*(k-1)*dx)       !adiabatic Rho scale.
 h(3)= (cap/ss)*(1-ss*(k-1)*dx)              !adiabatic P scale
 
 return
end

!************************************************************
!Computes the profile of the polytropic index m
!************************************************************
subroutine polyprof(x,mr)

 use commondat
 implicit none

 real (kind=8) x, mr
 
 if(imr==3) then
  mr=m1-(m1-m2)*0.5*(1+erf((x-xtac)/(d)))-(m2-m3)*0.5*(1+erf((x-xtop)/(d)))
 else if(imr==4) then
  mr=m1-(m1-m2)*0.5*(1+erf((x-x1)/(d)))-(m2-m3)*0.5*(1+erf((x-x2)/(d)))-(m3-m4)*0.5*(1+erf((x-x3)/(d)))
 else if(imr==1) then
  mr=m0
 endif

 return
end

!************************************************************
!Computes the (T,rho,p) derivatives for the integration 
!step in star_f
!************************************************************
subroutine fielddevs (x,n,u,fdev)
  
 use commondat
 implicit none
 
 integer (kind=4) n
 real (kind=8) x
 real (kind=8) u(n)
 real (kind=8) fdev(n)
 real (kind=8) mr

 call polyprof(x,mr)

 fdev(1)=-g/((mr+1)*rg)                 !dT/dr 
 fdev(2)=(u(2)/u(1))*(-g/rg-fdev(1))    !d(rho)/dr
 fdev(3)=-u(2)*g                        !dP/dr

 return
end

!************************************************************
! Computes the Temperature, density and pressure scale
! heights of the corresponding ambient stratification 
! profiles.
!************************************************************
subroutine scaleh (x,n,u,scales)
 
 use commondat
 implicit none 

 integer (kind=4) i, n
 real (kind=8) x
 real (kind=8) u(n), devs(n), scales(n)

 call fielddevs(x,n,u,devs)
 do i=1,n
  scales(i)=-1./(devs(i)/u(i))
 end do
 
 return
end 

!************************************************************
! Fourth order Runge-Kutta 
!************************************************************
subroutine rk4vec (t0,m,u0,dt,f,u)

 implicit none

 integer (kind=4) m
 real(kind=8) dt
 external f
 real (kind=8) f0(m),f1(m),f2(m),f3(m)
 real (kind=8) t0,t1,t2,t3
 real (kind=8) u(m),u0(m),u1(m),u2(m),u3(m)

 call f (t0,m,u0,f0)

 t1=t0+dt/2.0D+00
 u1(1:m)=u0(1:m)+dt*f0(1:m)/2.0D+00
 call f (t1,m,u1,f1)
 t2=t0+dt/2.0D+00
 u2(1:m)=u0(1:m)+dt*f1(1:m)/2.0D+00
 call f (t2,m,u2,f2)

 t3=t0+dt
 u3(1:m)=u0(1:m)+dt*f2(1:m)
 call f (t3,m,u3,f3)

 u(1:m)=u0(1:m)+dt*(f0(1:m)+2.0D+00*f1(1:m)+2.0D+00*f2(1:m)+f3(1:m))/6.0D+00
!debugging the rk4 coefficients
! print*, t0,u1(1:m),u2(1:m),u3(1:m) 

 return
end
