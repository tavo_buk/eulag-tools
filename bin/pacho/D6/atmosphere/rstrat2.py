import numpy as np

def Rstrat(folder):
#	folder = raw_input("folder = ")
	ad_file=folder+"adiabatic.txt"
	mr_file=folder+"polyprof.txt"
	poly_file=folder+"polystrat.txt"

	# reading adiabatic.txt
	ad = open(ad_file,"r")
	ad_dat = [line.strip() for line in ad]
	l = len(ad_dat)
	z = np.zeros((l),"float32")
	T_ad = np.zeros((l),"float32")
	rho_ad = np.zeros((l),"float32")
	P_ad = np.zeros((l),"float32") 
	for i in range(l):
		z[i] = float(ad_dat[i].split()[0])
		T_ad[i] = float(ad_dat[i].split()[1])
		rho_ad[i] = float(ad_dat[i].split()[2])
		P_ad[i] = float(ad_dat[i].split()[3])
	ad.close()

	# reading polyprof.txt
	mr = open(mr_file,"r")
	mr_dat = [line.strip() for line in mr]
	mr_prof = np.zeros((l),"float32")
	for i in range(l):
		mr_prof[i] = float(mr_dat[i].split()[1])
	mr.close()

	#reading polystrat.txt
	poly = open(poly_file,"r")
	poly_dat = [line.strip() for line in poly]
	T_poly = np.zeros((l),"float32") 
	rho_poly = np.zeros((l),"float32") 
	Th_poly = np.zeros((l),"float32")
	for i in range(l):
		T_poly[i] = float(poly_dat[i].split()[1])
		rho_poly[i] = float(poly_dat[i].split()[2])
		Th_poly[i] = float(poly_dat[i].split()[3])
	poly.close()

	return l, z, T_ad, rho_ad, P_ad, mr_prof, T_poly, rho_poly, Th_poly 
