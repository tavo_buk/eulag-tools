# In gnuplot run as >> load ''gpscript.gp''
#set terminal postfile       (These commented lines would be used to )
#set output  "d1_plot.ps"    (generate a postscript file.            )
set terminal wxt size 900,700
#set terminal latex
set terminal postscript eps enhanced colour 
set output 'strat.eps'
set termoption dash

unset title

set for [i=1:5] linetype i dt i

set style line 1 lc 3 lw 2
set style line 2 lc 1 lw 2
set style line 3 lc 7 lw 2
set style line 4 lt 3 lc 3 lw 2
set style line 5 lt 3 lc 1 lw 2
set style line 6 lt 3 lc 7 lw 2


set multiplot

set size 0.5,0.5
set origin 0.0,0.0
set logscale y
set xrange [0.0:51.0]
set yrange [1:1100]
set xlabel "z [Mm]"
set ylabel "{/Symbol r} [10^{-4} Kg m^{-3}]"
set key left bottom
#set title "Density (polytropic)" font ",20"
plot "Lz5.0_p/polytropic.dat" u 1:2 w l ls 1 t "L_z = 50 Mm", \
	"Lz4.0_p/polytropic.dat" u 1:2 w l ls 2 t "L_z = 40 Mm", \
	"Lz3.0_p/polytropic.dat" u 1:2 w l ls 3 t "L_z = 30 Mm", \
	"Lz3.0_p/adiabatic.dat" u 1:2 w l ls 6 notitle, \
	"Lz4.0_p/adiabatic.dat" u 1:2 w l ls 5 notitle, \
	"Lz5.0_p/adiabatic.dat" u 1:2 w l ls 4 notitle
#	"D5.x0.2/polystrat.txt" u 1:2 w p pt 13 ps 0.3 t "Lz = 3.0 (D5.x0.2)", \
#	"D5.x0.2/adiabatic.txt" u 1:2 w l ls 1 t "(ad) Lz = 3.0 (D5.x0.2)", \


set size 0.5,0.5
set origin 0.0,0.5
set logscale y
set yrange [0.07:1100]
set ylabel "T [10^3 K]"
#set title "Temperature (polytropic)" font ",20"
plot "Lz3.0_p/polytropic.dat" u 1:3 w l ls 3 t "L_z = 30 Mm", \
	"Lz4.0_p/polytropic.dat" u 1:3 w l ls 2 t "Lz = 40 Mm", \
	"Lz5.0_p/polytropic.dat" u 1:3 w l ls 1 t "Lz = 50 Mm", \
	"Lz3.0_p/adiabatic.dat" u 1:3 w l ls 6 notitle, \
	"Lz4.0_p/adiabatic.dat" u 1:3 w l ls 5 notitle, \
	"Lz5.0_p/adiabatic.dat" u 1:3 w l ls 4 notitle

set object 1 rect from 20.0,500 to 50.0,520 fc rgb "light-gray"

unset logscale y
set size 0.5,0.5
set origin 0.5,0.5
set yrange [450:800]
set key left top
set ylabel "Potential temperature {/Symbol Q}_e [10^3 K]"
#set title "Theta" font ",20"
plot "Lz3.0_P/polytropic.dat" u 1:4 w l ls 3 t "Lz = 30 Mm", \
	"Lz4.0_P/polytropic.dat" u 1:4 w l ls 2 t "Lz = 40 Mm", \
	"Lz5.0_P/polytropic.dat" u 1:4 w l ls 1 t "Lz = 50 Mm"

set origin 0.58, 0.73
set size 0.24, 0.15
set xrange [20.0:50.0]
set yrange [504:515]
set xtics (20,30,40,50)
set ytics  (506,511,515)
unset title
unset ylabel
unset xlabel
plot "Lz3.0_P/polytropic.dat" u 1:4 w l ls 3 notitle, \
	"Lz4.0_P/polytropic.dat" u 1:4 w l ls 2 notitle, \
	"Lz5.0_P/polytropic.dat" u 1:4 w l ls 1 notitle


set object 1 rect from 20.0,1.4 to 50.0,1.6 fc rgb "light-gray"

set size 0.5,0.5
set origin 0.5,0.0 
#set logscale y
set xrange [0.0:51.0]
set xlabel "z [Mm]"
set ylabel "polytropic index m"
set xtics (0,10,20,30,40,50)
set ytics (1.5,3,5)
set yrange [1:5.1]
#set title "polytropic index" font ",20"
plot "Lz3.0_P/polyprof.dat" u 1:2 w l ls 3 t "Lz = 30 Mm", \
	"Lz4.0_p/polyprof.dat" u 1:2 w l ls 2 t "Lz = 40 Mm", \
	"Lz5.0_p/polyprof.dat" u 1:2 w l ls 1 t "Lz = 50 Mm"

set origin 0.57, 0.23
set size 0.24, 0.15
set xrange [20.0:50.0]
set yrange [1.46:1.55]
set xtics (20,30,40,50)
set ytics  (1.46,1.5,1.55)
unset title
unset ylabel
unset xlabel
plot "Lz3.0_P/polyprof.dat" u 1:2 w l ls 3 notitle, \
	"Lz4.0_P/polyprof.dat" u 1:2 w l ls 2 notitle, \
	"Lz5.0_P/polyprof.dat" u 1:2 w l ls 1 notitle

unset object 1

unset multiplot

