import numpy as np
import matplotlib.pyplot as plt
import rstrat2 as rs

folder=raw_input('folder = ')

l, z, T_ad, rho_ad, P_ad, mr, T_poly, rho_poly, Th_poly = rs.Rstrat(folder) 

fig, axes1 = plt.subplots(dpi=90)
axes1.set_xlim([z[0], z[l-1]])
axes1.plot(z,T_ad/T_ad[0],'r--',linewidth=1.5)
axes1.plot(z,T_poly/T_poly[0],'r',linewidth=1.5,label=r'$T_{poly} / T_{bot} $')
axes1.plot(z,rho_ad/rho_ad[0],'b--',linewidth=1.5)
axes1.plot(z,rho_poly/rho_poly[0],'b',linewidth=1.5,label=r"$\rho /\rho_0$")
axes1.set_yscale('log')
axes1.set_xlabel('$z$',fontsize=15) 
axes1.legend(loc=3)

plt.show(fig)
