!******************************************************************
! rk4strat_v2: By Francisco Camacho
!
!********************************************************************
! Domain setup for vertical stratification
!
!   _______________________  z = 0 (top boundary: solar surface)
!  |m4 (stable lyr)        |
!  |-----------------------| z4 = -dst
!  |m3 (unstable lyr)      |
!  |-----------------------| z3 = -dst-dun
!  |                       |
!  |                       |
!  |m2 (adiabatic lyr)     |
!  |-----------------------| z2 = -dst-dun-dad
!  |m1 (stable lyr)        |
!  |-----------------------| z = -Lz (bottom boundary)
!
!
!_____________________________________________________________________
! -- Default values based on simulation D5.x0.2 (sdumont) --
!_____________________________________________________________________
!
! *** GRID
!
! lref(default)     = 132          : reference number of gridpoints 
! dl(default)       = 20           : # points to locate reference state vars
!                                    ahead of z = -Lzref
! Lzref(default)    = 3.0          : length of ref. box
! Lz(default)       = 3.0          : length of the box
! dz                = Lz/lref      : universal vertical z step for grid
! dst(default)      = 0.2          : width of the stable layers
! dun(default)      = 0.3          : width of the unstable layer
! dad               = Lz-2*dst-dun : width of the adiabatic layer
! m1, m4 (default)  = 3.0, 5.0     : politropic indices of bottom/top
!                                    layers, respectively
! m2 (fixed)        = 1.5          : politropic index of adiabatic layer
! m3 (default)      = 1.47         : politropic index of unstable layer
! d (default)       = 0.05         : width of transition for the error 
!                                    funcion 
! *** STATE VARIABLES 
! 
! rg(default)       = 0.4          : Rydberg constant
! cp(default)       = 1.0          : specific heat at constant pressure
! g(default)        = 33.2         : gravity reference value
! Tref(default)     = 100.0        : temperature value at ref. depth
! Dref(default)     = 200.0        : density value at ref. depth
! Pref(default)     = 8000.0       : pressure value at ref depth
! Thref             = Tref         : Potential Temp. at ref. depth      
! 
! *** IMPORTANT NOTES:
!     three new reference values for adiabatic state must be 
!     choosen (problem with diferent slopes at both states)
!     -> say, (Ttop, Dtop, Ptop), as backward integration is
!        more convenient, we choose equations for the adiabatic
!        state that depend on the values at the top surface.
!        (slightly different from the eqs. in rk4strat.f90)
!        STATUS: FAILED ... discard method ....
!
!     -> now we choose bottom values to be fixed for all box
!        sizes. (T,rho,P) by default to be the D5.x0.2 reference 
!        values.
!
! ***** COMMENTS ON THIS VERSION *****:
!
!                             > this release allows special kind of
!                               integration to create boxes with diff-
!                               erent vertical sizes but the same stra-
!                               tification at the top layers.
!
!_____________________________________________________________________


!********************************************************************
! block data of common parameters for the stratification 
! equations
!********************************************************************
module comdat
        implicit none
        integer (kind=8), public :: lref, l, dl
        real (kind=8), public    :: Lzref, Lz, dst, dun, dz
        real (kind=8), public    :: m1, m2, m3, m4, d
        real (kind=8), public    :: rg, cp, g 
        real (kind=8), public    :: Tref, Dref, Pref

 data lref, dl                / 132, 20 /
 data Lzref, Lz, dst, dun     / 3.0, 5.0, 0.2, 0.5 /
 data m1, m2, m3, m4, d       / 3.0, 1.5, 1.47, 5.0, 0.05 /
 data rg, cp, g               / 0.4, 1., 33.2 /
 data Tref, Dref              / 88.1607, 154.874 / 

contains
 subroutine cts()
  Pref = rg*Tref*Dref
  dz   = Lzref/(lref-1)
  l    = int(1+Lz/dz)
 end subroutine cts

end module comdat


program main
 use comdat
 implicit none
 call cts()
 call strat ()
 stop
end

!********************************************************************
subroutine strat ()
 use comdat 
 implicit none

 integer (kind=4) j, k, pos
 integer (kind=4), parameter :: n=3
 integer (kind=4), parameter :: out_unit=1
 real (kind=8) cap, ss
 real (kind=8) dad, Thref
 external star_f
 real (kind=8) u(n), uaux(n)
 real (kind=8) r(l), rr(l), T(l), Rho(l), P(l), Th(l)
 real (kind=8) Tad(l), Rhoad(l), Pa(l)
 real (kind=8) z, zaux, mr_out
 character (len=10) :: foldername
 character (len=1024) :: cmd
 character (len=15) :: file1, file2, file3
 logical :: file_exist
 real (kind=8) l0, T0, D0  ! physical constants  

 l0 = 10.0            ! [Mm]
 T0 = 5.8             ! x 10^3 [K]
 D0 = 2.5             ! x 10^(-4)[Kg/m^3]

 write(*,*) dz
 write(*,*) l

 foldername = "Lz5.0_p"
 inquire(file=foldername,exist=file_exist)
 if(file_exist) then
  write(*,*) "foldername already exist, overwritting ... "
  cmd = 'rm -rf '//foldername
  call system(cmd) 
 endif
  write(*,*) "Creating new working folder ..."
  cmd = "mkdir "//foldername
 call system(cmd)

 WRITE(*,*)  "printing polytropic index profile ..."
 z = -Lz
 do k=1,l
  call polyprof(z,mr_out)
  file1 = "polyprof.dat"
  rr(k)=z
  open(unit=out_unit,file=file1,action="write", position="append")
! write(out_unit,'(2x,g14.6,2x,g14.6)') rr(k), mr_out
  write(out_unit,'(2x,g14.6,2x,g14.6)') (rr(k)+Lz)*l0, mr_out
  close(out_unit)
  zaux=z+dz
  z=zaux
 end do


 WRITE(*,*)  "calculating polytropic stratification ..."

 cap = rg/cp
 u(1) = Tref
 u(2) = Dref
 u(3) = Pref
 Thref = u(1) 

 if(l>=lref) then
  z        = -Lzref+dl*dz
  pos      = l-lref+1+dl
  r(pos)   = z
  T(pos)   = u(1)
  Rho(pos) = u(2)
  P(pos)   = u(3)
  Th(pos)  = Thref
  write (*,*) "box larger than reference adiabatic depth: performing backward integration ... "
  
  do k=1,pos-1
   call rk4vec(z,n,u,-dz,star_f,uaux)
   zaux            = z - dz
   r(pos-k)        = zaux
   T(pos-k)        = uaux(1)
   Rho(pos-k)      = uaux(2)
   P(pos-k)        = uaux(3) 
   Th(pos-k)       = uaux(1)*((Tref*Dref)/(uaux(1)*uaux(2)))**cap 
   u(1:n)          = uaux(1:n)
   z               = zaux
  enddo
  
  write(*,*)  "performing forward integration ... "
  z           = -Lzref+dl*dz
  u(1)        = Tref
  u(2)        = Dref
  u(3)        = Pref
  Thref       = u(1)
  do k=1,lref-1-dl
   call rk4vec(z,n,u,dz,star_f,uaux)
   zaux            = z + dz
   r(pos+k)        = zaux
   T(pos+k)        = uaux(1)
   Rho(pos+k)      = uaux(2)
   P(pos+k)        = uaux(3)
   Th(pos+k)       = uaux(1)*((Tref*Dref)/(uaux(1)*uaux(2)))**cap 
   u(1:n)          = uaux(1:n)
   z               = zaux
  enddo
   
 endif
  
 file2 = "polytropic.dat"
 open(unit=out_unit, file=file2, action="write", position="append")
 do k=1,l 
! write(out_unit,'(2x,g14.6,2x,g14.6,2x,g14.6,2x,g14.6)') rr(k),T(k),Rho(k),Th(k)
  write(out_unit,'(2x,g14.6,2x,g14.6,2x,g14.6,2x,g14.6)') (rr(k)+Lz)*l0,T(k)*T0,Rho(k)*D0,Th(k)*T0
 enddo
 close(out_unit) 
 
 write(*,*)  "calculating adiabatic stratification ..."

 u(1)        = 100.  ! ref. vals for adiabatic strat. from reference vals at lref-dl pos. 
 u(2)        = 200.
 u(3)        = 8000.
 ss          = g/(cp*u(1))
 pos         = l-lref+1

 if(l==lref) then
  do k=1,l
   call adiabatic(u,ss,cap,k,n,uaux)
   Tad(k)   = uaux(1)
   Rhoad(k) = uaux(2)
   Pa(k)    = uaux(3)
  enddo
 endif
 if(l>lref) then
  do k=1,lref
   call adiabatic(u,ss,cap,k,n,uaux)
   Tad(pos+k-1)   = uaux(1)
   Rhoad(pos+k-1) = uaux(2)
   Pa(pos+k-1)    = uaux(3)
  enddo
  do k=1,pos-1
   call adiabatic(u,ss,cap,-k,n,uaux)
   Tad(pos-k)   = uaux(1)
   Rhoad(pos-k) = uaux(2)
   Pa(pos-k)    = uaux(3)
  enddo
 endif 

 file3="adiabatic.dat"
 open(unit=out_unit,file=file3, action="write",position="append")
 do k=1,l
! write(out_unit,'(g14.6,2x,g14.6,2x,g14.6,2x,g14.6)') rr(k),Tad(k),Rhoad(k),Pa(k)
  write(out_unit,'(g14.6,2x,g14.6,2x,g14.6,2x,g14.6)') (rr(k)+Lz)*l0,Tad(k)*T0,Rhoad(k)*D0,Pa(k)*T0*D0
 enddo
 close(out_unit)
 
 !moves files to the working directory 
 cmd ="mv "//file1//" "//foldername
 call system(cmd)
 cmd ="mv "//file2//" "//foldername
 call system(cmd)
 cmd ="mv "//file3//" "//foldername
 call system(cmd)

 return
end

!************************************************************
subroutine star_f (z,n,u,uprime)

 use comdat
 implicit none

 integer (kind=4) n
 real (kind=8) z
 real (kind=8) u(n)
 real (kind=8) devs(n)
 real (kind=8) uprime(n)
 
 call fielddevs(z,n,u,devs)
 uprime(1)=devs(1)
 uprime(2)=devs(2)
 uprime(3)=devs(3)

 return
end

!************************************************************
!Computes the correspoding adiabatic profiles
!************************************************************
subroutine adiabatic (utop,ss,cap,k,n,uad)

 use comdat
 implicit none
 
 integer (kind=4) n, k
 real (kind=8) uad(n), utop(n)
 real (kind=8) ss,cap

 uad(1)=utop(1)*(1-ss*(k-1)*dz)                 !isoentropic (T) prof.
 uad(2)=utop(2)*(1.-ss*(k-1)*dz)**((1./cap)-1) !isoentropic (rho) prof.
 uad(3)=utop(3)*(1-ss*(k-1)*dz)**(1./cap)         !isoentropic (P) prof.

 return
end

!************************************************************
!Computes the profile of the polytropic index mr(z) at point z
!************************************************************
subroutine polyprof(z,mr)
 use comdat
 implicit none 
 real (kind=8) dad
 real (kind=8) z4, z3, z2
 real (kind=8) z, mr
 
 dad = Lz-2*dst-dun
 z4  = -dst 
 z3  = z4-dun
 z2  = z3-dad

 mr=m1-(m1-m2)*0.5*(1+erf((z-z2)/(d)))-(m2-m3)*0.5*(1+erf((z-z3)/(d)))-(m3-m4)*0.5*(1+erf((z-z4)/(d)))
 return
end

!************************************************************
!Computes the (T,rho,p) derivatives for the integration 
!step in star_f
!************************************************************
subroutine fielddevs (z,n,u,fdev)

 use comdat        
 implicit none

 integer (kind=4) n
 real (kind=8) z
 real (kind=8) u(n)
 real (kind=8) fdev(n)
 real (kind=8) mr

 call polyprof(z,mr)

 fdev(1)=-g/((mr+1)*rg)                 !dT/dr 
 fdev(2)=(u(2)/u(1))*(-g/rg-fdev(1))    !d(rho)/dr
 fdev(3)=-u(2)*g                        !dP/dr

 return
end


!************************************************************
! Fourth order Runge-Kutta 
!************************************************************
subroutine rk4vec (z0,m,u0,dz,f,u)

 implicit none

 integer (kind=4) m
 real(kind=8) dz
 external f
 real (kind=8) f0(m),f1(m),f2(m),f3(m)
 real (kind=8) z0,z1,z2,z3
 real (kind=8) u(m),u0(m),u1(m),u2(m),u3(m)

 call f (z0,m,u0,f0)

 z1=z0+dz/2.0D+00
 u1(1:m)=u0(1:m)+dz*f0(1:m)/2.0D+00
 call f (z1,m,u1,f1)
 z2=z0+dz/2.0D+00
 u2(1:m)=u0(1:m)+dz*f1(1:m)/2.0D+00
 call f (z2,m,u2,f2)
 z3=z0+dz
 u3(1:m)=u0(1:m)+dz*f2(1:m)
 call f (z3,m,u3,f3)
 u(1:m)=u0(1:m)+dz*(f0(1:m)+2.0D+00*f1(1:m)+2.0D+00*f2(1:m)+f3(1:m))/6.0D+00
!debugging the rk4 coefficients
! print*, t0,u1(1:m),u2(1:m),u3(1:m) 

 return
end
