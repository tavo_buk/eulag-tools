import rstrat2 as rs
import matplotlib.pyplot as plt

l1, z1, T_a1, r_a1, P_a1, mr_1, T_1, r_p1, Th_p1 = rs.Rstrat("D5.m3.1/")
l2, z2, T_a2, r_a2, P_a2, mr_2, T_2, r_p2, Th_p2 = rs.Rstrat("D5.m3.2/")
l3, z3, T_a3, r_a3, P_a3, mr_3, T_3, r_p3, Th_p3 = rs.Rstrat("D5.m4.1/")
l4, z4, T_a4, r_a4, P_a4, mr_4, T_4, r_p4, Th_p4 = rs.Rstrat("D5.m4.2/")
l5, z5, T_a5, r_a5, P_a5, mr_5, T_5, r_p5, Th_p5 = rs.Rstrat("D5.x0.1/")
l6, z6, T_a6, r_a6, P_a6, mr_6, T_6, r_p6, Th_p6 = rs.Rstrat("D5.x0.2/")

imr=0
if imr == 1:
	fig, ax1 = plt.subplots(dpi=90)
	ax1.set_xlim(z1[0], z1[l1-1])
	ax1.plot(z1,mr_1,'o',markersize=4,label='D5.m3.1')
	ax1.plot(z2,mr_2,label='D5.m3.2', linewidth=2)
	ax1.plot(z3,mr_3,label='D5.m4.1', linewidth=2)
	ax1.plot(z4,mr_4,label='D5.m4.2', linewidth=2)
	ax1.plot(z5,mr_5,label='D5.x0.1', linewidth=2)
	ax1.plot(z6,mr_6,label='D5.x0.2', linewidth=2)
	ax1.legend(loc=2)
	ax1.set_xlabel('$z$', fontsize=16)
	ax1.set_ylabel('$m(z)$', fontsize=16)

	ax2=fig.add_axes([0.38,0.58,0.45,0.3])
	ax2.set_xlim(1.8, 2.6)
	ax2.set_ylim(1.46, 1.51)
	ax2.plot(z1,mr_1,'o',markersize=4,label='D5.m3.1')
	ax2.plot(z2,mr_2,label='D5.m3.2', linewidth=2)
	ax2.plot(z3,mr_3,label='D5.m4.1', linewidth=2)
	ax2.plot(z4,mr_4,label='D5.m4.2', linewidth=2)
	ax2.plot(z5,mr_5,label='D5.x0.1', linewidth=2)
	ax2.plot(z6,mr_6,label='D5.x0.2', linewidth=2)

	plt.show(fig)

irp = 0
if irp == 1:
	fig2, ax3 = plt.subplots(dpi=90)
	ax3.set_xlim(z1[0], z1[l1-1])
	ax3.set_ylim(1, 2e2)
	ax3.plot(z1,r_p1,'o-',markersize=4,label='D5.m3.1')
	ax3.plot(z2,r_p2,'x-',markersize=4,label='D5.m3.2')
	ax3.plot(z3,r_p3,'v-',markersize=4,label='D5.m4.1')
	ax3.plot(z4,r_p4,'>-',markersize=4,label='D5.m4.2')
	ax3.plot(z5,r_p5,'<-',markersize=4,label='D5.x0.1')
	ax3.plot(z6,r_p6,'p-',markersize=4,label='D5.x0.2')
	ax3.legend(loc=3)
	ax3.set_xlabel('$z$', fontsize=16)
	ax3.set_ylabel('polytropic density', fontsize=16)
	ax3.set_yscale('log')

	plt.show(fig2)

ithp = 1
if ithp == 1:
	fig3, ax4 = plt.subplots(dpi=90)
	ax4.set_xlim(z1[0], z1[l1-1])
#	ax4.set_ylim(1, 2e2)
	ax4.plot(z1,Th_p1,'o',markersize=4,label='D5.m3.1')
	ax4.plot(z2,Th_p2,label='D5.m3.2')
	ax4.plot(z3,Th_p3,label='D5.m4.1')
	ax4.plot(z4,Th_p4,label='D5.m4.2')
	ax4.plot(z5,Th_p5,label='D5.x0.1')
	ax4.plot(z6,Th_p6,label='D5.x0.2')
	ax4.legend(loc=2)
	ax4.set_xlabel('$z$', fontsize=16)
	ax4.set_ylabel('potential temperature', fontsize=16)
#	ax4.set_yscale('log')

	ax5=fig3.add_axes([0.38,0.58,0.45,0.3])
	ax5.set_xlim(1.8, 2.6)
	ax5.set_ylim(101.5, 103.5)
	ax5.plot(z1,Th_p1,'o',markersize=4,label='D5.m3.1')
	ax5.plot(z2,Th_p2,label='D5.m3.2', linewidth=2)
	ax5.plot(z3,Th_p3,label='D5.m4.1', linewidth=2)
	ax5.plot(z4,Th_p4,label='D5.m4.2', linewidth=2)
	ax5.plot(z5,Th_p5,label='D5.x0.1', linewidth=2)
	ax5.plot(z6,Th_p6,label='D5.x0.2', linewidth=2)

	plt.show(fig3)
