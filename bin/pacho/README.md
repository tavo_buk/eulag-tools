# Eulag versions for cartesian simulations of local magneto-convection
* Folder D6/ : Contains the definitive version of simulations developing magnetic structures (includes source fortran code and atmosphere fortran90 code for adiabatic and polytropic profiles for different box depths). Scripts with Python and bash_gnuplot can be found to plot the atmospheric profiles.
