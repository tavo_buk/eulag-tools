#include "define.h"
!.....7..0.........0.........0.........0.........0.........0.........012
      PROGRAM READ_BINARY           
!.....7..0.........0.........0.........0.........0.........0.........012

#include "param.nml"
#include "msg.inc"
      dimension u(1-ih:np+ih,1-ih:mp+ih,l),
     .          v(1-ih:np+ih,1-ih:mp+ih,l),
     .          w(1-ih:np+ih,1-ih:mp+ih,l),

      integer :: iplane
      character(len=5) :: sett
      character(len=5) :: true='y'
      character(len=5) :: false='n'
      logical :: file_exists

      common/grid/ time,dt,dx,dy,dz,dti,dxi,dyi,dzi,zb,igrid,j3

c igrid=0 for A grid (default); igrid=1 for B grid (special option)
      data igrid/0/
      common/metric0/ xcr(n,m),ycr(n,m)
      common/cyclbc/ ibcx,ibcy,ibcz,irlx,irly,irdbc

create model variables
      real x(n),y(m),z(l)

      print *,'START READING BINARY TAPE'
      do kf=0,nfil-1
         call ioread(u(1-ih,1-ih,1), v(1-ih,1-ih,1), 
     .        w(1-ih,1-ih,1),kf)
      enddo

      stop 'main'
      end   
! cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      subroutine ioread(u,v,w,icomm,kf)

#include "param.nml"
#include "msg.inc"

      dimension u(1-ih:np+ih,1-ih:mp+ih,l),
     .          v(1-ih:np+ih,1-ih:mp+ih,l),
     .          w(1-ih:np+ih,1-ih:mp+ih,l)

      call ioread0(u,v,w,icomm)

      return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine ioread0(u,v,w,icomm)
c
c     This subroutine reads the data from the history file.
c
#include "param.nml"
#include "msg.inc"
#include "tempr.def" 

      dimension u(1-ih:np+ih,1-ih:mp+ih,l),
     .          v(1-ih:np+ih,1-ih:mp+ih,l),
     .          w(1-ih:np+ih,1-ih:mp+ih,l),

#if (PARALLEL > 0)
      common/blocktemp/tmparray(np,mp,l),tempval
#endif
      inr=19    ! read from fort.10
      ifullarr=1
      call readit0(u,temp,ifullarr,icomm,inr)
      call readit0(v,temp,ifullarr,icomm,inr)
      call readit0(w,temp,ifullarr,icomm,inr)

      return
      end

      subroutine readit0(datarr,temp,ifullarr,icomm,inr)
c
c     This subroutine reads the data from the history file.
c
#include "param.nml"
#include "msg.inc"
#include "tempr.def" 

#if (PARALLEL > 0)
#include "msg.lnk"
#include "msg.lnp"
      common/blocktemp/tmparray(np,mp,l),tempval
#endif

      dimension datarr(1-ih:np+ih,1-ih:mp+ih,l)

      call readit0tape(temp,fval,ifullarr,inr,n,m)

      if (icomm.eq.1) then
      if (ifullarr.eq.1) then

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     transfer processor 0 data from big array to local array
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

         do k=1,l
            do j=1,mp
               do i=1,np
                  datarr(i,j,k)=temp(i,j,k)
               end do
            end do
         end do

#if (PARALLEL > 0)
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     loop over all other processors, sending them their data
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         nmlp=np*mp*l
         do iproc=1,(nprocx*nprocy - 1)

            npos1 = mod((iproc+nprocx), nprocx) + 1
            mpos1 = iproc/nprocx + 1
            do k=1,l
               do j=1,mp
                  do i=1,np
                     tmparray(i,j,k) = 
     .                   temp(((npos1-1)*np + i), ((mpos1-1)*mp + j), k)
                  end do
               end do
            end do

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
#if (PARALLEL == 1)
#if (PVM_IO == 0)
#if (SGI_O2K == 1)
            call shmem_put32(tmparray,tmparray,nmlp,iproc)
#endif
#if (SGI_O2K == 2)
            call shmem_put64(tmparray,tmparray,nmlp,iproc)
#endif
#if (CRAYT3D == 1 || CRAYT3E == 1)
            call shmem_put(tmparray,tmparray,nmlp,iproc)
#endif
#else
            call pvmfinitsend(PVMRAW, ierr)
            if (ierr.lt.0) then
               write(*,*)'***pvm1 error in readit0'
               stop
            end if
            call pvmfpack(REAL8, tmparray, nmlp, 1, ierr)
            if (ierr.lt.0) then
               write(*,*)'***pvm2 error in readit0'
               stop
            end if
            call pvmfsend(iproc,1,ierr)
            if (ierr.lt.0) then
               write(*,*)'***pvm3 error in readit0'
               stop
            end if
#endif
#endif
#if (PARALLEL == 2)
            call MPI_Send(tmparray, nmlp, DC_TYPE, iproc, 98,
     .                     MPI_COMM_WORLD, ierr)
            if (ierr.lt.0) then
               write(*,*)'***mpi error in readit0'
	       stop
            end if
#endif
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         
         end do

#endif /* (PARALLEL > 0) */
ccccccccccccccccccccccccccccccccccccccccc
c     read full array from tape file done
ccccccccccccccccccccccccccccccccccccccccc

      else   !ifullarr = 0 - degenerate array; read one value

         tempval=fval

ccccccccccccccccccccccccccccc
c     end of read one value
ccccccccccccccccccccccccccccc

      end if    !ifullarr
      end if    !icomm = 1

      return
      end

C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#endif /* IORFLAG == 0 */
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if (IORFLAG == 1) /* Read arrays written sequentially, distribute them to PEs */
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine readit0(datarr,temp,ifullarr,icomm,inr)
c
c     This subroutine reads the data from the history file.
c
#include "param.nml"
#include "msg.inc"
#include "tempr.def" 

      dimension datarr(1-ih:np+ih,1-ih:mp+ih,l)

#if (PARALLEL > 0)
#include "msg.lnk"
#include "msg.lnp"
      common/blocktemp/tmparray(np,mp,l),tempval
      nmlp=np*mp*l
#endif


      if (ifullarr.eq.1) then

      do iproc=0,(nprcxa*nprcya - 1)
      call readit0tape(temp,fval,ifullarr,inr,npa,mpa)
       if (icomm.eq.1) then

#if (PARALLEL == 0)
         npos1 = mod((iproc+nprcxa), nprcxa) + 1
         mpos1 = iproc/nprcxa + 1
         print *,"GGG here:", l,mpa,npa 
         do k=1,l
          do j=1,mpa
           do i=1,npa
            datarr(((npos1-1)*npa+i),((mpos1-1)*mpa+j),k)=temp(i,j,k)
           end do
          end do
         end do
#else
        if (iproc.eq.0) then
         do k=1,l
            do j=1,mpa
               do i=1,npa
                  datarr(i,j,k)=temp(i,j,k)
               end do
            end do
         end do
        else
cccccccccccccccccccccccccccccccccccccccccccccc
c     send small arrays for other processors
cccccccccccccccccccccccccccccccccccccccccccccc

            do k=1,l
               do j=1,mp
                  do i=1,np
                     tmparray(i,j,k) = temp(i,j,k) 
                  end do
               end do
            end do

#if (PARALLEL == 1)
#if (PVM_IO == 0)
#if (SGI_O2K == 1)
            call shmem_put32(tmparray,tmparray,nmlp,iproc)
#endif
#if (SGI_O2K == 2)
            call shmem_put64(tmparray,tmparray,nmlp,iproc)
#endif
#if (CRAYT3D == 1 || CRAYT3E == 1)
            call shmem_put(tmparray,tmparray,nmlp,iproc)
#endif
#else
            call pvmfinitsend(PVMRAW, ierr)
            if (ierr.lt.0) then
               write(*,*)'***pvm1 error in readit0'
               stop
            end if
            call pvmfpack(REAL8, tmparray, nmlp, 1, ierr)
            if (ierr.lt.0) then
               write(*,*)'***pvm2 error in readit0'
               stop
            end if
            call pvmfsend(iproc,1,ierr)
            if (ierr.lt.0) then
               write(*,*)'***pvm3 error in readit0'
               stop
            end if
#endif
#endif
#if (PARALLEL == 2)
            call MPI_Send(tmparray, nmlp, DC_TYPE, iproc, 98,
     .                     MPI_COMM_WORLD, ierr)
            if (ierr.lt.0) then
               write(*,*)'***mpi error in readit0'
	       stop
            end if
#endif
cccccccccccccccccccccccccccccccccccccccccccccc
c     send small arrays for other processors
cccccccccccccccccccccccccccccccccccccccccccccc
           end if  ! iproc

#endif /* (PARALLEL > 0) */

          end if  ! icomm
         end do  ! iproc

      else   !ifullarr = 0 ! degenerate array; read one value

      call readit0tape(temp,fval,ifullarr,inr,np,mp)

        if (icomm.eq.1) then

         tempval=fval

#if (PARALLEL > 0)
ccccccccccccccccccccccccccccccccccccccccccccccc
c     send data from pe 0 to other processors
ccccccccccccccccccccccccccccccccccccccccccccccc
         nmlp=1
#if (PARALLEL == 1)
#if (PVM_IO == 0)
         call shmem_barrier_all() ! tempval is available in common block 
#else
         call pvmfinitsend(PVMRAW, ierr)
         if (ierr.lt.0) then
            write(*,*)'***pvm1 error in readit0'
            stop
         end if
         call pvmfpack(REAL8, tempval, nmlp, 1, ierr)
         if (ierr.lt.0) then
            write(*,*)'***pvm2 error in readit0'
            stop
         end if
         call pvmfsend(iproc,1,ierr)
         if (ierr.lt.0) then
            write(*,*)'***pvm3 error in readit0'
            stop
         end if
#endif
#endif
#if (PARALLEL == 2)
         call MPI_Bcast(tempval,nmlp,DC_TYPE,0,MPI_COMM_WORLD,ierr)
         if (ierr.lt.0) then
            write(*,*)'***mpi error in readit0'
            stop
         end if
#endif
#endif /* (PARALLEL > 0) */
ccccccccccccccccccccccccccccc
c     end of read one value
ccccccccccccccccccccccccccccc

      end if    !icomm = 1
      end if    !ifullarr

      return
      end

C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#endif /* IORFLAG == 1 */
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      subroutine readit0tape(temp,fval,ifullarr,inr,n1,m1)
c
c     This subroutine reads the data from the history file by PE0
c
#include "param.nml"
#include "msg.inc"
#include "incl/common.hpcray"

#if (IORTAPE == 3)
      real*8 temp(n1,m1,l)
      real*8 fval
#endif
#if (IORTAPE == 2)
      real*4 temp(n1,m1,l)
      real*4 fval
#endif
#if (IORTAPE == 1)
      dimension temp(n1,m1,l)
      real fval
#endif


      if (ifullarr.eq.1) then
cccccccccccccccccccccccccccccccccccccc
c     read full array from tape file
cccccccccccccccccccccccccccccccccccccc
      nml0=n1*m1*l

#if (LNX > 0 || GGG > 0 || WORKS > 0 || IBM > 0 || FUJI_VPP > 0)
c read native Fortran data
      read(inr)temp
#endif

ccccccccccccccccccccccccccccccccccccccccc
c     read full array from tape file done
ccccccccccccccccccccccccccccccccccccccccc
      else   !ifullarr = 0
cccccccccccccccccccccccccccccccccccccccc
c     read one value from tape file
cccccccccccccccccccccccccccccccccccccccc
      nml0=1


#if (LNX > 0 || GGG > 0 || WORKS > 0 || IBM > 0 || FUJI_VPP > 0 || PLE > 0)
c read native Fortran data
      read(inr)fval
#endif

cccccccccccccccccccccccccccccccccccccccc
c     read one value from tape file done
cccccccccccccccccccccccccccccccccccccccc
      endif

      return
      end

