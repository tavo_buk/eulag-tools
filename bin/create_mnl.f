#include "define.h"

      PROGRAM create_nml           
      integer :: n,m,l
      integer :: nt,noutp,nplot,nstore
      integer :: lxyz
      integer :: implgw,isphere,icylind,icorio,ideep,ipsinc
      real :: dx00,dy00,dz00,dt00

      namelist / mylist / nt,n,m,l,lxyz,
     * dx00,dy00,dz00,dt00,nslice,noutp,nplot,nstore,nxaver

      call getpars (n,m,l,lxyz,nt,noutp,
     * nplot,nstore,nslice,nxaver,
     * dx00,dy00,dz00,dt00)


      open(8,file='mylist.nml',delim='apostrophe')
      write(unit=8,NML=mylist)
      print *,'writing ... '
      close(8)
      end program create_nml

      subroutine getpars(i01,i02,i03,i04,i05,i06,i07,i08,i09,i10,
     * p01,p02,p03,p04)
#include "param.nml"
      i01=n
      i02=m
      i03=l
      i04=lxyz
      i05=nt
      i06=noutp
      i07=nplot
      i08=nstore
      i09=nslice
      i10=nxaver
      p01=dx00
      p02=dy00
      p03=dz00
      p04=dt00
      return
      end subroutine getpars

