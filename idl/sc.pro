if !d.name eq 'PS' then begin
    device,xsize=18,ysize=12,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
end

!p.charsize=1.5

dd=fltarr(2,7)
;openr,1,'scaling.dat' 
openr,1,'scaling2.dat' 
readf,1,dd
free_lun,1

np=dd(0,*)
t=dd(1,*)
title='Scaling EULAG-code'
ins= '1024x512x383!6'
plot,np,t/10.,xtitle='!8N!D!6procs!N', ytitle='!8t!D!6per time step !N!6(sec)',$
	/xlog,/ylog,yr=[10,200],ystyle=1,title=title
oplot,np,t/10,psym=4,symsize=2
xyouts,1e3,100,ins

end
