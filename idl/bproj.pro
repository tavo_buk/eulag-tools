if !d.name eq 'PS' then begin
    device,xsize=18,ysize=12,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
end

xcoord=fltarr(nphi,nth)
ycoord=fltarr(nphi,nth)

@eu_read_bcprofl
irlev=16
it=68
for it=0,60 do begin
psi=mpsi(theta)
bphi=reform(bx[*,*,irlev,it])
bth=reform(by[*,*,irlev,it])
uz=reform(w[*,*,irlev,it])
for i=0,nphi-1 do begin
  for j=0,nth-1 do begin
    xcoord[i,j] = r[irlev]*2.*sqrt(2.)*cos(psi[j])*(phi[i]-!pi)/!pi
    ycoord[i,j] = r[irlev]*sqrt(2)*sin(psi[j])
  end
end

;plot_field,bphi,bth,n=500,length=1
contour,smooth(uz,2),xcoord,ycoord,/fi,nl=64,/iso
partvelvec,bphi,bth,xcoord,ycoord,fraction=0.5,length=.03,/over
wait,0.1
endfor

!p.multi=0
end
