restore,'grid.sav'

default, ivtk, 0
default, ivdf, 0

nr=(size(r))[1]
nth=(size(th))[1]
nphi=(size(ph))[1]
file='fort.11'
n=130D
uu=fltarr(nphi,nth,nr)
vv=fltarr(nphi,nth,nr)
ww=fltarr(nphi,nth,nr)
tt=fltarr(nphi,nth,nr)
pp=fltarr(nphi,nth,nr)
bxx=fltarr(nphi,nth,nr)
byy=fltarr(nphi,nth,nr)
bzz=fltarr(nphi,nth,nr)

wnf=fltarr(nphi,nth,nr,n)
w=fltarr(nphi,nth,nr,n)
bxnf=fltarr(nphi,nth,nr,n)
bx=fltarr(nphi,nth,nr,n)
ns=0L

openr,lun,file,/get_lun,/f77_unformatted
for ns=0,n-1 do begin 
  ; while (not eof(lun)) do begin
  readu,lun,uu
  readu,lun,vv
  readu,lun,ww
  readu,lun,tt
  readu,lun,pp
  readu,lun,bxx
  readu,lun,byy
  readu,lun,bzz

  wnf[*,*,*,ns] = ww
  bxnf[*,*,*,ns]= bxx
  print,'reading data from file: ',file, ns
  ;   ns++
  ; endwhile
endfor
free_lun,lun

;filtering
 
for k=1, nr-2 do begin
 w[*,*,k,*]  =0.25*(wnf[*,*,k-1,*]+2.*wnf[*,*,k,*]+wnf[*,*,k+1,*])
 bx[*,*,k,*]=0.25*(bxnf[*,*,k-1,*]+2.*bxnf[*,*,k,*]+bxnf[*,*,k+1,*])
endfor
  w[*,*,0,*] =0.0
 bx[*,*,0,*]=0.

  w[*,*,nr-1,*] =0.
 bx[*,*,nr-1,*]=0.

wm = total(w,1)/128.

wt = 0.*w
for i=0,127 do begin 
 wt[i,*,*,*] = w[i,*,*,*] - wm[*,*,*] 
endfor

end
