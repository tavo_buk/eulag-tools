if !d.name eq 'PS' then begin                                                                                                      
    device,xsize=22,ysize=12,yoffset=3
    !p.thick=2 & !x.thick=1 & !y.thick=1
end
!x.margin=[4,3] & !y.margin=[4,2]
!p.charsize=1.4
!p.multi=[0,2,1]

;restore,'stresses_mhd.sav'
nt=(size(int_frMC))[2]
fac=1./sqrt(5.)
sdfrmc=fltarr(l)
sdfrrs=fltarr(l)
sdfrmt=fltarr(l)
sdfrms=fltarr(l)
sdfthmc=fltarr(m)
sdfthrs=fltarr(m)
sdfthmt=fltarr(m)
sdfthms=fltarr(m)
r1=reverse(r)
xv=[r,r]
xv=[r,r1]
th=90.-theta*180./!pi
th1=reverse(th)
thv=[th,th]
thv=[th,th1]

print,'averaging over ',nn, ' time steps'
frMC=total(int_frMC[*,nt-nn:*],2)/double(nn)/1.e30
frRS=total(int_frRS[*,nt-nn:*],2)/double(nn)/1.e30
if (imhd gt 0) then begin
 frMT=total(int_frMT[*,nt-nn:*],2)/double(nn)/1.e30
 frMS=total(int_frMS[*,nt-nn:*],2)/double(nn)/1.e30
endif

for k = 0,l-1 do begin 
 a=moment(int_frMC[k,*]/1.e30) & sdfrmc[k]=sqrt(a(1))
 a=moment(int_frRS[k,*]/1.e30) & sdfrrs[k]=sqrt(a(1))
 if (imhd gt 0) then begin
  a=moment(int_frMT[k,*]/1.e30) & sdfrmt[k]=sqrt(a(1))
  a=moment(int_frMS[k,*]/1.e30) & sdfrms[k]=sqrt(a(1))
 endif
endfor

plot,r,frMC,xtitle='r/R'+sunsymbol(),title='a) !8I!Dr!N!6/!810!U30!N!6 [kg m!U2!N s!U-2!N]',$
;	xstyle=1,ystyle=1,yr=[-3,1.5],/nodata,xticks=3,xr=[0.61,0.96]
	xstyle=1,ystyle=1,yr=[-1.4,1.4],/nodata,xticks=3,xr=[0.61,0.96]

yv1=[(frMC-fac*sdfrmc)]                                                                                               
yv2=reverse((frMC+fac*sdfrmc))
yv=[yv1,yv2]
polyfill,xv,yv,/data,transparent=5,col=cgColor("red")
oplot,[0.61,0.96],r*0,li=1
oplot,[0.72,0.72],[-10,10],li=1

yv1=[(frRS-fac*sdfrrs)]                                                                                               
yv2=reverse((frRS+fac*sdfrrs))
yv=[yv1,yv2]
polyfill,xv,yv,/data,transparent=5,col=cgColor("orange")

oplot,r,smooth(frMC,2),thick=5, color=cgColor("black")
oplot,r,smooth(frRS,2),thick=5, color=cgColor("black"),li=2

xyouts,0.85,0.9,'MC'
xyouts,0.86,-0.9,'RS'

if (imhd gt 0) then begin
 sdfrmt=smooth(sdfrmt,2)
 yv1=[(frMT-fac*sdfrmt)]                                                                                               
 yv2=reverse((frMT+fac*sdfrmt))
 yv=[yv1,yv2]
 polyfill,xv,smooth(yv,2),/data,transparent=5,col=cgColor("blue")

 yv1=[(frMS-fac*sdfrms)]                                                                                               
 yv2=reverse((frMS+fac*sdfrms))
 yv=[yv1,yv2]
 polyfill,xv,yv,/data,transparent=0,col=cgColor("green")

 oplot,r,smooth(frMT,2),color=cgColor("black"),thick=5
 oplot,r,smooth(frMS,2),thick=5, color=cgColor("black"),li=2
 oplot,r,smooth((frMC+frMT+frRS+frMS),2),li=3,thick=5

 xyouts,0.68,0.2,'MT'
 xyouts,0.88,0.3,'MS'
endif


; NOW FOR I_TH

fthMC=total(int_fthMC[*,nt-nn:*],2)/double(nn)/1.e30
fthRS=total(int_fthRS[*,nt-nn:*],2)/double(nn)/1.e30
if (imhd gt 0) then begin
 fthMT=total(int_fthMT[*,nt-nn:*],2)/double(nn)/1.e30
 fthMS=total(int_fthMS[*,nt-nn:*],2)/double(nn)/1.e30
endif

for j = 0,m-1 do begin 
 a=moment(int_fthMC[j,*]/1.e30) & sdfthmc[j]=sqrt(a(1))
 a=moment(int_fthRS[j,*]/1.e30) & sdfthrs[j]=sqrt(a(1))
 if (imhd gt 0) then begin
  a=moment(int_fthMT[j,*]/1.e30) & sdfthmt[j]=sqrt(a(1))
  a=moment(int_fthMS[j,*]/1.e30) & sdfthms[j]=sqrt(a(1))
 endif
endfor

plot,th,fthMC,xtitle='90-!7h!6',title='b) !8I!D!7h!N!6/!810!U30!N!6 [kg m!U2!N s!U-2!N]',$
;	xstyle=1,ystyle=1,yr=[-0.35,0.35],/nodata,xticks=3
	xstyle=1,ystyle=1,yr=[-0.85,0.85],/nodata,xticks=3

sdfthmc=smooth(sdfthmc,2)
yv1=[(fthMC-fac*sdfthmc)]                                                                                               
yv2=reverse((fthMC+fac*sdfthmc))
yv=[yv1,yv2]
polyfill,thv,smooth(yv,2),/data,transparent=5,col=cgColor("red")

sdfthrs=smooth(sdfthrs,2)
yv1=[(fthRS-fac*sdfthrs)]                                                                                               
yv2=reverse((fthRS+fac*sdfthrs))
yv=[yv1,yv2]
polyfill,thv,smooth(yv,2),/data,transparent=5,col=cgColor("orange")

oplot,th,smooth(fthMC,2),thick=5, color=cgColor("black")
oplot,th,smooth(fthRS,2),thick=5, color=cgColor("black"),li=2

xyouts,-23,0.31,'MC'
xyouts,37,0.27,'RS'


if (imhd gt 0) then begin
 sdfthmt=smooth(sdfthmt,2)
 yv1=[(fthMT-fac*sdfthmt)]                                                                                               
 yv2=reverse((fthMT+fac*sdfthmt))
 yv=[yv1,yv2]
 polyfill,thv,smooth(yv,2),/data,transparent=5,col=cgColor("blue")

 sdfthms=smooth(sdfthms,2)
 yv1=[(fthMS-fac*sdfthms)]                                                                                               
 yv2=reverse((fthMS+fac*sdfthms))
 yv=[yv1,yv2]
 polyfill,thv,smooth(yv,2),/data,transparent=5,col=cgColor("green")
 
 oplot,th,smooth(fthMT,2),thick=5, color=cgColor("black")
 oplot,th,smooth(fthMS,2),thick=5, color=cgColor("black"),li=2
 oplot,th,smooth((fthMC+fthMT+fthRS+fthMS),2),li=3,thick=5
 
 xyouts,45,-0.25,'MT'
 xyouts,32,0.00,'MS'
endif

!p.multi=0
end
