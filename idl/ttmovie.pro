@tt_setup
!p.charsize=1.3
default,png,0
window,0,xsize=350,ysize=500
nt=(size(aarr))[3]
nlev=64
fo = "(F5.1)"
day=24.*3600.
lev=grange(min(aarr),max(aarr),nlev)/2.
for it=0,nt-1 do begin
 time = nslice*dt*double(it)/day
 print,it, 't=', time
 contour,smooth(transpose(aarr[*,*,it]),1),x,y,/fi,nl=64,/iso,lev=lev,xstyle=4,ystyle=4,$
	 title='!6T00 model, !8u!Dr!N , !8t =!6'+string(time,FORMAT=fo)+' !6day'
 plots,circle(0,0,0.95),thick=3
 plots,circle(0,0,0.1),thick=3
 oplot,[0,0],[-0.95,-0.1],thick=3,li=0
 oplot,[0,0],[0.1,0.96],thick=3,li=0
 if (png ge 1) then begin 
  istr2 = strtrim(string(it,'(I20.4)'),2) ;(only up to 9999 frames)
  image = tvrd(true=1)
  imgname = './img_'+istr2+'.png'
  write_png, imgname, image, red, green, blue
  print,"writing png",'./img_'+istr2+'.png'
 endif
 wait,0.3
endfor



end
