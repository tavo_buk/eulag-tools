eu_read_param,o=o                                                                                                           
default, itac, 0
 
n = o.n
m = o.m
l = o.l
nt = o.nt
dt = o.dt00
nslice = o.nslice
nxaver = o.nxaver
 
theta = indgen(m)*!pi/(m-1)
phi = indgen(n)*2.*!pi/(n-1)
rr = 6.97e8  ; Solar radii in [m/s]

if (itac eq 1) then begin
 rds = 0.61*rr ; base of the convection zone in [m/s]
endif else begin
 rds = 0.718*rr ; base of the convection zone in [m/s]
endelse
r = (rds + indgen(l)*o.dz00)/rr
x = r#sin(theta)
y = r#cos(theta)

px=r#cos(phi)
py=r#sin(phi)

th = 180.*theta/!pi - 90.
ph = 180.*phi/!pi -180.

save,r,th,ph,filename='grid.sav'
end
