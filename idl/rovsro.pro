
if !d.name eq 'PS' then begin
    device,xsize=18,ysize=12,yoffset=3
    !p.thick=2 & !x.thick=1 & !y.thick=1
end
!p.charsize=1.4

; Make a vector of 16 points, A[i] = 2pi/16:
A = FINDGEN(17) * (!PI*2/16.)
; Define the symbol to be a unit circle with 16 points, 
; and set the filled flag:
USERSYM, COS(A), SIN(A),/fill

;     112    56      49      42      35      28     14     
urm=[36.19,  33.98,  34.79,  35.40,  33.55,  28.47, 28.23]
chi=[-0.39, -0.27,  -0.29,   0.037,   0.074, 0.18,  0.025 ];  ,  0.168,  -0.067,  0.077,  0.066,  0.0005]
Roe=[-0.30, -0.28, -0.284, -0.017, -0.014,  0.10, -0.013  ];,   0.06,   -0.26,   0.15,   0.10,   0.08]
Ro= [0.16,   0.075,  0.067,  0.059,  0.046,  0.031, 0.016 ];  ,  0.019,   0.065,  0.038,  0.039,  0.036]
Ra= [16.,    4.0,    3.05,   2.24,   1.56,   1.,    0.25  ];  ,   0.39,    2,      1.02,   1.1,    1.78]
runs=['L1',  'L2',   'L3',   'L4',   'L5',   'L6',  'L7'                  ];   ,'L8','L9','M3','H3','N3']
n=(size(ra))[1]

urm2=[13,68,  47.04]
chi2=[0.12,  -0.22]   ;,  0.077,  0.066,  0.0005]
Roe2=[0.06,   -0.26]  ;,  0.15,   0.10,   0.08]
Ro2= [0.015,   0.052] ;,  0.038,  0.039,  0.036]
Ra2= [0.39,    2.]    ; ,      1.02,   1.1,    1.78]
runs2=['L8','L9']     ; ,'M3','H3','N3']
n2=(size(ra2))[1]

urm3=[28.13,  28.24,  26.26]
chi3=[0.17,   0.09,   0.104]
Roe3=[0.15,   0.10,   0.094]
Ro3= [0.031,  0.0312, 0.029]
Ra3= [1.02,   1.1,    1.78]
runs3=['M1','H1','N1']
n3=(size(ra3))[1]

; DYNAMO DATA
Ro4=[0.015,0.033,0.069,0.161,0.664]
chi4=[0.02,0.07,0.05,0.28,0.30] 
runs4=['RC00','RC01','RC02','RC03','RC04']
n4=(size(Ro4))[1]

print,n,n2
l=fltarr(2,n)
 for i=0,n-1 do begin
  for j=0,1 do begin
  if (j eq 0) then l[j,i]=ro[i]
;  if (j eq 1) then l[j,i]=roe[i]
  if (j eq 1) then l[j,i]=chi[i]+0.015
  endfor
 endfor

l2=fltarr(2,n2)
 for i=0,n2-1 do begin
  for j=0,1 do begin
  if (j eq 0) then l2[j,i]=ro2[i]
;  if (j eq 1) then l2[j,i]=roe2[i]
  if (j eq 1) then l2[j,i]=chi2[i]+0.015
  endfor
 endfor

l3=fltarr(2,n3)
 for i=0,n3-1 do begin
  for j=0,1 do begin
  if (j eq 0) then l3[j,i]=ro3[i]-0.0020
;  if (j eq 1) then l2[j,i]=roe2[i]
  if (j eq 1) then l3[j,i]=chi3[i]-0.035
  endfor
 endfor

l4=fltarr(2,n4)
 for i=0,n4-1 do begin
  for j=0,1 do begin
  if (j eq 0) then l4[j,i]=ro4[i]
;  if (j eq 1) then l2[j,i]=roe2[i]
  if (j eq 1) then l4[j,i]=chi4[i]-0.025
  endfor
 endfor

plot,ro,chi,/xlog,psym=8,symsize=1,ytitle='!4v!D!4X!N!6',xtitle='Ro',xr=[0.013,0.8],xstyle=1,yr=[-0.45,0.35],ystyle=1
oplot,[0.01,0.3],[0,0],li=1
oplot,[0.01,0.8],[0,0],li=1
oplot,[0.01,0.8],[0.19,0.19],li=4
oplot,[0.063,0.063],[-0.8,0.8],li=2
for i=0,n-1 do begin
xyouts,(l[*,i])[0], (l[*,i])[1],runs[i],charsize=1
endfor

oplot,ro2,chi2,psym=4,symsize=1,thick=5,color=120
for i=0,n2-1 do begin
xyouts,(l2[*,i])[0], (l2[*,i])[1],runs2[i],charsize=1
endfor

oplot,ro3,chi3,psym=5,thick=5,color=40
for i=0,n3-1 do begin
xyouts,(l3[*,i])[0], (l3[*,i])[1],runs3[i],charsize=0.8
endfor

oplot,ro4,chi4,psym=6,thick=6,color=cgcolor('green')
for i=0,n4-1 do begin
xyouts,(l4[*,i])[0], (l4[*,i])[1],runs4[i],charsize=0.8
endfor




xyouts,0.2,0.03,'!6 "solar-like" rotation',charsize=0.8
xyouts,0.2,-0.04,'!6 "anti-solar" rotation',charsize=0.8

xyouts,0.015,0.21,'!6 solar value',charsize=0.8

end
