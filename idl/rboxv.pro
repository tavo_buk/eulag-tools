default, ivtk, 0
default, ivdf, 0

nr=(size(r))[1]
nth=(size(th))[1]
nphi=(size(ph))[1]
file='fort.11'
nt=0D
openr,lun,file,/get_lun,/f77_unformatted
while (not eof(lun)) do begin
  readu,lun,uu
  readu,lun,vv
  readu,lun,ww
  readu,lun,tt
  readu,lun,pp
  readu,lun,bxx
  readu,lun,byy
  readu,lun,bzz
  nt++
endwhile
print,'number of outputs = ', nt

uu=fltarr(nphi,nth,nr)
vv=fltarr(nphi,nth,nr)
ww=fltarr(nphi,nth,nr)
tt=fltarr(nphi,nth,nr)
pp=fltarr(nphi,nth,nr)
bxx=fltarr(nphi,nth,nr)
byy=fltarr(nphi,nth,nr)
bzz=fltarr(nphi,nth,nr)

u=fltarr(nphi,nth,nt)
v=fltarr(nphi,nth,nt)
w=fltarr(nphi,nth,nt)
bx=fltarr(nphi,nth,nt)
by=fltarr(nphi,nth,nt)
bz=fltarr(nphi,nth,nt)

ns=0L
lev=62

openr,lun,file,/get_lun,/f77_unformatted
for it=0,nt-1 do begin 
  readu,lun,uu
  readu,lun,vv
  readu,lun,ww
  readu,lun,tt
  readu,lun,pp
  readu,lun,bxx
  readu,lun,byy
  readu,lun,bzz
  u[*,*,it]=uu[*,*,lev] 
  v[*,*,it]=vv[*,*,lev]
  w[*,*,it]=ww[*,*,lev]
  bx[*,*,it]=bxx[*,*,lev] 
  by[*,*,it]=byy[*,*,lev]
  bz[*,*,it]=bzz[*,*,lev]
endfor
free_lun,lun

vvec = fltarr(3,nphi,nth,nt)
bvec = fltarr(3,nphi,nth,nt)

vvec[0,*,*,*] = w[*,*,*]
vvec[1,*,*,*] = v[*,*,*]
vvec[2,*,*,*] = u[*,*,*]
save,filename='V_vec.sav',vvec

bvec[0,*,*,*] = bz[*,*,*]
bvec[1,*,*,*] = by[*,*,*]
bvec[2,*,*,*] = bx[*,*,*]
save,filename='B_vec.sav',bvec

end

