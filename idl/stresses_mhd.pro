if !d.name eq 'PS' then begin
    device,xsize=18,ysize=24,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
end

@eu_read_bcprofl
!p.multi=[0,2,4] 
!p.charsize=1.5 & !x.margin=[8,3] & !y.margin=[3,3]

default,tt,28.
nt=(size(u))[3]
year = 365.*24.*60.*60.
time = (nxaver*dt*indgen(nt))/year
; TIME GOOD
good=where(time ge 0)
nn = n_elements(good)
; ************************
;reading stratification
openr, lun, 'strat.dat', /get_lun
data=fltarr(7,l)
readf, lun, data
rho_s=reform(data(4,*))
close,lun

mu0 = 4.*!pi*1e-7 
omega_0=2.*!pi/(tt*28.*3600.)

ruu = (total((total(u2,1)/m),1)/l)-(total((total(u^2,1)/m),1)/l)
rvv = (total((total(v2,1)/m),1)/l)-(total((total(v^2,1)/m),1)/l)
rww = (total((total(w2,1)/m),1)/l)-(total((total(w^2,1)/m),1)/l)

qwu=fltarr(l,m,nn)
qvu=fltarr(l,m,nn)
mbzbx=fltarr(l,m,nn)
mbybx=fltarr(l,m,nn)
frMC=fltarr(l,m,nn)
frRS=fltarr(l,m,nn)
frMT=fltarr(l,m,nn)
frMS=fltarr(l,m,nn)
fthMC=fltarr(l,m,nn)
fthRS=fltarr(l,m,nn)
fthMT=fltarr(l,m,nn)
fthMS=fltarr(l,m,nn)

r2s_frMC=fltarr(l,m,nn)
r2s_frRS=fltarr(l,m,nn)
r2s_frMT=fltarr(l,m,nn)
r2s_frMS=fltarr(l,m,nn)
int_frMC=fltarr(l,nn)
int_frRS=fltarr(l,nn)
int_frMT=fltarr(l,nn)
int_frMS=fltarr(l,nn)

rs_fthMC=fltarr(l,m,nn)
rs_fthRS=fltarr(l,m,nn)
rs_fthMT=fltarr(l,m,nn)
rs_fthMS=fltarr(l,m,nn)
int_fthMC=fltarr(m,nn)
int_fthRS=fltarr(m,nn)
int_fthMT=fltarr(m,nn)
int_fthMS=fltarr(m,nn)

rsinth=fltarr(l,m)
uframe=fltarr(l,m)

urms2t = ruu+rvv+rww
urms2 = (total(urms2t[good]/float((size(good))[1])))

; reynolds stresses
for k=0, l-1 do begin
  for j=0,m-1 do begin
    rsinth[k,j]=rr*r[k]*sin(theta[j])
    uframe[k,j]=omega_0*rsinth[k,j]
    ; Reynolds stresss
    qwu[k,j,*]=(rwu[j,k,good]-rho[j,k]*w[j,k,good]*u[j,k,good])/rho[j,k]
    qvu[k,j,*]=(rvu[j,k,good]-rho[j,k]*v[j,k,good]*u[j,k,good])/rho[j,k]
    ; Maxwell stresses
    mbzbx[k,j,*]=(bzbx[j,k,good]-bz[j,k,good]*bx[j,k,good])
    mbybx[k,j,*]=(bxby[j,k,good]-by[j,k,good]*bx[j,k,good])
    ; r components of the angular momentum flux
    frMC[k,j,*] = rho_s[k]*rsinth[k,j]*(u[j,k,good]+uframe[k,j])*w[j,k,good]
    frRS[k,j,*] = rho_s[k]*rsinth[k,j]*qwu[k,j,*]
    frMT[k,j,*] = -rsinth[k,j]*bz[j,k,good]*bx[j,k,good]/mu0
    frMS[k,j,*] = -rsinth[k,j]*mbzbx[k,j,*]/mu0
    ; th components of the angular momentum flux
    fthMC[k,j,*] = rho_s[k]*rsinth[k,j]*(u[j,k,good]+uframe[k,j])*v[j,k,good]
    fthRS[k,j,*] = rho_s[k]*rsinth[k,j]*qvu[k,j,*]
    fthMT[k,j,*] = -rsinth[k,j]*by[j,k,good]*bx[j,k,good]/mu0
    fthMS[k,j,*] = -rsinth[k,j]*mbybx[k,j,*]/mu0
    ; quantities to integrate
    r2s_frMC[k,j,*] = rr*r[k]*rsinth[k,j]*frMC[k,j,*]
    r2s_frRS[k,j,*] = rr*r[k]*rsinth[k,j]*frRS[k,j,*]
    r2s_frMT[k,j,*] = rr*r[k]*rsinth[k,j]*frMT[k,j,*]
    r2s_frMS[k,j,*] = rr*r[k]*rsinth[k,j]*frMS[k,j,*]
    rs_fthMC[k,j,*] = rsinth[k,j]*fthMC[k,j,*]
    rs_fthRS[k,j,*] = rsinth[k,j]*fthRS[k,j,*]
    rs_fthMT[k,j,*] = rsinth[k,j]*fthMT[k,j,*]
    rs_fthMS[k,j,*] = rsinth[k,j]*fthMS[k,j,*]
  endfor
endfor

; Integrated fluxes
for it=0,nn-1 do begin
 for k = 0,l-1 do begin 
  int_frMC[k,it]=int_tabulated(theta,reform(r2s_frMC[k,*,it]),/double)
  int_frRS[k,it]=int_tabulated(theta,reform(r2s_frRS[k,*,it]),/double)
  int_frMT[k,it]=int_tabulated(theta,reform(r2s_frMT[k,*,it]),/double)
  int_frMS[k,it]=int_tabulated(theta,reform(r2s_frMS[k,*,it]),/double)
 endfor
endfor
for it=0,nn-1 do begin
 for j = 0,m-1 do begin 
  int_fthMC[j,it]=int_tabulated(rr*r[3:*],reform(rs_fthMC[3:*,j,it]),/double)
  int_fthRS[j,it]=int_tabulated(rr*r[3:*],reform(rs_fthRS[3:*,j,it]),/double)
  int_fthMT[j,it]=int_tabulated(rr*r,reform(rs_fthMT[*,j,it]),/double)
  int_fthMS[j,it]=int_tabulated(rr*r,reform(rs_fthMS[*,j,it]),/double)
 endfor
endfor

;save,filename='stresses_mhd.sav',int_frMC,int_frRS,int_frMT,int_frMS,$
;	int_fthMC,int_fthRS,int_fthMT,int_fthMS

;int_fthMC=rr*total(int_fthMC,1)
;int_fthRS=rr*total(int_fthRS,1)
;int_fthMT=rr*total(int_fthMT,1)
;int_fthMS=rr*total(int_fthMS,1)

; divergence r component
ffr=frMC+frRS-frMT-frMS
ffth=fthMT;+fthRS-fthMT-fthMS

divR=ffr*0.
for it=0,nn-1 do begin
 for j = 0,m-1 do begin 
  divR[*,j,it]=xder_curl((r*rr)^2*smooth(reform(ffr[*,j,it]),2),(r*rr))/(r*rr)^2
 endfor
endfor

divTH=ffth*0.
for it=0,nn-1 do begin
 for k = 0,l-1 do begin 
  divTH[k,1:m-2,it]=xder_curl(sin(theta[1:m-2])*smooth(reform(ffth[k,1:m-2,it]),2),theta[1:m-2])/rsinth[k,1:m-2]
 endfor
endfor


if (itac eq 0) then begin
 rlev=44
endif else begin
 rlev=61
endelse

iplot = 0
if(iplot eq 1) then begin

thg=where(th GE -75 AND th LT 75)
;xr=[30,120]
xr=[60,80]
lev=grange(-5e11,5e11,32)
contour,smooth(rotate(reform(frMC[rlev,thg,*]),3),2),time[good],th[thg],/fi,nl=32,$
	xr=xr,xstyle=1,ystyle=1,lev=lev
lev=grange(-5e13,5e13,32)
contour,smooth(rotate(reform(fthMC[rlev,thg,*]),3),2),time[good],th[thg],/fi,nl=32,$
	xr=xr,xstyle=1,ystyle=1,lev=lev

lev=grange(-5e11,5e11,32)
contour,smooth(rotate(reform(frRS[rlev,thg,*]),3),2),time[good],th[thg],/fi,nl=32,$
	xr=xr,xstyle=1,ystyle=1,lev=lev
lev=grange(-1e13,1e13,32)
contour,smooth(rotate(reform(fthRS[rlev,thg,*]),3),2),time[good],th[thg],/fi,nl=32,$
	xr=xr,xstyle=1,ystyle=1,lev=lev

lev=grange(-5e11,5e11,32)
contour,smooth(rotate(reform(frMT[rlev,thg,*]),3),2),time[good],th[thg],/fi,nl=32,$
	xr=xr,xstyle=1,ystyle=1,lev=lev
lev=grange(-1e13,1e13,32)
contour,smooth(rotate(reform(fthMT[rlev,thg,*]),3),2),time[good],th[thg],/fi,nl=32,$
	xr=xr,xstyle=1,ystyle=1,lev=lev

lev=grange(-5e11,5e11,32)
contour,smooth(rotate(reform(frMS[rlev,thg,*]),3),2),time[good],th[thg],/fi,nl=32,$
	xr=xr,xstyle=1,ystyle=1,lev=lev
lev=grange(-1e13,1e13,32)
contour,smooth(rotate(reform(fthMS[rlev,thg,*]),3),2),time[good],th[thg],/fi,nl=32,$
	xr=xr,xstyle=1,ystyle=1,lev=lev
endif

!p.multi=0
end
