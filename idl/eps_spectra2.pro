set_plot, 'ps'
device, filename='spectra.eps';, xsize=45,ysize=20

;plot, Er, /xlog, /ylog,xr=[2,200],xstyle=1,yr=[yrmin,yrmax]
;oplot, intercept1*ks^(-5./3.),linestyle=2, color=160
;oplot, intercept2*ks^(-3.), linestyle=2, color='FF0000'x

plot, Er, /xlog, /ylog,xr=[2,150], xtitle='Wavenumber k',ytitle='E(k)',xstyle=1,yr=[yrmin,yrmax]
oplot, intercept1*ks^(-5./3.),linestyle=2, color=160
oplot, intercept2*ks^(-3.), linestyle=2, color=160
for ii=0,1 do xyouts, xpos[ii], ypos[ii], names[ii]


device, /close

end
