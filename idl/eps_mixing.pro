
set_plot, 'ps'
device, filename='mixing.eps';, xsize=45,ysize=20

plot,z,g1,xtitle='(z)', thick=3 ;, yr=[-0.01,0.02]
oplot,z,g2*k_12_prom, thick=3, color=160
oplot,z,((g3)^(2./3.))*k_13_prom, thick=3, color='FF0000'x
oplot, z, make_array(z_num), linestyle=5
for ii=0, 2 do xyouts, xpos, ypos[ii], names[ii]

device, /close

end

