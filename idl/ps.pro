function gammac,f,g
  return,fix(255.*(float(f)/255.)^g)
end

PRO ps
COMMON cdevice,old_device
old_device=!d.name
tvlct,/get,r,g,b
set_plot,'ps'
gam=1.0
r=gammac(r,gam) & g=gammac(g,gam) & b=gammac(b,gam)
tvlct,rebin(r,256),rebin(g,256),rebin(b,256)
device,bits=8,/color
end
