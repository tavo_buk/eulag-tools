;mv idl.ps ~/Dropbox/tex/SPD/figs/sp3.eps

!p.multi=[0,1,2] & !p.charsize=1.0 
device,decomposed=0
loadct,5
!p.background=0
!p.color=255
default,ti,1

if !d.name eq 'PS' then begin
    device,xsize=18,ysize=15,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
end

year = 365.*24.*60.*60.
fo = "(F5.2)"
ffi='!8u!Dr!N!6'
nlev=128
thg=where(th GE -88 AND th LT 88)
aarr=reform(wnf[*,*,62,0:*])
bbrr=reform(wnf[*,*,61,0:*])

uz_xy=aarr
bx_xy=bbrr


for ii=0,n-2 do begin 
  uz_xy[ii,*,*]=0.5*(aarr[ii,*,*]+aarr[ii+1,*,*])
  bx_xy[ii,*,*]=0.5*(bbrr[ii,*,*]+bbrr[ii+1,*,*])
endfor                   
uz_xy[n-1,*,*]=aarr[n-1,*,*]
bx_xy[n-1,*,*]=bbrr[n-1,*,*]

for i=58,59 do begin
window,0,xsize=1.2*470,ysize=1.2*560
time = nslice*dt*double(i)/year
pos_ext=[0.17,0.55,0.96,0.95]
pos_bar=[0.12,0.65,0.13,0.85]
levav = sqrt(mean(aarr[*,thg,*]^2))
lev = 2.*max(abs(levav))*indgen(nlev)/float(nlev-1) $
        -max(abs(levav))
;lev=grange(-20,12,nlev)
;lev=grange(30,110,nlev)
loadct,3
map_set, 0, 0, 0, color=0, /horizon,/mollweide,E_HORIZON={FILL:1, COLOR:0},$
	 glinestyle=0, glinethick=4, /noborder,position=pos_ext,/noerase,latdel=30,/isotropic
contour,rotate(transpose(uz_xy[*,thg,i]),3),ph,th[thg],/fi,lev=lev, /over
map_set, 0, 0, 0, color=0, /mollweide,/horizon, /noerase, latdel=40,$
	 glinestyle=2, glinethick=2, /noborder,/grid,position=pos_ext
colorbar, pos=pos_bar, div=4, range=[min(lev),max(lev)], /vertical, $
          format='(F8.2)', charsize=1.3
          ;title='!8'+ffi+'!6 (!8r=!6'+fr+'!8R!6)'
;          title='!8'+ffi+'!6 (!8r=!6'+'!8R!6)',/top

pos_ext=[0.17,0.05,0.96,0.45]
pos_bar=[0.12,0.15,0.13,0.35]

levav = sqrt(mean(bbrr[*,thg,*]^2))
lev = 2.*max(abs(levav))*indgen(nlev)/float(nlev-1) $
        -max(abs(levav))
;lev=grange(min(-abs(levav)),max(levav),nlev)
;lev=grange(-0.09,0.09,nlev)

map_set, 0, 0, 0, color=0, /horizon,/mollweide,E_HORIZON={FILL:1, COLOR:0},$
	 glinestyle=0, glinethick=4, /noborder,position=pos_ext,/noerase,latdel=30
contour,rotate(transpose(bx_xy[*,thg,i]),3),ph,th[thg],/fi,lev=lev, /over
map_set, 0, 0, 0, color=0, /mollweide,/horizon, /noerase, latdel=40,$
	 glinestyle=2, glinethick=2, /noborder,/grid,position=pos_ext
colorbar, pos=pos_bar, div=4, range=[min(lev),max(lev)], /vertical, $
          format='(F8.2)', charsize=1.3
          ;title='!8'+ffi+'!6 (!8r=!6'+fr+'!8R!6)'
;          title='!8'+ffi+'!6 (!8r=!6'+'!8R!6)'

xyouts,0.05,0.38,'!8B!D!7u!N!6[T]',/normal,charsize=1.5
xyouts,0.03,0.88,'!8u!Dr!N!6[m/s]',/normal,charsize=1.5
xyouts,0.05,0.05,'!8t=!6'+string(time,FORMAT=fo)+' !6y',/normal,charsize=1.5
wait,0

png = 0
if (png EQ 1) then begin
     istr2 = strtrim(string(i,'(I20.4)'),2) ;(only up to 9999 frames)
     print,"writing png",'./img_'+istr2+'.png'
     image = tvrd(true=1)
     imgname = './img_'+istr2+'.png'
     write_png, imgname, image, red, green, blue
endif



print,i
endfor

end
