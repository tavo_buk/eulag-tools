
if !d.name eq 'PS' then begin
    device,xsize=20,ysize=7,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
end
default,d,0
!p.multi=[0,2,1]
!p.charsize=1.5


    ua = reform(u[*,*,d,it])
    va = reform(v[*,*,d,it])
    wa = reform(w[*,*,d,it])
    
    th = 180.*theta/!pi - 90.
    ph = 90+180.*phi/!pi-120.

    lev=grange(min(wa),max(wa),60)    
    map_set,/mollweide,/grid,/noborder,/isotropic,0,60,title='!8w!6'
    contour,(wa),ph,th,/fi,nl=64,/over,levels=lev
    
    lev=grange(min(ua),max(ua),60)    
    map_set,/mollweide,/grid,/noborder,/isotropic,0,60,/noerase,title='!8u!6'
    contour,(ua),ph,th,/fi,nl=64,/over
    



end
