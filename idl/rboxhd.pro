nr=(size(r))[1]
nth=(size(th))[1]
nphi=(size(ph))[1]
file='fort.11'
nt=0

openr,lun,file,/get_lun,/f77_unformatted
while (not eof(lun)) do begin
  readu,lun,uu
  readu,lun,vv
  readu,lun,ww
  readu,lun,tt
  readu,lun,pp
  nt++
endwhile

print,'number of outputs = ', nt
uu=fltarr(nphi,nth,nr)
vv=fltarr(nphi,nth,nr)
ww=fltarr(nphi,nth,nr)
tt=fltarr(nphi,nth,nr)
pp=fltarr(nphi,nth,nr)

unf=fltarr(nphi,nth,nr,nt)
vnf=fltarr(nphi,nth,nr,nt)
wnf=fltarr(nphi,nth,nr,nt)
tnf=fltarr(nphi,nth,nr,nt)
pnf=fltarr(nphi,nth,nr,nt)

u=fltarr(nphi,nth,nr,nt)
v=fltarr(nphi,nth,nr,nt)
w=fltarr(nphi,nth,nr,nt)
t=fltarr(nphi,nth,nr,nt)
p=fltarr(nphi,nth,nr,nt)

openr,lun,file,/get_lun,/f77_unformatted
print,n_elements(file)
for it=0,nt-1 do begin
  readu,lun,uu
  readu,lun,vv
  readu,lun,ww
  readu,lun,tt
  readu,lun,pp
  unf[*,*,*,it]=uu
  vnf[*,*,*,it]=vv
  wnf[*,*,*,it]=ww
  tnf[*,*,*,it]=tt
  pnf[*,*,*,it]=pp
  print,'reading data from file: ',file, it
endfor
free_lun,lun

;filtering p and q modes
print,'filtering'
for k=1, nr-2 do begin
 u[*,*,k,*]  =0.25*(unf[*,*,k-1,*]+2.*unf[*,*,k,*]+unf[*,*,k+1,*])     
 v[*,*,k,*]  =0.25*(vnf[*,*,k-1,*]+2.*vnf[*,*,k,*]+vnf[*,*,k+1,*])    
 w[*,*,k,*]  =0.25*(wnf[*,*,k-1,*]+2.*wnf[*,*,k,*]+wnf[*,*,k+1,*])   
 t[*,*,k,*]  =0.25*(tnf[*,*,k-1,*]+2.*tnf[*,*,k,*]+tnf[*,*,k+1,*])   
 p[*,*,k,*]  =0.25*(pnf[*,*,k-1,*]+2.*pnf[*,*,k,*]+pnf[*,*,k+1,*])   
endfor
  u[*,*,0,*] = u[*,*,1,*]
  v[*,*,0,*] = v[*,*,1,*]
  w[*,*,0,*] =0.0
  t[*,*,0,*] =0.5*(tnf[*,*,0,*]+tnf[*,*,1,*])   
  p[*,*,0,*] =0.5*(pnf[*,*,0,*]+pnf[*,*,1,*])   

  u[*,*,nr-1,*] = u[*,*,nr-2,*]
  v[*,*,nr-1,*] = v[*,*,nr-2,*]
  w[*,*,nr-1,*] =0.
  t[*,*,nr-1,*] =0.5*(tnf[*,*,nr-1,*]+tnf[*,*,nr-2,*])   
  p[*,*,nr-1,*] =0.5*(pnf[*,*,nr-1,*]+pnf[*,*,nr-2,*])   
end
