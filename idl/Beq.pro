;Must execute (mnew.pro,time_series.pro,fluxes.pro) before calling this procedure
;must define tt from the dimension of urms given after ex.time_series.pro 
tt=topslice
meanrho=total(rho_ad,1)/l
velrms=urms[tt]
mu=1 ;from the EULAG settings

Ekin=0.5*meanrho*velrms*velrms

Beq=sqrt(2*Ekin*mu)

print, 'Equipartition B field for this simulation -> Beq= ', Beq

end
