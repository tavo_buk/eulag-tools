print,'SETUP:  ttauri star with radiative core'

n = 128
m = 64
l = 256
nt = 5760000 
dt = 300.
nslice = 14400
nxaver = 2280
nplot = 2280
year=365.*24.*3600. ; seg
dz = 2026000.

theta = indgen(m)*!pi/(m-1)
phi = indgen(n)*2.*!pi/(n-1)

rsol = 6.0775e8 ; Solar radii in [m/s]
rr= 0.95*rsol
rds = 0.1*rr ; base of the convection zone in [m/s]
r = (rds + indgen(l)*dz)/rsol
x = r#sin(theta)
y = r#cos(theta)

px=r#cos(phi)
py=r#sin(phi)

th = 180.*theta/!pi - 90.
ph = 180.*phi/!pi -180.

; defining stratification

openr, lun, 'strat.dat', /get_lun
rows=l
data=fltarr(8,rows)
readf, lun, data
;setting variable vectors ...
t_am = reform(data[2,*])
t_ad = reform(data[3,*])
rho_am = reform(data[4,*])
rho_ad = reform(data[5,*])
rho_av=mean(rho_ad)
the = reform(data[6,*])
p_ad = reform(data[7,*])
close,lun

;save,r,th,ph,filename='grid.sav'
end
