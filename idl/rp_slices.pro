default,field,'w'
default,extension,'xy'
default,sindx,'04'
default,png,0

;window,1,xsize=480,ysize=640

!p.background=0
!p.color=255

if (extension EQ 'xy') then begin
  filename = field+extension+sindx
  arr = dblarr(n,m)
endif

if (extension EQ 'yz') then begin
  filename = field+extension+sindx
  arr = dblarr(m,l)
endif


if (extension EQ 'xz') then begin
  filename = field+extension+sindx
  arr = dblarr(n,l)
endif

t = 0.D
openr,1,filename,/f77
ns = 0L
while (not eof(1)) do begin
  readu,1,arr,t
  if (ns LE 1) then begin
    aarr = arr 
    print,'Reading file: ',filename
  endif else begin
    aarr = [[[aarr]],[[arr]]]
  endelse
  wait,0.1
  ns ++
end
close,1

nn = (size(aarr))[3]
year = 365.*24.*60.*60.
fo = "(F5.2)"

min = min(aarr)
max = max(aarr)
print,min,max

;if (sindx EQ '01') then fr=string(r[20],format=fo)
;if (sindx EQ '02') then fr=string(r[50],format=fo)
;if (sindx EQ '03') then fr=string(r[85],format=fo)
;if (sindx EQ '04') then fr=string(r[90],format=fo)
if (sindx EQ '01') then fr=string(r[20],format=fo)
if (sindx EQ '02') then fr=string(r[30],format=fo)
if (sindx EQ '03') then fr=string(r[40],format=fo)
if (sindx EQ '04') then fr=string(r[45],format=fo)

nlev=100
thg=where(th GE -88 AND th LT 88)

levav = total(aarr[*,thg,nn-11:nn-1],3)/10.
;levav = total(aarr[*,thg,*],3)/float(nn)
;lev = 2.*max(abs(levav))*indgen(nlev)/float(nlev-1) $
;        -max(abs(levav))

;lev=grange(min(aarr),max(aarr),nlev)
lev=grange(-25,20,nlev)
print,'minmax levels',min(lev),max(lev)
;
pos_ext=[0.07,0.24,0.93,0.92]
pos_int=[0.2,0.24,0.8,0.84]
ffi='!8u!Dr!N!6'
;
if (extension EQ 'xy') then begin
  for i = 0, nt/nslice-2 do begin
    time = nslice*dt*double(i)/year
    print,i,time
    map_set, 0, 0, 0, color=0, /horizon,/mollweide,E_HORIZON={FILL:1, COLOR:0},$
         glinestyle=2, glinethick=2, /noborder,position=pos_ext; ,latdel=30
    contour,smooth(aarr[*,thg,i],0),ph,th[thg],/fi,lev=lev, /over
    map_set, 0, 0, 0, color=0, /mollweide,/horizon, /noerase, latdel=45,$
         glinestyle=2, glinethick=2, /noborder,/grid,position=pos_ext
    colorbar, pos=[0.1,0.09,0.9,0.12], div=4, range=[min(lev),max(lev)], /horizontal, $
          format='(F8.2)', charsize=1.3, $
          title='!8'+ffi+'!6 (!8r=!6'+fr+'!8R!6), !8t=!6'+string(time,FORMAT=fo)+' !6y'
;          title='!8'+ffi+'!6 (!8r=!6'+fr+'!8R!6)'

;    map_set, 20, 0, color=0, /ORTHOGRAPHIC,/isotropic,E_HORIZON={FILL:1, COLOR:0},$
;      	     glinestyle=0, glinethick=4, /noborder,position=pos_ext,/noerase,latdel=40
;
;    contour,smooth(aarr[*,thg,i],1),ph,th[thg],/fi,lev=lev, /over
;    map_set, 20, 0, color=255, /ORTHOGRAPHIC,/isotropic, /noerase, latdel=30,londel=60,$
; 	     glinestyle=0, glinethick=1, /noborder,/grid,position=pos_ext
;    colorbar, pos=[0.2,0.09,0.8,0.12], div=4, range=[min(lev),max(lev)], /horizontal, $
;          format='(F8.2)', charsize=1.2, $
;          title='!8'+ffi+'!6 (!8r=!6'+fr+'!8R!6)'

    if (png EQ 1) then begin 
     istr2 = strtrim(string(i,'(I20.4)'),2) ;(only up to 9999 frames)
     print,"writing png",'./img_'+istr2+'.png'
     image = tvrd(true=1)
     imgname = './img_'+istr2+'.png'
     write_png, imgname, image, red, green, blue
    endif
    wait,1. 
  endfor 
endif

if (extension EQ 'yz') then begin
  for i = 0, nt/nslice-2 do begin
    print,i
    time = nxaver*dt*double(i)/year
    contour,smooth(transpose(aarr[thg,*,i]),3),x[*,thg],y[*,thg],/fi,/iso,levels=lev,$
            title=ffi+'  !N!8t=!6'+string(time,FORMAT=fo)+' !6y'
    colorbar, pos=[0.32,0.4,0.34,0.65], div=4, range=[min(lev),max(lev)], /vert, $
              format='(F8.2)', charsize=1.5; ,title=field+'!8t=!6'+string(time,FORMAT=fo)+' !6y'
    if (png EQ 1) then begin 
     istr2 = strtrim(string(i,'(I20.4)'),2) ;(only up to 9999 frames)
     image = tvrd(true=1)
     imgname = './img_'+istr2+'.png'
     write_png, imgname, image, red, green, blue
    endif

    wait,0.05
  endfor 
endif

if (extension EQ 'xz') then begin
  for i = 0, nt/nslice-2 do begin
    print,i
    time = nxaver*dt*double(i)/year
    contour,smooth(transpose(aarr[*,*,i]),3),px[*,*],py[*,*],/fi,/iso,levels=lev,$
            title=ffi+'  !N!8t=!6'+string(time,FORMAT=fo)+' !6y'
;    colorbar, pos=[0.32,0.4,0.34,0.65], div=4, range=[min(lev),max(lev)], /vert, $
;              format='(F8.2)', charsize=1.5; ,title=field+'!8t=!6'+string(time,FORMAT=fo)+' !6y'
    if (png EQ 1) then begin 
     istr2 = strtrim(string(i,'(I20.4)'),2) ;(only up to 9999 frames)
     image = tvrd(true=1)
     imgname = './img_'+istr2+'.png'
     write_png, imgname, image, red, green, blue
    endif

    wait,0.01
  endfor 
endif
end
