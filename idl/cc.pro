eu_read_param,o=o                                                                                                           
default, itac, 0
 
n = o.n
m = o.m
l = o.l
nt = o.nt
dt = o.dt00
nslice = o.nslice
nxaver = o.nxaver
 
y = indgen(m)*4./(m-1)
x = indgen(n)*4./(n-1)
z = -2 + indgen(l)*(1.8-(-2))/(l-1) ; Solar radii in [m/s]

ph = x
th = y 
r = z

save,x,y,z,r,th,ph,filename='grid.sav'
end
