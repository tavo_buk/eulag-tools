eu_read_param,o=o                                                                                                           
default, itac, 1
 
n = o.n
m = o.m
l = o.l
nt = o.nt
dt = o.dt00
nslice = o.nslice
nxaver = o.nxaver
nplot=o.nplot
year=365.*24.*3600. ; seg
 
theta = indgen(m)*!pi/(m-1)
phi = indgen(n)*2.*!pi/(n-1)
rr = 1.39e9  ; Solar radii in [m/s]

if (itac eq 1) then begin
 rds = 0.6*rr ; base of the convection zone in [m/s]
endif else begin
 rds = 0.74*rr ; base of the convection zone in [m/s]
endelse

; defining stratification
openr, lun, './strat.dat', /get_lun
rows=l
data=fltarr(7,rows)
readf, lun, data
;setting variable vectors ...
r = reform(data[0,*])
t_am = reform(data[2,*])
rho_am = reform(data[3,*])
rho_ad = reform(data[4,*])
rho_av=mean(rho_ad)
the = reform(data[5,*])
close,lun
;save,r,th,ph,filename='grid.sav'

x = r#sin(theta)
y = r#cos(theta)

px=r#cos(phi)
py=r#sin(phi)

th = 180.*theta/!pi - 90.
ph = 180.*phi/!pi -180.
end
