tsu2 = (total((total(u2,1)/m),1)/l)-(total((total(u^2,1)/m),1)/l)
tsv2 = (total((total(v2,1)/m),1)/l)-(total((total(v^2,1)/m),1)/l)
tsw2 = (total((total(w2,1)/m),1)/l)-(total((total(w^2,1)/m),1)/l)
tsth = (total((total(ptemp,1)/m),1)/l)

nn = n_elements(tsu2)
urms = sqrt(tsu2+tsv2+tsw2)
;year = 365.*24.*60.*60.
;time = (nxaver*dt*indgen(nn))/year
cputime=indgen(nn)

set_plot, 'ps'
device, filename='tseries.eps', xsize=45,ysize=20

!x.margin=[9,3]
!p.charsize=1.5

!p.multi=[0,2,1] & !p.charsize=2.5

plot,urms,xtitle='!8t!6 (time steps)',ytitle='!8u!D!6rms!N',thick=3
plot,tsth,xtitle='!8t!6 (time steps)',ytitle='!4h!6',thick=3

!p.multi=0

device, /close

;if ntime eq !Null then begin
; print, 'please enter the value (ntime) to plot the & w xaverages ... '
;endif else begin
 tamano=size(urms)
 ntime=tamano[1]-1
 set_plot, 'ps'
 device, filename='xaver.eps', xsize=48, ysize=20
  
 !x.margin=[9,3]
 !p.charsize=1.5

 !p.multi=[0,2,1] & !p.charsize=2.5

 plot, w[0,*,ntime], xtitle='!8depth', ytitle='!8w', thick=3
 plot, the[0,*,ntime], xtitle='!8depth', ytitle='!4h!6', thick=3 

 !p.multi=0
 device, /close
;endelse
end
