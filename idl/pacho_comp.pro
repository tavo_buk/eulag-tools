if folder eq !Null then begin
 print, 'please specify folder name and variable!'
endif else begin
 if variable eq 'the' then begin
  set_plot, 'ps'
  file='the-profs-'+folder+'.eps'
  device, filename=file, xsize=30, ysize=20

  var1=theR24l
  var2=theR24vfl
  var3=theR24vf2l
  size1=n_elements(var1[0,0,*])
  size2=n_elements(var2[0,0,*])
  size3=n_elements(var3[0,0,*]) 
  sizes=[size1,size2,size3]
  ntime=min(sizes)-1
  plot, var1[0,*,ntime], xtitle='!8depth', ytitle='!4h!6', thick=3
  oplot,var2[0,*,ntime],thick=3,linestyle=1;, color=60000
  oplot, var3[0,*,ntime],thick=3,linestyle=2;, color='FF0000'x  

  device, /close
 endif else begin
  set_plot, 'ps'
  file='w-profs-'+folder+'.eps'
  device, filename=file, xsize=30, ysize=20

  var1=wR24l
  var2=wR24vfl
  var3=wR24vf2l
  size1=n_elements(var1[0,0,*])
  size2=n_elements(var2[0,0,*])
  size3=n_elements(var3[0,0,*])
  sizes=[size1,size2,size3]
  ntime=min(sizes)-1
  plot, var1[0,*,ntime], xtitle='!8depth', ytitle='!4h!6', thick=3
  oplot,var2[0,*,ntime],thick=3,linestyle=1;, color=60000
  oplot, var3[0,*,ntime],thick=3,linestyle=2;, color='FF0000'x  

  device, /close
 endelse 
endelse
end
