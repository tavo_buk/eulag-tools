;restore,'grid.sav'

default, ivtk, 0
default, ivdf, 0

nr=(size(r))[1]
nth=(size(th))[1]
nphi=(size(ph))[1]

file='fort.9'
time = 0.
  uu  = fltarr(nphi,nth,nr)   
  vv=   fltarr(nphi,nth,nr) 
  ww  = fltarr(nphi,nth,nr) 
  oxx=  fltarr(nphi,nth,nr) 
  oyy=  fltarr(nphi,nth,nr) 
  ozz=  fltarr(nphi,nth,nr) 
  oxx2= fltarr(nphi,nth,nr)
  oyy2= fltarr(nphi,nth,nr)
  ozz2= fltarr(nphi,nth,nr)
  tt=   fltarr(nphi,nth,nr) 
  pp=   fltarr(nphi,nth,nr) 
  fxx=  fltarr(nphi,nth,nr) 
  fyy=  fltarr(nphi,nth,nr) 
  fzz=  fltarr(nphi,nth,nr) 
  ftt=  fltarr(nphi,nth,nr) 
  ppm=  fltarr(nphi,nth,nr) 
  bxx=  fltarr(nphi,nth,nr) 
  byy=  fltarr(nphi,nth,nr) 
  bzz=  fltarr(nphi,nth,nr) 
  fbxx= fltarr(nphi,nth,nr)
  fbyy= fltarr(nphi,nth,nr)
  fbzz= fltarr(nphi,nth,nr)
  tke=  fltarr(nphi,nth,nr) 
  chm=  fltarr(nphi,nth,nr) 
  chm=  fltarr(nphi,nth,nr) 
  fchm= fltarr(nphi,nth,nr)
  fchm= fltarr(nphi,nth,nr)
nt=0D
openr,lun,file,/get_lun,/f77_unformatted
while (not eof(lun)) do begin
  readu,lun,uu
  readu,lun,vv
  readu,lun,ww   
  readu,lun,oxx
  readu,lun,oyy
  readu,lun,ozz
  readu,lun,oxx2
  readu,lun,oyy2
  readu,lun,ozz2
  readu,lun,tt
  readu,lun,pp
  readu,lun,fxx
  readu,lun,fyy
  readu,lun,fzz
  readu,lun,ftt
  readu,lun,time
  readu,lun,time
  readu,lun,time
  readu,lun,ppm
  readu,lun,bxx
  readu,lun,byy
  readu,lun,bzz
  readu,lun,fbxx
  readu,lun,fbyy
  readu,lun,fbzz
  readu,lun,time
  readu,lun,time
  readu,lun,time
  readu,lun,time
  readu,lun,time
  readu,lun,time
  readu,lun,time
  readu,lun,time
  readu,lun,time
  readu,lun,time
  readu,lun,tke
  readu,lun,time
  readu,lun,chm
  readu,lun,chm
  readu,lun,fchm
  readu,lun,fchm
  nt++
  print,'number of outputs = ', nt
endwhile

free_lun,lun

;filtering p and q modes
ifilt = 0 
if (ifilt eq 1) then begin 
print,'filtering'
u=unf
v=vnf
w=wnf
t=tnf
p=pnf
bx=bxnf
by=bynf
bz=bznf
;for k=1, nr-2 do begin
; u[*,*,k,*] =0.5*(unf[*,*,k,*]+unf[*,*,k+1,*])     
; v[*,*,k,*] =0.5*(vnf[*,*,k,*]+vnf[*,*,k+1,*])    
; w[*,*,k,*] =0.5*(wnf[*,*,k,*]+wnf[*,*,k+1,*])   
; t[*,*,k,*] =0.5*(tnf[*,*,k,*]+tnf[*,*,k+1,*])   
; p[*,*,k,*] =0.5*(pnf[*,*,k,*]+pnf[*,*,k+1,*])   
; bx[*,*,k,*]=0.5*(bxnf[*,*,k,*]+bxnf[*,*,k+1,*]) 
; by[*,*,k,*]=0.5*(bynf[*,*,k,*]+bynf[*,*,k+1,*]) 
; bz[*,*,k,*]=0.5*(bznf[*,*,k,*]+bznf[*,*,k+1,*]) 
;endfor
;  u[*,*,0,*] =0.5*(unf[*,*,0,*]+unf[*,*,1,*])     
;  v[*,*,0,*] =0.5*(vnf[*,*,0,*]+vnf[*,*,1,*])    
;  w[*,*,0,*] =0.5*(wnf[*,*,0,*]+wnf[*,*,1,*])   
;  t[*,*,0,*] =0.5*(tnf[*,*,0,*]+tnf[*,*,1,*])   
;  p[*,*,0,*] =0.5*(pnf[*,*,0,*]+pnf[*,*,1,*])   
; bx[*,*,0,*]=0.5*(bxnf[*,*,0,*]+bxnf[*,*,1,*]) 
; by[*,*,0,*]=0.5*(bynf[*,*,0,*]+bynf[*,*,1,*]) 
; bz[*,*,0,*]=0.5*(bznf[*,*,0,*]+bznf[*,*,1,*]) 
;
;  u[*,*,nr-1,*] =0.5*(unf[*,*,nr-1,*]+unf[*,*,nr-2,*])     
;  v[*,*,nr-1,*] =0.5*(vnf[*,*,nr-1,*]+vnf[*,*,nr-2,*])    
;  w[*,*,nr-1,*] =0.5*(wnf[*,*,nr-1,*]+wnf[*,*,nr-2,*])   
;  t[*,*,nr-1,*] =0.5*(tnf[*,*,nr-1,*]+tnf[*,*,nr-2,*])   
;  p[*,*,nr-1,*] =0.5*(pnf[*,*,nr-1,*]+pnf[*,*,nr-2,*])   
; bx[*,*,nr-1,*]=0.5*(bxnf[*,*,nr-1,*]+bxnf[*,*,nr-2,*]) 
; by[*,*,nr-1,*]=0.5*(bynf[*,*,nr-1,*]+bynf[*,*,nr-2,*]) 
; bz[*,*,nr-1,*]=0.5*(bznf[*,*,nr-1,*]+bznf[*,*,nr-2,*]) 

for k=1, nr-2 do begin
 u[*,*,k,*]  =0.25*(unf[*,*,k-1,*]+2.*unf[*,*,k,*]+unf[*,*,k+1,*])     
 v[*,*,k,*]  =0.25*(vnf[*,*,k-1,*]+2.*vnf[*,*,k,*]+vnf[*,*,k+1,*])    
 w[*,*,k,*]  =0.25*(wnf[*,*,k-1,*]+2.*wnf[*,*,k,*]+wnf[*,*,k+1,*])   
 t[*,*,k,*]  =0.25*(tnf[*,*,k-1,*]+2.*tnf[*,*,k,*]+tnf[*,*,k+1,*])   
 p[*,*,k,*]  =0.25*(pnf[*,*,k-1,*]+2.*pnf[*,*,k,*]+pnf[*,*,k+1,*])   
 bx[*,*,k,*]=0.25*(bxnf[*,*,k-1,*]+2.*bxnf[*,*,k,*]+bxnf[*,*,k+1,*]) 
 by[*,*,k,*]=0.25*(bynf[*,*,k-1,*]+2.*bynf[*,*,k,*]+bynf[*,*,k+1,*]) 
 bz[*,*,k,*]=0.25*(bznf[*,*,k-1,*]+2.*bznf[*,*,k,*]+bznf[*,*,k+1,*]) 
endfor
  u[*,*,0,*] =0.5*(unf[*,*,0,*]+unf[*,*,1,*])     
  v[*,*,0,*] =0.5*(vnf[*,*,0,*]+vnf[*,*,1,*])    
  w[*,*,0,*] =0.5*(wnf[*,*,0,*]+wnf[*,*,1,*])   
  t[*,*,0,*] =0.5*(tnf[*,*,0,*]+tnf[*,*,1,*])   
  p[*,*,0,*] =0.5*(pnf[*,*,0,*]+pnf[*,*,1,*])   
 bx[*,*,0,*]=0.5*(bxnf[*,*,0,*]+bxnf[*,*,1,*]) 
 by[*,*,0,*]=0.5*(bynf[*,*,0,*]+bynf[*,*,1,*]) 
 bz[*,*,0,*]=0.5*(bznf[*,*,0,*]+bznf[*,*,1,*]) 

  u[*,*,nr-1,*] =0.5*(unf[*,*,nr-1,*]+unf[*,*,nr-2,*])     
  v[*,*,nr-1,*] =0.5*(vnf[*,*,nr-1,*]+vnf[*,*,nr-2,*])    
  w[*,*,nr-1,*] =0.5*(wnf[*,*,nr-1,*]+wnf[*,*,nr-2,*])   
  t[*,*,nr-1,*] =0.5*(tnf[*,*,nr-1,*]+tnf[*,*,nr-2,*])   
  p[*,*,nr-1,*] =0.5*(pnf[*,*,nr-1,*]+pnf[*,*,nr-2,*])   
 bx[*,*,nr-1,*]=0.5*(bxnf[*,*,nr-1,*]+bxnf[*,*,nr-2,*]) 
 by[*,*,nr-1,*]=0.5*(bynf[*,*,nr-1,*]+bynf[*,*,nr-2,*]) 
 bz[*,*,nr-1,*]=0.5*(bznf[*,*,nr-1,*]+bznf[*,*,nr-2,*]) 

; bouhou[*,*,i] = (bou[*,*,i-1] + 2.*bou[*,*,i] + bou[*,*,i+1])/4.
;for it=0,5 do begin
; print,'it = ',it
; for i=0,nphi-1 do begin
;  for j=1,nth-1 do begin
;  tmp=reform(wnf[i,j,*,it])
;   w[i,j,*,it] = interpol(tmp,r,rint,/lsquadratic)
;  endfor
; endfor
;endfor
endif

if (ivtk ne 0) then begin
 rectilinear_grid=1
; Open the VTK file for write.
 file = 'work.vtk'
; Find the dimensions of the data.
 x=ph
 y=th
 z=r*1000.
 dx=x[1]-x[0]
 dy=y[1]-y[0]
 dz=z[1]-z[0]
 data_type = strlowcase(size(x, /tname)) 
 print,data_type
 nx=n_elements(x)
 ny=n_elements(y)
 nz=n_elements(z)
 ntot=nx * ny * nz
 dimensions= [nx, ny, nz]
 origin=[x[0], y[0], z[0]]
 spacing=[dx,dy,dz]
 ndim=n_elements(where(dimensions gt 1))
	       
; Open the VTK file for write.
 print, 'Writing ', strtrim(file), '...'
; openw, lun, file, /get_lun, /swap_if_little_endian
 openw, lun, file, /get_lun
		   
; Write the header information.
 printf, lun, '# vtk DataFile Version 2.0'
 printf, lun, 'EULAG Data'
 printf, lun, 'ASCII'
 if (rectilinear_grid eq 1) then begin
  printf, lun, 'DATASET RECTILINEAR_GRID'
  printf, lun, 'DIMENSIONS ', dimensions
  printf, lun, 'X_COORDINATES ', nx, ' ', data_type
  printf, lun, x
  printf, lun, 'Y_COORDINATES ', ny, ' ', data_type
  printf, lun, y
  printf, lun, 'Z_COORDINATES ', nz, ' ', data_type
  printf, lun, z
 endif else begin
  print, 'writing header'
  printf, lun, 'DATASET STRUCTURED_GRID'
  printf, lun, 'DIMENSIONS ', dimensions
  printf, lun, 'POINTS ', ntot
  printf, lun, x
  printf, lun, y
  printf, lun, z 
 endelse

 printf, lun, 'POINT_DATA ', ntot
; Write out each data field.
; Write scalar field.
 print, 'SCALARS ', strlowcase('bx') , ' ', data_type, '...'
 printf, lun, 'SCALARS ', strlowcase('bx') , ' ', data_type
 printf, lun, 'LOOKUP_TABLE default'
 printf, lun, bxx

 print, 'SCALARS ', strlowcase('by') , ' ', data_type, '...'
 printf, lun, 'SCALARS ', strlowcase('by') , ' ', data_type
 printf, lun, 'LOOKUP_TABLE default'
 printf, lun, byy
 
 print, 'SCALARS ', strlowcase('bz') , ' ', data_type, '...'
 printf, lun, 'SCALARS ', strlowcase('bz') , ' ', data_type
 printf, lun, 'LOOKUP_TABLE default'
 printf, lun, bzz
 
 ; print, 'VECTORS ', strlowcase('bfield'), ' ', data_type
; printf, lun, 'VECTORS ', strlowcase('bfield'), ' ', data_type
;; printf, lun, swap_endian(bxx,byy,bzz, /swap_if_big_endian)                 
; printf, lun, bxx,byy,bzz                 


 close, lun
 free_lun, lun                     
endif

; VAPOR DATA COLLECTION
;
if (ivdf eq 1) then begin

 isph=0
 default, tinit, 0
 default, tend, 1
 tend=n
 default,variables,['u','v','w','bx','by','by']
 if (isph eq 1) then begin 
  varnames=['u','v','w','bx','by','bz']
 endif else begin
  varnames=['vx','vy','vz','bx','by','bz']
 endelse

; Find the dimensions of the data.
 x=ph
 y=th
 z=r*500.
 dx=x[1]-x[0]
 dy=y[1]-y[0]
 dz=z[1]-z[0]
 data_type = strlowcase(size(x, /tname)) 
 nx=n_elements(x)
 ny=n_elements(y)
 nz=n_elements(z)
 ntot=nx * ny * nz
 origin=[x[0], y[0], z[0]]
 spacing=[dx,dy,dz]

 if (isph eq 1) then dim = [nx,ny,nz] else dim = [nx,ny,nz] 
 ; coarsened approximations to create
  num_levels = 0   
 ; create a new VDF metadata object
  mfd = vdf_create(dim,num_levels)
 ; number of timesteps in the dataset
  timesteps = tend-tinit+1                                                                                     
  vdf_setnumtimesteps, mfd,timesteps
 ; set the names of the variables the data set 
  numvar = n_elements(varnames)
;
  vdf_setvarnames, mfd, varnames
 ; set extents of the volume
 if (isph eq 1) then begin 
   vdf_setcoordtype,mfd,'spherical'
   ; order for spherical coordinates phi(z),theta(y),r(x) 
   extents = [x[0], y[0], z[0], x[nx-1], y[ny-1], z[nz-1]]
   print,extents
   last_dim = nz-1
  endif else begin
   extents = [x[0], y[0], z[0], x[nx-1], y[ny-1], z[nz-1]]
   last_dim = nz-1
  endelse
 ;
 vdf_setextents, mfd, extents
 ; Store the metadata object in a file for subsequent use
 vdffile = 'work.vdf'
 vdf_write, mfd, vdffile
 ; Destroy the metadata object. We're done with it.
 vdf_destroy, mfd

 ; Writing data
 ;
 itt = 0                           
 for it = tinit,tend-1 do begin ; time loop
   for nvar = 0, numvar-1 do begin ; variables loop
    dfd = vdc_bufwritecreate(vdffile)             
    vdc_openvarwrite, dfd, itt, varnames(nvar), -1
    ; Write (transform) the volume to the data set one slice at a time
    if (keyword_set(isph)) then begin
     case nvar of      
      0: tmp_var = reform(u[*,*,*,itt])      
      1: tmp_var = reform(v[*,*,*,itt])
      2: tmp_var = reform(w[*,*,*,itt])
      3: tmp_var = reform(bx[*,*,*,itt])
      4: tmp_var = reform(by[*,*,*,itt])
      5: tmp_var = reform(bz[*,*,*,itt])
     endcase           
    endif else begin     
     case nvar of     
      0: tmp_var = reform(u[*,*,*,itt])   
      1: tmp_var = reform(v[*,*,*,itt])
      2: tmp_var = reform(w[*,*,*,itt])    
      3: tmp_var = reform(bx[*,*,*,itt])
      4: tmp_var = reform(by[*,*,*,itt])
      5: tmp_var = reform(bz[*,*,*,itt])
     endcase           
    endelse              
    for z = 0, last_dim do begin             
     vdc_bufwriteslice, dfd, float(tmp_var[*,*,z])
    endfor
    ; close variable / timestep
    print,'nvar = ', varnames[nvar],'  ',minmax(tmp_var)
    vdc_closevar, dfd
   endfor
   itt++
   print,itt
 endfor
 ;       Destroy the "buffered write" data transformation object. 
 ;       We're done with it.
 vdc_bufwritedestroy,dfd
endif
end



