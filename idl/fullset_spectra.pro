eu_read_param,o=o
default, itac, 0

nz=o.l
nx=o.n
Lz=o.dz00
Lx=o.dx00
pi=3.141592

;_______________________________
;FOLDER CONFIGURATIONS ....

set='set2'

f1='R21'
f2='R22'
f3='R23'
f4='R24'

;_______________________________

; READING FOLDERS WITH CONSTANT FORCING DATA ...

folder1=f1+'_hd.cf'
print, 'setting folder to ', folder1
cd, folder1
@sub_rboxhd
@sub_spectra
Er_folder1=Er

folder2=f2+'_hd.cf'
print, 'setting folder to ', folder2
cd, '../'+folder2
@sub_rboxhd
@sub_spectra
Er_folder2=Er

folder3=f3+'_hd.cf'
print, 'setting folder to ', folder3
cd, '../'+folder3
@sub_rboxhd
@sub_spectra
Er_folder3=Er

folder4=f4+'_hd.cf'
print, 'setting folder to ', folder4
cd, '../'+folder4
@sub_rboxhd
@sub_spectra
Er_folder4=Er

; READING FOLDERS WITH VF FORCING DATA

folder5=f1+'_hd.vf'
print, 'setting folder to ', folder5
cd, '../'+folder5
@sub_rboxhd
@sub_spectra
Er_folder5=Er

folder6=f2+'_hd.vf'
print, 'setting folder to ', folder6
cd, '../'+folder6
@sub_rboxhd
@sub_spectra
Er_folder6=Er

folder7=f3+'_hd.vf'
print, 'setting folder to ', folder7
cd, '../'+folder7
@sub_rboxhd
@sub_spectra
Er_folder7=Er

folder8=f4+'_hd.vf'
print, 'setting folder to ', folder8
cd, '../'+folder8
@sub_rboxhd
@sub_spectra
Er_folder8=Er


; READING FOLDERS WITH VF2 FORCING DATA

folder9=f1+'_hd.vf2'
print, 'setting folder to ', folder9
cd, '../'+folder9
@sub_rboxhd
@sub_spectra
Er_folder9=Er

folder10=f2+'_hd.vf2'
print, 'setting folder to ', folder10
cd, '../'+folder10
@sub_rboxhd
@sub_spectra
Er_folder10=Er

folder11=f3+'_hd.vf2'
print, 'setting folder to ', folder11
cd, '../'+folder11
@sub_rboxhd
@sub_spectra
Er_folder11=Er

folder12=f4+'_hd.vf2'
print, 'setting folder to ', folder12
cd, '../'+folder12
@sub_rboxhd
@sub_spectra
Er_folder12=Er

cd, '../'
print, 'Set spectra DONE.'

;plot, Er_folder1, /xlog, /ylog, xr=[1,150], xstyle=1, ystyle=1, yr=[1.e-9,max(Er_folder1)]

end
