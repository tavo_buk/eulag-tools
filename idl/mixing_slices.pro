if initslice eq !NULL then begin
 print, 'ERROR ... '
 print, 'please set a value for the initial time slice "initslice" '
endif else begin
 
 print, 'creating arrays and computing MLT quantities ...'
 
 ger1=make_array(z_num,t_num)
 ger2=make_array(z_num,t_num)
 ger3=make_array(z_num,t_num)
 for ii=initslice, t_num-1 do begin
  for jj=0, z_num-1 do begin
   ger1[jj,ii] = the_aver[jj,ii]/(the_aver[jj,ii]+theta_strat[jj])  
   ger2[jj,ii] = w2_aver[jj,ii]/((cs[jj])^2.)
   ger3[jj,ii] = fconv[jj,ii]/(rho_strat[jj]*(cs[jj])^3.)
  endfor
 endfor

 print, 'computing normalization coefficients, with method = ', method

 if method eq 2 then begin
  print, 'METHOD NOT AVAILABLE AT THE MOMENT ...'
 ; cof_12=make_array(z_num,t_num)
 ; cof_13=make_array(z_num,t_num)
 ; for ii=initslice, t_num-1 do begin
 ;  for jj=1, z_num-2 do begin
 ;   cof_12[jj,ii] = ger1[jj,ii]/ger2[jj,ii]
 ;   cof_13[jj,ii] = ger1[jj,ii]/ger2[jj,ii]
 ;  endfor
 ; endfor
 ; cof_12_prom=make_array(t_num)
 ; cof_13_prom=make_array(t_num)
 ; for ii=initslice, t_num-1 do begin
 ;  cof_12_prom[ii]=total(cof_12[35:-1,ii],1)/double(z_num-37)
 ;  cof_13_prom[ii]=total(cof_13[35:-1,ii],1)/double(z_num-37)
 ; endfor
 endif

 if method eq 1 then begin
  cof_12_prom=make_array(t_num)
  cof_13_prom=make_array(t_num) 
  ger1_mean=total(ger1[35:-1,*],1)/double(z_num-36)
  ger2_mean=total(ger2[35:-1,*],1)/double(z_num-36)
  ger3_mean=total(ger3[35:-1,*],1)/double(z_num-36)
  for ii=initslice, t_num-1 do begin
   cof_12_prom[ii]=ger1_mean[ii]/ger2_mean[ii]
   cof_13_prom[ii]=ger1_mean[ii]/(ger3_mean[ii]^(2./3.))
  endfor  
 endif

 ;_____________________________________________________________________

 print, 'initializing plots sequence ... '
 for ii=initslice,t_num-1 do begin
  
  if !d.name eq 'PS' then begin
    device,xsize=22,ysize=12,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
  end
  !x.margin=[12,3]
  !p.charsize=2

  !p.multi=[0,1,3] & !p.charsize=2.5
  
  t_array=indgen(ii)
  
  plot, z, ger1[*,ii], xtitle='(z)', thick=3, yr=[min(ger1[*,*]),max(ger1[*,*])];yr=[-0.004,0.04]
  oplot, z, ger2[*,ii]*cof_12_prom[ii], THICK=3, COLOR=160;,  yr=[min(ger2[*,*]),max(ger2[*,*])]
  oplot, z, ((ger3[*,ii])^(2./3.))*cof_13_prom[ii],  THICK=2, color='FF0000'x;,  yr=[min(ger3[*,*]),max(ger3[*,*])]
  oplot, z, make_array(z_num), linestyle=5
  ;_____________________________________________________________________
  plot, z, ger1[*,ii], xtitle='(z)', thick=3, yr=[-0.004,0.02]  
  oplot, z, ger2[*,ii]*cof_12_prom[ii], THICK=3, COLOR=160
  oplot, z, ((ger3[*,ii])^(2./3.))*cof_13_prom[ii],  THICK=2, color='FF0000'x
  oplot, z, make_array(z_num), linestyle=5
  ;_____________________________________________________________________
  plot, t_array, tsth[0:t_array[-1]], xtitle='(time)', thick=3, xr=[0,t_num],  yr=[min(tsth[*]),max(tsth[*,*])]
  wait=1
  print, 'time slice', ii
  
  !p.multi=0
 endfor
endelse

end  
