filename = 'bcprofl.dat'
tth0 = dblarr(m,l)
rrho = dblarr(m,l)
tthe = dblarr(m,l)
uue = dblarr(m,l)
vve = dblarr(m,l)

t = 0L
openr,1,filename,/f77
ns = 0L
while (not eof(1)) do begin
  readu,1,tth0
  readu,1,rrho
  readu,1,tthe
  readu,1,uue
  readu,1,vve
  if (ns LT 1) then begin
    th0 = tth0  
    rho = rrho
    the = tthe
    ue = uue
    ve = vve
    print,'Reading file: ',filename, ns
  endif else begin
    th0 = [[[th0]],[[tth0]]]
    rho = [[[rho]],[[rrho]]]
    the = [[[the]],[[tthe]]]
    ue = [[[ue]],[[uue]]]
    ve = [[[ve]],[[vve]]]
    print,'Reading file: ',filename, ns
  endelse
  wait,0.
  ns ++
end
close,1
