window,0,xsize=700,ysize=300
!x.margin=[7,3] & !y.margin=[5,3]
!P.Color =cgColor("black")
!P.Background =cgColor("white")

iend=(size(aarr))[3]
nl=64
mplane=fltarr(m,l)
lx=5.
ly=5.
lz=1.4

x=indgen(n)*lx/(n-1)
y=indgen(l)*ly/(l-1)
z=indgen(l)*lz/(l-1)

pmin=min(aarr)
pmax=max(aarr)
nlev=64
lev=grange(-0.07,0.01,nlev)

for ii=ibeg,iend-1 do begin
  for kk=1,m-2 do begin
    mplane[*,kk]=0.5*(aarr[*,kk,ii]+aarr[*,kk+1,ii])
  endfor
  mplane[*,l-1]=aarr[*,l-1,ii]
  mplane[*,0]=aarr[*,0,ii]
 contour, smooth(mplane[*,*],1),x,z, /fi, nl=nl,/iso,lev=lev,xstyle=1,ystyle=1,$
          xtitle='x', ytitle='z',charsize=1.5
 wait,0
 print,ii
 png=0
 if (png EQ 1) then begin
     istr2 = strtrim(string(ii,'(I20.4)'),2) ;(only up to 9999 frames)
     print,"writing png",'./img_'+istr2+'.png'
     image = tvrd(true=1)
     imgname = './img_'+istr2+'.png'
     write_png, imgname, image, red, green, blue
 endif
endfor



end

