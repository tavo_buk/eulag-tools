;
; $Id: pc_read_param.pro 14248 2010-07-05 13:51:22Z sven.bingert $
;
;  Read param.nml
;
;  Author: Tony Mee (A.J.Mee@ncl.ac.uk)
;  $Date: 2008-07-22 07:46:24 $
;  $Revision: 1.14 $
;
;  27-nov-02/tony: coded mostly from Wolgang's start.pro
;
;  REQUIRES: external 'nl2idl' perl script (WD)
;  
pro eu_read_param, object=object, dim=dim, datadir=datadir, $
    param2=param2, nodelete=nodelete, print=print, quiet=quiet, help=help
COMPILE_OPT IDL2,HIDDEN
  common pc_precision, zero, one
;
; If no meaningful parameters are given show some help!
;
  if (keyword_set(help)) THEN BEGIN
    print, "Usage: "
    print, ""
    print, "pc_read_param, object=object,"
    print, "               datadir=datadir, proc=proc,"
    print, "               /print, /quiet, /help,"
    print, "               /param2"
    print, ""
    print, "Returns the parameters of a Pencil-Code run."
    print, "Returns zeros and empty in all variables on failure."
    print, ""
    print, "   datadir: specify the root data directory. Default is './data'        [string]"
    print, ""
    print, "   object : optional structure in which to return all the above as tags  [struct]"
    print, ""
    print, "   /param2: for reading param2.nml"
    print, "   /print : instruction to print all variables to standard output"
    print, "   /quiet : instruction not to print any 'helpful' information"
    print, "   /help  : display this usage information, and exit"
    print
    return
  endif
;
; Default parameters.
;
  default, nodelete, 0
  default, quiet, 0
;
; Default data directory.
;
  if (not keyword_set(datadir)) then datadir="."
;
; Build the full path and filename.
;
  if (keyword_set(param2)) then begin
    filename=datadir+'/mylist.nml'
  endif else begin
    filename=datadir+'/mylist.nml'
  endelse
;
; Check that precision is set.
;
    nl2idl_d_opt = '-d'
;
; Check for existence and read the data.
;
  dummy = file_search(filename, count=found)
  if (found gt 0) then begin
    if (not keyword_set(quiet)) then print, 'Reading ' + filename + '...'
    tmpfile = './param.pro'
;
; Write content of mylist.nml to temporary file.
;
    spawn, '$PENCIL_HOME/bin/nl2idl '+nl2idl_d_opt+' -m '+filename+'> ' $
        + tmpfile, result
;
; Save old path.
;
    _path = !path
    if (not running_gdl()) then begin
      !path = datadir+':'
      resolve_routine, 'param', /is_function
    endif
    object = param()
;
; Restore old path.
;
    !path = _path
;
; Delete temporary file.
;
    if (not nodelete) then begin
;      file_delete, tmpfile      ; not in IDL <= 5.3
      spawn, 'rm -f '+tmpfile, /sh
    endif
  endif else begin
    message, 'Warning: cannot find file '+ filename
  endelse
;
; If requested print a summary
;
  if (keyword_set(print)) then begin
    print, 'For GLOBAL calculation domain:'
    print, '    NO SUMMARY INFORMATION CONFIGURED - edit pc_read_param.pro'
  endif
;
end
