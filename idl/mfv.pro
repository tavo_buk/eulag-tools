if !d.name eq 'PS' then begin
    device,xsize=18,ysize=12,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
end
!x.margin=[8,3]
!p.charsize=1.3
!p.multi=[0,2,1]

!p.charsize=1.3
!p.background=cgColor("white")
!p.color=cgColor("black")
default,tt,28.
rsinth=fltarr(l,m)
nt=(size(u))[3]
nth=(size(th))[1]
print,nt
nl=64
year = 365.*24.*60.*60.
time = (nxaver*dt*indgen(nt))/year
; TIME GOOD
good=where(time gt 0)
; ************************
;reading stratification
openr, lun, 'strat.dat', /get_lun
data=fltarr(7,l)
readf, lun, data
rho_s=reform(data(4,*))
close,lun

ng=n_elements(good)
umean=fltarr(l,m)
omega=fltarr(l,m,ng)
tosc=fltarr(l,m,ng)
omega_0=0.;1./(tt*24.*3600)

if (itac eq 0) then begin
 rlev=45
endif else begin
 rlev=61
endelse

vnr=reform(v[*,rlev,good])
vnr_mean = total(v,3)/double(ng)
vp = fltarr(m,l,ng)
for it=0,ng-1 do begin
 vp[*,*,it]=smooth(v[*,*,it],0)-smooth(vnr_mean[*,*],1)
endfor
vp_top = reform(vp[*,rlev,good])

; GRAPHICS

thg=where(th GE -75 AND th LT 75)
;lev_theta = grange(min(vnr[thg,*]),-min(vnr[thg,*]),32)/2.5
lev_theta = grange(min(vp_top[thg,*]),-min(vp_top[thg,*]),32)/3.
cpos=[0.13,0.56,0.85,0.92]
bpos=[0.95,0.56,0.96,0.92]
contour,smooth(rotate(vp_top[thg,*],3),2),time[good],th[thg],/fi,nl=32,lev=lev_theta,$
	ystyle=1,xstyle=1,pos=cpos,xticks=1.,ytitle='!890-!7h!N!6 [!Uo!N]',yticks=2,ytickv=[-50,0,50],$
	xr=[0,30], title='b) model RC02 - !7d!8u!D!7h!N!6 [m s!U-1!N]',xtickname=REPLICATE(' ', 2),xminor=18 
;	xr=[20,40], title='a) CZ02 - !8u!D!7h!N!6 [m s!U-1!N]',xtickname=REPLICATE(' ', 2),xminor=20
bphi=reform(bx[thg,rlev,good])
nlev_field=8
lev=grange(0,0.22,nlev_field)
contour,smooth(rotate(bphi,3),2),time[good],th[thg],nl=nlev_field,/over,lev=lev,$
	thick=1,min_value=-0.0,c_colors=cgColor("black"),c_thick=[2,2]

lev=grange(-0.22,0.0,nlev_field)
contour,smooth(rotate(bphi,3),2),time[good],th[thg],nl=nlev_field,/over,lev=lev,$
	c_linestyle=2,thick=1,max_value=-0.0,c_colors=cgColor("black"),c_thick=[2,2]


colorbar,range=[min(lev_theta),max(lev_theta)],/vertical,xaxis=2,$
         ytickformat='(F6.2)',yticks=4,pos=bpos,color=cgColor("black"),$
         ytickv=[min(lev_theta),0.5*(min(lev_theta)+max(lev_theta)),max(lev_theta)]

thlev=21
vnthn=reform(v[thlev,*,*])
vnth=vnthn
;for k=1, l-2 do begin
; vnth[k,*]=0.5*(vnthn[k,*]+vnthn[k+1,*])
;endfor
;lev_r = grange(min(vnth),-min(vnth),32)/2.

vp_th = reform(vp[thlev,*,good])
vp_th_f = 0.*vp_th

for k=1, l-2 do begin
 vp_th_f[k,*]=0.5*(vp_th[k,*]+vp_th[k+1,*])
endfor

lev_r = grange(min(vp_th_f),-min(vp_th_f),32)/3.

;lev_r = lev_theta
cpos=[0.13,0.13,0.85,0.49]
bpos=[0.95,0.13,0.96,0.49]
contour,smooth(rotate(vp_th_f[*,good],4),2),time[good],r,/fi,nl=32,lev=lev_r,$
	ytitle='!8r/R'+sunsymbol()  ,ystyle=1,xstyle=1,pos=cpos,xtitle='!6time [years]',yticks=4,$
	xr=[0,30]  ;title='!6b) !7dX!8/2!7p!6 [nHz], (!7h!8=20!U!6o!N!6)'
;	xr=[20,40] ;title='!6b) !7dX!8/2!7p!6 [nHz], (!7h!8=20!U!6o!N!6)'
bphi_r=reform(bx[thlev,*,good])

nlev_field=6
lev=grange(0,0.8,nlev_field)
contour,smooth(rotate(bphi_r,4),2),time[good],r,nl=nlev_field,/over,lev=lev,thick=1,min_value=-0.01,$
	c_colors=cgColor("black") ,c_thick=[2,2]
lev=grange(-0.8,0,nlev_field)
contour,smooth(rotate(bphi_r,4),2),time[good],r,nl=nlev_field,/over,lev=lev,c_linestyle=2,thick=1,max_value=0.01,$
	c_colors=cgColor("black") ,c_thick=[2,2]


colorbar,range=[min(lev_r),max(lev_r)],/vertical,xaxis=2,$
         ytickformat='(F6.2)',yticks=4,pos=bpos,color=cgColor("black"),$
         ytickv=[min(lev_r),0.5*(min(lev_r)+max(lev_r)),max(lev_r)]
!p.multi=0
end
