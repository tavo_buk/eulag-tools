slicemax=max(aarr[ibeg:*,*,*])
slicemin=min(aarr[ibeg:*,*,*])
dimss=(size(aarr))
iend=dimss[3]
nl=80

eu_read_param, o=o
default, itac, 0

Lz=o.dz00
Lx=o.dx00
nz=o.l
nx=o.n

zarray=(Lz/nz)*indgen(nz)
xarray=(Lx/nx)*indgen(nx)

xin=0.92
dx=0.02
yin=0.15
dy=0.56

levgeral=grange(slicemin,slicemax,nl)
;setting the same max and min levels for all the slices 
;levgeral=grange(min(aarr[*,*,*]),max(aarr[*,*,*]),nl)
for ii=ibeg,iend-1 do begin
 DEVICE, GET_DECOMPOSED=old_decomposed
 DEVICE, DECOMPOSED=0
 ;setting levels for each slice, they are in general different.
 ;lev=grange(min(aarr[*,*,ii]),max(aarr[*,*,ii]),nl)
 if extension eq 'yz' then begin
  contour, aarr[*,*,ii], /fi, nl=nl, lev=levgeral
 endif
 if extension eq 'xy' then begin
  contour, aarr[*,*,ii], /fi, nl=nl,/iso,lev=levgeral
 endif
 if extension eq 'xz' then begin
  contour, aarr[*,*,ii], xarray, zarray,/iso, /fi, nl=nl,lev=levgeral
; colorbar, range=[slicemin,slicemax], /horizontal, pos=[0.77,0.1,0.87,0.95], xtickformat='(F6.2)', xaxis=2, charsize=1.5
  colorbar, range=[slicemin,slicemax], /vertical, pos=[xin,yin,xin+dx,yin+dy], ytickformat='(F6.2)', yaxis=2, charsize=1.5
 endif
 ;window, 1, xsize=300, ysize=300
 ;tv, image
 ; saving image in a png file
 istr = strtrim(string(ii,'(I20.4)'),2)
 if field eq 'bz' then begin
  imagename = './img_'+istr+'_bz'+'.png'
 endif else begin
  imagename = './img_'+istr+'.png'
 endelse
 print, "writing png ", imagename
 WRITE_PNG, imagename, TVRD(/TRUE), red, green, blue   
 wait,0
endfor

end
