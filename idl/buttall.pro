if !d.name eq 'PS' then begin
    device,xsize=12,ysize=12,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
end
!p.multi=[0,1,4] & !p.charsize=2.
nl=64

dir_base='/nobackupp8/gaguerre/eumag/NOTAC/'
dir1=dir_base+'nt-14-rf/'
dir2=dir_base+'test_abs-rf/'
dir3=dir_base+'nt-56-rf/'

restore,dir1+'bfield.sav'
lev = 2.*max(abs(bphi))*indgen(nl)/float(nl-1) $
        -max(abs(bphi))
cpos=[0.13,0.72,0.85,0.95]
bpos=[0.95,0.72,0.96,0.95]
;
good=where(time ge 20)
bmax=max(abs(bphi[*,good]))
print,'bphi_max=',bmax
lev=grange(-bmax,bmax,nl)
;
contour,rotate(bphi,3),time,(th),/fi,nl=64,ytitle='!890-!7h!N!6 [!Uo!N]',$
        ystyle=1,lev=lev,pos=cpos,xstyle=1,xrange=[20,40]
colorbar,range=[min(lev),max(lev)],/vertical,xaxis=2,$
         ytickformat='(F6.2)',yticks=4,pos=bpos,charsize=1.5,$
         ytickv=[min(lev),0.5*(min(lev)+max(lev)),max(lev)]
xyouts,21.,50,'!6a) CZ01',charsize=1.1,charthick=3

restore,dir2+'bfield.sav'
cpos=[0.13,0.42,0.85,0.65]
bpos=[0.95,0.42,0.96,0.65]
lev = 2.*max(abs(bphi))*indgen(nl)/float(nl-1) $
        -max(abs(bphi))
lev=grange(-max(bphi),max(bphi),nl)
good=where(time ge 20)
bmax=max(abs(bphi[*,good]))
print,'bphi_max=',bmax
lev=grange(-bmax,bmax,nl)
contour,rotate(bphi,3),time,th,/fi,nl=64,ytitle='!890-!7h!N!6 [!Uo!N]',$
        ystyle=1,lev=lev,xstyle=1,pos=cpos,xrange=[20,40]
colorbar,range=[min(lev),max(lev)],/vertical,xaxis=2,$
         ytickformat='(F6.2)',yticks=4,pos=bpos,charsize=1.5,$
         ytickv=[min(lev),0.5*(min(lev)+max(lev)),max(lev)]
xyouts,21.,60,'!6b) CZ02',charsize=1.1,charthick=3


restore,dir3+'bfield.sav'
cpos=[0.13,0.12,0.85,0.35]
bpos=[0.95,0.12,0.96,0.35]
bphi=bphi
lev = 2.*max(abs(bphi[5:59,*]))*indgen(nl)/float(nl-1) $
        -max(abs(bphi[5:59,*]))
good=where(time ge 70)
bmax=max(abs(bphi[*,good]))
print,'bphi_max=',bmax
lev=grange(-bmax,bmax,nl)
contour,rotate(bphi,3),time,th,/fi,nl=64,ytitle='!890-!7h!N!6 [!Uo!N]' ,$
        xtitle='time [years]!6',ystyle=1,lev=lev,xstyle=1,pos=cpos,xrange=[70,175]
colorbar,range=[min(lev),max(lev)],/vertical,xaxis=2,$
         ytickformat='(F6.2)',yticks=4,pos=bpos,charsize=1.5,$
         ytickv=[min(lev),0.5*(min(lev)+max(lev)),max(lev)]
xyouts,75.,50,'!6c) CZ03',charsize=1.1,charthick=3,color=0

!p.multi=0
end
