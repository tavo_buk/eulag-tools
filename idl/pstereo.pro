;mv idl.ps ~/Dropbox/tex/SPD/figs/sp3.eps
window,1,xsize=480,ysize=640
device,decomposed=0
loadct,33
!p.background=0
!p.color=255
default,ti,1

if !d.name eq 'PS' then begin
    device,xsize=15,ysize=15,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
end

ffi='!8u!Dr!N!6'
;ffi="!7H!6`"

pos_ext=[0.07,0.24,0.93,0.92]
pos_int=[0.2,0.24,0.8,0.84]
restore,'uu.sav'

nthg=(size(thg))[1]
residual=fltarr(n,nthg)

for i=thg[0], thg[nthg-2] do begin
 print,i
 residual[0:n-1,i]=aarr[0:n-1,i,ti]-uu[42,i]
endfor

map_set, 20, 0, color=0, /ORTHOGRAPHIC,/isotropic,E_HORIZON={FILL:1, COLOR:0},$
	 glinestyle=0, glinethick=4, /noborder,position=pos_ext,/noerase,latdel=40
contour,smooth(residual[*,thg],1),ph,th[thg],/fi,lev=lev, /over
map_set, 20, 0, color=255, /ORTHOGRAPHIC,/isotropic, /noerase, latdel=30,londel=60,$
	 glinestyle=0, glinethick=1, /noborder,/grid,position=pos_ext
colorbar, pos=[0.2,0.09,0.8,0.12], div=4, range=[min(lev),max(lev)], /horizontal, $
          format='(F8.2)', charsize=1.2, $
          title='!8'+ffi+'!6 (!8r=!6'+fr+'!8R!6)'

end
