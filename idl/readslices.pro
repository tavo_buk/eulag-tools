function readslices,d1,d2,unit,str
  one = 1.D0
  f8 = dblarr(d1,d2)
  f = fltarr(d1,d2)
  readu,unit,f8
  for j=0,d2-1 do begin
     for i=0,d1-1 do begin
        f[i,j] = f8[i,j]
     endfor
  endfor
  print,str+'=',f(d1/2,d2/2), d1/2, d2/2
  return, f
end
