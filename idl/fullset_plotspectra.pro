names=['k!U-5/3!N','k!U-3!N']
xpos=[100,30]
ypos=[2e-7,4e-6]

intercept1=4e-4
intercept2=8e-2
yrmax=max(Er_folder1)
yrmin=1.e-8

; PLOT OF CF DATA

set_plot, 'ps'
device, filename=set+'cf_spectra.eps', /color

;loadct, 4
loadct, 13

!p.color=8
!x.margin=[9,3]
!p.charsize=1.5

plot, Er_folder1, /xlog, /ylog,xr=[2,150], xtitle='Wavenumber k',ytitle='E(k)',xstyle=1,yr=[yrmin,yrmax]
oplot, Er_folder2, color=68
oplot, Er_folder3, color=170
oplot, Er_folder4, color='FF0000'x
oplot, intercept1*ks^(-5./3.),linestyle=2;, color=160
oplot, intercept2*ks^(-3.), linestyle=2;, color='FF0000'x
for ii=0,1 do xyouts, xpos[ii], ypos[ii], names[ii]

device, /close

;PLOT OF VF DATA

set_plot, 'ps'
device, filename=set+'vf_spectra.eps', /color

;loadct, 4
loadct, 13

!p.color=8
!x.margin=[9,3]
!p.charsize=1.5

plot, Er_folder5, /xlog, /ylog,xr=[2,150], xtitle='Wavenumber k',ytitle='E(k)',xstyle=1,yr=[yrmin,yrmax]
oplot, Er_folder6, color=68
oplot, Er_folder7, color=170
oplot, Er_folder8, color='FF0000'x
oplot, intercept1*ks^(-5./3.),linestyle=2;, color=160
oplot, intercept2*ks^(-3.), linestyle=2;, color='FF0000'x
for ii=0,1 do xyouts, xpos[ii], ypos[ii], names[ii]

device, /close

;PLOT OF VF2 DATA

set_plot, 'ps'
device, filename=set+'vf2_spectra.eps', /color

;loadct, 4
loadct, 13

!p.color=8
!x.margin=[9,3]
!p.charsize=1.5

plot, Er_folder9, /xlog, /ylog,xr=[2,150], xtitle='Wavenumber k',ytitle='E(k)',xstyle=1,yr=[yrmin,yrmax]
oplot, Er_folder10, color=68
oplot, Er_folder11, color=170
oplot, Er_folder12, color='FF0000'x
oplot, intercept1*ks^(-5./3.),linestyle=2;, color=160
oplot, intercept2*ks^(-3.), linestyle=2;, color='FF0000'x
for ii=0,1 do xyouts, xpos[ii], ypos[ii], names[ii]

device, /close

end
