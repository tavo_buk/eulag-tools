eu_read_param,o=o                                                                                                           
default, itac, 0
 
n = o.n
m = o.m
l = o.l
nt = o.nt
dt = o.dt00
nslice = o.nslice
nxaver = o.nxaver
nplot=o.nplot
year=365.*24.*3600. ; seg
 
theta = indgen(m)*!pi/(m-1)
phi = indgen(n)*2.*!pi/(n-1)
rr = 6.97e8  ; Solar radii in [m/s]

if (itac eq 1) then begin
 rds = 0.61*rr ; base of the convection zone in [m/s]
endif else begin
 rds = 0.718*rr ; base of the convection zone in [m/s]
endelse
r = (rds + indgen(l)*o.dz00)/rr
rint = ((rds+o.dz00) + indgen(l)*o.dz00)/rr

px=r#cos(phi)
py=r#sin(phi)

th = 180.*theta/!pi - 90.
ph = 180.*phi/!pi -180.

; defining stratification
openr, lun, 'strat.dat', /get_lun
rows=l
data=fltarr(7,rows)
readf, lun, data
;setting variable vectors ...
radius = reform(data[0,*])
t_am = reform(data[2,*])
rho_am = reform(data[3,*])
rho_ad = reform(data[4,*])
rho_av=mean(rho_ad)
the = reform(data[5,*])
close,lun
x = radius#sin(theta)
y = radius#cos(theta)
;save,r,th,ph,filename='grid.sav'
;pressure
Rg = 13732.
Cp = 2.5*Rg
gamm = Cp/(Cp - Rg)
pad_bottom = 1.7788e13
pad = Pad_bottom*(rho_ad/rho_ad[0])^gamm

end
