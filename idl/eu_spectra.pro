nx=(size(ph))[1]
ny=(size(th))[1]
nz=(size(r))[1]


vx = fltarr(nx,ny,nt)
vy = fltarr(nx,ny,nt)
vz = fltarr(nx,ny,nt) 
vtx = fltarr(nx,ny)
vty = fltarr(nx,ny)
vtz = fltarr(nx,ny) 
re_vtx = fltarr(nx,ny)
re_vty = fltarr(nx,ny)
re_vtz = fltarr(nx,ny) 
im_vtx = fltarr(nx,ny)
im_vty = fltarr(nx,ny)
im_vtz = fltarr(nx,ny) 
sp = fltarr(nx/2,nx/2)
mean_sp = fltarr(nx/2,nt-1)

m=0L
Lx=max(ph*!pi/180.)
Ly=max(th*!pi/180.)
kx=shift(-(nx)/2+indgen(nx),+(nx+1)/2)
ky=shift(-(ny)/2+indgen(ny),+(ny+1)/2)
ks=indgen(nx/2)
nk=fix(sqrt(((nx))^2)/2)
spectrum=fltarr(nk,nt)
;nk=nx/2
ki=intarr(nk)
initt=1

psp = 0.
irg = where(r ge 0.73)
sirg = (size(irg))[1]

;for alt=irg[0], nz -3 do begin
for alt=48,48 do begin
 print,alt
 
; vx=reform(u[*,*,alt,*])
; vy=reform(v[*,*,alt,*])
; vz=reform(w[*,*,alt,*])
 vx=0*reform(bx[*,*,alt,*])
 vy=reform(by[*,*,alt,*])
 vz=0.*reform(bz[*,*,alt,*])

 for in=12,18 do begin
  for iky=0,ny-1 do begin
   vtx[*,iky] = fft(vx[*,iky,in],/double)
   vty[*,iky] = fft(vy[*,iky,in],/double)
   vtz[*,iky] = fft(vz[*,iky,in],/double)

   re_vtx[*,iky]=real_part(vtx[*,iky])
   re_vty[*,iky]=real_part(vty[*,iky])
   re_vtz[*,iky]=real_part(vtz[*,iky])

   im_vtx[*,iky]=imaginary(vtx[*,iky])
   im_vty[*,iky]=imaginary(vty[*,iky])
   im_vtz[*,iky]=imaginary(vtz[*,iky])
  endfor

  it=0D
  for iky=0,ny-1 do begin 
;iky=ny/2
   for ikx=0,nx-1 do begin
;   k=round(sqrt(kx[ikx]^2+ky[iky]^2))
    k=round(sqrt(kx[ikx]^2))
    if (k lt n_elements(ks)) then begin
     ki[k]=k
     prods=0.5*((re_vtx[ikx,iky]^2+im_vtx[ikx,iky]^2)+$
	       (re_vty[ikx,iky]^2+im_vty[ikx,iky]^2)+$
 	       (re_vtz[ikx,iky]^2+im_vtz[ikx,iky]^2))
     spectrum[k,in]=spectrum[k,in]+prods
    endif
    it++	
   endfor
  endfor

 endfor

 psp+=total(spectrum,2)/(nt-initt)
 plot,ki,psp,/xlog,/ylog,xr=[1,400],xtitle="m",ytitle="E!dK!n(k)",xstyle=1
endfor
;psp=rh[alt]*total(mean_sp,2)/(nt-1)
;psp=total(spectrum,2)/nt
save,filename='power_spectra.sav',psp,ki

; line with slope -5/3 
;plot,ki,psp/double(sirg),/xlog,/ylog,xr=[1,110],xtitle="m",ytitle="E!dK!n(k)",xstyle=1,charsize=1.5,yrange=[200,10000],ystyle=1
plot,ki,psp/2.,/xlog,/ylog,xr=[1,110],xtitle="m",ytitle="E!dK!n(k)",xstyle=1,charsize=1.5,yrange=[5,100],ystyle=1
y = 0.95e5*ki^(-5./3.) 
oplot,ki,y,linestyle=2

end
