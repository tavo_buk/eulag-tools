; mv idl.ps ~/Dropbox/Tex/paper.nssl/fig/theprof.eps
if !d.name eq 'PS' then begin
    device,xsize=18,ysize=12,yoffset=3
    !p.thick=2 & !x.thick=1 & !y.thick=1
end

@eu_read_bcprofl
!p.charsize=1.7
;!x.margin=[7,4]
thmax=max(the[*,*,0])
ll=(size(r))[1]
print,ll

mt=1e6
print,mt
the1=reform(the[32,*,0])
yr=[min(the1/mt),max(the1/mt)+0.05] 
;yr=[max(the1/mt)-0.00002,max(the1/mt)+0.000001] 

plot,[0.6,1],[yr[0],yr[0]],xr=[0.6,1],li=4, /nodata, charsize=1.3,yr=yr, $
     ystyle=1,ytickformat='(F8.1)',ytitle='!7h!6!De!N (!810!U6!N!6 K)',$
     xtitle='!8r/R!6'+sunsymbol()

oplot,r,the1/mt,thick=3
;oplot,r,th_ad01/mt,li=3,thick=3
oplot,[0.96,0.96],[min(the)-4,max(the)+50000],li=2
oplot,[0.96,0.96],[yr[0],yr[1]],li=2,thick=1
;oplot,[0.718,0.718],[yr[0],yr[1]],li=2,thick=1

plot,r,the1/mt,/noerase,yr=[max(the1/mt)-0.000015,max(the1/mt)+0.000001],$
     pos=[0.48,.25,0.82,.65],xr=[0.7,0.96],xstyle=1,charsize=1.2,ystyle=1,thick=3,$
     xticks=2,xtickv=[0.75,0.85,0.95]


the2=transpose(reform(the[*,*,0]))
;contour,the2,x,y,/fi,nl=64,/is

print,'\Delta\theta = ',(max(the1)-the1[ll-1])
save,the1,filename='the.sav'

end
