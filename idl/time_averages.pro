;-------------------------------------------------
; TIME AVERAGES
; 
; To compute the averages in time we take the 
; last "nslices" slices up to the slice "topslice"
; for all the averages  
;-------------------------------------------------
;if g eq !Null then begin
; print, 'ERROR... please set the value for g'
;endif else begin
 @fluxes2
 ;________________________________
 nslices=15
 topslice=n_elements(the[1,1,*])-1
 ;________________________________
 print, 'averages are calculated within the range [topslice,topslice-nslices]'
; if nslices eq !Null then begin
;  print, 'ERROR ...'
;  print, 'please set a value for nslices '
; endif else begin
;  print, 'current value of nslices = ', nslices
;  if topslice eq !NULL then begin
;   print, 'ERRROR ...'
;   print, 'please choose a topslice to compute the averages'
;  endif else begin
;   print, 'current value of topslice = ', topslice
;   print, '________________________________________________________________'  
   tmin=(topslice-1)-nslices 
 
   ; --> arrays of the type array[z]
   the_prom=total(the_aver[*,tmin:topslice-1],2)/double(nslices)
   w2_prom=total(w2_aver[*,tmin:topslice-1],2)/double(nslices)
   thew_prom=total(thew_aver[*,tmin:topslice-1],2)/double(nslices)
   p_prom=total(p_aver[*,tmin:topslice-1],2)/double(nslices)  
 
   print, 'time averaged variables ( w[z] ) computed:'
   print, 'the_prom'
   print, 'w2_prom'
   print, 'thew_prom'
   print, 'p_prom'
   ;-------------------------------------------------
   ;COMPUTE SPATIO-TEMPORAL AVERAGED FLUXES.
   ;-------------------------------------------------

   fconv_prom=total(fconv[*,tmin:topslice-1],2)/double(nslices)
   ;fkin_prom=total(fkin[*,tmin:topslice-1],2)/double(nslices)
   print, 'fconv_prom'
   print, '________________________________________________________________'
   
;  endelse
; endelse
;endelse
end
