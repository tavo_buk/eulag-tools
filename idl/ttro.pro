; make .r omega first
if !d.name eq 'PS' then begin
    device,xsize=18,ysize=12,yoffset=3
    !p.thick=2 & !x.thick=1 & !y.thick=1
end
!p.charsize=1.3
!x.margin=[9,4]
@eu_read_bcprofl
;@rhoprof

default,nn,10
ntimes = (size(u))[3]
good = (ntimes-nn)+indgen(nn)-1
print,'doing averages over',nn, ' xaver records ...'

quu=fltarr(l,m)
qvv=fltarr(l,m)
qww=fltarr(l,m)
urms2=fltarr(l,m)

; reynolds stresses
for k=0, l-1 do begin
  for j=0,m-1 do begin
    quu[k,j]=rho[j,k]*total(u2[j,k,good]- u[j,k,good]^2)/float((size(good))[1])
    qvv[k,j]=rho[j,k]*total(v2[j,k,good]- v[j,k,good]^2)/float((size(good))[1])
    qww[k,j]=rho[j,k]*total(w2[j,k,good]- w[j,k,good]^2)/float((size(good))[1])
;    urms2[k,j]=(quu[k,j]+qvv[k,j]+qww[k,j])/rho[j,k]
    urms2[k,j]=(qvv[k,j]+qww[k,j])/rho[j,k]
  endfor
endfor

urms1d=sqrt(total(urms2,2)/64.)
omega=2.*!pi/(7.*24.*3600.)
Ro1d=urms1d/(2.*omega*0.5*rsol)
plot,r,Ro1d,xrange=[0,1],xstyle=1,xtitle='!8r !6[Rs]',ytitle='!6Ro',/ylog,thick=4,title='tt02 1.1 Myr'

save,filename='urms.sav',urms1d

end
