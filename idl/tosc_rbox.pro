
if !d.name eq 'PS' then begin
    device,xsize=18,ysize=12,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
end

default,imhd,1
default,nn,300
nt=(size(time))[1]
rsinth=fltarr(m,l)

for k=0,l-1 do begin
 for j=0,m-1 do begin
  rsinth[j,k]=rr*r[k]*sin(theta[j])
 endfor
endfor

default,cycr,28.
omega_0=2.*!pi/(cycr*24.*3600.)
omega=fltarr(m,l,nt)
omega_mean=fltarr(m,l)
tosc=fltarr(m,l,nt)
umeanm=total(umean,3)/double(nt)

for k=0, l-1 do begin
  for j=1,m-2 do begin
   omega[j,k,*]=1.e9*(omega_0 + umean[j,k,*]/(2.*!pi*rsinth[j,k]))
  endfor
endfor
omega_mean[1:m-2,*]=1.e9*(omega_0+umeanm[1:m-2,*]/(2.*!pi*rsinth[1:m-2,*]))

for it=0,nt-1 do begin
 tosc[*,*,it]=omega[*,*,it]-omega_mean[*,*]
endfor
rlev=62

thg=where(th GE -80 AND th LT 80)
lev_theta = grange(min(tosc[thg,rlev,*]),-min(tosc[thg,rlev,*]),64)/1.5
cpos=[0.13,0.56,0.85,0.92]
bpos=[0.95,0.56,0.96,0.92]
contour,(rotate(reform(tosc[thg,rlev,*]),3)),time,th[thg],/fi,nl=64,pos=cpos,$
	ystyle=1,xstyle=1,ytitle='!890-!7h!N!6 [!Uo!N]',yticks=2,ytickv=[-50,0,50],$
	xr=[0,max(time)], title='b) RC02 - !7dX/!82!7p!6 [nHz]',xminor=18,lev=lev_theta
colorbar,range=[min(lev_theta),max(lev_theta)],/vertical,xaxis=2,$
         ytickformat='(F6.0)',yticks=4,pos=bpos,$
         ytickv=[min(lev_theta),0.5*(min(lev_theta)+max(lev_theta)),max(lev_theta)]
oplot,[3.5,3.5],[-90,90],li=2,thick=6,color=cgcolor('black') 
oplot,[19.5,19.5],[-90,90],li=2,thick=6,color=cgcolor('black') 
oplot,[11.5,11.5],[-90,90],li=2,thick=6,color=cgcolor('black') 
oplot,[7.5,7.5],[-90,90],li=2,thick=6,color=cgcolor('black') 
oplot,[15.5,15.5],[-90,90],li=2,thick=6,color=cgcolor('black') 

oplot,[46,46],[-90,90],li=2,thick=6,color=cgcolor('black') 
oplot,[51,51],[-90,90],li=2,thick=6,color=cgcolor('black') 

oplot,[21,21],[-90,90],li=1,thick=6,color=cgcolor('black') 
oplot,[26,26],[-90,90],li=1,thick=6,color=cgcolor('black') 

oplot,[56,56],[-90,90],li=1,thick=6,color=cgcolor('black') 
oplot,[61,61],[-90,90],li=1,thick=6,color=cgcolor('black') 
end
