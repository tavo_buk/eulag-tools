aarrdims=size(aarr)
slice=aarrdims[3]-1

print, 'contour plot set to the temporal slice', slice

slicemax=max(aarr[*,*,slice])
slicemin=min(aarr[*,*,slice])
nl=80

eu_read_param, o=o
default, itac, 0

Lz=o.dz00
Lx=o.dx00
nz=o.l
nx=o.n

zarray=(Lz/nz)*indgen(nz)
xarray=(Lx/nx)*indgen(nx)

print, 'x dimension length', Lx
print, 'z dimension length', Lz

lev=grange(slicemin,slicemax,nl)
contour, aarr[*,*,slice], xarray, zarray,/iso, /fi, nl=nl
;contour, aarr[*,*,tt], xarray, zarray, /iso, /fi, nl=nl
;colorbar, range=[slicemin,slicemax], /horizontal, pos=[0.77,0.1,0.87,0.95], xtickformat='(F6.3)', xaxis=2, charsize=1.5

xin=0.92
dx=0.02
yin=0.15
dy=0.4

colorbar, range=[slicemin,slicemax], /vertical, pos=[xin,yin,xin+dx,yin+dy], ytickformat='(F6.2)', yaxis=2, charsize=1.5

end
