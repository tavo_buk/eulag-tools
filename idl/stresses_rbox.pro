
; ************************
;reading stratification
openr, lun, 'strat.dat', /get_lun
data=fltarr(8,l)
readf, lun, data
rho_s=reform(data(5,*))
close,lun

default,cycr,28.
default,nn,50
default,ivav,0
default,imhd,1
nt=(size(u))[4]
time=nplot*dt*indgen(nt)/year

print,'averaging over phi', n, ' azimuthal grid points'
umean=total(u,1)/double(n)
vmean=total(v,1)/double(n)
wmean=total(w,1)/double(n)
if (imhd gt 0) then begin
 bxmean=total(bx,1)/double(n)
 bymean=total(by,1)/double(n)
 bzmean=total(bz,1)/double(n)

 bxturb=fltarr(n,m,l,nt)
 byturb=fltarr(n,m,l,nt)
 bzturb=fltarr(n,m,l,nt)

 for it = 0,nt-1 do begin
  for i=0,n-1 do begin
   bxturb[i,*,*,it]=bx[i,*,*,it]-bxmean[*,*,it]
   byturb[i,*,*,it]=by[i,*,*,it]-bymean[*,*,it]
   bzturb[i,*,*,it]=bz[i,*,*,it]-bzmean[*,*,it]
  endfor
 endfor
endif

rsinth=fltarr(m,l)
uframe=fltarr(m,l)
uturb=fltarr(n,m,l,nt)
vturb=fltarr(n,m,l,nt)
wturb=fltarr(n,m,l,nt)

mu0 = 4.*!pi*1e-7 
omega_0=2.*!pi/(cycr*24.*3600.)

for k=0,l-1 do begin
 for j=0,m-1 do begin
  rsinth[j,k]=rr*r[k]*sin(theta[j])
  uframe[j,k]= omega_0*rsinth[j,k]
 endfor
endfor

for it = 0,nt-1 do begin
 for i=0,n-1 do begin
  uturb[i,*,*,it]=u[i,*,*,it]-umean[*,*,it]
  vturb[i,*,*,it]=v[i,*,*,it]-vmean[*,*,it]
  wturb[i,*,*,it]=w[i,*,*,it]-wmean[*,*,it]
 endfor
endfor

frRS=fltarr(m,l,nt)
frMC=fltarr(m,l,nt)
fthRS=fltarr(m,l,nt)
fthMC=fltarr(m,l,nt)
;reynold stresses
quw=uturb*wturb
quv=uturb*vturb
if (imhd gt 0) then begin
 frMT=fltarr(m,l,nt)
 frMS=fltarr(m,l,nt)
 fthMT=fltarr(m,l,nt)
 fthMS=fltarr(m,l,nt)
;maxwell stresses
 qbxby=bxturb*byturb
 qbxbz=bxturb*bzturb
endif

for it=0,nt-1 do begin
 for k=0,l-1 do begin
  frRS[*,k,it] =rho_s[k]*rsinth[*,k]*total(reform(quw[*,*,k,it]),1)/double(n)
  fthRS[*,k,it]=rho_s[k]*rsinth[*,k]*total(reform(quv[*,*,k,it]),1)/double(n)
  frMC[*,k,it] =rho_s[k]*rsinth[*,k]*(umean[*,k,it]+uframe[*,k])*reform(wmean[*,k,it])
  fthMC[*,k,it]=rho_s[k]*rsinth[*,k]*(umean[*,k,it]+uframe[*,k])*reform(vmean[*,k,it])
  if (imhd gt 0) then begin
   frMS[*,k,it] =-(rsinth[*,k]/mu0)*total(reform(qbxbz[*,*,k,it]),1)/double(n)
   fthMS[*,k,it]=-(rsinth[*,k]/mu0)*total(reform(qbxby[*,*,k,it]),1)/double(n)
   frMT[*,k,it] =-(rsinth[*,k]/mu0)*bzmean[*,k,it]*bxmean[*,k,it]
   fthMT[*,k,it]=-(rsinth[*,k]/mu0)*bymean[*,k,it]*bxmean[*,k,it]
  endif
 endfor
endfor

print,'averaging over ', nn, ' times'
frRSm =total(frRS[*,*,nt-nn:*],3)/double(nn)
fthRSm=total(fthRS[*,*,nt-nn:*],3)/double(nn)
frMCm =total(frMC[*,*,nt-nn:*],3)/double(nn)
fthMCm=total(fthMC[*,*,nt-nn:*],3)/double(nn)
if (imhd gt 0) then begin
 frMSm=total(frMS[*,*,nt-nn:*],3)/double(nn)
 fthMSm=total(fthMS[*,*,nt-nn:*],3)/double(nn)
 frMTm=total(frMT[*,*,nt-nn:*],3)/double(nn)
 fthMTm=total(fthMT[*,*,nt-nn:*],3)/double(nn)
endif

if (ivav eq 0) then begin 
 print,'not computing volume integrals, saving data'
 save,filename='stresses.sav',time,umean,vmean,wmean,frrs,fthrs,frmc,fthmc,frmt,fthmt,frms,fthms 
endif else begin
; ivav: volume average
 r2s_frMC=fltarr(m,l,nt)
 r2s_frRS=fltarr(m,l,nt)
 int_frMC=fltarr(l,nt)
 int_frRS=fltarr(l,nt)
 
 if (imhd gt 0) then begin
  r2s_frMT=fltarr(m,l,nt)
  r2s_frMS=fltarr(m,l,nt)
  int_frMT=fltarr(l,nt)
  int_frMS=fltarr(l,nt)
 
  rs_fthMT=fltarr(m,l,nt)
  rs_fthMS=fltarr(m,l,nt)
  int_fthMT=fltarr(m,nt)
  int_fthMS=fltarr(m,nt)
 endif
 
 rs_fthMC=fltarr(m,l,nt)
 rs_fthRS=fltarr(m,l,nt)
 int_fthMC=fltarr(m,nt)
 int_fthRS=fltarr(m,nt)
 
 
 for j=0,m-1 do begin
  for k=0, l-1 do begin
     ; quantities to integrate
     r2s_frRS[j,k,*] = rr*r[k]*rsinth[j,k]*frRS[j,k,*]
     r2s_frMC[j,k,*] = rr*r[k]*rsinth[j,k]*frMC[j,k,*]
     rs_fthRS[j,k,*] = rsinth[j,k]*fthRS[j,k,*]
     rs_fthMC[j,k,*] = rsinth[j,k]*fthMC[j,k,*]
     if (imhd gt 0) then begin
      r2s_frMT[j,k,*] = rr*r[k]*rsinth[j,k]*frMT[j,k,*]
      r2s_frMS[j,k,*] = rr*r[k]*rsinth[j,k]*frMS[j,k,*]
      rs_fthMT[j,k,*] = rsinth[j,k]*fthMT[j,k,*]
      rs_fthMS[j,k,*] = rsinth[j,k]*fthMS[j,k,*]
     endif
   endfor
 endfor
 
 ; Integrated fluxes
 for it=0,nt-1 do begin
  for k = 0,l-1 do begin 
   int_frRS[k,it]=int_tabulated(theta,reform(r2s_frRS[*,k,it]))
   int_frMC[k,it]=int_tabulated(theta,reform(r2s_frMC[*,k,it]),/double)
   if (imhd gt 0) then begin
    int_frMT[k,it]=int_tabulated(theta,reform(r2s_frMT[*,k,it]),/double)
    int_frMS[k,it]=int_tabulated(theta,reform(r2s_frMS[*,k,it]),/double)
   endif
  endfor
 endfor
 for it=0,nt-1 do begin
  for j = 0,m-1 do begin 
   int_fthRS[j,it]=int_tabulated(rr*r,reform(rs_fthRS[j,*,it]),/double)
   int_fthMC[j,it]=int_tabulated(rr*r,reform(rs_fthMC[j,*,it]),/double)
   if (imhd gt 0) then begin
    int_fthMT[j,it]=int_tabulated(rr*r,reform(rs_fthMT[j,*,it]),/double)
    int_fthMS[j,it]=int_tabulated(rr*r,reform(rs_fthMS[j,*,it]),/double)
   endif
  endfor
 endfor
 
 int_frRSm=total(int_frRS[*,nt-nn:*],2)/double(nn)
 int_fthRSm=total(int_fthRS[*,nt-nn:*],2)/double(nn)
 int_frMCm=total(int_frMC[*,nt-nn:*],2)/double(nn)
 int_fthMCm=total(int_fthMC[*,nt-nn:*],2)/double(nn)
 if (imhd gt 0) then begin
  int_frMSm=total(int_frMS[*,nt-nn:*],2)/double(nn)
  int_fthMSm=total(int_fthMS[*,nt-nn:*],2)/double(nn)
  int_frMTm=total(int_frMT[*,nt-nn:*],2)/double(nn)
  int_fthMTm=total(int_fthMT[*,nt-nn:*],2)/double(nn)
 endif
endelse  ; (ivav)

end
