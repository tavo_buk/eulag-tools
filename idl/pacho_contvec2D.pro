if !d.name eq 'PS' then begin
    device,xsize=17,ysize=12,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
end

;__________________________________________
; from rboxhd_f .....
;__________________________________________
field='bz'

if field eq 'bz' then begin
 uref=reform(bx[*,0,*,*])
 wref=reform(bz[*,0,*,*])
endif else begin
 uref=reform(u[*,0,*,*])
 wref=reform(w[*,0,*,*])
endelse

;setting last temporal slice for the plot
dims=size(uref)
;slice=dims[3]-1
;slice=1001

;Setting field for the contour plot
cfield=wref[*,*,slice];

;!x.margin=[5,3]
;!p.charsize=1.5

;__________________________________________
; Preferences for contour plot
;__________________________________________

eu_read_param, o=o
default, itac, 0

Lz=o.dz00
Lx=o.dx00
nz=o.l
nx=o.n

zarray=(Lz/nz)*indgen(nz)
xarray=(Lx/nx)*indgen(nx)

print, 'x dimension length', Lx
print, 'z dimension length', Lz

nl=80
slicemin=min(cfield)
slicemax=max(cfield)

print, 'number of levels in the plot: ', nl
print, 'minimum of the field plotted: ', slicemin
print, 'maximum of the field plotted: ', slicemax

if field eq 'w' then begin
 ;window,0,retain=2
 ;device,decomposed=0
 loadct,3
endif else begin
 ;window,0,retain=2
 ;device,decomposed=0
 loadct,5
endelse

;lev=grange(slicemin,slicemax,nl)
lev=grange(-1.33,1.9,nl)
contour, cfield, xarray, zarray,/iso, /fi, nl=nl, lev=lev, xstyle=1, ystyle=1
xin=0.92
dx=0.02
yin=0.12
dy=0.25

if field eq 'bz' then begin
 stringfield='!8B!Dz!N!X'
endif else begin
 stringfield='!8U!Dz!N!X'
endelse

xstrpos=4.6
ystrpos=1.23
xyouts, xstrpos, ystrpos, stringfield, charsize=1.7, color=0
colorbar, range=[min(cfield),max(cfield)], /vertical, pos=[xin,yin,xin+dx,yin+dy], ytickformat='(F6.2)', yaxis=2, charsize=1.0, color=0

;__________________________________________
; Preferences for vector plot
;__________________________________________

zcut0=0
botx=3
topx=nx-20
posx=xarray[botx:topx]
;frac=0.12
;len=0.035


for ii=0, nz-1  do begin
 zcut=zcut0+ii
 posz=fltarr(topx+1-botx)+zarray[zcut]
 velx=uref[botx:topx,zcut,slice]
 velz=wref[botx:topx,zcut,slice]

;         POSX=RANDOMU(seed,200)
;         POSY=RANDOMU(seed,200)
;         VELX=RANDOMU(seed,200)-0.5
;         VELY=RANDOMU(seed,200)-0.5
;         magnitude = SQRT(velx^2 + vely^2)
;         LOADCT, 5, NCOLORS=254, BOTTOM=1 ; Load vector colors
;         TVLCT, 0, 255, 0, 255 ; Plot in GREEN
;         colors = BytScl(magnitude, Top=254) + 1B
;         PARTVELVEC, VELX, VELY, POSX, POSY, COLOR=255, VECCOLORS=colors, /over
;         partvelvec, VELX, VELY, POSX, POSY, /over
 partvelvec, velx, velz, posx, posz, /over, fraction=0.12, length=0.035, veccolors=1
endfor


end
