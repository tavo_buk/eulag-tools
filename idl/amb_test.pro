if !d.name eq 'PS' then begin
    device,xsize=18,ysize=24,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
end

@eu_read_bcprofl
!p.multi=[0,2,4] 
!p.charsize=1.5 & !x.margin=[8,3] & !y.margin=[3,3]

default,tt,28.
nt=(size(u))[3]
year = 365.*24.*60.*60.
time = (nxaver*dt*indgen(nt))/year
; TIME GOOD
good=where(time gt 30)
ng = n_elements(good)
; ************************
;reading stratification
openr, lun, 'strat.dat', /get_lun
data=fltarr(7,l)
readf, lun, data
rho_s=reform(data(4,*))
close,lun

mu0 = 4.*!pi*1e-7 
omega_0=2.*!pi/(tt*28.*3600.)

ruu = (total((total(u2,1)/m),1)/l)-(total((total(u^2,1)/m),1)/l)
rvv = (total((total(v2,1)/m),1)/l)-(total((total(v^2,1)/m),1)/l)
rww = (total((total(w2,1)/m),1)/l)-(total((total(w^2,1)/m),1)/l)

qwu=fltarr(l,m,ng)
qvu=fltarr(l,m,ng)
mbzbx=fltarr(l,m,ng)
mbybx=fltarr(l,m,ng)
frMC=fltarr(l,m,ng)
frRS=fltarr(l,m,ng)
frMT=fltarr(l,m,ng)
frMS=fltarr(l,m,ng)
fthMC=fltarr(l,m,ng)
fthRS=fltarr(l,m,ng)
fthMT=fltarr(l,m,ng)
fthMS=fltarr(l,m,ng)

div_frMC=fltarr(l,m,ng)
div_frRS=fltarr(l,m,ng)
div_frMT=fltarr(l,m,ng)
div_frMS=fltarr(l,m,ng)

div_fthMC=fltarr(l,m,ng)
div_fthRS=fltarr(l,m,ng)
div_fthMT=fltarr(l,m,ng)
div_fthMS=fltarr(l,m,ng)

rsinth=fltarr(l,m)
uframe=fltarr(l,m)

urms2t = ruu+rvv+rww
urms2 = (total(urms2t[good]/float((size(good))[1])))

; reynolds stresses
for k=0, l-1 do begin
  for j=0,m-1 do begin
    rsinth[k,j]=rr*r[k]*sin(theta[j])
    uframe[k,j]=omega_0*rsinth[k,j]
    ; Reynolds stresss
    qwu[k,j,*]=(rwu[j,k,good]-w[j,k,good]*u[j,k,good])
    qvu[k,j,*]=(rvu[j,k,good]-v[j,k,good]*u[j,k,good])
    ; Maxwell stresses
    mbzbx[k,j,*]=(bzbx[j,k,good]-bz[j,k,good]*bx[j,k,good])
    mbybx[k,j,*]=(bxby[j,k,good]-by[j,k,good]*bx[j,k,good])
    ; r components of the angular momentum flux
    frMC[k,j,*] = rho_s[k]*rsinth[k,j]*(u[j,k,good]+uframe[k,j])*w[j,k,good]
    frRS[k,j,*] = rho_s[k]*rsinth[k,j]*qwu[k,j,*]
    frMT[k,j,*] = -rsinth[k,j]*bz[j,k,good]*bx[j,k,good]/mu0
    frMS[k,j,*] = -rsinth[k,j]*mbzbx[k,j,*]/mu0
    ; th components of the angular momentum flux
    fthMC[k,j,*] = rho_s[k]*rsinth[k,j]*(u[j,k,good]+uframe[k,j])*v[j,k,good]
    fthRS[k,j,*] = rho_s[k]*rsinth[k,j]*qvu[k,j,*]
    fthMT[k,j,*] = -rsinth[k,j]*by[j,k,good]*bx[j,k,good]/mu0
    fthMS[k,j,*] = -rsinth[k,j]*mbybx[k,j,*]/mu0
  endfor
endfor


; divergence r component

divR=ffr*0.
for it=0,ng-1 do begin
 for j = 0,m-1 do begin 
  div_frMC[*,j,it]=deriv((r*rr),(r*rr)^2*smooth(reform(frMC[*,j,it]),2))/(r*rr)^2
  div_frRS[*,j,it]=deriv((r*rr),(r*rr)^2*smooth(reform(frRS[*,j,it]),2))/(r*rr)^2
 endfor
endfor

for it=0,ng-1 do begin
 for k = 0,l-1 do begin 
  div_fthMC[k,1:m-2,it]=deriv(theta[1:m-2],sin(theta[1:m-2])*smooth(reform(fthMC[k,1:m-2,it]),2))/rsinth[k,1:m-2]
  div_fthRS[k,1:m-2,it]=deriv(theta[1:m-2],sin(theta[1:m-2])*smooth(reform(fthRS[k,1:m-2,it]),2))/rsinth[k,1:m-2]
 endfor
endfor

lhs=div_frMC+div_fthMC
rhs=div_frRS+div_fthRS



if (itac eq 0) then begin
 rlev=44
endif else begin
 rlev=61
endelse

iplot = 1
if(iplot eq 1) then begin

thg=where(th GE -75 AND th LT 75)
;xr=[30,120]
xr=[20,40]
lev=grange(-5e11,5e11,32)
contour,smooth(rotate(reform(frMC[rlev,thg,*]),3),2),time[good],th[thg],/fi,nl=32,$
	xr=xr,xstyle=1,ystyle=1,lev=lev
lev=grange(-5e13,5e13,32)
contour,smooth(rotate(reform(fthMC[rlev,thg,*]),3),2),time[good],th[thg],/fi,nl=32,$
	xr=xr,xstyle=1,ystyle=1,lev=lev

lev=grange(-5e11,5e11,32)
contour,smooth(rotate(reform(frRS[rlev,thg,*]),3),2),time[good],th[thg],/fi,nl=32,$
	xr=xr,xstyle=1,ystyle=1,lev=lev
lev=grange(-1e13,1e13,32)
contour,smooth(rotate(reform(fthRS[rlev,thg,*]),3),2),time[good],th[thg],/fi,nl=32,$
	xr=xr,xstyle=1,ystyle=1,lev=lev

lev=grange(-5e11,5e11,32)
contour,smooth(rotate(reform(frMT[rlev,thg,*]),3),2),time[good],th[thg],/fi,nl=32,$
	xr=xr,xstyle=1,ystyle=1,lev=lev
lev=grange(-1e13,1e13,32)
contour,smooth(rotate(reform(fthMT[rlev,thg,*]),3),2),time[good],th[thg],/fi,nl=32,$
	xr=xr,xstyle=1,ystyle=1,lev=lev

lev=grange(-5e11,5e11,32)
contour,smooth(rotate(reform(frMS[rlev,thg,*]),3),2),time[good],th[thg],/fi,nl=32,$
	xr=xr,xstyle=1,ystyle=1,lev=lev
lev=grange(-1e13,1e13,32)
contour,smooth(rotate(reform(fthMS[rlev,thg,*]),3),2),time[good],th[thg],/fi,nl=32,$
	xr=xr,xstyle=1,ystyle=1,lev=lev
endif

!p.multi=0
end
