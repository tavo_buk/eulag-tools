if !d.name eq 'PS' then begin
    device,xsize=22,ysize=15,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
end
!p.multi=[0,3,3]
!p.charsize=1.5 & !x.margin=[5,3] & !y.margin=[3,3]
!P.Color =cgColor("black")
!P.Background =cgColor("white")
!x.style=1
xr=[0,40]

; selecting regions
time=nplot*dt*indgen(nt)/year

t1=3.5
t2=7.5
t3=11.5
t4=15.5
t5=19.5

kr1 = where(r ge 0.71 and r le 0.81) & nkr1=double(n_elements(kr1)) 
kr2 = where(r ge 0.81 and r le 0.88) & nkr2=double(n_elements(kr2))
kr3 = where(r ge 0.88 and r le 0.96) & nkr3=double(n_elements(kr3))

jth1=where(th GE -25 AND th LT  25) & njth1=double(n_elements(jth1)) 
jth2=where(th GE -50 AND th LT -25) & njth2=double(n_elements(jth2)) 
jth3=where(th GE -75 AND th LT -50) & njth3=double(n_elements(jth3)) 

x1=sin(theta[jth1[0]])*0.71
x2=sin(theta[jth1[0]])*0.96
y1=cos(theta[jth1[0]])*0.71
y2=cos(theta[jth1[0]])*0.96
print,x1,x2,y1,y2

x1=sin(theta[jth2[0]])*0.71
x2=sin(theta[jth2[0]])*0.96
y1=cos(theta[jth2[0]])*0.71
y2=cos(theta[jth2[0]])*0.96
print,x1,x2,y1,y2

x1=sin(theta[jth3[0]])*0.71
x2=sin(theta[jth3[0]])*0.96
y1=cos(theta[jth3[0]])*0.71
y2=cos(theta[jth3[0]])*0.96
print,x1,x2,y1,y2

mmc=-(div_frmc+div_fthmc)
mrs=-(div_frrs+div_fthrs)
mmt=-(div_frmt+div_fthmt)
mms=-(div_frms+div_fthms)

mmc_m=total(mmc,3)/double(nt)
mrs_m=total(mrs,3)/double(nt)
mmt_m=total(mmt,3)/double(nt)
mms_m=total(mms,3)/double(nt)

dmmc=0.*mmc
dmrs=0.*mmc
dmmt=0.*mmc
dmms=0.*mmc

for it=0,nt-1 do begin
 dmmc[*,*,it]=mmc[*,*,it]-mmc_m[*,*]
 dmrs[*,*,it]=mrs[*,*,it]-mrs_m[*,*]
 dmmt[*,*,it]=mmt[*,*,it]-mmt_m[*,*]
 dmms[*,*,it]=mms[*,*,it]-mms_m[*,*]
endfor

mx=max(abs(mmc))

; indexes run first for j 
; poles
mmc31=total(total(mmc[jth3,kr1,*],1)/njth3,1)/nkr1 & mx31=max(abs(mmc31)) 
mmc32=total(total(mmc[jth3,kr2,*],1)/njth3,1)/nkr2 & mx32=max(abs(mmc32)) 
mmc33=total(total(mmc[jth3,kr3,*],1)/njth3,1)/nkr3 & mx33=max(abs(mmc33)) 

mrs31=total(total(mrs[jth3,kr1,*],1)/njth3,1)/nkr1 
mrs32=total(total(mrs[jth3,kr2,*],1)/njth3,1)/nkr2 
mrs33=total(total(mrs[jth3,kr3,*],1)/njth3,1)/nkr3 

mmt31=total(total(mmt[jth3,kr1,*],1)/njth3,1)/nkr1 
mmt32=total(total(mmt[jth3,kr2,*],1)/njth3,1)/nkr2 
mmt33=total(total(mmt[jth3,kr3,*],1)/njth3,1)/nkr3 

dmmt31=total(total(dmmt[jth3,kr1,*],1)/njth3,1)/nkr1 
dmmt32=total(total(dmmt[jth3,kr2,*],1)/njth3,1)/nkr2 
dmmt33=total(total(dmmt[jth3,kr3,*],1)/njth3,1)/nkr3 

mms31=total(total(mms[jth3,kr1,*],1)/njth3,1)/nkr1 
mms32=total(total(mms[jth3,kr2,*],1)/njth3,1)/nkr2 
mms33=total(total(mms[jth3,kr3,*],1)/njth3,1)/nkr3 

toc31=total(total(tosc[jth3,kr1,*],1)/njth3,1)/nkr1 & mxt31=max(abs(toc31))
toc32=total(total(tosc[jth3,kr2,*],1)/njth3,1)/nkr2 & mxt32=max(abs(toc32))
toc33=total(total(tosc[jth3,kr3,*],1)/njth3,1)/nkr3 & mxt33=max(abs(toc33))

mxt=1.1e-7
mxt31=mxt
mxt32=mxt
mxt33=mxt

plot, time,smooth(mmc31/mx31,2),yr=[-1,1.2],ystyle=1,color=cgColor("black"),$
	title='!6(a)  R31',/nodata,xr=xr                     ,thick=3
polyfill,[t1,t1,t5,t5],[-0.99,1.19,1.19,-0.99],/data,transparent=0,col=cgcolor("grey")
oplot,[t1,t1],[-1,1.19],li=2
oplot,[t2,t2],[-1,1.19],li=2
oplot,[t3,t3],[-1,1.19],li=2
oplot,[t4,t4],[-1,1.19],li=2
oplot,[t5,t5],[-1,1.19],li=2
oplot,time,smooth(mmc31/mx31,2),color=cgColor("red"),   thick=3
oplot,time,smooth(mrs31/mx31,2),color=cgColor("orange"),thick=3
oplot,time,smooth(mmt31/mx31,2),  color=cgColor("blue"),thick=3
oplot,time,smooth(mms31/mx31,2), color=cgColor("green"),thick=3
oplot,time,smooth(toc31/mxt31,2),color=cgColor("black"),thick=3
oplot,time,smooth(dmmt31/mx31,2),  color=cgColor("blue"),thick=2,li=2

plot, time,smooth(mmc32/mx32,2),yr=[-1,1],ystyle=1,color=cgColor("black"),$
	title='!6(b)  R32',/nodata,YTICKFORMAT="(A1)"  ,thick=3 ,xr=xr 
polyfill,[t1,t1,t5,t5],[-0.99,0.99,0.99,-0.99],/data,transparent=0,col=cgcolor("grey")
oplot,[t1,t1],[-1,1],li=2
oplot,[t2,t2],[-1,1],li=2
oplot,[t3,t3],[-1,1],li=2
oplot,[t4,t4],[-1,1],li=2
oplot,[t5,t5],[-1,1],li=2
oplot,time,smooth(mmc32/mx32,2),color=cgColor("red"),thick=3
oplot,time,smooth(mrs32/mx32,2),color=cgColor("orange"),thick=3
oplot,time,smooth(mmt32/mx32,2),  color=cgColor("blue"),thick=3
oplot,time,smooth(mms32/mx32,2), color=cgColor("green"),thick=3
oplot,time,smooth(toc32/mxt32,2),color=cgColor("black"),thick=3
oplot,time,smooth(dmmt32/mx33,2),  color=cgColor("blue"),thick=2,li=2

plot, time,smooth(mmc33/mx33,2),yr=[-1,1],ystyle=1,color=cgColor("black"),$
	title='!6(c)  R33',/nodata,YTICKFORMAT="(A1)"  ,thick=3 ,xr=xr 
polyfill,[t1,t1,t5,t5],[-0.99,0.99,0.99,-0.99],/data,transparent=0,col=cgcolor("grey")
oplot,[t1,t1],[-1,1],li=2
oplot,[t2,t2],[-1,1],li=2
oplot,[t3,t3],[-1,1],li=2
oplot,[t4,t4],[-1,1],li=2
oplot,[t5,t5],[-1,1],li=2
oplot,time,smooth(mmc33/mx33,2),color=cgColor("red"),  thick=3
oplot,time,smooth(mrs33/mx33,2),color=cgColor("orange"),thick=3
oplot,time,smooth(mmt33/mx33,2),  color=cgColor("blue"),thick=3
oplot,time,smooth(mms33/mx33,2), color=cgColor("green"),thick=3
oplot,time,smooth(toc33/mxt33,2),color=cgColor("black"),thick=3
oplot,time,smooth(dmmt33/mx33,2),  color=cgColor("blue"),thick=2,li=2

; intermediate latitudes
mmc21=total(total(mmc[jth2,kr1,*],1)/njth2,1)/nkr1 & mx21=max(abs(mmc21)) 
mmc22=total(total(mmc[jth2,kr2,*],1)/njth2,1)/nkr2 & mx22=max(abs(mmc22)) 
mmc23=total(total(mmc[jth2,kr3,*],1)/njth2,1)/nkr3 & mx23=max(abs(mmc23)) 

mrs21=total(total(mrs[jth2,kr1,*],1)/njth2,1)/nkr1 
mrs22=total(total(mrs[jth2,kr2,*],1)/njth2,1)/nkr2 
mrs23=total(total(mrs[jth2,kr3,*],1)/njth2,1)/nkr3 

mmt21=total(total(mmt[jth2,kr1,*],1)/njth2,1)/nkr1 
mmt22=total(total(mmt[jth2,kr2,*],1)/njth2,1)/nkr2 
mmt23=total(total(mmt[jth2,kr3,*],1)/njth2,1)/nkr3 

mms21=total(total(mms[jth2,kr1,*],1)/njth2,1)/nkr1 
mms22=total(total(mms[jth2,kr2,*],1)/njth2,1)/nkr2 
mms23=total(total(mms[jth2,kr3,*],1)/njth2,1)/nkr3 

toc21=total(total(tosc[jth2,kr1,*],1)/njth2,1)/nkr1 & mxt21=max(abs(toc21))
toc22=total(total(tosc[jth2,kr2,*],1)/njth2,1)/nkr2 & mxt22=max(abs(toc22))
toc23=total(total(tosc[jth2,kr3,*],1)/njth2,1)/nkr3 & mxt23=max(abs(toc23))

mxt21=mxt
mxt22=mxt
mxt23=mxt

plot, time,smooth(mmc21/mx21,2),yr=[-1,1],ystyle=1,color=cgColor("black"),$
	title='!6(d)  R21',/nodata                     ,thick=3 ,xr=xr 
polyfill,[t1,t1,t5,t5],[-0.99,0.99,0.99,-0.99],/data,transparent=0,col=cgcolor("grey")
oplot,[t1,t1],[-1,1],li=2
oplot,[t2,t2],[-1,1],li=2
oplot,[t3,t3],[-1,1],li=2
oplot,[t4,t4],[-1,1],li=2
oplot,[t5,t5],[-1,1],li=2
xyouts,t1,0.77,'!7D!8t!D1!N!6',charsize=0.8
xyouts,t2,0.77,'!7D!8t!D2!N!6',charsize=0.8
xyouts,t3,0.77,'!7D!8t!D3!N!6',charsize=0.8
xyouts,t4,0.77,'!7D!8t!D4!N!6',charsize=0.8

oplot,time,smooth(mmc21/mx21,2),color=cgColor("red"),thick=3
oplot,time,smooth(mrs21/mx21,2),color=cgColor("orange"),thick=3
oplot,time,smooth(mmt21/mx21,2),  color=cgColor("blue"),thick=3
oplot,time,smooth(mms21/mx21,2), color=cgColor("green"),thick=3
oplot,time,smooth(toc21/mxt21,2),color=cgColor("black"),thick=3

plot, time,smooth(mmc22/mx22,2),yr=[-1,1],ystyle=1,color=cgColor("black"),$
	title='!6(e)  R22',/nodata,YTICKFORMAT="(A1)"  ,thick=3 ,xr=xr 
polyfill,[t1,t1,t5,t5],[-0.99,0.99,0.99,-0.99],/data,transparent=0,col=cgcolor("grey")
oplot,[t1,t1],[-1,1],li=2
oplot,[t2,t2],[-1,1],li=2
oplot,[t3,t3],[-1,1],li=2
oplot,[t4,t4],[-1,1],li=2
oplot,[t5,t5],[-1,1],li=2
xyouts,t1,0.77,'!7D!8t!D1!N!6',charsize=0.8
xyouts,t2,0.77,'!7D!8t!D2!N!6',charsize=0.8
xyouts,t3,0.77,'!7D!8t!D3!N!6',charsize=0.8
xyouts,t4,0.77,'!7D!8t!D4!N!6',charsize=0.8
oplot,time,smooth(mmc22/mx22,2),color=cgColor("red"),   thick=3
oplot,time,smooth(mrs22/mx22,2),color=cgColor("orange"),thick=3
oplot,time,smooth(mmt22/mx22,2),  color=cgColor("blue"),thick=3
oplot,time,smooth(mms22/mx22,2), color=cgColor("green"),thick=3
oplot,time,smooth(toc22/mxt22,2),color=cgColor("black"),thick=3

plot, time,smooth(mmc23/mx23,2),yr=[-1,1],ystyle=1,color=cgColor("black"),$
	title='!6(f)  R23',/nodata,YTICKFORMAT="(A1)"  ,thick=3 ,xr=xr 
polyfill,[t1,t1,t5,t5],[-0.99,0.99,0.99,-0.99],/data,transparent=0,col=cgcolor("grey")
oplot,[t1,t1],[-1,1],li=2
oplot,[t2,t2],[-1,1],li=2
oplot,[t3,t3],[-1,1],li=2
oplot,[t4,t4],[-1,1],li=2
oplot,[t5,t5],[-1,1],li=2
xyouts,t1,0.77,'!7D!8t!D1!N!6',charsize=0.8
xyouts,t2,0.77,'!7D!8t!D2!N!6',charsize=0.8
xyouts,t3,0.77,'!7D!8t!D3!N!6',charsize=0.8
xyouts,t4,0.77,'!7D!8t!D4!N!6',charsize=0.8
oplot,time,smooth(mmc23/mx23,2),color=cgColor("red"),   thick=3
oplot,time,smooth(mrs23/mx23,2),color=cgColor("orange"),thick=3
oplot,time,smooth(mmt23/mx23,2),  color=cgColor("blue"),thick=3
oplot,time,smooth(mms23/mx23,2), color=cgColor("green"),thick=3
oplot,time,smooth(toc23/mxt23,2),color=cgColor("black"),thick=3

; equator
mmc11=total(total(mmc[jth1,kr1,*],1)/njth1,1)/nkr1 & mx11=max(abs(mmc11)) 
mmc12=total(total(mmc[jth1,kr2,*],1)/njth1,1)/nkr2 & mx12=max(abs(mmc12)) 
mmc13=total(total(mmc[jth1,kr3,*],1)/njth1,1)/nkr3 & mx13=max(abs(mmc13)) 

mrs11=total(total(mrs[jth1,kr1,*],1)/njth1,1)/nkr1 
mrs12=total(total(mrs[jth1,kr2,*],1)/njth1,1)/nkr2 
mrs13=total(total(mrs[jth1,kr3,*],1)/njth1,1)/nkr3 

mmt11=total(total(mmt[jth1,kr1,*],1)/njth1,1)/nkr1 
mmt12=total(total(mmt[jth1,kr2,*],1)/njth1,1)/nkr2 
mmt13=total(total(mmt[jth1,kr3,*],1)/njth1,1)/nkr3 

mms11=total(total(mms[jth1,kr1,*],1)/njth1,1)/nkr1 
mms12=total(total(mms[jth1,kr2,*],1)/njth1,1)/nkr2 
mms13=total(total(mms[jth1,kr3,*],1)/njth1,1)/nkr3 

toc11=total(total(tosc[jth1,kr1,*],1)/njth1,1)/nkr1 & mxt11=max(abs(toc11))
toc12=total(total(tosc[jth1,kr2,*],1)/njth1,1)/nkr2 & mxt12=max(abs(toc12))
toc13=total(total(tosc[jth1,kr3,*],1)/njth1,1)/nkr3 & mxt13=max(abs(toc13))

mxt11=mxt
mxt12=mxt
mxt13=mxt

plot, time,smooth(mmc11/mx11,2),yr=[-1,1],ystyle=1,color=cgColor("black"),$
	xtitle='!6time !6 [yr]',title='!6(g)  R11',/nodata          ,thick=3 ,xr=xr 
polyfill,[t1,t1,t5,t5],[-0.99,0.99,0.99,-0.99],/data,transparent=0,col=cgcolor("grey")
oplot,[t1,t1],[-1,1],li=2
oplot,[t2,t2],[-1,1],li=2
oplot,[t3,t3],[-1,1],li=2
oplot,[t4,t4],[-1,1],li=2
oplot,[t5,t5],[-1,1],li=2
oplot,time,smooth(mmc11/mx11,2),color=cgColor("red"),thick=3
oplot,time,smooth(mrs11/mx11,2),color=cgColor("orange"),thick=3
oplot,time,smooth(mmt11/mx11,2),  color=cgColor("blue"),thick=3
oplot,time,smooth(mms11/mx11,2), color=cgColor("green"),thick=3
oplot,time,smooth(toc11/mxt11,2),color=cgColor("black"),thick=3

plot, time,smooth(mmc12/mx12,2),yr=[-1,1],ystyle=1,color=cgColor("black"),YTICKFORMAT="(A1)",$
	xtitle='!6time !6 [yr]',title='!6(h)  R12',/nodata  ,thick=3 ,xr=xr 
polyfill,[t1,t1,t5,t5],[-0.99,0.99,0.99,-0.99],/data,transparent=0,col=cgcolor("grey")
oplot,[t1,t1],[-1,1],li=2
oplot,[t2,t2],[-1,1],li=2
oplot,[t3,t3],[-1,1],li=2
oplot,[t4,t4],[-1,1],li=2
oplot,[t5,t5],[-1,1],li=2
oplot,time,smooth(mmc12/mx12,2),color=cgColor("red"),thick=3
oplot,time,smooth(mrs12/mx12,2),color=cgColor("orange"),thick=3
oplot,time,smooth(mmt12/mx12,2),  color=cgColor("blue"),thick=3
oplot,time,smooth(mms12/mx12,2), color=cgColor("green"),thick=3
oplot,time,smooth(toc12/mxt12,2),color=cgColor("black"),thick=3

plot, time,smooth(mmc13/mx13,2),yr=[-1,1],ystyle=1,color=cgColor("black"),YTICKFORMAT="(A1)",$
	xtitle='!6time !6 [yr]',title='!6(i)  R13',/nodata  ,thick=3 ,xr=xr 
polyfill,[t1,t1,t5,t5],[-0.99,0.99,0.99,-0.99],/data,transparent=0,col=cgcolor("grey")
oplot,[t1,t1],[-1,1],li=2
oplot,[t2,t2],[-1,1],li=2
oplot,[t3,t3],[-1,1],li=2
oplot,[t4,t4],[-1,1],li=2
oplot,[t5,t5],[-1,1],li=2
oplot,time,smooth(mmc13/mx13,2),color=cgColor("red"),   thick=3
oplot,time,smooth(mrs13/mx13,2),color=cgColor("orange"),thick=3
oplot,time,smooth(mmt13/mx13,2),  color=cgColor("blue"),thick=3
oplot,time,smooth(mms13/mx13,2), color=cgColor("green"),thick=3
oplot,time,smooth(toc13/mxt13,2),color=cgColor("black"),thick=3
!p.multi=0
end
