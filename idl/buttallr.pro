if !d.name eq 'PS' then begin
    device,xsize=12,ysize=12,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
end
!p.multi=[0,1,4] & !p.charsize=2.
nl=64

dir_base='/nobackupp8/gaguerre/eumag/NOTAC/'
dir1=dir_base+'nt-14-rf/'
dir2=dir_base+'test_abs-rf/'
dir3=dir_base+'nt-56-rf/'

restore,dir1+'bfield.sav'
lev = 2.*max(abs(bphi_r))*indgen(nl)/float(nl-1) $
        -max(abs(bphi_r))
cpos=[0.13,0.72,0.85,0.95]
bpos=[0.95,0.72,0.96,0.95]
contour,rotate(bphi_r,4),time,r,/fi,nl=64,ytitle='!8r/R'+sunsymbol(),$
        ystyle=1,lev=lev,pos=cpos,xstyle=1,xrange=[20,40]
colorbar,range=[min(lev),max(lev)],/vertical,xaxis=2,$
         ytickformat='(F6.2)',yticks=4,pos=bpos,charsize=1.5,$
         ytickv=[min(lev),0.5*(min(lev)+max(lev)),max(lev)]
;xyouts,21,0.74,'!6a) CZ01',charsize=1.1,charthick=3

restore,dir2+'bfield.sav'
cpos=[0.13,0.42,0.85,0.65]
bpos=[0.95,0.42,0.96,0.65]
lev = 2.*max(abs(bphi_r))*indgen(nl)/float(nl-1) $
        -max(abs(bphi_r))
contour,rotate(bphi_r,4),time,r,/fi,nl=64,ytitle='!8r/R'+sunsymbol(),$
        ystyle=1,lev=lev,xstyle=1,pos=cpos,xrange=[20,40]
colorbar,range=[min(lev),max(lev)],/vertical,xaxis=2,$
         ytickformat='(F6.2)',yticks=4,pos=bpos,charsize=1.5,$
         ytickv=[min(lev),0.5*(min(lev)+max(lev)),max(lev)]
;xyouts,21, 0.8,'!6b) CZ02',charsize=1.1,charthick=3


restore,dir3+'bfield.sav'
cpos=[0.13,0.12,0.85,0.35]
bpos=[0.95,0.12,0.96,0.35]
lev = 2.*max(abs(bphi_r))*indgen(nl)/float(nl-1) $
        -max(abs(bphi_r))
contour,rotate(bphi_r,4),time,r,/fi,nl=64,ytitle='!8r/R'+sunsymbol(),$
        xtitle='time [years]!6',ystyle=1,lev=lev,xstyle=1,pos=cpos,xrange=[70,175]
colorbar,range=[min(lev),max(lev)],/vertical,xaxis=2,$
         ytickformat='(F6.2)',yticks=4,pos=bpos,charsize=1.5,$
         ytickv=[min(lev),0.5*(min(lev)+max(lev)),max(lev)]
;xyouts,76, 0.8,'!6c) CZ03',charsize=1.1,charthick=3

!p.multi=0
end
