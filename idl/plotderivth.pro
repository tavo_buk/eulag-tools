; mv idl.ps ~/Dropbox/Tex/paper.nssl/fig/theprof.eps
if !d.name eq 'PS' then begin
    device,xsize=22,ysize=12,yoffset=3
    !p.thick=2 & !x.thick=1 & !y.thick=1
end
!x.margin=[5,3] & !y.margin=[4,3]
!p.multi=[0,2,1]

dir1='/nobackupp8/gaguerre/eumag/NOTAC/nt-14-rf/'
dir2='/nobackupp8/gaguerre/eumag/NOTAC/nt-28-rf/'
dir3='/nobackupp8/gaguerre/eumag/NOTAC/nt-56-rf/'
itac=0
print,'itac = ',itac
file_grid='grid.sav'
file_a='velocity.sav'
restore,dir1+file_grid
restore,dir1+file_a
m=(size(rurms2))[2]
l=(size(r))[1]
@rhoprof
rgood=where(r ge 0.72)
Hrho=-(1./(deriv(r,rh)/rh))*6.96e8
Hrhom=mean(Hrho[rgood])
urms_av=sqrt(mean(rurms2[rgood,*]))
tau=Hrhom/urms_av
eta_t = tau*(total(rurms2,2)/double(m))/3.
eta_t_av = mean(eta_t[rgood])

thg=where(th GE -84 AND th LT 84)

ibase=where(r ge 0.72 and r le 77)
rbmean=mean(r[ibase])
nbase=(size(ibase))[1]
omb=total(omega[ibase,*],1)/double(nbase)
Cwbth=(2.*!pi*1.e-9*rr^2/eta_t_av)*deriv(th,omb)/rbmean

itop=where(r ge 0.90 and r le 95)
rtmean=mean(r[itop])
ntop=(size(itop))[1]
omt=total(omega[itop,*],1)/double(ntop)
Cwtth=(2.*!pi*1.e-9*rr^2/eta_t_av)*deriv(th,omt)/rtmean
thg2=where(th GE -84 AND th LT 84)
print,'eta_t, Cwth_bot, Cwth_top '
print,eta_t_av, max(abs(Cwbth[thg2])),max(abs(Cwtth[thg2]))
th=90.-theta*180./!pi
plot,th[thg],Cwbth[thg],xrange=[-90,90],charsize=1.5,xstyle=1, $
	ystyle=1,thick=5,xtitle='!890-!7h!6',yr=[-25,25],$
	title='e) !8r!U-1!N!9d!D!7h!N!4X!8R'+sunsymbol()+'!U3!N/!4g!6!Dt0!N!6, CZ',/nodata,xticks=4
oplot,th[thg],Cwbth[thg],li=2,thick=10,color=cgColor('red')
oplot,th[thg],Cwtth[thg],li=0,thick=10,color=cgColor('red')

restore,dir2+file_a
rgood=where(r ge 0.72)
Hrho=-(1./(deriv(r,rh)/rh))*6.96e8
Hrhom=mean(Hrho[rgood])
urms_av=sqrt(mean(rurms2[rgood,*]))
tau=Hrhom/urms_av
eta_t = tau*(total(rurms2,2)/double(m))/3.
eta_t_av = mean(eta_t[rgood])
;
ibase=where(r ge 0.72 and r le 77)
rbmean=mean(r[ibase])
nbase=(size(ibase))[1]
omb=total(omega[ibase,*],1)/double(nbase)
Cwbth=(2.*!pi*1.e-9*rr^2/eta_t_av)*deriv(th,omb)/rbmean

itop=where(r ge 0.90 and r le 95)
rtmean=mean(r[itop])
ntop=(size(itop))[1]
omt=total(omega[itop,*],1)/double(ntop)
Cwtth=(2.*!pi*1.e-9*rr^2/eta_t_av)*deriv(th,omt)/rtmean

print,'eta_t, Cwth_bot, Cwth_top '
print,eta_t_av, max(abs(Cwbth[thg2])),max(abs(Cwtth[thg2]))
th=90.-theta*180./!pi
oplot,th[thg],Cwbth[thg],li=2,thick=10,color=cgColor('blue')
oplot,th[thg],Cwtth[thg],li=0,thick=10,color=cgColor('blue')
;
restore,dir3+file_a
rgood=where(r ge 0.72)
Hrho=-(1./(deriv(r,rh)/rh))*6.96e8
Hrhom=mean(Hrho[rgood])
urms_av=sqrt(mean(rurms2[rgood,*]))
tau=Hrhom/urms_av
eta_t = tau*(total(rurms2,2)/double(m))/3.
eta_t_av = mean(eta_t[rgood])
;
ibase=where(r ge 0.72 and r le 77)
rbmean=mean(r[ibase])
nbase=(size(ibase))[1]
omb=total(omega[ibase,*],1)/double(nbase)
Cwbth=(2.*!pi*1.e-9*rr^2/eta_t_av)*deriv(th,omb)/rbmean

itop=where(r ge 0.90 and r le 95)
rtmean=mean(r[itop])
ntop=(size(itop))[1]
omt=total(omega[itop,*],1)/double(ntop)
Cwtth=(2.*!pi*1.e-9*rr^2/eta_t_av)*deriv(th,omt)/rtmean

print,'eta_t, Cwth_bot, Cwth_top '
print,eta_t_av, max(abs(Cwbth[thg2])),max(abs(Cwtth[thg2]))
th=90.-theta*180./!pi
oplot,th[thg],Cwbth[thg],li=2,thick=10,color=cgColor('green')
oplot,th[thg],Cwtth[thg],li=0,thick=10,color=cgColor('green')

oplot,[-50,0],[-15.0,-15.0],li=2,thick=10
xyouts,10,-15.5,'!6bottom',charsize=1.5
oplot,[-50,0],[-20,-20],li=0,thick=10
xyouts,10,-20.5,'!6top',charsize=1.5

oplot,[-90,90],[0,0],li=1,thick=2
oplot,[0,0],[-1,1],li=1,thick=2

;;;;;;
;;;;;;
;;;;;;

dir1='/nobackupp8/gaguerre/eumag/TAC/tac-14-rf/'
dir2='/nobackupp8/gaguerre/eumag/TAC/tac-28-rf/'
dir3='/nobackupp8/gaguerre/eumag/TAC/tac-56-rf/'
itac=1
print,'itac = ',itac
file_grid='grid.sav'
file_a='velocity.sav'
restore,dir1+file_grid
restore,dir1+file_a
m=(size(rurms2))[2]
l=(size(r))[1]
@rhoprof
rgood=where(r ge 0.72)
Hrho=-(1./(deriv(r,rh)/rh))*6.96e8
Hrhom=mean(Hrho[rgood])
urms_av=sqrt(mean(rurms2[rgood,*]))
tau=Hrhom/urms_av
eta_t = tau*(total(rurms2,2)/double(m))/3.
eta_t_av = mean(eta_t[rgood])
;
thg=where(th GE -84 AND th LT 84)
;
ibase=where(r ge 0.72 and r le 77)
rbmean=mean(r[ibase])
nbase=(size(ibase))[1]
omb=total(omega[ibase,*],1)/double(nbase)
Cwbth=(2.*!pi*1.e-9*rr^2/eta_t_av)*deriv(th,omb)/rbmean

itop=where(r ge 0.90 and r le 95)
rtmean=mean(r[itop])
ntop=(size(itop))[1]
omt=total(omega[itop,*],1)/double(ntop)
Cwtth=(2.*!pi*1.e-9*rr^2/eta_t_av)*deriv(th,omt);/rtmean

print,'eta_t, Cwth_bot, Cwth_top '
print,eta_t_av, max(abs(Cwbth[thg2])),max(abs(Cwtth[thg2]))
th=90.-theta*180./!pi
plot,th[thg],Cwbth[thg],xrange=[-90,90],charsize=1.5,xstyle=1, $
	ystyle=1,thick=5,xtitle='!890-!7h!6',yr=[-25,25],$
	title='f) !8r!U-1!N!9d!D!7h!N!4X!8R'+sunsymbol()+'!U3!N/!4g!6!Dt0!N!6, RC',/nodata,xticks=4
oplot,th[thg],Cwbth[thg],li=2,thick=10,color=cgColor('red')
oplot,th[thg],Cwtth[thg],li=0,thick=10,color=cgColor('red')

restore,dir2+file_a
rgood=where(r ge 0.72)
Hrho=-(1./(deriv(r,rh)/rh))*6.96e8
Hrhom=mean(Hrho[rgood])
urms_av=sqrt(mean(rurms2[rgood,*]))
tau=Hrhom/urms_av
eta_t = tau*(total(rurms2,2)/double(m))/3.
eta_t_av = mean(eta_t[rgood])
;
ibase=where(r ge 0.72 and r le 77)
rbmean=mean(r[ibase])
nbase=(size(ibase))[1]
omb=total(omega[ibase,*],1)/double(nbase)
Cwbth=(2.*!pi*1.e-9*rr^2/eta_t_av)*deriv(th,omb)/rbmean

itop=where(r ge 0.90 and r le 95)
rtmean=mean(r[itop])
ntop=(size(itop))[1]
omt=total(omega[itop,*],1)/double(ntop)
Cwtth=(2.*!pi*1.e-9*rr^2/eta_t_av)*deriv(th,omt)/rtmean

print,'eta_t, Cwth_bot, Cwth_top '
print,eta_t_av, max(abs(Cwbth[thg2])),max(abs(Cwtth[thg2]))
th=90.-theta*180./!pi
oplot,th[thg],Cwbth[thg],li=2,thick=10,color=cgColor('blue')
oplot,th[thg],Cwtth[thg],li=0,thick=10,color=cgColor('blue')
;
restore,dir3+file_a
rgood=where(r ge 0.72)
Hrho=-(1./(deriv(r,rh)/rh))*6.96e8
Hrhom=mean(Hrho[rgood])
urms_av=sqrt(mean(rurms2[rgood,*]))
tau=Hrhom/urms_av
eta_t = tau*(total(rurms2,2)/double(m))/3.
eta_t_av = mean(eta_t[rgood])
;
ibase=where(r ge 0.72 and r le 77)
rbmean=mean(r[ibase])
nbase=(size(ibase))[1]
omb=total(omega[ibase,*],1)/double(nbase)
Cwbth=(2.*!pi*1.e-9*rr^2/eta_t_av)*deriv(th,omb)/rbmean

itop=where(r ge 0.90 and r le 95)
rtmean=mean(r[itop])
ntop=(size(itop))[1]
omt=total(omega[itop,*],1)/double(ntop)
Cwtth=(2.*!pi*1.e-9*rr^2/eta_t_av)*deriv(th,omt)/rtmean

print,'eta_t, Cwth_bot, Cwth_top '
print,eta_t_av, max(abs(Cwbth[thg2])),max(abs(Cwtth[thg2]))
th=90.-theta*180./!pi
oplot,th[thg],Cwbth[thg],li=2,thick=10,color=cgColor('green')
oplot,th[thg],Cwtth[thg],li=0,thick=10,color=cgColor('green')

oplot,[-90,90],[0,0],li=1,thick=2
oplot,[0,0],[-1,1],li=1,thick=2
oplot,[-50,0],[-15.0,-15.0],li=2,thick=10
xyouts,10,-15.5,'!6bottom',charsize=1.5
oplot,[-50,0],[-20,-20],li=0,thick=10
xyouts,10,-20.5,'!6top',charsize=1.5
end
