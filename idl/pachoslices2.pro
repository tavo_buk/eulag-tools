;__________________________________________
; from rboxhd_f .....
;__________________________________________


; fields for vector plot
uref=reform(u[*,0,*,*])
wref=reform(w[*,0,*,*])

dims=size(uref)
iend=dims[3]-1

cfield=wref

;__________________________________________
; Preferences for the plots
;__________________________________________

eu_read_param, o=o
default, itac, 0

Lz=o.dz00
Lx=o.dx00
nz=o.l
nx=o.n

zarray=(Lz/nz)*indgen(nz)
xarray=(Lx/nx)*indgen(nx)

print, 'x dimension length', Lx
print, 'z dimension length', Lz

nl=80
slicemin=min(cfield)
slicemax=max(cfield)

print, 'number of levels in the plot: ', nl
print, 'minimum of the field plotted: ', slicemin
print, 'maximum of the field plotted: ', slicemax

;__________________________________________
; Preferences for colorbar
;__________________________________________

xin=0.92
dx=0.02
yin=0.15
dy=0.4

;__________________________________________
; Preferences for vector plot
;__________________________________________

zcut0=0
botx=3
topx=100
posx=xarray[botx:topx]

frac=0.15 ;fraction of vectors to plot
len=0.03  ;relative length of vectors

;__________________________________________
; loop of slices
;__________________________________________


for slice=ibeg, iend-1 do begin
 
 ; setting device
 DEVICE, GET_DECOMPOSED=old_decomposed
 DEVICE, DECO

 ;creating contour field and colorbar
 contour, wref[*,*,slice], xarray, zarray,/iso, /fi, nl=nl
 colorbar, range=[slicemin,slicemax], /vertical, pos=[xin,yin,xin+dx,yin+dy], ytickformat='(F6.2)', yaxis=2, charsize=1.5

 ;mounting vector plot
 for ii=0, nz-1  do begin
 zcut=zcut0+ii
 posz=fltarr(topx+1-botx)+zarray[zcut]
 velx=uref[botx:topx,zcut,slice]
 velz=wref[botx:topx,zcut,slice]

 partvelvec, velx, velz, posx, posz, /over, fraction=frac, length=len, veccolors=1
 endfor

 ; printing plot to file "imagename"
 istr = strtrim(string(slice,'(I20.4)'),2)
 imagename = './img_'+istr+'.png'
 print, "writing png ", imagename
 WRITE_PNG, imagename, TVRD(/TRUE), red, green, blue
 wait,0
endfor

end
