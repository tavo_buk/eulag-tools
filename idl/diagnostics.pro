!p.charsize=2
@rhoprof
default,tt,28.

; presure scale heigth
hp=-1./(deriv(rr*r,pr)/pr)
plot,r,hp,xr=[0.61,0.95],xstyle=1,ystyle=1,xtitle='r/R',ytitle='Hp'
oplot,[0.72,0.72],[1e7,1e8],li=1
oplot,[0.61,0.96],[54785547.,54785547.],li=1



; urms velocity
nt=n_elements(reform(u[0,0,*]))
quu=fltarr(l,m)
qvv=fltarr(l,m)
qww=fltarr(l,m)
rsinth=fltarr(l,m)
urms2=fltarr(l,m)

for k=0, l-1 do begin
  for j=0,m-1 do begin
    rsinth[k,j]=rr*r[k]*sin(theta[j])
    quu[k,j]= total(u2[j,k,nt-nn:nt-1]- u[j,k,nt-nn:nt-1]^2)/float(nn)
    qvv[k,j]= total(v2[j,k,nt-nn:nt-1]- v[j,k,nt-nn:nt-1]^2)/float(nn)
    qww[k,j]= total(w2[j,k,nt-nn:nt-1]- w[j,k,nt-nn:nt-1]^2)/float(nn)
    urms2[k,j]=qvv[k,j]+qww[k,j]
  endfor
endfor

urms=total(sqrt(urms2),2)/float(m)
omega_0=2.*!pi/(tt*24.*3600.)
ir=where(r ge 0.72 and r le 0.92)
tau=2.*hp/urms

Ro1=urms/(2.*omega_0*0.05*rr)
Ro2=(tt*24.*3600.)/tau

end
