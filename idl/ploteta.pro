; mv idl.ps ~/Dropbox/Tex/paper.nssl/fig/theprof.eps
if !d.name eq 'PS' then begin
    device,xsize=18,ysize=12,yoffset=3
    !p.thick=2 & !x.thick=1 & !y.thick=1
end
!x.margin=[8,5] & !y.margin=[4,3]
!p.multi=[0,1,1]

file_a='velocity.sav'
file_b='khelicity.sav'
file_c='mhelicity.sav'
dir1='/nobackupp8/gaguerre/eumag/NOTAC/nt-14-rf/'
dir2='/nobackupp8/gaguerre/eumag/NOTAC/nt-28-rf/'
dir3='/nobackupp8/gaguerre/eumag/NOTAC/nt-56-rf/'
dir4='/nobackupp8/gaguerre/eumag/NOTAC/nt-28-rf-hd/'
itac=0
restore,dir1+file_a
restore,dir1+file_b
restore,dir1+file_c
print,'itac =', itac, dir1
;
restore,dir2+file_a
restore,dir2+file_b
restore,dir2+file_c
print,'itac =', itac, dir2
m=(size(rurms2))[2]
l=(size(r))[1]
@rhoprof
rgood=where(r ge 0.72)
Hrho=-(1./(deriv(r,rh)/rh))*6.96e8
Hrhom=mean(Hrho[rgood])
urms_av=sqrt(mean(rurms2[rgood,*]))
tau=Hrhom/urms_av
eta_t = tau*(total(rurms2,2)/double(m))/3.
eta_t_av = mean(eta_t[rgood])
yr=[1e4,5e9]
plot,r[0:l-2],smooth(eta_t[0:l-2],0),xrange=[0.61,0.96],charsize=1.5,xstyle=1, $
	yr=yr,/ylog,ystyle=1,thick=5,xtitle='!8r/R!6'+sunsymbol(),$
	ytitle='!4g!6!Dt!N!6 [m!U2!N s!U-1!N]',/nodata,xticks=4
oplot,r[1:l-2],smooth(eta_t[1:l-2],2),li=0,thick=10,color=cgColor('blue')

oplot,[0.68,0.68],[yr[0],yr[1]],li=1,thick=2
oplot,[0.77,0.77],[yr[0],yr[1]],li=1,thick=2

restore,dir4+file_a
print,'itac =', itac, dir2
m=(size(rurms2))[2]
l=(size(r))[1]
@rhoprof
rgood=where(r ge 0.72)
Hrho=-(1./(deriv(r,rh)/rh))*6.96e8
Hrhom=mean(Hrho[rgood])
urms_av=sqrt(mean(rurms2[rgood,*]))
tau=Hrhom/urms_av
eta_t = tau*(total(rurms2,2)/double(m))/3.
eta_t_av = mean(eta_t[rgood])
oplot,r[1:l-2],smooth(eta_t[1:l-2],0),li=2,thick=10,color=cgColor('blue')

oplot,[0.80,0.828],[3.2e7,3.2e7],color=cgColor('blue'),thick=10
xyouts,0.832,3e7,' CZ02',charsize=1.5
oplot,[0.80,0.828],[1.1e7,1.1e7],color=cgColor('blue'),thick=10,li=2
xyouts,0.832,1e7,' CZ02-hd',charsize=1.5
oplot,[0.80,0.828],[3.2e6,3.2e6],color=cgColor('red'),thick=10
xyouts,0.832,3.2e6,' RC02',charsize=1.5
oplot,[0.80,0.828],[1.1e6,1.1e6],color=cgColor('red'),thick=10,li=2
xyouts,0.832,1.1e6,' RC02-hd',charsize=1.5



;;;;;;;;;;;;;;
;;;;;;;;;;;;;;
;;;;;;;;;;;;;;
print,'MODELS WITH TACHOCLINE'

dir1='/nobackupp8/gaguerre/eumag/TAC/tac-14-rf/'
dir2='/nobackupp8/gaguerre/eumag/TAC/tac-28-rf/'
dir3='/nobackupp8/gaguerre/eumag/TAC/tac-56-rf/'
dir4='/nobackupp8/gaguerre/eumag/TAC/tac-28-rf-hd/'
itac=1
restore,dir2+file_a
restore,dir2+file_b
restore,dir2+file_c
print,'itac =', itac, dir2
m=(size(rurms2))[2]
l=(size(r))[1]
print,m,l
@rhoprof
rgood=where(r ge 0.72)
Hrho=-(1./(deriv(r,rh)/rh))*6.96e8
Hrhom=mean(Hrho[rgood])
urms_av=sqrt(mean(rurms2[rgood,*]))
tau=Hrhom/urms_av
eta_t = tau*(total(rurms2,2)/double(m))/3.
print,size(eta_t),size(rurms2)
eta_t_av = mean(eta_t[rgood])
oplot,r[1:l-2],smooth(eta_t[1:l-2],0),li=0,thick=10,color=cgColor('red')

restore,dir4+file_a
print,'itac =', itac, dir2
m=(size(rurms2))[2]
l=(size(r))[1]
@rhoprof
rgood=where(r ge 0.72)
Hrho=-(1./(deriv(r,rh)/rh))*6.96e8
Hrhom=mean(Hrho[rgood])
urms_av=sqrt(mean(rurms2[rgood,*]))
tau=Hrhom/urms_av
eta_t = tau*(total(rurms2,2)/double(m))/3.
eta_t_av = mean(eta_t[rgood])
oplot,r[1:l-2],smooth(eta_t[1:l-2],1),li=2,thick=10,color=cgColor('red')

end
