if !d.name eq 'PS' then begin
    device,xsize=20,ysize=10,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
end
!p.multi=[0,4,2] 
!p.charsize=1.5 & !x.margin=[8,3] & !y.margin=[3,3]
thg=where(th GE -85 AND th LT 0)
; divergence r component

; hydro terms
dudt=fltarr(m,l,nt)
div_frMC=fltarr(m,l,nt)
div_frRS=fltarr(m,l,nt)
div_fthMC=fltarr(m,l,nt)
div_fthRS=fltarr(m,l,nt)
; magnetic terms
if (imhd ne 0) then begin
 div_frMT=fltarr(m,l,nt)
 div_frMS=fltarr(m,l,nt)
 div_fthMT=fltarr(m,l,nt)
 div_fthMS=fltarr(m,l,nt)
endif

for it=0,nt-1 do begin
 for j = 0,m-1 do begin 
  div_frMC[j,*,it]=deriv((r*rr),(r*rr)^2*reform(frMC[j,*,it]))/(r*rr)^2
  div_frRS[j,*,it]=deriv((r*rr),(r*rr)^2*reform(frRS[j,*,it]))/(r*rr)^2
  if (imhd ne 0) then begin
   div_frMT[j,*,it]=deriv((r*rr),(r*rr)^2*reform(frMT[j,*,it]))/(r*rr)^2
   div_frMS[j,*,it]=deriv((r*rr),(r*rr)^2*reform(frMS[j,*,it]))/(r*rr)^2
  endif
 endfor
endfor

for it=0,nt-1 do begin
 for k = 0,l-1 do begin 
  div_fthMC[1:m-2,k,it]=deriv(theta[1:m-2],sin(theta[1:m-2])*reform(fthMC[1:m-2,k,it]))/rsinth[1:m-2,k]
  div_fthRS[1:m-2,k,it]=deriv(theta[1:m-2],sin(theta[1:m-2])*reform(fthRS[1:m-2,k,it]))/rsinth[1:m-2,k]
  if (imhd ne 0) then begin
   div_fthMT[1:m-2,k,it]=deriv(theta[1:m-2],sin(theta[1:m-2])*reform(fthMT[1:m-2,k,it]))/rsinth[1:m-2,k]
   div_fthMS[1:m-2,k,it]=deriv(theta[1:m-2],sin(theta[1:m-2])*reform(fthMS[1:m-2,k,it]))/rsinth[1:m-2,k]
  endif
 endfor
endfor

;time integrals

;time interval 1
int_div_frMC=fltarr(m,l)
int_div_frRS=fltarr(m,l)
int_div_fthMC=fltarr(m,l)
int_div_fthRS=fltarr(m,l)
if (imhd ne 0) then begin
 int_div_frMT=fltarr(m,l)
 int_div_frMS=fltarr(m,l)
 int_div_fthMT=fltarr(m,l)
 int_div_fthMS=fltarr(m,l)
endif
int_dudt=fltarr(m,l)

g1=where(time ge 0 and time le 1.5) 
print,g1
for j=1,m-2 do begin
 for k=0,l-1 do begin
  dudt[j,k,*]=deriv(year*time,reform(umean[j,k,*]))
  int_dudt[j,k]=int_tabulated(year*time[g1],reform(dudt[j,k,g1]))
  ;time integral hydro terms
  int_div_frMC[j,k]=int_tabulated(year*time[g1],reform(div_frMC[j,k,g1]))/(rho_s[k]*rsinth[j,k]) 
  int_div_frRS[j,k]=int_tabulated(year*time[g1],reform(div_frRS[j,k,g1]))/(rho_s[k]*rsinth[j,k])
  int_div_fthMC[j,k]=int_tabulated(year*time[g1],reform(div_fthMC[j,k,g1]))/(rho_s[k]*rsinth[j,k]) 
  int_div_fthRS[j,k]=int_tabulated(year*time[g1],reform(div_fthRS[j,k,g1]))/(rho_s[k]*rsinth[j,k])
  ;mhd terms
  if (imhd ne 0) then begin
   int_div_frMT[j,k]=int_tabulated(year*time[g1],reform(div_frMT[j,k,g1]))/(rho_s[k]*rsinth[j,k])
   int_div_frMS[j,k]=int_tabulated(year*time[g1],reform(div_frMS[j,k,g1]))/(rho_s[k]*rsinth[j,k])
   int_div_fthMT[j,k]=int_tabulated(year*time[g1],reform(div_fthMT[j,k,g1]))/(rho_s[k]*rsinth[j,k])
   int_div_fthMS[j,k]=int_tabulated(year*time[g1],reform(div_fthMS[j,k,g1]))/(rho_s[k]*rsinth[j,k])
  endif
 endfor
endfor

if (imhd ne 0) then begin
; magnetic case
 levmc=grange(-max(int_div_fthMC),max(int_div_fthMC),64)/1.
 levmt=grange(-max(int_div_fthMT),max(int_div_fthMT),64)/1.5
 contour,transpose(smooth(int_div_frMC[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmc ,xstyle=4,ystyle=4 
 contour,transpose(smooth(int_div_fthMC[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmc,xstyle=4,ystyle=4 
 contour,transpose(smooth(int_div_frRS[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmt,xstyle=4,ystyle=4
 contour,transpose(smooth(int_div_fthRS[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmt,xstyle=4,ystyle=4
 contour,transpose(smooth(int_div_frMT[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmt,xstyle=4,ystyle=4
 contour,transpose(smooth(int_div_fthMT[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmt,xstyle=4,ystyle=4
 int_all = - (int_div_fthMC + int_div_frMC) $
           - (int_div_fthRS + int_div_frRS) $
           - (int_div_fthMT + int_div_frMT) $ 
           - (int_div_fthMS + int_div_frMS)
 levdd=grange(-max(int_dudt),max(int_dudt),64)/0.9
 contour,transpose(smooth(int_all[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmt,xstyle=4,ystyle=4
 contour,transpose(smooth(int_dudt[thg,*],0)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levdd,xstyle=4,ystyle=4
endif else begin
; hydro case
 levmc=grange(-max(int_div_fthMC),max(int_div_fthMC),64)/1.
 levrs=grange(-max(int_div_fthRS),max(int_div_fthRS),64)/1.
 contour,transpose(smooth(int_div_frMC[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmc,xstyle=4,ystyle=4 
 contour,transpose(smooth(int_div_fthMC[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmc,xstyle=4,ystyle=4 
 contour,transpose(smooth(int_div_frRS[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levrs,xstyle=4,ystyle=4
 contour,transpose(smooth(int_div_fthRS[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levrs, xstyle=4,ystyle=4
 int_all = -int_div_fthMC - int_div_fthRS - int_div_frMC - int_div_frRS
 
 levdd=grange(-max(int_dudt),max(int_dudt),64)/0.5
 contour,transpose(smooth(int_all[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=10*levmt,xstyle=4,ystyle=4
 contour,transpose(smooth(int_dudt[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levdd,xstyle=4,ystyle=4    
endelse

;g2=where(time ge 21 and time le 27) 
;print,g2
;for j=1,m-2 do begin
; for k=0,l-1 do begin
;  ;time integral r terms
;  int_div_frMC[j,k]=int_tabulated(time[g2],reform(div_frMC[j,k,g2]))/(rho_s[k]*rsinth[j,k]) 
;  int_div_frRS[j,k]=int_tabulated(time[g2],reform(div_frRS[j,k,g2]))/(rho_s[k]*rsinth[j,k])
;  int_div_frMT[j,k]=int_tabulated(time[g2],reform(div_frMT[j,k,g2]))/(rho_s[k]*rsinth[j,k])
;  int_div_frMS[j,k]=int_tabulated(time[g2],reform(div_frMS[j,k,g2]))/(rho_s[k]*rsinth[j,k])
;  ;theta terms
;  int_div_fthMC[j,k]=int_tabulated(time[g2],reform(div_fthMC[j,k,g2]))/(rho_s[k]*rsinth[j,k]) 
;  int_div_fthRS[j,k]=int_tabulated(time[g2],reform(div_fthRS[j,k,g2]))/(rho_s[k]*rsinth[j,k])
;  int_div_fthMT[j,k]=int_tabulated(time[g2],reform(div_fthMT[j,k,g2]))/(rho_s[k]*rsinth[j,k])
;  int_div_fthMS[j,k]=int_tabulated(time[g2],reform(div_fthMS[j,k,g2]))/(rho_s[k]*rsinth[j,k])
; endfor
;endfor
;contour,transpose(smooth(int_div_fthMC[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmc 
;contour,transpose(smooth(int_div_fthRS[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmt 
;contour,transpose(smooth(int_div_fthMT[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmt
;int_all = -int_div_fthMC - int_div_fthRS + int_div_fthMT + int_div_fthMS $
;          -int_div_frMC - int_div_frRS + int_div_frMT + int_div_frMS
;;contour,transpose(smooth(int_div_fthMS[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmt
;contour,transpose(smooth(int_all[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmt


!p.multi=0
end
