if !d.name eq 'PS' then begin
    device,xsize=18,ysize=12,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
end
!x.margin=[8,3]
!p.multi=[0,2,1]

!p.charsize=1.3
default,tt,28.
rsinth=fltarr(l,m)
nt=(size(u))[3]
nth=(size(th))[1]
print,nt
nl=64
year = 365.*24.*60.*60.
time = (nxaver*dt*indgen(nt))/year
nt=n_elements(time)
; TIME GOOD
ng=200
t0=0.
t1=30
print,'times 0, 1', t0,t1, ng,nt
; ************************
;reading stratification
openr, lun, 'strat.dat', /get_lun
data=fltarr(7,l)
readf, lun, data
rho_s=reform(data(4,*))
close,lun

umean=fltarr(l,m)
omega=fltarr(l,m,nt)
tosc=fltarr(l,m,nt)
omega_0=1./(28.*24.*3600)

for k=0, l-1 do begin
  for j=0,m-1 do begin
    rsinth[k,j]=rr*r[k]*sin(theta[j])
    umean[k,j] = total(u[j,k,nt-ng:nt-1],3)/float(ng)
  endfor
endfor

for k=0, l-1 do begin
  for j=1,m-2 do begin
   omega[k,j,*]=1.e9*(omega_0 + u[j,k,*]/(2.*!pi*rsinth[k,j]))
  endfor
endfor

omega_mean=0.*umean
omega_mean[*,1:m-2]=1.e9*(omega_0+umean[*,1:m-2]/(2.*!pi*rsinth[*,1:m-2]))

for it=0,nt-1 do begin
 tosc[*,*,it]=smooth(omega[*,*,it],0)-smooth(omega_mean[*,*],1)
endfor

irtac=where(r ge 0.68 and r le 0.77)
ithe=where(th ge -45 and th le -1)
ithp=where(th ge -80 and th le -45)
print,'to_po',sqrt(mean(reform(tosc[irtac,ithp,nt-ng:nt-1])^2))
print,'to_eq',sqrt(mean(reform(tosc[irtac,ithe,nt-ng:nt-1])^2))

thg=where(th GE -80 AND th LT 80)

if (itac eq 0) then begin
 rlev=44
endif else begin
 rlev=62
endelse

dudt=fltarr(l,m,nt)
for k=0, l-1 do begin
 for j=1,m-2 do begin
 dudt[k,j,*]=rsinth[k,j]*xder_curl(reform(rho_s[k]*smooth(u[j,k,*],2)),time[*])
 endfor
endfor

; GRAPHICS
;lev_theta = grange(min(tosc[rlev,thg,*]),-min(tosc[rlev,thg,*]),32)/2.
;lev_theta = grange(-11,11,32)
lev_theta = grange(min(tosc[rlev,thg,*]),-min(tosc[rlev,thg,*]),32)/2.
cpos=[0.13,0.56,0.85,0.92]
bpos=[0.95,0.56,0.96,0.92]
contour,smooth(rotate(reform(tosc[rlev,thg,*]),3),1),time,th[thg],/fi,nl=32,$
	ystyle=1,xstyle=1,pos=cpos,xticks=1.,ytitle='!890-!7h!N!6 [!Uo!N]',yticks=2,ytickv=[-50,0,50],$
;	xr=[t0,t1], title='d) RC02 - !7dX/!82!7p!6 [nHz]',xtickname=REPLICATE(' ', 2),xminor=18,lev=lev_theta
	xr=[t0,t1], title='a) model RC02 - !7dX/!82!7p!6 [nHz]',xtickname=REPLICATE(' ', 2),xminor=18,lev=lev_theta
bphi=reform(bx[thg,rlev,*])
nlev_field=15
lev=grange(0,0.2,nlev_field)
contour,smooth(rotate(bphi,3),2),time,th[thg],nl=nlev_field,/over,lev=lev,$
	thick=1,min_value=-0.0,c_colors=cgColor("white"),c_thick=[3,3]

lev=grange(-0.2,0.0,nlev_field)
contour,smooth(rotate(bphi,3),2),time,th[thg],nl=nlev_field,/over,lev=lev,$
	c_linestyle=2,thick=1,max_value=-0.0,c_colors=cgColor("white"),c_thick=[3,3]

colorbar,range=[min(lev_theta),max(lev_theta)],/vertical,xaxis=2,$
         ytickformat='(F6.0)',yticks=4,pos=bpos,$
         ytickv=[min(lev_theta),0.5*(min(lev_theta)+max(lev_theta)),max(lev_theta)]


oplot,[3.5,3.5],[-90,90],li=2,thick=6,color=cgcolor('black') 
oplot,[7.5,7.5],[-90,90],li=2,thick=6,color=cgcolor('black') 
oplot,[11.5,11.5],[-90,90],li=2,thick=6,color=cgcolor('black') 
oplot,[15.5,15.5],[-90,90],li=2,thick=6,color=cgcolor('black') 
oplot,[19.5,19.5],[-90,90],li=2,thick=6,color=cgcolor('black') 

thlev=21
lev_r = grange(min(tosc[*,thlev,*]),-min(tosc[*,thlev,*]),32)/1
cpos=[0.13,0.13,0.85,0.49]
bpos=[0.95,0.13,0.96,0.49]
contour,smooth(rotate(reform(tosc[*,thlev,*]),4),2),time,r,/fi,nl=32,lev=lev_r,$
	ytitle='!8r/R'+sunsymbol()  ,ystyle=1,xstyle=1,pos=cpos,xtitle='!6time [years]',yticks=4,$
	xr=[t0,t1]  ;title='!6b) !7dX!8/2!7p!6 [nHz], (!7h!8=20!U!6o!N!6)'
;	xr=[20,40] ;title='!6b) !7dX!8/2!7p!6 [nHz], (!7h!8=20!U!6o!N!6)'
bphi_r=reform(bx[thlev,*,*])

nlev_field=8
lev=grange(0,0.8,nlev_field)
contour,smooth(rotate(bphi_r,4),2),time,r,nl=nlev_field,/over,lev=lev,thick=1,min_value=-0.01,$
	c_colors=cgColor("white") ,c_thick=[3,3]
lev=grange(-0.8,0,nlev_field)
contour,smooth(rotate(bphi_r,4),2),time,r,nl=nlev_field,/over,lev=lev,c_linestyle=2,thick=1,max_value=0.01,$
	c_colors=cgColor("white") ,c_thick=[3,3]

colorbar,range=[min(lev_r),max(lev_r)],/vertical,xaxis=2,$
         ytickformat='(F6.0)',yticks=4,pos=bpos,$
         ytickv=[min(lev_r),0.5*(min(lev_r)+max(lev_r)),max(lev_r)]
oplot,[3.5,3.5],  [0.61,0.96],li=2,thick=6,color=cgcolor('black') 
oplot,[7.5,7.5],  [0.61,0.96],li=2,thick=6,color=cgcolor('black') 
oplot,[11.5,11.5],[0.61,0.96],li=2,thick=6,color=cgcolor('black') 
oplot,[15.5,15.5],[0.61,0.96],li=2,thick=6,color=cgcolor('black') 
oplot,[19.5,19.5],[0.61,0.96],li=2,thick=6,color=cgcolor('black') 
!p.multi=0
end
