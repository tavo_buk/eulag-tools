
default,imhd,1
default,nn,300
default,nlev,21
nt=(size(u))[4]
time=nplot*dt*indgen(nt)/year
rsinth=fltarr(m,l)
thg=where(th GE -85 AND th LT 85)

for k=0,l-1 do begin
 for j=0,m-1 do begin
  rsinth[j,k]=rr*r[k]*sin(theta[j])
 endfor
endfor

default,cycr,28.
umean=fltarr(m,l,nt)
omega_0=0.;1./(cycr*24.*3600.)
omega=fltarr(m,l,nt)
omega_mean=fltarr(m,l)
umean=total(u,1)/double(n)
umeanm=total(umean[*,*,nt-nn:*],3)/double(nn)

for k=0, l-1 do begin
  for j=1,m-2 do begin
   omega[j,k,*]=1.e9*(omega_0 + umean[j,k,*]/(2.*!pi*rsinth[j,k]))
  endfor
endfor
omega_mean[1:m-2,*]=1.e9*(omega_0+umeanm[1:m-2,*]/(2.*!pi*rsinth[1:m-2,*]))

lev=grange(320.,460.,nlev)       ;28
lev=grange(min(omega_mean),max(omega_mean),nlev)       ;28
xlength=0.45
xinterval=0.05
xpos1=xinterval
xpos2=xinterval+xlength
cpos=[xpos1,0.04,xpos2,0.94]
bpos=[0.20,0.36,0.22,0.65]
contour,transpose(omega_mean[thg,*]),x[*,thg],y[*,thg],/fi,nl=nlev,/iso,pos=cpos,$
	title=title,lev=lev,xtitle='!8x!6',ytitle='!8y!6',xstyle=4,ystyle=4,/nodata
contour,transpose(omega_mean[thg,*]),x[*,thg],y[*,thg],/fi,nl=nlev,/iso,pos=cpos,/over,lev=lev
colorbar,range=[min(lev),max(lev)],pos=bpos,/vertical,$
         ytickformat='(F6.1)',yticks=2,ytickv=[min(lev),0.5*(min(lev)+max(lev)),max(lev)],$
         xaxis=0,char=1.5,title='!7X!6/2!7p!6 (nHz)',ANNOTATECOLOR=cgColor("black")
contour,transpose(omega_mean[thg,*]),x[*,thg],y[*,thg],nl=21,/iso,/over,lev=lev,color=cgColor("black")
plots,circle(0,0,0.96),thick=3
plots,circle(0,0,0.61),thick=3
plots,circle(0,0,0.718),thick=2,li=2
oplot,[0,0],[-0.96,-0.61],thick=3,li=0
oplot,[0,0],[0.61,0.96],thick=3,li=0
end
