restore,'grid.sav'
nr=(size(r))[1]
nth=(size(th))[1]
nphi=(size(ph))[1]
file='fort.11'

n=0D
openr,lun,file,/get_lun,/f77_unformatted
while (not eof(lun)) do begin
  readu,lun,uu
  readu,lun,vv
  readu,lun,ww
  readu,lun,tt
  readu,lun,pp
  n++
endwhile
print,'number of outputs = ', n

uu=fltarr(nphi,nth,nr)
vv=fltarr(nphi,nth,nr)
ww=fltarr(nphi,nth,nr)
tt=fltarr(nphi,nth,nr)
pp=fltarr(nphi,nth,nr)

unf=fltarr(nphi,nth,nr,n)
vnf=fltarr(nphi,nth,nr,n)
wnf=fltarr(nphi,nth,nr,n)
tnf=fltarr(nphi,nth,nr,n)
pnf=fltarr(nphi,nth,nr,n)


u=fltarr(nphi,nth,nr,n)
v=fltarr(nphi,nth,nr,n)
w=fltarr(nphi,nth,nr,n)
t=fltarr(nphi,nth,nr,n)
p=fltarr(nphi,nth,nr,n)

ns=0L

openr,lun,file,/get_lun,/f77_unformatted
for ns=0,n-1 do begin
;while (not eof(lun)) do begin
  readu,lun,uu
  readu,lun,vv
  readu,lun,ww
  readu,lun,tt
  readu,lun,pp

  unf[*,*,*,ns]=uu
  vnf[*,*,*,ns]=vv
  wnf[*,*,*,ns]=ww
  tnf[*,*,*,ns]=tt
  pnf[*,*,*,ns]=pp
  print,'reading data from file: ',file, ns
  ns++
;endwhile
endfor
free_lun,lun

;filtering p and q modes
print,'filtering'
for k=1, nr-2 do begin
 u[*,*,k,*]=0.5*(unf[*,*,k,*]+unf[*,*,k+1,*])
 v[*,*,k,*]=0.5*(vnf[*,*,k,*]+vnf[*,*,k+1,*])
 w[*,*,k,*]=0.5*(wnf[*,*,k,*]+wnf[*,*,k+1,*])
 t[*,*,k,*]=0.5*(tnf[*,*,k,*]+tnf[*,*,k+1,*])
 p[*,*,k,*]=0.5*(pnf[*,*,k,*]+pnf[*,*,k+1,*])
endfor

end

;uu=total(u,4)/ns
;vv=total(v,4)/ns
;ww=total(w,4)/ns

;ufield=dblarr(nphi,nth,nr,3)
;ufield[*,*,*,0]=uu
;ufield[*,*,*,1]=vv
;ufield[*,*,*,2]=ww
