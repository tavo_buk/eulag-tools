rds=r(0)*rr     ;m
rdsi=1./rds
GG=6.674e-11   ;N(m/kg)^2
mass=1.988e30     ;kg
dz=(r[l-1]-r[0])*rr/double(l-1)
g=GG*mass/(rr*r)^2     ;m/s^2
g0=g[0]
z = indgen(l)*dz

; bottom values

rho00= 420.920
p00=   1.71889e+13
T00=   2.97382e+06
th00= 1.14*T00       

Rg=13732.
cp=2.5*rg
cap=rg/cp
capi=1./cap
ss=g0/(cp*th00)

rho_0 = rho00*(1.-(ss*z)/(1.+z*rdsi))^(capi-1)
rh2d = fltarr(l,m)

for k=0, l-1 do begin
  for j=0,m-1 do begin
    rh2d[k,j]=rho_0[k]
  endfor
endfor
