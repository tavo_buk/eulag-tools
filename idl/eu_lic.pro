@eu_setup
if (itac eq 0) then rlev=37 else rlev = 62

it=55
s=6L
bph=reform(bx[*,*,rlev,it])
bth=reform(by[*,*,rlev,it])
ufield=rotate(transpose(bph),3)
vfield=rotate(transpose(bth),3)

uf_l=rebin(ufield,n*s,m*s)
vf_l=rebin(vfield,n*s,m*s)
ph_l=rebin(ph,n*s)
th_l=rebin(th,m*s)

sigma = 3.5
mean = 0.50
array = 0.*RANDOMN(seed, n*s,m*s)
tt = byte(array * sigma + mean)

;tt=abs(noise_scatter(tt))
;tt=byte(tt)

im=vis_lic(smooth(uf_l,2),smooth(vf_l,2),texture=tt)
imn=im/max(float(im))

mag=alog(uf_l^2) 
magn=mag/max(mag)

im2=magn*imn
lev=grange(min(im2),0.7*max(im2),264)

;map_set, 0, 0, 0, color=0, /horizon,/mollweide,E_HORIZON={FILL:1, COLOR:0},$
;	 glinestyle=0, glinethick=4, /noborder,position=pos_ext,/noerase,latdel=30
contour,smooth(im,0),ph_l,th_l,/fi,nl=264,lev=lev
;contour,ufield/max(ufield),ph,th,/iso,nl=64,/fi,/over

end
