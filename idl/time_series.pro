if !d.name eq 'PS' then begin
    device,xsize=22,ysize=12,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
end
!x.margin=[9,3]
!p.charsize=1.5

!p.multi=[0,2,2] & !p.charsize=1.5 
read,imhd,prompt='press 0 for HD and 1 for MHD: '

if imhd eq 0 then begin

 tsu2 = (total((total(u2,1)/m),1)/l)-(total((total(u^2,1)/m),1)/l)
 tsv2 = (total((total(v2,1)/m),1)/l)-(total((total(v^2,1)/m),1)/l)
 tsw2 = (total((total(w2,1)/m),1)/l)-(total((total(w^2,1)/m),1)/l)
 tsp = (total((total(p,1)/m),1)/l)
 tsth = (total((total(ptemp,1)/m),1)/l)
 u_k = 0.5*rho_av*total((total((u2+v2+w2),1)/m),1)/l 

 nn = n_elements(tsu2)
 urms = sqrt(tsu2+tsv2+tsw2)
 year = 365.*24.*60.*60.
 time = (nxaver*dt*indgen(nn))/year

 plot,time,urms,xtitle='!8t!6 (years)',ytitle='!8u!D!6rms!N',thick=3,yr=[min(urms),max(urms)]
 plot,time,tsp,xtitle='!8t!6 (years)',ytitle='!8p!6',thick=3,yr=[min(tsp),max(tsp)]
 plot,time,tsth,xtitle='!8t!6 (years)',ytitle='!4h!6',thick=3,yr=[min(tsth),max(tsth)]
 plot,time,u_k,xtitle='!8t!6 (years)',ytitle='!8u!Dk!N!6',thick=3,$
      yr=[min(u_k),max(u_k)],ystyle=1

endif else begin

 tsu2 = (total((total(u2,1)/m),1)/l)-(total((total(u^2,1)/m),1)/l)
 tsv2 = (total((total(v2,1)/m),1)/l)-(total((total(v^2,1)/m),1)/l)
 tsw2 = (total((total(w2,1)/m),1)/l)-(total((total(w^2,1)/m),1)/l)
 tsbx2 = (total((total(bx2,1)/m),1)/l)-(total((total(bx^2,1)/m),1)/l)
 tsby2 = (total((total(by2,1)/m),1)/l)-(total((total(by^2,1)/m),1)/l)
 tsbz2 = (total((total(bz2,1)/m),1)/l)-(total((total(bz^2,1)/m),1)/l)
 tsp = sqrt(total((total(p^2,1)/m),1)/l)
 tsth = (total((total(ptemp,1)/m),1)/l)
 
 nn = n_elements(tsu2)
 urms = sqrt(tsu2+tsv2+tsw2)
 year = 365.*24.*60.*60.
 time = (nxaver*dt*indgen(nn))/year
 
; u_k = 0.5*rho_av*total((total((u^2+v^2+w^2),1)/m),1)/l 
 u_k = 0.5*rho_av*total((total((u2+v2+w2),1)/m),1)/l 
; u_m = 0.5*(total((total((bx^2+by^2+bz^2),1)/m),1)/l)/(4.e-7*!pi)
 u_m = 0.5*(total((total((bx2+by2+bz2),1)/m),1)/l)/(4.e-7*!pi)
 u_pol = 0.5*(total((total((by2+bz2),1)/m),1)/l)/(4.e-7*!pi)
 u_tor = 0.5*(total((total((bx2),1)/m),1)/l)/(4.e-7*!pi)

 tgood = 0.
 itime = where(time gt tgood)
 print,'time interval: t >',tgood
 print,'E_m/E_k = ',mean(u_m[itime])/mean(u_k[itime])
 print,'E_tor/E_k = ',mean(u_tor[itime])/mean(u_k[itime])
 print,'E_pol/E_k = ',mean(u_pol[itime])/mean(u_k[itime])
 
 nn = n_elements(tsu2)
 urms = sqrt(tsu2+tsv2+tsw2)
 brms = sqrt(tsbx2+tsby2+tsbz2)
 year = 365.*24.*60.*60.
 time = (nxaver*dt*indgen(nn))/year

 plot,time,urms,xtitle='!8t!6 (years)',ytitle='!8u!D!6rms!N',thick=3,$
         yr=[min(urms),max(urms)]
 plot,time,brms,/ylog,xtitle='!8t!6 (years)',ytitle='!8B!D!6rms!N',thick=3,$
	 yr=[min(brms),max(brms)]
 plot,time,tsth,xtitle='!8t!6 (years)',ytitle='!4h!6',thick=3,yr=[min(tsth),max(tsth)]
 plot,time,tsp,xtitle='!8t!6 (years)',ytitle='!8p!N!6',thick=3,$
      yr=[min(tsp),max(tsp)],ystyle=1,/ylog

endelse 

save,filename='brms.sav',brms,time
!p.multi=0
end
