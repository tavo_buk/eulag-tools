FUNCTION CIRCLE, xcenter, ycenter, radius
; therange=therange

;if (keyword_set(therange)) then begin
;  thi=therange(0)
;  the=therange(1)
;endif else begin  
;  thi = 37*!pi/180
;  the = 53*!pi/180
;endelse

;;points = thi + (the-thi)*findgen(100)/99
points = 0.5*!PI-(!PI / (99.) ) * FINDGEN(100)
x = xcenter + radius * COS(points )
y = ycenter + radius * SIN(points )
RETURN, TRANSPOSE([[x],[y]])
end
