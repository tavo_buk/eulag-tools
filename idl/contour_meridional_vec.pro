;--------------------------------------------------------------------------------
;Contour plot of vector field in meridional plane.
; The poloidal component is plotted as arrows, the toroidal component
; is plotted in contour.  
;---------------------------------------------------------------------------------
PRO contour_meridional_vec,aa,r,theta,overplot=overplot,plot_title=plot_title,nlevels=nlevels,pos=pos,lev=lev
compile_opt IDL2,hidden
;-------set default values ---------------
default,overplot,'nooverplot'
default,plot_title,''
default,nlevels,32
; ----toroidal component -----------
psi=aa[*,*,2]
;------range and levels -------------
;
maxpsi=max(psi)
minpsi=min(psi)
drange=(maxpsi-minpsi)/(nlevels-1)
lev=minpsi+findgen(nlevels)*drange
;
;----select colortable --------------
;loadct,5,ncolors=nlevels, bottom=3
; -----set range of colors ----------
contour,psi,$
      r#sin(theta),r#cos(theta),$
      levels=lev,/fil,$
      C_colors=indgen(nlevels)+3,$
;      color=!P.Background, background=!P.Color,$
      xstyle=1,ystyle=1,/isotropic,$
      tit=plot_title,$
      xtitle='!8x!6',ytitle='!8z!6'
      POSITION=[0.15, 0.15, 0.85, 0.90]
;
;COLORBAR, /vertical,/right,POSITION=[0.90, 0.15, 0.92, 0.9],$
;      range=[min(lev),max(lev)],levels=lev,charsize=0.8,$
;      FORMAT='(F6.3)' , color=!P.Background, background=!P.Color,$
;    ncolors=nlevels, bottom=3
;------- vector plot of poloidal component -----------------
;
dimension=size(aa)
nx=dimension[1]
ny=dimension[2]
ux=findgen(nx,ny)
uy=findgen(nx,ny)
xcoord=findgen(nx,ny)
ycoord=findgen(nx,ny)
for ix=0,nx-1 do begin
  for iy=0,ny-1 do begin
    ux[ix,iy]=aa[ix,iy,0]*sin(theta[iy])+aa[ix,iy,1]*cos(theta[iy])
    uy[ix,iy]=aa[ix,iy,0]*cos(theta[iy])-aa[ix,iy,1]*sin(theta[iy])
    xcoord[ix,iy] = r[ix]*sin(theta[iy])
    ycoord[ix,iy] = r[ix]*cos(theta[iy])
  end
end
;
partvelvec,ux,uy,xcoord,ycoord,fraction=0.05,/over,length=0.06,col=255
;
;-------------------------------------------------
END
