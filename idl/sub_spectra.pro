print, 'setting cutoff wavenumbers in z direction ...'

kzmin=2*pi/Lz
kzmax=nz*kzmin

kxmin=2*pi/Lx
kxmax=nx*kxmin

kx=[indgen(nx/2+1),-reverse(indgen(nx/2-1)+1)]*kxmin
kz=[indgen(nz/2+1),-reverse(indgen(nz/2-1)+1)]*kzmin
ks=indgen(nz/2)

len=size(u)
nt=len[4]

umod = fltarr(nx,nz,nt)
wmod = fltarr(nx,nz,nt)

print, 'reshaping arrays ...'
for it=0,nt-1 do begin
 for ix=0,nx-1 do begin
  for iz=0,nz-1 do begin
   umod[ix,iz,it] = u[ix,0,iz,it]
   wmod[ix,iz,it] = w[ix,0,iz,it]
  endfor
 endfor
endfor

ftu = fltarr(nx,nz,nt)
ftw = fltarr(nx,nz,nt)
E = fltarr(n_elements(ks))

init=200
print, 'Computing fft from ', nt-init-1
print, '.... until last time step ', nt-1

for it=nt-init-1,nt-1 do begin
 print, 'computing 2D fft for temporal slice', it
 ftu[*,*,it] = fft(umod[*,*,it],/double)
 ftw[*,*,it] = fft(wmod[*,*,it],/double)
endfor

print, 'computing energy amplitudes ...'
for it=nt-init-1,nt-1 do begin
 for ix=0,nx-1 do begin
  for iz=0,nz-1 do begin
   k=round(sqrt(kx[ix]^2+kz[iz]^2))
   if(k lt n_elements(ks)) then begin
    E[k]=E[k]+abs(ftu[ix,iz,it])^2+abs(ftw[ix,iz,it])^2
   endif
  endfor
 endfor
endfor

Er=E/double(nx*init)

kzlist=fltarr(nz)
for iz=0, nz-1 do begin
 kzlist[iz]=kzmin*(iz+1)
endfor


