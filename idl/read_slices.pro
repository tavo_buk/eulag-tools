default,field,'w'
default,extension,'xy'
default,sindx,'04'
default,png,0

!p.background=0
!p.color=255

if (extension EQ 'xy') then begin
  filename = field+extension+sindx
  arr = dblarr(n,m)
endif

if (extension EQ 'yz') then begin
  filename = field+extension+sindx
  arr = dblarr(m,l)
endif


if (extension EQ 'xz') then begin
  filename = field+extension+sindx
  arr = dblarr(n,l)
endif

t = 0.D
openr,1,filename,/f77
ns = 0L
while (not eof(1)) do begin
  readu,1,arr,t
  if (ns LE 1) then begin
    aarr = arr 
    print,'Reading file: ',filename
  endif else begin
    aarr = [[[aarr]],[[arr]]]
  endelse
  wait,0.1
  ns ++
end
close,1

nn = (size(aarr))[3]
year = 365.*24.*60.*60.
fo = "(F5.2)"

min = min(aarr)
max = max(aarr)
print,min,max

;if (sindx EQ '01') then fr=string(r[60],format=fo)
;if (sindx EQ '02') then fr=string(r[100],format=fo)
;if (sindx EQ '03') then fr=string(r[160],format=fo)
;if (sindx EQ '04') then fr=string(r[180],format=fo)
;if (sindx EQ '01') then fr=string(r[20],format=fo)
;if (sindx EQ '02') then fr=string(r[50],format=fo)
;if (sindx EQ '03') then fr=string(r[85],format=fo)
;if (sindx EQ '04') then fr=string(r[89],format=fo)
if (sindx EQ '01') then fr=string(r[20],format=fo)
if (sindx EQ '02') then fr=string(r[30],format=fo)
if (sindx EQ '03') then fr=string(r[40],format=fo)
if (sindx EQ '04') then fr=string(r[44],format=fo)

nlev=128
thg=where(th GE -89 AND th LT 89)
nav=5
levav = total(aarr[*,thg,nn-(nav+1):nn-nav],3)/nav
lev = 2.*max(abs(levav))*indgen(nlev)/float(nlev-1) $
        -max(abs(levav))
lev=grange(min(levav),max(levav),128)
print,minmax(lev)
help,aarr
end
