if !d.name eq 'PS' then begin
    device,xsize=10,ysize=22,yoffset=3
    !p.thick=2 & !x.thick=1 & !y.thick=1
end
;!p.multi=[0,1,1] & !p.charsize=1.5 

nlev=32
xlength=0.95
xinterval=0.05
bini=0.10
bint=0.24
xpos1=xinterval
xpos2=xinterval+xlength

base='/home/guerrero/eumag/'
dir1='tac-28-rf-hd/'
dir2='tac-28-rf-hd/'
dir3='tac-28-rf-hd/'

file1='grid.sav'
file2='ptemp.sav'
restore,base+dir1+file1
restore,base+dir1+file2
nth=(size(th))[1]
th=!pi*(90.+th)/180.
thgood=indgen(nth/2)
x=r#sin(th[thgood])
y=r#cos(th[thgood])
lev = 2.*max(abs(pt0))*indgen(nlev)/float(nlev-1)-max(abs(pt0))
lev=grange(min(pt0),max(pt0),nlev)

cpos=[xpos1,0.05,xpos2,.95]
bpos=[bini,0.31,bini+0.01,0.63]
contour,pt0[*,thgood],x[*,*],y[*,*],/fi,nl=64,/iso,lev=lev,xstyle=4,ystyle=4,$
        title="a) L6"
plots,circle2(0,0,0.96),thick=6
plots,circle2(0,0,0.61),thick=6
plots,circle2(0,0,0.718),thick=3,li=2
oplot,[0.96,0.61],[0,0],thick=6,li=0
oplot,[0,0],[0.61,0.96],thick=6,li=0

cont=0
if (cont eq 1) then begin

restore,base+dir2+file1
restore,base+dir2+file2
nth=(size(th))[1]
th=!pi*(90.+th)/180.
thgood=indgen(nth/2)
print,thgood
x=r#sin(th[thgood])
y=r#cos(th[thgood])
xpos1=xpos2+xinterval
xpos2=xpos1+xlength
cpos=[xpos1,0.04,xpos2,0.96]
;lev = 2.*max(abs(pt0))*indgen(nlev)/float(nlev-1)-max(abs(pt0))
contour,pt0[*,thgood],x,y,/fi,nl=64,/iso,lev=lev,xstyle=4,ystyle=4,$
        title="b) M1",pos=cpos
plots,circle2(0,0,0.96),thick=6
plots,circle2(0,0,0.61),thick=6
plots,circle2(0,0,0.718),thick=3,li=2
oplot,[0.96,0.61],[0,0],thick=6,li=0
oplot,[0,0],[0.61,0.96],thick=6,li=0

restore,base+dir3+file1
restore,base+dir3+file2
nth=(size(th))[1]
thgood=indgen(nth/2)
th=!pi*(90.+th)/180.
x=r#sin(th[thgood])
y=r#cos(th[thgood])
xpos1=xpos2+xinterval
xpos2=xpos1+xlength
cpos=[xpos1,0.04,xpos2,0.96]
;lev = 2.*max(abs(pt0))*indgen(nlev)/float(nlev-1)-max(abs(pt0))
contour,pt0[*,thgood],x,y,/fi,nl=64,/iso,lev=lev,xstyle=4,ystyle=4,$
        title="c) H1",pos=cpos
plots,circle2(0,0,0.96),thick=6
plots,circle2(0,0,0.61),thick=6
plots,circle2(0,0,0.718),thick=3,li=2
oplot,[0.96,0.61],[0,0],thick=6,li=0
oplot,[0,0],[0.61,0.96],thick=6,li=0
endif

end
