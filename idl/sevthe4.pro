; mv idl.ps ~/Dropbox/Tex/paper.nssl/fig/sevthe.eps
if !d.name eq 'PS' then begin
    device,xsize=32,ysize=12,yoffset=3
    !p.thick=2 & !x.thick=1 & !y.thick=1
end
!p.multi=[0,2,1]
@eu_read_bcprofl
!p.charsize=1.7
!x.margin=[13,4]

basedir='~/eulag/'
dir1=basedir+'/euHD.1slices/'
;dir1=basedir+'/nssl_2xbslices/'

mt=1e6
restore,dir1+'the.sav'
restore,dir1+'grid.sav'
restore,dir1+'velocity.sav'
nr=(size(pt0))[1]
nth=(size(pt0))[2]
th_eq=where(th gt -15. and th lt 15.)
ntheq=(size(th_eq))[1]
th_pole=where(th lt -60. or th gt 60)  
nthpo=(size(th_pole))[1]

yr=[max(the1/mt)-0.000012,max(the1/mt)+0.000002] 
print,yr
plot,[0.6,1],[yr[0],yr[0]],yr=yr,li=4, /nodata, charsize=1.3,xr=[0.71,0.995], $
     ystyle=1,ytitle='!7h!6!De!N+<!7h!6`> (!810!U6!N!6 K)',xstyle=1,$
     xtitle='!8r/R!6'+sunsymbol()
th1d_eq=total(pt0[*,th_eq],2)/double(ntheq)
th1d_po=total(pt0[*,th_pole],2)/double(nthpo)
oplot,r,(the1+th1d_eq)/mt,li=0
;oplot,r,(the1+th1d_po)/mt,li=2
oplot,r,the1/mt,li=1

;restore,dir2+'velocity.sav'
;th1d_eq=total(pt0[*,th_eq],2)/double(ntheq)
;oplot,r,(the1+th1d_eq)/mt,li=2

xyouts,0.8,3.878045,'EQUATOR'

oplot,[0.74,0.78],[3.878039,3.878039],li=1,thick=4
xyouts,0.785,3.8780388,'!7h!6!De!N'
oplot,[0.74,0.78],[3.878038,3.878038],li=3,thick=4
xyouts,0.785,3.8780378,'!80.8!6y'
oplot,[0.74,0.78],[3.878037,3.878037],li=2,thick=4
xyouts,0.785,3.8780368,'!82.5!6y'
oplot,[0.74,0.78],[3.878036,3.878036],li=0,thick=4
xyouts,0.785,3.8780358,'!85!6y'
oplot,[0.74,0.78],[3.878035,3.878035],li=4,thick=4
xyouts,0.785,3.8780348,'!88!6y'

restore,dir1+'velocity.sav'
th1d_po=total(pt0[*,th_pole],2)/double(nthpo)
plot,[0.6,1],[yr[0],yr[0]],yr=yr,li=4, /nodata, charsize=1.3,xr=[0.71,0.995], $
     ystyle=1,xstyle=1,$
     xtitle='!8r/R!6'+sunsymbol()
oplot,r,the1/mt,li=1
oplot,r,(the1+th1d_po)/mt,li=0

;restore,dir2+'velocity.sav'
;th1d_po=total(pt0[*,th_pole],2)/double(nthpo)
;oplot,r,(the1+th1d_po)/mt,li=2

xyouts,0.8,3.878045,'POLES'

oplot,[0.74,0.78],[3.878039,3.878039],li=1,thick=4
xyouts,0.785,3.8780388,'!7h!6!De!N'
oplot,[0.74,0.78],[3.878038,3.878038],li=3,thick=4
xyouts,0.785,3.8780378,'L10'
oplot,[0.74,0.78],[3.878037,3.878037],li=2,thick=4
xyouts,0.785,3.8780368,'L11'
oplot,[0.74,0.78],[3.878036,3.878036],li=0,thick=4
xyouts,0.785,3.8780358,'L6'
oplot,[0.74,0.78],[3.878035,3.878035],li=4,thick=4
xyouts,0.785,3.8780348,'L12'

end
