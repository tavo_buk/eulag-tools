if !d.name eq 'PS' then begin
    device,xsize=27,ysize=8.5,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
end
loadct,24, FILE=colorFile
!p.multi=[0,5,2] 
!p.charsize=1.7 & !x.margin=[8,3] & !y.margin=[3,3]
!P.Color =cgColor("black")
!P.Background =cgColor("white")

default,imhd,1
default,cycr,28.
nt=(size(time))[1]
thg=where(th GE -80 AND th LT 80)

; ************************
;reading stratification
openr, lun, 'strat.dat', /get_lun
data=fltarr(7,l)
readf, lun, data
rho_s=reform(data(4,*))
close,lun

umeanm=total(umean,3)/double(nt)
rsinth=fltarr(m,l)
omega_0=2.*!pi/(cycr*24.*3600.)
omega=fltarr(m,l,nt)
omega_mean=fltarr(m,l)
tosc=fltarr(m,l,nt)
for k=0,l-1 do begin
 for j=0,m-1 do begin
  rsinth[j,k]=rr*r[k]*sin(theta[j])
 endfor
endfor


for k=0, l-1 do begin
  for j=1,m-2 do begin
   omega[j,k,*]=(omega_0 + umean[j,k,*]/(rsinth[j,k]))
  endfor
endfor
omega_mean[1:m-2,*]=(omega_0+umeanm[1:m-2,*]/(rsinth[1:m-2,*]))

for it=0,nt-1 do begin
 tosc[*,*,it]=omega[*,*,it]-omega_mean[*,*]
endfor

; divergence r component
; hydro terms
dudt=fltarr(m,l,nt)
div_frMC=fltarr(m,l,nt)
div_frRS=fltarr(m,l,nt)
div_fthMC=fltarr(m,l,nt)
div_fthRS=fltarr(m,l,nt)
; magnetic terms
if (imhd ne 0) then begin
 div_frMT=fltarr(m,l,nt)
 div_frMS=fltarr(m,l,nt)
 div_fthMT=fltarr(m,l,nt)
 div_fthMS=fltarr(m,l,nt)
endif

for it=0,nt-1 do begin
 for j = 0,m-1 do begin 
  div_frMC[j,*,it]=deriv((r*rr),(r*rr)^2*reform(frMC[j,*,it]))/(r*rr)^2
  div_frRS[j,*,it]=deriv((r*rr),(r*rr)^2*reform(frRS[j,*,it]))/(r*rr)^2
  if (imhd ne 0) then begin
   div_frMT[j,*,it]=deriv((r*rr),(r*rr)^2*reform(frMT[j,*,it]))/(r*rr)^2
   div_frMS[j,*,it]=deriv((r*rr),(r*rr)^2*reform(frMS[j,*,it]))/(r*rr)^2
  endif
 endfor
endfor

for it=0,nt-1 do begin
 for k = 0,l-1 do begin 
  div_fthMC[1:m-2,k,it]=deriv(theta[1:m-2],sin(theta[1:m-2])*reform(fthMC[1:m-2,k,it]))/rsinth[1:m-2,k]
  div_fthRS[1:m-2,k,it]=deriv(theta[1:m-2],sin(theta[1:m-2])*reform(fthRS[1:m-2,k,it]))/rsinth[1:m-2,k]
  if (imhd ne 0) then begin
   div_fthMT[1:m-2,k,it]=deriv(theta[1:m-2],sin(theta[1:m-2])*reform(fthMT[1:m-2,k,it]))/rsinth[1:m-2,k]
   div_fthMS[1:m-2,k,it]=deriv(theta[1:m-2],sin(theta[1:m-2])*reform(fthMS[1:m-2,k,it]))/rsinth[1:m-2,k]
  endif
 endfor
endfor

;time integrals

;time interval 1
int_div_frMC=fltarr(m,l)
int_div_frRS=fltarr(m,l)
int_div_fthMC=fltarr(m,l)
int_div_fthRS=fltarr(m,l)
if (imhd ne 0) then begin
 int_div_frMT=fltarr(m,l)
 int_div_frMS=fltarr(m,l)
 int_div_fthMT=fltarr(m,l)
 int_div_fthMS=fltarr(m,l)
 int_all=fltarr(m,l)
endif
int_dudt=fltarr(m,l)
div_fall=div_frMC+div_frRS+div_fthMC+div_fthRS $
	+div_frMT+div_frMS+div_fthMT+div_fthMS

g1=where(time ge 3.5 and time le 7.5) 

print,'ng1 = ',n_elements(g1)
for k=0,l-1 do begin
 for j=1,m-2 do begin
  ;time integral hydro terms
  dudt[j,k,*]=deriv(year*time[*],smooth(reform(umean[j,k,*]),2))
  int_dudt[j,k]=mean(reform(tosc[j,k,g1]))
  int_div_frMC[j,k] =mean(reform( div_frMC[j,k,g1])) 
  int_div_frRS[j,k] =mean(reform( div_frRS[j,k,g1]))
  int_div_fthMC[j,k]=mean(reform(div_fthMC[j,k,g1])) 
  int_div_fthRS[j,k]=mean(reform(div_fthRS[j,k,g1]))
  ;mhd terms
  if (imhd ne 0) then begin
   int_div_frMT[j,k] =mean(reform( div_frMT[j,k,g1]))
   int_div_frMS[j,k] =mean(reform( div_frMS[j,k,g1]))
   int_div_fthMT[j,k]=mean(reform(div_fthMT[j,k,g1]))
   int_div_fthMS[j,k]=mean(reform(div_fthMS[j,k,g1]))
   int_all[j,k]=      mean(reform( div_fall[j,k,g1]))
  endif
 endfor
endfor

if (imhd ne 0) then begin
 divALL=-transpose(int_all)/1.e5
 divMC= -transpose(int_div_frMC[*,*]+int_div_fthMC[*,*])/1.0e+5 
 divRS= -transpose(int_div_frRS[*,*]+int_div_fthRS[*,*])/1.0e+5
 divMT= -transpose(int_div_frMT[*,*]+int_div_fthMT[*,*])/1.0e+5
 divMS= -transpose(int_div_frMS[*,*]+int_div_fthMS[*,*])/1.0e+5
 meanTO= int_dudt/1e-7

 levmc=grange(-max(divMC),max(divMC),64)
; levmt=grange(-max(divMT),max(divMT),64)/20.
 levmc=grange(-3.,3.,64)
 levmt=grange(-1.2,1.2,64)
 levdd=grange(-max(meanTO),max(meanTO),64)/3.
 levdd=grange(-1.2,1.2,64)
 levall=1.5*grange(-1.,1.,64)


xlength=0.14
xinterval=0.055
bini=0.08
bint=0.24
xpos1=xinterval
xpos2=xinterval+xlength

grad='!9'+String(71B)+'!6'
cpos=[xpos1,0.03,xpos2,0.94]
bpos=[bini,0.34,bini+0.01,0.63]
contour,smooth(divMC[*,thg],2),x[*,thg],y[*,thg],/fi,nl=64,/iso,pos=cpos,$
        lev=levmc,xtitle='!8x!6',ytitle='!8y!6',xstyle=4,ystyle=4,title='-'+grad+'!U.!N!8!S!A-!R!NF!D!6MC!N!6'
plots,circle(0,0,0.96),thick=6
plots,circle(0,0,0.61),thick=6
plots,circle(0,0,0.718),thick=2,li=2
oplot,[0,0],[-0.96,-0.61],thick=6,li=0
oplot,[0,0],[0.61,0.96],thick=6,li=0
plots,circle(0,0,0.81),thick=2,li=2
plots,circle(0,0,0.88),thick=2,li=2
oplot,[0.647169,0.875046],[0.292014,0.394836],thick=2,li=2
oplot,[0.456379,0.617076],[0.543892,0.735403],thick=2,li=2
oplot,[0.209276,0.282965],[0.678457,0.917350],thick=2,li=2
oplot,[0.71,0.96],[0.,0.],thick=2,li=2

xyouts,0.0,0.01,'!6(!7D!8t!D1!N!6)',charsize=2
;
xpos1=xpos2+xinterval
xpos2=xpos1+xlength
bini=xpos1+0.025

cpos=[xpos1,0.03,xpos2,0.94]
bpos=[bini,0.31,bini+0.01,0.63]
contour,smooth(divRS[*,thg],2),x[*,thg],y[*,thg],/fi,nl=nlev,/iso,pos=cpos,$
	xtitle='!8x!6',xstyle=4,ystyle=4,lev=levmt,title='-'+grad+'!U.!N!8!S!A-!R!NF!D!6RS!N!6'
colorbar,range=[min(levmc),max(levmc)],pos=bpos,/vertical,$
         ytickformat='(F6.1)',yticks=2,ytickv=[min(levmc),0.5*(min(levmc)+max(levmc)),max(levmc)],$
         xaxis=0,char=2,title='!6(!810!U5!N !6Kg m!U-1!N s!U-2!N!6) ',ANNOTATECOLOR=cgColor("black")
plots,circle(0,0,0.96),thick=6
plots,circle(0,0,0.61),thick=6
plots,circle(0,0,0.718),thick=2,li=2
oplot,[0,0],[-0.96,-0.61],thick=6,li=0
oplot,[0,0],[0.61,0.96],thick=6,li=0
;
xpos1=xpos2+xinterval
xpos2=xpos1+xlength
bini=xpos1+0.025

cpos=[xpos1,0.03,xpos2,0.94]
bpos=[bini,0.31,bini+0.01,0.63]
contour,smooth(divMT[*,thg],2),x[*,thg],y[*,thg],/fi,nl=nlev,/iso,pos=cpos,$
	xtitle='!8x!6',xstyle=4,ystyle=4,lev=levmt,title='-'+grad+'!U.!N!8!S!A-!R!NF!D!6MT!N!6'
colorbar,range=[min(levmt),max(levmt)],pos=bpos,/vertical,$
         ytickformat='(F6.1)',yticks=2,ytickv=[min(levmt),0.5*(min(levmt)+max(levmt)),max(levmt)],$
         xaxis=0,char=2,title='!6(!810!U5!N !6Kg m!U-1!N s!U-2!N !6) ',ANNOTATECOLOR=cgColor("black")
plots,circle(0,0,0.96),thick=6
plots,circle(0,0,0.61),thick=6
plots,circle(0,0,0.718),thick=2,li=2
oplot,[0,0],[-0.96,-0.61],thick=6,li=0
oplot,[0,0],[0.61,0.96],thick=6,li=0
;
xpos1=xpos2+xinterval
xpos2=xpos1+xlength
bini=xpos1+0.025

cpos=[xpos1,0.03,xpos2,0.94]
bpos=[bini,0.31,bini+0.01,0.63]
contour,smooth(divALL[*,thg],2),x[*,thg],y[*,thg],/fi,nl=nlev,/iso,pos=cpos,$
	xtitle='!8x!6',xstyle=4,ystyle=4,lev=levall,title='-'+grad+'!U.!N!8!S!A-!R!NF!D!6total!N!6'
colorbar,range=[min(levall),max(levall)],pos=bpos,/vertical,$
         ytickformat='(F6.1)',yticks=2,ytickv=[min(levall),0.5*(min(levall)+max(levall)),max(levall)],$
         xaxis=0,char=2,title='!6(!810!U5!N !6Kg m!U-1!N s!U-2!N !6) ',ANNOTATECOLOR=cgColor("black")
plots,circle(0,0,0.96),thick=6
plots,circle(0,0,0.61),thick=6
plots,circle(0,0,0.718),thick=2,li=2
oplot,[0,0],[-0.96,-0.61],thick=6,li=0
oplot,[0,0],[0.61,0.96],thick=6,li=0

xpos1=xpos2+xinterval
xpos2=xpos1+xlength
bini=xpos1+0.025
loadct,4
cpos=[xpos1,0.03,xpos2,0.94]
bpos=[bini,0.34,bini+0.01,0.63]
ti='!6!S!A-!R!N!7dX!6'
contour,smooth(transpose(meanTO[thg,*]),2),x[*,thg],y[*,thg],/fi,nl=nlev,/iso,pos=cpos,$
;        title='!8Fz!N!6 ',$
	xtitle='!8x!6',xstyle=4,ystyle=4,lev=levdd,color=cgColor("black"),title=ti
colorbar,range=[min(levdd),max(levdd)],pos=bpos,/vertical,$
         ytickformat='(F6.1)',yticks=2,ytickv=[min(levdd),0.5*(min(levdd)+max(levdd)),max(levdd)],$
         xaxis=0,char=2 ,title='!6(!810!U-7!N !6Hz!6) '
plots,circle(0,0,0.96),thick=6,color=cgColor("black")
plots,circle(0,0,0.61),thick=6,color=cgColor("black")
plots,circle(0,0,0.718),thick=2,li=2,color=cgColor("black")
oplot,[0,0],[-0.96,-0.61],thick=6,li=0,color=cgColor("black")
oplot,[0,0],[0.61,0.96],thick=6,li=0,color=cgColor("black")
plots,circle(0,0,0.81),thick=2,li=2
plots,circle(0,0,0.88),thick=2,li=2
oplot,[0.647169,0.875046],[0.292014,0.394836],thick=2,li=2
oplot,[0.456379,0.617076],[0.543892,0.735403],thick=2,li=2
oplot,[0.209276,0.282965],[0.678457,0.917350],thick=2,li=2
oplot,[0.71,0.97],[0.,0.],thick=2,li=2

xyouts,0.98,0.25,'!6R11, R12, R13!6 ',charsize=0.81
xyouts,0.81,0.57,'!6R21, R22, R23!6',charsize=0.81
xyouts,0.50,0.88,'!6R31, R32, R33!6',charsize=0.81



endif else begin
; hydro case
 levmc=grange(-max(int_div_fthMC),max(int_div_fthMC),64)/1.
 levrs=grange(-max(int_div_fthRS),max(int_div_fthRS),64)/1.
 contour,transpose(smooth(int_div_frMC[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmc,xstyle=4,ystyle=4 
 contour,transpose(smooth(int_div_fthMC[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmc,xstyle=4,ystyle=4 
 contour,transpose(smooth(int_div_frRS[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levrs,xstyle=4,ystyle=4
 contour,transpose(smooth(int_div_fthRS[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levrs, xstyle=4,ystyle=4
 int_all = -int_div_fthMC - int_div_fthRS - int_div_frMC - int_div_frRS
 
 levdd=grange(-max(int_dudt),max(int_dudt),64)/0.5
 contour,transpose(smooth(int_all[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=10*levmt,xstyle=4,ystyle=4
 contour,transpose(smooth(int_dudt[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levdd,xstyle=4,ystyle=4    
endelse

;g2=where(time ge 21 and time le 27) 
;print,g2
;for j=1,m-2 do begin
; for k=0,l-1 do begin
;  ;time integral r terms
;  int_div_frMC[j,k]=int_tabulated(time[g2],reform(div_frMC[j,k,g2]))/(rho_s[k]*rsinth[j,k]) 
;  int_div_frRS[j,k]=int_tabulated(time[g2],reform(div_frRS[j,k,g2]))/(rho_s[k]*rsinth[j,k])
;  int_div_frMT[j,k]=int_tabulated(time[g2],reform(div_frMT[j,k,g2]))/(rho_s[k]*rsinth[j,k])
;  int_div_frMS[j,k]=int_tabulated(time[g2],reform(div_frMS[j,k,g2]))/(rho_s[k]*rsinth[j,k])
;  ;theta terms
;  int_div_fthMC[j,k]=int_tabulated(time[g2],reform(div_fthMC[j,k,g2]))/(rho_s[k]*rsinth[j,k]) 
;  int_div_fthRS[j,k]=int_tabulated(time[g2],reform(div_fthRS[j,k,g2]))/(rho_s[k]*rsinth[j,k])
;  int_div_fthMT[j,k]=int_tabulated(time[g2],reform(div_fthMT[j,k,g2]))/(rho_s[k]*rsinth[j,k])
;  int_div_fthMS[j,k]=int_tabulated(time[g2],reform(div_fthMS[j,k,g2]))/(rho_s[k]*rsinth[j,k])
; endfor
;endfor
;contour,transpose(smooth(int_div_fthMC[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmc 
;contour,transpose(smooth(int_div_fthRS[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmt 
;contour,transpose(smooth(int_div_fthMT[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmt
;int_all = -int_div_fthMC - int_div_fthRS + int_div_fthMT + int_div_fthMS $
;          -int_div_frMC - int_div_frRS + int_div_frMT + int_div_frMS
;;contour,transpose(smooth(int_div_fthMS[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmt
;contour,transpose(smooth(int_all[thg,*],2)),x[*,thg],y[*,thg],/fi,nl=64,/iso,lev=levmt


!p.multi=0
end
