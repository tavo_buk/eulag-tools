@eu_setup 

filename = 'xaverages.dat'
uu = dblarr(m,l)
vv = dblarr(m,l)
ww = dblarr(m,l)
oox = dblarr(m,l)
ooy = dblarr(m,l)
ooz = dblarr(m,l)
uu2 = dblarr(m,l)
vv2 = dblarr(m,l)
ww2 = dblarr(m,l)
oox2 = dblarr(m,l)
ooy2 = dblarr(m,l)
ooz2 = dblarr(m,l)
rrwv = dblarr(m,l)
rrwu = dblarr(m,l)
rrvu = dblarr(m,l)
pp= dblarr(m,l)
tth = dblarr(m,l)

t = 0L
openr,1,filename,/f77
ns = 0L
while (not eof(1)) do begin
  readu,1,uu,t
  readu,1,vv,t
  readu,1,ww,t
  readu,1,oox,t
  readu,1,ooy,t
  readu,1,ooz,t
  readu,1,uu2,t
  readu,1,vv2,t
  readu,1,ww2,t
  readu,1,oox2,t
  readu,1,ooy2,t
  readu,1,ooz2,t
  readu,1,rrwv,t
  readu,1,rrwu,t
  readu,1,rrvu,t
  readu,1,pp,t
  readu,1,tth,t
  if (ns LT 1) then begin
    u = uu  
    v = vv
    w = ww
    ox = oox  
    oy = ooy
    oz = ooz
    u2 = uu2
    v2 = vv2
    w2 = ww2
    ox2 = oox2
    oy2 = ooy2
    oz2 = ooz2
    rwv = rrwv
    rwu = rrwu
    rvu = rrvu
    p = pp
    th = tth
    print,'Reading file: ',filename, ns
  endif else begin
    u = [[[u]],[[uu]]]
    v = [[[v]],[[vv]]]
    w = [[[w]],[[ww]]]
    ox = [[[ox]],[[oox]]]
    oy = [[[oy]],[[ooy]]]
    oz = [[[oz]],[[ooz]]]
    u2 = [[[u2]],[[uu2]]]
    v2 = [[[v2]],[[vv2]]]
    w2 = [[[w2]],[[ww2]]]
    ox2 = [[[ox2]],[[oox2]]]
    oy2 = [[[oy2]],[[ooy2]]]
    oz2 = [[[oz2]],[[ooz2]]]
    rwv = [[[rwv]],[[rrwv]]]
    rwu = [[[rwu]],[[rrwu]]]
    rvu = [[[rvu]],[[rrvu]]]
    p = [[[p]],[[pp]]]
    th = [[[th]],[[tth]]]
    print,'Reading file: ',filename, ns
  endelse
  wait,0.
  ns ++
end
close,1
ptemp = th
help,th,ptemp
end
