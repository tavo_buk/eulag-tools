; mv idl.ps ~/Dropbox/Tex/paper.nssl/fig/theprof.eps
if !d.name eq 'PS' then begin
    device,xsize=22,ysize=12,yoffset=3
    !p.thick=2 & !x.thick=1 & !y.thick=1
end
!x.margin=[6,3] & !y.margin=[4,3]
!p.multi=[0,2,1]

dir1='/nobackupp8/gaguerre/eumag/NOTAC/nt-14-rf/'
dir2='/nobackupp8/gaguerre/eumag/NOTAC/nt-28-rf/'
dir3='/nobackupp8/gaguerre/eumag/NOTAC/nt-56-rf/'
itac=0
print,'itac = ',itac
file_grid='grid.sav'
file_a='velocity.sav'
restore,dir1+file_grid
restore,dir1+file_a
m=(size(rurms2))[2]
l=(size(r))[1]
@rhoprof
m=(size(rurms2))[2]
rgood=where(r ge 0.72)
Hrho=-(1./(deriv(r,rh)/rh))*6.96e8
Hrhom=mean(Hrho[rgood])
urms_av=sqrt(mean(rurms2[rgood,*]))
tau=Hrhom/urms_av
eta_t = tau*(total(rurms2,2)/double(m))/3.
eta_t_av = mean(eta_t[rgood])
ithp=where(th ge -70 and th le -50)
nthp=(size(ithp))[1]
omp=total(omega[*,ithp],2)/double(nthp)
Cwpr=(2.*!pi*1e-9*rr^2/eta_t_av)*deriv(r,omp)

ithe=where(th ge -30 and th le -10)
nthe=(size(ithe))[1]
ome=total(omega[*,ithe],2)/double(nthe)
Cwer=(2.*!pi*1e-9*rr^2/eta_t_av)*deriv(r,ome)
print,'eta_t, Cwr_pole, Cwr_eq '
print,eta_t_av, max(abs(Cwpr[3:l-2])),max(abs(Cwer[3:l-2]))
plot,r[1:l-2],Cwpr[1:l-2],xrange=[0.61,0.96],charsize=1.5,xstyle=1, $
	ystyle=1,thick=5,xtitle='!8r/R!6'+sunsymbol(),yr=2.*!pi*[-220,130],$
	title='c) !9d!D!8r!N!4X!8R'+sunsymbol()+'!U3!N/!4g!6!Dt0!N!6, CZ',/nodata,xticks=4
oplot,r[1:l-2],smooth(Cwpr[1:l-2],2),li=2,thick=10,color=cgColor('red')
oplot,r[1:l-2],smooth(Cwer[1:l-2],2),li=0,thick=10,color=cgColor('red')


restore,dir2+file_a
@rhoprof
m=(size(rurms2))[2]
rgood=where(r ge 0.72)
Hrho=-(1./(deriv(r,rh)/rh))*6.96e8
Hrhom=mean(Hrho[rgood])
urms_av=sqrt(mean(rurms2[rgood,*]))
tau=Hrhom/urms_av
eta_t = tau*(total(rurms2,2)/double(m))/3.
eta_t_av = mean(eta_t[rgood])

ithp=where(th ge -70 and th le -50)
nthp=(size(ithp))[1]
omp=total(omega[*,ithp],2)/double(nthp)
Cwpr=(2.*!pi*1e-9*rr^2/eta_t_av)*deriv(r,omp)

ithe=where(th ge -30 and th le -10)
nthe=(size(ithe))[1]
ome=total(omega[*,ithe],2)/double(nthe)
Cwer=(2.*!pi*1e-9*rr^2/eta_t_av)*deriv(r,ome)
print,'eta_t, Cwr_pole, Cwr_eq '
print,eta_t_av, max(abs(Cwpr[3:l-2])),max(abs(Cwer[3:l-2]))

oplot,r[1:l-2],smooth(Cwpr[1:l-2],2),li=2,thick=10,color=cgColor('blue')
oplot,r[1:l-2],smooth(Cwer[1:l-2],2),li=0,thick=10,color=cgColor('blue')


restore,dir3+file_a
@rhoprof
m=(size(rurms2))[2]
rgood=where(r ge 0.72)
Hrho=-(1./(deriv(r,rh)/rh))*6.96e8
Hrhom=mean(Hrho[rgood])
urms_av=sqrt(mean(rurms2[rgood,*]))
tau=Hrhom/urms_av
eta_t = tau*(total(rurms2,2)/double(m))/3.
eta_t_av = mean(eta_t[rgood])

ithp=where(th ge -70 and th le -50)
nthp=(size(ithp))[1]
omp=total(omega[*,ithp],2)/double(nthp)
Cwpr=(2.*!pi*1e-9*rr^2/eta_t_av)*deriv(r,omp)

ithe=where(th ge -30 and th le -10)
nthe=(size(ithe))[1]
ome=total(omega[*,ithe],2)/double(nthe)
Cwer=(2.*!pi*1e-9*rr^2/eta_t_av)*deriv(r,ome)
print,'eta_t, Cwr_pole, Cwr_eq '
print,eta_t_av, max(abs(Cwpr[3:l-2])),max(abs(Cwer[3:l-2]))

oplot,r[1:l-2],smooth(Cwpr[1:l-2],2),li=2,thick=10,color=cgColor('green')
oplot,r[1:l-2],smooth(Cwer[1:l-2],2),li=0,thick=10,color=cgColor('green')


oplot,[0.6,0.98],[0.,0.],li=1,thick=2
oplot,[0.68,0.68],[0,1900],li=1,thick=2
oplot,[0.77,0.77],[0,1900],li=1,thick=2

oplot,[0.69,0.76],[-500,-500],li=2,thick=10
xyouts,0.78,-500.0,'!6pole',charsize=1.5
oplot,[0.69,0.76],[-730,-730],li=0,thick=10
xyouts,0.78,-730,'!6equator',charsize=1.5

;;;;;;;;;;;;;;
;;;;;;;;;;;;;;
;;;;;;;;;;;;;;

dir1='/nobackupp8/gaguerre/eumag/TAC/tac-14-rf/'
dir2='/nobackupp8/gaguerre/eumag/TAC/tac-28-rf/'
dir3='/nobackupp8/gaguerre/eumag/TAC/tac-56-rf/'
itac=1
print,'itac = ',itac
file_grid='grid.sav'
file_a='velocity.sav'
restore,dir1+file_grid
restore,dir1+file_a
m=(size(rurms2))[2]
l=(size(r))[1]
@rhoprof
m=(size(rurms2))[2]
rgood=where(r ge 0.72)
Hrho=-(1./(deriv(r,rh)/rh))*6.96e8
Hrhom=mean(Hrho[rgood])
urms_av=sqrt(mean(rurms2[rgood,*]))
tau=Hrhom/urms_av
eta_t = tau*(total(rurms2,2)/double(m))/3.
eta_t_av = mean(eta_t[rgood])

ithp=where(th ge -70 and th le -50)
nthp=(size(ithp))[1]
omp=total(omega[*,ithp],2)/double(nthp)
Cwpr=(2.*!pi*1e-9*rr^2/eta_t_av)*deriv(r,omp)

ithe=where(th ge -30 and th le -10)
nthe=(size(ithe))[1]
ome=total(omega[*,ithe],2)/double(nthe)
Cwer=(2.*!pi*1e-9*rr^2/eta_t_av)*deriv(r,ome)

print,'eta_t, Cwr_pole, Cwr_eq '
print,eta_t_av, max(abs(Cwpr[3:l-2])),max(abs(Cwer[3:l-2]))
plot,r[1:l-2],Cwpr[1:l-2],xrange=[0.61,0.96],charsize=1.5,xstyle=1, $
	ystyle=1,thick=5,xtitle='!8r/R!6'+sunsymbol(),yr=2.*!pi*[-300,350],$
	title='d) !9d!D!8r!N!4X!8R'+sunsymbol()+'!U3!N/!4g!6!Dt0!N!6, RC',/nodata,xticks=4
oplot,r[3:l-2],smooth(Cwpr[3:l-2],2),li=2,thick=10,color=cgColor('red')
oplot,r[3:l-2],smooth(Cwer[3:l-2],2),li=0,thick=10,color=cgColor('red')

restore,dir2+file_a
@rhoprof
m=(size(rurms2))[2]
rgood=where(r ge 0.72)
Hrho=-(1./(deriv(r,rh)/rh))*6.96e8
Hrhom=mean(Hrho[rgood])
urms_av=sqrt(mean(rurms2[rgood,*]))
tau=Hrhom/urms_av
eta_t = tau*(total(rurms2,2)/double(m))/3.
eta_t_av = mean(eta_t[rgood])

ithp=where(th ge -70 and th le -50)
nthp=(size(ithp))[1]
omp=total(omega[*,ithp],2)/double(nthp)
Cwpr=(2.*!pi*1e-9*rr^2/eta_t_av)*deriv(r,omp)

ithe=where(th ge -30 and th le -10)
nthe=(size(ithe))[1]
ome=total(omega[*,ithe],2)/double(nthe)
Cwer=(2.*!pi*1e-9*rr^2/eta_t_av)*deriv(r,ome)
print,'eta_t, Cwr_pole, Cwr_eq '
print,eta_t_av, max(abs(Cwpr[3:l-2])),max(abs(Cwer[3:l-2]))

oplot,r[3:l-2],smooth(Cwpr[3:l-2],2),li=2,thick=10,color=cgColor('blue')
oplot,r[3:l-2],smooth(Cwer[3:l-2],2),li=0,thick=10,color=cgColor('blue')


restore,dir3+file_a
@rhoprof
m=(size(rurms2))[2]
rgood=where(r ge 0.72)
Hrho=-(1./(deriv(r,rh)/rh))*6.96e8
Hrhom=mean(Hrho[rgood])
urms_av=sqrt(mean(rurms2[rgood,*]))
tau=Hrhom/urms_av
eta_t = tau*(total(rurms2,2)/double(m))/3.
eta_t_av = mean(eta_t[rgood])

ithp=where(th ge -70 and th le -50)
nthp=(size(ithp))[1]
omp=total(omega[*,ithp],2)/double(nthp)
Cwpr=(2.*!pi*1e-9*rr^2/eta_t_av)*deriv(r,omp)

ithe=where(th ge -30 and th le -10)
nthe=(size(ithe))[1]
ome=total(omega[*,ithe],2)/double(nthe)
Cwer=(2.*!pi*1e-9*rr^2/eta_t_av)*deriv(r,ome)

print,'eta_t, Cwr_pole, Cwr_eq '
print,eta_t_av, max(abs(Cwpr[3:l-2])),max(abs(Cwer[3:l-2]))
oplot,r[3:l-2],0.5*smooth(Cwpr[3:l-2],2),li=2,thick=10,color=cgColor('green')
oplot,r[3:l-2],0.5*smooth(Cwer[3:l-2],2),li=0,thick=10,color=cgColor('green')


oplot,[0.6,0.98],[0.,0.],li=1,thick=2
oplot,[0.68,0.68],[0,2200],li=1,thick=2
oplot,[0.77,0.77],[0,2200],li=1,thick=2

oplot,[0.69,0.76],[-800,-800],li=2,thick=10
xyouts,0.78,-800.0,'!6pole',charsize=1.5
oplot,[0.69,0.76],[-1200,-1200],li=0,thick=10
xyouts,0.78,-1200,'!6equator',charsize=1.5

;oplot,[0.65,0.69],[-800,-800],color=cgColor('red'),thick=13
;xyouts,0.7,-800,'models 01',charsize=1.5
;oplot,[0.65,0.69],[-1200,-1200],color=cgColor('blue'),thick=13
;xyouts,0.7,-1200,'models 02',charsize=1.5
;oplot,[0.65,0.69],[-1600,-1600],color=cgColor('green'),thick=13
;xyouts,0.7,-1600,'models 03',charsize=1.5

end
