;must execute (mnew.pro)
;reading file ....
openr, lun, 'polyindex.dat', /get_lun
rows=l
data=fltarr(2,rows)
readf, lun, data
;setting variable vectors ...
print, 'reading data of polytropic index profile ...'

z_t=data(0,*)
z_mpoly=z_t(0:*)
;
mpoly_t=data(1,*)
mpoly=mpoly_t(0:*)
;printing important diagnostic data ...
print, ' data ready ...'
print, 'polyindex.dat initial values: ...'
print, 'mpoly0 = ', mpoly(1)
print, 'mpoly1 = ', mpoly(l/2)
print, 'mpoly2 = ', mpoly(l-1)
print, '____________________________________________________________'
