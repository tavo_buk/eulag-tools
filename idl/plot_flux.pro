if !d.name eq 'PS' then begin
    device,xsize=22,ysize=12,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
end
!x.margin=[12,3]
!p.charsize=2

!p.multi=[0,2,2] & !p.charsize=2.5

plot,z,fconv,xtitle='(z)',ytitle='!8F(conv)', thick=3
plot,z,fconv_prom,xtitle='(z)',ytitle='!8<F(conv)>', thick=3
plot,z,fkin,xtitle='(z)',ytitle='!8F(kin)', thick=3
plot,z,fkin_prom,xtitle='(z)',ytitle='!8<F(kin)>', thick=3
;plot,z,cs,xtitle='(z)',ytitle='!8Cs', yr=[min(cs),max(cs)]

!p.multi=0
end
