;must execute (mnew.pro)
;reading file ....
openr, lun, 'strat.dat', /get_lun
rows=l
data=fltarr(6,rows)
readf, lun, data
;setting variable vectors ...
z_temp=data(0,*)
z_strat=z_temp(0:*)
;
zcum_temp=data(1,*)
zcum_strat=zcum_temp(0:*)
;
temp_temp=data(2,*)
temp_strat=temp_temp(0:*)
;
rho_temp=data(3,*)
rho_strat=rho_temp(0:*)
;
rhoad_temp=data(4,*)
rhoad_strat=rhoad_temp(0:*)
;
theta_temp=data(5,*)
theta_strat=theta_temp(0:*)
;printing important diagnostic data ...
drho=rho_strat(0)/rho_strat(rows-1)
maxtheta=max(theta_strat)
dTheta=maxtheta-theta_strat(rows-1)
print, 'Polytropic stratification data ready ...'
print, 'strat.dat initial values: ...'
print, 'rho00 = ', rho_strat(0)
print, 'temp00 = ', temp_strat(0)
print, 'Density stratification quotient ... '
print, 'rho (Bottom/Top) = ', drho
print, 'max-top difference of pot. temperature in the unstable zone ...'
print, 'Theta (Bmax-Top) = ', dTheta
print, '________________________________________________________________'
end
