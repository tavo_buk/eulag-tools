if !d.name eq 'PS' then begin                                                                                                      
    device,xsize=22,ysize=12,yoffset=3
    !p.thick=2 & !x.thick=1 & !y.thick=1
end
!x.margin=[4,3] & !y.margin=[4,2]
!p.charsize=1.4
!p.multi=[0,2,1]
default,imhd,1
default,nn,300
nt=(size(time))[1]
rsinth=fltarr(m,l)

for k=0,l-1 do begin
 for j=0,m-1 do begin
  rsinth[j,k]=rr*r[k]*sin(theta[j])
 endfor
endfor

; ivav: volume average
 r2s_frMC=fltarr(m,l,nt)
 r2s_frRS=fltarr(m,l,nt)
 int_frMC=fltarr(l,nt)
 int_frRS=fltarr(l,nt)
 
 if (imhd gt 0) then begin
  r2s_frMT=fltarr(m,l,nt)
  r2s_frMS=fltarr(m,l,nt)
  int_frMT=fltarr(l,nt)
  int_frMS=fltarr(l,nt)
 
  rs_fthMT=fltarr(m,l,nt)
  rs_fthMS=fltarr(m,l,nt)
  int_fthMT=fltarr(m,nt)
  int_fthMS=fltarr(m,nt)
 endif
 
 rs_fthMC=fltarr(m,l,nt)
 rs_fthRS=fltarr(m,l,nt)
 int_fthMC=fltarr(m,nt)
 int_fthRS=fltarr(m,nt)
 
 
 for j=0,m-1 do begin
  for k=0, l-1 do begin
     ; quantities to integrate
     r2s_frRS[j,k,*] = rr*r[k]*rsinth[j,k]*frRS[j,k,*]
     r2s_frMC[j,k,*] = rr*r[k]*rsinth[j,k]*frMC[j,k,*]
     rs_fthRS[j,k,*] = rsinth[j,k]*fthRS[j,k,*]
     rs_fthMC[j,k,*] = rsinth[j,k]*fthMC[j,k,*]
     if (imhd gt 0) then begin
      r2s_frMT[j,k,*] = rr*r[k]*rsinth[j,k]*frMT[j,k,*]
      r2s_frMS[j,k,*] = rr*r[k]*rsinth[j,k]*frMS[j,k,*]
      rs_fthMT[j,k,*] = rsinth[j,k]*fthMT[j,k,*]
      rs_fthMS[j,k,*] = rsinth[j,k]*fthMS[j,k,*]
     endif
   endfor
 endfor
 
 ; Integrated fluxes
 for it=0,nt-1 do begin
  for k = 0,l-1 do begin 
   int_frRS[k,it]=int_tabulated(theta,reform(r2s_frRS[*,k,it]))
   int_frMC[k,it]=int_tabulated(theta,reform(r2s_frMC[*,k,it]),/double)
   if (imhd gt 0) then begin
    int_frMT[k,it]=int_tabulated(theta,reform(r2s_frMT[*,k,it]),/double)
    int_frMS[k,it]=int_tabulated(theta,reform(r2s_frMS[*,k,it]),/double)
   endif
  endfor
 endfor
 for it=0,nt-1 do begin
  for j = 0,m-1 do begin 
   int_fthRS[j,it]=int_tabulated(rr*r,reform(rs_fthRS[j,*,it]),/double)
   int_fthMC[j,it]=int_tabulated(rr*r,reform(rs_fthMC[j,*,it]),/double)
   if (imhd gt 0) then begin
    int_fthMT[j,it]=int_tabulated(rr*r,reform(rs_fthMT[j,*,it]),/double)
    int_fthMS[j,it]=int_tabulated(rr*r,reform(rs_fthMS[j,*,it]),/double)
   endif
  endfor
 endfor
 
 int_frRSm=total(int_frRS[*,nt-nn:*],2)/double(nn)
 int_fthRSm=total(int_fthRS[*,nt-nn:*],2)/double(nn)
 int_frMCm=total(int_frMC[*,nt-nn:*],2)/double(nn)
 int_fthMCm=total(int_fthMC[*,nt-nn:*],2)/double(nn)
 if (imhd gt 0) then begin
  int_frMSm=total(int_frMS[*,nt-nn:*],2)/double(nn)
  int_fthMSm=total(int_fthMS[*,nt-nn:*],2)/double(nn)
  int_frMTm=total(int_frMT[*,nt-nn:*],2)/double(nn)
  int_fthMTm=total(int_fthMT[*,nt-nn:*],2)/double(nn)
 endif
;restore,'stresses_mhd.sav'
nt=(size(int_frMC))[2]
fac=1./sqrt(5.)
sdfrmc=fltarr(l)
sdfrrs=fltarr(l)
sdfrmt=fltarr(l)
sdfrms=fltarr(l)
sdfthmc=fltarr(m)
sdfthrs=fltarr(m)
sdfthmt=fltarr(m)
sdfthms=fltarr(m)
r1=reverse(r)
xv=[r,r]
xv=[r,r1]
th=90.-theta*180./!pi
th1=reverse(th)
thv=[th,th]
thv=[th,th1]

print,'averaging over ',nn, ' time steps'
frMCm=total(int_frMC[*,nt-nn:*],2)/double(nn)/1.e30
frRSm=total(int_frRS[*,nt-nn:*],2)/double(nn)/1.e30
if (imhd gt 0) then begin
 frMTm=total(int_frMT[*,nt-nn:*],2)/double(nn)/1.e30
 frMSm=total(int_frMS[*,nt-nn:*],2)/double(nn)/1.e30
endif

plot,r,frMCm,xtitle='r/R'+sunsymbol(),title='a) !8I!Dr!N!6/!810!U30!N!6 [kg m!U2!N s!U-2!N]',$
;	xstyle=1,ystyle=1,yr=[-3,1.5],/nodata,xticks=3,xr=[0.61,0.96]
;	xstyle=1,ystyle=1,yr=[-0.05,0.05],/nodata,xticks=3,xr=[0.61,0.96]
	xstyle=1,ystyle=1,yr=[-80,80.0],/nodata,xticks=3,xr=[0.1,0.96]

oplot,[0.61,0.96],r*0,li=1
oplot,[0.72,0.72],[-10,10],li=1

oplot,r,smooth(frMCm,2),thick=7, color=cgColor("red")
oplot,r,smooth(frRSm,2),thick=7, color=cgColor("orange"),li=0

xyouts,0.85,0.9,'MC'
xyouts,0.86,-0.9,'RS'

if (imhd gt 0) then begin
 oplot,r,smooth(frMTm,2),color=cgColor("blue"),thick=7
 oplot,r,smooth(frMSm,2),thick=7, color=cgColor("green"),li=0
 oplot,r,smooth((frMCm+frMTm+frRSm+frMSm),2),li=3,thick=7

 xyouts,0.68,0.2,'MT'
 xyouts,0.88,0.3,'MS'
endif


; NOW FOR I_TH

fthMCm=total(int_fthMC[*,nt-nn:*],2)/double(nn)/1.e30
fthRSm=total(int_fthRS[*,nt-nn:*],2)/double(nn)/1.e30
if (imhd gt 0) then begin
 fthMTm=total(int_fthMT[*,nt-nn:*],2)/double(nn)/1.e30
 fthMSm=total(int_fthMS[*,nt-nn:*],2)/double(nn)/1.e30
endif
tti='!S!A-!R!NXY'
plot,th,fthMCm,xtitle='90-!7h!6',title=tti,$      ; 'b) !8I!D!7h!N!6/!810!U30!N!6 [kg m!U2!N s!U-2!N]',$
;	xstyle=1,ystyle=1,yr=[-0.30,0.30],/nodata,xticks=3
;	xstyle=1,ystyle=1,yr=[-0.85,0.85],/nodata,xticks=3
	xstyle=1,ystyle=1,yr=[-13,13],/nodata,xticks=3

oplot,th,smooth(fthMCm,2),thick=7, color=cgColor("red")
oplot,th,smooth(fthRSm,2),thick=7, color=cgColor("orange"),li=0

xyouts,-55,0.05,'MC'
xyouts,35,0.24,'RS'


if (imhd gt 0) then begin
 oplot,th,smooth(fthMTm,2),thick=7, color=cgColor("blue")
 oplot,th,smooth(fthMSm,2),thick=7, color=cgColor("green"),li=0
 oplot,th,smooth((fthMCm+fthMTm+fthRSm+fthMSm),2),li=3,thick=7
 
 xyouts,45,-0.22,'MT'
 xyouts,-40,-0.02,'MS'
endif

!p.multi=0
end
