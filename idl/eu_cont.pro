
if !d.name eq 'PS' then begin
    device,xsize=20,ysize=12,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
end

!p.multi=[0,3,1]
!p.charsize=2
;; creating 2D phi averages 

ua = total(u[*,*,*,*],1)/nphi
va = total(v[*,*,*,*],1)/nphi
wa = total(w[*,*,*,*],1)/nphi
xt = '!8x!6'
yt = '!8y!6'

uat = total(ua[*,*,it-6:it-1],3)/5.
vat = total(va[*,*,it-6:it-1],3)/5.
wat = total(wa[*,*,it-6:it-1],3)/5.

lev=grange(min(uat),max(uat),60)    
contour,transpose(uat),x,y,/fi,nl=64,/iso,title='!8u!6',xtitle=xt,ytitle=yt,levels=lev
lev=grange(-0.2,0.2,60)    
contour,transpose(vat),x,y,/fi,nl=64,/iso,title='!8v!6',xtitle=xt,levels=lev
xyouts,0.05,1.2,'Half Sphere',charsize=1.5
lev=grange(min(wat),max(wat),60)    
contour,transpose(wat),x,y,/fi,nl=64,/iso,title='!8w!6',xtitle=xt,levels=lev



end
