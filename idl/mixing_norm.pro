;________________________________________________________________
;NORMALIZATION COEFFICIENTS averaged over the unstable zone
;________________________________________________________________

;default set the method to 1, following Axel

method=1
print, 'computing normalization coefficients with method ', method
print, '__________________________________________________________'

;if method eq !Null then begin
; print, 'please choose a method to compute the normalization factors ...'
 print, 'method = 1 --> coefs = <a>/<b>'
 print, 'method = 2 --> coefs = <a/b>'
 print, '___________________________________________________________'
;endif else begin

 print, 'computing coefficients at slice = ', topslice 

 if method eq 2 then begin
  print, 'METHOD NOT AVAILABLE AT THE MOMENT ...'
  ; --> local coefficients before spatial average
  ;k_12=make_array(z_num)
  ;k_13=make_array(z_num)

  ;for jj=1,z_num-2 do begin
  ; k_12(jj)=g1(jj)/g2(jj)
  ; k_13(jj)=g1(jj)/g3(jj)
  ;endfor
  ; --> averaged normalization coeficients
  ;k_12_prom=total(k_12[35:-1],1)/double(z_num-37)
  ;k_13_prom=total(k_13[35:-1],1)/double(z_num-37)
 endif

 if method eq 1 then begin
  g1_mean=total(g1[35:-1],1)/double(z_num-36)
  g2_mean=total(g2[35:-1],1)/double(z_num-36)
  g3_mean=total(g3[35:-1],1)/double(z_num-36)
  k_12_prom=g1_mean/g2_mean
  k_13_prom=g1_mean/((g3_mean)^(2./3.))
 endif

 print, 'plotting using method = ', method 
 print, 'method = 1 --> coefs = <a>/<b>'

 ;plotting mode with the legend
 names=['!8solid = g!D!61','!8dotted = g!D!62!N!8(k!D!612!N)', $
  '!8dash-dot = g!D!63!N!8(k!D!613!N)']
 xpos=0.2
;ypos=[0.0005,0.0007,0.0009]
;ypos=[0.003,0.004,0.005]    ; vf
 ypos=[0.03,0.04,0.05]       ; cf
;ypos=[0.01,0.02,0.03]
 ;plot1=plot(z,g1,name='g1')
 ;plot2=plot(z,g2*k_12_prom,name='g2*k_12')
 ;plot3=plot(z,((g3)^(2./3.))*k_13_prom,name='g3*k_13')
 ;leg = legend(target=[plot1,plot2,plot3],position=[170,0.9],$
 ;  /DATA, /auto_text_color)

 loadct, 30

 plot,z,g1,xtitle='(z)', thick=3 ;, yr=[-0.01,0.02]
 oplot,z,g2*k_12_prom, thick=3, linestyle=1
 oplot,z,((g3)^(2./3.))*k_13_prom, thick=3, linestyle=3
 oplot, z, make_array(z_num), linestyle=5
 for ii=0, 2 do xyouts, xpos, ypos[ii], names[ii]
;endelse
end
