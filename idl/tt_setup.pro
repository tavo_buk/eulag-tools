eu_read_param,o=o
default, itac, 0

; defining grid

n = o.n
m = o.m
l = o.l
nt = o.nt
dt = o.dt00
nslice = o.nslice
nxaver = o.nxaver

theta = indgen(m)*!pi/(m-1)
phi = indgen(n)*2.*!pi/(n-1)

rsol = 1.36206e9 ; Solar radii in [m/s]
rr= 0.95*rsol
rds = 0.1*rr ; base of the convection zone in [m/s]
r = (rds + indgen(l)*o.dz00)/rsol
x = r#sin(theta)
y = r#cos(theta)

px=r#cos(phi)
py=r#sin(phi)

th = 180.*theta/!pi - 90.
ph = 180.*phi/!pi -180.

; defining stratification

openr, lun, 'strat.dat', /get_lun
rows=l
data=fltarr(8,rows)
readf, lun, data
;setting variable vectors ...
t_am = reform(data[2,*])
t_ad = reform(data[3,*])
rho_am = reform(data[4,*])
rho_ad = reform(data[5,*])
the = reform(data[6,*])
p_ad = reform(data[7,*])
close,lun

save,r,th,ph,filename='grid.sav'
end
