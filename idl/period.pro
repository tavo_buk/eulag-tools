if !d.name eq 'PS' then begin
    device,xsize=12,ysize=12,yoffset=3
    !p.thick=1 & !x.thick=1 & !y.thick=1
end
@eu_setup
!p.multi=[0,1,2] & !p.charsize=2.
nt=(size(by))[3]
print,nt
nl=64
year = 365.*24.*60.*60.
time = (nxaver*dt*indgen(nt))/year
good=where(time gt 0)
ngood=(size(good))[1]

bphi_or=fltarr(l,m,ngood)

; organizing the toroidal magnetic field
for k=0, l-1 do begin
  for j=0,m-1 do begin
    bphi_or[k,j,good] = (bx[j,k,good])^2
  endfor
endfor

; calculating the period using fft
tgood=20.
itg=where(time ge tgood)
print,'calculating period after t = ',time[itg[0]]
bperiod = reform(bphi_or[59,31,itg])
tperiod = time[itg]
mspec = abs(fft(bperiod))
pspec = mspec^2
sampling_interval = nxaver*dt/year   ; in years
freq_nyquist = 0.5/sampling_interval
n_sunspots = n_elements(bperiod)
freq = findgen(n_sunspots/2)/(n_sunspots/2-1)*freq_nyquist
freq_nodc = freq[1:n_sunspots/2-1]
pspec_nodc = 2*pspec[1:n_sunspots/2-1]

pspec_nodc_peak = max(pspec_nodc, i_peak)
freq_nodc_peak = freq_nodc[i_peak]
sunspot_peak_period = 1.0/freq_nodc_peak
print,'freq = ',freq_nodc_peak
print,'T_m = ',sunspot_peak_period

dd=where(freq_nodc lt 0.5)
pspec_nodc_peak2 = max(pspec_nodc[dd], i_peak2)
freq_nodc_peak2 = freq_nodc[i_peak2]
sunspot_peak_period2 = 1.0/freq_nodc_peak2
print,'freq2 = ',freq_nodc_peak2
print,'T_m2 = ',sunspot_peak_period2


plot,tperiod,bperiod,xtitle='!8t !6[years]',ytitle='!8B!6'

plot,freq_nodc, pspec_nodc, /xlog, /ylog,xtitle='!8f!6 [years!U-1!N]',$
     ytitle='Spectral Density'
oplot,[freq_nodc_peak,freq_nodc_peak],[min(pspec_nodc),max(pspec_nodc)],li=2
oplot,[freq_nodc_peak2,freq_nodc_peak2],[min(pspec_nodc),max(pspec_nodc)],li=2

!p.multi=0
end
