rh00=rho_strat(0)
t00=temp_strat(0)
th00=t00
;g=1.
if g eq !Null then begin
 print, 'please set the value of the g aceleration ...'
endif
rg=0.4
cp=2.5*rg
cap=rg/cp
capi=1./cap
ss=g/(cp*th00)
press00=rg*rh00*t00
gam=5./3.

zmin=z_strat(0)
zmax=z_strat(-1)
dz=(zmax-zmin)/l
z=indgen(l)*dz+zmin

rho_ad = rh00*(1-ss*(z))^(capi-1)
p_ad = press00*(1-ss*z)^(capi)
t_ad = t00*(1-ss*z)
;theta_ad = t_ad*((t00*rh00)/(t_ad*rho_ad))^cap
theta_ad = t00
cs_ad=sqrt(gam*p_ad/rho_ad)



theaver=total(the,1)/m                       ; <theta>
ttot_aver=make_array(n_elements(theaver(*,0)),n_elements(theaver(0,*)))
  for ii=0,n_elements(theaver(0,*))-1 do begin
   for jj=0,n_elements(theaver(*,0))-1 do begin
    ttot_aver(jj,ii)=(t_ad(jj)/theta_ad)*theaver(jj,ii)
                                 ; <T>=(T_ad/theta_ad)*<theta>
   endfor
  endfor

end
