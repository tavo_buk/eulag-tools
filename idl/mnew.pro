filename= ''
read, filename, prompt='enter file to read xaverages '
if (strlen(filename) eq 0) then filename='xaverages.dat'
tthew = dblarr(m,l)
uu = dblarr(m,l)
vv = dblarr(m,l)
ww = dblarr(m,l)
oox = dblarr(m,l)
ooy = dblarr(m,l)
ooz = dblarr(m,l)
uu2 = dblarr(m,l)
vv2 = dblarr(m,l)
ww2 = dblarr(m,l)
oox2 = dblarr(m,l)
ooy2 = dblarr(m,l)
ooz2 = dblarr(m,l)
rrwv = dblarr(m,l)
rrwu = dblarr(m,l)
rrvu = dblarr(m,l)
bbx = dblarr(m,l)
bby = dblarr(m,l)
bbz = dblarr(m,l)
bbx2 = dblarr(m,l)
bby2 = dblarr(m,l)
bbz2 = dblarr(m,l)
bbzby = dblarr(m,l)
bbzbx = dblarr(m,l)
bbxby = dblarr(m,l)
pp= dblarr(m,l)
tthe = dblarr(m,l)

t = 0L
openr,1,filename,/f77
ns = 0L
while (not eof(1)) do begin
  readu,1,tthew,t
  readu,1,uu,t
  readu,1,vv,t
  readu,1,ww,t
  readu,1,oox,t
  readu,1,ooy,t
  readu,1,ooz,t
  readu,1,uu2,t
  readu,1,vv2,t
  readu,1,ww2,t
  readu,1,oox2,t
  readu,1,ooy2,t
  readu,1,ooz2,t
  readu,1,rrwv,t
  readu,1,rrwu,t
  readu,1,rrvu,t
  readu,1,bbx,t
  readu,1,bby,t
  readu,1,bbz,t
  readu,1,bbx2,t
  readu,1,bby2,t
  readu,1,bbz2,t
  readu,1,bbzby,t
  readu,1,bbzbx,t
  readu,1,bbxby,t
  readu,1,pp,t
  readu,1,tthe,t
  if (ns LT 1) then begin
    thew = tthew  
    u = uu  
    v = vv
    w = ww
    ox = oox  
    oy = ooy
    oz = ooz
    u2 = uu2
    v2 = vv2
    w2 = ww2
    ox2 = oox2
    oy2 = ooy2
    oz2 = ooz2
    rwv = rrwv
    rwu = rrwu
    rvu = rrvu
    bx = bbx  
    by = bby
    bz = bbz
    bx2 = bbx2  
    by2 = bby2
    bz2 = bbz2
    bzby = bbzby  
    bzbx = bbzbx
    bxby = bbxby
    p = pp
    the = tthe
    print,'Reading file: ',filename, ns
  endif else begin
    thew = [[[thew]],[[tthew]]]
    u = [[[u]],[[uu]]]
    v = [[[v]],[[vv]]]
    w = [[[w]],[[ww]]]
    ox = [[[ox]],[[oox]]]
    oy = [[[oy]],[[ooy]]]
    oz = [[[oz]],[[ooz]]]
    u2 = [[[u2]],[[uu2]]]
    v2 = [[[v2]],[[vv2]]]
    w2 = [[[w2]],[[ww2]]]
    ox2 = [[[ox2]],[[oox2]]]
    oy2 = [[[oy2]],[[ooy2]]]
    oz2 = [[[oz2]],[[ooz2]]]
    rwv = [[[rwv]],[[rrwv]]]
    rwu = [[[rwu]],[[rrwu]]]
    rvu = [[[rvu]],[[rrvu]]]
    bx = [[[bx]],[[bbx]]]
    by = [[[by]],[[bby]]]
    bz = [[[bz]],[[bbz]]]
    bx2 = [[[bx2]],[[bbx2]]]
    by2 = [[[by2]],[[bby2]]]
    bz2 = [[[bz2]],[[bbz2]]]
    bzby = [[[bzby]],[[bbzby]]]
    bzbx = [[[bzbx]],[[bbzbx]]]
    bxby = [[[bxby]],[[bbxby]]]
    p = [[[p]],[[pp]]]
    the = [[[the]],[[tthe]]]
    print,'Reading file: ',filename, ns
  endelse
  wait,0.
  ns ++
end
close,1
ptemp = the
help,the,ptemp
end
