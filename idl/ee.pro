if !d.name eq 'PS' then begin
 device,xsize=18,ysize=12,yoffset=3
 !p.thick=2 & !x.thick=1 & !y.thick=1
end
!p.charsize=1.5
!x.margin=[8,4]


file='table.dat'
aro=fltarr(6)
ara=fltarr(6)
aurms=fltarr(6)
ama=fltarr(6)
axhi=fltarr(6)
alambda=fltarr(6)
aem=fltarr(6)
aet=fltarr(6)
aep=fltarr(6)
aex=fltarr(6)
aey=fltarr(6)
aez=fltarr(6)
openr,1,file
ns = 0L
for i=0,5 do begin
 readf,1,ro,ra,urms,ma,xhi,lambda,em,et,ep,ex,ey,ez
 aro[i]=ro
 axhi[i]=xhi
 aurms[i]=urms
 aem[i]=em
 aex[i]=ex
 aey[i]=ey
 aez[i]=ez
endfor
close,1
bmean=sqrt(aex^2+aey^2+aez^2)
bpol=sqrt(aey^2+aez^2)
binter = linfit(alog(aro),alog(bmean))
binterp = linfit(alog(aro),alog(bpol))
print,binter
print,binterp

plot,aro,bmean,xtitle='!6Ro',ytitle='maximum magnetic field',xr=[1e-2,1],xstyle=1,yrange=[0.002,1],$
	ystyle=1,/ylog,/xlog,/nodata
oplot,aro,bmean,psym=6,symsize=1.,thick=10
plots,aro[0],bmean[0],psym=6,symsize=1.5,color=cgColor('red'),thick=6
plots,aro[5],bmean[5],psym=6,symsize=1.5,color=cgColor('red'),thick=6

oplot,aro,exp(binter[0])*aro^(binter[1])

;oplot,aro,bpol,li=2
oplot,aro,bpol,psym=5,thick=10,color=cgColor('blue')
oplot,aro,exp(binterp[0])*aro^(binterp[1]),li=2,color=cgColor('blue')

plots,0.02,0.02,psym=6,thick=10,symsize=1
xyouts,0.023,0.019,'!8B!D!7u!N!6 (T) ',charsize=1.5

plots,0.02,0.01,psym=5,thick=10,color=cgColor('blue')
xyouts,0.023,(0.01-0.001),'!8B!D!6p!N!6 (T)',charsize=1.5

end
