program main

 implicit none

 call star ()

 stop
end

!************************************************************

subroutine star ()

 implicit none

 integer (kind=4),parameter :: n=2
 integer (kind=4), parameter :: l=64
 integer (kind=4) k
 external star_f
 real (kind=8) x0,x1,xmin,xmax,p,dx
 real (kind=8) u0(n),u1(n)
 real (kind=8) the0, the1
 real (kind=8) Rg,cp,cap,Rs,Z
 real (kind=8) t00,rh00,M00,PI,g0,g1,Gc
 real (kind=8) xt,zt,fs

 PI = 3.14159
 Gc = 6.674e-11
 Rs = 6.96e8
 xmin = 0.55*Rs
 xmax = 1.*Rs
 x0 = xmin
 Rg = 13732.
 cp = 2.5*Rg
 cap = Rg/cp
 rh00 = 815.  
 t00 = 3.5e6
 p = Rg*rh00*t00
 Z = 15.
 
! dx = (xmax-x0)/(l-1)

 u0(1) = t00
 u0(2) = rh00

 the0 = u0(1) * (t00*rh00 / (u0(1)*u0(2)) )**cap
 do k=1,l
  xt = (xmax-xmin)*k/(l)
  fs = (exp(((xmax-xmin) - xt)*log(1. + Z)/(xmax-xmin)) - 1.)/Z   
  write( *, '(2x,g14.6,2x,g14.6,2x,g14.6,2x,g16.8,2x,e10.4)') x0/Rs,u0(1),u0(2),the0,p
  ! streetching function used in Chan & Sofia 1986
  x1 = xmax/(1.+(xmax-xmin)/(Z*xmin)*(exp((l - k)*log(1.+Z)/(l-1.))-1.))
  dx =  x1 - x0
  call rk4vec(x0,n,u0,dx,star_f,u1)
  the1 = u1(1)*( (t00*rh00) / (u1(1)*u1(2)) )**cap
  p = Rg*u1(1)*u1(2)
  x0 = x1
  u0(1:n) = u1(1:n)
  the0 = the1

 end do
 
 return
end

!*************************************************************

subroutine star_f (x,n,u,uprime)

 implicit none
 
 integer (kind=4) n
 real (kind=8) x
 real (kind=8) u(n)
 real (kind=8) uprime(n)
 real (kind=8) g0,g,Ms,Rs,Rb,PI
 real (kind=8) Rg
 real (kind=8) mr
 real (kind=8) m1
 real (kind=8) m2
 real (kind=8) m3,m4
 real (kind=8) d
 real (kind=8) xtac
 real (kind=8) xtop,x1,x2,x3,width,width1
 
 PI = 3.14159
 g = 680.
 x1 = 0.718*6.96e8
 x2 = 0.96*6.96e8
 x3 = 0.995*6.96e8
 width = 0.015*6.96e8
 width1 = 0.005*6.96e8 

 Rg = 13732.
 m1 = 3.
 m2 = 1.5
 m3 = 1.49987
 m4 = 1.5

! mr=m1-(m1-m2)*0.5*(1.+erf((x-x1)/(width)))
! mr=m1- (m1-m2)*0.5*(1.+erf((x-x1)/(width))) &
!      + (m3-m2)*(1.+erf((x-x2)/(width1))) &
!      + (m4-m3)*(1.+erf((x-x3)/(width1)))
 mr = 1.500000
 g0 = g/(x/(0.55*6.96e8))**2
 uprime(1) = -g0 / ((mr+1.)*Rg)
 uprime(2) = (u(2) / u(1)) * (-g0/Rg - uprime(1))
 
 return 
end

!**************************************************************

subroutine rk4vec (t0,m,u0,dt,f,u)

 implicit none

 integer (kind=4) m
 real(kind=8) dt
 external f
 real (kind=8) f0(m),f1(m),f2(m),f3(m)
 real (kind=8) t0,t1,t2,t3
 real (kind=8) u(m),u0(m),u1(m),u2(m),u3(m)

 call f (t0,m,u0,f0)
 
 t1=t0+dt/2.0D+00
 u1(1:m)=u0(1:m)+dt*f0(1:m)/2.0D+00
 call f (t1,m,u1,f1)

 t2=t0+dt/2.0D+00
 u2(1:m)=u0(1:m)+dt*f1(1:m)/2.0D+00
 call f (t2,m,u2,f2)

 t3=t0+dt
 u3(1:m)=u0(1:m)+dt*f2(1:m)
 call f (t3,m,u3,f3)

 u(1:m)=u0(1:m)+dt*(f0(1:m)+2.0D+00*f1(1:m)+2.0D+00*f2(1:m)+f3(1:m))/6.0D+00
 
 return
end

