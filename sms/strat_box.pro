;
rr=1.             ; m
nr=64            ; nlevels to compute stratification
Tbot=20.         ; base temperature
rho_bot=1.       ; bse density
rbot=0. & r1=0.5*rr & r2=2.*rr & d=0.1*rr 
dz=(r2-rbot)/double(nr-1)
Luminosity=0.0404275
;
r=rbot+findgen(nr)/(nr-1)*(r2-rbot) & dr=r(1)-r(0) ; Radius
;
rds=rbot       ;m
GG=10.0   ;N(m/kg)^2
r=rds+indgen(nr)*dz
gr=-GG     ;m/s^2
g0=gr
gamma=5./3.
cp=1.
cv=cp/gamma ; Heat capacities
;
rho=fltarr(nr) & T=fltarr(nr) & lnr=fltarr(nr) & m=fltarr(nr)
;
; Polytropic index 
;
m1=3.
m2=1.499
m=m1-(m1-m2)*0.5*(1.+ erf((r-r1)/d))
;
; Temperature stratification
;
dTdr =gr/(cv*(gamma-1.)*(m+1.))     ; T-gradient for thermal stratification
;
T(0)=Tbot
for i=1,nr-1 do begin
  T(i)=T(i-1)+dTdr(i-1)*dr
endfor
;
cs2=cV*T*(gamma-1.)*gamma ; Sound speed squared
;
dlnTdr=deriv(r,T)/T
dlnrdr=gr/(T*cv*(gamma-1.))-dlnTdr
;
; Density stratification
;
lnr(0)=alog(rho_bot)
for i=1,nr-1 do begin
  lnr(i)=lnr(i-1)+dlnrdr(i-1)*dr
endfor
;
rho=exp(lnr)                         ; Density
p=rho*cv*T*(gamma-1.)                ; Pressure
s=cv*alog(p)-cp*lnr                 ; Entropy
;s=alog(p)/gamma-lnr                  ; Entropy
th=exp(s/cp)                         ;(p)^(1./gamma)/(rho)
; Brunt-V�isal� frequency
BV=r*(deriv(r,p)/(gamma*p)-deriv(r,rho)/rho)

;jj=where(r ge 0.72*rr and r le 0.74*rr)
th_max=max(th)
th00=22.
th=th00*th/th_max

l=nr
;r_new=congrid(r/rr,l)
;rho_new=congrid(rho,l)
;p_new=congrid(p,l)
;t_new=congrid(t,l)
;th_new=congrid(th_n,l)

;print,'dz= ', rr*(r_new[1]-r_new[0])
;print,'rho0= ',rho[0]
;print,'p0= ',p[0]
;print,'g0= ',gr[0]
;print,'t0= ',T[0]
;print,'th0= ',t00

openw,1,'strat.dat'
for i=0,l-1 do printf,1,rho[i],t[i],p[i],th[i] ,$
               format='(3x,F10.6,3x,F10.1,3x,E17.8,3x,F10.2)'
close,1

plot,r,th,yr=[min(th),max(th)]
;oplot,[0,1],[t00,t00],li=2

end
