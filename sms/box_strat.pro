; routine to verify the isentropic background state
; cartesian model with constant gravity

!p.multi=[0,2,1]
!p.charsize=1.5
l=64
rr=1. 
rmin=-1
rmax=rmin+2.
rds=rmin*rr     ;m
rdsi=1./rds
GG=1.
M=1.
dz=(rmax-rmin)*rr/double(l)
r=rds+indgen(l)*dz
g=-1.
g0=g
z = indgen(l-1)*dz

Rg=0.4
cp=2.5*rg
cap=rg/cp
capi=1./cap
; bottom values
rho00=150.
T00=1.8
p00=rho00*T00*Rg
th00= 1.2*T00       ;2.06008e+06

ss=g0/(cp*th00)
rho = rho00*(1.+g0*(r-rmin)/(th00*cp))^(capi-1)
;rho = rho00*(1.+cap*rho00*g0*(r-rmin)/p00)^(capi-1)
;pm0=p00*(1.-cap*z*sdi)**capi ; CAUTION: max. alt. exists
;tm0=T00*(1.-cap*z*sdi)       ; CAUTION: max. alt. exists

plot,r/rr,rho

st=-3./(th00*(l-1)*dz)
the=th00*(1.+ st*z)
plot,r/rr,the,yr=[min(the),max(the)]

!p.multi=0
end

