
!x.style=1
!y.style=1
!p.charsize=1.5
dd=read_ascii('jcd5bid15.dat')
nglobal=15
nvar=30L
ncol=(size(dd.field1))[1]
nlines=(size(dd.field1))[2]
ntotal=0L
ntotal=ncol*nlines
nn=ntotal/nvar
aa=fltarr(ntotal)
var=dblarr(nvar,nn)
k=0L
for i=0,nlines-1 do begin
  for j=0,ncol-1 do begin
    aa[k]=dd.field1[j,i]
    k++
  endfor
endfor

gaa=aa[0:nglobal-1]
vaa=aa[nglobal:*]

sr=gaa[1]
k=0L
for i=0,nn-1 do begin
  for j=0,nvar-1 do begin
    var(j,i)=vaa[k]
    k++
  endfor
endfor
print,k

gamma=5./3
gad=(gamma-1.)/gamma


;; reading the data
rr=reform(var(0,*))
rmin=0.55*sr
rmax=0.95*sr
jj=where(rr gt rmin and rr lt rmax)
ll=n_elements(jj)
r=(reverse(rr[jj]))
mass_interior=reverse(reform(var(1,jj)))
temp=reverse(reform(var(2,jj)))
p=reverse(reform(var(3,jj)))
rho=reverse(reform(var(4,jj)))
L=reverse(reform(var(6,jj)))
gamma1=reverse(reform(var(9,jj)))
gamma_ad=reverse(reform(var(10,jj)))
delta=reverse(reform(var(11,jj)))
dsdr=reverse(reform(var(14,jj)))
cp =reverse(reform(var(13,jj)))

GG=6.674e-8   ;N(m/kg)^2
M=1.988e33     ;gr
gr=GG*M/r^2/100.    ;m/s^2
omega=2.*!pi*460e-9


r_sms=r
rho_sms=rho
p_sms=p
t_sms=temp
s_sms=alog(p)/gamma1 - alog(rho)


;; organizing the data in a uniformly spaced array
lev=512
th=fltarr(lev)
rx=rmin+indgen(lev)*(rmax-rmin)/(lev-1)
dr=rx[1]-rx[0]
rhox=1e3*interpol(rho,r,rx)
px=interpol(p,r,rx)/10.
tempx=interpol(temp,r,rx)
dth=interpol(dsdr,r,rx)
gamma1x=interpol(gamma1,r,rx)

;print,'T0= ', tempx[0]
;print,'rho0= ',rhox[0]
;print,'p0= ', px[0]


;Rg=13732.

th[0]=0.
for i=1,lev-1 do begin
  th[i]=th[i-1]+dth[i-1]*dr
endfor

th1 = px^(1./gamma)/rhox

;cp=2.5*Rg

;;;;;;;;;;;;;



;plot,r,dsdr,yr=[-1,2],xr=xr

end
