; $Id: $
;
; 07-01-2011 PJK
;
; Generates piecewise polytropic stratification and heat conductivity 
; profiles for spherical convection runs. The upper layer between 
; r2/R and r/R=1. is isothermal.
;
; nr         : Number of grid points in radius. Must match nxgrid in 
;              src/cparam.local
; Tbot       : Temperature at r=rbot
; rho_bot    : Density at r=rbot
; rbot       : Radial position of the bottom of the domain in units of 
;              the stellar radius R, must match xyz0[1] in start.in
; r1         : Base of the convectively unstable region in units of the
;              stellar radius R
; r2         : Top of the convectively unstable region in units of the 
;              stellar radius R
; GM         : Gravity constant times mass of the star, must match 
;              gravx in start.in/run.in
; d          : Depth of transition layers at stable/unstable interfaces
; Luminosity : Luminosity of the star
;
; The outer boundary is assumed to be at r/R=1.
;
rr=6.96e8        ; m
nr=512          ; nlevels to compute stratification


Tbot=2973823.2   ; base temperature
rho_bot=420.92   ; bse density
rbot=0.62*rr & r1=0.71*rr & r2=0.96*rr & d=0.015*rr 
Luminosity=0.0404275
;
r=rbot+findgen(nr)/(nr-1)*(r2-rbot) & dr=r(1)-r(0) ; Radius
;
rds=rbot       ;m
rdsi=1./rds    ; bottom of the domain in m
GG=6.674e-11   ;N(m/kg)^2
M=1.988e30     ;kg

dz=(r2-rbot)/double(nr-1)
r=rds+indgen(nr)*dz
gr=-GG*M/r^2     ;m/s^2
g0=gr[0]
gamma=5./3.
Rg=13732.
cp=2.5*rg
cv=cp/gamma ; Heat capacities
;
rho=fltarr(nr) & T=fltarr(nr) & lnr=fltarr(nr) & m=fltarr(nr)
;
; Define a depth dependent polytropic index that describes the
; stratification. For the corresponding equations for dT/dr and
; drho/dr see K�pyl� et al. (2004), Astron. Astrophys., 422, 793-816
;
m1=3.
m2=1.499999
m=m1-(m1-m2)*0.5*(1.+ erf((r-r1)/d))
;
; Temperature stratification
;
dTdr =gr/(cv*(gamma-1.)*(m+1.))     ; T-gradient for thermal stratification
;
T(0)=Tbot
for i=1,nr-1 do begin
  T(i)=T(i-1)+dTdr(i-1)*dr
endfor
;
cs2=cV*T*(gamma-1.)*gamma ; Sound speed squared
;
dlnTdr=deriv(r,T)/T
dlnrdr=gr/(T*cv*(gamma-1.))-dlnTdr
;
; Density stratification
;
lnr(0)=alog(rho_bot)
for i=1,nr-1 do begin
  lnr(i)=lnr(i-1)+dlnrdr(i-1)*dr
endfor
;
;; Thermal conductivity
;;
kappa=-Luminosity/(4.*!pi*r^2*dTdr) ; Heat conductivity
;dlogkappa=fltarr(nr,3)
;dlogkappa(*,0)=deriv(r,kappa)        ; Gradient of heat conductivity
;Fbot=-kappa(0)*dTdr(0)               ; Energy flux at the base
;Ftop=-kappa(nr-1)*dTdrc(nr-1)        ; Energy flux at the surface
;;
rho=exp(lnr)                         ; Density
p=rho*cv*T*(gamma-1.)                ; Pressure
;s=cv*alog(p)-cp*lnr                  ; Entropy

s=dblarr(nr)
th=fltarr(nr)

s=alog(p)/gamma-lnr                  ; Entropy
;th=exp(s/6e3)   ;(p)^(1./gamma)/(rho)
;th=exp(s/cp)   ;(p)^(1./gamma)/(rho)
cap=rg/cp
tt0kst=min(T)
rh0kst=min(rho)
th=T*(tt0kst*rh0kst/(rho*T))^cap

jj=where(r ge 0.73*rr and r le 0.75*rr)
th00=max(th[jj])
t00=1.14*max(T)
th_n=t00*th/th00
;th_n=th



l=47
r_new=congrid(r/rr,l)
rho_new=congrid(rho,l)
p_new=congrid(p,l)
t_new=congrid(t,l)
th_new=congrid(th_n,l)

print,'dz= ', rr*(r_new[1]-r_new[0])
print,'rho0= ',rho[0]
print,'p0= ',p[0]
print,'g0= ',gr[0]
print,'t0= ',T[0]
print,'th0= ',t00

openw,1,'strat.dat'
for i=0,l-1 do printf,1,rho_new[i],t_new[i],p_new[i],th_new[i] ,$
               format='(3x,F10.6,3x,F10.1,3x,E17.8,3x,F11.3)'
close,1

plot,r/rr,th_n,yr=[min(th_n),max(th_n)] ,xr=[0.62,0.96],xstyle=1
oplot,[0,1],[t00,t00],li=2

;;
;chi=kappa/(cp*rho)                   ; Thermal diffusivity
;;
;; Brunt-V�isal� frequency
;;
BV=r*(deriv(r,p)/(gamma*p)-deriv(r,rho)/rho)
;;
;plot,r,rho/rho_bot,yr=[-0.2,1.05],ys=3,xtit='!8r!6/!8R!6',tit='!7q!8, T, p, !6B-V freq.'
;oplot,r,T/Tbot,li=2
;oplot,r,p/p(0),li=3
;oplot,r,r*0,li=1
;oplot,r,BV,li=4
;;
;print,''
;print,'Writing stratification.dat...'
;openw,1,'stratification.dat'
;for i=0,nr-1 do printf,1,alog(rho(i)),alog(rho(i)),alog(T(i))
;close,1
;;
;print,'Writing hcond_glhc.dat...'
;openw,1,'hcond_glhc.dat'
;for i=0,nr-1 do printf,1,kappa(i),dlogkappa(i,0),dlogkappa(i,1),dlogkappa(i,2)
;close,1
;print,''
;print,'Place the files stratification.dat rnd hcond_glhc.dat in the run'
;print,'directory of your simulation and insert the following lines in '
;print,'run.in:'
;print,''
;print,'&density_run_pars:'
;print,'  cs2top=',cs2(nr-1)
;print,''
;print,'&entropy_run_pars:'
;print,'  Fbot=',Fbot
;print,'  cs2cool=',cs2(nr-1)
;print,''
;;
end
